A formalisation and code generation project for complete test strategies on finite state machines.

[TOC]


# 1. Overview

This project aims at formalising, proving complete, and making executable a variety of test strategies using [Isabelle/HOL](http://isabelle.in.tum.de/), which incorporates several aspects:

- formalisation of finite state machines (FSM)
- formalisation and computation of components of test strategies such as 
    - state covers
    - distinguishing traces
    - state preambles
    - state separators
- formalisation of test strategies such as
    - W-Method [1]
    - Wp-Method [2]
    - HSI-Method [3]
    - H-Method [4]
    - SPY-Method [5]
    - SPYH-Method [6]
    - Adaptive State Counting [7]
- generating trustworthy executable code from the formalisation

This project contains the following:
- The formalisation and corresponding code generation as an Isabelle session in folder [Formalisation](Formalisation) 
- A collection of command line tools for application of the generated implementations to reference models and systems under test in folder [Tools](Tools)
- Evaluation data in folder [Evaluation](Evaluation)

# 2. Formalisation 

## 2.1 Main Components

- Theory files directly contained in folder [Formalisation](Formalisation) formalise FSMs and basic operations on them.
- Theory files in subfolder [EquivalenceTesting](Formalisation/EquivalenceTesting) formalise test strategies for language-equivalence testing and provide unifying frameworks.
- Theory files in subfolder [AdaptiveStateCounting](Formalisation/AdaptiveStateCounting) formalise the adaptive state counting strategy for reduction testing.

## 2.2. Installation and Dependencies

This project was build using [Isabelle2021-1](http://isabelle.in.tum.de/) and depends on two entries of the [Archive of Formal Proofs](https://www.isa-afp.org/index.html):

- [Light-weight Containers](https://www.isa-afp.org/entries/Containers.html)
- [Generating linear orders for datatypes](https://www.isa-afp.org/entries/Datatype_Order_Generator.html)

To install the entries, please [download](https://www.isa-afp.org/download/) the Archive and then follow [these instructions](https://www.isa-afp.org/help/) to configure your Isabelle installation to use the downloaded theories.


## 2.3. Generating the Test Suite Generator Code

The test suite generator code can be generated as follows:

    cd Formalisation
    isabelle build -e -D .

The generated code is stored in [code_export](Formalisation/code_export).
Copies of the Standard ML (SML) and Haskell exports are employed in the test suite generators ([here](Tools/Generator/SML/sml_export.sml) and [here](Tools/Generator/Haskell/test-suite-generator/src/GeneratedCode.hs)), as well as the [evaluator tool](Evaluation/tools/evaluator/sml_export.sml).

The above build process also generates a session graph of the theories developed in the formalisation and a PDF file collecting the content of the formalisation, copies of which is available [here](resources/session_graph.pdf) and [here](resources/proof_document.pdf).


# 3. Test Suite Generator

## 3.1. Building the Test Suite Generator Command Line Tool

The test-suite-generator command line tool is a Standard ML program that can be build as follows using [MLton](mlton.org):

    cd Tools/Generator/SML
    mlton test-suite-generator.mlb

## 3.2. The raw Format

The command line tool will generate test suites for finite state machines specified in the raw format of the [libfsmtest](https://bitbucket.org/JanPeleska/libfsmtest) library.
This format consists of of lines of four integers each, representing transitions of the FSM as

    <source state> <input> <output> <target state>

where the first source state is used as initial state.    

For example, the file [example.fsm](resources/example.fsm)

    0 0 2 2
    0 0 3 2
    0 1 0 3
    0 1 1 3
    1 0 3 2
    1 1 1 3
    2 0 2 2
    2 1 3 3
    3 0 2 2
    3 1 0 2
    3 1 1 1

represents the following finite state machine:

![graphical representation of example.fsm](resources/example_spec.png)



## 3.3. Using the Test Suite Generator Command Line Tool

After building it, the command line tool can be invoked by 

    cd test-suite-generator
    ./test-suite-generator <path to .fsm file> \
                           <test suite file name> \
                           <test strategy> \
                           <num of additional states> \
                           <input is already prime (0: false, 1: true)> \
                           <test suite type (0: input sequences, 1: IO pairs with flags)>

The following test strategies are supported, where the identifiers consist of a test strategy (W,Wp,HSI,H,SPY,SPYH), a framework (H,SPY,Pair), and in some cases additional completion flags, where 0 and 1 indicate true and false, respectively:

- W_H
- W_H_2
- W_SPY
- W_Pair
- Wp_H
- Wp_SPY
- HSI_H
- HSI_SPY
- HSI_Pair
- H_H_00
- H_H_10
- H_H_01
- H_H_11
- H_Pair
- H_Pair_2_0
- H_Pair_2_1
- H_Pair_3_00
- H_Pair_3_01
- H_Pair_3_10
- H_Pair_3_11
- SPY_H
- SPY_SPY
- SPYH_H_00
- SPYH_H_01
- SPYH_H_10
- SPYH_H_11
- SPYH_SPY_00
- SPYH_SPY_01
- SPYH_SPY_10
- SPYH_SPY_11
- PARTIAL_S_H_00
- PARTIAL_S_H_01
- PARTIAL_S_H_10
- PARTIAL_S_H_11

For example, using

    ./test-suite-generator ../../../resources/example.fsm testsuite.txt W_Pair 0 1 1

calculates a test suite for [example.fsm](resources/example.fsm) using the W-Method implemented in the Pair-Framework.
This produces a test suite which contains a number of test cases (sequences of IO-pairs augmented with boolean flags) such that each line contains a single test case, beginning as follows:

    ((0/0),F)
    ((0/1),F)
    ((0/2),T).((0/2),T)
    ((0/2),T).((0/3),F)
    ((0/2),T).((1/0),F)
    ((0/3),T).((0/0),F)
    ((0/3),T).((0/1),F)

Here, the fourth test case requires that an implementation conforming to [example.fsm](resources/example.fsm) w.r.t. language-equivalence should exhibit IO behaviour 0/2 but should not exhibit 00/23.

To apply the adaptive state counting strategy, see the Haskell-based test suite generator in folder [Tools/Haskell](Tools/Haskell).


# 4. Test Harness 

## 4.1. Building the Test Harness Command Line Tool

The command line tool is a Haskell Stack project and can be build as follows:

    cd Tools/test-harness
    stack build

For further installation options see the [Stack documentation](https://docs.haskellstack.org/en/stable/README/).

## 4.2. Using the Test Suite Generator Command Line Tool

After building it, the command line tool can be invoked by 

    cd test-harness
    stack exec test-harness -- <PARAMETERS>

The usage is as follows (this information can also be obtained using -h as a parameter above):

    Usage: test-harness TESTSUITE_PATH [-o|--output PATH] 
                                       [-n|--repetitions N] 
                                       [-r|--reduction] 
                                       (--IO | --IOB)
    Applies a test suite to an SUT (establishing an interface via sut_wrapper.c)
    and checks whether the SUT passes the test suite.

    Available options:
        TESTSUITE_PATH          path to a test suite
        -o,--output PATH        File into which the results are written (will be
                                cleared if it already exists).
                                (default: "results.txt")
        -n,--repetitions N      Number of times each line of the test suite is
                                applied to the SUT to try to observe all reactions.
                                (default: 100)
        -r,--reduction          If set, then the reduction conformance relation is
                                tested against (instead of language-equivalence).
        --IO                    Test suite is given as a list of IO traces (currently
                                only supported for reduction testing).
        --IOB                   Test suite is given as a list of IO traces with
                                containment flags (currently only supported for
                                equivalence testing).

The test harness interfaces with the system under test via an [SUT Wrapper](Tools/test-harness/src/sut_wrapper.c) as described in the [libfsmtest](https://bitbucket.org/JanPeleska/libfsmtest) library.
The given implementation tests as an example SUT the function `example_apply` of [sut_example.h](test-harness/src/sut_example.h) against the [example.fsm](resources/example.fsm) serving as a specification.

For example, the provided example [test suite](resources/testsuite.txt) (derived above using the W-Method) can be applied to this SUT as follows:

    stack exec test-harness -- ../../resources/testsuite.txt --IOB -n 100 -o test_application.log

This applies each test case (represented as a list of IO-pairs with boolean flags) 100 times.
A test case fails if the system under test exhibits a behaviour flagged with F in the test case or if a test case (or prefix thereof) flagged only with T is not exhibited. The overall application of the test suite passes if and only if no test case application fails. This process is documented in the file specified by `-o` above, which may begins as follows: 

    Applying              : ((0/0),F)
        observed reaction : (0/3)
            verdict       : PASS
    Applying              : ((0/0),F)
        observed reaction : (0/2)
            verdict       : PASS

Note that some test case applications may obtain the verdict INCONCLUSIVE, as the nondeterminism of the system under test may exhibit different behaviours for the same input, such as the following:

    Applying              : ((1/1),T).((1/0),T).((0/3),F)
        observed reaction : (1/1).(1/1)
            verdict       : INCONCLUSIVE (observed different response)

The result file ends with a verdict:

     verdict: PASS

If a fault is inserted into the [system under test](Tools/test-harness/src/sut_example.c) by redirecting the transitions from state 0 for input 1 to state 2 instead of the correct state 3 in line 27, then several errors are observed by applying the test harness again, including the following observation:

    Applying              : ((1/1),T).((1/3),F)
        observed reaction : (1/1).(1/3)
            verdict       : FAIL (observed response not in reference model)

The verdict then changes to FAIL:            

    verdict: FAIL (observed response not in reference model) -- see test case: ((1/1),T).((1/3),F)     



## 4.3. Using the Test Harness to Test Different SUTs

The following steps are required to adapt the test harness to a different SUT:

- The file [sut_wrapper.c](Tools/test-harness/src/sut_wrapper.c), which serves as a wrapper around the SUT and is accessed in the test-harness, must be updated. To do so, the following three functions must be implemented:

    * `void sut_init()` - This function is called once by the test harness before any other interaction with the SUT. It must set up any resources required by the SUT and initialise the SUT itself.
    * `void sut_reset()` - This function is called once before each test case is applied to the SUT. It must reset the SUT to its initial state.
    * `int sut(int input)` - This function is called each time an input is to be applied to the SUT and must apply this input to the SUT and return the output produced by the SUT. In doing so, this function must also translate inputs and outputs from the integers used in the .fsm files to the actual inputs or outputs used by the SUT, if these are not identical to those specified in the .fsm file.
    
- The file [package.yaml](Tools/test-harness/package.yaml) must be updated to include any new C files (see the `c-sources` parameter). 

# 5. Evaluation

The [Evaluation](evaluation) folder contains data sets on which the implementations generated from the formalisation have been compared to each other and to an implementation in a C++ library.

## 5.1 Data Sets

The archive [data_sets](Evaluation/data_sets.zip) contains 8 sets of randomly generated FSMs, using the following naming scheme

    random_<t>FSMs_complete_<i>_<o>

where t is either O or D, indicating OFSMs and DFSMs, respectively, and parameters i and o denote the number of inputs and outputs. 
Each of these folders contains 10.000 FSMs in the raw format discussed above, such that for each number of states from 1 to 100 there exist 100 FSMs, following the naming scheme

    n<n>i<i>o<o>r<r>

where the parameters from left to right are the number of states, inputs, outputs, and the number of the FSM for the preceding parameters (ranging from 1 to 100).    

## 5.2 Evaluator

The collection of data on the performance of test strategy implementations generated from the formalisation has been performed using a small command line tool [evaluator](Evaluation/tools/evaluator), which closely resembles the test-suite-generator discussed above.
It is written in SML and may again be compiled using [MLton](mlton.org):

    cd Evaluation/tools/evaluator
    mlton evaluator.mlb

Running this tool applies the test-suite-generator for a given strategy (using the same identifiers as the test-suite-generator) to each file in a given directory:

    ./evaluator <dir containing only .fsm files> 
                <test strategy> 
                <num of additional states> 
                <input is already prime (0:false, 1:true)>
                <number of repetitions per size>
                <initial group count>

For example, after extracting the [data sets](Evaluation/data_sets.zip) into a folder of the same name in directory [Evaluation](Evaluation), the evaluator may be applied as follows:

    ./evaluator ../../data_sets/random_DFSMs_complete_2_2 W_Pair 0 1 100 1

This applies the W-Method (as implemented in the Pair-Framework) to all FSMs in random_DFSMs_complete_2_2, using 0 additional states and assuming that all FSMs therein are prime and grouped into groups of 100 FSMs for each number of states, beginning at 1.
This call generates a results-file detailing the observations for each individual FSM; containing lines such as the following:

    n0014i02o02r0048.fsm,W_Pair,0,5,60,391

where the columns from left to right show the name of the FSM, the applied strategy, the number of additional states, the time (in ms) required to compute the test suite, as well as its size (number of maximal input sequences) and length (sum of the length of all test cases).

These individual results are aggregated in a stats-file, which contains lines such as the following:

    14;3;57;401

which show that for FSMs with 14 states, the average computation time was 3 ms and the test suites had average size 57 and average length 401.

## 5.3 Comparison Between Generated Implementations

Folder [comparison_of_strategies](Evaluation/results/comparison_of_strategies) contains the stats-files for a selection of strategies supported by the test-suite-generator, applied to data set random_DFSMs_complete_2_2 for 0 additional states. The following behaviour has been observed:

![Observed average test suite sizes for selected strategies](resources/strategy_comparison.png)

## 5.4 Comparison with a Manually Developed Implementation

Folder [comparison_of_H_method_implementations](Evaluation/results/comparison_of_H_method_implementations) contains the stats-files comparing two implementations of the H-Method (H_Pair, H_Pair_3_11) generated from the formalisation with an implementation of the H-Method available in the manually written, open source C++ library [fsmlib-cpp](https://github.com/agbs-uni-bremen/fsmlib-cpp). The comparison has been performed over data set random_DFSMs_complete_3_3 for up to 80 states and up to 2 additional states, observing both the sizes of the generated test suites and the average time required for their computation. The following behaviour has been observed:

![Comparison with the fsmlib-cpp](resources/implementation_comparison.png)

# 6. References 

1. T. S. Chow. Testing software design modeled by finite-state machines. IEEE Transactions on Software Engineering, 4(3):178–187, 1978.
2. G. Luo, G. von Bochmann, and A. Petrenko. Test selection based on communicating nondeterministic finite-state machines using a generalized wp-method. IEEE Trans. Software Eng., 20(2):149–162, 1994.
3. G. Luo, A. Petrenko, and G. von Bochmann. Selecting test sequences for partially-specified nondeterministic finite state machines. In T. Mizuno, T. Higashino, and N. Shiratori, editors, Protocol Test Systems: 7th workshop 7th IFIP WG 6.1 international workshop on protocol text systems, pages 95–110. Springer US, Boston, MA, 1995.
4. R. Dorofeeva, K. El-Fakih, and N. Yevtushenko. An improved conformance testing method. In F. Wang, editor, Formal Techniques for Networked and Distributed Systems - FORTE 2005, 25th IFIP WG 6.1 International Conference, Taipei, Taiwan, October 2-5, 2005, Proceedings, volume 3731 of Lecture Notes in Computer Science, pages 204–218. Springer, 2005.
5. A. da Silva Simão, A. Petrenko, and N. Yevtushenko. On reducing test length for fsms with extra states. Software Testing, Verification and Reliability, 22(6):435–454, 2012.
6. M. Soucha and K. Bogdanov. Spyh-method: An improvement in testing of finite-state machines. In 2018 IEEE International Conference on Software Testing, Verification and Validation Workshops, ICST Workshops, Västerås, Sweden, April 9-13, 2018, pages 194–203. IEEE Computer Society, 2018.
7. A. Petrenko and N. Yevtushenko. Adaptive testing of nondeterministic systems with FSM. In 15th International IEEE Symposium on High-Assurance Systems Engineering, HASE 2014, Miami Beach, FL, USA, January 9-11, 2014, pages 224–228. IEEE Computer Society, 2014.

