(* args: <dir containing only fsms> 
         <test strategy> 
         <num of additional states> 
         <input is already prime (0:false, 1:true)>
         <number of repetitions per size>
         <initial group count>
*)
val args = CommandLine.arguments()
val fsmDir = List.nth (args,0)
val stratName = List.nth (args,1)
val additionalStatesString = (List.nth (args,2))
val additionalStates = Int.toLarge (valOf (Int.fromString additionalStatesString))  
val isAlreadyPrime = ((valOf (Int.fromString (List.nth (args,3)))) = 1)
val testStrategy = 
     if      stratName = "W_H" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "W_H_2" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_2_input fsm addStates isAlreadyPrime))
     else if stratName = "W_SPY" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "W_Pair" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "Wp_H" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "Wp_SPY" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_H" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_SPY" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_Pair" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "H_H_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "H_H_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "H_H_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "H_H_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "H_Pair" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "H_Pair_2_0" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_input fsm addStates isAlreadyPrime false))
     else if stratName = "H_Pair_2_1" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_input fsm addStates isAlreadyPrime true))
     else if stratName = "H_Pair_3_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime false false))
     else if stratName = "H_Pair_3_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime false true))
     else if stratName = "H_Pair_3_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime true false))
     else if stratName = "H_Pair_3_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime true true))
     else if stratName = "SPY_H" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "SPY_SPY" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "SPYH_H_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_H_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_H_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_H_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "SPYH_SPY_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_SPY_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_SPY_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_SPY_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "PARTIAL_S_H_00" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "PARTIAL_S_H_01" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "PARTIAL_S_H_10" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "PARTIAL_S_H_11" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else (fn fsm => (fn addStates => []))

val fsmsPerSize = Int.toLarge (valOf (Int.fromString (List.nth (args,4)))) 
val initialGroup = Int.toLarge (valOf (Int.fromString (List.nth (args,5)))) 

val dirString = String.map (fn x => if x = #"." orelse x = #"/" then #"_" else x) fsmDir

val resultFileName = "results___" ^ dirString ^ "___" ^ stratName ^ "_a" ^ additionalStatesString ^ ".log"
val statFileName = "stats___" ^ dirString ^ "___" ^ stratName ^ "_a" ^ additionalStatesString ^ ".log"

fun testMethod fsm = testStrategy fsm additionalStates 

fun readInts(file : string) = let
    val ins = TextIO.openIn file
    fun loop ins =
        case TextIO.scanStream( Int.scan StringCvt.DEC) ins of
          SOME int => int :: loop ins
          | NONE => []
    in
      loop ins before TextIO.closeIn ins
  end;

fun readTransitionList [] = [] 
  | readTransitionList (q1 :: x :: y :: q2 :: ts) = (Int.toLarge q1,(Int.toLarge x,(Int.toLarge y,Int.toLarge q2))) :: (readTransitionList ts)  

fun readFSM file = GeneratedCode.fsm_from_list_integer (Int.toLarge 0) (readTransitionList (readInts file))

fun testFSM file = let 
    val fsm = readFSM file
    val t0 = Timer.startCPUTimer ()
    val TS = testMethod (readFSM file)
    val t1 = Timer.checkCPUTimer t0
    val tsSize = length TS
    val tsTime = Time.toMilliseconds (Time.+(#usr t1, #sys t1))
    val tsLength = foldl IntInf.+ 0 (map Int.toLarge (map length TS))
  in   
    ( file ^ "," ^ stratName ^ "," ^ additionalStatesString ^ "," ^ IntInf.toString tsTime ^ "," ^ Int.toString tsSize ^ "," ^ IntInf.toString tsLength ^ "\n"
    , tsSize, tsTime, tsLength)
  end;


fun testFSMsInDir (dir : string) = let
  val dirStream = Posix.FileSys.opendir dir
  val resFile = TextIO.openAppend resultFileName
  val statFile = TextIO.openAppend statFileName
  val _ = Posix.FileSys.chdir dir
  fun sortStrings ss = ListMergeSort.sort (fn (s : string, t) => s > t) ss;
  fun collectFiles ds fileNames = 
    case Posix.FileSys.readdir ds of
      SOME fileName => collectFiles ds (fileName::fileNames)
      | NONE => fileNames
  val fileNames = sortStrings (collectFiles dirStream nil)
  val fileNumber = length fileNames
  fun loop fileNames (groupCount : IntInf.int) (fsmCount : IntInf.int) (sizeSum : IntInf.int) (timeSum : IntInf.int) (lengthSum : IntInf.int) = 
    case fileNames of
      fileName::fileNames' => 
        if groupCount < initialGroup
        then if fsmCount = fsmsPerSize
          then loop fileNames' (groupCount+1) (Int.toLarge 1) (Int.toLarge 0) (Int.toLarge 0) (Int.toLarge 0)
          else loop fileNames' groupCount (fsmCount+1) sizeSum timeSum lengthSum
        else let 
          val (resString,s,t,l) = testFSM fileName
          val _ = TextIO.output(resFile,resString)
          val _ = TextIO.flushOut(resFile)
        in if fsmCount = fsmsPerSize
            then let val avgSize = (IntInf.toString (IntInf.div ((sizeSum + Int.toLarge s), fsmCount)))
                     val avgTime = (IntInf.toString (IntInf.div ((timeSum + t), fsmCount)))
                     val avgLength = (IntInf.toString (IntInf.div ((lengthSum + l), fsmCount)))
                     val statString = IntInf.toString groupCount ^ ";" ^ avgTime ^ ";" ^ avgSize ^ ";" ^ avgLength ^ "\n"
                     val _ = TextIO.output(statFile,statString)
                     val _ = TextIO.flushOut(statFile)  
                 in loop fileNames' (groupCount+1) (Int.toLarge 1) (Int.toLarge 0) (Int.toLarge 0) (Int.toLarge 0) end

            else loop fileNames' groupCount (fsmCount+1) (sizeSum + Int.toLarge s) (timeSum + t) (lengthSum + l)
        end
      | [] => ()
  in 
    loop fileNames (Int.toLarge 1) (Int.toLarge 1) (Int.toLarge 0) (Int.toLarge 0) (Int.toLarge 0)
  end;

val _ = testFSMsInDir fsmDir
val _ = OS.Process.exit(OS.Process.success)