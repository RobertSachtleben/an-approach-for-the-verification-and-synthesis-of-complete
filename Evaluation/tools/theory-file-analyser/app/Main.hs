module Main where

import Options.Applicative
import System.Environment
import System.Exit

import System.FilePath
import System.Directory
import System.IO
import Control.Monad
import System.Timeout

import Data.Set(Set)
import qualified Data.Set as Set
import Data.List
import Data.Char
import Data.List.Split
import Text.PrettyPrint.ANSI.Leijen(string)



data ProofRule = NONE | DASH | INDUCTION | CASES | CCONTR deriving (Show,Eq,Ord)
type ProofMethod = String
data ProofType = Proof ProofRule | By ProofMethod (Set ProofRule) deriving (Eq,Ord)
data ProofAspects = OF | USING | UNFOLDING deriving (Show,Eq,Ord)

-- <Name> <Simple or Isar> <Features between "lemma" and "proof"/"by"> <max depth>
data Lemma = Lemma { name :: String
                   , proofType :: ProofType
                   , proofAspects :: Set ProofAspects
                   , maxDepth :: Int 
                   } 

instance Show ProofType where 
    show (Proof pr) = "'proof' (" ++ show pr ++ ")"
    show (By pm prs) = "'by' (" ++ pm ++ ", " ++ show (Set.toList prs) ++ ")"

instance Show Lemma where 
    show (Lemma name pt pas d) = "lemma " ++ name ++ " proven via " ++ show pt ++ " using " ++ show (Set.toList pas) ++ " with max depth: " ++ show d     

trim = dropWhileEnd isSpace . dropWhile isSpace    
getFirstWord = takeWhile isAlpha . dropWhile (not . isAlpha)

extractProofType :: String -> ProofType 
extractProofType "." = By "." Set.empty 
extractProofType l = if isPrefixOf "by" lt
    then extractSimpleProofProps $ trim $ drop 2 lt
    else if isPrefixOf "proof" lt 
        then extractComplexProofProps $ trim $ drop 5 lt
        else error $ "extractProofType: unable to handle: " ++ l
    where    
        lt = trim l
        extractComplexProofProps l' 
            | null (trim l') = Proof NONE 
            | isPrefixOf "-" (trim l') = Proof DASH
            | isPrefixOf "induct" (getFirstWord l') = Proof INDUCTION 
            | isPrefixOf "case" (getFirstWord l') = Proof CASES 
            | isPrefixOf "rule ccontr" (dropWhile (not . isAlpha) l') = Proof CCONTR 
            | otherwise = error $ "extractComplexProofProps: unable to handle: " ++ l

        extractSimpleProofProps l' = By pm prs
            where 
                steps = splitOn ";" l'
                pm = if elem ';' l' then ";" else getFirstWord l'   -- ";" indicates comples "by" proofs                
                prsInduct = if any (isPrefixOf "induct" . getFirstWord) steps then Set.singleton INDUCTION else Set.empty
                prsCases = if any (isPrefixOf "case" . getFirstWord) steps then Set.singleton CASES else Set.empty
                prs = Set.union prsInduct prsCases
            
extractProofAspects :: [String] -> Set ProofAspects
extractProofAspects [] = Set.empty
extractProofAspects (l:ls) = Set.union aspects $ extractProofAspects ls 
    where 
        a1 = if isInfixOf "OF " l then Set.singleton OF else Set.empty
        a2 = if isInfixOf "unfolding " l then Set.singleton UNFOLDING else Set.empty
        a3 = if isInfixOf "using " l then Set.singleton USING else Set.empty
        aspects = Set.union a1 $ Set.union a2 a3 

parseLemma :: [String] -> (Lemma,[String])  
parseLemma ls = if null afterHeaderBody
    then error $ "parseLemma: lemma " ++ name ++ " has no 'by' or 'proof'"
    else case proofType of 
        By _ _  -> (Lemma name proofType proofAspects 0, tail afterHeaderBody)
        Proof _ -> if null afterProofBody
            then error $ "parseLemma: proof of lemma " ++ name ++ " has no 'qed'"
            else (Lemma name proofType proofAspects proofDepth, tail afterProofBody)
    where 
        
        name = takeWhile (not . (flip elem) ['[',':']) $ head $ words $ drop 5 $ head ls
        terminatesLemmaHeader l = elem (getFirstWord l) ["by","proof"] || trim l == "." || isInfixOf " by " l || isInfixOf " proof" l
        
        afterHeaderBody = dropWhile (not . terminatesLemmaHeader) $ ls
        headerOrProofLine = head afterHeaderBody

        extractProofLineAndLastHeaderLine l = if elem (getFirstWord l) ["by","proof"] || trim l == "."
                        then ([],trim l)
                        else if isInfixOf " by " l 
                            then let [h,p] = splitOn " by " l in (h, "by "++p) 
                            else let [h,p] = splitOn " proof" l in (h, "proof"++p) 
        (h,p) = extractProofLineAndLastHeaderLine headerOrProofLine

        headerBody' = takeWhile (not . terminatesLemmaHeader) $ tail ls
        headerBody = if null h then headerBody' else headerBody' ++ [h]
        proofLine = p
        proofAspects = extractProofAspects headerBody        
        proofType = extractProofType p
        proofBody = takeWhile (not . isPrefixOf "qed") $ tail afterHeaderBody
        afterProofBody = dropWhile (not . isPrefixOf "qed") $ tail afterHeaderBody
        
        getMaxDepth n m [] = m
        getMaxDepth n m (l:ls) = if isInfixOf " qed" l then getMaxDepth (n-1) m ls
                               else if isInfixOf " proof" l then getMaxDepth (n+1) (if n+1>m then n+1 else m) ls
                               else getMaxDepth n m ls
        proofDepth = getMaxDepth 0 0 proofBody
        


getNextLemma :: [String] -> Maybe (Lemma,[String])
getNextLemma [] = Nothing
getNextLemma (l:ls) = if isPrefixOf "lemma" l
    then Just $ parseLemma (l:ls)
    else getNextLemma ls    
     


analyseLemmata :: [String] -> [Lemma]
analyseLemmata theoryContents = case getNextLemma theoryContents of Just (l,ls) -> l : analyseLemmata ls  
                                                                    Nothing -> [] 




data Opts = Opts { theoryFileDir :: String
                 , resultFileName :: String }

getOpts :: Parser Opts
getOpts = Opts 
            <$> strOption
                ( short 't'
                    <> long "theory-file-directory"
                    <> metavar "THEORY-FILE-DIRECTORY"
                    <> help "path to a directory containing .thy files" )
            <*> strOption
                ( short 'r'
                    <> long "result-file-name"
                    <> metavar "RESULT-FILE-NAME"
                    <> help "name of the result file - will also be used as prefix (excluding extensions) for the result (.log) files of individual theory files" 
                    <> showDefault
                    <> value "results" )

main :: IO ()
main = performAnalysis =<< execParser opts
  where
    opts = info (getOpts <**> helper)
      ( fullDesc
        <> progDescDoc ( Just . string $  "Analyses .thy files")
        <> header "Theory File Analyser" )

performAnalysis :: Opts -> IO ()
performAnalysis opts = do
    let tpDir = theoryFileDir opts 
    let rn = resultFileName opts
    isDir <- doesDirectoryExist tpDir
    if isDir 
        then do
            --setCurrentDirectory tpDir               
            files <- listDirectory tpDir 
            let tps = map ((++) (tpDir ++ "/")) $ reverse $ filter (\file -> takeExtension file == ".thy") files
            putStrLn $ show tps
            lemmataLists <- mapM (analyseFile rn) tps
            collectStats rn $ zip (map takeFileName tps) lemmataLists
        else error $ tpDir ++ " is not a directory"
            
analyseFile :: String -> String -> IO [Lemma]
analyseFile rn tp = do 
    isFile <- doesFileExist tp
    if isFile && takeExtension tp == ".thy"
        then do
            theoryContents <- readFile tp
            let lemmata = analyseLemmata $ lines theoryContents
            writeLemmata lemmata tp rn
            return lemmata 
        else error $ "Error: " ++ tp  ++ " is not a .thy file or does not exist."
    
writeLemmata :: [Lemma] -> String -> String -> IO ()
writeLemmata lemmata theoryFilePath resultFileName = 
    let filePath = dropExtension resultFileName ++ "_" ++ takeBaseName theoryFilePath ++ ".log"
    in withFile filePath WriteMode $ \file -> do mapM_ ((hPutStrLn file) . show) lemmata    

collectStats :: String -> [(String,[Lemma])] -> IO ()
collectStats resultFileName tls =  
    let filePath = dropExtension resultFileName ++ ".csv"
    in withFile filePath WriteMode $ \file -> do 
        hPutStrLn file "file;maxD;avgDofNested;lemmata;by;bySimplest;bySimplestMetis;byComplex;proof;proofInduction;proofCcontr;proofNoNesting;proofNesting"
        mapM_ (writeLemmataStats file) tls
        writeLemmataStats file ("",concatMap snd tls) -- include last line that provides overall stats

writeLemmataStats :: Handle -> (String,[Lemma]) -> IO ()
writeLemmataStats file (theoryFileName,lemmata) = 
    hPutStrLn file $ intercalate ";" $ [theoryFileName,show maxD, show avgD] ++ map (show . length) [lemmata,byLemmata,byLemmataWithSimplestPM,byLemmataWithMetis,byLemmataWithComplexPM,proofLemmata,proofLemmataInduction,proofLemmataCcontr,proofLemmataWithoutNesting,proofLemmataWithNesting]
    where
        maxD = if null lemmata then 0 else maximum $ map maxDepth lemmata
        
        byLemmata = filter (\l -> case proofType l of (By _ _ ) -> True; _ -> False) lemmata   
        byLemmataWithSimplestPM = filter (\l -> case proofType l of (By pm pas) -> pm /= ";" && pas == Set.empty) byLemmata
        byLemmataWithComplexPM = filter (\l -> case proofType l of (By pm pas) -> pm == ";" || pas /= Set.empty) byLemmata
        byLemmataWithMetis = filter (\l -> case proofType l of (By pm _) -> pm == "metis") byLemmata
        
        proofLemmata = filter (\l -> case proofType l of (Proof _) -> True; _ -> False) lemmata  
        proofLemmataInduction = filter (\l -> case proofType l of (Proof INDUCTION) -> True; _ -> False) lemmata 
        proofLemmataCcontr = filter (\l -> case proofType l of (Proof CCONTR) -> True; _ -> False) lemmata 
        proofLemmataWithoutNesting = filter ((==) 0 . maxDepth) proofLemmata
        proofLemmataWithNesting = filter ((/=) 0 . maxDepth) proofLemmata

        avgD = if null proofLemmataWithNesting then 0.0 else (fromIntegral $ sum $ map maxDepth proofLemmataWithNesting) / (fromIntegral $ length proofLemmataWithNesting)
