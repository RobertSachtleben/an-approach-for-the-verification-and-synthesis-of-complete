(* args: <path to .fsm file> 
         <test suite file name> 
         <test strategy> 
         <num of additional states> 
         <input is already prime (0: false, 1: true)> 
         <test suite type (0: input sequences, 1: IO pairs with flags)> *)
val args = CommandLine.arguments()
val fsmFileName = List.nth (args,0)
val testSuiteFileName = List.nth (args,1)
val stratName = List.nth (args,2)
val additionalStatesString = (List.nth (args,3))
val additionalStates = Int.toLarge (valOf (Int.fromString additionalStatesString))  
val isAlreadyPrime = ((valOf (Int.fromString (List.nth (args,4)))) = 1)
val isTypeB = ((valOf (Int.fromString (List.nth (args,5)))) = 1)
val testStrategy = 
     if      stratName = "W_H" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "W_H_2" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_2_input fsm addStates isAlreadyPrime))
     else if stratName = "W_SPY" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "W_Pair" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "Wp_H" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "Wp_SPY" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_H" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_SPY" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "HSI_Pair" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "H_H_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "H_H_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "H_H_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "H_H_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "H_Pair" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "H_Pair_2_0" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_input fsm addStates isAlreadyPrime false))
     else if stratName = "H_Pair_2_1" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_input fsm addStates isAlreadyPrime true))
     else if stratName = "H_Pair_3_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime false false))
     else if stratName = "H_Pair_3_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime false true))
     else if stratName = "H_Pair_3_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime true false))
     else if stratName = "H_Pair_3_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_input fsm addStates isAlreadyPrime true true))
     else if stratName = "SPY_H" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_h_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "SPY_SPY" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_spy_framework_input fsm addStates isAlreadyPrime))
     else if stratName = "SPYH_H_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_H_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_H_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_H_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "SPYH_SPY_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_SPY_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_SPY_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_SPY_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_input fsm addStates isAlreadyPrime true true))
     else if stratName = "PARTIAL_S_H_00" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime false false))
     else if stratName = "PARTIAL_S_H_01" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime false true))
     else if stratName = "PARTIAL_S_H_10" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime true false))
     else if stratName = "PARTIAL_S_H_11" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_input fsm addStates isAlreadyPrime true true))
     else (fn fsm => (fn addStates => []))

val testStrategyB = 
     if      stratName = "W_H" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "W_H_2" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_h_framework_2_ts fsm addStates isAlreadyPrime))
     else if stratName = "W_SPY" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_spy_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "W_Pair" then (fn fsm => (fn addStates => GeneratedCode.w_method_via_pair_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "Wp_H" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_h_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "Wp_SPY" then (fn fsm => (fn addStates => GeneratedCode.wp_method_via_spy_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "HSI_H" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_h_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "HSI_SPY" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_spy_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "HSI_Pair" then (fn fsm => (fn addStates => GeneratedCode.hsi_method_via_pair_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "H_H_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_ts fsm addStates isAlreadyPrime false false))
     else if stratName = "H_H_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_ts fsm addStates isAlreadyPrime false true))
     else if stratName = "H_H_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_ts fsm addStates isAlreadyPrime true false))
     else if stratName = "H_H_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_h_framework_ts fsm addStates isAlreadyPrime true true))
     else if stratName = "H_Pair" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "H_Pair_2_0" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_ts fsm addStates isAlreadyPrime false))
     else if stratName = "H_Pair_2_1" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_2_ts fsm addStates isAlreadyPrime true))
     else if stratName = "H_Pair_3_00" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_ts fsm addStates isAlreadyPrime false false))
     else if stratName = "H_Pair_3_01" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_ts fsm addStates isAlreadyPrime false true))
     else if stratName = "H_Pair_3_10" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_ts fsm addStates isAlreadyPrime true false))
     else if stratName = "H_Pair_3_11" then (fn fsm => (fn addStates => GeneratedCode.h_method_via_pair_framework_3_ts fsm addStates isAlreadyPrime true true))
     else if stratName = "SPY_H" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_h_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "SPY_SPY" then (fn fsm => (fn addStates => GeneratedCode.spy_method_via_spy_framework_ts fsm addStates isAlreadyPrime))
     else if stratName = "SPYH_H_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_ts fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_H_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_ts fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_H_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_ts fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_H_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_h_framework_ts fsm addStates isAlreadyPrime true true))
     else if stratName = "SPYH_SPY_00" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_ts fsm addStates isAlreadyPrime false false))
     else if stratName = "SPYH_SPY_01" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_ts fsm addStates isAlreadyPrime false true))
     else if stratName = "SPYH_SPY_10" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_ts fsm addStates isAlreadyPrime true false))
     else if stratName = "SPYH_SPY_11" then (fn fsm => (fn addStates => GeneratedCode.spyh_method_via_spy_framework_ts fsm addStates isAlreadyPrime true true))
     else if stratName = "PARTIAL_S_H_00" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_ts fsm addStates isAlreadyPrime false false))
     else if stratName = "PARTIAL_S_H_01" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_ts fsm addStates isAlreadyPrime false true))
     else if stratName = "PARTIAL_S_H_10" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_ts fsm addStates isAlreadyPrime true false))
     else if stratName = "PARTIAL_S_H_11" then (fn fsm => (fn addStates => GeneratedCode.partial_s_method_via_h_framework_ts fsm addStates isAlreadyPrime true true))
     else (fn fsm => (fn addStates => []))

fun testMethod fsm = testStrategy fsm additionalStates 
fun testMethodB fsm = testStrategyB fsm additionalStates 

fun readInts(file : string) = let
    val ins = TextIO.openIn file
    fun loop ins =
        case TextIO.scanStream( Int.scan StringCvt.DEC) ins of
          SOME int => int :: loop ins
          | NONE => []
    in
      loop ins before TextIO.closeIn ins
  end;

fun readTransitionList [] = [] 
  | readTransitionList (q1 :: x :: y :: q2 :: ts) = (Int.toLarge q1,(Int.toLarge x,(Int.toLarge y,Int.toLarge q2))) :: (readTransitionList ts)  

fun readFSM file = GeneratedCode.fsm_from_list_integer (Int.toLarge 0) (readTransitionList (readInts file))


fun showTriple (xyb : (IntInf.int * IntInf.int) * bool) = "((" ^ IntInf.toString (#1 (#1 xyb)) ^ "/" ^ IntInf.toString (#2 (#1 xyb)) ^ ")," ^ (if (#2 xyb) then "T" else "F") ^ ")";
fun showTestCase tc = String.concatWith "," (map IntInf.toString tc);
fun showTestCaseB tc = String.concatWith "." (map showTriple tc);

fun writeTestSuite ts = let 
      val fd = TextIO.openOut testSuiteFileName
      val _ = map ( fn tc =>  TextIO.output (fd, showTestCase tc ^ "\n")) ts
      val _ = TextIO.closeOut fd
    in () end

fun writeTestSuiteB ts = let 
      val fd = TextIO.openOut testSuiteFileName
      val _ = map ( fn tc =>  TextIO.output (fd, showTestCaseB tc ^ "\n")) ts
      val _ = TextIO.closeOut fd
    in () end    

fun testFSM file = 
  if isTypeB then let 
      val fsm = readFSM file
      val TS = testMethodB fsm
      val tsSize = length TS
      val _ = writeTestSuiteB TS
    in   
      print("test cases  : " ^ Int.toString tsSize ^ "\ntotal length: " ^ (Int.toString (foldl Int.+ 0 (map length TS))) ^ "\n")
    end
  else let 
      val fsm = readFSM file
      val TS = testMethod fsm
      val tsSize = length TS
      val _ = writeTestSuite TS
    in   
      print("test cases  : " ^ Int.toString tsSize ^ "\ntotal length: " ^ (Int.toString (foldl Int.+ 0 (map length TS))) ^ "\n")
    end


val _ = testFSM fsmFileName
val _ = OS.Process.exit(OS.Process.success)