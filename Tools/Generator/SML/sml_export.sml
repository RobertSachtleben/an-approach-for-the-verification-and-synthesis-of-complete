structure GeneratedCode : sig
  type 'a ccompare
  type 'a ceq
  type 'a set
  type 'a cenum
  type 'a equal
  type ('a, 'b, 'c) fsm_with_precomputations_impl
  type ('c, 'b, 'a) fsm_with_precomputations
  type ('a, 'b, 'c) fsm_impl
  type ('c, 'b, 'a) fsm
  type 'a set_impl
  type nat
  val integer_of_nat : nat -> IntInf.int
  type 'a linorder
  type 'a finite_UNIV
  type 'a card_UNIV
  type 'a mapping_impl
  type 'a cproper_interval
  type 'a fset
  type 'a infinite_UNIV
  datatype ('a, 'b) sum = Inl of 'a | Inr of 'b
  type 'a prefix_tree
  val size : 'a card_UNIV * 'a ceq * 'a ccompare -> ('a, 'b, 'c) fsm -> nat
  val rename_statesb :
    'd ceq * 'd ccompare ->
      'b ceq * 'b ccompare * 'b equal * 'b mapping_impl * 'b set_impl ->
      'c ceq * 'c ccompare * 'c equal * 'c mapping_impl * 'c set_impl ->
      'a card_UNIV * 'a ceq * 'a ccompare * 'a equal * 'a mapping_impl *
        'a set_impl ->
      ('d, 'b, 'c) fsm_with_precomputations ->
        ('d -> 'a) -> ('a, 'b, 'c) fsm_with_precomputations
  val index_states :
    'a ceq * 'a ccompare * 'a equal * 'a linorder ->
      'b ceq * 'b ccompare * 'b equal * 'b mapping_impl * 'b set_impl ->
      'c ceq * 'c ccompare * 'c equal * 'c mapping_impl * 'c set_impl ->
      ('a, 'b, 'c) fsm -> (nat, 'b, 'c) fsm
  val fsm_from_list :
    'a card_UNIV * 'a ceq * 'a ccompare * 'a equal * 'a mapping_impl *
      'a set_impl ->
      'b ceq * 'b ccompare * 'b equal * 'b mapping_impl * 'b set_impl ->
      'c ceq * 'c ccompare * 'c equal * 'c mapping_impl * 'c set_impl ->
      'a -> ('a * ('b * ('c * 'a))) list -> ('a, 'b, 'c) fsm
  val make_observable :
    'a card_UNIV * 'a cenum * 'a ceq * 'a ccompare * 'a equal * 'a linorder *
      'a set_impl * 'a infinite_UNIV ->
      'b finite_UNIV * 'b cenum * 'b ceq * 'b cproper_interval * 'b equal *
        'b mapping_impl * 'b linorder * 'b set_impl ->
      'c finite_UNIV * 'c cenum * 'c ceq * 'c cproper_interval * 'c equal *
        'c mapping_impl * 'c linorder * 'c set_impl ->
      ('a, 'b, 'c) fsm -> ('a fset, 'b, 'c) fsm
  val restrict_to_reachable_states :
    'a card_UNIV * 'a ceq * 'a ccompare * 'a equal * 'a linorder *
      'a set_impl ->
      'b ceq * 'b ccompare * 'b equal * 'b linorder ->
      'c ceq * 'c ccompare * 'c equal * 'c mapping_impl * 'c linorder ->
      ('a, 'b, 'c) fsm -> ('a, 'b, 'c) fsm
  val to_prime :
    'a card_UNIV * 'a cenum * 'a ceq * 'a ccompare * 'a equal * 'a linorder *
      'a set_impl * 'a infinite_UNIV ->
      'b finite_UNIV * 'b cenum * 'b ceq * 'b cproper_interval * 'b equal *
        'b mapping_impl * 'b linorder * 'b set_impl ->
      'c finite_UNIV * 'c cenum * 'c ceq * 'c cproper_interval * 'c equal *
        'c mapping_impl * 'c linorder * 'c set_impl ->
      ('a, 'b, 'c) fsm -> (IntInf.int, 'b, 'c) fsm
  val fsm_from_list_integer :
    IntInf.int ->
      (IntInf.int * (IntInf.int * (IntInf.int * IntInf.int))) list ->
        (IntInf.int, IntInf.int, IntInf.int) fsm
  val h_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val w_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val wp_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val hsi_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val spy_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val w_method_via_h_framework_2_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val w_method_via_spy_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val h_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> bool -> (IntInf.int list) list
  val h_method_via_pair_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val spyh_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val w_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val w_method_via_pair_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val wp_method_via_spy_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val hsi_method_via_spy_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val spy_method_via_spy_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val wp_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val h_method_via_pair_framework_2_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val h_method_via_pair_framework_3_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val hsi_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val hsi_method_via_pair_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val spy_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val spyh_method_via_spy_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val w_method_via_h_framework_2_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val w_method_via_spy_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val h_method_via_pair_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val spyh_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> bool -> (IntInf.int list) list
  val w_method_via_pair_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val wp_method_via_spy_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val hsi_method_via_spy_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val spy_method_via_spy_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val generate_reduction_test_suite_naive :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> (string, (((IntInf.int * IntInf.int) list) list)) sum
  val h_method_via_pair_framework_2_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> (IntInf.int list) list
  val h_method_via_pair_framework_3_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> bool -> (IntInf.int list) list
  val hsi_method_via_pair_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> (IntInf.int list) list
  val partial_s_method_via_h_framework_ts :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int ->
        bool -> bool -> bool -> (((IntInf.int * IntInf.int) * bool) list) list
  val spyh_method_via_spy_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> bool -> (IntInf.int list) list
  val generate_reduction_test_suite_greedy :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> (string, (((IntInf.int * IntInf.int) list) list)) sum
  val partial_s_method_via_h_framework_input :
    (IntInf.int, IntInf.int, IntInf.int) fsm ->
      IntInf.int -> bool -> bool -> bool -> (IntInf.int list) list
end = struct

datatype ordera = Eq | Lt | Gt;

type 'a ccompare = {ccompare : ('a -> 'a -> ordera) option};
val ccompare = #ccompare : 'a ccompare -> ('a -> 'a -> ordera) option;

fun equal_order Lt Gt = false
  | equal_order Gt Lt = false
  | equal_order Eq Gt = false
  | equal_order Gt Eq = false
  | equal_order Eq Lt = false
  | equal_order Lt Eq = false
  | equal_order Gt Gt = true
  | equal_order Lt Lt = true
  | equal_order Eq Eq = true;

datatype ('a, 'b) generator = Generator of (('b -> bool) * ('b -> 'a * 'b));

fun generator (Generator x) = x;

fun fst (x1, x2) = x1;

fun has_next g = fst (generator g);

fun snd (x1, x2) = x2;

fun next g = snd (generator g);

fun sorted_list_subset_fusion less eq g1 g2 s1 s2 =
  (if has_next g1 s1
    then let
           val (x, s1a) = next g1 s1;
         in
           has_next g2 s2 andalso
             let
               val (y, s2a) = next g2 s2;
             in
               (if eq x y then sorted_list_subset_fusion less eq g1 g2 s1a s2a
                 else less y x andalso
                        sorted_list_subset_fusion less eq g1 g2 s1 s2a)
             end
         end
    else true);

fun list_all_fusion g p s =
  (if has_next g s then let
                          val (x, sa) = next g s;
                        in
                          p x andalso list_all_fusion g p sa
                        end
    else true);

type 'a ceq = {ceq : ('a -> 'a -> bool) option};
val ceq = #ceq : 'a ceq -> ('a -> 'a -> bool) option;

datatype color = R | B;

datatype ('a, 'b) rbt = Empty |
  Branch of color * ('a, 'b) rbt * 'a * 'b * ('a, 'b) rbt;

fun rbt_keys_next ((k, t) :: kts, Empty) = (k, (kts, t))
  | rbt_keys_next (kts, Branch (c, l, k, v, r)) =
    rbt_keys_next ((k, r) :: kts, l);

fun rbt_has_next ([], Empty) = false
  | rbt_has_next (vb :: vc, va) = true
  | rbt_has_next (v, Branch (vb, vc, vd, ve, vf)) = true;

val rbt_keys_generator :
  ('a, (('a * ('a, 'b) rbt) list * ('a, 'b) rbt)) generator
  = Generator (rbt_has_next, rbt_keys_next);

fun lt_of_comp acomp x y =
  (case acomp x y of Eq => false | Lt => true | Gt => false);

datatype ('b, 'a) mapping_rbt = Mapping_RBTa of ('b, 'a) rbt;

datatype 'a set_dlist = Abs_dlist of 'a list;

datatype 'a set = Collect_set of ('a -> bool) | DList_set of 'a set_dlist |
  RBT_set of ('a, unit) mapping_rbt | Set_Monad of 'a list |
  Complement of 'a set;

fun list_of_dlist A_ (Abs_dlist x) = x;

fun list_all p [] = true
  | list_all p (x :: xs) = p x andalso list_all p xs;

fun dlist_all A_ x xc = list_all x (list_of_dlist A_ xc);

fun impl_ofa B_ (Mapping_RBTa x) = x;

fun rbt_init x = ([], x);

fun init A_ xa = rbt_init (impl_ofa A_ xa);

type 'a cenum =
  {cEnum :
     ('a list * ((('a -> bool) -> bool) * (('a -> bool) -> bool))) option};
val cEnum = #cEnum :
  'a cenum ->
    ('a list * ((('a -> bool) -> bool) * (('a -> bool) -> bool))) option;

fun filtera p [] = []
  | filtera p (x :: xs) = (if p x then x :: filtera p xs else filtera p xs);

fun collect A_ p =
  (case cEnum A_ of NONE => Collect_set p
    | SOME (enum, _) => Set_Monad (filtera p enum));

fun list_member equal (x :: xs) y = equal x y orelse list_member equal xs y
  | list_member equal [] y = false;

fun the (SOME x2) = x2;

fun memberc A_ xa = list_member (the (ceq A_)) (list_of_dlist A_ xa);

type 'a equal = {equal : 'a -> 'a -> bool};
val equal = #equal : 'a equal -> 'a -> 'a -> bool;

fun eq A_ a b = equal A_ a b;

fun equal_option A_ NONE (SOME x2) = false
  | equal_option A_ (SOME x2) NONE = false
  | equal_option A_ (SOME x2) (SOME y2) = eq A_ x2 y2
  | equal_option A_ NONE NONE = true;

fun rbt_comp_lookup c Empty k = NONE
  | rbt_comp_lookup c (Branch (uu, l, x, y, r)) k =
    (case c k x of Eq => SOME y | Lt => rbt_comp_lookup c l k
      | Gt => rbt_comp_lookup c r k);

fun lookupb A_ xa = rbt_comp_lookup (the (ccompare A_)) (impl_ofa A_ xa);

fun equal_unita u v = true;

val equal_unit = {equal = equal_unita} : unit equal;

fun memberb A_ t x = equal_option equal_unit (lookupb A_ t x) (SOME ());

fun member (A1_, A2_) x (Set_Monad xs) =
  (case ceq A1_
    of NONE =>
      (raise Fail "member Set_Monad: ceq = None")
        (fn _ => member (A1_, A2_) x (Set_Monad xs))
    | SOME eq => list_member eq xs x)
  | member (A1_, A2_) xa (Complement x) = not (member (A1_, A2_) xa x)
  | member (A1_, A2_) x (RBT_set rbt) = memberb A2_ rbt x
  | member (A1_, A2_) x (DList_set dxs) = memberc A1_ dxs x
  | member (A1_, A2_) x (Collect_set a) = a x;

fun subset_eq (A1_, A2_, A3_) (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare A3_
    of NONE =>
      (raise Fail "subset RBT_set RBT_set: ccompare = None")
        (fn _ => subset_eq (A1_, A2_, A3_) (RBT_set rbt1) (RBT_set rbt2))
    | SOME c =>
      (case ceq A2_
        of NONE =>
          sorted_list_subset_fusion (lt_of_comp c)
            (fn x => fn y => equal_order (c x y) Eq) rbt_keys_generator
            rbt_keys_generator (init A3_ rbt1) (init A3_ rbt2)
        | SOME eq =>
          sorted_list_subset_fusion (lt_of_comp c) eq rbt_keys_generator
            rbt_keys_generator (init A3_ rbt1) (init A3_ rbt2)))
  | subset_eq (A1_, A2_, A3_) (Complement a1) (Complement a2) =
    subset_eq (A1_, A2_, A3_) a2 a1
  | subset_eq (A1_, A2_, A3_) (Collect_set p) (Complement a) =
    subset_eq (A1_, A2_, A3_) a (collect A1_ (fn x => not (p x)))
  | subset_eq (A1_, A2_, A3_) (Set_Monad xs) c =
    list_all (fn x => member (A2_, A3_) x c) xs
  | subset_eq (A1_, A2_, A3_) (DList_set dxs) c =
    (case ceq A2_
      of NONE =>
        (raise Fail "subset DList_set1: ceq = None")
          (fn _ => subset_eq (A1_, A2_, A3_) (DList_set dxs) c)
      | SOME _ => dlist_all A2_ (fn x => member (A2_, A3_) x c) dxs)
  | subset_eq (A1_, A2_, A3_) (RBT_set rbt) b =
    (case ccompare A3_
      of NONE =>
        (raise Fail "subset RBT_set1: ccompare = None")
          (fn _ => subset_eq (A1_, A2_, A3_) (RBT_set rbt) b)
      | SOME _ =>
        list_all_fusion rbt_keys_generator (fn x => member (A2_, A3_) x b)
          (init A3_ rbt));

fun less_eq_set (A1_, A2_, A3_) = subset_eq (A1_, A2_, A3_);

fun list_all2_fusion p g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 andalso let
                                  val (x, s1a) = next g1 s1;
                                  val (y, s2a) = next g2 s2;
                                in
                                  p x y andalso list_all2_fusion p g1 g2 s1a s2a
                                end
    else not (has_next g2 s2));

fun set_eq (A1_, A2_, A3_) (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare A3_
    of NONE =>
      (raise Fail "set_eq RBT_set RBT_set: ccompare = None")
        (fn _ => set_eq (A1_, A2_, A3_) (RBT_set rbt1) (RBT_set rbt2))
    | SOME c =>
      (case ceq A2_
        of NONE =>
          list_all2_fusion (fn x => fn y => equal_order (c x y) Eq)
            rbt_keys_generator rbt_keys_generator (init A3_ rbt1)
            (init A3_ rbt2)
        | SOME eq =>
          list_all2_fusion eq rbt_keys_generator rbt_keys_generator
            (init A3_ rbt1) (init A3_ rbt2)))
  | set_eq (A1_, A2_, A3_) (Complement a) (Complement b) =
    set_eq (A1_, A2_, A3_) a b
  | set_eq (A1_, A2_, A3_) a b =
    less_eq_set (A1_, A2_, A3_) a b andalso less_eq_set (A1_, A2_, A3_) b a;

datatype ('b, 'a) alist = Alist of ('b * 'a) list;

datatype ('a, 'b) mapping = Assoc_List_Mapping of ('a, 'b) alist |
  RBT_Mapping of ('a, 'b) mapping_rbt | Mapping of ('a -> 'b option);

datatype ('a, 'b, 'c) fsm_with_precomputations_impl =
  FSMWPI of
    'a * 'a set * 'b set * 'c set * ('a * ('b * ('c * 'a))) set *
      (('a * 'b), ('c * 'a) set) mapping *
      (('a * 'b), ('c, 'a) mapping) mapping;

datatype ('c, 'b, 'a) fsm_with_precomputations =
  Fsm_with_precomputations of ('c, 'b, 'a) fsm_with_precomputations_impl;

fun fsm_with_precomputations_impl_of_fsm_with_precomputations
  (Fsm_with_precomputations x) = x;

fun transitions_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x5;

fun transitions_wp x =
  transitions_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

datatype ('a, 'b, 'c) fsm_impl = FSMWP of ('a, 'b, 'c) fsm_with_precomputations;

fun transitionsa (FSMWP m) = transitions_wp m;

datatype ('c, 'b, 'a) fsm = Abs_fsm of ('c, 'b, 'a) fsm_impl;

fun fsm_impl_of_fsm (Abs_fsm x) = x;

fun transitions x = transitionsa (fsm_impl_of_fsm x);

fun outputs_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x4;

fun outputs_wp x =
  outputs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

fun outputsa (FSMWP m) = outputs_wp m;

fun outputs x = outputsa (fsm_impl_of_fsm x);

fun initial_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x1;

fun initial_wp x =
  initial_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

fun initiala (FSMWP m) = initial_wp m;

fun initial x = initiala (fsm_impl_of_fsm x);

fun states_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x2;

fun states_wp x =
  states_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

fun statesa (FSMWP m) = states_wp m;

fun states x = statesa (fsm_impl_of_fsm x);

fun inputs_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x3;

fun inputs_wp x =
  inputs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

fun inputsa (FSMWP m) = inputs_wp m;

fun inputs x = inputsa (fsm_impl_of_fsm x);

fun comparator_prod comp_a comp_b (x, xa) (y, ya) =
  (case comp_a x y of Eq => comp_b xa ya | Lt => Lt | Gt => Gt);

fun ccompare_proda A_ B_ =
  (case ccompare A_ of NONE => NONE
    | SOME comp_a =>
      (case ccompare B_ of NONE => NONE
        | SOME comp_b => SOME (comparator_prod comp_a comp_b)));

fun ccompare_prod A_ B_ = {ccompare = ccompare_proda A_ B_} :
  ('a * 'b) ccompare;

fun mapa f [] = []
  | mapa f (x21 :: x22) = f x21 :: mapa f x22;

fun product [] uu = []
  | product (x :: xs) ys = mapa (fn a => (x, a)) ys @ product xs ys;

fun cEnum_prod A_ B_ =
  (case cEnum A_ of NONE => NONE
    | SOME (enum_a, (enum_all_a, enum_ex_a)) =>
      (case cEnum B_ of NONE => NONE
        | SOME (enum_b, (enum_all_b, enum_ex_b)) =>
          SOME (product enum_a enum_b,
                 ((fn p => enum_all_a (fn x => enum_all_b (fn y => p (x, y)))),
                   (fn p =>
                     enum_ex_a (fn x => enum_ex_b (fn y => p (x, y))))))));

fun cenum_prod A_ B_ = {cEnum = cEnum_prod A_ B_} : ('a * 'b) cenum;

fun equality_prod eq_a eq_b (x, xa) (y, ya) = eq_a x y andalso eq_b xa ya;

fun ceq_proda A_ B_ =
  (case ceq A_ of NONE => NONE
    | SOME eq_a =>
      (case ceq B_ of NONE => NONE
        | SOME eq_b => SOME (equality_prod eq_a eq_b)));

fun ceq_prod A_ B_ = {ceq = ceq_proda A_ B_} : ('a * 'b) ceq;

fun equal_fsm (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (C1_, C2_, C3_) x y =
  eq A4_ (initial x) (initial y) andalso
    (set_eq (A1_, A2_, A3_) (states x) (states y) andalso
      (set_eq (B1_, B2_, B3_) (inputs x) (inputs y) andalso
        (set_eq (C1_, C2_, C3_) (outputs x) (outputs y) andalso
          set_eq
            (cenum_prod A1_ (cenum_prod B1_ (cenum_prod C1_ A1_)),
              ceq_prod A2_ (ceq_prod B2_ (ceq_prod C2_ A2_)),
              ccompare_prod A3_ (ccompare_prod B3_ (ccompare_prod C3_ A3_)))
            (transitions x) (transitions y))));

fun ceq_fsma (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (C1_, C2_, C3_) =
  SOME (equal_fsm (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (C1_, C2_, C3_));

fun ceq_fsm (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (C1_, C2_, C3_) =
  {ceq = ceq_fsma (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (C1_, C2_, C3_)} :
  ('a, 'b, 'c) fsm ceq;

datatype ('a, 'b) phantom = Phantom of 'b;

datatype set_impla = Set_Choose | Set_Collect | Set_DList | Set_RBT |
  Set_Monada;

val set_impl_fsma : (('a, 'b, 'c) fsm, set_impla) phantom = Phantom Set_DList;

type 'a set_impl = {set_impl : ('a, set_impla) phantom};
val set_impl = #set_impl : 'a set_impl -> ('a, set_impla) phantom;

val set_impl_fsm = {set_impl = set_impl_fsma} : ('a, 'b, 'c) fsm set_impl;

val ccompare_fsma : (('a, 'b, 'c) fsm -> ('a, 'b, 'c) fsm -> ordera) option =
  NONE;

val ccompare_fsm = {ccompare = ccompare_fsma} : ('a, 'b, 'c) fsm ccompare;

datatype nat = Nat of IntInf.int;

fun integer_of_nat (Nat x) = x;

fun equal_nata m n = (((integer_of_nat m) : IntInf.int) = (integer_of_nat n));

val equal_nat = {equal = equal_nata} : nat equal;

datatype num = One | Bit0 of num | Bit1 of num;

val one_nata : nat = Nat (1 : IntInf.int);

type 'a one = {one : 'a};
val one = #one : 'a one -> 'a;

val one_nat = {one = one_nata} : nat one;

fun times_nata m n = Nat (IntInf.* (integer_of_nat m, integer_of_nat n));

type 'a times = {times : 'a -> 'a -> 'a};
val times = #times : 'a times -> 'a -> 'a -> 'a;

type 'a power = {one_power : 'a one, times_power : 'a times};
val one_power = #one_power : 'a power -> 'a one;
val times_power = #times_power : 'a power -> 'a times;

val times_nat = {times = times_nata} : nat times;

val power_nat = {one_power = one_nat, times_power = times_nat} : nat power;

fun less_eq_nat m n = IntInf.<= (integer_of_nat m, integer_of_nat n);

type 'a ord = {less_eq : 'a -> 'a -> bool, less : 'a -> 'a -> bool};
val less_eq = #less_eq : 'a ord -> 'a -> 'a -> bool;
val less = #less : 'a ord -> 'a -> 'a -> bool;

fun less_nat m n = IntInf.< (integer_of_nat m, integer_of_nat n);

val ord_nat = {less_eq = less_eq_nat, less = less_nat} : nat ord;

type 'a preorder = {ord_preorder : 'a ord};
val ord_preorder = #ord_preorder : 'a preorder -> 'a ord;

type 'a order = {preorder_order : 'a preorder};
val preorder_order = #preorder_order : 'a order -> 'a preorder;

val preorder_nat = {ord_preorder = ord_nat} : nat preorder;

val order_nat = {preorder_order = preorder_nat} : nat order;

val ceq_nata : (nat -> nat -> bool) option = SOME equal_nata;

val ceq_nat = {ceq = ceq_nata} : nat ceq;

val set_impl_nata : (nat, set_impla) phantom = Phantom Set_RBT;

val set_impl_nat = {set_impl = set_impl_nata} : nat set_impl;

type 'a linorder = {order_linorder : 'a order};
val order_linorder = #order_linorder : 'a linorder -> 'a order;

val linorder_nat = {order_linorder = order_nat} : nat linorder;

val finite_UNIV_nata : (nat, bool) phantom = Phantom false;

val zero_nat : nat = Nat (0 : IntInf.int);

val card_UNIV_nata : (nat, nat) phantom = Phantom zero_nat;

type 'a finite_UNIV = {finite_UNIV : ('a, bool) phantom};
val finite_UNIV = #finite_UNIV : 'a finite_UNIV -> ('a, bool) phantom;

type 'a card_UNIV =
  {finite_UNIV_card_UNIV : 'a finite_UNIV, card_UNIVa : ('a, nat) phantom};
val finite_UNIV_card_UNIV = #finite_UNIV_card_UNIV :
  'a card_UNIV -> 'a finite_UNIV;
val card_UNIVa = #card_UNIVa : 'a card_UNIV -> ('a, nat) phantom;

val finite_UNIV_nat = {finite_UNIV = finite_UNIV_nata} : nat finite_UNIV;

val card_UNIV_nat =
  {finite_UNIV_card_UNIV = finite_UNIV_nat, card_UNIVa = card_UNIV_nata} :
  nat card_UNIV;

val cEnum_nat :
  (nat list * (((nat -> bool) -> bool) * ((nat -> bool) -> bool))) option
  = NONE;

val cenum_nat = {cEnum = cEnum_nat} : nat cenum;

fun comparator_of (A1_, A2_) x y =
  (if less ((ord_preorder o preorder_order o order_linorder) A2_) x y then Lt
    else (if eq A1_ x y then Eq else Gt));

fun compare_nat x = comparator_of (equal_nat, linorder_nat) x;

val ccompare_nata : (nat -> nat -> ordera) option = SOME compare_nat;

val ccompare_nat = {ccompare = ccompare_nata} : nat ccompare;

datatype mapping_impla = Mapping_Choose | Mapping_Assoc_List | Mapping_RBT |
  Mapping_Mapping;

val mapping_impl_nata : (nat, mapping_impla) phantom = Phantom Mapping_RBT;

type 'a mapping_impl = {mapping_impl : ('a, mapping_impla) phantom};
val mapping_impl = #mapping_impl :
  'a mapping_impl -> ('a, mapping_impla) phantom;

val mapping_impl_nat = {mapping_impl = mapping_impl_nata} : nat mapping_impl;

fun max A_ a b = (if less_eq A_ a b then b else a);

val ord_integer =
  {less_eq = (fn a => fn b => IntInf.<= (a, b)),
    less = (fn a => fn b => IntInf.< (a, b))}
  : IntInf.int ord;

fun minus_nat m n =
  Nat (max ord_integer (0 : IntInf.int)
        (IntInf.- (integer_of_nat m, integer_of_nat n)));

fun proper_interval_nat no NONE = true
  | proper_interval_nat NONE (SOME x) = less_nat zero_nat x
  | proper_interval_nat (SOME x) (SOME y) = less_nat one_nata (minus_nat y x);

fun cproper_interval_nata x = proper_interval_nat x;

type 'a cproper_interval =
  {ccompare_cproper_interval : 'a ccompare,
    cproper_interval : 'a option -> 'a option -> bool};
val ccompare_cproper_interval = #ccompare_cproper_interval :
  'a cproper_interval -> 'a ccompare;
val cproper_interval = #cproper_interval :
  'a cproper_interval -> 'a option -> 'a option -> bool;

val cproper_interval_nat =
  {ccompare_cproper_interval = ccompare_nat,
    cproper_interval = cproper_interval_nata}
  : nat cproper_interval;

fun equal_seta (A1_, A2_, A3_, A4_) a b =
  less_eq_set (A1_, A2_, A3_) a b andalso less_eq_set (A1_, A2_, A3_) b a;

fun equal_set (A1_, A2_, A3_, A4_) = {equal = equal_seta (A1_, A2_, A3_, A4_)} :
  'a set equal;

fun uminus_set (Complement b) = b
  | uminus_set (Collect_set p) = Collect_set (fn x => not (p x))
  | uminus_set a = Complement a;

fun balance (Branch (R, a, w, x, b)) s t (Branch (R, c, y, z, d)) =
  Branch (R, Branch (B, a, w, x, b), s, t, Branch (B, c, y, z, d))
  | balance (Branch (R, Branch (R, a, w, x, b), s, t, c)) y z Empty =
    Branch (R, Branch (B, a, w, x, b), s, t, Branch (B, c, y, z, Empty))
  | balance (Branch (R, Branch (R, a, w, x, b), s, t, c)) y z
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (R, Branch (B, a, w, x, b), s, t,
        Branch (B, c, y, z, Branch (B, va, vb, vc, vd)))
  | balance (Branch (R, Empty, w, x, Branch (R, b, s, t, c))) y z Empty =
    Branch (R, Branch (B, Empty, w, x, b), s, t, Branch (B, c, y, z, Empty))
  | balance
    (Branch (R, Branch (B, va, vb, vc, vd), w, x, Branch (R, b, s, t, c))) y z
    Empty =
    Branch
      (R, Branch (B, Branch (B, va, vb, vc, vd), w, x, b), s, t,
        Branch (B, c, y, z, Empty))
  | balance (Branch (R, Empty, w, x, Branch (R, b, s, t, c))) y z
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (R, Branch (B, Empty, w, x, b), s, t,
        Branch (B, c, y, z, Branch (B, va, vb, vc, vd)))
  | balance
    (Branch (R, Branch (B, ve, vf, vg, vh), w, x, Branch (R, b, s, t, c))) y z
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (R, Branch (B, Branch (B, ve, vf, vg, vh), w, x, b), s, t,
        Branch (B, c, y, z, Branch (B, va, vb, vc, vd)))
  | balance Empty w x (Branch (R, b, s, t, Branch (R, c, y, z, d))) =
    Branch (R, Branch (B, Empty, w, x, b), s, t, Branch (B, c, y, z, d))
  | balance (Branch (B, va, vb, vc, vd)) w x
    (Branch (R, b, s, t, Branch (R, c, y, z, d))) =
    Branch
      (R, Branch (B, Branch (B, va, vb, vc, vd), w, x, b), s, t,
        Branch (B, c, y, z, d))
  | balance Empty w x (Branch (R, Branch (R, b, s, t, c), y, z, Empty)) =
    Branch (R, Branch (B, Empty, w, x, b), s, t, Branch (B, c, y, z, Empty))
  | balance Empty w x
    (Branch (R, Branch (R, b, s, t, c), y, z, Branch (B, va, vb, vc, vd))) =
    Branch
      (R, Branch (B, Empty, w, x, b), s, t,
        Branch (B, c, y, z, Branch (B, va, vb, vc, vd)))
  | balance (Branch (B, va, vb, vc, vd)) w x
    (Branch (R, Branch (R, b, s, t, c), y, z, Empty)) =
    Branch
      (R, Branch (B, Branch (B, va, vb, vc, vd), w, x, b), s, t,
        Branch (B, c, y, z, Empty))
  | balance (Branch (B, va, vb, vc, vd)) w x
    (Branch (R, Branch (R, b, s, t, c), y, z, Branch (B, ve, vf, vg, vh))) =
    Branch
      (R, Branch (B, Branch (B, va, vb, vc, vd), w, x, b), s, t,
        Branch (B, c, y, z, Branch (B, ve, vf, vg, vh)))
  | balance Empty s t Empty = Branch (B, Empty, s, t, Empty)
  | balance Empty s t (Branch (B, va, vb, vc, vd)) =
    Branch (B, Empty, s, t, Branch (B, va, vb, vc, vd))
  | balance Empty s t (Branch (v, Empty, vb, vc, Empty)) =
    Branch (B, Empty, s, t, Branch (v, Empty, vb, vc, Empty))
  | balance Empty s t (Branch (v, Branch (B, ve, vf, vg, vh), vb, vc, Empty)) =
    Branch
      (B, Empty, s, t, Branch (v, Branch (B, ve, vf, vg, vh), vb, vc, Empty))
  | balance Empty s t (Branch (v, Empty, vb, vc, Branch (B, vf, vg, vh, vi))) =
    Branch
      (B, Empty, s, t, Branch (v, Empty, vb, vc, Branch (B, vf, vg, vh, vi)))
  | balance Empty s t
    (Branch (v, Branch (B, ve, vj, vk, vl), vb, vc, Branch (B, vf, vg, vh, vi)))
    = Branch
        (B, Empty, s, t,
          Branch
            (v, Branch (B, ve, vj, vk, vl), vb, vc, Branch (B, vf, vg, vh, vi)))
  | balance (Branch (B, va, vb, vc, vd)) s t Empty =
    Branch (B, Branch (B, va, vb, vc, vd), s, t, Empty)
  | balance (Branch (B, va, vb, vc, vd)) s t (Branch (B, ve, vf, vg, vh)) =
    Branch (B, Branch (B, va, vb, vc, vd), s, t, Branch (B, ve, vf, vg, vh))
  | balance (Branch (B, va, vb, vc, vd)) s t (Branch (v, Empty, vf, vg, Empty))
    = Branch
        (B, Branch (B, va, vb, vc, vd), s, t, Branch (v, Empty, vf, vg, Empty))
  | balance (Branch (B, va, vb, vc, vd)) s t
    (Branch (v, Branch (B, vi, vj, vk, vl), vf, vg, Empty)) =
    Branch
      (B, Branch (B, va, vb, vc, vd), s, t,
        Branch (v, Branch (B, vi, vj, vk, vl), vf, vg, Empty))
  | balance (Branch (B, va, vb, vc, vd)) s t
    (Branch (v, Empty, vf, vg, Branch (B, vj, vk, vl, vm))) =
    Branch
      (B, Branch (B, va, vb, vc, vd), s, t,
        Branch (v, Empty, vf, vg, Branch (B, vj, vk, vl, vm)))
  | balance (Branch (B, va, vb, vc, vd)) s t
    (Branch (v, Branch (B, vi, vn, vo, vp), vf, vg, Branch (B, vj, vk, vl, vm)))
    = Branch
        (B, Branch (B, va, vb, vc, vd), s, t,
          Branch
            (v, Branch (B, vi, vn, vo, vp), vf, vg, Branch (B, vj, vk, vl, vm)))
  | balance (Branch (v, Empty, vb, vc, Empty)) s t Empty =
    Branch (B, Branch (v, Empty, vb, vc, Empty), s, t, Empty)
  | balance (Branch (v, Empty, vb, vc, Branch (B, ve, vf, vg, vh))) s t Empty =
    Branch
      (B, Branch (v, Empty, vb, vc, Branch (B, ve, vf, vg, vh)), s, t, Empty)
  | balance (Branch (v, Branch (B, vf, vg, vh, vi), vb, vc, Empty)) s t Empty =
    Branch
      (B, Branch (v, Branch (B, vf, vg, vh, vi), vb, vc, Empty), s, t, Empty)
  | balance
    (Branch (v, Branch (B, vf, vg, vh, vi), vb, vc, Branch (B, ve, vj, vk, vl)))
    s t Empty =
    Branch
      (B, Branch
            (v, Branch (B, vf, vg, vh, vi), vb, vc, Branch (B, ve, vj, vk, vl)),
        s, t, Empty)
  | balance (Branch (v, Empty, vf, vg, Empty)) s t (Branch (B, va, vb, vc, vd))
    = Branch
        (B, Branch (v, Empty, vf, vg, Empty), s, t, Branch (B, va, vb, vc, vd))
  | balance (Branch (v, Empty, vf, vg, Branch (B, vi, vj, vk, vl))) s t
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (B, Branch (v, Empty, vf, vg, Branch (B, vi, vj, vk, vl)), s, t,
        Branch (B, va, vb, vc, vd))
  | balance (Branch (v, Branch (B, vj, vk, vl, vm), vf, vg, Empty)) s t
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (B, Branch (v, Branch (B, vj, vk, vl, vm), vf, vg, Empty), s, t,
        Branch (B, va, vb, vc, vd))
  | balance
    (Branch (v, Branch (B, vj, vk, vl, vm), vf, vg, Branch (B, vi, vn, vo, vp)))
    s t (Branch (B, va, vb, vc, vd)) =
    Branch
      (B, Branch
            (v, Branch (B, vj, vk, vl, vm), vf, vg, Branch (B, vi, vn, vo, vp)),
        s, t, Branch (B, va, vb, vc, vd));

fun rbt_comp_ins c f k v Empty = Branch (R, Empty, k, v, Empty)
  | rbt_comp_ins c f k v (Branch (B, l, x, y, r)) =
    (case c k x of Eq => Branch (B, l, x, f k y v, r)
      | Lt => balance (rbt_comp_ins c f k v l) x y r
      | Gt => balance l x y (rbt_comp_ins c f k v r))
  | rbt_comp_ins c f k v (Branch (R, l, x, y, r)) =
    (case c k x of Eq => Branch (R, l, x, f k y v, r)
      | Lt => Branch (R, rbt_comp_ins c f k v l, x, y, r)
      | Gt => Branch (R, l, x, y, rbt_comp_ins c f k v r));

fun paint c Empty = Empty
  | paint c (Branch (uu, l, k, v, r)) = Branch (c, l, k, v, r);

fun rbt_comp_insert_with_key c f k v t = paint B (rbt_comp_ins c f k v t);

fun rbt_comp_insert c =
  rbt_comp_insert_with_key c (fn _ => fn _ => fn nv => nv);

fun insertc A_ xc xd xe =
  Mapping_RBTa (rbt_comp_insert (the (ccompare A_)) xc xd (impl_ofa A_ xe));

fun comp_sunion_with c f ((ka, va) :: asa) ((k, v) :: bs) =
  (case c k ka of Eq => (ka, f ka va v) :: comp_sunion_with c f asa bs
    | Lt => (k, v) :: comp_sunion_with c f ((ka, va) :: asa) bs
    | Gt => (ka, va) :: comp_sunion_with c f asa ((k, v) :: bs))
  | comp_sunion_with c f [] bs = bs
  | comp_sunion_with c f asa [] = asa;

datatype compare = LT | GT | EQ;

fun skip_red (Branch (R, l, k, v, r)) = l
  | skip_red Empty = Empty
  | skip_red (Branch (B, va, vb, vc, vd)) = Branch (B, va, vb, vc, vd);

fun skip_black t =
  let
    val ta = skip_red t;
  in
    (case ta of Empty => ta | Branch (R, _, _, _, _) => ta
      | Branch (B, l, _, _, _) => l)
  end;

fun compare_height sx s t tx =
  (case (skip_red sx, (skip_red s, (skip_red t, skip_red tx)))
    of (Empty, (Empty, (_, Empty))) => EQ
    | (Empty, (Empty, (_, Branch (_, _, _, _, _)))) => LT
    | (Empty, (Branch (_, _, _, _, _), (Empty, _))) => EQ
    | (Empty, (Branch (_, _, _, _, _), (Branch (_, _, _, _, _), Empty))) => EQ
    | (Empty,
        (Branch (_, sa, _, _, _),
          (Branch (_, ta, _, _, _), Branch (_, txa, _, _, _))))
      => compare_height Empty sa ta (skip_black txa)
    | (Branch (_, _, _, _, _), (Empty, (Empty, Empty))) => GT
    | (Branch (_, _, _, _, _), (Empty, (Empty, Branch (_, _, _, _, _)))) => LT
    | (Branch (_, _, _, _, _), (Empty, (Branch (_, _, _, _, _), Empty))) => EQ
    | (Branch (_, _, _, _, _),
        (Empty, (Branch (_, _, _, _, _), Branch (_, _, _, _, _))))
      => LT
    | (Branch (_, _, _, _, _), (Branch (_, _, _, _, _), (Empty, _))) => GT
    | (Branch (_, sxa, _, _, _),
        (Branch (_, sa, _, _, _), (Branch (_, ta, _, _, _), Empty)))
      => compare_height (skip_black sxa) sa ta Empty
    | (Branch (_, sxa, _, _, _),
        (Branch (_, sa, _, _, _),
          (Branch (_, ta, _, _, _), Branch (_, txa, _, _, _))))
      => compare_height (skip_black sxa) sa ta (skip_black txa));

fun plus_nat m n = Nat (IntInf.+ (integer_of_nat m, integer_of_nat n));

fun suc n = plus_nat n one_nata;

fun gen_length n (x :: xs) = gen_length (suc n) xs
  | gen_length n [] = n;

fun size_list x = gen_length zero_nat x;

fun nat_of_integer k = Nat (max ord_integer (0 : IntInf.int) k);

fun apfst f (x, y) = (f x, y);

fun map_prod f g (a, b) = (f a, g b);

fun divmod_nat m n =
  let
    val k = integer_of_nat m;
    val l = integer_of_nat n;
  in
    map_prod nat_of_integer nat_of_integer
      (if ((k : IntInf.int) = (0 : IntInf.int))
        then ((0 : IntInf.int), (0 : IntInf.int))
        else (if ((l : IntInf.int) = (0 : IntInf.int))
               then ((0 : IntInf.int), k)
               else IntInf.divMod (IntInf.abs k, IntInf.abs l)))
  end;

fun rbtreeify_g n kvs =
  (if equal_nata n zero_nat orelse equal_nata n one_nata then (Empty, kvs)
    else let
           val (na, r) = divmod_nat n (nat_of_integer (2 : IntInf.int));
         in
           (if equal_nata r zero_nat
             then let
                    val (t1, (k, v) :: kvsa) = rbtreeify_g na kvs;
                  in
                    apfst (fn a => Branch (B, t1, k, v, a))
                      (rbtreeify_g na kvsa)
                  end
             else let
                    val (t1, (k, v) :: kvsa) = rbtreeify_f na kvs;
                  in
                    apfst (fn a => Branch (B, t1, k, v, a))
                      (rbtreeify_g na kvsa)
                  end)
         end)
and rbtreeify_f n kvs =
  (if equal_nata n zero_nat then (Empty, kvs)
    else (if equal_nata n one_nata then let
  val (k, v) :: kvsa = kvs;
in
  (Branch (R, Empty, k, v, Empty), kvsa)
end
           else let
                  val (na, r) = divmod_nat n (nat_of_integer (2 : IntInf.int));
                in
                  (if equal_nata r zero_nat
                    then let
                           val (t1, (k, v) :: kvsa) = rbtreeify_f na kvs;
                         in
                           apfst (fn a => Branch (B, t1, k, v, a))
                             (rbtreeify_g na kvsa)
                         end
                    else let
                           val (t1, (k, v) :: kvsa) = rbtreeify_f na kvs;
                         in
                           apfst (fn a => Branch (B, t1, k, v, a))
                             (rbtreeify_f na kvsa)
                         end)
                end));

fun rbtreeify kvs = fst (rbtreeify_g (suc (size_list kvs)) kvs);

fun gen_entries kvts (Branch (c, l, k, v, r)) =
  gen_entries (((k, v), r) :: kvts) l
  | gen_entries ((kv, t) :: kvts) Empty = kv :: gen_entries kvts t
  | gen_entries [] Empty = [];

fun entries x = gen_entries [] x;

fun folda f (Branch (c, lt, k, v, rt)) x = folda f rt (f k v (folda f lt x))
  | folda f Empty x = x;

fun rbt_comp_union_with_key c f t1 t2 =
  (case compare_height t1 t1 t2 t2
    of LT =>
      folda (rbt_comp_insert_with_key c (fn k => fn v => fn w => f k w v)) t1 t2
    | GT => folda (rbt_comp_insert_with_key c f) t2 t1
    | EQ => rbtreeify (comp_sunion_with c f (entries t1) (entries t2)));

fun join A_ xc xd xe =
  Mapping_RBTa
    (rbt_comp_union_with_key (the (ccompare A_)) xc (impl_ofa A_ xd)
      (impl_ofa A_ xe));

fun list_insert equal x xs = (if list_member equal xs x then xs else x :: xs);

fun inserta A_ xb xc =
  Abs_dlist (list_insert (the (ceq A_)) xb (list_of_dlist A_ xc));

fun fold f (x :: xs) s = fold f xs (f x s)
  | fold f [] s = s;

fun foldc A_ x xc = fold x (list_of_dlist A_ xc);

fun union A_ = foldc A_ (inserta A_);

fun id x = (fn xa => xa) x;

fun is_none (SOME x) = false
  | is_none NONE = true;

fun inter_list A_ xb xc =
  Mapping_RBTa
    (fold (fn k => rbt_comp_insert (the (ccompare A_)) k ())
      (filtera
        (fn x =>
          not (is_none
                (rbt_comp_lookup (the (ccompare A_)) (impl_ofa A_ xb) x)))
        xc)
      Empty);

fun filtere A_ xb xc =
  Mapping_RBTa (rbtreeify (filtera xb (entries (impl_ofa A_ xc))));

fun comp_sinter_with c f ((ka, va) :: asa) ((k, v) :: bs) =
  (case c k ka of Eq => (ka, f ka va v) :: comp_sinter_with c f asa bs
    | Lt => comp_sinter_with c f ((ka, va) :: asa) bs
    | Gt => comp_sinter_with c f asa ((k, v) :: bs))
  | comp_sinter_with c f [] uu = []
  | comp_sinter_with c f uv [] = [];

fun map_option f NONE = NONE
  | map_option f (SOME x2) = SOME (f x2);

fun map_filter f [] = []
  | map_filter f (x :: xs) =
    (case f x of NONE => map_filter f xs | SOME y => y :: map_filter f xs);

fun rbt_comp_inter_with_key c f t1 t2 =
  (case compare_height t1 t1 t2 t2
    of LT =>
      rbtreeify
        (map_filter
          (fn (k, v) =>
            map_option (fn w => (k, f k v w)) (rbt_comp_lookup c t2 k))
          (entries t1))
    | GT =>
      rbtreeify
        (map_filter
          (fn (k, v) =>
            map_option (fn w => (k, f k w v)) (rbt_comp_lookup c t1 k))
          (entries t2))
    | EQ => rbtreeify (comp_sinter_with c f (entries t1) (entries t2)));

fun meet A_ xc xd xe =
  Mapping_RBTa
    (rbt_comp_inter_with_key (the (ccompare A_)) xc (impl_ofa A_ xd)
      (impl_ofa A_ xe));

fun filterd A_ xb xc = Abs_dlist (filtera xb (list_of_dlist A_ xc));

fun inf_seta (A1_, A2_) (RBT_set rbt1) (Set_Monad xs) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "inter RBT_set Set_Monad: ccompare = None")
        (fn _ => inf_seta (A1_, A2_) (RBT_set rbt1) (Set_Monad xs))
    | SOME _ => RBT_set (inter_list A2_ rbt1 xs))
  | inf_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter RBT_set DList_set: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
      | SOME _ =>
        (case ceq A1_
          of NONE =>
            (raise Fail "inter RBT_set DList_set: ceq = None")
              (fn _ => inf_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
          | SOME _ => RBT_set (inter_list A2_ rbt (list_of_dlist A1_ dxs))))
  | inf_seta (A1_, A2_) (RBT_set rbt1) (RBT_set rbt2) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter RBT_set RBT_set: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) (RBT_set rbt1) (RBT_set rbt2))
      | SOME _ => RBT_set (meet A2_ (fn _ => fn _ => id) rbt1 rbt2))
  | inf_seta (A1_, A2_) (DList_set dxs1) (Set_Monad xs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter DList_set Set_Monad: ceq = None")
          (fn _ => inf_seta (A1_, A2_) (DList_set dxs1) (Set_Monad xs))
      | SOME eq => DList_set (filterd A1_ (list_member eq xs) dxs1))
  | inf_seta (A1_, A2_) (DList_set dxs1) (DList_set dxs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter DList_set DList_set: ceq = None")
          (fn _ => inf_seta (A1_, A2_) (DList_set dxs1) (DList_set dxs2))
      | SOME _ => DList_set (filterd A1_ (memberc A1_ dxs2) dxs1))
  | inf_seta (A1_, A2_) (DList_set dxs) (RBT_set rbt) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter DList_set RBT_set: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) (DList_set dxs) (RBT_set rbt))
      | SOME _ =>
        (case ceq A1_
          of NONE =>
            (raise Fail "inter DList_set RBT_set: ceq = None")
              (fn _ => inf_seta (A1_, A2_) (DList_set dxs) (RBT_set rbt))
          | SOME _ => RBT_set (inter_list A2_ rbt (list_of_dlist A1_ dxs))))
  | inf_seta (A1_, A2_) (Set_Monad xs1) (Set_Monad xs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter Set_Monad Set_Monad: ceq = None")
          (fn _ => inf_seta (A1_, A2_) (Set_Monad xs1) (Set_Monad xs2))
      | SOME eq => Set_Monad (filtera (list_member eq xs2) xs1))
  | inf_seta (A1_, A2_) (Set_Monad xs) (DList_set dxs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter Set_Monad DList_set: ceq = None")
          (fn _ => inf_seta (A1_, A2_) (Set_Monad xs) (DList_set dxs2))
      | SOME eq => DList_set (filterd A1_ (list_member eq xs) dxs2))
  | inf_seta (A1_, A2_) (Set_Monad xs) (RBT_set rbt1) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter Set_Monad RBT_set: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) (RBT_set rbt1) (Set_Monad xs))
      | SOME _ => RBT_set (inter_list A2_ rbt1 xs))
  | inf_seta (A1_, A2_) (Complement ba) (Complement b) =
    Complement (sup_seta (A1_, A2_) ba b)
  | inf_seta (A1_, A2_) g (RBT_set rbt2) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter RBT_set2: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) g (RBT_set rbt2))
      | SOME _ =>
        RBT_set (filtere A2_ ((fn x => member (A1_, A2_) x g) o fst) rbt2))
  | inf_seta (A1_, A2_) (RBT_set rbt1) g =
    (case ccompare A2_
      of NONE =>
        (raise Fail "inter RBT_set1: ccompare = None")
          (fn _ => inf_seta (A1_, A2_) (RBT_set rbt1) g)
      | SOME _ =>
        RBT_set (filtere A2_ ((fn x => member (A1_, A2_) x g) o fst) rbt1))
  | inf_seta (A1_, A2_) h (DList_set dxs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter DList_set2: ceq = None")
          (fn _ => inf_seta (A1_, A2_) h (DList_set dxs2))
      | SOME _ => DList_set (filterd A1_ (fn x => member (A1_, A2_) x h) dxs2))
  | inf_seta (A1_, A2_) (DList_set dxs1) h =
    (case ceq A1_
      of NONE =>
        (raise Fail "inter DList_set1: ceq = None")
          (fn _ => inf_seta (A1_, A2_) (DList_set dxs1) h)
      | SOME _ => DList_set (filterd A1_ (fn x => member (A1_, A2_) x h) dxs1))
  | inf_seta (A1_, A2_) i (Set_Monad xs) =
    Set_Monad (filtera (fn x => member (A1_, A2_) x i) xs)
  | inf_seta (A1_, A2_) (Set_Monad xs) i =
    Set_Monad (filtera (fn x => member (A1_, A2_) x i) xs)
  | inf_seta (A1_, A2_) j (Collect_set a) =
    Collect_set (fn x => a x andalso member (A1_, A2_) x j)
  | inf_seta (A1_, A2_) (Collect_set a) j =
    Collect_set (fn x => a x andalso member (A1_, A2_) x j)
and sup_seta (A1_, A2_) ba (Complement b) =
  Complement (inf_seta (A1_, A2_) (uminus_set ba) b)
  | sup_seta (A1_, A2_) (Complement ba) b =
    Complement (inf_seta (A1_, A2_) ba (uminus_set b))
  | sup_seta (A1_, A2_) b (Collect_set a) =
    Collect_set (fn x => a x orelse member (A1_, A2_) x b)
  | sup_seta (A1_, A2_) (Collect_set a) b =
    Collect_set (fn x => a x orelse member (A1_, A2_) x b)
  | sup_seta (A1_, A2_) (Set_Monad xs) (Set_Monad ys) = Set_Monad (xs @ ys)
  | sup_seta (A1_, A2_) (DList_set dxs1) (Set_Monad ws) =
    (case ceq A1_
      of NONE =>
        (raise Fail "union DList_set Set_Monad: ceq = None")
          (fn _ => sup_seta (A1_, A2_) (DList_set dxs1) (Set_Monad ws))
      | SOME _ => DList_set (fold (inserta A1_) ws dxs1))
  | sup_seta (A1_, A2_) (Set_Monad ws) (DList_set dxs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "union Set_Monad DList_set: ceq = None")
          (fn _ => sup_seta (A1_, A2_) (Set_Monad ws) (DList_set dxs2))
      | SOME _ => DList_set (fold (inserta A1_) ws dxs2))
  | sup_seta (A1_, A2_) (RBT_set rbt1) (Set_Monad zs) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "union RBT_set Set_Monad: ccompare = None")
          (fn _ => sup_seta (A1_, A2_) (RBT_set rbt1) (Set_Monad zs))
      | SOME _ => RBT_set (fold (fn k => insertc A2_ k ()) zs rbt1))
  | sup_seta (A1_, A2_) (Set_Monad zs) (RBT_set rbt2) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "union Set_Monad RBT_set: ccompare = None")
          (fn _ => sup_seta (A1_, A2_) (Set_Monad zs) (RBT_set rbt2))
      | SOME _ => RBT_set (fold (fn k => insertc A2_ k ()) zs rbt2))
  | sup_seta (A1_, A2_) (DList_set dxs1) (DList_set dxs2) =
    (case ceq A1_
      of NONE =>
        (raise Fail "union DList_set DList_set: ceq = None")
          (fn _ => sup_seta (A1_, A2_) (DList_set dxs1) (DList_set dxs2))
      | SOME _ => DList_set (union A1_ dxs1 dxs2))
  | sup_seta (A1_, A2_) (DList_set dxs) (RBT_set rbt) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "union DList_set RBT_set: ccompare = None")
          (fn _ => sup_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
      | SOME _ =>
        (case ceq A1_
          of NONE =>
            (raise Fail "union DList_set RBT_set: ceq = None")
              (fn _ => sup_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
          | SOME _ => RBT_set (foldc A1_ (fn k => insertc A2_ k ()) dxs rbt)))
  | sup_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "union RBT_set DList_set: ccompare = None")
          (fn _ => sup_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
      | SOME _ =>
        (case ceq A1_
          of NONE =>
            (raise Fail "union RBT_set DList_set: ceq = None")
              (fn _ => sup_seta (A1_, A2_) (RBT_set rbt) (DList_set dxs))
          | SOME _ => RBT_set (foldc A1_ (fn k => insertc A2_ k ()) dxs rbt)))
  | sup_seta (A1_, A2_) (RBT_set rbt1) (RBT_set rbt2) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "union RBT_set RBT_set: ccompare = None")
          (fn _ => sup_seta (A1_, A2_) (RBT_set rbt1) (RBT_set rbt2))
      | SOME _ => RBT_set (join A2_ (fn _ => fn _ => id) rbt1 rbt2));

type 'a inf = {inf : 'a -> 'a -> 'a};
val inf = #inf : 'a inf -> 'a -> 'a -> 'a;

fun inf_set (A1_, A2_) = {inf = inf_seta (A1_, A2_)} : 'a set inf;

type 'a sup = {sup : 'a -> 'a -> 'a};
val sup = #sup : 'a sup -> 'a -> 'a -> 'a;

fun sup_set (A1_, A2_) = {sup = sup_seta (A1_, A2_)} : 'a set sup;

fun less_set (A1_, A2_, A3_) a b =
  less_eq_set (A1_, A2_, A3_) a b andalso not (less_eq_set (A1_, A2_, A3_) b a);

fun ord_set (A1_, A2_, A3_) =
  {less_eq = less_eq_set (A1_, A2_, A3_), less = less_set (A1_, A2_, A3_)} :
  'a set ord;

fun preorder_set (A1_, A2_, A3_) = {ord_preorder = ord_set (A1_, A2_, A3_)} :
  'a set preorder;

fun order_set (A1_, A2_, A3_) = {preorder_order = preorder_set (A1_, A2_, A3_)}
  : 'a set order;

type 'a semilattice_sup =
  {sup_semilattice_sup : 'a sup, order_semilattice_sup : 'a order};
val sup_semilattice_sup = #sup_semilattice_sup : 'a semilattice_sup -> 'a sup;
val order_semilattice_sup = #order_semilattice_sup :
  'a semilattice_sup -> 'a order;

type 'a semilattice_inf =
  {inf_semilattice_inf : 'a inf, order_semilattice_inf : 'a order};
val inf_semilattice_inf = #inf_semilattice_inf : 'a semilattice_inf -> 'a inf;
val order_semilattice_inf = #order_semilattice_inf :
  'a semilattice_inf -> 'a order;

type 'a lattice =
  {semilattice_inf_lattice : 'a semilattice_inf,
    semilattice_sup_lattice : 'a semilattice_sup};
val semilattice_inf_lattice = #semilattice_inf_lattice :
  'a lattice -> 'a semilattice_inf;
val semilattice_sup_lattice = #semilattice_sup_lattice :
  'a lattice -> 'a semilattice_sup;

fun semilattice_sup_set (A1_, A2_, A3_) =
  {sup_semilattice_sup = sup_set (A2_, A3_),
    order_semilattice_sup = order_set (A1_, A2_, A3_)}
  : 'a set semilattice_sup;

fun semilattice_inf_set (A1_, A2_, A3_) =
  {inf_semilattice_inf = inf_set (A2_, A3_),
    order_semilattice_inf = order_set (A1_, A2_, A3_)}
  : 'a set semilattice_inf;

fun lattice_set (A1_, A2_, A3_) =
  {semilattice_inf_lattice = semilattice_inf_set (A1_, A2_, A3_),
    semilattice_sup_lattice = semilattice_sup_set (A1_, A2_, A3_)}
  : 'a set lattice;

fun ceq_seta (A1_, A2_, A3_) =
  (case ceq A2_ of NONE => NONE | SOME _ => SOME (set_eq (A1_, A2_, A3_)));

fun ceq_set (A1_, A2_, A3_) = {ceq = ceq_seta (A1_, A2_, A3_)} : 'a set ceq;

val set_impl_seta : ('a set, set_impla) phantom = Phantom Set_Choose;

val set_impl_set = {set_impl = set_impl_seta} : 'a set set_impl;

fun of_phantom (Phantom x) = x;

fun finite_UNIV_seta A_ = Phantom (of_phantom (finite_UNIV A_));

fun power A_ a n =
  (if equal_nata n zero_nat then one (one_power A_)
    else times (times_power A_) a (power A_ a (minus_nat n one_nata)));

fun card_UNIV_seta A_ =
  Phantom
    let
      val c = of_phantom (card_UNIVa A_);
    in
      (if equal_nata c zero_nat then zero_nat
        else power power_nat (nat_of_integer (2 : IntInf.int)) c)
    end;

fun finite_UNIV_set A_ = {finite_UNIV = finite_UNIV_seta A_} :
  'a set finite_UNIV;

fun card_UNIV_set A_ =
  {finite_UNIV_card_UNIV = finite_UNIV_set (finite_UNIV_card_UNIV A_),
    card_UNIVa = card_UNIV_seta A_}
  : 'a set card_UNIV;

fun emptyd A_ = Mapping_RBTa Empty;

fun emptyb A_ = Abs_dlist [];

fun set_empty_choose (A1_, A2_) =
  (case ccompare A2_
    of NONE =>
      (case ceq A1_ of NONE => Set_Monad [] | SOME _ => DList_set (emptyb A1_))
    | SOME _ => RBT_set (emptyd A2_));

fun set_empty (A1_, A2_) Set_Choose = set_empty_choose (A1_, A2_)
  | set_empty (A1_, A2_) Set_Monada = Set_Monad []
  | set_empty (A1_, A2_) Set_RBT = RBT_set (emptyd A2_)
  | set_empty (A1_, A2_) Set_DList = DList_set (emptyb A1_)
  | set_empty (A1_, A2_) Set_Collect = Collect_set (fn _ => false);

fun fun_upda equal f aa b a = (if equal aa a then b else f a);

fun balance_right a k x (Branch (R, b, s, y, c)) =
  Branch (R, a, k, x, Branch (B, b, s, y, c))
  | balance_right (Branch (B, a, k, x, b)) s y Empty =
    balance (Branch (R, a, k, x, b)) s y Empty
  | balance_right (Branch (B, a, k, x, b)) s y (Branch (B, va, vb, vc, vd)) =
    balance (Branch (R, a, k, x, b)) s y (Branch (B, va, vb, vc, vd))
  | balance_right (Branch (R, a, k, x, Branch (B, b, s, y, c))) t z Empty =
    Branch (R, balance (paint R a) k x b, s, y, Branch (B, c, t, z, Empty))
  | balance_right (Branch (R, a, k, x, Branch (B, b, s, y, c))) t z
    (Branch (B, va, vb, vc, vd)) =
    Branch
      (R, balance (paint R a) k x b, s, y,
        Branch (B, c, t, z, Branch (B, va, vb, vc, vd)))
  | balance_right Empty k x Empty = Empty
  | balance_right (Branch (R, va, vb, vc, Empty)) k x Empty = Empty
  | balance_right (Branch (R, va, vb, vc, Branch (R, ve, vf, vg, vh))) k x Empty
    = Empty
  | balance_right Empty k x (Branch (B, va, vb, vc, vd)) = Empty
  | balance_right (Branch (R, ve, vf, vg, Empty)) k x
    (Branch (B, va, vb, vc, vd)) = Empty
  | balance_right (Branch (R, ve, vf, vg, Branch (R, vi, vj, vk, vl))) k x
    (Branch (B, va, vb, vc, vd)) = Empty;

fun balance_left (Branch (R, a, k, x, b)) s y c =
  Branch (R, Branch (B, a, k, x, b), s, y, c)
  | balance_left Empty k x (Branch (B, a, s, y, b)) =
    balance Empty k x (Branch (R, a, s, y, b))
  | balance_left (Branch (B, va, vb, vc, vd)) k x (Branch (B, a, s, y, b)) =
    balance (Branch (B, va, vb, vc, vd)) k x (Branch (R, a, s, y, b))
  | balance_left Empty k x (Branch (R, Branch (B, a, s, y, b), t, z, c)) =
    Branch (R, Branch (B, Empty, k, x, a), s, y, balance b t z (paint R c))
  | balance_left (Branch (B, va, vb, vc, vd)) k x
    (Branch (R, Branch (B, a, s, y, b), t, z, c)) =
    Branch
      (R, Branch (B, Branch (B, va, vb, vc, vd), k, x, a), s, y,
        balance b t z (paint R c))
  | balance_left Empty k x Empty = Empty
  | balance_left Empty k x (Branch (R, Empty, vb, vc, vd)) = Empty
  | balance_left Empty k x (Branch (R, Branch (R, ve, vf, vg, vh), vb, vc, vd))
    = Empty
  | balance_left (Branch (B, va, vb, vc, vd)) k x Empty = Empty
  | balance_left (Branch (B, va, vb, vc, vd)) k x
    (Branch (R, Empty, vf, vg, vh)) = Empty
  | balance_left (Branch (B, va, vb, vc, vd)) k x
    (Branch (R, Branch (R, vi, vj, vk, vl), vf, vg, vh)) = Empty;

fun combine Empty x = x
  | combine (Branch (v, va, vb, vc, vd)) Empty = Branch (v, va, vb, vc, vd)
  | combine (Branch (R, a, k, x, b)) (Branch (R, c, s, y, d)) =
    (case combine b c
      of Empty => Branch (R, a, k, x, Branch (R, Empty, s, y, d))
      | Branch (R, b2, t, z, c2) =>
        Branch (R, Branch (R, a, k, x, b2), t, z, Branch (R, c2, s, y, d))
      | Branch (B, b2, t, z, c2) =>
        Branch (R, a, k, x, Branch (R, Branch (B, b2, t, z, c2), s, y, d)))
  | combine (Branch (B, a, k, x, b)) (Branch (B, c, s, y, d)) =
    (case combine b c
      of Empty => balance_left a k x (Branch (B, Empty, s, y, d))
      | Branch (R, b2, t, z, c2) =>
        Branch (R, Branch (B, a, k, x, b2), t, z, Branch (B, c2, s, y, d))
      | Branch (B, b2, t, z, c2) =>
        balance_left a k x (Branch (B, Branch (B, b2, t, z, c2), s, y, d)))
  | combine (Branch (B, va, vb, vc, vd)) (Branch (R, b, k, x, c)) =
    Branch (R, combine (Branch (B, va, vb, vc, vd)) b, k, x, c)
  | combine (Branch (R, a, k, x, b)) (Branch (B, va, vb, vc, vd)) =
    Branch (R, a, k, x, combine b (Branch (B, va, vb, vc, vd)));

fun rbt_comp_del c x Empty = Empty
  | rbt_comp_del c x (Branch (uu, a, y, s, b)) =
    (case c x y of Eq => combine a b | Lt => rbt_comp_del_from_left c x a y s b
      | Gt => rbt_comp_del_from_right c x a y s b)
and rbt_comp_del_from_left c x (Branch (B, lt, z, v, rt)) y s b =
  balance_left (rbt_comp_del c x (Branch (B, lt, z, v, rt))) y s b
  | rbt_comp_del_from_left c x Empty y s b =
    Branch (R, rbt_comp_del c x Empty, y, s, b)
  | rbt_comp_del_from_left c x (Branch (R, va, vb, vc, vd)) y s b =
    Branch (R, rbt_comp_del c x (Branch (R, va, vb, vc, vd)), y, s, b)
and rbt_comp_del_from_right c x a y s (Branch (B, lt, z, v, rt)) =
  balance_right a y s (rbt_comp_del c x (Branch (B, lt, z, v, rt)))
  | rbt_comp_del_from_right c x a y s Empty =
    Branch (R, a, y, s, rbt_comp_del c x Empty)
  | rbt_comp_del_from_right c x a y s (Branch (R, va, vb, vc, vd)) =
    Branch (R, a, y, s, rbt_comp_del c x (Branch (R, va, vb, vc, vd)));

fun rbt_comp_delete c k t = paint B (rbt_comp_del c k t);

fun delete A_ xb xc =
  Mapping_RBTa (rbt_comp_delete (the (ccompare A_)) xb (impl_ofa A_ xc));

fun list_remove1 equal x (y :: xs) =
  (if equal x y then xs else y :: list_remove1 equal x xs)
  | list_remove1 equal x [] = [];

fun removea A_ xb xc =
  Abs_dlist (list_remove1 (the (ceq A_)) xb (list_of_dlist A_ xc));

fun insert (A1_, A2_) xa (Complement x) = Complement (remove (A1_, A2_) xa x)
  | insert (A1_, A2_) x (RBT_set rbt) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "insert RBT_set: ccompare = None")
          (fn _ => insert (A1_, A2_) x (RBT_set rbt))
      | SOME _ => RBT_set (insertc A2_ x () rbt))
  | insert (A1_, A2_) x (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "insert DList_set: ceq = None")
          (fn _ => insert (A1_, A2_) x (DList_set dxs))
      | SOME _ => DList_set (inserta A1_ x dxs))
  | insert (A1_, A2_) x (Set_Monad xs) = Set_Monad (x :: xs)
  | insert (A1_, A2_) x (Collect_set a) =
    (case ceq A1_
      of NONE =>
        (raise Fail "insert Collect_set: ceq = None")
          (fn _ => insert (A1_, A2_) x (Collect_set a))
      | SOME eq => Collect_set (fun_upda eq a x true))
and remove (A1_, A2_) x (Complement a) = Complement (insert (A1_, A2_) x a)
  | remove (A1_, A2_) x (RBT_set rbt) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "remove RBT_set: ccompare = None")
          (fn _ => remove (A1_, A2_) x (RBT_set rbt))
      | SOME _ => RBT_set (delete A2_ x rbt))
  | remove (A1_, A2_) x (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "remove DList_set: ceq = None")
          (fn _ => remove (A1_, A2_) x (DList_set dxs))
      | SOME _ => DList_set (removea A1_ x dxs))
  | remove (A1_, A2_) x (Collect_set a) =
    (case ceq A1_
      of NONE =>
        (raise Fail "remove Collect: ceq = None")
          (fn _ => remove (A1_, A2_) x (Collect_set a))
      | SOME eq => Collect_set (fun_upda eq a x false));

fun foldl f a [] = a
  | foldl f a (x :: xs) = foldl f (f a x) xs;

fun set_aux (A1_, A2_) Set_Monada = Set_Monad
  | set_aux (A1_, A2_) Set_Choose =
    (case ccompare A2_
      of NONE =>
        (case ceq A1_ of NONE => Set_Monad
          | SOME _ =>
            foldl (fn s => fn x => insert (A1_, A2_) x s)
              (DList_set (emptyb A1_)))
      | SOME _ =>
        foldl (fn s => fn x => insert (A1_, A2_) x s) (RBT_set (emptyd A2_)))
  | set_aux (A1_, A2_) impl =
    foldl (fn s => fn x => insert (A1_, A2_) x s) (set_empty (A1_, A2_) impl);

fun set (A1_, A2_, A3_) xs = set_aux (A1_, A2_) (of_phantom (set_impl A3_)) xs;

fun subseqs [] = [[]]
  | subseqs (x :: xs) = let
                          val xss = subseqs xs;
                        in
                          mapa (fn a => x :: a) xss @ xss
                        end;

fun list_ex p [] = false
  | list_ex p (x :: xs) = p x orelse list_ex p xs;

fun cEnum_set (A1_, A2_, A3_, A4_) =
  (case cEnum A1_ of NONE => NONE
    | SOME (enum_a, (_, _)) =>
      SOME (mapa (set (A2_, A3_, A4_)) (subseqs enum_a),
             ((fn p =>
                list_all p (mapa (set (A2_, A3_, A4_)) (subseqs enum_a))),
               (fn p =>
                 list_ex p (mapa (set (A2_, A3_, A4_)) (subseqs enum_a))))));

fun cenum_set (A1_, A2_, A3_, A4_) = {cEnum = cEnum_set (A1_, A2_, A3_, A4_)} :
  'a set cenum;

fun set_less_eq_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then let
                  val (x, s1a) = next g1 s1;
                  val (y, s2a) = next g2 s2;
                in
                  (if less x y
                    then proper_interval ao (SOME x) orelse
                           set_less_eq_aux_Compl_fusion less proper_interval g1
                             g2 (SOME x) s1a s2
                    else (if less y x
                           then proper_interval ao (SOME y) orelse
                                  set_less_eq_aux_Compl_fusion less
                                    proper_interval g1 g2 (SOME y) s1 s2a
                           else proper_interval ao (SOME y)))
                end
           else true)
    else true);

fun compl_set_less_eq_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then let
           val (x, s1a) = next g1 s1;
         in
           (if has_next g2 s2
             then let
                    val (y, s2a) = next g2 s2;
                  in
                    (if less x y
                      then not (proper_interval ao (SOME x)) andalso
                             compl_set_less_eq_aux_fusion less proper_interval
                               g1 g2 (SOME x) s1a s2
                      else (if less y x
                             then not (proper_interval ao (SOME y)) andalso
                                    compl_set_less_eq_aux_fusion less
                                      proper_interval g1 g2 (SOME y) s1 s2a
                             else not (proper_interval ao (SOME y))))
                  end
             else not (proper_interval ao (SOME x)) andalso
                    compl_set_less_eq_aux_fusion less proper_interval g1 g2
                      (SOME x) s1a s2)
         end
    else (if has_next g2 s2
           then let
                  val (y, s2a) = next g2 s2;
                in
                  not (proper_interval ao (SOME y)) andalso
                    compl_set_less_eq_aux_fusion less proper_interval g1 g2
                      (SOME y) s1 s2a
                end
           else not (proper_interval ao NONE)));

fun set_less_eq_aux_Compl less proper_interval ao (x :: xs) (y :: ys) =
  (if less x y
    then proper_interval ao (SOME x) orelse
           set_less_eq_aux_Compl less proper_interval (SOME x) xs (y :: ys)
    else (if less y x
           then proper_interval ao (SOME y) orelse
                  set_less_eq_aux_Compl less proper_interval (SOME y) (x :: xs)
                    ys
           else proper_interval ao (SOME y)))
  | set_less_eq_aux_Compl less proper_interval ao xs [] = true
  | set_less_eq_aux_Compl less proper_interval ao [] ys = true;

fun compl_set_less_eq_aux less proper_interval ao (x :: xs) (y :: ys) =
  (if less x y
    then not (proper_interval ao (SOME x)) andalso
           compl_set_less_eq_aux less proper_interval (SOME x) xs (y :: ys)
    else (if less y x
           then not (proper_interval ao (SOME y)) andalso
                  compl_set_less_eq_aux less proper_interval (SOME y) (x :: xs)
                    ys
           else not (proper_interval ao (SOME y))))
  | compl_set_less_eq_aux less proper_interval ao (x :: xs) [] =
    not (proper_interval ao (SOME x)) andalso
      compl_set_less_eq_aux less proper_interval (SOME x) xs []
  | compl_set_less_eq_aux less proper_interval ao [] (y :: ys) =
    not (proper_interval ao (SOME y)) andalso
      compl_set_less_eq_aux less proper_interval (SOME y) [] ys
  | compl_set_less_eq_aux less proper_interval ao [] [] =
    not (proper_interval ao NONE);

fun lexord_eq_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 andalso
           let
             val (x, s1a) = next g1 s1;
             val (y, s2a) = next g2 s2;
           in
             less x y orelse
               not (less y x) andalso lexord_eq_fusion less g1 g2 s1a s2a
           end
    else true);

fun remdups_sorted less (x :: y :: xs) =
  (if less x y then x :: remdups_sorted less (y :: xs)
    else remdups_sorted less (y :: xs))
  | remdups_sorted less [x] = [x]
  | remdups_sorted less [] = [];

fun quicksort_acc less ac (x :: v :: va) =
  quicksort_part less ac x [] [] [] (v :: va)
  | quicksort_acc less ac [x] = x :: ac
  | quicksort_acc less ac [] = ac
and quicksort_part less ac x lts eqs gts (z :: zs) =
  (if less x z then quicksort_part less ac x lts eqs (z :: gts) zs
    else (if less z x then quicksort_part less ac x (z :: lts) eqs gts zs
           else quicksort_part less ac x lts (z :: eqs) gts zs))
  | quicksort_part less ac x lts eqs gts [] =
    quicksort_acc less (eqs @ x :: quicksort_acc less ac gts) lts;

fun quicksort less = quicksort_acc less [];

fun gen_keys kts (Branch (c, l, k, v, r)) = gen_keys ((k, r) :: kts) l
  | gen_keys ((k, t) :: kts) Empty = k :: gen_keys kts t
  | gen_keys [] Empty = [];

fun keysa x = gen_keys [] x;

fun keysb A_ xa = keysa (impl_ofa A_ xa);

fun csorted_list_of_set (A1_, A2_) (Set_Monad xs) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "csorted_list_of_set Set_Monad: ccompare = None")
        (fn _ => csorted_list_of_set (A1_, A2_) (Set_Monad xs))
    | SOME c => remdups_sorted (lt_of_comp c) (quicksort (lt_of_comp c) xs))
  | csorted_list_of_set (A1_, A2_) (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "csorted_list_of_set DList_set: ceq = None")
          (fn _ => csorted_list_of_set (A1_, A2_) (DList_set dxs))
      | SOME _ =>
        (case ccompare A2_
          of NONE =>
            (raise Fail "csorted_list_of_set DList_set: ccompare = None")
              (fn _ => csorted_list_of_set (A1_, A2_) (DList_set dxs))
          | SOME c => quicksort (lt_of_comp c) (list_of_dlist A1_ dxs)))
  | csorted_list_of_set (A1_, A2_) (RBT_set rbt) =
    (case ccompare A2_
      of NONE =>
        (raise Fail "csorted_list_of_set RBT_set: ccompare = None")
          (fn _ => csorted_list_of_set (A1_, A2_) (RBT_set rbt))
      | SOME _ => keysb A2_ rbt);

fun bot_set (A1_, A2_, A3_) = set_empty (A1_, A2_) (of_phantom (set_impl A3_));

fun top_set (A1_, A2_, A3_) = uminus_set (bot_set (A1_, A2_, A3_));

fun le_of_comp acomp x y =
  (case acomp x y of Eq => true | Lt => true | Gt => false);

fun null [] = true
  | null (x :: xs) = false;

fun lexordp_eq less (x :: xs) (y :: ys) =
  less x y orelse not (less y x) andalso lexordp_eq less xs ys
  | lexordp_eq less (x :: xs) [] = false
  | lexordp_eq less xs [] = null xs
  | lexordp_eq less [] ys = true;

fun finite (A1_, A2_, A3_) (Collect_set p) =
  of_phantom (finite_UNIV A1_) orelse
    (raise Fail "finite Collect_set")
      (fn _ => finite (A1_, A2_, A3_) (Collect_set p))
  | finite (A1_, A2_, A3_) (Set_Monad xs) = true
  | finite (A1_, A2_, A3_) (Complement a) =
    (if of_phantom (finite_UNIV A1_) then true
      else (if finite (A1_, A2_, A3_) a then false
             else (raise Fail "finite Complement: infinite set")
                    (fn _ => finite (A1_, A2_, A3_) (Complement a))))
  | finite (A1_, A2_, A3_) (RBT_set rbt) =
    (case ccompare A3_
      of NONE =>
        (raise Fail "finite RBT_set: ccompare = None")
          (fn _ => finite (A1_, A2_, A3_) (RBT_set rbt))
      | SOME _ => true)
  | finite (A1_, A2_, A3_) (DList_set dxs) =
    (case ceq A2_
      of NONE =>
        (raise Fail "finite DList_set: ceq = None")
          (fn _ => finite (A1_, A2_, A3_) (DList_set dxs))
      | SOME _ => true);

fun set_less_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then let
           val (x, s1a) = next g1 s1;
         in
           (if has_next g2 s2
             then let
                    val (y, s2a) = next g2 s2;
                  in
                    (if less x y
                      then proper_interval ao (SOME x) orelse
                             set_less_aux_Compl_fusion less proper_interval g1
                               g2 (SOME x) s1a s2
                      else (if less y x
                             then proper_interval ao (SOME y) orelse
                                    set_less_aux_Compl_fusion less
                                      proper_interval g1 g2 (SOME y) s1 s2a
                             else proper_interval ao (SOME y)))
                  end
             else proper_interval ao (SOME x) orelse
                    set_less_aux_Compl_fusion less proper_interval g1 g2
                      (SOME x) s1a s2)
         end
    else (if has_next g2 s2
           then let
                  val (y, s2a) = next g2 s2;
                in
                  proper_interval ao (SOME y) orelse
                    set_less_aux_Compl_fusion less proper_interval g1 g2
                      (SOME y) s1 s2a
                end
           else proper_interval ao NONE));

fun compl_set_less_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 andalso
    (has_next g2 s2 andalso
      let
        val (x, s1a) = next g1 s1;
        val (y, s2a) = next g2 s2;
      in
        (if less x y
          then not (proper_interval ao (SOME x)) andalso
                 compl_set_less_aux_fusion less proper_interval g1 g2 (SOME x)
                   s1a s2
          else (if less y x
                 then not (proper_interval ao (SOME y)) andalso
                        compl_set_less_aux_fusion less proper_interval g1 g2
                          (SOME y) s1 s2a
                 else not (proper_interval ao (SOME y))))
      end);

fun set_less_aux_Compl less proper_interval ao (x :: xs) (y :: ys) =
  (if less x y
    then proper_interval ao (SOME x) orelse
           set_less_aux_Compl less proper_interval (SOME x) xs (y :: ys)
    else (if less y x
           then proper_interval ao (SOME y) orelse
                  set_less_aux_Compl less proper_interval (SOME y) (x :: xs) ys
           else proper_interval ao (SOME y)))
  | set_less_aux_Compl less proper_interval ao (x :: xs) [] =
    proper_interval ao (SOME x) orelse
      set_less_aux_Compl less proper_interval (SOME x) xs []
  | set_less_aux_Compl less proper_interval ao [] (y :: ys) =
    proper_interval ao (SOME y) orelse
      set_less_aux_Compl less proper_interval (SOME y) [] ys
  | set_less_aux_Compl less proper_interval ao [] [] = proper_interval ao NONE;

fun compl_set_less_aux less proper_interval ao (x :: xs) (y :: ys) =
  (if less x y
    then not (proper_interval ao (SOME x)) andalso
           compl_set_less_aux less proper_interval (SOME x) xs (y :: ys)
    else (if less y x
           then not (proper_interval ao (SOME y)) andalso
                  compl_set_less_aux less proper_interval (SOME y) (x :: xs) ys
           else not (proper_interval ao (SOME y))))
  | compl_set_less_aux less proper_interval ao xs [] = false
  | compl_set_less_aux less proper_interval ao [] ys = false;

fun lexord_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then let
                  val (x, s1a) = next g1 s1;
                  val (y, s2a) = next g2 s2;
                in
                  less x y orelse
                    not (less y x) andalso lexord_fusion less g1 g2 s1a s2a
                end
           else false)
    else has_next g2 s2);

fun lexordp less (x :: xs) (y :: ys) =
  less x y orelse not (less y x) andalso lexordp less xs ys
  | lexordp less xs [] = false
  | lexordp less [] ys = not (null ys);

fun comp_of_ords le lt x y =
  (if lt x y then Lt else (if le x y then Eq else Gt));

fun ccompare_seta (A1_, A2_, A3_, A4_) =
  (case ccompare (ccompare_cproper_interval A3_) of NONE => NONE
    | SOME _ =>
      SOME (comp_of_ords (cless_eq_set (A1_, A2_, A3_, A4_))
             (cless_set (A1_, A2_, A3_, A4_))))
and cless_set (A1_, A2_, A3_, A4_) (Complement (RBT_set rbt1)) (RBT_set rbt2) =
  (case ccompare (ccompare_cproper_interval A3_)
    of NONE =>
      (raise Fail "cless_set (Complement RBT_set) RBT_set: ccompare = None")
        (fn _ =>
          cless_set (A1_, A2_, A3_, A4_) (Complement (RBT_set rbt1))
            (RBT_set rbt2))
    | SOME c =>
      finite (A1_, A2_, ccompare_cproper_interval A3_)
        (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
        compl_set_less_aux_fusion (lt_of_comp c) (cproper_interval A3_)
          rbt_keys_generator rbt_keys_generator NONE
          (init (ccompare_cproper_interval A3_) rbt1)
          (init (ccompare_cproper_interval A3_) rbt2))
  | cless_set (A1_, A2_, A3_, A4_) (RBT_set rbt1) (Complement (RBT_set rbt2)) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set RBT_set (Complement RBT_set): ccompare = None")
          (fn _ =>
            cless_set (A1_, A2_, A3_, A4_) (RBT_set rbt1)
              (Complement (RBT_set rbt2)))
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_)
              (top_set (A2_, ccompare_cproper_interval A3_, A4_))
          then set_less_aux_Compl_fusion (lt_of_comp c) (cproper_interval A3_)
                 rbt_keys_generator rbt_keys_generator NONE
                 (init (ccompare_cproper_interval A3_) rbt1)
                 (init (ccompare_cproper_interval A3_) rbt2)
          else true))
  | cless_set (A1_, A2_, A3_, A4_) (RBT_set rbta) (RBT_set rbt) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set RBT_set RBT_set: ccompare = None")
          (fn _ => cless_set (A1_, A2_, A3_, A4_) (RBT_set rbta) (RBT_set rbt))
      | SOME c =>
        lexord_fusion (fn x => fn y => lt_of_comp c y x) rbt_keys_generator
          rbt_keys_generator (init (ccompare_cproper_interval A3_) rbta)
          (init (ccompare_cproper_interval A3_) rbt))
  | cless_set (A1_, A2_, A3_, A4_) (Complement a) (Complement b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set Complement Complement: ccompare = None")
          (fn _ => cless_set (A1_, A2_, A3_, A4_) (Complement a) (Complement b))
      | SOME _ => lt_of_comp (the (ccompare_seta (A1_, A2_, A3_, A4_))) b a)
  | cless_set (A1_, A2_, A3_, A4_) (Complement a) b =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set Complement1: ccompare = None")
          (fn _ => cless_set (A1_, A2_, A3_, A4_) (Complement a) b)
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then finite (A1_, A2_, ccompare_cproper_interval A3_)
                 (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
                 compl_set_less_aux (lt_of_comp c) (cproper_interval A3_) NONE
                   (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
                   (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b)
          else (raise Fail "cless_set Complement1: infinite set")
                 (fn _ => cless_set (A1_, A2_, A3_, A4_) (Complement a) b)))
  | cless_set (A1_, A2_, A3_, A4_) a (Complement b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set Complement2: ccompare = None")
          (fn _ => cless_set (A1_, A2_, A3_, A4_) a (Complement b))
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then (if finite (A1_, A2_, ccompare_cproper_interval A3_)
                     (top_set (A2_, ccompare_cproper_interval A3_, A4_))
                 then set_less_aux_Compl (lt_of_comp c) (cproper_interval A3_)
                        NONE
                        (csorted_list_of_set
                          (A2_, ccompare_cproper_interval A3_) a)
                        (csorted_list_of_set
                          (A2_, ccompare_cproper_interval A3_) b)
                 else true)
          else (raise Fail "cless_set Complement2: infinite set")
                 (fn _ => cless_set (A1_, A2_, A3_, A4_) a (Complement b))))
  | cless_set (A1_, A2_, A3_, A4_) a b =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_set: ccompare = None")
          (fn _ => cless_set (A1_, A2_, A3_, A4_) a b)
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then lexordp (fn x => fn y => lt_of_comp c y x)
                 (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
                 (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b)
          else (raise Fail "cless_set: infinite set")
                 (fn _ => cless_set (A1_, A2_, A3_, A4_) a b)))
and cless_eq_set (A1_, A2_, A3_, A4_) (Complement (RBT_set rbt1)) (RBT_set rbt2)
  = (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail
          "cless_eq_set (Complement RBT_set) RBT_set: ccompare = None")
          (fn _ =>
            cless_eq_set (A1_, A2_, A3_, A4_) (Complement (RBT_set rbt1))
              (RBT_set rbt2))
      | SOME c =>
        finite (A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          compl_set_less_eq_aux_fusion (lt_of_comp c) (cproper_interval A3_)
            rbt_keys_generator rbt_keys_generator NONE
            (init (ccompare_cproper_interval A3_) rbt1)
            (init (ccompare_cproper_interval A3_) rbt2))
  | cless_eq_set (A1_, A2_, A3_, A4_) (RBT_set rbt1) (Complement (RBT_set rbt2))
    = (case ccompare (ccompare_cproper_interval A3_)
        of NONE =>
          (raise Fail
            "cless_eq_set RBT_set (Complement RBT_set): ccompare = None")
            (fn _ =>
              cless_eq_set (A1_, A2_, A3_, A4_) (RBT_set rbt1)
                (Complement (RBT_set rbt2)))
        | SOME c =>
          (if finite (A1_, A2_, ccompare_cproper_interval A3_)
                (top_set (A2_, ccompare_cproper_interval A3_, A4_))
            then set_less_eq_aux_Compl_fusion (lt_of_comp c)
                   (cproper_interval A3_) rbt_keys_generator rbt_keys_generator
                   NONE (init (ccompare_cproper_interval A3_) rbt1)
                   (init (ccompare_cproper_interval A3_) rbt2)
            else true))
  | cless_eq_set (A1_, A2_, A3_, A4_) (RBT_set rbta) (RBT_set rbt) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_eq_set RBT_set RBT_set: ccompare = None")
          (fn _ =>
            cless_eq_set (A1_, A2_, A3_, A4_) (RBT_set rbta) (RBT_set rbt))
      | SOME c =>
        lexord_eq_fusion (fn x => fn y => lt_of_comp c y x) rbt_keys_generator
          rbt_keys_generator (init (ccompare_cproper_interval A3_) rbta)
          (init (ccompare_cproper_interval A3_) rbt))
  | cless_eq_set (A1_, A2_, A3_, A4_) (Complement a) (Complement b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_eq_set Complement Complement: ccompare = None")
          (fn _ =>
            le_of_comp (the (ccompare_seta (A1_, A2_, A3_, A4_))) (Complement a)
              (Complement b))
      | SOME _ => cless_eq_set (A1_, A2_, A3_, A4_) b a)
  | cless_eq_set (A1_, A2_, A3_, A4_) (Complement a) b =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_eq_set Complement1: ccompare = None")
          (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) (Complement a) b)
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then finite (A1_, A2_, ccompare_cproper_interval A3_)
                 (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
                 compl_set_less_eq_aux (lt_of_comp c) (cproper_interval A3_)
                   NONE
                   (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
                   (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b)
          else (raise Fail "cless_eq_set Complement1: infinite set")
                 (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) (Complement a) b)))
  | cless_eq_set (A1_, A2_, A3_, A4_) a (Complement b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_eq_set Complement2: ccompare = None")
          (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) a (Complement b))
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then (if finite (A1_, A2_, ccompare_cproper_interval A3_)
                     (top_set (A2_, ccompare_cproper_interval A3_, A4_))
                 then set_less_eq_aux_Compl (lt_of_comp c)
                        (cproper_interval A3_) NONE
                        (csorted_list_of_set
                          (A2_, ccompare_cproper_interval A3_) a)
                        (csorted_list_of_set
                          (A2_, ccompare_cproper_interval A3_) b)
                 else true)
          else (raise Fail "cless_eq_set Complement2: infinite set")
                 (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) a (Complement b))))
  | cless_eq_set (A1_, A2_, A3_, A4_) a b =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cless_eq_set: ccompare = None")
          (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) a b)
      | SOME c =>
        (if finite (A1_, A2_, ccompare_cproper_interval A3_) a andalso
              finite (A1_, A2_, ccompare_cproper_interval A3_) b
          then lexordp_eq (fn x => fn y => lt_of_comp c y x)
                 (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
                 (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b)
          else (raise Fail "cless_eq_set: infinite set")
                 (fn _ => cless_eq_set (A1_, A2_, A3_, A4_) a b)));

fun ccompare_set (A1_, A2_, A3_, A4_) =
  {ccompare = ccompare_seta (A1_, A2_, A3_, A4_)} : 'a set ccompare;

val mapping_impl_seta : ('a set, mapping_impla) phantom =
  Phantom Mapping_Choose;

val mapping_impl_set = {mapping_impl = mapping_impl_seta} : 'a set mapping_impl;

fun fold_fusion g f s b =
  (if has_next g s then let
                          val (x, sa) = next g s;
                        in
                          fold_fusion g f sa (f x b)
                        end
    else b);

fun length_last_fusion g s =
  (if has_next g s
    then let
           val (x, sa) = next g s;
         in
           fold_fusion g (fn xa => fn (n, _) => (plus_nat n one_nata, xa)) sa
             (one_nata, x)
         end
    else (zero_nat, (raise Fail "undefined")));

fun gen_length_fusion g n s =
  (if has_next g s then gen_length_fusion g (suc n) (snd (next g s)) else n);

fun length_fusion g = gen_length_fusion g zero_nat;

fun card_UNIV A_ = card_UNIVa A_;

fun proper_interval_set_Compl_aux_fusion A_ less proper_interval g1 g2 ao n s1
  s2 =
  (if has_next g1 s1
    then let
           val (x, s1a) = next g1 s1;
         in
           (if has_next g2 s2
             then let
                    val (y, s2a) = next g2 s2;
                  in
                    (if less x y
                      then proper_interval ao (SOME x) orelse
                             proper_interval_set_Compl_aux_fusion A_ less
                               proper_interval g1 g2 (SOME x)
                               (plus_nat n one_nata) s1a s2
                      else (if less y x
                             then proper_interval ao (SOME y) orelse
                                    proper_interval_set_Compl_aux_fusion A_ less
                                      proper_interval g1 g2 (SOME y)
                                      (plus_nat n one_nata) s1 s2a
                             else proper_interval ao (SOME x) andalso
                                    let
                                      val m =
minus_nat (of_phantom (card_UNIV A_)) n;
                                    in
                                      not
(equal_nata (minus_nat m (length_fusion g2 s2a))
  (nat_of_integer (2 : IntInf.int))) orelse
not (equal_nata (minus_nat m (length_fusion g1 s1a))
      (nat_of_integer (2 : IntInf.int)))
                                    end))
                  end
             else let
                    val m = minus_nat (of_phantom (card_UNIV A_)) n;
                    val (len_x, xa) = length_last_fusion g1 s1;
                  in
                    not (equal_nata m len_x) andalso
                      (if equal_nata m (plus_nat len_x one_nata)
                        then not (proper_interval (SOME xa) NONE) else true)
                  end)
         end
    else (if has_next g2 s2
           then let
                  val (_, _) = next g2 s2;
                  val m = minus_nat (of_phantom (card_UNIV A_)) n;
                  val (len_y, y) = length_last_fusion g2 s2;
                in
                  not (equal_nata m len_y) andalso
                    (if equal_nata m (plus_nat len_y one_nata)
                      then not (proper_interval (SOME y) NONE) else true)
                end
           else less_nat (plus_nat n one_nata) (of_phantom (card_UNIV A_))));

fun proper_interval_Compl_set_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 andalso
    (has_next g2 s2 andalso
      let
        val (x, s1a) = next g1 s1;
        val (y, s2a) = next g2 s2;
      in
        (if less x y
          then not (proper_interval ao (SOME x)) andalso
                 proper_interval_Compl_set_aux_fusion less proper_interval g1 g2
                   (SOME x) s1a s2
          else (if less y x
                 then not (proper_interval ao (SOME y)) andalso
                        proper_interval_Compl_set_aux_fusion less
                          proper_interval g1 g2 (SOME y) s1 s2a
                 else not (proper_interval ao (SOME x)) andalso
                        (has_next g2 s2a orelse has_next g1 s1a)))
      end);

fun exhaustive_above_fusion proper_interval g y s =
  (if has_next g s
    then let
           val (x, sa) = next g s;
         in
           not (proper_interval (SOME y) (SOME x)) andalso
             exhaustive_above_fusion proper_interval g x sa
         end
    else not (proper_interval (SOME y) NONE));

fun proper_interval_set_aux_fusion less proper_interval g1 g2 s1 s2 =
  has_next g2 s2 andalso
    let
      val (y, s2a) = next g2 s2;
    in
      (if has_next g1 s1
        then let
               val (x, s1a) = next g1 s1;
             in
               (if less x y then false
                 else (if less y x
                        then proper_interval (SOME y) (SOME x) orelse
                               (has_next g2 s2a orelse
                                 not (exhaustive_above_fusion proper_interval g1
                                       x s1a))
                        else proper_interval_set_aux_fusion less proper_interval
                               g1 g2 s1a s2a))
             end
        else has_next g2 s2a orelse proper_interval (SOME y) NONE)
    end;

fun length_last (x :: xs) =
  fold (fn xa => fn (n, _) => (plus_nat n one_nata, xa)) xs (one_nata, x)
  | length_last [] = (zero_nat, (raise Fail "undefined"));

fun proper_interval_set_Compl_aux A_ less proper_interval ao n (x :: xs)
  (y :: ys) =
  (if less x y
    then proper_interval ao (SOME x) orelse
           proper_interval_set_Compl_aux A_ less proper_interval (SOME x)
             (plus_nat n one_nata) xs (y :: ys)
    else (if less y x
           then proper_interval ao (SOME y) orelse
                  proper_interval_set_Compl_aux A_ less proper_interval (SOME y)
                    (plus_nat n one_nata) (x :: xs) ys
           else proper_interval ao (SOME x) andalso
                  let
                    val m = minus_nat (of_phantom (card_UNIV A_)) n;
                  in
                    not (equal_nata (minus_nat m (size_list ys))
                          (nat_of_integer (2 : IntInf.int))) orelse
                      not (equal_nata (minus_nat m (size_list xs))
                            (nat_of_integer (2 : IntInf.int)))
                  end))
  | proper_interval_set_Compl_aux A_ less proper_interval ao n (x :: xs) [] =
    let
      val m = minus_nat (of_phantom (card_UNIV A_)) n;
      val (len_x, xa) = length_last (x :: xs);
    in
      not (equal_nata m len_x) andalso
        (if equal_nata m (plus_nat len_x one_nata)
          then not (proper_interval (SOME xa) NONE) else true)
    end
  | proper_interval_set_Compl_aux A_ less proper_interval ao n [] (y :: ys) =
    let
      val m = minus_nat (of_phantom (card_UNIV A_)) n;
      val (len_y, ya) = length_last (y :: ys);
    in
      not (equal_nata m len_y) andalso
        (if equal_nata m (plus_nat len_y one_nata)
          then not (proper_interval (SOME ya) NONE) else true)
    end
  | proper_interval_set_Compl_aux A_ less proper_interval ao n [] [] =
    less_nat (plus_nat n one_nata) (of_phantom (card_UNIV A_));

fun proper_interval_Compl_set_aux less proper_interval ao uu [] = false
  | proper_interval_Compl_set_aux less proper_interval ao [] uv = false
  | proper_interval_Compl_set_aux less proper_interval ao (x :: xs) (y :: ys) =
    (if less x y
      then not (proper_interval ao (SOME x)) andalso
             proper_interval_Compl_set_aux less proper_interval (SOME x) xs
               (y :: ys)
      else (if less y x
             then not (proper_interval ao (SOME y)) andalso
                    proper_interval_Compl_set_aux less proper_interval (SOME y)
                      (x :: xs) ys
             else not (proper_interval ao (SOME x)) andalso
                    (if null ys then not (null xs) else true)));

fun exhaustive_above proper_interval x (y :: ys) =
  not (proper_interval (SOME x) (SOME y)) andalso
    exhaustive_above proper_interval y ys
  | exhaustive_above proper_interval x [] = not (proper_interval (SOME x) NONE);

fun proper_interval_set_aux less proper_interval (x :: xs) (y :: ys) =
  (if less x y then false
    else (if less y x
           then proper_interval (SOME y) (SOME x) orelse
                  (not (null ys) orelse
                    not (exhaustive_above proper_interval x xs))
           else proper_interval_set_aux less proper_interval xs ys))
  | proper_interval_set_aux less proper_interval [] (y :: ys) =
    not (null ys) orelse proper_interval (SOME y) NONE
  | proper_interval_set_aux less proper_interval xs [] = false;

fun exhaustive_fusion proper_interval g s =
  has_next g s andalso
    let
      val (x, sa) = next g s;
    in
      not (proper_interval NONE (SOME x)) andalso
        exhaustive_above_fusion proper_interval g x sa
    end;

fun list_remdups equal (x :: xs) =
  (if list_member equal xs x then list_remdups equal xs
    else x :: list_remdups equal xs)
  | list_remdups equal [] = [];

fun length A_ xa = size_list (list_of_dlist A_ xa);

fun card (A1_, A2_, A3_) (Complement a) =
  let
    val aa = card (A1_, A2_, A3_) a;
    val s = of_phantom (card_UNIV A1_);
  in
    (if less_nat zero_nat s then minus_nat s aa
      else (if finite (finite_UNIV_card_UNIV A1_, A2_, A3_) a then zero_nat
             else (raise Fail "card Complement: infinite")
                    (fn _ => card (A1_, A2_, A3_) (Complement a))))
  end
  | card (A1_, A2_, A3_) (Set_Monad xs) =
    (case ceq A2_
      of NONE =>
        (raise Fail "card Set_Monad: ceq = None")
          (fn _ => card (A1_, A2_, A3_) (Set_Monad xs))
      | SOME eq => size_list (list_remdups eq xs))
  | card (A1_, A2_, A3_) (RBT_set rbt) =
    (case ccompare A3_
      of NONE =>
        (raise Fail "card RBT_set: ccompare = None")
          (fn _ => card (A1_, A2_, A3_) (RBT_set rbt))
      | SOME _ => size_list (keysb A3_ rbt))
  | card (A1_, A2_, A3_) (DList_set dxs) =
    (case ceq A2_
      of NONE =>
        (raise Fail "card DList_set: ceq = None")
          (fn _ => card (A1_, A2_, A3_) (DList_set dxs))
      | SOME _ => length A2_ dxs);

fun is_UNIV (A1_, A2_, A3_) (RBT_set rbt) =
  (case ccompare (ccompare_cproper_interval A3_)
    of NONE =>
      (raise Fail "is_UNIV RBT_set: ccompare = None")
        (fn _ => is_UNIV (A1_, A2_, A3_) (RBT_set rbt))
    | SOME _ =>
      of_phantom (finite_UNIV (finite_UNIV_card_UNIV A1_)) andalso
        exhaustive_fusion (cproper_interval A3_) rbt_keys_generator
          (init (ccompare_cproper_interval A3_) rbt))
  | is_UNIV (A1_, A2_, A3_) a =
    let
      val aa = of_phantom (card_UNIV A1_);
      val b = card (A1_, A2_, ccompare_cproper_interval A3_) a;
    in
      (if less_nat zero_nat aa then equal_nata aa b
        else (if less_nat zero_nat b then false
               else (raise Fail "is_UNIV called on infinite type and set")
                      (fn _ => is_UNIV (A1_, A2_, A3_) a)))
    end;

fun is_emptya A_ xa =
  (case impl_ofa A_ xa of Empty => true | Branch (_, _, _, _, _) => false);

fun nulla A_ xa = null (list_of_dlist A_ xa);

fun is_empty (A1_, A2_, A3_) (Complement a) = is_UNIV (A1_, A2_, A3_) a
  | is_empty (A1_, A2_, A3_) (RBT_set rbt) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "is_empty RBT_set: ccompare = None")
          (fn _ => is_empty (A1_, A2_, A3_) (RBT_set rbt))
      | SOME _ => is_emptya (ccompare_cproper_interval A3_) rbt)
  | is_empty (A1_, A2_, A3_) (DList_set dxs) =
    (case ceq A2_
      of NONE =>
        (raise Fail "is_empty DList_set: ceq = None")
          (fn _ => is_empty (A1_, A2_, A3_) (DList_set dxs))
      | SOME _ => nulla A2_ dxs)
  | is_empty (A1_, A2_, A3_) (Set_Monad xs) = null xs;

fun cproper_interval_seta (A1_, A2_, A3_, A4_)
  (SOME (Complement (RBT_set rbt1))) (SOME (RBT_set rbt2)) =
  (case ccompare (ccompare_cproper_interval A3_)
    of NONE =>
      (raise Fail
        "cproper_interval (Complement RBT_set) RBT_set: ccompare = None")
        (fn _ =>
          cproper_interval_seta (A1_, A2_, A3_, A4_)
            (SOME (Complement (RBT_set rbt1))) (SOME (RBT_set rbt2)))
    | SOME c =>
      finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
        (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
        proper_interval_Compl_set_aux_fusion (lt_of_comp c)
          (cproper_interval A3_) rbt_keys_generator rbt_keys_generator NONE
          (init (ccompare_cproper_interval A3_) rbt1)
          (init (ccompare_cproper_interval A3_) rbt2))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (RBT_set rbt1))
    (SOME (Complement (RBT_set rbt2))) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail
          "cproper_interval RBT_set (Complement RBT_set): ccompare = None")
          (fn _ =>
            cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (RBT_set rbt1))
              (SOME (Complement (RBT_set rbt2))))
      | SOME c =>
        finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          proper_interval_set_Compl_aux_fusion A1_ (lt_of_comp c)
            (cproper_interval A3_) rbt_keys_generator rbt_keys_generator NONE
            zero_nat (init (ccompare_cproper_interval A3_) rbt1)
            (init (ccompare_cproper_interval A3_) rbt2))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (RBT_set rbt1))
    (SOME (RBT_set rbt2)) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cproper_interval RBT_set RBT_set: ccompare = None")
          (fn _ =>
            cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (RBT_set rbt1))
              (SOME (RBT_set rbt2)))
      | SOME c =>
        finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          proper_interval_set_aux_fusion (lt_of_comp c) (cproper_interval A3_)
            rbt_keys_generator rbt_keys_generator
            (init (ccompare_cproper_interval A3_) rbt1)
            (init (ccompare_cproper_interval A3_) rbt2))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (Complement a))
    (SOME (Complement b)) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cproper_interval Complement Complement: ccompare = None")
          (fn _ =>
            cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (Complement a))
              (SOME (Complement b)))
      | SOME _ => cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME b) (SOME a))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (Complement a)) (SOME b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cproper_interval Complement1: ccompare = None")
          (fn _ =>
            cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME (Complement a))
              (SOME b))
      | SOME c =>
        finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          proper_interval_Compl_set_aux (lt_of_comp c) (cproper_interval A3_)
            NONE (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
            (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME a) (SOME (Complement b)) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cproper_interval Complement2: ccompare = None")
          (fn _ =>
            cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME a)
              (SOME (Complement b)))
      | SOME c =>
        finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          proper_interval_set_Compl_aux A1_ (lt_of_comp c)
            (cproper_interval A3_) NONE zero_nat
            (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
            (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME a) (SOME b) =
    (case ccompare (ccompare_cproper_interval A3_)
      of NONE =>
        (raise Fail "cproper_interval: ccompare = None")
          (fn _ => cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME a) (SOME b))
      | SOME c =>
        finite (finite_UNIV_card_UNIV A1_, A2_, ccompare_cproper_interval A3_)
          (top_set (A2_, ccompare_cproper_interval A3_, A4_)) andalso
          proper_interval_set_aux (lt_of_comp c) (cproper_interval A3_)
            (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) a)
            (csorted_list_of_set (A2_, ccompare_cproper_interval A3_) b))
  | cproper_interval_seta (A1_, A2_, A3_, A4_) (SOME a) NONE =
    not (is_UNIV (A1_, A2_, A3_) a)
  | cproper_interval_seta (A1_, A2_, A3_, A4_) NONE (SOME b) =
    not (is_empty (A1_, A2_, A3_) b)
  | cproper_interval_seta (A1_, A2_, A3_, A4_) NONE NONE = true;

fun cproper_interval_set (A1_, A2_, A3_, A4_) =
  {ccompare_cproper_interval =
     ccompare_set (finite_UNIV_card_UNIV A1_, A2_, A3_, A4_),
    cproper_interval = cproper_interval_seta (A1_, A2_, A3_, A4_)}
  : 'a set cproper_interval;

fun equal_boola p true = p
  | equal_boola p false = not p
  | equal_boola true p = p
  | equal_boola false p = not p;

val equal_bool = {equal = equal_boola} : bool equal;

fun less_eq_bool true b = b
  | less_eq_bool false b = true;

fun less_bool true b = false
  | less_bool false b = b;

val ord_bool = {less_eq = less_eq_bool, less = less_bool} : bool ord;

val preorder_bool = {ord_preorder = ord_bool} : bool preorder;

val order_bool = {preorder_order = preorder_bool} : bool order;

val ceq_boola : (bool -> bool -> bool) option = SOME equal_boola;

val ceq_bool = {ceq = ceq_boola} : bool ceq;

val set_impl_boola : (bool, set_impla) phantom = Phantom Set_DList;

val set_impl_bool = {set_impl = set_impl_boola} : bool set_impl;

val linorder_bool = {order_linorder = order_bool} : bool linorder;

val finite_UNIV_boola : (bool, bool) phantom = Phantom true;

val card_UNIV_boola : (bool, nat) phantom =
  Phantom (nat_of_integer (2 : IntInf.int));

val finite_UNIV_bool = {finite_UNIV = finite_UNIV_boola} : bool finite_UNIV;

val card_UNIV_bool =
  {finite_UNIV_card_UNIV = finite_UNIV_bool, card_UNIVa = card_UNIV_boola} :
  bool card_UNIV;

fun enum_all_bool p = p false andalso p true;

fun enum_ex_bool p = p false orelse p true;

val enum_bool : bool list = [false, true];

val cEnum_bool :
  (bool list * (((bool -> bool) -> bool) * ((bool -> bool) -> bool))) option
  = SOME (enum_bool, (enum_all_bool, enum_ex_bool));

val cenum_bool = {cEnum = cEnum_bool} : bool cenum;

fun comparator_bool false false = Eq
  | comparator_bool false true = Lt
  | comparator_bool true true = Eq
  | comparator_bool true false = Gt;

fun compare_bool x = comparator_bool x;

val ccompare_boola : (bool -> bool -> ordera) option = SOME compare_bool;

val ccompare_bool = {ccompare = ccompare_boola} : bool ccompare;

val mapping_impl_boola : (bool, mapping_impla) phantom =
  Phantom Mapping_Assoc_List;

val mapping_impl_bool = {mapping_impl = mapping_impl_boola} : bool mapping_impl;

fun proper_interval_bool (SOME x) (SOME y) = false
  | proper_interval_bool (SOME x) NONE = not x
  | proper_interval_bool NONE (SOME y) = y
  | proper_interval_bool NONE NONE = true;

fun cproper_interval_boola x = proper_interval_bool x;

val cproper_interval_bool =
  {ccompare_cproper_interval = ccompare_bool,
    cproper_interval = cproper_interval_boola}
  : bool cproper_interval;

datatype 'a fset = Abs_fset of 'a set;

fun fset (Abs_fset x) = x;

fun less_eq_fset (A1_, A2_, A3_) xa xc =
  less_eq_set (A1_, A2_, A3_) (fset xa) (fset xc);

fun equal_fseta (A1_, A2_, A3_, A4_) a b =
  less_eq_fset (A1_, A2_, A3_) a b andalso less_eq_fset (A1_, A2_, A3_) b a;

fun equal_fset (A1_, A2_, A3_, A4_) = {equal = equal_fseta (A1_, A2_, A3_, A4_)}
  : 'a fset equal;

fun ceq_fseta (A1_, A2_, A3_, A4_) =
  SOME (eq (equal_fset (A1_, A2_, A3_, A4_)));

fun ceq_fset (A1_, A2_, A3_, A4_) = {ceq = ceq_fseta (A1_, A2_, A3_, A4_)} :
  'a fset ceq;

val set_impl_fseta : ('a fset, set_impla) phantom = Phantom Set_DList;

val set_impl_fset = {set_impl = set_impl_fseta} : 'a fset set_impl;

fun finite_UNIV_fseta A_ = Phantom (of_phantom (finite_UNIV A_));

fun card_UNIV_fseta A_ =
  Phantom
    let
      val c = of_phantom (card_UNIVa A_);
    in
      (if equal_nata c zero_nat then zero_nat
        else power power_nat (nat_of_integer (2 : IntInf.int)) c)
    end;

fun finite_UNIV_fset A_ = {finite_UNIV = finite_UNIV_fseta A_} :
  'a fset finite_UNIV;

fun card_UNIV_fset A_ =
  {finite_UNIV_card_UNIV = finite_UNIV_fset (finite_UNIV_card_UNIV A_),
    card_UNIVa = card_UNIV_fseta A_}
  : 'a fset card_UNIV;

val cEnum_fset :
  ('a fset list *
    ((('a fset -> bool) -> bool) * (('a fset -> bool) -> bool))) option
  = NONE;

val cenum_fset = {cEnum = cEnum_fset} : 'a fset cenum;

val ccompare_fseta : ('a fset -> 'a fset -> ordera) option = NONE;

val ccompare_fset = {ccompare = ccompare_fseta} : 'a fset ccompare;

val mapping_impl_fseta : ('a fset, mapping_impla) phantom =
  Phantom Mapping_Choose;

val mapping_impl_fset = {mapping_impl = mapping_impl_fseta} :
  'a fset mapping_impl;

type 'a infinite_UNIV = {};

fun cproper_interval_fseta A_ uu uv = (raise Fail "undefined");

fun cproper_interval_fset A_ =
  {ccompare_cproper_interval = ccompare_fset,
    cproper_interval = cproper_interval_fseta A_}
  : 'a fset cproper_interval;

fun equal_lista A_ [] (x21 :: x22) = false
  | equal_lista A_ (x21 :: x22) [] = false
  | equal_lista A_ (x21 :: x22) (y21 :: y22) =
    eq A_ x21 y21 andalso equal_lista A_ x22 y22
  | equal_lista A_ [] [] = true;

fun equal_list A_ = {equal = equal_lista A_} : ('a list) equal;

fun less_eq_list (A1_, A2_) (x :: xs) (y :: ys) =
  less ((ord_preorder o preorder_order) A2_) x y orelse
    eq A1_ x y andalso less_eq_list (A1_, A2_) xs ys
  | less_eq_list (A1_, A2_) [] xs = true
  | less_eq_list (A1_, A2_) (x :: xs) [] = false;

fun less_list (A1_, A2_) (x :: xs) (y :: ys) =
  less ((ord_preorder o preorder_order) A2_) x y orelse
    eq A1_ x y andalso less_list (A1_, A2_) xs ys
  | less_list (A1_, A2_) [] (x :: xs) = true
  | less_list (A1_, A2_) xs [] = false;

fun ord_list (A1_, A2_) =
  {less_eq = less_eq_list (A1_, A2_), less = less_list (A1_, A2_)} :
  ('a list) ord;

fun preorder_list (A1_, A2_) = {ord_preorder = ord_list (A1_, A2_)} :
  ('a list) preorder;

fun order_list (A1_, A2_) = {preorder_order = preorder_list (A1_, A2_)} :
  ('a list) order;

fun equality_list eq_a (x :: xa) (y :: ya) =
  eq_a x y andalso equality_list eq_a xa ya
  | equality_list eq_a (x :: xa) [] = false
  | equality_list eq_a [] (y :: ya) = false
  | equality_list eq_a [] [] = true;

fun ceq_lista A_ =
  (case ceq A_ of NONE => NONE | SOME eq_a => SOME (equality_list eq_a));

fun ceq_list A_ = {ceq = ceq_lista A_} : ('a list) ceq;

val set_impl_lista : (('a list), set_impla) phantom = Phantom Set_Choose;

val set_impl_list = {set_impl = set_impl_lista} : ('a list) set_impl;

fun linorder_list (A1_, A2_) =
  {order_linorder = order_list (A1_, order_linorder A2_)} : ('a list) linorder;

val finite_UNIV_lista : (('a list), bool) phantom = Phantom false;

val card_UNIV_lista : (('a list), nat) phantom = Phantom zero_nat;

val finite_UNIV_list = {finite_UNIV = finite_UNIV_lista} :
  ('a list) finite_UNIV;

val card_UNIV_list =
  {finite_UNIV_card_UNIV = finite_UNIV_list, card_UNIVa = card_UNIV_lista} :
  ('a list) card_UNIV;

val cEnum_list :
  (('a list) list *
    ((('a list -> bool) -> bool) * (('a list -> bool) -> bool))) option
  = NONE;

val cenum_list = {cEnum = cEnum_list} : ('a list) cenum;

fun comparator_list comp_a (x :: xa) (y :: ya) =
  (case comp_a x y of Eq => comparator_list comp_a xa ya | Lt => Lt | Gt => Gt)
  | comparator_list comp_a (x :: xa) [] = Gt
  | comparator_list comp_a [] (y :: ya) = Lt
  | comparator_list comp_a [] [] = Eq;

fun ccompare_lista A_ =
  (case ccompare A_ of NONE => NONE
    | SOME comp_a => SOME (comparator_list comp_a));

fun ccompare_list A_ = {ccompare = ccompare_lista A_} : ('a list) ccompare;

val mapping_impl_lista : (('a list), mapping_impla) phantom =
  Phantom Mapping_Choose;

val mapping_impl_list = {mapping_impl = mapping_impl_lista} :
  ('a list) mapping_impl;

fun cproper_interval_lista A_ xso yso = (raise Fail "undefined");

fun cproper_interval_list A_ =
  {ccompare_cproper_interval = ccompare_list A_,
    cproper_interval = cproper_interval_lista A_}
  : ('a list) cproper_interval;

datatype ('a, 'b) sum = Inl of 'a | Inr of 'b;

fun equal_suma A_ B_ (Inl x1) (Inr x2) = false
  | equal_suma A_ B_ (Inr x2) (Inl x1) = false
  | equal_suma A_ B_ (Inr x2) (Inr y2) = eq B_ x2 y2
  | equal_suma A_ B_ (Inl x1) (Inl y1) = eq A_ x1 y1;

fun equal_sum A_ B_ = {equal = equal_suma A_ B_} : ('a, 'b) sum equal;

fun less_eq_sum A_ B_ (Inl a) (Inl b) = less_eq A_ a b
  | less_eq_sum A_ B_ (Inl a) (Inr b) = true
  | less_eq_sum A_ B_ (Inr a) (Inl b) = false
  | less_eq_sum A_ B_ (Inr a) (Inr b) = less_eq B_ a b;

fun less_sum (A1_, A2_) (B1_, B2_) a b =
  less_eq_sum A2_ B2_ a b andalso not (equal_suma A1_ B1_ a b);

fun ord_sum (A1_, A2_) (B1_, B2_) =
  {less_eq = less_eq_sum A2_ B2_, less = less_sum (A1_, A2_) (B1_, B2_)} :
  ('a, 'b) sum ord;

fun preorder_sum (A1_, A2_) (B1_, B2_) =
  {ord_preorder =
     ord_sum (A1_, (ord_preorder o preorder_order o order_linorder) A2_)
       (B1_, (ord_preorder o preorder_order o order_linorder) B2_)}
  : ('a, 'b) sum preorder;

fun order_sum (A1_, A2_) (B1_, B2_) =
  {preorder_order = preorder_sum (A1_, A2_) (B1_, B2_)} : ('a, 'b) sum order;

fun equality_sum eq_a eq_b (Inr x) (Inr ya) = eq_b x ya
  | equality_sum eq_a eq_b (Inr x) (Inl y) = false
  | equality_sum eq_a eq_b (Inl x) (Inr ya) = false
  | equality_sum eq_a eq_b (Inl x) (Inl y) = eq_a x y;

fun ceq_suma A_ B_ =
  (case ceq A_ of NONE => NONE
    | SOME eq_a =>
      (case ceq B_ of NONE => NONE
        | SOME eq_b => SOME (equality_sum eq_a eq_b)));

fun ceq_sum A_ B_ = {ceq = ceq_suma A_ B_} : ('a, 'b) sum ceq;

fun set_impl_choose2 Set_Monada Set_Monada = Set_Monada
  | set_impl_choose2 Set_RBT Set_RBT = Set_RBT
  | set_impl_choose2 Set_DList Set_DList = Set_DList
  | set_impl_choose2 Set_Collect Set_Collect = Set_Collect
  | set_impl_choose2 x y = Set_Choose;

fun set_impl_suma A_ B_ =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl A_)) (of_phantom (set_impl B_)));

fun set_impl_sum A_ B_ = {set_impl = set_impl_suma A_ B_} :
  ('a, 'b) sum set_impl;

fun linorder_sum (A1_, A2_) (B1_, B2_) =
  {order_linorder = order_sum (A1_, A2_) (B1_, B2_)} : ('a, 'b) sum linorder;

fun finite_UNIV_suma A_ B_ =
  Phantom (of_phantom (finite_UNIV A_) andalso of_phantom (finite_UNIV B_));

fun card_UNIV_suma A_ B_ =
  Phantom
    let
      val ca = of_phantom (card_UNIVa A_);
      val cb = of_phantom (card_UNIVa B_);
    in
      (if not (equal_nata ca zero_nat) andalso not (equal_nata cb zero_nat)
        then plus_nat ca cb else zero_nat)
    end;

fun finite_UNIV_sum A_ B_ = {finite_UNIV = finite_UNIV_suma A_ B_} :
  ('a, 'b) sum finite_UNIV;

fun card_UNIV_sum A_ B_ =
  {finite_UNIV_card_UNIV =
     finite_UNIV_sum (finite_UNIV_card_UNIV A_) (finite_UNIV_card_UNIV B_),
    card_UNIVa = card_UNIV_suma A_ B_}
  : ('a, 'b) sum card_UNIV;

fun cEnum_sum A_ B_ =
  (case cEnum A_ of NONE => NONE
    | SOME (enum_a, (enum_all_a, enum_ex_a)) =>
      (case cEnum B_ of NONE => NONE
        | SOME (enum_b, (enum_all_b, enum_ex_b)) =>
          SOME (mapa Inl enum_a @ mapa Inr enum_b,
                 ((fn p =>
                    enum_all_a (fn x => p (Inl x)) andalso
                      enum_all_b (fn x => p (Inr x))),
                   (fn p =>
                     enum_ex_a (fn x => p (Inl x)) orelse
                       enum_ex_b (fn x => p (Inr x)))))));

fun cenum_sum A_ B_ = {cEnum = cEnum_sum A_ B_} : ('a, 'b) sum cenum;

fun comparator_sum comp_a comp_b (Inr x) (Inr ya) = comp_b x ya
  | comparator_sum comp_a comp_b (Inr x) (Inl y) = Gt
  | comparator_sum comp_a comp_b (Inl x) (Inr ya) = Lt
  | comparator_sum comp_a comp_b (Inl x) (Inl y) = comp_a x y;

fun ccompare_suma A_ B_ =
  (case ccompare A_ of NONE => NONE
    | SOME comp_a =>
      (case ccompare B_ of NONE => NONE
        | SOME comp_b => SOME (comparator_sum comp_a comp_b)));

fun ccompare_sum A_ B_ = {ccompare = ccompare_suma A_ B_} :
  ('a, 'b) sum ccompare;

fun mapping_impl_choose2 Mapping_RBT Mapping_RBT = Mapping_RBT
  | mapping_impl_choose2 Mapping_Assoc_List Mapping_Assoc_List =
    Mapping_Assoc_List
  | mapping_impl_choose2 Mapping_Mapping Mapping_Mapping = Mapping_Mapping
  | mapping_impl_choose2 x y = Mapping_Choose;

fun mapping_impl_suma A_ B_ =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl A_))
      (of_phantom (mapping_impl B_)));

fun mapping_impl_sum A_ B_ = {mapping_impl = mapping_impl_suma A_ B_} :
  ('a, 'b) sum mapping_impl;

fun cproper_interval_suma A_ B_ NONE NONE = true
  | cproper_interval_suma A_ B_ NONE (SOME (Inl x)) =
    cproper_interval A_ NONE (SOME x)
  | cproper_interval_suma A_ B_ NONE (SOME (Inr y)) = true
  | cproper_interval_suma A_ B_ (SOME (Inl x)) NONE = true
  | cproper_interval_suma A_ B_ (SOME (Inl x)) (SOME (Inl y)) =
    cproper_interval A_ (SOME x) (SOME y)
  | cproper_interval_suma A_ B_ (SOME (Inl x)) (SOME (Inr y)) =
    cproper_interval A_ (SOME x) NONE orelse cproper_interval B_ NONE (SOME y)
  | cproper_interval_suma A_ B_ (SOME (Inr y)) NONE =
    cproper_interval B_ (SOME y) NONE
  | cproper_interval_suma A_ B_ (SOME (Inr y)) (SOME (Inl x)) = false
  | cproper_interval_suma A_ B_ (SOME (Inr x)) (SOME (Inr y)) =
    cproper_interval B_ (SOME x) (SOME y);

fun cproper_interval_sum A_ B_ =
  {ccompare_cproper_interval =
     ccompare_sum (ccompare_cproper_interval A_) (ccompare_cproper_interval B_),
    cproper_interval = cproper_interval_suma A_ B_}
  : ('a, 'b) sum cproper_interval;

fun equality_option eq_a (SOME x) (SOME y) = eq_a x y
  | equality_option eq_a (SOME x) NONE = false
  | equality_option eq_a NONE (SOME y) = false
  | equality_option eq_a NONE NONE = true;

fun ceq_optiona A_ =
  (case ceq A_ of NONE => NONE | SOME eq_a => SOME (equality_option eq_a));

fun ceq_option A_ = {ceq = ceq_optiona A_} : ('a option) ceq;

fun set_impl_optiona A_ = Phantom (of_phantom (set_impl A_));

fun set_impl_option A_ = {set_impl = set_impl_optiona A_} :
  ('a option) set_impl;

fun comparator_option comp_a (SOME x) (SOME y) = comp_a x y
  | comparator_option comp_a (SOME x) NONE = Gt
  | comparator_option comp_a NONE (SOME y) = Lt
  | comparator_option comp_a NONE NONE = Eq;

fun ccompare_optiona A_ =
  (case ccompare A_ of NONE => NONE
    | SOME comp_a => SOME (comparator_option comp_a));

fun ccompare_option A_ = {ccompare = ccompare_optiona A_} :
  ('a option) ccompare;

fun equal_proda A_ B_ (x1, x2) (y1, y2) = eq A_ x1 y1 andalso eq B_ x2 y2;

fun equal_prod A_ B_ = {equal = equal_proda A_ B_} : ('a * 'b) equal;

fun less_eq_prod A_ B_ (x1, y1) (x2, y2) =
  less A_ x1 x2 orelse less_eq A_ x1 x2 andalso less_eq B_ y1 y2;

fun less_prod A_ B_ (x1, y1) (x2, y2) =
  less A_ x1 x2 orelse less_eq A_ x1 x2 andalso less B_ y1 y2;

fun ord_prod A_ B_ = {less_eq = less_eq_prod A_ B_, less = less_prod A_ B_} :
  ('a * 'b) ord;

fun preorder_prod A_ B_ =
  {ord_preorder = ord_prod (ord_preorder A_) (ord_preorder B_)} :
  ('a * 'b) preorder;

fun order_prod A_ B_ =
  {preorder_order = preorder_prod (preorder_order A_) (preorder_order B_)} :
  ('a * 'b) order;

fun set_impl_proda A_ B_ =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl A_)) (of_phantom (set_impl B_)));

fun set_impl_prod A_ B_ = {set_impl = set_impl_proda A_ B_} :
  ('a * 'b) set_impl;

fun linorder_prod A_ B_ =
  {order_linorder = order_prod (order_linorder A_) (order_linorder B_)} :
  ('a * 'b) linorder;

fun finite_UNIV_proda A_ B_ =
  Phantom (of_phantom (finite_UNIV A_) andalso of_phantom (finite_UNIV B_));

fun card_UNIV_proda A_ B_ =
  Phantom
    (times_nata (of_phantom (card_UNIVa A_)) (of_phantom (card_UNIVa B_)));

fun finite_UNIV_prod A_ B_ = {finite_UNIV = finite_UNIV_proda A_ B_} :
  ('a * 'b) finite_UNIV;

fun card_UNIV_prod A_ B_ =
  {finite_UNIV_card_UNIV =
     finite_UNIV_prod (finite_UNIV_card_UNIV A_) (finite_UNIV_card_UNIV B_),
    card_UNIVa = card_UNIV_proda A_ B_}
  : ('a * 'b) card_UNIV;

fun mapping_impl_proda A_ B_ =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl A_))
      (of_phantom (mapping_impl B_)));

fun mapping_impl_prod A_ B_ = {mapping_impl = mapping_impl_proda A_ B_} :
  ('a * 'b) mapping_impl;

fun cproper_interval_proda A_ B_ NONE NONE = true
  | cproper_interval_proda A_ B_ NONE (SOME (y1, y2)) =
    cproper_interval A_ NONE (SOME y1) orelse cproper_interval B_ NONE (SOME y2)
  | cproper_interval_proda A_ B_ (SOME (x1, x2)) NONE =
    cproper_interval A_ (SOME x1) NONE orelse cproper_interval B_ (SOME x2) NONE
  | cproper_interval_proda A_ B_ (SOME (x1, x2)) (SOME (y1, y2)) =
    cproper_interval A_ (SOME x1) (SOME y1) orelse
      (lt_of_comp (the (ccompare (ccompare_cproper_interval A_))) x1 y1 andalso
         (cproper_interval B_ (SOME x2) NONE orelse
           cproper_interval B_ NONE (SOME y2)) orelse
        not (lt_of_comp (the (ccompare (ccompare_cproper_interval A_))) y1
              x1) andalso
          cproper_interval B_ (SOME x2) (SOME y2));

fun cproper_interval_prod A_ B_ =
  {ccompare_cproper_interval =
     ccompare_prod (ccompare_cproper_interval A_)
       (ccompare_cproper_interval B_),
    cproper_interval = cproper_interval_proda A_ B_}
  : ('a * 'b) cproper_interval;

val equal_integer = {equal = (fn a => fn b => ((a : IntInf.int) = b))} :
  IntInf.int equal;

val preorder_integer = {ord_preorder = ord_integer} : IntInf.int preorder;

val order_integer = {preorder_order = preorder_integer} : IntInf.int order;

val ceq_integera : (IntInf.int -> IntInf.int -> bool) option =
  SOME (fn a => fn b => ((a : IntInf.int) = b));

val ceq_integer = {ceq = ceq_integera} : IntInf.int ceq;

val set_impl_integera : (IntInf.int, set_impla) phantom = Phantom Set_RBT;

val set_impl_integer = {set_impl = set_impl_integera} : IntInf.int set_impl;

val linorder_integer = {order_linorder = order_integer} : IntInf.int linorder;

val finite_UNIV_integera : (IntInf.int, bool) phantom = Phantom false;

val card_UNIV_integera : (IntInf.int, nat) phantom = Phantom zero_nat;

val finite_UNIV_integer = {finite_UNIV = finite_UNIV_integera} :
  IntInf.int finite_UNIV;

val card_UNIV_integer =
  {finite_UNIV_card_UNIV = finite_UNIV_integer, card_UNIVa = card_UNIV_integera}
  : IntInf.int card_UNIV;

val cEnum_integer :
  (IntInf.int list *
    (((IntInf.int -> bool) -> bool) * ((IntInf.int -> bool) -> bool))) option
  = NONE;

val cenum_integer = {cEnum = cEnum_integer} : IntInf.int cenum;

fun compare_integer x = comparator_of (equal_integer, linorder_integer) x;

val ccompare_integera : (IntInf.int -> IntInf.int -> ordera) option =
  SOME compare_integer;

val ccompare_integer = {ccompare = ccompare_integera} : IntInf.int ccompare;

val mapping_impl_integera : (IntInf.int, mapping_impla) phantom =
  Phantom Mapping_RBT;

val mapping_impl_integer = {mapping_impl = mapping_impl_integera} :
  IntInf.int mapping_impl;

fun proper_interval_integer xo NONE = true
  | proper_interval_integer NONE yo = true
  | proper_interval_integer (SOME x) (SOME y) =
    IntInf.< ((1 : IntInf.int), IntInf.- (y, x));

fun cproper_interval_integera x = proper_interval_integer x;

val cproper_interval_integer =
  {ccompare_cproper_interval = ccompare_integer,
    cproper_interval = cproper_interval_integera}
  : IntInf.int cproper_interval;

val infinite_UNIV_integer = {} : IntInf.int infinite_UNIV;

datatype int = Int_of_integer of IntInf.int;

datatype ('a, 'b, 'c, 'd) test_suite =
  Test_Suite of
    ('a * ('a, 'b, 'c) fsm) set * ('a -> (('a * ('b * ('c * 'a))) list) set) *
      ('a * ('a * ('b * ('c * 'a))) list -> 'a set) *
      ('a * 'a -> (('d, 'b, 'c) fsm * ('d * 'd)) set);

datatype ('b, 'a) comp_fun_idem = Abs_comp_fun_idem of ('b -> 'a -> 'a);

datatype 'a prefix_tree = MPT of ('a, 'a prefix_tree) mapping;

datatype 'a mp_trie = MP_Trie of ('a * 'a mp_trie) list;

fun impl_of (Alist x) = x;

fun update A_ k v [] = [(k, v)]
  | update A_ k v (p :: ps) =
    (if eq A_ (fst p) k then (k, v) :: ps else p :: update A_ k v ps);

fun updatea A_ xc xd xe = Alist (update A_ xc xd (impl_of xe));

fun fun_upd A_ f a b = (fn x => (if eq A_ x a then b else f x));

fun updateb (A1_, A2_) k v (RBT_Mapping t) =
  (case ccompare A1_
    of NONE =>
      (raise Fail "update RBT_Mapping: ccompare = None")
        (fn _ => updateb (A1_, A2_) k v (RBT_Mapping t))
    | SOME _ => RBT_Mapping (insertc A1_ k v t))
  | updateb (A1_, A2_) k v (Assoc_List_Mapping al) =
    Assoc_List_Mapping (updatea A2_ k v al)
  | updateb (A1_, A2_) k v (Mapping m) = Mapping (fun_upd A2_ m k (SOME v));

fun map_of A_ ((l, v) :: ps) k = (if eq A_ l k then SOME v else map_of A_ ps k)
  | map_of A_ [] k = NONE;

fun lookup A_ xa = map_of A_ (impl_of xa);

fun lookupa (A1_, A2_) (RBT_Mapping t) = lookupb A1_ t
  | lookupa (A1_, A2_) (Assoc_List_Mapping al) = lookup A2_ al;

fun foldb A_ x xc = folda (fn a => fn _ => x a) (impl_ofa A_ xc);

val empty : ('a, 'b) alist = Alist [];

fun mapping_empty_choose A_ =
  (case ccompare A_ of NONE => Assoc_List_Mapping empty
    | SOME _ => RBT_Mapping (emptyd A_));

fun mapping_empty A_ Mapping_RBT = RBT_Mapping (emptyd A_)
  | mapping_empty A_ Mapping_Assoc_List = Assoc_List_Mapping empty
  | mapping_empty A_ Mapping_Mapping = Mapping (fn _ => NONE)
  | mapping_empty A_ Mapping_Choose = mapping_empty_choose A_;

fun emptya (A1_, A2_) = mapping_empty A1_ (of_phantom (mapping_impl A2_));

fun set_as_map_image A_ B_ (C1_, C2_, C3_) (D1_, D2_, D3_) (RBT_set t) f1 =
  (case ccompare_proda A_ B_
    of NONE =>
      (raise Fail "set_as_map_image RBT_set: ccompare = None")
        (fn _ =>
          set_as_map_image A_ B_ (C1_, C2_, C3_) (D1_, D2_, D3_) (RBT_set t) f1)
    | SOME _ =>
      lookupa (C1_, C2_)
        (foldb (ccompare_prod A_ B_)
          (fn kv => fn m1 =>
            let
              val (x, z) = f1 kv;
            in
              (case lookupa (C1_, C2_) m1 x
                of NONE =>
                  updateb (C1_, C2_) x
                    (insert (D1_, D2_) z (bot_set (D1_, D2_, D3_))) m1
                | SOME zs => updateb (C1_, C2_) x (insert (D1_, D2_) z zs) m1)
            end)
          t (emptya (C1_, C3_))));

fun h (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m (q, x) =
  let
    val ma =
      set_as_map_image A2_ (ccompare_prod B1_ (ccompare_prod C2_ A2_))
        (ccompare_prod A2_ B1_, equal_prod A3_ B2_, mapping_impl_prod A4_ B3_)
        (ceq_prod C1_ A1_, ccompare_prod C2_ A2_, set_impl_prod C3_ A5_)
        (transitions m) (fn (qb, (xa, (y, qa))) => ((qb, xa), (y, qa)));
  in
    (case ma (q, x)
      of NONE =>
        bot_set (ceq_prod C1_ A1_, ccompare_prod C2_ A2_, set_impl_prod C3_ A5_)
      | SOME yqs => yqs)
  end;

fun dlist_ex A_ x xc = list_ex x (list_of_dlist A_ xc);

fun rBT_Impl_rbt_ex p (Branch (c, l, k, v, r)) =
  p k v orelse (rBT_Impl_rbt_ex p l orelse rBT_Impl_rbt_ex p r)
  | rBT_Impl_rbt_ex p Empty = false;

fun ex A_ xb xc = rBT_Impl_rbt_ex xb (impl_ofa A_ xc);

fun bex (A1_, A2_) (RBT_set rbt) p =
  (case ccompare A2_
    of NONE =>
      (raise Fail "Bex RBT_set: ccompare = None")
        (fn _ => bex (A1_, A2_) (RBT_set rbt) p)
    | SOME _ => ex A2_ (fn k => fn _ => p k) rbt)
  | bex (A1_, A2_) (DList_set dxs) p =
    (case ceq A1_
      of NONE =>
        (raise Fail "Bex DList_set: ceq = None")
          (fn _ => bex (A1_, A2_) (DList_set dxs) p)
      | SOME _ => dlist_ex A1_ p dxs)
  | bex (A1_, A2_) (Set_Monad xs) p = list_ex p xs;

fun size (A1_, A2_, A3_) m = card (A1_, A2_, A3_) (states m);

fun nth (x :: xs) n =
  (if equal_nata n zero_nat then x else nth xs (minus_nat n one_nata));

fun rev xs = fold (fn a => fn b => a :: b) xs [];

fun upt i j = (if less_nat i j then i :: upt (suc i) j else []);

fun zip (x :: xs) (y :: ys) = (x, y) :: zip xs ys
  | zip xs [] = []
  | zip [] ys = [];

fun rBT_Impl_rbt_all p (Branch (c, l, k, v, r)) =
  p k v andalso (rBT_Impl_rbt_all p l andalso rBT_Impl_rbt_all p r)
  | rBT_Impl_rbt_all p Empty = true;

fun all A_ xb xc = rBT_Impl_rbt_all xb (impl_ofa A_ xc);

fun ball (A1_, A2_) (RBT_set rbt) p =
  (case ccompare A2_
    of NONE =>
      (raise Fail "Ball RBT_set: ccompare = None")
        (fn _ => ball (A1_, A2_) (RBT_set rbt) p)
    | SOME _ => all A2_ (fn k => fn _ => p k) rbt)
  | ball (A1_, A2_) (DList_set dxs) p =
    (case ceq A1_
      of NONE =>
        (raise Fail "Ball DList_set: ceq = None")
          (fn _ => ball (A1_, A2_) (DList_set dxs) p)
      | SOME _ => dlist_all A1_ p dxs)
  | ball (A1_, A2_) (Set_Monad xs) p = list_all p xs;

fun h_obs_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x7;

fun h_obs_wp x =
  h_obs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

fun h_obsa (A1_, A2_) (B1_, B2_) (C1_, C2_) (FSMWP m) q x y =
  (case lookupa (ccompare_prod A1_ B1_, equal_prod A2_ B2_) (h_obs_wp m) (q, x)
    of NONE => NONE | SOME ma => lookupa (C1_, C2_) ma y);

fun h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) x =
  h_obsa (A1_, A2_) (B1_, B2_) (C1_, C2_) (fsm_impl_of_fsm x);

fun after (A1_, A2_) (B1_, B2_) (C1_, C2_) m q [] = q
  | after (A1_, A2_) (B1_, B2_) (C1_, C2_) m q ((x, y) :: io) =
    after (A1_, A2_) (B1_, B2_) (C1_, C2_) m
      (the (h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q x y)) io;

fun drop n [] = []
  | drop n (x :: xs) =
    (if equal_nata n zero_nat then x :: xs else drop (minus_nat n one_nata) xs);

fun find uu [] = NONE
  | find p (x :: xs) = (if p x then SOME x else find p xs);

fun last (x :: xs) = (if null xs then x else last xs);

fun maps f [] = []
  | maps f (x :: xs) = f x @ maps f xs;

fun take n [] = []
  | take n (x :: xs) =
    (if equal_nata n zero_nat then [] else x :: take (minus_nat n one_nata) xs);

fun image (A1_, A2_) (B1_, B2_, B3_) h (RBT_set rbt) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "image RBT_set: ccompare = None")
        (fn _ => image (A1_, A2_) (B1_, B2_, B3_) h (RBT_set rbt))
    | SOME _ => foldb A2_ (insert (B1_, B2_) o h) rbt (bot_set (B1_, B2_, B3_)))
  | image (A1_, A2_) (B1_, B2_, B3_) g (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "image DList_set: ceq = None")
          (fn _ => image (A1_, A2_) (B1_, B2_, B3_) g (DList_set dxs))
      | SOME _ =>
        foldc A1_ (insert (B1_, B2_) o g) dxs (bot_set (B1_, B2_, B3_)))
  | image (A1_, A2_) (B1_, B2_, B3_) f (Complement (Complement b)) =
    image (A1_, A2_) (B1_, B2_, B3_) f b
  | image (A1_, A2_) (B1_, B2_, B3_) f (Collect_set a) =
    (raise Fail "image Collect_set")
      (fn _ => image (A1_, A2_) (B1_, B2_, B3_) f (Collect_set a))
  | image (A1_, A2_) (B1_, B2_, B3_) f (Set_Monad xs) = Set_Monad (mapa f xs);

fun set_as_map (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (DList_set xs) =
  (case ceq_proda A1_ B1_
    of NONE =>
      (raise Fail "set_as_map RBT_set: ccompare = None")
        (fn _ => set_as_map (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (DList_set xs))
    | SOME _ =>
      lookupa (A2_, A3_)
        (foldc (ceq_prod A1_ B1_)
          (fn (x, z) => fn m =>
            (case lookupa (A2_, A3_) m x
              of NONE =>
                updateb (A2_, A3_) x
                  (insert (B1_, B2_) z (bot_set (B1_, B2_, B3_))) m
              | SOME zs => updateb (A2_, A3_) x (insert (B1_, B2_) z zs) m))
          xs (emptya (A2_, A4_))))
  | set_as_map (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (RBT_set t) =
    (case ccompare_proda A2_ B2_
      of NONE =>
        (raise Fail "set_as_map RBT_set: ccompare = None")
          (fn _ => set_as_map (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (RBT_set t))
      | SOME _ =>
        lookupa (A2_, A3_)
          (foldb (ccompare_prod A2_ B2_)
            (fn (x, z) => fn m =>
              (case lookupa (A2_, A3_) m x
                of NONE =>
                  updateb (A2_, A3_) x
                    (insert (B1_, B2_) z (bot_set (B1_, B2_, B3_))) m
                | SOME zs => updateb (A2_, A3_) x (insert (B1_, B2_) z zs) m))
            t (emptya (A2_, A4_))));

fun h_from (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m q =
  let
    val ma =
      set_as_map (A1_, A2_, A3_, A4_)
        (ceq_prod B1_ (ceq_prod C1_ A1_),
          ccompare_prod B2_ (ccompare_prod C2_ A2_),
          set_impl_prod B3_ (set_impl_prod C3_ A5_))
        (transitions m);
  in
    (case ma q
      of NONE =>
        bot_set
          (ceq_prod B1_ (ceq_prod C1_ A1_),
            ccompare_prod B2_ (ccompare_prod C2_ A2_),
            set_impl_prod B3_ (set_impl_prod C3_ A5_))
      | SOME yqs => yqs)
  end;

fun targeta q [] = q
  | targeta q (v :: va) = snd (snd (snd (last (v :: va))));

fun target q p = targeta q p;

fun foldr f [] = id
  | foldr f (x :: xs) = f x o foldr f xs;

fun filter (A1_, A2_) p a = inf_seta (A1_, A2_) a (Collect_set p);

fun apsnd f (x, y) = (x, f y);

fun divmod_integer k l =
  (if ((k : IntInf.int) = (0 : IntInf.int))
    then ((0 : IntInf.int), (0 : IntInf.int))
    else (if IntInf.< ((0 : IntInf.int), l)
           then (if IntInf.< ((0 : IntInf.int), k)
                  then IntInf.divMod (IntInf.abs k, IntInf.abs l)
                  else let
                         val (r, s) =
                           IntInf.divMod (IntInf.abs k, IntInf.abs l);
                       in
                         (if ((s : IntInf.int) = (0 : IntInf.int))
                           then (IntInf.~ r, (0 : IntInf.int))
                           else (IntInf.- (IntInf.~ r, (1 : IntInf.int)),
                                  IntInf.- (l, s)))
                       end)
           else (if ((l : IntInf.int) = (0 : IntInf.int))
                  then ((0 : IntInf.int), k)
                  else apsnd IntInf.~
                         (if IntInf.< (k, (0 : IntInf.int))
                           then IntInf.divMod (IntInf.abs k, IntInf.abs l)
                           else let
                                  val (r, s) =
                                    IntInf.divMod (IntInf.abs k, IntInf.abs l);
                                in
                                  (if ((s : IntInf.int) = (0 : IntInf.int))
                                    then (IntInf.~ r, (0 : IntInf.int))
                                    else (IntInf.- (IntInf.~
              r, (1 : IntInf.int)),
   IntInf.- (IntInf.~ l, s)))
                                end))));

fun divide_integer k l = fst (divmod_integer k l);

fun divide_nat m n = Nat (divide_integer (integer_of_nat m) (integer_of_nat n));

fun part B_ f pivot (x :: xs) =
  let
    val (lts, (eqs, gts)) = part B_ f pivot xs;
    val xa = f x;
  in
    (if less ((ord_preorder o preorder_order o order_linorder) B_) xa pivot
      then (x :: lts, (eqs, gts))
      else (if less ((ord_preorder o preorder_order o order_linorder) B_) pivot
                 xa
             then (lts, (eqs, x :: gts)) else (lts, (x :: eqs, gts))))
  end
  | part B_ f pivot [] = ([], ([], []));

fun sort_key B_ f xs =
  (case xs of [] => [] | [_] => xs
    | [x, y] =>
      (if less_eq ((ord_preorder o preorder_order o order_linorder) B_) (f x)
            (f y)
        then xs else [y, x])
    | _ :: _ :: _ :: _ =>
      let
        val (lts, (eqs, gts)) =
          part B_ f
            (f (nth xs
                 (divide_nat (size_list xs) (nat_of_integer (2 : IntInf.int)))))
            xs;
      in
        sort_key B_ f lts @ eqs @ sort_key B_ f gts
      end);

fun membera A_ [] y = false
  | membera A_ (x :: xs) y = eq A_ x y orelse membera A_ xs y;

fun remdups A_ [] = []
  | remdups A_ (x :: xs) =
    (if membera A_ xs x then remdups A_ xs else x :: remdups A_ xs);

fun sorted_list_of_set (A1_, A2_, A3_, A4_) (RBT_set rbt) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "sorted_list_of_set RBT_set: ccompare = None")
        (fn _ => sorted_list_of_set (A1_, A2_, A3_, A4_) (RBT_set rbt))
    | SOME _ => sort_key A4_ (fn x => x) (keysb A2_ rbt))
  | sorted_list_of_set (A1_, A2_, A3_, A4_) (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "sorted_list_of_set DList_set: ceq = None")
          (fn _ => sorted_list_of_set (A1_, A2_, A3_, A4_) (DList_set dxs))
      | SOME _ => sort_key A4_ (fn x => x) (list_of_dlist A1_ dxs))
  | sorted_list_of_set (A1_, A2_, A3_, A4_) (Set_Monad xs) =
    sort_key A4_ (fn x => x) (remdups A3_ xs);

fun inputs_as_list (B1_, B2_, B3_, B4_) m =
  sorted_list_of_set (B1_, B2_, B3_, B4_) (inputs m);

fun fset_of_list (A1_, A2_, A3_) xa = Abs_fset (set (A1_, A2_, A3_) xa);

fun finputs (B1_, B2_, B3_, B4_, B5_) m =
  fset_of_list (B1_, B2_, B5_) (inputs_as_list (B1_, B2_, B3_, B4_) m);

fun states_as_list (A1_, A2_, A3_, A4_) m =
  sorted_list_of_set (A1_, A2_, A3_, A4_) (states m);

fun fstates (A1_, A2_, A3_, A4_, A5_) m =
  fset_of_list (A1_, A2_, A5_) (states_as_list (A1_, A2_, A3_, A4_) m);

fun fimage (B1_, B2_) (A1_, A2_, A3_) xb xc =
  Abs_fset (image (B1_, B2_) (A1_, A2_, A3_) xb (fset xc));

fun concat xss = foldr (fn a => fn b => a @ b) xss [];

fun map_add m1 m2 = (fn x => (case m2 x of NONE => m1 x | SOME a => SOME a));

fun outputs_as_list (C1_, C2_, C3_, C4_) m =
  sorted_list_of_set (C1_, C2_, C3_, C4_) (outputs m);

fun foutputs (C1_, C2_, C3_, C4_, C5_) m =
  fset_of_list (C1_, C2_, C5_) (outputs_as_list (C1_, C2_, C3_, C4_) m);

fun h_wpi (FSMWPI (x1, x2, x3, x4, x5, x6, x7)) = x6;

fun from_FSMI_impl (A1_, A2_) m q =
  (if member (A1_, A2_) q (states_wpi m)
    then FSMWPI
           (q, states_wpi m, inputs_wpi m, outputs_wpi m, transitions_wpi m,
             h_wpi m, h_obs_wpi m)
    else m);

fun from_FSMIa (A1_, A2_) xb xc =
  Fsm_with_precomputations
    (from_FSMI_impl (A1_, A2_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

fun from_FSMI (A1_, A2_) (FSMWP m) q = FSMWP (from_FSMIa (A1_, A2_) m q);

fun from_FSM (A1_, A2_) xb xc =
  Abs_fsm (from_FSMI (A1_, A2_) (fsm_impl_of_fsm xb) xc);

fun comp_fun_idem_apply (Abs_comp_fun_idem x) = x;

fun set_fold_cfi (A1_, A2_) f b (RBT_set rbt) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "set_fold_cfi RBT_set: ccompare = None")
        (fn _ => set_fold_cfi (A1_, A2_) f b (RBT_set rbt))
    | SOME _ => foldb A2_ (comp_fun_idem_apply f) rbt b)
  | set_fold_cfi (A1_, A2_) f b (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "set_fold_cfi DList_set: ceq = None")
          (fn _ => set_fold_cfi (A1_, A2_) f b (DList_set dxs))
      | SOME _ => foldc A1_ (comp_fun_idem_apply f) dxs b)
  | set_fold_cfi (A1_, A2_) f b (Set_Monad xs) =
    fold (comp_fun_idem_apply f) xs b
  | set_fold_cfi (A1_, A2_) f b (Collect_set p) =
    (raise Fail "set_fold_cfi not supported on Collect_set")
      (fn _ => set_fold_cfi (A1_, A2_) f b (Collect_set p))
  | set_fold_cfi (A1_, A2_) f b (Complement a) =
    (raise Fail "set_fold_cfi not supported on Complement")
      (fn _ => set_fold_cfi (A1_, A2_) f b (Complement a));

fun sup_cfi A_ =
  Abs_comp_fun_idem (sup ((sup_semilattice_sup o semilattice_sup_lattice) A_));

fun sup_setb (A1_, A2_, A3_, A4_, A5_) a =
  (if finite
        (finite_UNIV_set A1_, ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
          ccompare_set (A1_, A3_, A4_, A5_))
        a
    then set_fold_cfi
           (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
             ccompare_set (A1_, A3_, A4_, A5_))
           (sup_cfi (lattice_set (A2_, A3_, ccompare_cproper_interval A4_)))
           (bot_set (A3_, ccompare_cproper_interval A4_, A5_)) a
    else (raise Fail "Sup: infinite")
           (fn _ => sup_setb (A1_, A2_, A3_, A4_, A5_) a));

fun ffUnion (A1_, A2_, A3_, A4_, A5_, A6_) xa =
  Abs_fset
    (sup_setb (A1_, A2_, A3_, A4_, A6_)
      (image
        (ceq_fset (A2_, A3_, ccompare_cproper_interval A4_, A5_), ccompare_fset)
        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
          ccompare_set (A1_, A3_, A4_, A6_), set_impl_set)
        fset (fset xa)));

fun ffilter (A1_, A2_) xb xc = Abs_fset (filter (A1_, A2_) xb (fset xc));

fun finsert (A1_, A2_) xb xc = Abs_fset (insert (A1_, A2_) xb (fset xc));

fun fmember (A1_, A2_) x xc = member (A1_, A2_) x (fset xc);

fun butlast [] = []
  | butlast (x :: xs) = (if null xs then [] else x :: butlast xs);

fun hd (x21 :: x22) = x21;

fun remove1 A_ x [] = []
  | remove1 A_ x (y :: xs) = (if eq A_ x y then xs else y :: remove1 A_ x xs);

fun map_upds A_ m xs ys = map_add m (map_of A_ (rev (zip xs ys)));

fun map f Empty = Empty
  | map f (Branch (c, lt, k, v, rt)) = Branch (c, map f lt, k, f k v, map f rt);

fun mapb A_ xb xc = Mapping_RBTa (map xb (impl_ofa A_ xc));

fun keysc (A1_, A2_, A3_) xa = set (A1_, A2_, A3_) (mapa fst (impl_of xa));

fun keys (A1_, A2_, A3_, A4_) (RBT_Mapping t) =
  RBT_set (mapb A3_ (fn _ => fn _ => ()) t)
  | keys (A1_, A2_, A3_, A4_) (Assoc_List_Mapping al) = keysc (A2_, A3_, A4_) al
  | keys (A1_, A2_, A3_, A4_) (Mapping m) =
    collect A1_ (fn k => not (is_none (m k)));

fun the_elem (A1_, A2_) (RBT_set rbt) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "the_elem RBT_set: ccompare = None")
        (fn _ => the_elem (A1_, A2_) (RBT_set rbt))
    | SOME _ =>
      (case impl_ofa A2_ rbt
        of Empty =>
          (raise Fail "the_elem RBT_set: not unique")
            (fn _ => the_elem (A1_, A2_) (RBT_set rbt))
        | Branch (_, Empty, x, _, Empty) => x
        | Branch (_, Empty, _, _, Branch (_, _, _, _, _)) =>
          (raise Fail "the_elem RBT_set: not unique")
            (fn _ => the_elem (A1_, A2_) (RBT_set rbt))
        | Branch (_, Branch (_, _, _, _, _), _, _, _) =>
          (raise Fail "the_elem RBT_set: not unique")
            (fn _ => the_elem (A1_, A2_) (RBT_set rbt))))
  | the_elem (A1_, A2_) (DList_set dxs) =
    (case ceq A1_
      of NONE =>
        (raise Fail "the_elem DList_set: ceq = None")
          (fn _ => the_elem (A1_, A2_) (DList_set dxs))
      | SOME _ =>
        (case list_of_dlist A1_ dxs
          of [] =>
            (raise Fail "the_elem DList_set: not unique")
              (fn _ => the_elem (A1_, A2_) (DList_set dxs))
          | [x] => x
          | _ :: _ :: _ =>
            (raise Fail "the_elem DList_set: not unique")
              (fn _ => the_elem (A1_, A2_) (DList_set dxs))))
  | the_elem (A1_, A2_) (Set_Monad [x]) = x;

fun filterb xb xc = Alist (filtera xb (impl_of xc));

fun pow_list [] = [[]]
  | pow_list (x :: xs) = let
                           val pxs = pow_list xs;
                         in
                           pxs @ mapa (fn a => x :: a) pxs
                         end;

fun acyclic_paths_up_to_lengtha (A1_, A2_) (B1_, B2_) (C1_, C2_) prev q hF
  visitedStates k =
  (if equal_nata k zero_nat
    then insert
           (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
             ccompare_list
               (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
           prev
           (set_empty
             (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
               ccompare_list
                 (ccompare_prod A2_
                   (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
             (of_phantom set_impl_lista))
    else let
           val tF =
             filter
               (ceq_prod B1_ (ceq_prod C1_ A1_),
                 ccompare_prod B2_ (ccompare_prod C2_ A2_))
               (fn (_, (_, qa)) => not (member (A1_, A2_) qa visitedStates))
               (hF q);
         in
           insert
             (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
               ccompare_list
                 (ccompare_prod A2_
                   (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
             prev
             (sup_setb
               (finite_UNIV_list, cenum_list,
                 ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
                 cproper_interval_list
                   (ccompare_prod A2_
                     (ccompare_prod B2_ (ccompare_prod C2_ A2_))),
                 set_impl_list)
               (image
                 (ceq_prod B1_ (ceq_prod C1_ A1_),
                   ccompare_prod B2_ (ccompare_prod C2_ A2_))
                 (ceq_set
                    (cenum_list,
                      ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
                      ccompare_cproper_interval
                        (cproper_interval_list
                          (ccompare_prod A2_
                            (ccompare_prod B2_ (ccompare_prod C2_ A2_))))),
                   ccompare_set
                     (finite_UNIV_list,
                       ceq_list
                         (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
                       cproper_interval_list
                         (ccompare_prod A2_
                           (ccompare_prod B2_ (ccompare_prod C2_ A2_))),
                       set_impl_list),
                   set_impl_set)
                 (fn (x, (y, qa)) =>
                   acyclic_paths_up_to_lengtha (A1_, A2_) (B1_, B2_) (C1_, C2_)
                     (prev @ [(q, (x, (y, qa)))]) qa hF
                     (insert (A1_, A2_) qa visitedStates)
                     (minus_nat k one_nata))
                 tF))
         end);

fun acyclic_paths_up_to_length (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m q k =
  (if member (A1_, A2_) q (states m)
    then acyclic_paths_up_to_lengtha (A1_, A2_) (B1_, B2_) (C1_, C2_) [] q
           (fn x =>
             (case set_as_map (A1_, A2_, A3_, A4_)
                     (ceq_prod B1_ (ceq_prod C1_ A1_),
                       ccompare_prod B2_ (ccompare_prod C2_ A2_),
                       set_impl_prod B3_ (set_impl_prod C3_ A5_))
                     (transitions m) x
               of NONE =>
                 bot_set
                   (ceq_prod B1_ (ceq_prod C1_ A1_),
                     ccompare_prod B2_ (ccompare_prod C2_ A2_),
                     set_impl_prod B3_ (set_impl_prod C3_ A5_))
               | SOME xs => xs))
           (insert (A1_, A2_) q (bot_set (A1_, A2_, A5_))) k
    else set_empty
           (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
             ccompare_list
               (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
           (of_phantom set_impl_lista));

fun lS_acyclic (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_) (C1_, C2_, C3_) m
  q = image (ceq_list (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_))),
              ccompare_list
                (ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_))))
        (ceq_list (ceq_prod B1_ C1_), ccompare_list (ccompare_prod B2_ C2_),
          set_impl_list)
        (mapa (fn t => (fst (snd t), fst (snd (snd t)))))
        (acyclic_paths_up_to_length (A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_)
          (C1_, C2_, C3_) m q (minus_nat (size (A1_, A2_, A3_) m) one_nata));

fun observable (A1_, A2_, A3_) (B1_, B2_, B3_) (C1_, C2_, C3_) m =
  ball (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
         ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)))
    (transitions m)
    (fn t1 =>
      ball (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
             ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)))
        (transitions m)
        (fn t2 =>
          (if eq A3_ (fst t1) (fst t2) andalso
                (eq B3_ (fst (snd t1)) (fst (snd t2)) andalso
                  eq C3_ (fst (snd (snd t1))) (fst (snd (snd t2))))
            then eq A3_ (snd (snd (snd t1))) (snd (snd (snd t2))) else true)));

fun dropWhile p [] = []
  | dropWhile p (x :: xs) = (if p x then dropWhile p xs else x :: xs);

fun removeAll A_ x [] = []
  | removeAll A_ x (y :: xs) =
    (if eq A_ x y then removeAll A_ x xs else y :: removeAll A_ x xs);

fun filterc A_ p (RBT_Mapping t) =
  (case ccompare A_
    of NONE =>
      (raise Fail "filter RBT_Mapping: ccompare = None")
        (fn _ => filterc A_ p (RBT_Mapping t))
    | SOME _ => RBT_Mapping (filtere A_ (fn (a, b) => p a b) t))
  | filterc A_ p (Assoc_List_Mapping al) =
    Assoc_List_Mapping (filterb (fn (a, b) => p a b) al)
  | filterc A_ p (Mapping m) =
    Mapping
      (fn k =>
        (case m k of NONE => NONE
          | SOME v => (if p k v then SOME v else NONE)));

fun is_prefix A_ [] uu = true
  | is_prefix A_ (x :: xs) [] = false
  | is_prefix A_ (x :: xs) (y :: ys) = eq A_ x y andalso is_prefix A_ xs ys;

fun find_index f [] = NONE
  | find_index f (x :: xs) =
    (if f x then SOME zero_nat
      else (case find_index f xs of NONE => NONE | SOME k => SOME (suc k)));

fun transitions_as_list (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m =
  sorted_list_of_set
    (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
      ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)),
      equal_prod A3_ (equal_prod B3_ (equal_prod C3_ A3_)),
      linorder_prod A4_ (linorder_prod B4_ (linorder_prod C4_ A4_)))
    (transitions m);

fun ftransitions (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m =
  fset_of_list
    (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
      ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)),
      set_impl_prod A5_ (set_impl_prod B5_ (set_impl_prod C5_ A5_)))
    (transitions_as_list (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_) m);

fun assign_indices (A1_, A2_, A3_, A4_) xs =
  (fn x =>
    the (find_index (eq A3_ x) (sorted_list_of_set (A1_, A2_, A3_, A4_) xs)));

fun set_as_mapping_image (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_) (D1_, D2_, D3_)
  (DList_set xs) f2 =
  (case ceq_proda A1_ B1_
    of NONE =>
      (raise Fail "set_as_map_image DList_set: ccompare = None")
        (fn _ =>
          set_as_mapping_image (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_)
            (D1_, D2_, D3_) (DList_set xs) f2)
    | SOME _ =>
      foldc (ceq_prod A1_ B1_)
        (fn kv => fn m1 =>
          let
            val (x, z) = f2 kv;
          in
            (case lookupa (C1_, C2_) m1 x
              of NONE =>
                updateb (C1_, C2_) x
                  (insert (D1_, D2_) z (bot_set (D1_, D2_, D3_))) m1
              | SOME zs => updateb (C1_, C2_) x (insert (D1_, D2_) z zs) m1)
          end)
        xs (emptya (C1_, C3_)))
  | set_as_mapping_image (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_) (D1_, D2_, D3_)
    (RBT_set t) f1 =
    (case ccompare_proda A2_ B2_
      of NONE =>
        (raise Fail "set_as_map_image RBT_set: ccompare = None")
          (fn _ =>
            set_as_mapping_image (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_)
              (D1_, D2_, D3_) (RBT_set t) f1)
      | SOME _ =>
        foldb (ccompare_prod A2_ B2_)
          (fn kv => fn m1 =>
            let
              val (x, z) = f1 kv;
            in
              (case lookupa (C1_, C2_) m1 x
                of NONE =>
                  updateb (C1_, C2_) x
                    (insert (D1_, D2_) z (bot_set (D1_, D2_, D3_))) m1
                | SOME zs => updateb (C1_, C2_) x (insert (D1_, D2_) z zs) m1)
            end)
          t (emptya (C1_, C3_)));

fun set_as_mapping (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (DList_set xs) =
  (case ceq_proda A1_ B1_
    of NONE =>
      (raise Fail "set_as_map RBT_set: ccompare = None")
        (fn _ =>
          set_as_mapping (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (DList_set xs))
    | SOME _ =>
      foldc (ceq_prod A1_ B1_)
        (fn (x, z) => fn m =>
          (case lookupa (A2_, A3_) m x
            of NONE =>
              updateb (A2_, A3_) x
                (insert (B1_, B2_) z (bot_set (B1_, B2_, B3_))) m
            | SOME zs => updateb (A2_, A3_) x (insert (B1_, B2_) z zs) m))
        xs (emptya (A2_, A4_)))
  | set_as_mapping (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (RBT_set t) =
    (case ccompare_proda A2_ B2_
      of NONE =>
        (raise Fail "set_as_map RBT_set: ccompare = None")
          (fn _ =>
            set_as_mapping (A1_, A2_, A3_, A4_) (B1_, B2_, B3_) (RBT_set t))
      | SOME _ =>
        foldb (ccompare_prod A2_ B2_)
          (fn (x, z) => fn m =>
            (case lookupa (A2_, A3_) m x
              of NONE =>
                updateb (A2_, A3_) x
                  (insert (B1_, B2_) z (bot_set (B1_, B2_, B3_))) m
              | SOME zs => updateb (A2_, A3_) x (insert (B1_, B2_) z zs) m))
          t (emptya (A2_, A4_)));

fun map_valuesa xb xc = Alist (mapa (fn (x, y) => (x, xb x y)) (impl_of xc));

fun map_values A_ f (RBT_Mapping t) =
  (case ccompare A_
    of NONE =>
      (raise Fail "map_values RBT_Mapping: ccompare = None")
        (fn _ => map_values A_ f (RBT_Mapping t))
    | SOME _ => RBT_Mapping (mapb A_ f t))
  | map_values A_ f (Assoc_List_Mapping al) =
    Assoc_List_Mapping (map_valuesa f al)
  | map_values A_ f (Mapping m) = Mapping (fn k => map_option (f k) (m k));

fun h_obs_impl_from_h (A1_, A2_, A3_, A4_) B_ (C1_, C2_, C3_, C4_) h =
  map_values (ccompare_prod A3_ B_)
    (fn _ => fn yqs =>
      let
        val m = set_as_mapping (C1_, C2_, C3_, C4_) (A2_, A3_, A4_) yqs;
        val ma =
          filterc C2_
            (fn _ => fn qs => equal_nata (card (A1_, A2_, A3_) qs) one_nata) m;
        val mb = map_values C2_ (fn _ => the_elem (A2_, A3_)) ma;
      in
        mb
      end)
    h;

fun rename_states_impl (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) (D1_, D2_, D3_, D4_, D5_, D6_) m f =
  let
    val ts =
      image (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
              ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)))
        (ceq_prod D2_ (ceq_prod B1_ (ceq_prod C1_ D2_)),
          ccompare_prod D3_ (ccompare_prod B2_ (ccompare_prod C2_ D3_)),
          set_impl_prod D6_ (set_impl_prod B5_ (set_impl_prod C5_ D6_)))
        (fn t =>
          (f (fst t),
            (fst (snd t), (fst (snd (snd t)), f (snd (snd (snd t)))))))
        (transitions_wpi m);
    val h =
      set_as_mapping_image (D2_, D3_)
        (ceq_prod B1_ (ceq_prod C1_ D2_),
          ccompare_prod B2_ (ccompare_prod C2_ D3_))
        (ccompare_prod D3_ B2_, equal_prod D4_ B3_, mapping_impl_prod D5_ B4_)
        (ceq_prod C1_ D2_, ccompare_prod C2_ D3_, set_impl_prod C5_ D6_) ts
        (fn (q, (x, (y, qa))) => ((q, x), (y, qa)));
  in
    FSMWPI
      (f (initial_wpi m), image (A1_, A2_) (D2_, D3_, D6_) f (states_wpi m),
        inputs_wpi m, outputs_wpi m, ts, h,
        h_obs_impl_from_h (D1_, D2_, D3_, D6_) B2_ (C1_, C2_, C3_, C4_) h)
  end;

fun rename_statesb (D1_, D2_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) (A1_, A2_, A3_, A4_, A5_, A6_) xb xc =
  Fsm_with_precomputations
    (rename_states_impl (D1_, D2_) (B1_, B2_, B3_, B4_, B5_)
      (C1_, C2_, C3_, C4_, C5_) (A1_, A2_, A3_, A4_, A5_, A6_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

fun rename_statesa (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) (D1_, D2_, D3_, D4_, D5_, D6_) (FSMWP m) f =
  FSMWP (rename_statesb (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
          (C1_, C2_, C3_, C4_, C5_) (D1_, D2_, D3_, D4_, D5_, D6_) m f);

fun rename_states (D1_, D2_) (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
  (A1_, A2_, A3_, A4_, A5_, A6_) xb xc =
  Abs_fsm
    (rename_statesa (D1_, D2_) (B1_, B2_, B3_, B4_, B5_)
      (C1_, C2_, C3_, C4_, C5_) (A1_, A2_, A3_, A4_, A5_, A6_)
      (fsm_impl_of_fsm xb) xc);

fun index_states (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m =
  rename_states (A1_, A2_) (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
    (card_UNIV_nat, ceq_nat, ccompare_nat, equal_nat, mapping_impl_nat,
      set_impl_nat)
    m (assign_indices (A1_, A2_, A3_, A4_) (states m));

fun paths_for_ioa (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_) f [] q prev =
  insert
    (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
      ccompare_list
        (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
    prev
    (set_empty
      (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
        ccompare_list
          (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
      (of_phantom set_impl_lista))
  | paths_for_ioa (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_) f ((x, y) :: io) q prev
    = sup_setb
        (finite_UNIV_list, cenum_list,
          ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
          cproper_interval_list
            (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))),
          set_impl_list)
        (image (ceq_prod C1_ A1_, ccompare_prod C2_ A2_)
          (ceq_set
             (cenum_list,
               ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
               ccompare_cproper_interval
                 (cproper_interval_list
                   (ccompare_prod A2_
                     (ccompare_prod B2_ (ccompare_prod C2_ A2_))))),
            ccompare_set
              (finite_UNIV_list,
                ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
                cproper_interval_list
                  (ccompare_prod A2_
                    (ccompare_prod B2_ (ccompare_prod C2_ A2_))),
                set_impl_list),
            set_impl_set)
          (fn yq =>
            paths_for_ioa (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_) f io (snd yq)
              (prev @ [(q, (x, (y, snd yq)))]))
          (filter (ceq_prod C1_ A1_, ccompare_prod C2_ A2_)
            (fn yq => eq C3_ (fst yq) y) (f (q, x))));

fun paths_for_io (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m q io =
  (if member (A1_, A2_) q (states m)
    then paths_for_ioa (A1_, A2_) (B1_, B2_) (C1_, C2_, C3_)
           (h (A1_, A2_, A3_, A4_, A5_) (B2_, B3_, B4_) (C1_, C2_, C4_) m) io q
           []
    else set_empty
           (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
             ccompare_list
               (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
           (of_phantom set_impl_lista));

fun productc A_ B_ dxs1 dxs2 =
  Abs_dlist
    (foldc A_ (fn a => foldc B_ (fn c => (fn b => (a, c) :: b)) dxs2) dxs1 []);

fun rbt_product f rbt1 rbt2 =
  rbtreeify
    (rev (folda
           (fn a => fn b =>
             folda (fn c => fn d => (fn e => ((a, c), f a b c d) :: e)) rbt2)
           rbt1 []));

fun productf A_ B_ xc xd xe =
  Mapping_RBTa (rbt_product xc (impl_ofa A_ xd) (impl_ofa B_ xe));

fun productb A_ B_ rbt1 rbt2 =
  productf A_ B_ (fn _ => fn _ => fn _ => fn _ => ()) rbt1 rbt2;

fun producte (A1_, A2_, A3_) (B1_, B2_, B3_) (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare A2_
    of NONE =>
      (raise Fail "product RBT_set RBT_set: ccompare1 = None")
        (fn _ =>
          producte (A1_, A2_, A3_) (B1_, B2_, B3_) (RBT_set rbt1)
            (RBT_set rbt2))
    | SOME _ =>
      (case ccompare B2_
        of NONE =>
          (raise Fail "product RBT_set RBT_set: ccompare2 = None")
            (fn _ =>
              producte (A1_, A2_, A3_) (B1_, B2_, B3_) (RBT_set rbt1)
                (RBT_set rbt2))
        | SOME _ => RBT_set (productb A2_ B2_ rbt1 rbt2)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) a2 (RBT_set rbt2) =
    (case ccompare B2_
      of NONE =>
        (raise Fail "product RBT_set: ccompare2 = None")
          (fn _ => producte (A1_, A2_, A3_) (B1_, B2_, B3_) a2 (RBT_set rbt2))
      | SOME _ =>
        foldb B2_
          (fn y =>
            sup_seta (ceq_prod A1_ B1_, ccompare_prod A2_ B2_)
              (image (A1_, A2_)
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)
                (fn x => (x, y)) a2))
          rbt2
          (bot_set
            (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) (RBT_set rbt1) b2 =
    (case ccompare A2_
      of NONE =>
        (raise Fail "product RBT_set: ccompare1 = None")
          (fn _ => producte (A1_, A2_, A3_) (B1_, B2_, B3_) (RBT_set rbt1) b2)
      | SOME _ =>
        foldb A2_
          (fn x =>
            sup_seta (ceq_prod A1_ B1_, ccompare_prod A2_ B2_)
              (image (B1_, B2_)
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)
                (fn a => (x, a)) b2))
          rbt1
          (bot_set
            (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) (DList_set dxs) (DList_set dys) =
    (case ceq A1_
      of NONE =>
        (raise Fail "product DList_set DList_set: ceq1 = None")
          (fn _ =>
            producte (A1_, A2_, A3_) (B1_, B2_, B3_) (DList_set dxs)
              (DList_set dys))
      | SOME _ =>
        (case ceq B1_
          of NONE =>
            (raise Fail "product DList_set DList_set: ceq2 = None")
              (fn _ =>
                producte (A1_, A2_, A3_) (B1_, B2_, B3_) (DList_set dxs)
                  (DList_set dys))
          | SOME _ => DList_set (productc A1_ B1_ dxs dys)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) a1 (DList_set dys) =
    (case ceq B1_
      of NONE =>
        (raise Fail "product DList_set2: ceq = None")
          (fn _ => producte (A1_, A2_, A3_) (B1_, B2_, B3_) a1 (DList_set dys))
      | SOME _ =>
        foldc B1_
          (fn y =>
            sup_seta (ceq_prod A1_ B1_, ccompare_prod A2_ B2_)
              (image (A1_, A2_)
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)
                (fn x => (x, y)) a1))
          dys (bot_set
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_,
                  set_impl_prod A3_ B3_)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) (DList_set dxs) b1 =
    (case ceq A1_
      of NONE =>
        (raise Fail "product DList_set1: ceq = None")
          (fn _ => producte (A1_, A2_, A3_) (B1_, B2_, B3_) (DList_set dxs) b1)
      | SOME _ =>
        foldc A1_
          (fn x =>
            sup_seta (ceq_prod A1_ B1_, ccompare_prod A2_ B2_)
              (image (B1_, B2_)
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_, set_impl_prod A3_ B3_)
                (fn a => (x, a)) b1))
          dxs (bot_set
                (ceq_prod A1_ B1_, ccompare_prod A2_ B2_,
                  set_impl_prod A3_ B3_)))
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) (Set_Monad xs) (Set_Monad ys) =
    Set_Monad (fold (fn x => fold (fn y => (fn a => (x, y) :: a)) ys) xs [])
  | producte (A1_, A2_, A3_) (B1_, B2_, B3_) a b =
    Collect_set
      (fn (x, y) => member (A1_, A2_) x a andalso member (B1_, B2_) y b);

fun product_impl (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  (D1_, D2_, D3_, D4_, D5_, D6_, D7_) a b =
  let
    val ts =
      image (ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
               (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
              ccompare_prod
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_prod (ccompare_cproper_interval C4_)
                      (ccompare_cproper_interval A4_))))
                (ccompare_prod (ccompare_cproper_interval D4_)
                  (ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_prod (ccompare_cproper_interval C4_)
                      (ccompare_cproper_interval D4_)))))
        (ceq_prod (ceq_prod A3_ D3_)
           (ceq_prod B3_ (ceq_prod C3_ (ceq_prod A3_ D3_))),
          ccompare_prod
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval D4_))
            (ccompare_prod (ccompare_cproper_interval B4_)
              (ccompare_prod (ccompare_cproper_interval C4_)
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval D4_)))),
          set_impl_prod (set_impl_prod A7_ D7_)
            (set_impl_prod B7_ (set_impl_prod C7_ (set_impl_prod A7_ D7_))))
        (fn (aa, ba) =>
          let
            val (qA, (x, (y, qAa))) = aa;
          in
            (fn (qB, (_, (_, qBa))) => ((qA, qB), (x, (y, (qAa, qBa)))))
          end
            ba)
        (filter
          (ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
             (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
            ccompare_prod
              (ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_prod (ccompare_cproper_interval C4_)
                    (ccompare_cproper_interval A4_))))
              (ccompare_prod (ccompare_cproper_interval D4_)
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_prod (ccompare_cproper_interval C4_)
                    (ccompare_cproper_interval D4_)))))
          (fn (aa, ba) =>
            let
              val (_, (x, (y, _))) = aa;
            in
              (fn (_, (xa, (ya, _))) => eq B5_ x xa andalso eq C5_ y ya)
            end
              ba)
          (sup_setb
            (finite_UNIV_prod
               (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod B1_
                   (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))))
               (finite_UNIV_prod (finite_UNIV_card_UNIV D1_)
                 (finite_UNIV_prod B1_
                   (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV D1_)))),
              cenum_prod (cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)))
                (cenum_prod D2_ (cenum_prod B2_ (cenum_prod C2_ D2_))),
              ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
                (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
              cproper_interval_prod
                (cproper_interval_prod A4_
                  (cproper_interval_prod B4_ (cproper_interval_prod C4_ A4_)))
                (cproper_interval_prod D4_
                  (cproper_interval_prod B4_ (cproper_interval_prod C4_ D4_))),
              set_impl_prod
                (set_impl_prod A7_ (set_impl_prod B7_ (set_impl_prod C7_ A7_)))
                (set_impl_prod D7_ (set_impl_prod B7_ (set_impl_prod C7_ D7_))))
            (image
              (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_prod (ccompare_cproper_interval C4_)
                      (ccompare_cproper_interval A4_))))
              (ceq_set
                 (cenum_prod
                    (cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)))
                    (cenum_prod D2_ (cenum_prod B2_ (cenum_prod C2_ D2_))),
                   ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
                     (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
                   ccompare_cproper_interval
                     (cproper_interval_prod
                       (cproper_interval_prod A4_
                         (cproper_interval_prod B4_
                           (cproper_interval_prod C4_ A4_)))
                       (cproper_interval_prod D4_
                         (cproper_interval_prod B4_
                           (cproper_interval_prod C4_ D4_))))),
                ccompare_set
                  (finite_UNIV_prod
                     (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                       (finite_UNIV_prod B1_
                         (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))))
                     (finite_UNIV_prod (finite_UNIV_card_UNIV D1_)
                       (finite_UNIV_prod B1_
                         (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV D1_)))),
                    ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
                      (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
                    cproper_interval_prod
                      (cproper_interval_prod A4_
                        (cproper_interval_prod B4_
                          (cproper_interval_prod C4_ A4_)))
                      (cproper_interval_prod D4_
                        (cproper_interval_prod B4_
                          (cproper_interval_prod C4_ D4_))),
                    set_impl_prod
                      (set_impl_prod A7_
                        (set_impl_prod B7_ (set_impl_prod C7_ A7_)))
                      (set_impl_prod D7_
                        (set_impl_prod B7_ (set_impl_prod C7_ D7_)))),
                set_impl_set)
              (fn tA =>
                image (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_)),
                        ccompare_prod (ccompare_cproper_interval D4_)
                          (ccompare_prod (ccompare_cproper_interval B4_)
                            (ccompare_prod (ccompare_cproper_interval C4_)
                              (ccompare_cproper_interval D4_))))
                  (ceq_prod (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)))
                     (ceq_prod D3_ (ceq_prod B3_ (ceq_prod C3_ D3_))),
                    ccompare_prod
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod (ccompare_cproper_interval B4_)
                          (ccompare_prod (ccompare_cproper_interval C4_)
                            (ccompare_cproper_interval A4_))))
                      (ccompare_prod (ccompare_cproper_interval D4_)
                        (ccompare_prod (ccompare_cproper_interval B4_)
                          (ccompare_prod (ccompare_cproper_interval C4_)
                            (ccompare_cproper_interval D4_)))),
                    set_impl_prod
                      (set_impl_prod A7_
                        (set_impl_prod B7_ (set_impl_prod C7_ A7_)))
                      (set_impl_prod D7_
                        (set_impl_prod B7_ (set_impl_prod C7_ D7_))))
                  (fn aa => (tA, aa)) (transitions_wpi b))
              (transitions_wpi a))));
    val h =
      set_as_mapping_image
        (ceq_prod A3_ D3_,
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_cproper_interval D4_))
        (ceq_prod B3_ (ceq_prod C3_ (ceq_prod A3_ D3_)),
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_prod (ccompare_cproper_interval C4_)
              (ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_cproper_interval D4_))))
        (ccompare_prod
           (ccompare_prod (ccompare_cproper_interval A4_)
             (ccompare_cproper_interval D4_))
           (ccompare_cproper_interval B4_),
          equal_prod (equal_prod A5_ D5_) B5_,
          mapping_impl_prod (mapping_impl_prod A6_ D6_) B6_)
        (ceq_prod C3_ (ceq_prod A3_ D3_),
          ccompare_prod (ccompare_cproper_interval C4_)
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval D4_)),
          set_impl_prod C7_ (set_impl_prod A7_ D7_))
        ts (fn (q, (x, (y, qa))) => ((q, x), (y, qa)));
  in
    FSMWPI
      ((initial_wpi a, initial_wpi b),
        producte (A3_, ccompare_cproper_interval A4_, A7_)
          (D3_, ccompare_cproper_interval D4_, D7_) (states_wpi a)
          (states_wpi b),
        sup_seta (B3_, ccompare_cproper_interval B4_) (inputs_wpi a)
          (inputs_wpi b),
        sup_seta (C3_, ccompare_cproper_interval C4_) (outputs_wpi a)
          (outputs_wpi b),
        ts, h,
        h_obs_impl_from_h
          (card_UNIV_prod A1_ D1_, ceq_prod A3_ D3_,
            ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval D4_),
            set_impl_prod A7_ D7_)
          (ccompare_cproper_interval B4_)
          (C3_, ccompare_cproper_interval C4_, C5_, C6_) h)
  end;

fun productg (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_) (D1_, D2_, D3_, D4_, D5_, D6_, D7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) xb xc =
  Fsm_with_precomputations
    (product_impl (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_) (D1_, D2_, D3_, D4_, D5_, D6_, D7_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xc));

fun producta (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  (D1_, D2_, D3_, D4_, D5_, D6_, D7_) (FSMWP a) (FSMWP b) =
  FSMWP (productg (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
          (B1_, B2_, B3_, B4_, B5_, B6_, B7_)
          (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
          (D1_, D2_, D3_, D4_, D5_, D6_, D7_) a b);

fun zip_with_index_from n (x :: xs) = (n, x) :: zip_with_index_from (suc n) xs
  | zip_with_index_from n [] = [];

fun rbt_comp_bulkload c xs =
  foldr (fn (a, b) => rbt_comp_insert c a b) xs Empty;

fun bulkloada A_ xa = Mapping_RBTa (rbt_comp_bulkload (the (ccompare A_)) xa);

fun bulkload vs =
  RBT_Mapping (bulkloada ccompare_nat (zip_with_index_from zero_nat vs));

fun tabulate (A1_, A2_, A3_) xs f =
  fold (fn k => updateb (A1_, A2_) k (f k)) xs (emptya (A1_, A3_));

fun isin (A1_, A2_) (MPT m) xs =
  (case xs of [] => true
    | x :: xsa =>
      (case lookupa (A1_, A2_) m x of NONE => false
        | SOME t => isin (A1_, A2_) t xsa));

fun prefixes xs =
  rev (snd (foldl
             (fn (acc1, acc2) => fn x => (x :: acc1, rev (x :: acc1) :: acc2))
             ([], [[]]) xs));

fun find_removea p [] uu = NONE
  | find_removea p (x :: xs) prev =
    (if p x then SOME (x, prev @ xs) else find_removea p xs (prev @ [x]));

fun find_remove p xs = find_removea p xs [];

fun separate_by p xs =
  foldr (fn x => fn (prevPass, prevFail) =>
          (if p x then (x :: prevPass, prevFail)
            else (prevPass, x :: prevFail)))
    xs ([], []);

fun filter_states_impl (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_) m p
  = (if p (initial_wpi m)
      then let
             val h =
               filterc (ccompare_prod A3_ B2_) (fn (q, _) => fn _ => p q)
                 (h_wpi m);
             val ha =
               map_values (ccompare_prod A3_ B2_)
                 (fn _ =>
                   filter (ceq_prod C1_ A2_, ccompare_prod C2_ A3_)
                     (fn (_, a) => p a))
                 h;
           in
             FSMWPI
               (initial_wpi m, filter (A2_, A3_) p (states_wpi m), inputs_wpi m,
                 outputs_wpi m,
                 filter
                   (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
                     ccompare_prod A3_
                       (ccompare_prod B2_ (ccompare_prod C2_ A3_)))
                   (fn t => p (fst t) andalso p (snd (snd (snd t))))
                   (transitions_wpi m),
                 ha, h_obs_impl_from_h (A1_, A2_, A3_, A4_) B2_
                       (C1_, C2_, C3_, C4_) ha)
           end
      else m);

fun filter_statesb (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_) xb xc =
  Fsm_with_precomputations
    (filter_states_impl (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

fun filter_statesa (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_)
  (FSMWP m) p =
  FSMWP (filter_statesb (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_) m
          p);

fun filter_states (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_) xb xc =
  Abs_fsm
    (filter_statesa (A1_, A2_, A3_, A4_) (B1_, B2_) (C1_, C2_, C3_, C4_)
      (fsm_impl_of_fsm xb) xc);

fun list_as_mapping (A1_, A2_, A3_) (B1_, B2_, B3_) xs =
  foldr (fn (x, z) => fn m =>
          (case lookupa (A1_, A2_) m x
            of NONE =>
              updateb (A1_, A2_) x
                (insert (B1_, B2_) z (bot_set (B1_, B2_, B3_))) m
            | SOME zs => updateb (A1_, A2_) x (insert (B1_, B2_) z zs) m))
    xs (emptya (A1_, A3_));

fun fsm_with_precomputations_impl_from_lista (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q [] =
  FSMWPI
    (q, insert (A2_, A3_) q (bot_set (A2_, A3_, A6_)), bot_set (B1_, B2_, B5_),
      bot_set (C1_, C2_, C5_),
      bot_set
        (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
          ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_)),
          set_impl_prod A6_ (set_impl_prod B5_ (set_impl_prod C5_ A6_))),
      emptya (ccompare_prod A3_ B2_, mapping_impl_prod A5_ B4_),
      emptya (ccompare_prod A3_ B2_, mapping_impl_prod A5_ B4_))
  | fsm_with_precomputations_impl_from_lista (A1_, A2_, A3_, A4_, A5_, A6_)
    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q (t :: ts) =
    let
      val tsr =
        remdups (equal_prod A4_ (equal_prod B3_ (equal_prod C3_ A4_)))
          (t :: ts);
      val h =
        list_as_mapping
          (ccompare_prod A3_ B2_, equal_prod A4_ B3_, mapping_impl_prod A5_ B4_)
          (ceq_prod C1_ A2_, ccompare_prod C2_ A3_, set_impl_prod C5_ A6_)
          (mapa (fn (qb, (x, (y, qa))) => ((qb, x), (y, qa))) tsr);
    in
      FSMWPI
        (fst t,
          set (A2_, A3_, A6_)
            (remdups A4_ (mapa fst tsr @ mapa (fn a => snd (snd (snd a))) tsr)),
          set (B1_, B2_, B5_) (remdups B3_ (mapa (fn a => fst (snd a)) tsr)),
          set (C1_, C2_, C5_)
            (remdups C3_ (mapa (fn a => fst (snd (snd a))) tsr)),
          set (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
                ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_)),
                set_impl_prod A6_ (set_impl_prod B5_ (set_impl_prod C5_ A6_)))
            tsr,
          h, h_obs_impl_from_h (A1_, A2_, A3_, A6_) B2_ (C1_, C2_, C3_, C4_) h)
    end;

fun fsm_with_precomputations_impl_from_list (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q ts =
  fsm_with_precomputations_impl_from_lista (A1_, A2_, A3_, A4_, A5_, A6_)
    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q ts;

fun fsm_with_precomputations_from_list (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q ts =
  Fsm_with_precomputations
    (fsm_with_precomputations_impl_from_list (A1_, A2_, A3_, A4_, A5_, A6_)
      (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q ts);

fun fsm_impl_from_list (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) q ts =
  FSMWP (fsm_with_precomputations_from_list (A1_, A2_, A3_, A4_, A5_, A6_)
          (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) q ts);

fun fsm_from_list (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) xb xc =
  Abs_fsm
    (fsm_impl_from_list (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
      (C1_, C2_, C3_, C4_, C5_) xb xc);

fun emptyc (A1_, A2_) = MPT (emptya (A1_, A2_));

fun aftera (A1_, A2_, A3_) (MPT m) xs =
  (case xs of [] => MPT m
    | x :: xsa =>
      (case lookupa (A1_, A2_) m x of NONE => emptyc (A1_, A3_)
        | SOME t => aftera (A1_, A2_, A3_) t xsa));

fun prefix_pairs [] = []
  | prefix_pairs (v :: va) =
    prefix_pairs (butlast (v :: va)) @
      mapa (fn ys => (ys, v :: va)) (butlast (prefixes (v :: va)));

fun is_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q [] = true
  | is_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q ((x, y) :: io) =
    (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q x y of NONE => false
      | SOME qa => is_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m qa io);

fun insertb (A1_, A2_, A3_) (MPT m) xs =
  (case xs of [] => MPT m
    | x :: xsa =>
      MPT (updateb (A1_, A2_) x
            (insertb (A1_, A2_, A3_)
              (case lookupa (A1_, A2_) m x of NONE => emptyc (A1_, A3_)
                | SOME t => t)
              xsa)
            m));

fun find_remove_2a p [] uu uv = NONE
  | find_remove_2a p (x :: xs) ys prev =
    (case find (p x) ys of NONE => find_remove_2a p xs ys (prev @ [x])
      | SOME y => SOME (x, (y, prev @ xs)));

fun find_remove_2 p xs ys = find_remove_2a p xs ys [];

fun add_transitions_impl (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) m ts =
  (if ball (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
             ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_)))
        ts (fn t =>
             member (A2_, A3_) (fst t) (states_wpi m) andalso
               (member (B1_, B2_) (fst (snd t)) (inputs_wpi m) andalso
                 (member (C1_, C2_) (fst (snd (snd t))) (outputs_wpi m) andalso
                   member (A2_, A3_) (snd (snd (snd t))) (states_wpi m))))
    then let
           val tsa =
             sup_seta
               (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
                 ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_)))
               (transitions_wpi m) ts;
           val h =
             set_as_mapping_image (A2_, A3_)
               (ceq_prod B1_ (ceq_prod C1_ A2_),
                 ccompare_prod B2_ (ccompare_prod C2_ A3_))
               (ccompare_prod A3_ B2_, equal_prod A4_ B3_,
                 mapping_impl_prod A5_ B4_)
               (ceq_prod C1_ A2_, ccompare_prod C2_ A3_, set_impl_prod C5_ A6_)
               tsa (fn (q, (x, (y, qa))) => ((q, x), (y, qa)));
         in
           FSMWPI
             (initial_wpi m, states_wpi m, inputs_wpi m, outputs_wpi m, tsa, h,
               h_obs_impl_from_h (A1_, A2_, A3_, A6_) B2_ (C1_, C2_, C3_, C4_)
                 h)
         end
    else m);

fun add_transitionsb (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) xb xc =
  Fsm_with_precomputations
    (add_transitions_impl (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_, C5_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

fun add_transitionsa (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) (FSMWP m) ts =
  FSMWP (add_transitionsb (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
          (C1_, C2_, C3_, C4_, C5_) m ts);

fun add_transitions (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) xb xc =
  Abs_fsm
    (add_transitionsa (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_, C5_) (fsm_impl_of_fsm xb) xc);

fun combinea A_ (MPT (RBT_Mapping m1)) (MPT (RBT_Mapping m2)) =
  (case ccompare A_
    of NONE =>
      (raise Fail "combine_MPT_RBT_Mapping: ccompare = None")
        (fn _ => combinea A_ (MPT (RBT_Mapping m1)) (MPT (RBT_Mapping m2)))
    | SOME _ => MPT (RBT_Mapping (join A_ (fn _ => combinea A_) m1 m2)));

fun is_leaf A_ (MPT (RBT_Mapping m)) =
  (case ccompare A_
    of NONE =>
      (raise Fail "is_leaf_MPT_RBT_Mapping: ccompare = None")
        (fn _ => is_leaf A_ (MPT (RBT_Mapping m)))
    | SOME _ => is_emptya A_ m);

fun productd (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_) (D1_, D2_, D3_, D4_, D5_, D6_, D7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) xb xc =
  Abs_fsm
    (producta (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_) (D1_, D2_, D3_, D4_, D5_, D6_, D7_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (fsm_impl_of_fsm xb)
      (fsm_impl_of_fsm xc));

fun remove_subsets (A1_, A2_, A3_) [] = []
  | remove_subsets (A1_, A2_, A3_) (x :: xs) =
    (case find_remove (less_set (A1_, A2_, A3_) x) xs
      of NONE =>
        x :: remove_subsets (A1_, A2_, A3_)
               (filtera (fn y => not (less_eq_set (A1_, A2_, A3_) y x)) xs)
      | SOME (y, xsa) =>
        remove_subsets (A1_, A2_, A3_)
          (y :: filtera (fn ya => not (less_eq_set (A1_, A2_, A3_) ya x)) xsa));

fun does_distinguish (A1_, A2_) (B1_, B2_) (C1_, C2_) m q1 q2 io =
  not (equal_boola (is_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q1 io)
        (is_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q2 io));

fun reaching_paths_up_to_depth (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m nexts dones assignment k =
  (if equal_nata k zero_nat then assignment
    else let
           val usable_transitions =
             filtera
               (fn t =>
                 member (A1_, A2_) (fst t) nexts andalso
                   (not (member (A1_, A2_) (snd (snd (snd t))) dones) andalso
                     not (member (A1_, A2_) (snd (snd (snd t))) nexts)))
               (transitions_as_list (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
                 (C1_, C2_, C3_, C4_) m);
           val targets = mapa (fn a => snd (snd (snd a))) usable_transitions;
           val transition_choice =
             map_upds A3_ (fn _ => NONE) targets usable_transitions;
           val assignmenta =
             map_upds A3_ assignment targets
               (mapa (fn q => let
                                val SOME t = transition_choice q;
                                val SOME p = assignment (fst t);
                              in
                                p @ [t]
                              end)
                 targets);
           val nextsa = set (A1_, A2_, A5_) targets;
           val donesa = sup_seta (A1_, A2_) nexts dones;
         in
           reaching_paths_up_to_depth (A1_, A2_, A3_, A4_, A5_)
             (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m nextsa donesa
             assignmenta (minus_nat k one_nata)
         end);

fun reachable_states (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m =
  let
    val path_assignments =
      reaching_paths_up_to_depth (A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
        (C1_, C2_, C3_, C4_) m
        (insert (A2_, A3_) (initial m) (bot_set (A2_, A3_, A6_)))
        (bot_set (A2_, A3_, A6_))
        (fun_upd A4_ (fn _ => NONE) (initial m) (SOME []))
        (minus_nat (size (A1_, A2_, A3_) m) one_nata);
  in
    filter (A2_, A3_) (fn q => not (is_none (path_assignments q))) (states m)
  end;

fun entriesa A_ xa = entries (impl_ofa A_ xa);

fun mergesort_by_rel_split (xs1, xs2) [] = (xs1, xs2)
  | mergesort_by_rel_split (xs1, xs2) [x] = (x :: xs1, xs2)
  | mergesort_by_rel_split (xs1, xs2) (x1 :: x2 :: xs) =
    mergesort_by_rel_split (x1 :: xs1, x2 :: xs2) xs;

fun mergesort_by_rel_merge r (x :: xs) (y :: ys) =
  (if r x y then x :: mergesort_by_rel_merge r xs (y :: ys)
    else y :: mergesort_by_rel_merge r (x :: xs) ys)
  | mergesort_by_rel_merge r xs [] = xs
  | mergesort_by_rel_merge r [] (v :: va) = v :: va;

fun mergesort_by_rel r (x1 :: x2 :: xs) =
  let
    val (xs1, xs2) = mergesort_by_rel_split ([x1], [x2]) xs;
  in
    mergesort_by_rel_merge r (mergesort_by_rel r xs1) (mergesort_by_rel r xs2)
  end
  | mergesort_by_rel r [x] = [x]
  | mergesort_by_rel r [] = [];

fun from_list (A1_, A2_, A3_) xs =
  foldr (fn x => fn t => insertb (A1_, A2_, A3_) t x) xs (emptyc (A1_, A3_));

fun filter_transitions_impl (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) m p =
  let
    val ts =
      filter
        (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)),
          ccompare_prod A3_ (ccompare_prod B2_ (ccompare_prod C2_ A3_)))
        p (transitions_wpi m);
    val h =
      set_as_mapping_image (A2_, A3_)
        (ceq_prod B1_ (ceq_prod C1_ A2_),
          ccompare_prod B2_ (ccompare_prod C2_ A3_))
        (ccompare_prod A3_ B2_, equal_prod A4_ B3_, mapping_impl_prod A5_ B4_)
        (ceq_prod C1_ A2_, ccompare_prod C2_ A3_, set_impl_prod C5_ A6_) ts
        (fn (q, (x, (y, qa))) => ((q, x), (y, qa)));
  in
    FSMWPI
      (initial_wpi m, states_wpi m, inputs_wpi m, outputs_wpi m, ts, h,
        h_obs_impl_from_h (A1_, A2_, A3_, A6_) B2_ (C1_, C2_, C3_, C4_) h)
  end;

fun filter_transitionsb (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) xb xc =
  Fsm_with_precomputations
    (filter_transitions_impl (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_, C5_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

fun filter_transitionsa (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) (FSMWP m) p =
  FSMWP (filter_transitionsb (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
          (C1_, C2_, C3_, C4_, C5_) m p);

fun filter_transitions (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) xb xc =
  Abs_fsm
    (filter_transitionsa (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_, C5_) (fsm_impl_of_fsm xb) xc);

fun language_for_input (A1_, A2_, A3_) (B1_, B2_, B3_) (C1_, C2_, C3_, C4_) m q
  [] = [[]]
  | language_for_input (A1_, A2_, A3_) (B1_, B2_, B3_) (C1_, C2_, C3_, C4_) m q
    (x :: xs) =
    let
      val a = outputs_as_list (C1_, C2_, C3_, C4_) m;
    in
      maps (fn y =>
             (case h_obs (A1_, A2_) (B1_, B2_) (C2_, C3_) m q x y of NONE => []
               | SOME qa =>
                 mapa (fn aa => (x, y) :: aa)
                   (language_for_input (A1_, A2_, A3_) (B1_, B2_, B3_)
                     (C1_, C2_, C3_, C4_) m qa xs)))
        a
    end;

fun lookup_default (B1_, B2_) d m k =
  (case lookupa (B1_, B2_) m k of NONE => d | SOME v => v);

fun covered_transitions (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_) m v alpha =
  let
    val ts =
      the_elem
        (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))),
          ccompare_list
            (ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
        (paths_for_io (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_)
          (C1_, C2_, C3_, C4_) m (initial m) alpha);
  in
    set (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
          ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)),
          set_impl_prod A5_ (set_impl_prod B5_ (set_impl_prod C4_ A5_)))
      (filtera
        (fn t =>
          equal_lista (equal_prod B3_ C3_)
            (v (fst t) @ [(fst (snd t), fst (snd (snd t)))])
            (v (snd (snd (snd t)))))
        ts)
  end;

fun reachable_states_as_list (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m =
  sorted_list_of_set (A2_, A3_, A4_, A5_)
    (reachable_states (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_, C4_) m);

fun h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma get_state_cover handle_state_cover sort_transitions
  handle_unverified_transition handle_unverified_io_pair cg_initial cg_insert
  cg_lookup cg_merge m =
  let
    val rstates_set =
      reachable_states (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma;
    val rstates =
      reachable_states_as_list
        (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma;
    val rstates_io =
      product rstates
        (product
          (inputs_as_list (B3_, ccompare_cproper_interval B4_, B5_, B7_) ma)
          (outputs_as_list (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma));
    val undefined_io_pairs =
      filtera
        (fn (q, (x, y)) =>
          is_none
            (h_obs (ccompare_cproper_interval A4_, A5_)
              (ccompare_cproper_interval B4_, B5_)
              (ccompare_cproper_interval C4_, C5_) ma q x y))
        rstates_io;
    val v = get_state_cover ma;
    val tG1 = handle_state_cover ma v cg_initial cg_insert cg_lookup;
    val sc_covered_transitions =
      sup_setb
        (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
           (finite_UNIV_prod B1_
             (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))),
          cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)),
          ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
          cproper_interval_prod A4_
            (cproper_interval_prod B4_ (cproper_interval_prod C4_ A4_)),
          set_impl_prod A8_ (set_impl_prod B8_ (set_impl_prod C7_ A8_)))
        (image (A3_, ccompare_cproper_interval A4_)
          (ceq_set
             (cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)),
               ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
               ccompare_cproper_interval
                 (cproper_interval_prod A4_
                   (cproper_interval_prod B4_
                     (cproper_interval_prod C4_ A4_)))),
            ccompare_set
              (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod B1_
                   (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))),
                ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                cproper_interval_prod A4_
                  (cproper_interval_prod B4_ (cproper_interval_prod C4_ A4_)),
                set_impl_prod A8_ (set_impl_prod B8_ (set_impl_prod C7_ A8_))),
            set_impl_set)
          (fn q =>
            covered_transitions
              (A3_, ccompare_cproper_interval A4_, A5_, A6_, A8_)
              (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
              (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma v (v q))
          rstates_set);
    val unverified_transitions =
      sort_transitions ma v
        (filtera
          (fn t =>
            member (A3_, ccompare_cproper_interval A4_) (fst t)
              rstates_set andalso
              not (member
                    (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                      ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod (ccompare_cproper_interval B4_)
                          (ccompare_prod (ccompare_cproper_interval C4_)
                            (ccompare_cproper_interval A4_))))
                    t sc_covered_transitions))
          (transitions_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
            (B3_, ccompare_cproper_interval B4_, B5_, B7_)
            (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma));
    val verify_transition =
      (fn (x, (t, g)) => fn ta =>
        handle_unverified_transition ma v t g cg_insert cg_lookup cg_merge m ta
          x);
    val tG2 =
      snd (foldl verify_transition (unverified_transitions, tG1)
            unverified_transitions);
    val verify_undefined_io_pair =
      (fn t => fn (q, (x, y)) =>
        fst (handle_unverified_io_pair ma v t (snd tG2) cg_insert cg_lookup q x
              y));
  in
    foldl verify_undefined_io_pair (fst tG2) undefined_io_pairs
  end;

val emptye : 'a mp_trie = MP_Trie [];

fun paths (MP_Trie []) = [[]]
  | paths (MP_Trie (t :: ts)) =
    maps (fn (x, ta) => mapa (fn a => x :: a) (paths ta)) (t :: ts);

fun ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m f k q
  = (if equal_nata k zero_nat
      then (if member (A2_, A3_) q (states m) then f q
             else bot_set (A2_, A3_, A5_))
      else let
             val prev_table =
               ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
                 (C1_, C2_, C3_) m f (minus_nat k one_nata);
           in
             filter (A2_, A3_)
               (fn qa =>
                 ball (B1_, B2_) (inputs m)
                   (fn x =>
                     ball (C1_, C2_) (outputs m)
                       (fn y =>
                         (case h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q x y
                           of NONE =>
                             is_none
                               (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m qa x y)
                           | SOME qT =>
                             (case h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m qa x
                                     y
                               of NONE => false
                               | SOME qTa =>
                                 set_eq (A1_, A2_, A3_) (prev_table qT)
                                   (prev_table qTa))))))
               (prev_table q)
           end);

fun min A_ a b = (if less_eq A_ a b then a else b);

fun select_inputs (A1_, A2_, A3_) (C1_, C2_, C3_) f q0 inputList [] stateSet m =
  (case find (fn x =>
               not (is_empty
                     (card_UNIV_prod C1_ A1_, ceq_prod C2_ A2_,
                       cproper_interval_prod C3_ A3_)
                     (f (q0, x))) andalso
                 ball (ceq_prod C2_ A2_,
                        ccompare_prod (ccompare_cproper_interval C3_)
                          (ccompare_cproper_interval A3_))
                   (f (q0, x))
                   (fn (_, q) =>
                     member (A2_, ccompare_cproper_interval A3_) q stateSet))
          inputList
    of NONE => m | SOME x => m @ [(q0, x)])
  | select_inputs (A1_, A2_, A3_) (C1_, C2_, C3_) f q0 inputList (n :: nL)
    stateSet m =
    (case find (fn x =>
                 not (is_empty
                       (card_UNIV_prod C1_ A1_, ceq_prod C2_ A2_,
                         cproper_interval_prod C3_ A3_)
                       (f (q0, x))) andalso
                   ball (ceq_prod C2_ A2_,
                          ccompare_prod (ccompare_cproper_interval C3_)
                            (ccompare_cproper_interval A3_))
                     (f (q0, x))
                     (fn (_, q) =>
                       member (A2_, ccompare_cproper_interval A3_) q stateSet))
            inputList
      of NONE =>
        (case find_remove_2
                (fn q => fn x =>
                  not (is_empty
                        (card_UNIV_prod C1_ A1_, ceq_prod C2_ A2_,
                          cproper_interval_prod C3_ A3_)
                        (f (q, x))) andalso
                    ball (ceq_prod C2_ A2_,
                           ccompare_prod (ccompare_cproper_interval C3_)
                             (ccompare_cproper_interval A3_))
                      (f (q, x))
                      (fn (_, qa) =>
                        member (A2_, ccompare_cproper_interval A3_) qa
                          stateSet))
                (n :: nL) inputList
          of NONE => m
          | SOME (q, (x, stateList)) =>
            select_inputs (A1_, A2_, A3_) (C1_, C2_, C3_) f q0 inputList
              stateList (insert (A2_, ccompare_cproper_interval A3_) q stateSet)
              (m @ [(q, x)]))
      | SOME x => m @ [(q0, x)]);

fun d_states (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_) m q =
  (if eq A4_ q (initial m) then []
    else select_inputs (A1_, A2_, A3_) (C1_, C2_, C3_)
           (h (A2_, ccompare_cproper_interval A3_, A4_, A5_, A7_)
             (B2_, B3_, B4_) (C2_, ccompare_cproper_interval C3_, C4_) m)
           (initial m) (inputs_as_list (B1_, B2_, B3_, B5_) m)
           (removeAll A4_ q
             (removeAll A4_ (initial m)
               (states_as_list (A2_, ccompare_cproper_interval A3_, A4_, A6_)
                 m)))
           (insert (A2_, ccompare_cproper_interval A3_) q
             (bot_set (A2_, ccompare_cproper_interval A3_, A7_)))
           []);

fun list_ordered_pairs [] = []
  | list_ordered_pairs (x :: xs) =
    mapa (fn a => (x, a)) xs @ list_ordered_pairs xs;

fun minus_set (A1_, A2_) a b = inf_seta (A1_, A2_) a (uminus_set b);

fun distinguishing_transitions (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) f q1 q2 stateSet inputSet
  = sup_setb
      (finite_UNIV_prod (finite_UNIV_sum (finite_UNIV_prod A1_ A1_) A1_)
         (finite_UNIV_prod B1_
           (finite_UNIV_prod C1_
             (finite_UNIV_sum (finite_UNIV_prod A1_ A1_) A1_))),
        cenum_prod (cenum_sum (cenum_prod A2_ A2_) A2_)
          (cenum_prod B2_
            (cenum_prod C2_ (cenum_sum (cenum_prod A2_ A2_) A2_))),
        ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
          (ceq_prod B3_ (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
        cproper_interval_prod
          (cproper_interval_sum (cproper_interval_prod A4_ A4_) A4_)
          (cproper_interval_prod B4_
            (cproper_interval_prod C4_
              (cproper_interval_sum (cproper_interval_prod A4_ A4_) A4_))),
        set_impl_prod (set_impl_sum (set_impl_prod A5_ A5_) A5_)
          (set_impl_prod B5_
            (set_impl_prod C5_ (set_impl_sum (set_impl_prod A5_ A5_) A5_))))
      (image
        (ceq_prod (ceq_prod A3_ A3_) B3_,
          ccompare_prod
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_))
            (ccompare_cproper_interval B4_))
        (ceq_set
           (cenum_prod (cenum_sum (cenum_prod A2_ A2_) A2_)
              (cenum_prod B2_
                (cenum_prod C2_ (cenum_sum (cenum_prod A2_ A2_) A2_))),
             ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
               (ceq_prod B3_ (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
             ccompare_cproper_interval
               (cproper_interval_prod
                 (cproper_interval_sum (cproper_interval_prod A4_ A4_) A4_)
                 (cproper_interval_prod B4_
                   (cproper_interval_prod C4_
                     (cproper_interval_sum (cproper_interval_prod A4_ A4_)
                       A4_))))),
          ccompare_set
            (finite_UNIV_prod (finite_UNIV_sum (finite_UNIV_prod A1_ A1_) A1_)
               (finite_UNIV_prod B1_
                 (finite_UNIV_prod C1_
                   (finite_UNIV_sum (finite_UNIV_prod A1_ A1_) A1_))),
              ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                (ceq_prod B3_ (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
              cproper_interval_prod
                (cproper_interval_sum (cproper_interval_prod A4_ A4_) A4_)
                (cproper_interval_prod B4_
                  (cproper_interval_prod C4_
                    (cproper_interval_sum (cproper_interval_prod A4_ A4_)
                      A4_))),
              set_impl_prod (set_impl_sum (set_impl_prod A5_ A5_) A5_)
                (set_impl_prod B5_
                  (set_impl_prod C5_
                    (set_impl_sum (set_impl_prod A5_ A5_) A5_)))),
          set_impl_set)
        (fn (a, b) =>
          let
            val (q1a, q2a) = a;
          in
            (fn x =>
              sup_seta
                (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                   (ceq_prod B3_
                     (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
                  ccompare_prod
                    (ccompare_sum
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_cproper_interval A4_))
                      (ccompare_cproper_interval A4_))
                    (ccompare_prod (ccompare_cproper_interval B4_)
                      (ccompare_prod (ccompare_cproper_interval C4_)
                        (ccompare_sum
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_cproper_interval A4_))
                          (ccompare_cproper_interval A4_)))))
                (image (C3_, ccompare_cproper_interval C4_)
                  (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                     (ceq_prod B3_
                       (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
                    ccompare_prod
                      (ccompare_sum
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_cproper_interval A4_))
                        (ccompare_cproper_interval A4_))
                      (ccompare_prod (ccompare_cproper_interval B4_)
                        (ccompare_prod (ccompare_cproper_interval C4_)
                          (ccompare_sum
                            (ccompare_prod (ccompare_cproper_interval A4_)
                              (ccompare_cproper_interval A4_))
                            (ccompare_cproper_interval A4_)))),
                    set_impl_prod (set_impl_sum (set_impl_prod A5_ A5_) A5_)
                      (set_impl_prod B5_
                        (set_impl_prod C5_
                          (set_impl_sum (set_impl_prod A5_ A5_) A5_))))
                  (fn y => (Inl (q1a, q2a), (x, (y, Inr q1))))
                  (minus_set (C3_, ccompare_cproper_interval C4_) (f (q1a, x))
                    (f (q2a, x))))
                (image (C3_, ccompare_cproper_interval C4_)
                  (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                     (ceq_prod B3_
                       (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
                    ccompare_prod
                      (ccompare_sum
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_cproper_interval A4_))
                        (ccompare_cproper_interval A4_))
                      (ccompare_prod (ccompare_cproper_interval B4_)
                        (ccompare_prod (ccompare_cproper_interval C4_)
                          (ccompare_sum
                            (ccompare_prod (ccompare_cproper_interval A4_)
                              (ccompare_cproper_interval A4_))
                            (ccompare_cproper_interval A4_)))),
                    set_impl_prod (set_impl_sum (set_impl_prod A5_ A5_) A5_)
                      (set_impl_prod B5_
                        (set_impl_prod C5_
                          (set_impl_sum (set_impl_prod A5_ A5_) A5_))))
                  (fn y => (Inl (q1a, q2a), (x, (y, Inr q2))))
                  (minus_set (C3_, ccompare_cproper_interval C4_) (f (q2a, x))
                    (f (q1a, x)))))
          end
            b)
        (producte
          (ceq_prod A3_ A3_,
            ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_),
            set_impl_prod A5_ A5_)
          (B3_, ccompare_cproper_interval B4_, B5_) stateSet inputSet));

fun shifted_transitions (A1_, A2_, A3_) (B1_, B2_, B3_) (C1_, C2_, C3_)
  (D1_, D2_, D3_) ts =
  image (ceq_prod (ceq_prod A1_ A1_)
           (ceq_prod B1_ (ceq_prod C1_ (ceq_prod A1_ A1_))),
          ccompare_prod (ccompare_prod A2_ A2_)
            (ccompare_prod B2_ (ccompare_prod C2_ (ccompare_prod A2_ A2_))))
    (ceq_prod (ceq_sum (ceq_prod A1_ A1_) D1_)
       (ceq_prod B1_ (ceq_prod C1_ (ceq_sum (ceq_prod A1_ A1_) D1_))),
      ccompare_prod (ccompare_sum (ccompare_prod A2_ A2_) D2_)
        (ccompare_prod B2_
          (ccompare_prod C2_ (ccompare_sum (ccompare_prod A2_ A2_) D2_))),
      set_impl_prod (set_impl_sum (set_impl_prod A3_ A3_) D3_)
        (set_impl_prod B3_
          (set_impl_prod C3_ (set_impl_sum (set_impl_prod A3_ A3_) D3_))))
    (fn t =>
      (Inl (fst t),
        (fst (snd t), (fst (snd (snd t)), Inl (snd (snd (snd t)))))))
    ts;

fun canonical_separator_impl (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m p q1
  q2 =
  (if equal_proda A5_ A5_ (initial_wpi p) (q1, q2)
    then let
           val f =
             set_as_mapping_image (A3_, ccompare_cproper_interval A4_)
               (ceq_prod B3_ (ceq_prod C3_ A3_),
                 ccompare_prod (ccompare_cproper_interval B4_)
                   (ccompare_prod (ccompare_cproper_interval C4_)
                     (ccompare_cproper_interval A4_)))
               (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval B4_),
                 equal_prod A5_ B5_, mapping_impl_prod A6_ B6_)
               (C3_, ccompare_cproper_interval C4_, C7_) (transitions_wpi m)
               (fn (q, (x, (y, _))) => ((q, x), y));
           val fa =
             (fn qx =>
               (case lookupa
                       (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_cproper_interval B4_),
                         equal_prod A5_ B5_)
                       f qx
                 of NONE => bot_set (C3_, ccompare_cproper_interval C4_, C7_)
                 | SOME yqs => yqs));
           val shifted_transitionsa =
             shifted_transitions (A3_, ccompare_cproper_interval A4_, A7_)
               (B3_, ccompare_cproper_interval B4_, B7_)
               (C3_, ccompare_cproper_interval C4_, C7_)
               (A3_, ccompare_cproper_interval A4_, A7_) (transitions_wpi p);
           val distinguishing_transitions_lr =
             distinguishing_transitions
               (finite_UNIV_card_UNIV A1_, A2_, A3_, A4_, A7_)
               (B1_, B2_, B3_, B4_, B7_) (C1_, C2_, C3_, C4_, C7_) fa q1 q2
               (states_wpi p) (inputs_wpi p);
           val ts =
             sup_seta
               (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                  (ceq_prod B3_
                    (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
                 ccompare_prod
                   (ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_))
                   (ccompare_prod (ccompare_cproper_interval B4_)
                     (ccompare_prod (ccompare_cproper_interval C4_)
                       (ccompare_sum
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_cproper_interval A4_))
                         (ccompare_cproper_interval A4_)))))
               shifted_transitionsa distinguishing_transitions_lr;
           val h =
             set_as_mapping_image
               (ceq_sum (ceq_prod A3_ A3_) A3_,
                 ccompare_sum
                   (ccompare_prod (ccompare_cproper_interval A4_)
                     (ccompare_cproper_interval A4_))
                   (ccompare_cproper_interval A4_))
               (ceq_prod B3_ (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_)),
                 ccompare_prod (ccompare_cproper_interval B4_)
                   (ccompare_prod (ccompare_cproper_interval C4_)
                     (ccompare_sum
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_cproper_interval A4_))
                       (ccompare_cproper_interval A4_))))
               (ccompare_prod
                  (ccompare_sum
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_cproper_interval A4_))
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval B4_),
                 equal_prod (equal_sum (equal_prod A5_ A5_) A5_) B5_,
                 mapping_impl_prod
                   (mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_) B6_)
               (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_),
                 ccompare_prod (ccompare_cproper_interval C4_)
                   (ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_)),
                 set_impl_prod C7_ (set_impl_sum (set_impl_prod A7_ A7_) A7_))
               ts (fn (q, (x, (y, qa))) => ((q, x), (y, qa)));
         in
           FSMWPI
             (Inl (q1, q2),
               sup_seta
                 (ceq_sum (ceq_prod A3_ A3_) A3_,
                   ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_))
                 (image
                   (ceq_prod A3_ A3_,
                     ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                   (ceq_sum (ceq_prod A3_ A3_) A3_,
                     ccompare_sum
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_cproper_interval A4_))
                       (ccompare_cproper_interval A4_),
                     set_impl_sum (set_impl_prod A7_ A7_) A7_)
                   Inl (states_wpi p))
                 (insert
                   (ceq_sum (ceq_prod A3_ A3_) A3_,
                     ccompare_sum
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_cproper_interval A4_))
                       (ccompare_cproper_interval A4_))
                   (Inr q1)
                   (insert
                     (ceq_sum (ceq_prod A3_ A3_) A3_,
                       ccompare_sum
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_cproper_interval A4_))
                         (ccompare_cproper_interval A4_))
                     (Inr q2)
                     (bot_set
                       (ceq_sum (ceq_prod A3_ A3_) A3_,
                         ccompare_sum
                           (ccompare_prod (ccompare_cproper_interval A4_)
                             (ccompare_cproper_interval A4_))
                           (ccompare_cproper_interval A4_),
                         set_impl_sum (set_impl_prod A7_ A7_) A7_)))),
               sup_seta (B3_, ccompare_cproper_interval B4_) (inputs_wpi m)
                 (inputs_wpi p),
               sup_seta (C3_, ccompare_cproper_interval C4_) (outputs_wpi m)
                 (outputs_wpi p),
               ts, h,
               h_obs_impl_from_h
                 (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
                   ceq_sum (ceq_prod A3_ A3_) A3_,
                   ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_),
                   set_impl_sum (set_impl_prod A7_ A7_) A7_)
                 (ccompare_cproper_interval B4_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C6_) h)
         end
    else FSMWPI
           (Inl (q1, q2),
             insert
               (ceq_sum (ceq_prod A3_ A3_) A3_,
                 ccompare_sum
                   (ccompare_prod (ccompare_cproper_interval A4_)
                     (ccompare_cproper_interval A4_))
                   (ccompare_cproper_interval A4_))
               (Inl (q1, q2))
               (bot_set
                 (ceq_sum (ceq_prod A3_ A3_) A3_,
                   ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_),
                   set_impl_sum (set_impl_prod A7_ A7_) A7_)),
             bot_set (B3_, ccompare_cproper_interval B4_, B7_),
             bot_set (C3_, ccompare_cproper_interval C4_, C7_),
             bot_set
               (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
                  (ceq_prod B3_
                    (ceq_prod C3_ (ceq_sum (ceq_prod A3_ A3_) A3_))),
                 ccompare_prod
                   (ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_))
                   (ccompare_prod (ccompare_cproper_interval B4_)
                     (ccompare_prod (ccompare_cproper_interval C4_)
                       (ccompare_sum
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_cproper_interval A4_))
                         (ccompare_cproper_interval A4_)))),
                 set_impl_prod (set_impl_sum (set_impl_prod A7_ A7_) A7_)
                   (set_impl_prod B7_
                     (set_impl_prod C7_
                       (set_impl_sum (set_impl_prod A7_ A7_) A7_)))),
             emptya
               (ccompare_prod
                  (ccompare_sum
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_cproper_interval A4_))
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval B4_),
                 mapping_impl_prod
                   (mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_) B6_),
             emptya
               (ccompare_prod
                  (ccompare_sum
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_cproper_interval A4_))
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval B4_),
                 mapping_impl_prod
                   (mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_) B6_)));

fun canonical_separatorc (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) xc xe
  xf xg =
  Fsm_with_precomputations
    (canonical_separator_impl (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xc)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xe) xf xg);

fun canonical_separatora (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  (FSMWP m) (FSMWP p) q1 q2 =
  FSMWP (canonical_separatorc (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
          (B1_, B2_, B3_, B4_, B5_, B6_, B7_)
          (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m p q1 q2);

fun canonical_separator (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) xc xe
  xf xg =
  Abs_fsm
    (canonical_separatora (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
      (fsm_impl_of_fsm xc) (fsm_impl_of_fsm xe) xf xg);

fun completely_specified (A1_, A2_, A3_) (B1_, B2_, B3_) (C1_, C2_) m =
  ball (A1_, A2_) (states m)
    (fn q =>
      ball (B1_, B2_) (inputs m)
        (fn x =>
          bex (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
                ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)))
            (transitions m)
            (fn t => eq A3_ (fst t) q andalso eq B3_ (fst (snd t)) x)));

fun index_states_integer (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m =
  rename_states (A1_, A2_) (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
    (card_UNIV_integer, ceq_integer, ccompare_integer, equal_integer,
      mapping_impl_integer, set_impl_integer)
    m (integer_of_nat o assign_indices (A1_, A2_, A3_, A4_) (states m));

fun sorted_list_of_fset (A1_, A2_, A3_, A4_) xa =
  sorted_list_of_set (A1_, A2_, A3_, A4_) (fset xa);

val one_int : int = Int_of_integer (1 : IntInf.int);

fun update_assoc_list_with_default A_ k f d [] = [(k, f d)]
  | update_assoc_list_with_default A_ k f d ((x, y) :: xys) =
    (if eq A_ k x then (x, f y) :: xys
      else (x, y) :: update_assoc_list_with_default A_ k f d xys);

fun insertd A_ [] t = t
  | insertd A_ (x :: xs) (MP_Trie ts) =
    MP_Trie (update_assoc_list_with_default A_ x (insertd A_ xs) emptye ts);

fun integer_of_int (Int_of_integer k) = k;

fun less_int k l = IntInf.< (integer_of_int k, integer_of_int l);

fun combine_after (A1_, A2_, A3_) (MPT m) xs t =
  (case xs of [] => combinea A1_ (MPT m) t
    | x :: xsa =>
      MPT (updateb (A1_, A2_) x
            (combine_after (A1_, A2_, A3_)
              (case lookupa (A1_, A2_) m x of NONE => emptyc (A1_, A3_)
                | SOME ta => ta)
              xsa t)
            m));

fun is_maximal_in (A1_, A2_, A3_) t alpha =
  isin (A1_, A2_) t alpha andalso is_leaf A1_ (aftera (A1_, A2_, A3_) t alpha);

fun int_of_nat n = Int_of_integer (integer_of_nat n);

fun initial_singleton_impl (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_) m =
  FSMWPI
    (initial_wpi m, insert (A1_, A2_) (initial_wpi m) (bot_set (A1_, A2_, A4_)),
      inputs_wpi m, outputs_wpi m,
      bot_set
        (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
          ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)),
          set_impl_prod A4_ (set_impl_prod B4_ (set_impl_prod C3_ A4_))),
      emptya (ccompare_prod A2_ B2_, mapping_impl_prod A3_ B3_),
      emptya (ccompare_prod A2_ B2_, mapping_impl_prod A3_ B3_));

fun initial_singletona (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_)
  xa =
  Fsm_with_precomputations
    (initial_singleton_impl (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
      (C1_, C2_, C3_)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xa));

fun initial_singleton (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_)
  (FSMWP m) =
  FSMWP (initial_singletona (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
          (C1_, C2_, C3_) m);

val zero_int : int = Int_of_integer (0 : IntInf.int);

fun maximum_prefix (A1_, A2_) (MPT m) xs =
  (case xs of [] => []
    | x :: xsa =>
      (case lookupa (A1_, A2_) m x of NONE => []
        | SOME t => x :: maximum_prefix (A1_, A2_) t xsa));

fun bot_fset (A1_, A2_, A3_) = Abs_fset (bot_set (A1_, A2_, A3_));

fun sup_fset (A1_, A2_) xb xc =
  Abs_fset (sup_seta (A1_, A2_) (fset xb) (fset xc));

fun from_lista A_ seqs = foldr (insertd A_) seqs emptye;

fun ofsm_table_fix (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m f
  k = let
        val cur_table =
          ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m
            (fn q => inf_seta (A2_, A3_) (f q) (states m)) k;
        val next_table =
          ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m
            (fn q => inf_seta (A2_, A3_) (f q) (states m)) (suc k);
      in
        (if ball (A2_, A3_) (states m)
              (fn q => set_eq (A1_, A2_, A3_) (cur_table q) (next_table q))
          then cur_table
          else ofsm_table_fix (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
                 (C1_, C2_, C3_) m f (suc k))
      end;

fun language_up_to_length_with_extensions q hM iM ex k =
  (if equal_nata k zero_nat then ex
    else ex @ maps (fn x =>
                     maps (fn (y, qa) =>
                            mapa (fn a => (x, y) :: a)
                              (language_up_to_length_with_extensions qa hM iM ex
                                (minus_nat k one_nata)))
                       (hM q x))
                iM);

fun h_extensions (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m q k =
  let
    val iM = inputs_as_list (B1_, B2_, B3_, B5_) m;
    val ex =
      mapa (fn xy => [xy])
        (product iM (outputs_as_list (C1_, C2_, C3_, C4_) m));
    val hM =
      (fn qa => fn x =>
        sorted_list_of_set
          (ceq_prod C1_ A1_, ccompare_prod C2_ A2_, equal_prod C3_ A3_,
            linorder_prod C4_ A5_)
          (h (A1_, A2_, A3_, A4_, A6_) (B2_, B3_, B4_) (C1_, C2_, C5_) m
            (qa, x)));
  in
    language_up_to_length_with_extensions q hM iM ex k
  end;

fun spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma get_state_cover separate_state_cover sort_unverified_transitions
  establish_convergence append_io_pair cg_initial cg_insert cg_lookup cg_merge m
  = let
      val rstates_set =
        reachable_states
          (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
          (B3_, ccompare_cproper_interval B4_, B5_, B7_)
          (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma;
      val rstates =
        reachable_states_as_list
          (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
          (B3_, ccompare_cproper_interval B4_, B5_, B7_)
          (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma;
      val rstates_io =
        product rstates
          (product
            (inputs_as_list (B3_, ccompare_cproper_interval B4_, B5_, B7_) ma)
            (outputs_as_list (C3_, ccompare_cproper_interval C4_, C5_, C6_)
              ma));
      val undefined_io_pairs =
        filtera
          (fn (q, (x, y)) =>
            is_none
              (h_obs (ccompare_cproper_interval A4_, A5_)
                (ccompare_cproper_interval B4_, B5_)
                (ccompare_cproper_interval C4_, C5_) ma q x y))
          rstates_io;
      val v = get_state_cover ma;
      val _ =
        card (A1_, A3_, ccompare_cproper_interval A4_)
          (reachable_states
            (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
            (B3_, ccompare_cproper_interval B4_, B5_, B7_)
            (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma);
      val tG1 = separate_state_cover ma v cg_initial cg_insert cg_lookup;
      val sc_covered_transitions =
        sup_setb
          (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
             (finite_UNIV_prod B1_
               (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))),
            cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)),
            ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
            cproper_interval_prod A4_
              (cproper_interval_prod B4_ (cproper_interval_prod C4_ A4_)),
            set_impl_prod A8_ (set_impl_prod B8_ (set_impl_prod C7_ A8_)))
          (image (A3_, ccompare_cproper_interval A4_)
            (ceq_set
               (cenum_prod A2_ (cenum_prod B2_ (cenum_prod C2_ A2_)),
                 ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                 ccompare_cproper_interval
                   (cproper_interval_prod A4_
                     (cproper_interval_prod B4_
                       (cproper_interval_prod C4_ A4_)))),
              ccompare_set
                (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                   (finite_UNIV_prod B1_
                     (finite_UNIV_prod C1_ (finite_UNIV_card_UNIV A1_))),
                  ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                  cproper_interval_prod A4_
                    (cproper_interval_prod B4_ (cproper_interval_prod C4_ A4_)),
                  set_impl_prod A8_
                    (set_impl_prod B8_ (set_impl_prod C7_ A8_))),
              set_impl_set)
            (fn q =>
              covered_transitions
                (A3_, ccompare_cproper_interval A4_, A5_, A6_, A8_)
                (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
                (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma v (v q))
            rstates_set);
      val unverified_transitions =
        sort_unverified_transitions ma v
          (filtera
            (fn t =>
              member (A3_, ccompare_cproper_interval A4_) (fst t)
                rstates_set andalso
                not (member
                      (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                        ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod (ccompare_cproper_interval B4_)
                            (ccompare_prod (ccompare_cproper_interval C4_)
                              (ccompare_cproper_interval A4_))))
                      t sc_covered_transitions))
            (transitions_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
              (C3_, ccompare_cproper_interval C4_, C5_, C6_) ma));
      val verify_transition =
        (fn (t, g) => fn ta =>
          let
            val tGxy =
              append_io_pair ma v t g cg_insert cg_lookup (fst ta)
                (fst (snd ta))
                (fst (snd (snd ta)));
            val (tb, ga) =
              establish_convergence ma v (fst tGxy) (snd tGxy) cg_insert
                cg_lookup
                m
                ta;
            val a =
              cg_merge ga (v (fst ta) @ [(fst (snd ta), fst (snd (snd ta)))])
                (v (snd (snd (snd ta))));
          in
            (tb, a)
          end);
      val tG2 = foldl verify_transition tG1 unverified_transitions;
      val verify_undefined_io_pair =
        (fn t => fn (q, (x, y)) =>
          fst (append_io_pair ma v t (snd tG2) cg_insert cg_lookup q x y));
    in
      foldl verify_undefined_io_pair (fst tG2) undefined_io_pairs
    end;

fun remove_proper_prefixes (A1_, A2_, A3_) (RBT_set t) =
  (case ccompare_lista A2_
    of NONE =>
      (raise Fail "remove_proper_prefixes RBT_set: ccompare = None")
        (fn _ => remove_proper_prefixes (A1_, A2_, A3_) (RBT_set t))
    | SOME _ =>
      (if is_emptya (ccompare_list A2_) t
        then set_empty (ceq_list A1_, ccompare_list A2_)
               (of_phantom set_impl_lista)
        else set (ceq_list A1_, ccompare_list A2_, set_impl_list)
               (paths (from_lista A3_ (keysb (ccompare_list A2_) t)))));

fun equal_int k l = (((integer_of_int k) : IntInf.int) = (integer_of_int l));

fun minus_fset (A1_, A2_) xb xc =
  Abs_fset (minus_set (A1_, A2_) (fset xb) (fset xc));

fun make_observable_transitions (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_) (C1_, C2_, C3_, C4_, C5_, C6_) base_trans nexts
  dones ts =
  let
    val qtrans =
      ffUnion
        (finite_UNIV_prod (finite_UNIV_fset A1_)
           (finite_UNIV_prod B1_ (finite_UNIV_prod C1_ (finite_UNIV_fset A1_))),
          cenum_prod cenum_fset (cenum_prod B2_ (cenum_prod C2_ cenum_fset)),
          ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
            (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
          cproper_interval_prod (cproper_interval_fset A7_)
            (cproper_interval_prod B4_
              (cproper_interval_prod C4_ (cproper_interval_fset A7_))),
          equal_prod (equal_fset (A2_, A3_, A4_, A5_))
            (equal_prod B5_ (equal_prod C5_ (equal_fset (A2_, A3_, A4_, A5_)))),
          set_impl_prod set_impl_fset
            (set_impl_prod B6_ (set_impl_prod C6_ set_impl_fset)))
        (fimage (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
          (ceq_fset
             (cenum_prod cenum_fset
                (cenum_prod B2_ (cenum_prod C2_ cenum_fset)),
               ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
                 (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
               ccompare_prod ccompare_fset
                 (ccompare_prod (ccompare_cproper_interval B4_)
                   (ccompare_prod (ccompare_cproper_interval C4_)
                     ccompare_fset)),
               equal_prod (equal_fset (A2_, A3_, A4_, A5_))
                 (equal_prod B5_
                   (equal_prod C5_ (equal_fset (A2_, A3_, A4_, A5_))))),
            ccompare_fset, set_impl_fset)
          (fn q =>
            let
              val qts =
                ffilter
                  (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                    ccompare_prod A4_
                      (ccompare_prod (ccompare_cproper_interval B4_)
                        (ccompare_prod (ccompare_cproper_interval C4_) A4_)))
                  (fn t => fmember (A3_, A4_) (fst t) q) base_trans;
              val a =
                fimage
                  (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                    ccompare_prod A4_
                      (ccompare_prod (ccompare_cproper_interval B4_)
                        (ccompare_prod (ccompare_cproper_interval C4_) A4_)))
                  (ceq_prod B3_ C3_,
                    ccompare_prod (ccompare_cproper_interval B4_)
                      (ccompare_cproper_interval C4_),
                    set_impl_prod B6_ C6_)
                  (fn t => (fst (snd t), fst (snd (snd t)))) qts;
            in
              fimage
                (ceq_prod B3_ C3_,
                  ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_cproper_interval C4_))
                (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
                   (ceq_prod B3_
                     (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
                  ccompare_prod ccompare_fset
                    (ccompare_prod (ccompare_cproper_interval B4_)
                      (ccompare_prod (ccompare_cproper_interval C4_)
                        ccompare_fset)),
                  set_impl_prod set_impl_fset
                    (set_impl_prod B6_ (set_impl_prod C6_ set_impl_fset)))
                (fn (x, y) =>
                  (q, (x, (y, fimage
                                (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                                  ccompare_prod A4_
                                    (ccompare_prod
                                      (ccompare_cproper_interval B4_)
                                      (ccompare_prod
(ccompare_cproper_interval C4_) A4_)))
                                (A3_, A4_, A6_) (fn aa => snd (snd (snd aa)))
                                (ffilter
                                  (ceq_prod A3_
                                     (ceq_prod B3_ (ceq_prod C3_ A3_)),
                                    ccompare_prod A4_
                                      (ccompare_prod
(ccompare_cproper_interval B4_)
(ccompare_prod (ccompare_cproper_interval C4_) A4_)))
                                  (fn t =>
                                    equal_proda B5_ C5_
                                      (fst (snd t), fst (snd (snd t))) (x, y))
                                  qts)))))
                a
            end)
          nexts);
    val donesa =
      sup_fset (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset) dones nexts;
    val tsa =
      sup_fset
        (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
           (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
          ccompare_prod ccompare_fset
            (ccompare_prod (ccompare_cproper_interval B4_)
              (ccompare_prod (ccompare_cproper_interval C4_) ccompare_fset)))
        ts qtrans;
    val nextsa =
      minus_fset (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
        (fimage
          (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
             (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
            ccompare_prod ccompare_fset
              (ccompare_prod (ccompare_cproper_interval B4_)
                (ccompare_prod (ccompare_cproper_interval C4_) ccompare_fset)))
          (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset)
          (fn a => snd (snd (snd a))) qtrans)
        donesa;
  in
    (if eq (equal_fset
             (cenum_fset, ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset,
               equal_fset (A2_, A3_, A4_, A5_)))
          nextsa
          (bot_fset
            (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset))
      then tsa
      else make_observable_transitions (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
             (B1_, B2_, B3_, B4_, B5_, B6_) (C1_, C2_, C3_, C4_, C5_, C6_)
             base_trans nextsa donesa tsa)
  end;

fun create_unconnected_fsm_from_fsets_impl (A1_, A2_, A3_, A4_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_) q ns ins outs =
  FSMWPI
    (q, insert (A1_, A2_) q (fset ns), fset ins, fset outs,
      bot_set
        (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_)),
          ccompare_prod A2_ (ccompare_prod B2_ (ccompare_prod C2_ A2_)),
          set_impl_prod A4_ (set_impl_prod B4_ (set_impl_prod C3_ A4_))),
      emptya (ccompare_prod A2_ B2_, mapping_impl_prod A3_ B3_),
      emptya (ccompare_prod A2_ B2_, mapping_impl_prod A3_ B3_));

fun create_unconnected_fsm_from_fsetsb (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_) q ns ins outs =
  Fsm_with_precomputations
    (create_unconnected_fsm_from_fsets_impl (A1_, A2_, A3_, A4_)
      (B1_, B2_, B3_, B4_) (C1_, C2_, C3_) q ns ins outs);

fun create_unconnected_fsm_from_fsetsa (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_) q ns ins outs =
  FSMWP (create_unconnected_fsm_from_fsetsb (A1_, A2_, A3_, A4_)
          (B1_, B2_, B3_, B4_) (C1_, C2_, C3_) q ns ins outs);

fun create_unconnected_fsm_from_fsets (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_) xc xd xe xf =
  Abs_fsm
    (create_unconnected_fsm_from_fsetsa (A1_, A2_, A3_, A4_)
      (B1_, B2_, B3_, B4_) (C1_, C2_, C3_) xc xd xe xf);

fun make_observable (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m =
  let
    val initial_trans =
      let
        val qts =
          ffilter
            (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
              ccompare_prod A4_
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_prod (ccompare_cproper_interval C4_) A4_)))
            (fn t => eq A5_ (fst t) (initial m))
            (ftransitions (A3_, A4_, A5_, A6_, A7_)
              (B3_, ccompare_cproper_interval B4_, B5_, B7_, B8_)
              (C3_, ccompare_cproper_interval C4_, C5_, C7_, C8_) m);
        val a =
          fimage
            (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
              ccompare_prod A4_
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_prod (ccompare_cproper_interval C4_) A4_)))
            (ceq_prod B3_ C3_,
              ccompare_prod (ccompare_cproper_interval B4_)
                (ccompare_cproper_interval C4_),
              set_impl_prod B8_ C8_)
            (fn t => (fst (snd t), fst (snd (snd t)))) qts;
      in
        fimage
          (ceq_prod B3_ C3_,
            ccompare_prod (ccompare_cproper_interval B4_)
              (ccompare_cproper_interval C4_))
          (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
             (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
            ccompare_prod ccompare_fset
              (ccompare_prod (ccompare_cproper_interval B4_)
                (ccompare_prod (ccompare_cproper_interval C4_) ccompare_fset)),
            set_impl_prod set_impl_fset
              (set_impl_prod B8_ (set_impl_prod C8_ set_impl_fset)))
          (fn (x, y) =>
            (finsert (A3_, A4_) (initial m) (bot_fset (A3_, A4_, A7_)),
              (x, (y, fimage
                        (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                          ccompare_prod A4_
                            (ccompare_prod (ccompare_cproper_interval B4_)
                              (ccompare_prod (ccompare_cproper_interval C4_)
                                A4_)))
                        (A3_, A4_, A7_) (fn aa => snd (snd (snd aa)))
                        (ffilter
                          (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_)),
                            ccompare_prod A4_
                              (ccompare_prod (ccompare_cproper_interval B4_)
                                (ccompare_prod (ccompare_cproper_interval C4_)
                                  A4_)))
                          (fn t =>
                            equal_proda B5_ C5_ (fst (snd t), fst (snd (snd t)))
                              (x, y))
                          qts)))))
          a
      end;
    val nexts =
      minus_fset (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
        (fimage
          (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
             (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
            ccompare_prod ccompare_fset
              (ccompare_prod (ccompare_cproper_interval B4_)
                (ccompare_prod (ccompare_cproper_interval C4_) ccompare_fset)))
          (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset)
          (fn a => snd (snd (snd a))) initial_trans)
        (finsert (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
          (finsert (A3_, A4_) (initial m) (bot_fset (A3_, A4_, A7_)))
          (bot_fset
            (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset)));
    val ptransitions =
      make_observable_transitions
        (finite_UNIV_card_UNIV A1_, A2_, A3_, A4_, A5_, A7_, A8_)
        (B1_, B2_, B3_, B4_, B5_, B8_) (C1_, C2_, C3_, C4_, C5_, C8_)
        (ftransitions (A3_, A4_, A5_, A6_, A7_)
          (B3_, ccompare_cproper_interval B4_, B5_, B7_, B8_)
          (C3_, ccompare_cproper_interval C4_, C5_, C7_, C8_) m)
        nexts
        (finsert (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
          (finsert (A3_, A4_) (initial m) (bot_fset (A3_, A4_, A7_)))
          (bot_fset
            (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset)))
        initial_trans;
    val pstates =
      finsert (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset)
        (finsert (A3_, A4_) (initial m) (bot_fset (A3_, A4_, A7_)))
        (fimage
          (ceq_prod (ceq_fset (A2_, A3_, A4_, A5_))
             (ceq_prod B3_ (ceq_prod C3_ (ceq_fset (A2_, A3_, A4_, A5_)))),
            ccompare_prod ccompare_fset
              (ccompare_prod (ccompare_cproper_interval B4_)
                (ccompare_prod (ccompare_cproper_interval C4_) ccompare_fset)))
          (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, set_impl_fset)
          (fn a => snd (snd (snd a))) ptransitions);
    val ma =
      create_unconnected_fsm_from_fsets
        (ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset, mapping_impl_fset,
          set_impl_fset)
        (B3_, ccompare_cproper_interval B4_, B6_, B8_)
        (C3_, ccompare_cproper_interval C4_, C8_)
        (finsert (A3_, A4_) (initial m) (bot_fset (A3_, A4_, A7_))) pstates
        (finputs (B3_, ccompare_cproper_interval B4_, B5_, B7_, B8_) m)
        (foutputs (C3_, ccompare_cproper_interval C4_, C5_, C7_, C8_) m);
  in
    add_transitions
      (card_UNIV_fset A1_, ceq_fset (A2_, A3_, A4_, A5_), ccompare_fset,
        equal_fset (A2_, A3_, A4_, A5_), mapping_impl_fset, set_impl_fset)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C8_) ma (fset ptransitions)
  end;

fun pair_framework A_ (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) ma m
  get_initial_test_suite get_pairs get_separating_traces =
  let
    val ts = get_initial_test_suite ma m;
    val d = get_pairs ma m;
    val dist_extension =
      (fn t => fn (a, b) =>
        let
          val (alpha, q) = a;
        in
          (fn (beta, qa) =>
            let
              val tDist = get_separating_traces ma ((alpha, q), (beta, qa)) t;
            in
              combine_after
                (ccompare_prod B1_ C1_, equal_prod B2_ C2_,
                  mapping_impl_prod B3_ C3_)
                (combine_after
                  (ccompare_prod B1_ C1_, equal_prod B2_ C2_,
                    mapping_impl_prod B3_ C3_)
                  t alpha tDist)
                beta tDist
            end)
        end
          b);
  in
    foldl dist_extension ts d
  end;

fun fset_states_to_list_states (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m =
  rename_states (ceq_fset (A1_, A2_, A3_, A4_), ccompare_fset)
    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
    (card_UNIV_list, ceq_list A2_, ccompare_list A3_, equal_list A4_,
      mapping_impl_list, set_impl_list)
    m (sorted_list_of_fset (A2_, A3_, A4_, A5_));

fun set_states_to_list_states (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m =
  rename_states
    (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
      ccompare_set (A1_, A3_, A4_, A7_))
    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
    (card_UNIV_list, ceq_list A3_,
      ccompare_list (ccompare_cproper_interval A4_), equal_list A5_,
      mapping_impl_list, set_impl_list)
    m (sorted_list_of_set (A3_, ccompare_cproper_interval A4_, A5_, A6_));

fun initial_ofsm_table (A1_, A2_, A3_, A4_, A5_) m =
  tabulate (A2_, A3_, A4_) (states_as_list (A1_, A2_, A3_, A5_) m)
    (fn _ => states m);

fun next_ofsm_table (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m prev_table =
  tabulate (A3_, A4_, A5_) (states_as_list (A2_, A3_, A4_, A6_) m)
    (fn q =>
      filter (A2_, A3_)
        (fn qa =>
          ball (B1_, B2_) (inputs m)
            (fn x =>
              ball (C1_, C2_) (outputs m)
                (fn y =>
                  (case h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q x y
                    of NONE =>
                      is_none (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m qa x y)
                    | SOME qT =>
                      (case h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m qa x y
                        of NONE => false
                        | SOME qTa =>
                          set_eq (A1_, A2_, A3_)
                            (lookup_default (A3_, A4_) (bot_set (A2_, A3_, A7_))
                              prev_table qT)
                            (lookup_default (A3_, A4_) (bot_set (A2_, A3_, A7_))
                              prev_table qTa))))))
        (lookup_default (A3_, A4_) (bot_set (A2_, A3_, A7_)) prev_table q));

fun compute_ofsm_table_list (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m k =
  rev (foldr
        (fn _ => fn prev =>
          next_ofsm_table (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
            (C1_, C2_, C3_) m (hd prev) ::
            prev)
        (upt zero_nat k) [initial_ofsm_table (A2_, A3_, A4_, A5_, A6_) m]);

fun compute_ofsm_tables (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m k =
  bulkload
    (compute_ofsm_table_list (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
      (C1_, C2_, C3_) m k);

fun minimise_refined (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_) (C1_, C2_, C3_, C4_, C5_, C6_) m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B1_, B2_, B3_) (C1_, C2_, C3_) m
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) m) one_nata);
    val eq_class =
      lookup_default (ccompare_cproper_interval A4_, A5_)
        (bot_set (A3_, ccompare_cproper_interval A4_, A8_))
        (the (lookupa (ccompare_nat, equal_nat) tables
               (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) m)
                 one_nata)));
    val ts =
      image (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_)),
              ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_prod B2_
                  (ccompare_prod C2_ (ccompare_cproper_interval A4_))))
        (ceq_prod (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
           (ceq_prod B1_
             (ceq_prod C1_
               (ceq_set (A2_, A3_, ccompare_cproper_interval A4_)))),
          ccompare_prod
            (ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A8_))
            (ccompare_prod B2_
              (ccompare_prod C2_
                (ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A8_)))),
          set_impl_prod set_impl_set
            (set_impl_prod B6_ (set_impl_prod C6_ set_impl_set)))
        (fn t =>
          (eq_class (fst t),
            (fst (snd t), (fst (snd (snd t)), eq_class (snd (snd (snd t)))))))
        (transitions m);
    val q0 = eq_class (initial m);
    val eq_states =
      fimage (A3_, ccompare_cproper_interval A4_)
        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
          ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A8_), set_impl_set)
        eq_class
        (fstates (A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_) m);
    val ma =
      create_unconnected_fsm_from_fsets
        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
          ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A8_),
          mapping_impl_set, set_impl_set)
        (B1_, B2_, B4_, B6_) (C1_, C2_, C6_) q0 eq_states
        (finputs (B1_, B2_, B3_, B5_, B6_) m)
        (foutputs (C1_, C2_, C3_, C5_, C6_) m);
  in
    add_transitions
      (card_UNIV_set A1_, ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
        ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A8_),
        equal_set (A2_, A3_, ccompare_cproper_interval A4_, A5_),
        mapping_impl_set, set_impl_set)
      (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_, C6_) ma ts
  end;

fun restrict_to_reachable_states (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_, C5_) m =
  let
    val path_assignments =
      reaching_paths_up_to_depth (A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
        (C1_, C2_, C3_, C5_) m
        (insert (A2_, A3_) (initial m) (bot_set (A2_, A3_, A6_)))
        (bot_set (A2_, A3_, A6_))
        (fun_upd A4_ (fn _ => NONE) (initial m) (SOME []))
        (minus_nat (size (A1_, A2_, A3_) m) one_nata);
  in
    filter_states (A1_, A2_, A3_, A6_) (B1_, B2_) (C1_, C2_, C3_, C4_) m
      (fn q => not (is_none (path_assignments q)))
  end;

fun to_prime (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m =
  restrict_to_reachable_states
    (card_UNIV_integer, ceq_integer, ccompare_integer, equal_integer,
      linorder_integer, set_impl_integer)
    (B3_, ccompare_cproper_interval B4_, B5_, B7_)
    (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_)
    (index_states_integer
      (ceq_list ceq_nat, ccompare_list ccompare_nat, equal_list equal_nat,
        linorder_list (equal_nat, linorder_nat))
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C8_)
      (set_states_to_list_states
        (finite_UNIV_nat, cenum_nat, ceq_nat, cproper_interval_nat, equal_nat,
          linorder_nat, set_impl_nat)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C8_)
        (minimise_refined
          (card_UNIV_nat, cenum_nat, ceq_nat, cproper_interval_nat, equal_nat,
            mapping_impl_nat, linorder_nat, set_impl_nat)
          (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_, B8_)
          (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_, C8_)
          (index_states
            (ceq_list A3_, ccompare_list A4_, equal_list A5_,
              linorder_list (A5_, A6_))
            (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
            (C3_, ccompare_cproper_interval C4_, C5_, C6_, C8_)
            (fset_states_to_list_states (A2_, A3_, A4_, A5_, A6_)
              (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
              (C3_, ccompare_cproper_interval C4_, C5_, C6_, C8_)
              (make_observable (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
                (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_)
                (restrict_to_reachable_states (A1_, A3_, A4_, A5_, A6_, A7_)
                  (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                  (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) m)))))));

fun maximal_prefix_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q [] = []
  | maximal_prefix_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m q
    ((x, y) :: io) =
    (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q x y of NONE => []
      | SOME qa =>
        (x, y) ::
          maximal_prefix_in_language (A1_, A2_) (B1_, B2_) (C1_, C2_) m qa io);

fun uminus_int k = Int_of_integer (IntInf.~ (integer_of_int k));

fun initial_preamble (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_)
  xa =
  Abs_fsm
    (initial_singleton (A1_, A2_, A3_, A4_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_)
      (fsm_impl_of_fsm xa));

fun maximum_fst_prefixes (A1_, A2_) (B1_, B2_) (MPT m) xs ys =
  (case xs
    of [] => (if is_leaf (ccompare_prod A1_ B1_) (MPT m) then [[]] else [])
    | x :: xsa =>
      (if is_leaf (ccompare_prod A1_ B1_) (MPT m) then [[]]
        else concat
               (map_filter
                 (fn xa =>
                   (if not (is_none
                             (lookupa
                               (ccompare_prod A1_ B1_, equal_prod A2_ B2_) m
                               (x, xa)))
                     then SOME (mapa (fn a => (x, xa) :: a)
                                 (maximum_fst_prefixes (A1_, A2_) (B1_, B2_)
                                   (the (lookupa
  (ccompare_prod A1_ B1_, equal_prod A2_ B2_) m (x, xa)))
                                   xsa ys))
                     else NONE))
                 ys)));

fun pairs_to_distinguish (A1_, A2_) B_ C_ m v x rstates =
  let
    val pi = mapa (fn q => (v q, q)) rstates;
    val a = product pi pi;
    val b =
      product pi
        (maps (fn q =>
                mapa (fn (tau, aa) =>
                       (v q @ mapa (fn t => (fst (snd t), fst (snd (snd t))))
                                tau,
                         aa))
                  (x q))
          rstates);
    val c =
      maps (fn q =>
             maps (fn (tau, qa) =>
                    mapa (fn taua =>
                           ((v q @ mapa (fn t =>
  (fst (snd t), fst (snd (snd t))))
                                     taua,
                              target q taua),
                             (v q @ mapa (fn t =>
   (fst (snd t), fst (snd (snd t))))
                                      tau,
                               qa)))
                      (prefixes tau))
               (x q))
        rstates;
  in
    filtera (fn (aa, ba) => let
                              val (_, q) = aa;
                            in
                              (fn (_, qa) => not (eq A1_ q qa))
                            end
                              ba)
      (a @ b @ c)
  end;

fun canonical_separatorb (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m q1
  q2 =
  canonical_separator (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
    (productd (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
      (from_FSM (A3_, ccompare_cproper_interval A4_) m q1)
      (from_FSM (A3_, ccompare_cproper_interval A4_) m q2))
    q1 q2;

fun select_diverging_ofsm_table_io (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q1 q2 k =
  let
    val ins = inputs_as_list (B1_, B2_, B3_, B4_) m;
    val outs = outputs_as_list (C1_, C2_, C3_, C4_) m;
    val table =
      ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m
        (fn _ => states m) (minus_nat k one_nata);
    val f =
      (fn (x, y) =>
        (case (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q1 x y,
                h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q2 x y)
          of (NONE, NONE) => NONE
          | (NONE, SOME q2a) => SOME ((x, y), (NONE, SOME q2a))
          | (SOME q1a, NONE) => SOME ((x, y), (SOME q1a, NONE))
          | (SOME q1a, SOME q2a) =>
            (if not (set_eq (A1_, A2_, A3_) (table q1a) (table q2a))
              then SOME ((x, y), (SOME q1a, SOME q2a)) else NONE)));
  in
    hd (map_filter f (product ins outs))
  end;

fun assemble_distinguishing_sequence_from_ofsm_table (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q1 q2 k =
  (if equal_nata k zero_nat then []
    else (case select_diverging_ofsm_table_io (A1_, A2_, A3_, A4_, A5_)
                 (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q1 q2
                 (suc (minus_nat k one_nata))
           of ((x, y), (NONE, _)) => [(x, y)]
           | ((x, y), (SOME _, NONE)) => [(x, y)]
           | ((x, y), (SOME q1a, SOME q2a)) =>
             (x, y) ::
               assemble_distinguishing_sequence_from_ofsm_table
                 (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_, B4_)
                 (C1_, C2_, C3_, C4_) m q1a q2a (minus_nat k one_nata)));

fun find_first_distinct_ofsm_table_no_check (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_) (C1_, C2_, C3_) m q1 q2 k =
  (if not (set_eq (A1_, A2_, A3_)
            (ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
              (C1_, C2_, C3_) m (fn _ => states m) k q1)
            (ofsm_table (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
              (C1_, C2_, C3_) m (fn _ => states m) k q2))
    then k
    else find_first_distinct_ofsm_table_no_check (A1_, A2_, A3_, A4_, A5_)
           (B1_, B2_, B3_) (C1_, C2_, C3_) m q1 q2 (suc k));

fun find_first_distinct_ofsm_table_gta (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m q1 q2 k =
  (if member (A2_, A3_) q1 (states m) andalso
        (member (A2_, A3_) q2 (states m) andalso
          not (member (A2_, A3_) q2
                (ofsm_table_fix (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
                  (C1_, C2_, C3_) m (fn _ => states m) zero_nat q1)))
    then find_first_distinct_ofsm_table_no_check (A1_, A2_, A3_, A4_, A5_)
           (B1_, B2_, B3_) (C1_, C2_, C3_) m q1 q2 k
    else zero_nat);

fun find_first_distinct_ofsm_table_gt (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m q1 q2 k =
  find_first_distinct_ofsm_table_gta (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_)
    (C1_, C2_, C3_) m q1 q2 k;

fun get_distinguishing_sequence_from_ofsm_tables (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q1 q2 =
  let
    val a =
      find_first_distinct_ofsm_table_gt (A1_, A2_, A3_, A4_, A5_)
        (B1_, B2_, B3_) (C1_, C2_, C3_) m q1 q2 zero_nat;
  in
    assemble_distinguishing_sequence_from_ofsm_table (A1_, A2_, A3_, A4_, A5_)
      (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q1 q2 a
  end;

fun get_HSI (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m q =
  from_list
    (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
    (map_filter
      (fn x =>
        (if not (eq A4_ q x)
          then SOME (get_distinguishing_sequence_from_ofsm_tables
                      (A1_, A2_, A3_, A4_, A6_) (B1_, B2_, B3_, B5_)
                      (C1_, C2_, C3_, C5_) m q x)
          else NONE))
      (states_as_list (A2_, A3_, A4_, A5_) m));

fun has_leaf (A1_, A2_, A3_) (B1_, B2_, B3_) t g cg_lookup alpha =
  not (is_none
        (find (is_maximal_in
                (ccompare_prod A1_ B1_, equal_prod A2_ B2_,
                  mapping_impl_prod A3_ B3_)
                t)
          (alpha :: cg_lookup g alpha)));

val empty_cg_empty : unit = ();

fun empty_cg_merge g u v = empty_cg_empty;

fun get_state_cover_assignment (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m =
  let
    val path_assignments =
      reaching_paths_up_to_depth (A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
        (C1_, C2_, C3_, C4_) m
        (insert (A2_, A3_) (initial m) (bot_set (A2_, A3_, A6_)))
        (bot_set (A2_, A3_, A6_))
        (fun_upd A4_ (fn _ => NONE) (initial m) (SOME []))
        (minus_nat (size (A1_, A2_, A3_) m) one_nata);
  in
    (fn q =>
      (case path_assignments q of NONE => []
        | SOME a => mapa (fn t => (fst (snd t), fst (snd (snd t)))) a))
  end;

fun acyclic_language_intersection (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  (D1_, D2_, D3_, D4_, D5_, D6_, D7_) m a =
  let
    val p =
      productd (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
        (D1_, D2_, D3_, D4_, D5_, D6_, D7_) m a;
  in
    image (ceq_list
             (ceq_prod (ceq_prod A3_ D3_)
               (ceq_prod B3_ (ceq_prod C3_ (ceq_prod A3_ D3_)))),
            ccompare_list
              (ccompare_prod
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval D4_))
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_prod (ccompare_cproper_interval C4_)
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_cproper_interval D4_))))))
      (ceq_list (ceq_prod B3_ C3_),
        ccompare_list
          (ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_)),
        set_impl_list)
      (mapa (fn t => (fst (snd t), fst (snd (snd t)))))
      (acyclic_paths_up_to_length
        (ceq_prod A3_ D3_,
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_cproper_interval D4_),
          equal_prod A5_ D5_, mapping_impl_prod A6_ D6_, set_impl_prod A7_ D7_)
        (B3_, ccompare_cproper_interval B4_, B7_)
        (C3_, ccompare_cproper_interval C4_, C7_) p (initial p)
        (minus_nat (size (D1_, D3_, ccompare_cproper_interval D4_) a) one_nata))
  end;

fun test_suite_to_io_maximal (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  (D1_, D2_, D3_, D4_, D5_, D6_, D7_, D8_) m
  (Test_Suite (prs, tps, rd_targets, atcs)) =
  remove_proper_prefixes
    (ceq_prod B3_ C3_,
      ccompare_prod (ccompare_cproper_interval B4_)
        (ccompare_cproper_interval C4_),
      equal_prod B5_ C5_)
    (sup_setb
      (finite_UNIV_list, cenum_list, ceq_list (ceq_prod B3_ C3_),
        cproper_interval_list
          (ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_)),
        set_impl_list)
      (image
        (ceq_prod A3_
           (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
             (B2_, B3_, ccompare_cproper_interval B4_)
             (C2_, C3_, ccompare_cproper_interval C4_)),
          ccompare_prod (ccompare_cproper_interval A4_) ccompare_fsm)
        (ceq_set
           (cenum_list, ceq_list (ceq_prod B3_ C3_),
             ccompare_cproper_interval
               (cproper_interval_list
                 (ccompare_prod (ccompare_cproper_interval B4_)
                   (ccompare_cproper_interval C4_)))),
          ccompare_set
            (finite_UNIV_list, ceq_list (ceq_prod B3_ C3_),
              cproper_interval_list
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_cproper_interval C4_)),
              set_impl_list),
          set_impl_set)
        (fn (q, p) =>
          sup_seta
            (ceq_list (ceq_prod B3_ C3_),
              ccompare_list
                (ccompare_prod (ccompare_cproper_interval B4_)
                  (ccompare_cproper_interval C4_)))
            (lS_acyclic (A1_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A8_)
              (B3_, ccompare_cproper_interval B4_, B8_)
              (C3_, ccompare_cproper_interval C4_, C7_) p (initial p))
            (sup_setb
              (finite_UNIV_list, cenum_list, ceq_list (ceq_prod B3_ C3_),
                cproper_interval_list
                  (ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_cproper_interval C4_)),
                set_impl_list)
              (image
                (ceq_list (ceq_prod B3_ C3_),
                  ccompare_list
                    (ccompare_prod (ccompare_cproper_interval B4_)
                      (ccompare_cproper_interval C4_)))
                (ceq_set
                   (cenum_list, ceq_list (ceq_prod B3_ C3_),
                     ccompare_cproper_interval
                       (cproper_interval_list
                         (ccompare_prod (ccompare_cproper_interval B4_)
                           (ccompare_cproper_interval C4_)))),
                  ccompare_set
                    (finite_UNIV_list, ceq_list (ceq_prod B3_ C3_),
                      cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval B4_)
                          (ccompare_cproper_interval C4_)),
                      set_impl_list),
                  set_impl_set)
                (fn ioP =>
                  sup_setb
                    (finite_UNIV_list, cenum_list, ceq_list (ceq_prod B3_ C3_),
                      cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval B4_)
                          (ccompare_cproper_interval C4_)),
                      set_impl_list)
                    (image
                      (ceq_list
                         (ceq_prod A3_ (ceq_prod B3_ (ceq_prod C3_ A3_))),
                        ccompare_list
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_prod (ccompare_cproper_interval B4_)
                              (ccompare_prod (ccompare_cproper_interval C4_)
                                (ccompare_cproper_interval A4_)))))
                      (ceq_set
                         (cenum_list, ceq_list (ceq_prod B3_ C3_),
                           ccompare_cproper_interval
                             (cproper_interval_list
                               (ccompare_prod (ccompare_cproper_interval B4_)
                                 (ccompare_cproper_interval C4_)))),
                        ccompare_set
                          (finite_UNIV_list, ceq_list (ceq_prod B3_ C3_),
                            cproper_interval_list
                              (ccompare_prod (ccompare_cproper_interval B4_)
                                (ccompare_cproper_interval C4_)),
                            set_impl_list),
                        set_impl_set)
                      (fn pt =>
                        insert
                          (ceq_list (ceq_prod B3_ C3_),
                            ccompare_list
                              (ccompare_prod (ccompare_cproper_interval B4_)
                                (ccompare_cproper_interval C4_)))
                          (ioP @
                            mapa (fn t => (fst (snd t), fst (snd (snd t)))) pt)
                          (sup_setb
                            (finite_UNIV_list, cenum_list,
                              ceq_list (ceq_prod B3_ C3_),
                              cproper_interval_list
                                (ccompare_prod (ccompare_cproper_interval B4_)
                                  (ccompare_cproper_interval C4_)),
                              set_impl_list)
                            (image (A3_, ccompare_cproper_interval A4_)
                              (ceq_set
                                 (cenum_list, ceq_list (ceq_prod B3_ C3_),
                                   ccompare_cproper_interval
                                     (cproper_interval_list
                                       (ccompare_prod
 (ccompare_cproper_interval B4_) (ccompare_cproper_interval C4_)))),
                                ccompare_set
                                  (finite_UNIV_list,
                                    ceq_list (ceq_prod B3_ C3_),
                                    cproper_interval_list
                                      (ccompare_prod
(ccompare_cproper_interval B4_) (ccompare_cproper_interval C4_)),
                                    set_impl_list),
                                set_impl_set)
                              (fn qa =>
                                sup_setb
                                  (finite_UNIV_list, cenum_list,
                                    ceq_list (ceq_prod B3_ C3_),
                                    cproper_interval_list
                                      (ccompare_prod
(ccompare_cproper_interval B4_) (ccompare_cproper_interval C4_)),
                                    set_impl_list)
                                  (image
                                    (ceq_prod
                                       (ceq_fsm
 (D2_, D3_, ccompare_cproper_interval D4_, D5_)
 (B2_, B3_, ccompare_cproper_interval B4_)
 (C2_, C3_, ccompare_cproper_interval C4_))
                                       (ceq_prod D3_ D3_),
                                      ccompare_prod ccompare_fsm
(ccompare_prod (ccompare_cproper_interval D4_) (ccompare_cproper_interval D4_)))
                                    (ceq_set
                                       (cenum_list, ceq_list (ceq_prod B3_ C3_),
 ccompare_cproper_interval
   (cproper_interval_list
     (ccompare_prod (ccompare_cproper_interval B4_)
       (ccompare_cproper_interval C4_)))),
                                      ccompare_set
(finite_UNIV_list, ceq_list (ceq_prod B3_ C3_),
  cproper_interval_list
    (ccompare_prod (ccompare_cproper_interval B4_)
      (ccompare_cproper_interval C4_)),
  set_impl_list),
                                      set_impl_set)
                                    (fn (a, (_, _)) =>
                                      image
(ceq_list (ceq_prod B3_ C3_),
  ccompare_list
    (ccompare_prod (ccompare_cproper_interval B4_)
      (ccompare_cproper_interval C4_)))
(ceq_list (ceq_prod B3_ C3_),
  ccompare_list
    (ccompare_prod (ccompare_cproper_interval B4_)
      (ccompare_cproper_interval C4_)),
  set_impl_list)
(fn io_atc => ioP @ mapa (fn t => (fst (snd t), fst (snd (snd t)))) pt @ io_atc)
(remove_proper_prefixes
  (ceq_prod B3_ C3_,
    ccompare_prod (ccompare_cproper_interval B4_)
      (ccompare_cproper_interval C4_),
    equal_prod B5_ C5_)
  (acyclic_language_intersection (A1_, A2_, A3_, A4_, A5_, A6_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
    (D1_, D2_, D3_, D4_, D5_, D6_, D8_)
    (from_FSM (A3_, ccompare_cproper_interval A4_) m (target q pt)) a)))
                                    (atcs (target q pt, qa))))
                              (rd_targets (q, pt)))))
                      (tps q)))
                (remove_proper_prefixes
                  (ceq_prod B3_ C3_,
                    ccompare_prod (ccompare_cproper_interval B4_)
                      (ccompare_cproper_interval C4_),
                    equal_prod B5_ C5_)
                  (lS_acyclic
                    (A1_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A8_)
                    (B3_, ccompare_cproper_interval B4_, B8_)
                    (C3_, ccompare_cproper_interval C4_, C7_) p (initial p))))))
        prs));

fun empty_cg_insert g v = empty_cg_empty;

fun empty_cg_lookup g v = [v];

fun extend_until_conflict (A1_, A2_) non_confl_set candidates xs k =
  (if equal_nata k zero_nat then xs
    else (case dropWhile
                 (fn x =>
                   not (is_none
                         (find (fn y =>
                                 not (member
                                       (ceq_prod A1_ A1_, ccompare_prod A2_ A2_)
                                       (x, y) non_confl_set))
                           xs)))
                 candidates
           of [] => xs
           | c :: cs =>
             extend_until_conflict (A1_, A2_) non_confl_set cs (c :: xs)
               (minus_nat k one_nata)));

fun empty_cg_initial m t = empty_cg_empty;

fun paths_up_to_length_with_targets q hM iM k =
  (if equal_nata k zero_nat then [([], q)]
    else ([], q) ::
           maps (fn x =>
                  maps (fn t =>
                         mapa (fn (p, a) => (t :: p, a))
                           (paths_up_to_length_with_targets (snd (snd (snd t)))
                             hM iM (minus_nat k one_nata)))
                    (hM q x))
             iM);

fun get_pairs_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) v ma m =
  let
    val rstates =
      reachable_states_as_list (A1_, A2_, A3_, A4_, A6_, A7_)
        (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C4_) ma;
    val n =
      card (A1_, A2_, A3_)
        (reachable_states (A1_, A2_, A3_, A4_, A6_, A7_) (B1_, B2_, B3_, B5_)
          (C1_, C2_, C3_, C4_) ma);
    val iM = inputs_as_list (B1_, B2_, B3_, B5_) ma;
    val hMap =
      map_of (equal_prod A4_ B3_)
        (mapa (fn (q, x) =>
                ((q, x),
                  mapa (fn (y, qa) => (q, (x, (y, qa))))
                    (sorted_list_of_set
                      (ceq_prod C1_ A2_, ccompare_prod C2_ A3_,
                        equal_prod C3_ A4_, linorder_prod C4_ A6_)
                      (h (A2_, A3_, A4_, A5_, A7_) (B2_, B3_, B4_)
                        (C1_, C2_, C5_) ma (q, x)))))
          (product (states_as_list (A2_, A3_, A4_, A6_) ma) iM));
    val hM = (fn q => fn x => (case hMap (q, x) of NONE => [] | SOME ts => ts));
    val pairs =
      pairs_to_distinguish (A4_, A6_) B5_ C4_ ma v
        (fn q =>
          paths_up_to_length_with_targets q hM iM
            (plus_nat (minus_nat m n) one_nata))
        rstates;
  in
    pairs
  end;

val simple_cg_empty : ('a list) fset list = [];

fun can_merge_by_intersection (A1_, A2_) x1 x2 =
  bex (ceq_list A1_, ccompare_list A2_) (fset x1)
    (fn alpha => fmember (ceq_list A1_, ccompare_list A2_) alpha x2);

fun simple_cg_closure_phase_2_helper (A1_, A2_) x1 xs =
  let
    val (x2s, others) =
      separate_by (can_merge_by_intersection (A1_, A2_) x1) xs;
    val x1Union = foldl (sup_fset (ceq_list A1_, ccompare_list A2_)) x1 x2s;
  in
    (not (null x2s), (x1Union, others))
  end;

fun simple_cg_closure_phase_2a (A1_, A2_) [] (b, done) = (b, done)
  | simple_cg_closure_phase_2a (A1_, A2_) (x :: xs) (b, done) =
    (case simple_cg_closure_phase_2_helper (A1_, A2_) x xs
      of (true, (xa, xsa)) =>
        simple_cg_closure_phase_2a (A1_, A2_) xsa (true, xa :: done)
      | (false, (_, _)) =>
        simple_cg_closure_phase_2a (A1_, A2_) xs (b, x :: done));

fun simple_cg_closure_phase_2 (A1_, A2_) xs =
  simple_cg_closure_phase_2a (A1_, A2_) xs (false, []);

fun can_merge_by_suffix (A1_, A2_, A3_) x x1 x2 =
  bex (ceq_list A1_, ccompare_list A2_) (fset x)
    (fn ys =>
      bex (ceq_list A1_, ccompare_list A2_) (fset x1)
        (fn ys1 =>
          is_prefix A3_ ys ys1 andalso
            bex (ceq_list A1_, ccompare_list A2_) (fset x)
              (fn ysa =>
                fmember (ceq_list A1_, ccompare_list A2_)
                  (ysa @ drop (size_list ys) ys1) x2)));

fun simple_cg_closure_phase_1_helpera (A1_, A2_, A3_) x x1 xs =
  let
    val (x2s, others) =
      separate_by (can_merge_by_suffix (A1_, A2_, A3_) x x1) xs;
    val x1Union = foldl (sup_fset (ceq_list A1_, ccompare_list A2_)) x1 x2s;
  in
    (not (null x2s), (x1Union, others))
  end;

fun simple_cg_closure_phase_1_helper (A1_, A2_, A3_) x [] (b, done) = (b, done)
  | simple_cg_closure_phase_1_helper (A1_, A2_, A3_) x (x1 :: xs) (b, done) =
    let
      val (hasChanged, (x1a, xsa)) =
        simple_cg_closure_phase_1_helpera (A1_, A2_, A3_) x x1 xs;
    in
      simple_cg_closure_phase_1_helper (A1_, A2_, A3_) x xsa
        (b orelse hasChanged, x1a :: done)
    end;

fun simple_cg_closure_phase_1 (A1_, A2_, A3_) xs =
  foldl (fn (b, xsa) => fn x =>
          let
            val a =
              simple_cg_closure_phase_1_helper (A1_, A2_, A3_) x xsa
                (false, []);
            val (ba, aa) = a;
          in
            (b orelse ba, aa)
          end)
    (false, xs) xs;

fun simple_cg_closure (A1_, A2_, A3_) g =
  let
    val (hasChanged1, g1) = simple_cg_closure_phase_1 (A1_, A2_, A3_) g;
    val (hasChanged2, g2) = simple_cg_closure_phase_2 (A1_, A2_) g1;
  in
    (if hasChanged1 orelse hasChanged2 then simple_cg_closure (A1_, A2_, A3_) g2
      else g2)
  end;

fun simple_cg_merge (A1_, A2_, A3_) g ys1 ys2 =
  simple_cg_closure (A1_, A2_, A3_)
    (finsert (ceq_list A1_, ccompare_list A2_) ys1
       (finsert (ceq_list A1_, ccompare_list A2_) ys2
         (bot_fset (ceq_list A1_, ccompare_list A2_, set_impl_list))) ::
      g);

fun prefix_pair_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_) (C1_, C2_) q
  (RBT_set t) =
  (case ccompare_proda
          (ccompare_list
            (ccompare_prod (ccompare_cproper_interval A3_)
              (ccompare_prod B2_
                (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
          (ccompare_prod (ccompare_set (A1_, A2_, A3_, A5_))
            (ccompare_set (A1_, A2_, A3_, A5_)))
    of NONE =>
      (raise Fail "prefix_pair_tests RBT_set: ccompare = None")
        (fn _ =>
          prefix_pair_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_) (C1_, C2_) q
            (RBT_set t))
    | SOME _ =>
      set (ceq_prod A2_
             (ceq_prod
               (ceq_list (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_)))) A2_),
            ccompare_prod (ccompare_cproper_interval A3_)
              (ccompare_prod
                (ccompare_list
                  (ccompare_prod (ccompare_cproper_interval A3_)
                    (ccompare_prod B2_
                      (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
                (ccompare_cproper_interval A3_)),
            set_impl_prod A5_ (set_impl_prod set_impl_list A5_))
        (maps (fn (p, (rd, _)) =>
                concat
                  (map_filter
                    (fn x =>
                      (if let
                            val (p1, p2) = x;
                          in
                            not (eq A4_ (target q p1) (target q p2)) andalso
                              (member (A2_, ccompare_cproper_interval A3_)
                                 (target q p1) rd andalso
                                member (A2_, ccompare_cproper_interval A3_)
                                  (target q p2) rd)
                          end
                        then SOME let
                                    val (p1, p2) = x;
                                  in
                                    [(q, (p1, target q p2)),
                                      (q, (p2, target q p1))]
                                  end
                        else NONE))
                    (prefix_pairs p)))
          (keysb
            (ccompare_prod
              (ccompare_list
                (ccompare_prod (ccompare_cproper_interval A3_)
                  (ccompare_prod B2_
                    (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
              (ccompare_prod (ccompare_set (A1_, A2_, A3_, A5_))
                (ccompare_set (A1_, A2_, A3_, A5_))))
            t)));

fun simple_cg_inserta (A1_, A2_, A3_) xs ys =
  (case find (fmember (ceq_list A1_, ccompare_list A2_) ys) xs
    of NONE =>
      finsert (ceq_list A1_, ccompare_list A2_) ys
        (bot_fset (ceq_list A1_, ccompare_list A2_, set_impl_list)) ::
        xs
    | SOME _ => xs);

fun simple_cg_insert (A1_, A2_, A3_) xs ys =
  foldl (simple_cg_inserta (A1_, A2_, A3_)) xs (prefixes ys);

fun dual_set_as_map_image A_ B_ (C1_, C2_, C3_) (D1_, D2_, D3_) (E1_, E2_, E3_)
  (F1_, F2_, F3_) (RBT_set t) f1 f2 =
  (case ccompare_proda A_ B_
    of NONE =>
      (raise Fail "dual_set_as_map_image RBT_set: ccompare = None")
        (fn _ =>
          dual_set_as_map_image A_ B_ (C1_, C2_, C3_) (D1_, D2_, D3_)
            (E1_, E2_, E3_) (F1_, F2_, F3_) (RBT_set t) f1 f2)
    | SOME _ =>
      let
        val mm =
          foldb (ccompare_prod A_ B_)
            (fn kv => fn (m1, m2) =>
              (let
                 val (x, z) = f1 kv;
               in
                 (case lookupa (C1_, C2_) m1 x
                   of NONE =>
                     updateb (C1_, C2_) x
                       (insert (D1_, D2_) z (bot_set (D1_, D2_, D3_))) m1
                   | SOME zs =>
                     updateb (C1_, C2_) x (insert (D1_, D2_) z zs) m1)
               end,
                let
                  val (x, z) = f2 kv;
                in
                  (case lookupa (E1_, E2_) m2 x
                    of NONE =>
                      updateb (E1_, E2_) x
                        (insert (F1_, F2_) z (bot_set (F1_, F2_, F3_))) m2
                    | SOME zs =>
                      updateb (E1_, E2_) x (insert (F1_, F2_) z zs) m2)
                end))
            t (emptya (C1_, C3_), emptya (E1_, E3_));
      in
        (lookupa (C1_, C2_) (fst mm), lookupa (E1_, E2_) (snd mm))
      end);

fun paths_up_to_length_or_condition_with_witnessa (A1_, A2_) (B1_, B2_)
  (C1_, C2_) (D1_, D2_, D3_, D4_, D5_) f p prev k q =
  (if equal_nata k zero_nat
    then (case p prev
           of NONE =>
             bot_set
               (ceq_prod
                  (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                  D3_,
                 ccompare_prod
                   (ccompare_list
                     (ccompare_prod A2_
                       (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                   (ccompare_cproper_interval D4_),
                 set_impl_prod set_impl_list D5_)
           | SOME w =>
             insert
               (ceq_prod
                  (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                  D3_,
                 ccompare_prod
                   (ccompare_list
                     (ccompare_prod A2_
                       (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                   (ccompare_cproper_interval D4_))
               (prev, w)
               (bot_set
                 (ceq_prod
                    (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                    D3_,
                   ccompare_prod
                     (ccompare_list
                       (ccompare_prod A2_
                         (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                     (ccompare_cproper_interval D4_),
                   set_impl_prod set_impl_list D5_)))
    else (case p prev
           of NONE =>
             sup_setb
               (finite_UNIV_prod finite_UNIV_list D1_,
                 cenum_prod cenum_list D2_,
                 ceq_prod
                   (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                   D3_,
                 cproper_interval_prod
                   (cproper_interval_list
                     (ccompare_prod A2_
                       (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                   D4_,
                 set_impl_prod set_impl_list D5_)
               (image
                 (ceq_prod B1_ (ceq_prod C1_ A1_),
                   ccompare_prod B2_ (ccompare_prod C2_ A2_))
                 (ceq_set
                    (cenum_prod cenum_list D2_,
                      ceq_prod
                        (ceq_list
                          (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                        D3_,
                      ccompare_cproper_interval
                        (cproper_interval_prod
                          (cproper_interval_list
                            (ccompare_prod A2_
                              (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                          D4_)),
                   ccompare_set
                     (finite_UNIV_prod finite_UNIV_list D1_,
                       ceq_prod
                         (ceq_list
                           (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                         D3_,
                       cproper_interval_prod
                         (cproper_interval_list
                           (ccompare_prod A2_
                             (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                         D4_,
                       set_impl_prod set_impl_list D5_),
                   set_impl_set)
                 (fn (x, (y, qa)) =>
                   paths_up_to_length_or_condition_with_witnessa (A1_, A2_)
                     (B1_, B2_) (C1_, C2_) (D1_, D2_, D3_, D4_, D5_) f p
                     (prev @ [(q, (x, (y, qa)))]) (minus_nat k one_nata) qa)
                 (f q))
           | SOME w =>
             insert
               (ceq_prod
                  (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                  D3_,
                 ccompare_prod
                   (ccompare_list
                     (ccompare_prod A2_
                       (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                   (ccompare_cproper_interval D4_))
               (prev, w)
               (bot_set
                 (ceq_prod
                    (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
                    D3_,
                   ccompare_prod
                     (ccompare_list
                       (ccompare_prod A2_
                         (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
                     (ccompare_cproper_interval D4_),
                   set_impl_prod set_impl_list D5_))));

fun paths_up_to_length_or_condition_with_witness (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_, B3_) (C1_, C2_, C3_) (D1_, D2_, D3_, D4_, D5_) m p k q =
  (if member (A1_, A2_) q (states m)
    then paths_up_to_length_or_condition_with_witnessa (A1_, A2_) (B1_, B2_)
           (C1_, C2_) (D1_, D2_, D3_, D4_, D5_)
           (h_from (A1_, A2_, A3_, A4_, A5_) (B1_, B2_, B3_) (C1_, C2_, C3_) m)
           p [] k q
    else bot_set
           (ceq_prod (ceq_list (ceq_prod A1_ (ceq_prod B1_ (ceq_prod C1_ A1_))))
              D3_,
             ccompare_prod
               (ccompare_list
                 (ccompare_prod A2_
                   (ccompare_prod B2_ (ccompare_prod C2_ A2_))))
               (ccompare_cproper_interval D4_),
             set_impl_prod set_impl_list D5_));

fun m_traversal_paths_with_witness_up_to_length
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_) (C1_, C2_, C3_) ma q d m k
  = paths_up_to_length_or_condition_with_witness
      (A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_) (B1_, B2_, B3_)
      (C1_, C2_, C3_)
      (finite_UNIV_prod (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
         (finite_UNIV_set (finite_UNIV_card_UNIV A1_)),
        cenum_prod (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_))
          (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_)),
        ceq_prod (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_)),
        cproper_interval_prod (cproper_interval_set (A1_, A3_, A4_, A7_))
          (cproper_interval_set (A1_, A3_, A4_, A7_)),
        set_impl_prod set_impl_set set_impl_set)
      ma (fn p =>
           find (fn da =>
                  less_eq_nat
                    (suc (minus_nat m
                           (card (A1_, A3_, ccompare_cproper_interval A4_)
                             (snd da))))
                    (size_list
                      (filtera
                        (fn t =>
                          member (A3_, ccompare_cproper_interval A4_)
                            (snd (snd (snd t))) (fst da))
                        p)))
             d)
      k q;

fun m_traversal_paths_with_witness (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_) (C1_, C2_, C3_) ma q d m =
  m_traversal_paths_with_witness_up_to_length
    (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_) (C1_, C2_, C3_) ma q d m
    (suc (times_nata (size (A1_, A3_, ccompare_cproper_interval A4_) ma) m));

fun preamble_prefix_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_) (C1_, C2_) q
  (RBT_set t1) (RBT_set t2) =
  (case ccompare_proda
          (ccompare_list
            (ccompare_prod (ccompare_cproper_interval A3_)
              (ccompare_prod B2_
                (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
          (ccompare_prod (ccompare_set (A1_, A2_, A3_, A5_))
            (ccompare_set (A1_, A2_, A3_, A5_)))
    of NONE =>
      (raise Fail "prefix_pair_tests RBT_set: ccompare = None")
        (fn _ =>
          preamble_prefix_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_) (C1_, C2_)
            q (RBT_set t1) (RBT_set t2))
    | SOME _ =>
      (case ccompare (ccompare_cproper_interval A3_)
        of NONE =>
          (raise Fail "prefix_pair_tests RBT_set: ccompare = None")
            (fn _ =>
              preamble_prefix_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_)
                (C1_, C2_) q (RBT_set t1) (RBT_set t2))
        | SOME _ =>
          set (ceq_prod A2_
                 (ceq_prod
                   (ceq_list (ceq_prod A2_ (ceq_prod B1_ (ceq_prod C1_ A2_))))
                   A2_),
                ccompare_prod (ccompare_cproper_interval A3_)
                  (ccompare_prod
                    (ccompare_list
                      (ccompare_prod (ccompare_cproper_interval A3_)
                        (ccompare_prod B2_
                          (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
                    (ccompare_cproper_interval A3_)),
                set_impl_prod A5_ (set_impl_prod set_impl_list A5_))
            (maps (fn (p, (rd, _)) =>
                    concat
                      (map_filter
                        (fn x =>
                          (if let
                                val (p1, q2) = x;
                              in
                                not (eq A4_ (target q p1) q2) andalso
                                  (member (A2_, ccompare_cproper_interval A3_)
                                     (target q p1) rd andalso
                                    member (A2_, ccompare_cproper_interval A3_)
                                      q2 rd)
                              end
                            then SOME let
val (p1, q2) = x;
                                      in
[(q, (p1, q2)), (q2, ([], target q p1))]
                                      end
                            else NONE))
                        (product (prefixes p)
                          (keysb (ccompare_cproper_interval A3_) t2))))
              (keysb
                (ccompare_prod
                  (ccompare_list
                    (ccompare_prod (ccompare_cproper_interval A3_)
                      (ccompare_prod B2_
                        (ccompare_prod C2_ (ccompare_cproper_interval A3_)))))
                  (ccompare_prod (ccompare_set (A1_, A2_, A3_, A5_))
                    (ccompare_set (A1_, A2_, A3_, A5_))))
                t1))));

fun preamble_pair_tests (A1_, A2_, A3_, A4_, A5_) (B1_, B2_) (C1_, C2_) drss rds
  = sup_setb
      (finite_UNIV_prod A1_ (finite_UNIV_prod finite_UNIV_list A1_),
        cenum_prod A2_ (cenum_prod cenum_list A2_),
        ceq_prod A3_
          (ceq_prod (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
            A3_),
        cproper_interval_prod A4_
          (cproper_interval_prod
            (cproper_interval_list
              (ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_prod B2_
                  (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
            A4_),
        set_impl_prod A5_ (set_impl_prod set_impl_list A5_))
      (image
        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
          ccompare_set (A1_, A3_, A4_, A5_))
        (ceq_set
           (cenum_prod A2_ (cenum_prod cenum_list A2_),
             ceq_prod A3_
               (ceq_prod
                 (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                 A3_),
             ccompare_cproper_interval
               (cproper_interval_prod A4_
                 (cproper_interval_prod
                   (cproper_interval_list
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_prod B2_
                         (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                   A4_))),
          ccompare_set
            (finite_UNIV_prod A1_ (finite_UNIV_prod finite_UNIV_list A1_),
              ceq_prod A3_
                (ceq_prod
                  (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                  A3_),
              cproper_interval_prod A4_
                (cproper_interval_prod
                  (cproper_interval_list
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_prod B2_
                        (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                  A4_),
              set_impl_prod A5_ (set_impl_prod set_impl_list A5_)),
          set_impl_set)
        (fn drs =>
          image (ceq_prod A3_ A3_,
                  ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
            (ceq_prod A3_
               (ceq_prod
                 (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                 A3_),
              ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_prod
                  (ccompare_list
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_prod B2_
                        (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                  (ccompare_cproper_interval A4_)),
              set_impl_prod A5_ (set_impl_prod set_impl_list A5_))
            (fn (q1, q2) => (q1, ([], q2)))
            (inf_seta
              (ceq_prod A3_ A3_,
                ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval A4_))
              (producte (A3_, ccompare_cproper_interval A4_, A5_)
                (A3_, ccompare_cproper_interval A4_, A5_) drs drs)
              rds))
        drss);

fun calculate_test_paths (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) ma m d_reachable_states
  r_distinguishable_pairs repetition_sets =
  let
    val paths_with_witnesses =
      image (A3_, ccompare_cproper_interval A4_)
        (ceq_prod A3_
           (ceq_set
             (cenum_prod cenum_list
                (cenum_prod
                  (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                  (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
               ceq_prod
                 (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                 (ceq_prod (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                   (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
               ccompare_cproper_interval
                 (cproper_interval_prod
                   (cproper_interval_list
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_prod B2_
                         (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                   (cproper_interval_prod
                     (cproper_interval_set (A1_, A3_, A4_, A7_))
                     (cproper_interval_set (A1_, A3_, A4_, A7_)))))),
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_set
              (finite_UNIV_prod finite_UNIV_list
                 (finite_UNIV_prod (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                   (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                ceq_prod
                  (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                  (ceq_prod (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                    (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                cproper_interval_prod
                  (cproper_interval_list
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_prod B2_
                        (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                  (cproper_interval_prod
                    (cproper_interval_set (A1_, A3_, A4_, A7_))
                    (cproper_interval_set (A1_, A3_, A4_, A7_))),
                set_impl_prod set_impl_list
                  (set_impl_prod set_impl_set set_impl_set))),
          set_impl_prod A7_ set_impl_set)
        (fn q =>
          (q, m_traversal_paths_with_witness (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                (B1_, B2_, B4_) (C1_, C2_, C4_) ma q repetition_sets m))
        d_reachable_states;
    val get_paths =
      (fn x =>
        (case set_as_map (A3_, ccompare_cproper_interval A4_, A5_, A6_)
                (ceq_set
                   (cenum_prod cenum_list
                      (cenum_prod
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
                     ceq_prod
                       (ceq_list
                         (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                       (ceq_prod
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                     ccompare_cproper_interval
                       (cproper_interval_prod
                         (cproper_interval_list
                           (ccompare_prod (ccompare_cproper_interval A4_)
                             (ccompare_prod B2_
                               (ccompare_prod C2_
                                 (ccompare_cproper_interval A4_)))))
                         (cproper_interval_prod
                           (cproper_interval_set (A1_, A3_, A4_, A7_))
                           (cproper_interval_set (A1_, A3_, A4_, A7_))))),
                  ccompare_set
                    (finite_UNIV_prod finite_UNIV_list
                       (finite_UNIV_prod
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                      ceq_prod
                        (ceq_list
                          (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                        (ceq_prod
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                      cproper_interval_prod
                        (cproper_interval_list
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_prod B2_
                              (ccompare_prod C2_
                                (ccompare_cproper_interval A4_)))))
                        (cproper_interval_prod
                          (cproper_interval_set (A1_, A3_, A4_, A7_))
                          (cproper_interval_set (A1_, A3_, A4_, A7_))),
                      set_impl_prod set_impl_list
                        (set_impl_prod set_impl_set set_impl_set)),
                  set_impl_set)
                paths_with_witnesses x
          of NONE =>
            set_empty
              (ceq_set
                 (cenum_prod cenum_list
                    (cenum_prod
                      (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                      (cenum_set
                        (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
                   ceq_prod
                     (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                     (ceq_prod
                       (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                       (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                   ccompare_cproper_interval
                     (cproper_interval_prod
                       (cproper_interval_list
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_prod B2_
                             (ccompare_prod C2_
                               (ccompare_cproper_interval A4_)))))
                       (cproper_interval_prod
                         (cproper_interval_set (A1_, A3_, A4_, A7_))
                         (cproper_interval_set (A1_, A3_, A4_, A7_))))),
                ccompare_set
                  (finite_UNIV_prod finite_UNIV_list
                     (finite_UNIV_prod
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                    ceq_prod
                      (ceq_list
                        (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                      (ceq_prod
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                    cproper_interval_prod
                      (cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod B2_
                            (ccompare_prod C2_
                              (ccompare_cproper_interval A4_)))))
                      (cproper_interval_prod
                        (cproper_interval_set (A1_, A3_, A4_, A7_))
                        (cproper_interval_set (A1_, A3_, A4_, A7_))),
                    set_impl_prod set_impl_list
                      (set_impl_prod set_impl_set set_impl_set)))
              (of_phantom set_impl_seta)
          | SOME xs => xs));
    val prefixPairTests =
      sup_setb
        (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
           (finite_UNIV_prod finite_UNIV_list (finite_UNIV_card_UNIV A1_)),
          cenum_prod A2_ (cenum_prod cenum_list A2_),
          ceq_prod A3_
            (ceq_prod
              (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_)))) A3_),
          cproper_interval_prod A4_
            (cproper_interval_prod
              (cproper_interval_list
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_prod B2_
                    (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
              A4_),
          set_impl_prod A7_ (set_impl_prod set_impl_list A7_))
        (image (A3_, ccompare_cproper_interval A4_)
          (ceq_set
             (cenum_prod A2_ (cenum_prod cenum_list A2_),
               ceq_prod A3_
                 (ceq_prod
                   (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                   A3_),
               ccompare_cproper_interval
                 (cproper_interval_prod A4_
                   (cproper_interval_prod
                     (cproper_interval_list
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_prod B2_
                           (ccompare_prod C2_
                             (ccompare_cproper_interval A4_)))))
                     A4_))),
            ccompare_set
              (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod finite_UNIV_list
                   (finite_UNIV_card_UNIV A1_)),
                ceq_prod A3_
                  (ceq_prod
                    (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                    A3_),
                cproper_interval_prod A4_
                  (cproper_interval_prod
                    (cproper_interval_list
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod B2_
                          (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                    A4_),
                set_impl_prod A7_ (set_impl_prod set_impl_list A7_)),
            set_impl_set)
          (fn q =>
            sup_setb
              (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod finite_UNIV_list
                   (finite_UNIV_card_UNIV A1_)),
                cenum_prod A2_ (cenum_prod cenum_list A2_),
                ceq_prod A3_
                  (ceq_prod
                    (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                    A3_),
                cproper_interval_prod A4_
                  (cproper_interval_prod
                    (cproper_interval_list
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod B2_
                          (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                    A4_),
                set_impl_prod A7_ (set_impl_prod set_impl_list A7_))
              (image
                (ceq_set
                   (cenum_prod cenum_list
                      (cenum_prod
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
                     ceq_prod
                       (ceq_list
                         (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                       (ceq_prod
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                     ccompare_cproper_interval
                       (cproper_interval_prod
                         (cproper_interval_list
                           (ccompare_prod (ccompare_cproper_interval A4_)
                             (ccompare_prod B2_
                               (ccompare_prod C2_
                                 (ccompare_cproper_interval A4_)))))
                         (cproper_interval_prod
                           (cproper_interval_set (A1_, A3_, A4_, A7_))
                           (cproper_interval_set (A1_, A3_, A4_, A7_))))),
                  ccompare_set
                    (finite_UNIV_prod finite_UNIV_list
                       (finite_UNIV_prod
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                      ceq_prod
                        (ceq_list
                          (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                        (ceq_prod
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                      cproper_interval_prod
                        (cproper_interval_list
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_prod B2_
                              (ccompare_prod C2_
                                (ccompare_cproper_interval A4_)))))
                        (cproper_interval_prod
                          (cproper_interval_set (A1_, A3_, A4_, A7_))
                          (cproper_interval_set (A1_, A3_, A4_, A7_))),
                      set_impl_prod set_impl_list
                        (set_impl_prod set_impl_set set_impl_set)))
                (ceq_set
                   (cenum_prod A2_ (cenum_prod cenum_list A2_),
                     ceq_prod A3_
                       (ceq_prod
                         (ceq_list
                           (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                         A3_),
                     ccompare_cproper_interval
                       (cproper_interval_prod A4_
                         (cproper_interval_prod
                           (cproper_interval_list
                             (ccompare_prod (ccompare_cproper_interval A4_)
                               (ccompare_prod B2_
                                 (ccompare_prod C2_
                                   (ccompare_cproper_interval A4_)))))
                           A4_))),
                  ccompare_set
                    (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                       (finite_UNIV_prod finite_UNIV_list
                         (finite_UNIV_card_UNIV A1_)),
                      ceq_prod A3_
                        (ceq_prod
                          (ceq_list
                            (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                          A3_),
                      cproper_interval_prod A4_
                        (cproper_interval_prod
                          (cproper_interval_list
                            (ccompare_prod (ccompare_cproper_interval A4_)
                              (ccompare_prod B2_
                                (ccompare_prod C2_
                                  (ccompare_cproper_interval A4_)))))
                          A4_),
                      set_impl_prod A7_ (set_impl_prod set_impl_list A7_)),
                  set_impl_set)
                (prefix_pair_tests
                  (finite_UNIV_card_UNIV A1_, A3_, A4_, A5_, A7_) (B1_, B2_)
                  (C1_, C2_) q)
                (get_paths q)))
          d_reachable_states);
    val preamblePrefixTests =
      sup_setb
        (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
           (finite_UNIV_prod finite_UNIV_list (finite_UNIV_card_UNIV A1_)),
          cenum_prod A2_ (cenum_prod cenum_list A2_),
          ceq_prod A3_
            (ceq_prod
              (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_)))) A3_),
          cproper_interval_prod A4_
            (cproper_interval_prod
              (cproper_interval_list
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_prod B2_
                    (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
              A4_),
          set_impl_prod A7_ (set_impl_prod set_impl_list A7_))
        (image (A3_, ccompare_cproper_interval A4_)
          (ceq_set
             (cenum_prod A2_ (cenum_prod cenum_list A2_),
               ceq_prod A3_
                 (ceq_prod
                   (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                   A3_),
               ccompare_cproper_interval
                 (cproper_interval_prod A4_
                   (cproper_interval_prod
                     (cproper_interval_list
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_prod B2_
                           (ccompare_prod C2_
                             (ccompare_cproper_interval A4_)))))
                     A4_))),
            ccompare_set
              (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod finite_UNIV_list
                   (finite_UNIV_card_UNIV A1_)),
                ceq_prod A3_
                  (ceq_prod
                    (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                    A3_),
                cproper_interval_prod A4_
                  (cproper_interval_prod
                    (cproper_interval_list
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod B2_
                          (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                    A4_),
                set_impl_prod A7_ (set_impl_prod set_impl_list A7_)),
            set_impl_set)
          (fn q =>
            sup_setb
              (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                 (finite_UNIV_prod finite_UNIV_list
                   (finite_UNIV_card_UNIV A1_)),
                cenum_prod A2_ (cenum_prod cenum_list A2_),
                ceq_prod A3_
                  (ceq_prod
                    (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                    A3_),
                cproper_interval_prod A4_
                  (cproper_interval_prod
                    (cproper_interval_list
                      (ccompare_prod (ccompare_cproper_interval A4_)
                        (ccompare_prod B2_
                          (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                    A4_),
                set_impl_prod A7_ (set_impl_prod set_impl_list A7_))
              (image
                (ceq_set
                   (cenum_prod cenum_list
                      (cenum_prod
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                        (cenum_set
                          (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
                     ceq_prod
                       (ceq_list
                         (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                       (ceq_prod
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                     ccompare_cproper_interval
                       (cproper_interval_prod
                         (cproper_interval_list
                           (ccompare_prod (ccompare_cproper_interval A4_)
                             (ccompare_prod B2_
                               (ccompare_prod C2_
                                 (ccompare_cproper_interval A4_)))))
                         (cproper_interval_prod
                           (cproper_interval_set (A1_, A3_, A4_, A7_))
                           (cproper_interval_set (A1_, A3_, A4_, A7_))))),
                  ccompare_set
                    (finite_UNIV_prod finite_UNIV_list
                       (finite_UNIV_prod
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                         (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                      ceq_prod
                        (ceq_list
                          (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                        (ceq_prod
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                          (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                      cproper_interval_prod
                        (cproper_interval_list
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_prod B2_
                              (ccompare_prod C2_
                                (ccompare_cproper_interval A4_)))))
                        (cproper_interval_prod
                          (cproper_interval_set (A1_, A3_, A4_, A7_))
                          (cproper_interval_set (A1_, A3_, A4_, A7_))),
                      set_impl_prod set_impl_list
                        (set_impl_prod set_impl_set set_impl_set)))
                (ceq_set
                   (cenum_prod A2_ (cenum_prod cenum_list A2_),
                     ceq_prod A3_
                       (ceq_prod
                         (ceq_list
                           (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                         A3_),
                     ccompare_cproper_interval
                       (cproper_interval_prod A4_
                         (cproper_interval_prod
                           (cproper_interval_list
                             (ccompare_prod (ccompare_cproper_interval A4_)
                               (ccompare_prod B2_
                                 (ccompare_prod C2_
                                   (ccompare_cproper_interval A4_)))))
                           A4_))),
                  ccompare_set
                    (finite_UNIV_prod (finite_UNIV_card_UNIV A1_)
                       (finite_UNIV_prod finite_UNIV_list
                         (finite_UNIV_card_UNIV A1_)),
                      ceq_prod A3_
                        (ceq_prod
                          (ceq_list
                            (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                          A3_),
                      cproper_interval_prod A4_
                        (cproper_interval_prod
                          (cproper_interval_list
                            (ccompare_prod (ccompare_cproper_interval A4_)
                              (ccompare_prod B2_
                                (ccompare_prod C2_
                                  (ccompare_cproper_interval A4_)))))
                          A4_),
                      set_impl_prod A7_ (set_impl_prod set_impl_list A7_)),
                  set_impl_set)
                (fn mrsps =>
                  preamble_prefix_tests
                    (finite_UNIV_card_UNIV A1_, A3_, A4_, A5_, A7_) (B1_, B2_)
                    (C1_, C2_) q mrsps d_reachable_states)
                (get_paths q)))
          d_reachable_states);
    val preamblePairTests =
      preamble_pair_tests (finite_UNIV_card_UNIV A1_, A2_, A3_, A4_, A7_)
        (B1_, B2_) (C1_, C2_)
        (sup_setb
          (finite_UNIV_set (finite_UNIV_card_UNIV A1_),
            cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_),
            ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
            cproper_interval_set (A1_, A3_, A4_, A7_), set_impl_set)
          (image
            (ceq_prod A3_
               (ceq_set
                 (cenum_prod cenum_list
                    (cenum_prod
                      (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_))
                      (cenum_set
                        (A2_, A3_, ccompare_cproper_interval A4_, A7_))),
                   ceq_prod
                     (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                     (ceq_prod
                       (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                       (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                   ccompare_cproper_interval
                     (cproper_interval_prod
                       (cproper_interval_list
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_prod B2_
                             (ccompare_prod C2_
                               (ccompare_cproper_interval A4_)))))
                       (cproper_interval_prod
                         (cproper_interval_set (A1_, A3_, A4_, A7_))
                         (cproper_interval_set (A1_, A3_, A4_, A7_)))))),
              ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_set
                  (finite_UNIV_prod finite_UNIV_list
                     (finite_UNIV_prod
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                    ceq_prod
                      (ceq_list
                        (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                      (ceq_prod
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                    cproper_interval_prod
                      (cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod B2_
                            (ccompare_prod C2_
                              (ccompare_cproper_interval A4_)))))
                      (cproper_interval_prod
                        (cproper_interval_set (A1_, A3_, A4_, A7_))
                        (cproper_interval_set (A1_, A3_, A4_, A7_))),
                    set_impl_prod set_impl_list
                      (set_impl_prod set_impl_set set_impl_set))))
            (ceq_set
               (cenum_set (A2_, A3_, ccompare_cproper_interval A4_, A7_),
                 ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
                 ccompare_cproper_interval
                   (cproper_interval_set (A1_, A3_, A4_, A7_))),
              ccompare_set
                (finite_UNIV_set (finite_UNIV_card_UNIV A1_),
                  ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
                  cproper_interval_set (A1_, A3_, A4_, A7_), set_impl_set),
              set_impl_set)
            (fn (_, a) =>
              image (ceq_prod
                       (ceq_list
                         (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                       (ceq_prod
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                         (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                      ccompare_prod
                        (ccompare_list
                          (ccompare_prod (ccompare_cproper_interval A4_)
                            (ccompare_prod B2_
                              (ccompare_prod C2_
                                (ccompare_cproper_interval A4_)))))
                        (ccompare_prod
                          (ccompare_set
                            (finite_UNIV_card_UNIV A1_, A3_, A4_, A7_))
                          (ccompare_set
                            (finite_UNIV_card_UNIV A1_, A3_, A4_, A7_))))
                (ceq_set (A2_, A3_, ccompare_cproper_interval A4_),
                  ccompare_set (finite_UNIV_card_UNIV A1_, A3_, A4_, A7_),
                  set_impl_set)
                (fn (_, (_, dr)) => dr) a)
            paths_with_witnesses))
        r_distinguishable_pairs;
    val tests =
      sup_seta
        (ceq_prod A3_
           (ceq_prod (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
             A3_),
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_prod
              (ccompare_list
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_prod B2_
                    (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
              (ccompare_cproper_interval A4_)))
        (sup_seta
          (ceq_prod A3_
             (ceq_prod
               (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_)))) A3_),
            ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_prod
                (ccompare_list
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_prod B2_
                      (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
                (ccompare_cproper_interval A4_)))
          prefixPairTests preamblePrefixTests)
        preamblePairTests;
    val tps =
      (fn x =>
        (case set_as_map_image (ccompare_cproper_interval A4_)
                (ccompare_set
                  (finite_UNIV_prod finite_UNIV_list
                     (finite_UNIV_prod
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))
                       (finite_UNIV_set (finite_UNIV_card_UNIV A1_))),
                    ceq_prod
                      (ceq_list
                        (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))))
                      (ceq_prod
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))
                        (ceq_set (A2_, A3_, ccompare_cproper_interval A4_))),
                    cproper_interval_prod
                      (cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod B2_
                            (ccompare_prod C2_
                              (ccompare_cproper_interval A4_)))))
                      (cproper_interval_prod
                        (cproper_interval_set (A1_, A3_, A4_, A7_))
                        (cproper_interval_set (A1_, A3_, A4_, A7_))),
                    set_impl_prod set_impl_list
                      (set_impl_prod set_impl_set set_impl_set)))
                (ccompare_cproper_interval A4_, A5_, A6_)
                (ceq_set
                   (cenum_list,
                     ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                     ccompare_cproper_interval
                       (cproper_interval_list
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_prod B2_
                             (ccompare_prod C2_
                               (ccompare_cproper_interval A4_)))))),
                  ccompare_set
                    (finite_UNIV_list,
                      ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                      cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod B2_
                            (ccompare_prod C2_
                              (ccompare_cproper_interval A4_)))),
                      set_impl_list),
                  set_impl_set)
                paths_with_witnesses
                (fn (q, p) =>
                  (q, image (ceq_prod
                               (ceq_list
                                 (ceq_prod A3_
                                   (ceq_prod B1_ (ceq_prod C1_ A3_))))
                               (ceq_prod
                                 (ceq_set
                                   (A2_, A3_, ccompare_cproper_interval A4_))
                                 (ceq_set
                                   (A2_, A3_, ccompare_cproper_interval A4_))),
                              ccompare_prod
                                (ccompare_list
                                  (ccompare_prod (ccompare_cproper_interval A4_)
                                    (ccompare_prod B2_
                                      (ccompare_prod C2_
(ccompare_cproper_interval A4_)))))
                                (ccompare_prod
                                  (ccompare_set
                                    (finite_UNIV_card_UNIV A1_, A3_, A4_, A7_))
                                  (ccompare_set
                                    (finite_UNIV_card_UNIV A1_, A3_, A4_,
                                      A7_))))
                        (ceq_list
                           (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                          ccompare_list
                            (ccompare_prod (ccompare_cproper_interval A4_)
                              (ccompare_prod B2_
                                (ccompare_prod C2_
                                  (ccompare_cproper_interval A4_)))),
                          set_impl_list)
                        fst p))
                x
          of NONE =>
            sup_setb
              (finite_UNIV_list, cenum_list,
                ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                cproper_interval_list
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_prod B2_
                      (ccompare_prod C2_ (ccompare_cproper_interval A4_)))),
                set_impl_list)
              (set_empty
                (ceq_set
                   (cenum_list,
                     ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                     ccompare_cproper_interval
                       (cproper_interval_list
                         (ccompare_prod (ccompare_cproper_interval A4_)
                           (ccompare_prod B2_
                             (ccompare_prod C2_
                               (ccompare_cproper_interval A4_)))))),
                  ccompare_set
                    (finite_UNIV_list,
                      ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                      cproper_interval_list
                        (ccompare_prod (ccompare_cproper_interval A4_)
                          (ccompare_prod B2_
                            (ccompare_prod C2_
                              (ccompare_cproper_interval A4_)))),
                      set_impl_list))
                (of_phantom set_impl_seta))
          | SOME a =>
            sup_setb
              (finite_UNIV_list, cenum_list,
                ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                cproper_interval_list
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_prod B2_
                      (ccompare_prod C2_ (ccompare_cproper_interval A4_)))),
                set_impl_list)
              a));
    val dual_maps =
      dual_set_as_map_image (ccompare_cproper_interval A4_)
        (ccompare_prod
          (ccompare_list
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_prod B2_
                (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
          (ccompare_cproper_interval A4_))
        (ccompare_cproper_interval A4_, A5_, A6_)
        (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
          ccompare_list
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_prod B2_
                (ccompare_prod C2_ (ccompare_cproper_interval A4_)))),
          set_impl_list)
        (ccompare_prod (ccompare_cproper_interval A4_)
           (ccompare_list
             (ccompare_prod (ccompare_cproper_interval A4_)
               (ccompare_prod B2_
                 (ccompare_prod C2_ (ccompare_cproper_interval A4_))))),
          equal_prod A5_
            (equal_list (equal_prod A5_ (equal_prod B3_ (equal_prod C3_ A5_)))),
          mapping_impl_prod A6_ mapping_impl_list)
        (A3_, ccompare_cproper_interval A4_, A7_) tests
        (fn (q, (p, _)) => (q, p)) (fn (q, a) => let
           val (p, aa) = a;
         in
           ((q, p), aa)
         end);
    val tpsa =
      (fn x =>
        (case fst dual_maps x
          of NONE =>
            set_empty
              (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
                ccompare_list
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_prod B2_
                      (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
              (of_phantom set_impl_lista)
          | SOME xs => xs));
    val tpsb =
      (fn q =>
        sup_seta
          (ceq_list (ceq_prod A3_ (ceq_prod B1_ (ceq_prod C1_ A3_))),
            ccompare_list
              (ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_prod B2_
                  (ccompare_prod C2_ (ccompare_cproper_interval A4_)))))
          (tps q) (tpsa q));
    val a =
      (fn x =>
        (case snd dual_maps x
          of NONE => bot_set (A3_, ccompare_cproper_interval A4_, A7_)
          | SOME xs => xs));
  in
    (tpsb, a)
  end;

fun combine_test_suite (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) (D1_, D2_, D3_, D4_, D5_)
  ma m states_with_preambles pairs_with_separators repetition_sets =
  let
    val drs =
      image (ceq_prod A3_
               (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
                 (B1_, B2_, B3_) (C1_, C2_, C3_)),
              ccompare_prod (ccompare_cproper_interval A4_) ccompare_fsm)
        (A3_, ccompare_cproper_interval A4_, A7_) fst states_with_preambles;
    val rds =
      image (ceq_prod (ceq_prod A3_ A3_)
               (ceq_prod
                 (ceq_fsm (D1_, D2_, D3_, D4_) (B1_, B2_, B3_) (C1_, C2_, C3_))
                 (ceq_prod D2_ D2_)),
              ccompare_prod
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval A4_))
                (ccompare_prod ccompare_fsm (ccompare_prod D3_ D3_)))
        (ceq_prod A3_ A3_,
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_cproper_interval A4_),
          set_impl_prod A7_ A7_)
        fst pairs_with_separators;
    val tps_and_targets =
      calculate_test_paths (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
        (B2_, B3_, B4_, B5_) (C2_, C3_, C4_, C5_) ma m drs rds repetition_sets;
    val a =
      (fn x =>
        (case set_as_map
                (ceq_prod A3_ A3_,
                  ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_),
                  equal_prod A5_ A5_, mapping_impl_prod A6_ A6_)
                (ceq_prod
                   (ceq_fsm (D1_, D2_, D3_, D4_) (B1_, B2_, B3_)
                     (C1_, C2_, C3_))
                   (ceq_prod D2_ D2_),
                  ccompare_prod ccompare_fsm (ccompare_prod D3_ D3_),
                  set_impl_prod set_impl_fsm (set_impl_prod D5_ D5_))
                pairs_with_separators x
          of NONE =>
            bot_set
              (ceq_prod
                 (ceq_fsm (D1_, D2_, D3_, D4_) (B1_, B2_, B3_) (C1_, C2_, C3_))
                 (ceq_prod D2_ D2_),
                ccompare_prod ccompare_fsm (ccompare_prod D3_ D3_),
                set_impl_prod set_impl_fsm (set_impl_prod D5_ D5_))
          | SOME xs => xs));
  in
    Test_Suite
      (states_with_preambles, fst tps_and_targets, snd tps_and_targets, a)
  end;

fun get_extension (A1_, A2_) (B1_, B2_) t g cg_lookup alpha x y =
  find (fn beta =>
         isin (ccompare_prod A1_ B1_, equal_prod A2_ B2_) t (beta @ [(x, y)]))
    (alpha :: cg_lookup g alpha);

fun sorted_list_of_sequences_in_tree (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (MPT m)
  = (if is_empty (A1_, A3_, A4_)
          (keys (A2_, A3_, ccompare_cproper_interval A4_, A7_) m)
      then [[]]
      else [] :: maps (fn k =>
                        mapa (fn a => k :: a)
                          (sorted_list_of_sequences_in_tree
                            (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                            (the (lookupa (ccompare_cproper_interval A4_, A5_) m
                                   k))))
                   (sorted_list_of_set
                     (A3_, ccompare_cproper_interval A4_, A5_, A6_)
                     (keys (A2_, A3_, ccompare_cproper_interval A4_, A7_) m)));

fun simple_cg_initial (A1_, A2_) (B1_, B2_, B3_, B4_, B5_, B6_, B7_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m1 t =
  foldl (simple_cg_inserta
          (ceq_prod B3_ C3_,
            ccompare_prod (ccompare_cproper_interval B4_)
              (ccompare_cproper_interval C4_),
            linorder_prod B6_ C6_))
    simple_cg_empty
    (filtera
      (is_in_language (A1_, A2_) (ccompare_cproper_interval B4_, B5_)
        (ccompare_cproper_interval C4_, C5_) m1 (initial m1))
      (sorted_list_of_sequences_in_tree
        (card_UNIV_prod B1_ C1_, cenum_prod B2_ C2_, ceq_prod B3_ C3_,
          cproper_interval_prod B4_ C4_, equal_prod B5_ C5_,
          linorder_prod B6_ C6_, set_impl_prod B7_ C7_)
        t));

fun select_diverging_ofsm_table_io_with_provided_tables
  (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_)
  tables m q1 q2 k =
  let
    val ins = inputs_as_list (B1_, B2_, B3_, B4_) m;
    val outs = outputs_as_list (C1_, C2_, C3_, C4_) m;
    val table =
      lookup_default (A3_, A4_) (bot_set (A2_, A3_, A6_))
        (the (lookupa (ccompare_nat, equal_nat) tables (minus_nat k one_nata)));
    val f =
      (fn (x, y) =>
        (case (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q1 x y,
                h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m q2 x y)
          of (NONE, NONE) => NONE
          | (NONE, SOME q2a) => SOME ((x, y), (NONE, SOME q2a))
          | (SOME q1a, NONE) => SOME ((x, y), (SOME q1a, NONE))
          | (SOME q1a, SOME q2a) =>
            (if not (set_eq (A1_, A2_, A3_) (table q1a) (table q2a))
              then SOME ((x, y), (SOME q1a, SOME q2a)) else NONE)));
  in
    hd (map_filter f (product ins outs))
  end;

fun assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables
  (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_)
  tables m q1 q2 k =
  (if equal_nata k zero_nat then []
    else (case select_diverging_ofsm_table_io_with_provided_tables
                 (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
                 (C1_, C2_, C3_, C4_) tables m q1 q2
                 (suc (minus_nat k one_nata))
           of ((x, y), (NONE, _)) => [(x, y)]
           | ((x, y), (SOME _, NONE)) => [(x, y)]
           | ((x, y), (SOME q1a, SOME q2a)) =>
             (x, y) ::
               assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables
                 (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_)
                 (C1_, C2_, C3_, C4_) tables m q1a q2a (minus_nat k one_nata)));

fun get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_)
  tables m q1 q2 =
  let
    val a =
      (if member (A3_, A4_) q1 (states m) andalso
            (member (A3_, A4_) q2 (states m) andalso
              not (set_eq (A2_, A3_, A4_)
                    (lookup_default (A4_, A5_) (bot_set (A3_, A4_, A7_))
                      (the (lookupa (ccompare_nat, equal_nat) tables
                             (minus_nat (size (A1_, A3_, A4_) m) one_nata)))
                      q1)
                    (lookup_default (A4_, A5_) (bot_set (A3_, A4_, A7_))
                      (the (lookupa (ccompare_nat, equal_nat) tables
                             (minus_nat (size (A1_, A3_, A4_) m) one_nata)))
                      q2)))
        then the (find_index
                   (fn i =>
                     not (set_eq (A2_, A3_, A4_)
                           (lookup_default (A4_, A5_) (bot_set (A3_, A4_, A7_))
                             (the (lookupa (ccompare_nat, equal_nat) tables i))
                             q1)
                           (lookup_default (A4_, A5_) (bot_set (A3_, A4_, A7_))
                             (the (lookupa (ccompare_nat, equal_nat) tables i))
                             q2)))
                   (upt zero_nat (size (A1_, A3_, A4_) m)))
        else zero_nat);
  in
    assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables
      (A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_)
      tables m q1 q2 a
  end;

fun sort_unverified_transitions_by_state_cover_length (A1_, A2_, A3_, A4_, A5_)
  (B1_, B2_) (C1_, C2_) m v ts =
  let
    val default_weight =
      times_nata (nat_of_integer (2 : IntInf.int)) (size (A1_, A2_, A3_) m);
    val weights =
      map_of (equal_prod A4_ (equal_prod B1_ (equal_prod C1_ A4_)))
        (mapa (fn t =>
                (t, plus_nat (size_list (v (fst t)))
                      (size_list (v (snd (snd (snd t)))))))
          ts);
    val weight =
      (fn q => (case weights q of NONE => default_weight | SOME w => w));
  in
    mergesort_by_rel (fn t1 => fn t2 => less_eq_nat (weight t1) (weight t2)) ts
  end;

fun append_heuristic_input A_ (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_, C5_) m t
  w (uBest, lBest) u =
  let
    val ta =
      aftera
        (ccompare_prod B1_ C2_, equal_prod B2_ C3_, mapping_impl_prod B3_ C4_) t
        u;
    val ws =
      maximum_fst_prefixes (B1_, B2_) (C2_, C3_) ta (mapa fst w)
        (outputs_as_list (C1_, C2_, C3_, C5_) m);
  in
    foldr (fn wa => fn (uBesta, lBesta) =>
            (if equal_lista (equal_prod B2_ C3_) wa w then (u, zero_int)
              else (if less_int lBesta (int_of_nat (size_list wa)) orelse
                         equal_int (int_of_nat (size_list wa)) lBesta andalso
                           less_nat (size_list u) (size_list uBesta)
                     then (u, int_of_nat (size_list wa))
                     else (uBesta, lBesta))))
      ws (uBest, lBest)
  end;

fun append_heuristic_io (A1_, A2_, A3_) (B1_, B2_, B3_) t w (uBest, lBest) u =
  let
    val ta =
      aftera
        (ccompare_prod A1_ B1_, equal_prod A2_ B2_, mapping_impl_prod A3_ B3_) t
        u;
    val wa = maximum_prefix (ccompare_prod A1_ B1_, equal_prod A2_ B2_) ta w;
  in
    (if equal_lista (equal_prod A2_ B2_) wa w then (u, zero_int)
      else (if is_maximal_in
                 (ccompare_prod A1_ B1_, equal_prod A2_ B2_,
                   mapping_impl_prod A3_ B3_)
                 ta wa andalso
                 (less_int lBest (int_of_nat (size_list wa)) orelse
                   equal_int (int_of_nat (size_list wa)) lBest andalso
                     less_nat (size_list u) (size_list uBest))
             then (u, int_of_nat (size_list wa)) else (uBest, lBest)))
  end;

fun shortest_list_or_default xs x =
  foldl (fn a => fn b =>
          (if less_nat (size_list a) (size_list b) then a else b))
    x xs;

fun estimate_growth (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_)
  (C1_, C2_, C3_) m dist_fun q1 q2 x y errorValue =
  (case h_obs (A3_, A4_) (B1_, B2_) (C1_, C2_) m q1 x y
    of NONE =>
      (case h_obs (A3_, A4_) (B1_, B2_) (C1_, C2_) m q1 x y
        of NONE => errorValue | SOME _ => one_nata)
    | SOME q1a =>
      (case h_obs (A3_, A4_) (B1_, B2_) (C1_, C2_) m q2 x y of NONE => one_nata
        | SOME q2a =>
          (if eq A4_ q1a q2a orelse
                set_eq (A1_, A2_, A3_)
                  (insert (A2_, A3_) q1a
                    (insert (A2_, A3_) q2a (bot_set (A2_, A3_, A6_))))
                  (insert (A2_, A3_) q1
                    (insert (A2_, A3_) q2 (bot_set (A2_, A3_, A6_))))
            then errorValue
            else plus_nat one_nata
                   (times_nata (nat_of_integer (2 : IntInf.int))
                     (size_list (dist_fun q1 q2))))));

fun get_prefix_of_separating_sequence (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup
  get_distinguishing_trace u v k =
  (if equal_nata k zero_nat then (one_nata, [])
    else let
           val ua = shortest_list_or_default (cg_lookup g u) u;
           val va = shortest_list_or_default (cg_lookup g v) v;
           val su = after (A3_, A4_) (B2_, B3_) (C2_, C3_) m (initial m) u;
           val sv = after (A3_, A4_) (B2_, B3_) (C2_, C3_) m (initial m) v;
           val bestPrefix0 = get_distinguishing_trace su sv;
           val minEst0 =
             plus_nat
               (plus_nat (size_list bestPrefix0)
                 (if has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup ua
                   then zero_nat else size_list ua))
               (if has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup va
                 then zero_nat else size_list va);
           val errorValue = suc minEst0;
           val xy =
             product (inputs_as_list (B1_, B2_, B3_, B5_) m)
               (outputs_as_list (C1_, C2_, C3_, C5_) m);
           val tryIO =
             (fn (minEst, bestPrefix) => fn (x, y) =>
               (if equal_nata minEst zero_nat then (minEst, bestPrefix)
                 else (case get_extension (B2_, B3_) (C2_, C3_) t g cg_lookup ua
                              x y
                        of NONE =>
                          (case get_extension (B2_, B3_) (C2_, C3_) t g
                                  cg_lookup va x y
                            of NONE => (minEst, bestPrefix)
                            | SOME vb =>
                              let
                                val e =
                                  estimate_growth (A1_, A2_, A3_, A4_, A5_, A6_)
                                    (B2_, B3_, B5_) (C2_, C3_, C5_) m
                                    get_distinguishing_trace su sv x y
                                    errorValue;
                                val ea =
                                  (if not (equal_nata e one_nata)
                                    then (if has_leaf (B2_, B3_, B4_)
       (C2_, C3_, C4_) t g cg_lookup vb
   then plus_nat e one_nata
   else (if not (has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup
                  (vb @ [(x, y)]))
          then plus_nat (plus_nat e (size_list va)) one_nata else e))
                                    else e);
                                val eb =
                                  plus_nat ea
                                    (if not
  (has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup ua)
                                      then size_list ua else zero_nat);
                              in
                                (if less_eq_nat eb minEst then (eb, [(x, y)])
                                  else (minEst, bestPrefix))
                              end)
                        | SOME ub =>
                          (case get_extension (B2_, B3_) (C2_, C3_) t g
                                  cg_lookup va x y
                            of NONE =>
                              let
                                val e =
                                  estimate_growth (A1_, A2_, A3_, A4_, A5_, A6_)
                                    (B2_, B3_, B5_) (C2_, C3_, C5_) m
                                    get_distinguishing_trace su sv x y
                                    errorValue;
                                val ea =
                                  (if not (equal_nata e one_nata)
                                    then (if has_leaf (B2_, B3_, B4_)
       (C2_, C3_, C4_) t g cg_lookup ub
   then plus_nat e one_nata
   else (if not (has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup
                  (ub @ [(x, y)]))
          then plus_nat (plus_nat e (size_list ua)) one_nata else e))
                                    else e);
                                val eb =
                                  plus_nat ea
                                    (if not
  (has_leaf (B2_, B3_, B4_) (C2_, C3_, C4_) t g cg_lookup va)
                                      then size_list va else zero_nat);
                              in
                                (if less_eq_nat eb minEst then (eb, [(x, y)])
                                  else (minEst, bestPrefix))
                              end
                            | SOME vb =>
                              (if not (equal_boola
(is_none (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m su x y))
(is_none (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m sv x y)))
                                then (zero_nat, [])
                                else (if equal_option A4_
   (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m su x y)
   (h_obs (A3_, A4_) (B2_, B3_) (C2_, C3_) m sv x y)
                                       then (minEst, bestPrefix)
                                       else let
      val (e, w) =
        get_prefix_of_separating_sequence (A1_, A2_, A3_, A4_, A5_, A6_)
          (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup
          get_distinguishing_trace (ub @ [(x, y)]) (vb @ [(x, y)])
          (minus_nat k one_nata);
    in
      (if equal_nata e zero_nat then (zero_nat, [])
        else (if less_eq_nat e minEst then (e, (x, y) :: w)
               else (minEst, bestPrefix)))
    end))))));
         in
           (if not (isin (ccompare_prod B2_ C2_, equal_prod B3_ C3_) t
                     ua) orelse
                 not (isin (ccompare_prod B2_ C2_, equal_prod B3_ C3_) t va)
             then (errorValue, []) else foldl tryIO (minEst0, []) xy)
         end);

fun shortest_list_in_tree_or_default (A1_, A2_) xs t x =
  foldl (fn a => fn b =>
          (if isin (A1_, A2_) t a andalso less_nat (size_list a) (size_list b)
            then a else b))
    x xs;

fun complete_inputs_to_tree (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m q ys [] =
  emptyc (ccompare_prod B1_ C1_, mapping_impl_prod B3_ C3_)
  | complete_inputs_to_tree (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
    (C1_, C2_, C3_, C4_) m q ys (x :: xs) =
    foldl (fn t => fn y =>
            (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q x y
              of NONE =>
                insertb
                  (ccompare_prod B1_ C1_, equal_prod B2_ C2_,
                    mapping_impl_prod B3_ C3_)
                  t [(x, y)]
              | SOME qa =>
                combine_after
                  (ccompare_prod B1_ C1_, equal_prod B2_ C2_,
                    mapping_impl_prod B3_ C3_)
                  t [(x, y)]
                  (complete_inputs_to_tree (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
                    (C1_, C2_, C3_, C4_) m qa ys xs)))
      (emptyc (ccompare_prod B1_ C1_, mapping_impl_prod B3_ C3_)) ys;

fun distribute_extension (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup cg_insert u w completeInputTraces
  append_heuristic =
  let
    val cu = cg_lookup g u;
    val u0 =
      shortest_list_in_tree_or_default
        (ccompare_prod B1_ C2_, equal_prod B2_ C3_) cu t u;
    val l0 = uminus_int one_int;
    val ua =
      fst (foldl (append_heuristic t w) (u0, l0)
            (filtera (isin (ccompare_prod B1_ C2_, equal_prod B2_ C3_) t) cu));
    val ta =
      insertb
        (ccompare_prod B1_ C2_, equal_prod B2_ C3_, mapping_impl_prod B3_ C4_) t
        (ua @ w);
    val ga =
      cg_insert g
        (maximal_prefix_in_language (A1_, A2_) (B1_, B2_) (C2_, C3_) m
          (initial m) (ua @ w));
  in
    (if completeInputTraces
      then let
             val tc =
               complete_inputs_to_tree (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
                 (C2_, C3_, C4_, C5_) m (initial m)
                 (outputs_as_list (C1_, C2_, C3_, C5_) m) (mapa fst (ua @ w));
             val tb = combinea (ccompare_prod B1_ C2_) ta tc;
           in
             (tb, ga)
           end
      else (ta, ga))
  end;

fun spyh_distinguish (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup cg_insert get_distinguishing_trace u
  x k completeInputTraces append_heuristic =
  let
    val dist_helper =
      (fn (ta, ga) => fn v =>
        (if eq A4_ (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m (initial m) u)
              (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m (initial m) v)
          then (ta, ga)
          else let
                 val ew =
                   get_prefix_of_separating_sequence
                     (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
                     (C1_, C2_, C3_, C4_, C5_) m ta ga cg_lookup
                     get_distinguishing_trace u v k;
               in
                 (if equal_nata (fst ew) zero_nat then (ta, ga)
                   else let
                          val ua = u @ snd ew;
                          val va = v @ snd ew;
                          val w =
                            (if does_distinguish (A3_, A4_) (B2_, B3_)
                                  (C2_, C3_) m
                                  (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m
                                    (initial m) u)
                                  (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m
                                    (initial m) v)
                                  (snd ew)
                              then snd ew
                              else snd ew @
                                     get_distinguishing_trace
                                       (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m
 (initial m) ua)
                                       (after (A3_, A4_) (B2_, B3_) (C2_, C3_) m
 (initial m) va));
                          val tg =
                            distribute_extension (A3_, A4_, A5_)
                              (B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m
                              ta ga cg_lookup cg_insert u w completeInputTraces
                              append_heuristic;
                        in
                          distribute_extension (A3_, A4_, A5_)
                            (B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m
                            (fst tg) (snd tg) cg_lookup cg_insert v w
                            completeInputTraces append_heuristic
                        end)
               end));
  in
    foldl dist_helper (t, g) x
  end;

fun handle_state_cover_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) completeInputTraces
  useInputHeuristic get_distinguishing_trace m v cg_initial cg_insert cg_lookup
  = let
      val k =
        times_nata (nat_of_integer (2 : IntInf.int)) (size (A1_, A3_, A4_) m);
      val heuristic =
        (if useInputHeuristic
          then append_heuristic_input A6_ (B2_, B3_, B4_, B5_)
                 (C1_, C2_, C3_, C4_, C5_) m
          else append_heuristic_io (B2_, B3_, B4_) (C2_, C3_, C4_));
      val rstates =
        reachable_states_as_list (A1_, A3_, A4_, A5_, A6_, A7_)
          (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) m;
      val t0 =
        from_list
          (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
          (mapa v rstates);
      val t0a =
        (if completeInputTraces
          then combinea (ccompare_prod B2_ C2_) t0
                 (from_list
                   (ccompare_prod B2_ C2_, equal_prod B3_ C3_,
                     mapping_impl_prod B4_ C4_)
                   (maps (fn q =>
                           language_for_input (A4_, A5_, A6_) (B2_, B3_, B5_)
                             (C1_, C2_, C3_, C5_) m (initial m)
                             (mapa fst (v q)))
                     rstates))
          else t0);
      val g0 = cg_initial m t0a;
      val separate_state =
        (fn (x, (t, g)) => fn q =>
          let
            val u = v q;
            val tg =
              spyh_distinguish (A2_, A3_, A4_, A5_, A6_, A7_)
                (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m t g
                cg_lookup cg_insert get_distinguishing_trace u x k
                completeInputTraces heuristic;
            val xa = u :: x;
          in
            (xa, tg)
          end);
    in
      snd (foldl separate_state ([], (t0a, g0)) rstates)
    end;

fun simple_cg_lookup_with_conv (A1_, A2_, A3_, A4_) g ys =
  let
    val lookup_for_prefix =
      (fn i =>
        let
          val pref = take i ys;
          val suff = drop i ys;
          val a =
            foldl (sup_fset (ceq_list A1_, ccompare_list A2_))
              (bot_fset (ceq_list A1_, ccompare_list A2_, set_impl_list))
              (filtera (fmember (ceq_list A1_, ccompare_list A2_) pref) g);
        in
          fimage (ceq_list A1_, ccompare_list A2_)
            (ceq_list A1_, ccompare_list A2_, set_impl_list)
            (fn prefa => prefa @ suff) a
        end);
  in
    sorted_list_of_fset
      (ceq_list A1_, ccompare_list A2_, equal_list A3_,
        linorder_list (A3_, A4_))
      (finsert (ceq_list A1_, ccompare_list A2_) ys
        (foldl
          (fn cs => fn i =>
            sup_fset (ceq_list A1_, ccompare_list A2_) (lookup_for_prefix i) cs)
          (bot_fset (ceq_list A1_, ccompare_list A2_, set_impl_list))
          (upt zero_nat (suc (size_list ys)))))
  end;

fun distinguish_from_set (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m va t g cg_lookup
  cg_insert get_distinguishing_trace u v x k depth completeInputTraces
  append_heuristic u_is_v =
  let
    val tg =
      spyh_distinguish (A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_, B5_)
        (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup cg_insert
        get_distinguishing_trace u x k completeInputTraces append_heuristic;
    val vClass =
      insert
        (ceq_list (ceq_prod B1_ C1_), ccompare_list (ccompare_prod B2_ C2_)) v
        (set (ceq_list (ceq_prod B1_ C1_),
               ccompare_list (ccompare_prod B2_ C2_), set_impl_list)
          (cg_lookup (snd tg) v));
    val notReferenced =
      not u_is_v andalso
        ball (A3_, A4_)
          (reachable_states (A1_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B5_)
            (C1_, C2_, C3_, C5_) m)
          (fn q =>
            not (member
                  (ceq_list (ceq_prod B1_ C1_),
                    ccompare_list (ccompare_prod B2_ C2_))
                  (va q) vClass));
    val tga =
      (if notReferenced
        then spyh_distinguish (A2_, A3_, A4_, A5_, A6_, A7_)
               (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m (fst tg)
               (snd tg) cg_lookup cg_insert get_distinguishing_trace v x k
               completeInputTraces append_heuristic
        else tg);
  in
    (if less_nat zero_nat depth
      then let
             val xa = (if notReferenced then v :: u :: x else u :: x);
             val xy =
               product (inputs_as_list (B1_, B2_, B3_, B5_) m)
                 (outputs_as_list (C1_, C2_, C3_, C5_) m);
             val handleIO =
               (fn (ta, ga) => fn (xaa, y) =>
                 let
                   val tGu =
                     distribute_extension (A4_, A5_, A6_) (B2_, B3_, B4_, B5_)
                       (C1_, C2_, C3_, C4_, C5_) m ta ga cg_lookup cg_insert u
                       [(xaa, y)] completeInputTraces append_heuristic;
                   val tGv =
                     (if u_is_v then tGu
                       else distribute_extension (A4_, A5_, A6_)
                              (B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m
                              (fst tGu) (snd tGu) cg_lookup cg_insert v
                              [(xaa, y)] completeInputTraces append_heuristic);
                 in
                   (if is_in_language (A4_, A5_) (B2_, B3_) (C2_, C3_) m
                         (initial m) (u @ [(xaa, y)])
                     then distinguish_from_set
                            (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                            (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_)
                            m va (fst tGv) (snd tGv) cg_lookup cg_insert
                            get_distinguishing_trace (u @ [(xaa, y)])
                            (v @ [(xaa, y)]) xa k (minus_nat depth one_nata)
                            completeInputTraces append_heuristic u_is_v
                     else tGv)
                 end);
           in
             foldl handleIO tga xy
           end
      else tga)
  end;

fun handle_io_pair (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) completeInputTraces useInputHeuristic m v t g
  cg_insert cg_lookup q x y =
  distribute_extension (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
    (C1_, C2_, C3_, C4_, C5_) m t g cg_lookup cg_insert (v q) [(x, y)]
    completeInputTraces
    (if useInputHeuristic
      then append_heuristic_input A3_ (B1_, B2_, B3_, B4_)
             (C1_, C2_, C3_, C4_, C5_) m
      else append_heuristic_io (B1_, B2_, B3_) (C2_, C3_, C4_));

fun handleUT_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) complete_input_traces
  use_input_heuristic dist_fun do_establish_convergence ma v ta g cg_insert
  cg_lookup cg_merge m t x =
  let
    val k =
      times_nata (nat_of_integer (2 : IntInf.int)) (size (A1_, A3_, A4_) ma);
    val l =
      minus_nat m
        (card (A1_, A3_, A4_)
          (reachable_states (A1_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B5_)
            (C1_, C2_, C3_, C5_) ma));
    val heuristic =
      (if use_input_heuristic
        then append_heuristic_input A6_ (B2_, B3_, B4_, B5_)
               (C1_, C2_, C3_, C4_, C5_) ma
        else append_heuristic_io (B2_, B3_, B4_) (C2_, C3_, C4_));
    val rstates =
      mapa v
        (reachable_states_as_list (A1_, A3_, A4_, A5_, A6_, A7_)
          (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) ma);
    val (t1, g1) =
      handle_io_pair (A4_, A5_, A6_) (B2_, B3_, B4_, B5_)
        (C1_, C2_, C3_, C4_, C5_) complete_input_traces use_input_heuristic ma v
        ta g cg_insert cg_lookup (fst t) (fst (snd t)) (fst (snd (snd t)));
    val u = v (fst t) @ [(fst (snd t), fst (snd (snd t)))];
    val va = v (snd (snd (snd t)));
    val xa = butlast x;
  in
    (if do_establish_convergence ma v t xa l
      then let
             val (t2, g2) =
               distinguish_from_set (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                 (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) ma v t1 g1
                 cg_lookup cg_insert dist_fun u va rstates k l
                 complete_input_traces heuristic false;
             val g3 = cg_merge g2 u va;
           in
             (xa, (t2, g3))
           end
      else (xa, distinguish_from_set (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) ma v t1 g1
                  cg_lookup cg_insert dist_fun u u rstates k l
                  complete_input_traces heuristic true))
  end;

fun h_framework_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) convergence_decisision m1 m
  completeInputTraces useInputHeuristic =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) m1
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) m1)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables m1 q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) m1)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              m1)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states m1) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states m1) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) m1 q1 q2));
  in
    h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
      (get_state_cover_assignment
        (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C7_))
      (handle_state_cover_dynamic
        (A1_, A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic distHelper)
      (sort_unverified_transitions_by_state_cover_length
        (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_) (B5_, B7_)
        (C5_, C7_))
      (handleUT_dynamic
        (A1_, A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic distHelper convergence_decisision)
      (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
        (ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic)
      (simple_cg_initial (ccompare_cproper_interval A4_, A5_)
        (B1_, B2_, B3_, B4_, B5_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C7_, C8_))
      (simple_cg_insert
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          linorder_prod B7_ C7_))
      (simple_cg_lookup_with_conv
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, linorder_prod B7_ C7_))
      (simple_cg_merge
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_))
      m
  end;

fun sorted_list_of_maximal_sequences_in_tree (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (MPT m) =
  (if is_empty (A1_, A3_, A4_)
        (keys (A2_, A3_, ccompare_cproper_interval A4_, A7_) m)
    then [[]]
    else maps (fn k =>
                mapa (fn a => k :: a)
                  (sorted_list_of_maximal_sequences_in_tree
                    (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
                    (the (lookupa (ccompare_cproper_interval A4_, A5_) m k))))
           (sorted_list_of_set (A3_, ccompare_cproper_interval A4_, A5_, A6_)
             (keys (A2_, A3_, ccompare_cproper_interval A4_, A7_) m)));

fun traces_to_check (A1_, A2_) (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_) m q k =
  (if equal_nata k zero_nat then []
    else let
           val a =
             product (inputs_as_list (B1_, B2_, B3_, B4_) m)
               (outputs_as_list (C1_, C2_, C3_, C4_) m);
         in
           maps (fn (x, y) =>
                  (case h_obs (A1_, A2_) (B2_, B3_) (C2_, C3_) m q x y
                    of NONE => [[(x, y)]]
                    | SOME qa =>
                      [(x, y)] ::
                        mapa (fn aa => (x, y) :: aa)
                          (traces_to_check (A1_, A2_) (B1_, B2_, B3_, B4_)
                            (C1_, C2_, C3_, C4_) m qa (minus_nat k one_nata))))
             a
         end);

fun establish_convergence_static (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun ma v ta g cg_insert
  cg_lookup m t =
  let
    val alpha = v (fst t);
    val xy = (fst (snd t), fst (snd (snd t)));
    val beta = v (snd (snd (snd t)));
    val _ =
      after (A3_, A4_) (ccompare_cproper_interval B4_, B5_)
        (ccompare_cproper_interval C4_, C5_) ma (initial ma) (v (fst t));
    val qTarget =
      after (A3_, A4_) (ccompare_cproper_interval B4_, B5_)
        (ccompare_cproper_interval C4_, C5_) ma (initial ma)
        (v (snd (snd (snd t))));
    val k =
      minus_nat m
        (card (A1_, A2_, A3_)
          (reachable_states (A1_, A2_, A3_, A4_, A5_, A6_)
            (B3_, ccompare_cproper_interval B4_, B5_, B7_)
            (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma));
    val ttc =
      [] :: traces_to_check (A3_, A4_)
              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
              (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma qTarget k;
    val handleTrace =
      (fn (tb, ga) => fn u =>
        (if is_in_language (A3_, A4_) (ccompare_cproper_interval B4_, B5_)
              (ccompare_cproper_interval C4_, C5_) ma qTarget u
          then let
                 val qu =
                   after (A3_, A4_) (ccompare_cproper_interval B4_, B5_)
                     (ccompare_cproper_interval C4_, C5_) ma qTarget u;
                 val ws =
                   sorted_list_of_maximal_sequences_in_tree
                     (card_UNIV_prod B1_ C1_, cenum_prod B2_ C2_,
                       ceq_prod B3_ C3_, cproper_interval_prod B4_ C4_,
                       equal_prod B5_ C5_, linorder_prod B7_ C7_,
                       set_impl_prod B8_ C8_)
                     (dist_fun (suc (size_list u)) qu);
                 val appendDistTrace =
                   (fn (tc, gb) => fn w =>
                     let
                       val (td, gc) =
                         distribute_extension (A3_, A4_, A5_)
                           (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                           (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_)
                           ma tc gb cg_lookup cg_insert alpha (xy :: u @ w)
                           false
                           (append_heuristic_input A5_
                             (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                             (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_)
                             ma);
                     in
                       distribute_extension (A3_, A4_, A5_)
                         (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                         (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma
                         td gc cg_lookup cg_insert beta (u @ w) false
                         (append_heuristic_input A5_
                           (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                           (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_)
                           ma)
                     end);
               in
                 foldl appendDistTrace (tb, ga) ws
               end
          else let
                 val (tc, gb) =
                   distribute_extension (A3_, A4_, A5_)
                     (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                     (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma tb
                     ga cg_lookup cg_insert alpha (xy :: u) false
                     (append_heuristic_input A5_
                       (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                       (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma);
               in
                 distribute_extension (A3_, A4_, A5_)
                   (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                   (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma tc gb
                   cg_lookup cg_insert beta u false
                   (append_heuristic_input A5_
                     (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                     (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma)
               end));
  in
    foldl handleTrace (ta, g) ttc
  end;

fun handleUT_static (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun m v ta g cg_insert cg_lookup
  cg_merge l t x =
  let
    val (t1, g1) =
      handle_io_pair (A3_, A4_, A5_)
        (ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) false false m v ta g
        cg_insert cg_lookup (fst t) (fst (snd t)) (fst (snd (snd t)));
    val (t2, g2) =
      establish_convergence_static (A1_, A2_, A3_, A4_, A5_, A6_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
        (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun m v t1 g1 cg_insert
        cg_lookup l t;
    val g3 =
      cg_merge g2 (v (fst t) @ [(fst (snd t), fst (snd (snd t)))])
        (v (snd (snd (snd t))));
  in
    (x, (t2, g3))
  end;

fun state_separator_from_input_choices (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m cSep q1 q2 cs =
  let
    val css =
      set (ceq_prod (ceq_sum (ceq_prod A2_ A2_) A2_) B1_,
            ccompare_prod (ccompare_sum (ccompare_prod A3_ A3_) A3_) B2_,
            set_impl_prod (set_impl_sum (set_impl_prod A6_ A6_) A6_) B5_)
        cs;
    val cssQ =
      sup_seta
        (ceq_sum (ceq_prod A2_ A2_) A2_,
          ccompare_sum (ccompare_prod A3_ A3_) A3_)
        (set (ceq_sum (ceq_prod A2_ A2_) A2_,
               ccompare_sum (ccompare_prod A3_ A3_) A3_,
               set_impl_sum (set_impl_prod A6_ A6_) A6_)
          (mapa fst cs))
        (insert
          (ceq_sum (ceq_prod A2_ A2_) A2_,
            ccompare_sum (ccompare_prod A3_ A3_) A3_)
          (Inr q1)
          (insert
            (ceq_sum (ceq_prod A2_ A2_) A2_,
              ccompare_sum (ccompare_prod A3_ A3_) A3_)
            (Inr q2)
            (bot_set
              (ceq_sum (ceq_prod A2_ A2_) A2_,
                ccompare_sum (ccompare_prod A3_ A3_) A3_,
                set_impl_sum (set_impl_prod A6_ A6_) A6_))));
    val s0 =
      filter_states
        (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
          ceq_sum (ceq_prod A2_ A2_) A2_,
          ccompare_sum (ccompare_prod A3_ A3_) A3_,
          set_impl_sum (set_impl_prod A6_ A6_) A6_)
        (B1_, B2_) (C1_, C2_, C3_, C4_) cSep
        (fn q =>
          member
            (ceq_sum (ceq_prod A2_ A2_) A2_,
              ccompare_sum (ccompare_prod A3_ A3_) A3_)
            q cssQ);
    val s1 =
      filter_transitions
        (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
          ceq_sum (ceq_prod A2_ A2_) A2_,
          ccompare_sum (ccompare_prod A3_ A3_) A3_,
          equal_sum (equal_prod A4_ A4_) A4_,
          mapping_impl_sum (mapping_impl_prod A5_ A5_) A5_,
          set_impl_sum (set_impl_prod A6_ A6_) A6_)
        (B1_, B2_, B3_, B4_) (C1_, C2_, C3_, C4_, C5_) s0
        (fn t =>
          member
            (ceq_prod (ceq_sum (ceq_prod A2_ A2_) A2_) B1_,
              ccompare_prod (ccompare_sum (ccompare_prod A3_ A3_) A3_) B2_)
            (fst t, fst (snd t)) css);
  in
    s1
  end;

fun state_separator_from_s_states (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  q1 q2 =
  let
    val c =
      canonical_separatorb (A1_, A2_, A3_, A4_, A5_, A6_, A8_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B8_)
        (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C6_, C7_) m q1 q2;
    val cs =
      select_inputs
        (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
          ceq_sum (ceq_prod A3_ A3_) A3_,
          cproper_interval_sum (cproper_interval_prod A4_ A4_) A4_)
        (C1_, C3_, C4_)
        (h (ceq_sum (ceq_prod A3_ A3_) A3_,
             ccompare_sum
               (ccompare_prod (ccompare_cproper_interval A4_)
                 (ccompare_cproper_interval A4_))
               (ccompare_cproper_interval A4_),
             equal_sum (equal_prod A5_ A5_) A5_,
             mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_,
             set_impl_sum (set_impl_prod A8_ A8_) A8_)
          (ccompare_cproper_interval B4_, B5_, B6_)
          (C3_, ccompare_cproper_interval C4_, C7_) c)
        (initial c)
        (inputs_as_list (B3_, ccompare_cproper_interval B4_, B5_, B7_) c)
        (remove1 (equal_sum (equal_prod A5_ A5_) A5_) (Inl (q1, q2))
          (remove1 (equal_sum (equal_prod A5_ A5_) A5_) (Inr q1)
            (remove1 (equal_sum (equal_prod A5_ A5_) A5_) (Inr q2)
              (states_as_list
                (ceq_sum (ceq_prod A3_ A3_) A3_,
                  ccompare_sum
                    (ccompare_prod (ccompare_cproper_interval A4_)
                      (ccompare_cproper_interval A4_))
                    (ccompare_cproper_interval A4_),
                  equal_sum (equal_prod A5_ A5_) A5_,
                  linorder_sum (equal_prod A5_ A5_, linorder_prod A7_ A7_)
                    (A5_, A7_))
                c))))
        (insert
          (ceq_sum (ceq_prod A3_ A3_) A3_,
            ccompare_sum
              (ccompare_prod (ccompare_cproper_interval A4_)
                (ccompare_cproper_interval A4_))
              (ccompare_cproper_interval A4_))
          (Inr q1)
          (insert
            (ceq_sum (ceq_prod A3_ A3_) A3_,
              ccompare_sum
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval A4_))
                (ccompare_cproper_interval A4_))
            (Inr q2)
            (bot_set
              (ceq_sum (ceq_prod A3_ A3_) A3_,
                ccompare_sum
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval A4_),
                set_impl_sum (set_impl_prod A8_ A8_) A8_))))
        [];
  in
    (if equal_nata (size_list cs) zero_nat then NONE
      else (if equal_suma (equal_prod A5_ A5_) A5_ (fst (last cs))
                 (Inl (q1, q2))
             then SOME (state_separator_from_input_choices
                         (A1_, A3_, ccompare_cproper_interval A4_, A5_, A6_,
                           A8_)
                         (B3_, ccompare_cproper_interval B4_, B5_, B6_, B8_)
                         (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) m c
                         q1 q2 cs)
             else NONE))
  end;

fun h_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) =
  h_framework_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_)
    (fn _ => fn _ => fn _ => fn _ => fn _ => false);

fun distance_at_most (A1_, A2_, A3_, A4_, A5_, A6_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_) m q1 q2 k =
  (if equal_nata k zero_nat then eq A3_ q1 q2
    else eq A3_ q1 q2 orelse
           bex (B1_, B2_) (inputs m)
             (fn x =>
               bex (ceq_prod C1_ A1_, ccompare_prod C2_ A2_)
                 (h (A1_, A2_, A3_, A4_, A6_) (B2_, B3_, B4_) (C1_, C2_, C4_) m
                   (q1, x))
                 (fn (_, q1a) =>
                   distance_at_most (A1_, A2_, A3_, A4_, A5_, A6_)
                     (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_) m q1a q2
                     (minus_nat k one_nata))));

fun r_distinguishable_state_pairs_with_separators
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = set (ceq_prod (ceq_prod A3_ A3_)
           (ceq_fsm
             (cenum_sum (cenum_prod A2_ A2_) A2_,
               ceq_sum (ceq_prod A3_ A3_) A3_,
               ccompare_sum
                 (ccompare_prod (ccompare_cproper_interval A4_)
                   (ccompare_cproper_interval A4_))
                 (ccompare_cproper_interval A4_),
               equal_sum (equal_prod A5_ A5_) A5_)
             (B2_, B3_, ccompare_cproper_interval B4_)
             (C2_, C3_, ccompare_cproper_interval C4_)),
          ccompare_prod
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_))
            ccompare_fsm,
          set_impl_prod (set_impl_prod A8_ A8_) set_impl_fsm)
      (concat
        (map_filter
          (fn x =>
            (if let
                  val (_, a) = x;
                in
                  not (is_none a)
                end
              then SOME let
                          val (a, b) = x;
                        in
                          let
                            val (q1, q2) = a;
                          in
                            (fn aa => [((q1, q2), the aa), ((q2, q1), the aa)])
                          end
                            b
                        end
              else NONE))
          (map_filter
            (fn x =>
              (if let
                    val (a, b) = x;
                  in
                    less ((ord_preorder o preorder_order o order_linorder) A7_)
                      a b
                  end
                then SOME let
                            val (q1, q2) = x;
                          in
                            ((q1, q2),
                              state_separator_from_s_states
                                (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                                (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
                                (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m q1 q2)
                          end
                else NONE))
            (product
              (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) m)
              (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
                m)))));

fun pairwise_r_distinguishable_state_sets_from_separators_list
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = let
      val rds =
        image (ceq_prod (ceq_prod A3_ A3_)
                 (ceq_fsm
                   (cenum_sum (cenum_prod A2_ A2_) A2_,
                     ceq_sum (ceq_prod A3_ A3_) A3_,
                     ccompare_sum
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_cproper_interval A4_))
                       (ccompare_cproper_interval A4_),
                     equal_sum (equal_prod A5_ A5_) A5_)
                   (B2_, B3_, ccompare_cproper_interval B4_)
                   (C2_, C3_, ccompare_cproper_interval C4_)),
                ccompare_prod
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
                  ccompare_fsm)
          (ceq_prod A3_ A3_,
            ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_),
            set_impl_prod A8_ A8_)
          fst (r_distinguishable_state_pairs_with_separators
                (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
                (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m);
    in
      filtera
        (fn s =>
          ball (A3_, ccompare_cproper_interval A4_) s
            (fn q1 =>
              ball (A3_, ccompare_cproper_interval A4_) s
                (fn q2 =>
                  (if not (eq A5_ q1 q2)
                    then member
                           (ceq_prod A3_ A3_,
                             ccompare_prod (ccompare_cproper_interval A4_)
                               (ccompare_cproper_interval A4_))
                           (q1, q2) rds
                    else true))))
        (mapa (set (A3_, ccompare_cproper_interval A4_, A8_))
          (pow_list
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) m)))
    end;

fun maximal_pairwise_r_distinguishable_state_sets_from_separators_list
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = remove_subsets (A2_, A3_, ccompare_cproper_interval A4_)
      (pairwise_r_distinguishable_state_sets_from_separators_list
        (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
        (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m);

fun calculate_state_preamble_from_input_choices
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_, B5_, B6_)
  (C1_, C2_, C3_, C4_, C5_, C6_) m q =
  (if eq A4_ q (initial m)
    then SOME (initial_preamble (A2_, ccompare_cproper_interval A3_, A5_, A7_)
                (B1_, B2_, B4_, B6_) (C2_, ccompare_cproper_interval C3_, C6_)
                m)
    else let
           val ds =
             d_states (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
               (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C6_) m q;
           val dss =
             set (ceq_prod A2_ B1_,
                   ccompare_prod (ccompare_cproper_interval A3_) B2_,
                   set_impl_prod A7_ B6_)
               ds;
         in
           (case ds of [] => NONE
             | _ :: _ =>
               (if eq A4_ (fst (last ds)) (initial m)
                 then SOME (filter_transitions
                             (A1_, A2_, ccompare_cproper_interval A3_, A4_, A5_,
                               A7_)
                             (B1_, B2_, B3_, B4_)
                             (C2_, ccompare_cproper_interval C3_, C4_, C5_, C6_)
                             m (fn t =>
                                 member
                                   (ceq_prod A2_ B1_,
                                     ccompare_prod
                                       (ccompare_cproper_interval A3_) B2_)
                                   (fst t, fst (snd t)) dss))
                 else NONE))
         end);

fun d_reachable_states_with_preambles (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m =
  image (ceq_prod A3_
           (ceq_option
             (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
               (B1_, B2_, B3_) (C2_, C3_, ccompare_cproper_interval C4_))),
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_option ccompare_fsm))
    (ceq_prod A3_
       (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_) (B1_, B2_, B3_)
         (C2_, C3_, ccompare_cproper_interval C4_)),
      ccompare_prod (ccompare_cproper_interval A4_) ccompare_fsm,
      set_impl_prod A8_ set_impl_fsm)
    (fn qp => (fst qp, the (snd qp)))
    (filter
      (ceq_prod A3_
         (ceq_option
           (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
             (B1_, B2_, B3_) (C2_, C3_, ccompare_cproper_interval C4_))),
        ccompare_prod (ccompare_cproper_interval A4_)
          (ccompare_option ccompare_fsm))
      (fn qp => not (is_none (snd qp)))
      (image (A3_, ccompare_cproper_interval A4_)
        (ceq_prod A3_
           (ceq_option
             (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
               (B1_, B2_, B3_) (C2_, C3_, ccompare_cproper_interval C4_))),
          ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_option ccompare_fsm),
          set_impl_prod A8_ (set_impl_option set_impl_fsm))
        (fn q =>
          (q, calculate_state_preamble_from_input_choices
                (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
                (B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C3_, C4_, C5_, C6_, C7_) m
                q))
        (states m)));

fun maximal_repetition_sets_from_separators_list_naive
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = let
      val dr =
        image (ceq_prod A3_
                 (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
                   (B2_, B3_, ccompare_cproper_interval B4_)
                   (C2_, C3_, ccompare_cproper_interval C4_)),
                ccompare_prod (ccompare_cproper_interval A4_) ccompare_fsm)
          (A3_, ccompare_cproper_interval A4_, A8_) fst
          (d_reachable_states_with_preambles
            (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
            (B2_, B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_, B8_)
            (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m);
    in
      mapa (fn s => (s, inf_seta (A3_, ccompare_cproper_interval A4_) s dr))
        (maximal_pairwise_r_distinguishable_state_sets_from_separators_list
          (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
          (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
          (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m)
    end;

fun calculate_test_suite_for_repetition_sets
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma m repetition_sets =
  let
    val states_with_preambles =
      d_reachable_states_with_preambles (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
        (B2_, B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_, B8_)
        (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma;
    val pairs_with_separators =
      image (ceq_prod (ceq_prod A3_ A3_)
               (ceq_fsm
                 (cenum_sum (cenum_prod A2_ A2_) A2_,
                   ceq_sum (ceq_prod A3_ A3_) A3_,
                   ccompare_sum
                     (ccompare_prod (ccompare_cproper_interval A4_)
                       (ccompare_cproper_interval A4_))
                     (ccompare_cproper_interval A4_),
                   equal_sum (equal_prod A5_ A5_) A5_)
                 (B2_, B3_, ccompare_cproper_interval B4_)
                 (C2_, C3_, ccompare_cproper_interval C4_)),
              ccompare_prod
                (ccompare_prod (ccompare_cproper_interval A4_)
                  (ccompare_cproper_interval A4_))
                ccompare_fsm)
        (ceq_prod (ceq_prod A3_ A3_)
           (ceq_prod
             (ceq_fsm
               (cenum_sum (cenum_prod A2_ A2_) A2_,
                 ceq_sum (ceq_prod A3_ A3_) A3_,
                 ccompare_sum
                   (ccompare_prod (ccompare_cproper_interval A4_)
                     (ccompare_cproper_interval A4_))
                   (ccompare_cproper_interval A4_),
                 equal_sum (equal_prod A5_ A5_) A5_)
               (B2_, B3_, ccompare_cproper_interval B4_)
               (C2_, C3_, ccompare_cproper_interval C4_))
             (ceq_prod (ceq_sum (ceq_prod A3_ A3_) A3_)
               (ceq_sum (ceq_prod A3_ A3_) A3_))),
          ccompare_prod
            (ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_))
            (ccompare_prod ccompare_fsm
              (ccompare_prod
                (ccompare_sum
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval A4_))
                (ccompare_sum
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
                  (ccompare_cproper_interval A4_)))),
          set_impl_prod (set_impl_prod A8_ A8_)
            (set_impl_prod set_impl_fsm
              (set_impl_prod (set_impl_sum (set_impl_prod A8_ A8_) A8_)
                (set_impl_sum (set_impl_prod A8_ A8_) A8_))))
        (fn (a, b) => let
                        val (q1, q2) = a;
                      in
                        (fn aa => ((q1, q2), (aa, (Inr q1, Inr q2))))
                      end
                        b)
        (r_distinguishable_state_pairs_with_separators
          (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
          (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
          (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma);
  in
    combine_test_suite (A1_, A2_, A3_, A4_, A5_, A6_, A8_)
      (B2_, B3_, ccompare_cproper_interval B4_, B5_, B8_)
      (C2_, C3_, ccompare_cproper_interval C4_, C5_, C7_)
      (cenum_sum (cenum_prod A2_ A2_) A2_, ceq_sum (ceq_prod A3_ A3_) A3_,
        ccompare_sum
          (ccompare_prod (ccompare_cproper_interval A4_)
            (ccompare_cproper_interval A4_))
          (ccompare_cproper_interval A4_),
        equal_sum (equal_prod A5_ A5_) A5_,
        set_impl_sum (set_impl_prod A8_ A8_) A8_)
      ma m states_with_preambles pairs_with_separators repetition_sets
  end;

fun calculate_test_suite_naive (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma m =
  calculate_test_suite_for_repetition_sets
    (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
    ma m
    (maximal_repetition_sets_from_separators_list_naive
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma);

fun handle_state_cover_static (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) dist_set m v cg_initial
  cg_insert cg_lookup =
  let
    val separate_state =
      (fn t => fn q =>
        combine_after
          (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
          t (v q) (dist_set zero_nat q));
    val t =
      foldl separate_state
        (emptyc (ccompare_prod B2_ C2_, mapping_impl_prod B4_ C4_))
        (reachable_states_as_list (A1_, A2_, A3_, A4_, A5_, A6_)
          (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) m);
    val a = cg_initial m t;
  in
    (t, a)
  end;

fun h_framework_static_with_empty_graph (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m1 dist_fun m =
  h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
    (get_state_cover_assignment
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C7_))
    (handle_state_cover_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) dist_fun)
    (fn _ => fn _ => fn ts => ts)
    (handleUT_static (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun)
    (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
      (ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) false false)
    empty_cg_initial empty_cg_insert empty_cg_lookup empty_cg_merge m;

fun w_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distSet =
      from_list
        (ccompare_prod (ccompare_cproper_interval B4_)
           (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
        (mapa (fn (a, b) => distHelper a b) pairs);
    val distFun = (fn _ => fn _ => distSet);
  in
    h_framework_static_with_empty_graph (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun greedy_pairwise_r_distinguishable_state_sets_from_separators
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = let
      val pwrds =
        image (ceq_prod (ceq_prod A3_ A3_)
                 (ceq_fsm
                   (cenum_sum (cenum_prod A2_ A2_) A2_,
                     ceq_sum (ceq_prod A3_ A3_) A3_,
                     ccompare_sum
                       (ccompare_prod (ccompare_cproper_interval A4_)
                         (ccompare_cproper_interval A4_))
                       (ccompare_cproper_interval A4_),
                     equal_sum (equal_prod A5_ A5_) A5_)
                   (B2_, B3_, ccompare_cproper_interval B4_)
                   (C2_, C3_, ccompare_cproper_interval C4_)),
                ccompare_prod
                  (ccompare_prod (ccompare_cproper_interval A4_)
                    (ccompare_cproper_interval A4_))
                  ccompare_fsm)
          (ceq_prod A3_ A3_,
            ccompare_prod (ccompare_cproper_interval A4_)
              (ccompare_cproper_interval A4_),
            set_impl_prod A8_ A8_)
          fst (r_distinguishable_state_pairs_with_separators
                (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
                (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m);
      val k = size (A1_, A3_, ccompare_cproper_interval A4_) m;
      val nL = states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) m;
    in
      mapa (fn q =>
             set (A3_, ccompare_cproper_interval A4_, A8_)
               (extend_until_conflict (A3_, ccompare_cproper_interval A4_) pwrds
                 (remove1 A5_ q nL) [q] k))
        nL
    end;

fun maximal_repetition_sets_from_separators_list_greedy
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m
  = let
      val dr =
        image (ceq_prod A3_
                 (ceq_fsm (A2_, A3_, ccompare_cproper_interval A4_, A5_)
                   (B2_, B3_, ccompare_cproper_interval B4_)
                   (C2_, C3_, ccompare_cproper_interval C4_)),
                ccompare_prod (ccompare_cproper_interval A4_) ccompare_fsm)
          (A3_, ccompare_cproper_interval A4_, A8_) fst
          (d_reachable_states_with_preambles
            (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
            (B2_, B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_, B8_)
            (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m);
    in
      remdups
        (equal_prod (equal_set (A2_, A3_, ccompare_cproper_interval A4_, A5_))
          (equal_set (A2_, A3_, ccompare_cproper_interval A4_, A5_)))
        (mapa (fn s => (s, inf_seta (A3_, ccompare_cproper_interval A4_) s dr))
          (greedy_pairwise_r_distinguishable_state_sets_from_separators
            (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
            (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
            (C1_, C2_, C3_, C4_, C5_, C6_, C7_) m))
    end;

fun calculate_test_suite_greedy (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma m =
  calculate_test_suite_for_repetition_sets
    (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
    ma m
    (maximal_repetition_sets_from_separators_list_greedy
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma);

fun test_suite_from_io_tree (A1_, A2_) (B1_, B2_, B3_) (C1_, C2_, C3_) ma q
  (MPT (RBT_Mapping m)) =
  (case ccompare_proda B1_ C1_
    of NONE =>
      (raise Fail "test_suite_from_io_tree RBT_set: ccompare = None")
        (fn _ =>
          test_suite_from_io_tree (A1_, A2_) (B1_, B2_, B3_) (C1_, C2_, C3_) ma
            q (MPT (RBT_Mapping m)))
    | SOME _ =>
      MPT (tabulate
            (ccompare_prod (ccompare_prod B1_ C1_) ccompare_bool,
              equal_prod (equal_prod B2_ C2_) equal_bool,
              mapping_impl_prod (mapping_impl_prod B3_ C3_) mapping_impl_bool)
            (mapa (fn (a, b) =>
                    let
                      val (x, y) = a;
                    in
                      (fn _ =>
                        ((x, y),
                          not (is_none
                                (h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) ma q x
                                  y))))
                    end
                      b)
              (entriesa (ccompare_prod B1_ C1_) m))
            (fn (a, b) =>
              let
                val (x, y) = a;
              in
                (fn _ =>
                  (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) ma q x y
                    of NONE =>
                      emptyc
                        (ccompare_prod (ccompare_prod B1_ C1_) ccompare_bool,
                          mapping_impl_prod (mapping_impl_prod B3_ C3_)
                            mapping_impl_bool)
                    | SOME qa =>
                      test_suite_from_io_tree (A1_, A2_) (B1_, B2_, B3_)
                        (C1_, C2_, C3_) ma qa
                        let
                          val SOME t = lookupb (ccompare_prod B1_ C1_) m (x, y);
                        in
                          t
                        end))
              end
                b)));

fun get_initial_test_suite_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) v ma m =
  let
    val rstates =
      reachable_states_as_list (A1_, A2_, A3_, A4_, A6_, A7_)
        (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) ma;
    val n =
      card (A1_, A2_, A3_)
        (reachable_states (A1_, A2_, A3_, A4_, A6_, A7_) (B1_, B2_, B3_, B5_)
          (C1_, C2_, C3_, C5_) ma);
    val iM = inputs_as_list (B1_, B2_, B3_, B5_) ma;
    val hMap =
      map_of (equal_prod A4_ B3_)
        (mapa (fn (q, x) =>
                ((q, x),
                  mapa (fn (y, qa) => (q, (x, (y, qa))))
                    (sorted_list_of_set
                      (ceq_prod C1_ A2_, ccompare_prod C2_ A3_,
                        equal_prod C3_ A4_, linorder_prod C5_ A6_)
                      (h (A2_, A3_, A4_, A5_, A7_) (B2_, B3_, B4_)
                        (C1_, C2_, C6_) ma (q, x)))))
          (product (states_as_list (A2_, A3_, A4_, A6_) ma) iM));
    val _ = (fn q => fn x => (case hMap (q, x) of NONE => [] | SOME ts => ts));
    val t =
      from_list
        (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
        (maps (fn q =>
                mapa (fn a => v q @ a)
                  (h_extensions (A2_, A3_, A4_, A5_, A6_, A7_)
                    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C5_, C6_) ma q
                    (minus_nat m n)))
          rstates);
  in
    t
  end;

fun pair_framework_h_components (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) ma m
  get_separating_traces =
  let
    val v =
      get_state_cover_assignment (A1_, A2_, A3_, A4_, A6_, A7_)
        (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) ma;
  in
    pair_framework A6_ (B2_, B3_, B4_, B5_) (C2_, C3_, C4_, C5_) ma m
      (get_initial_test_suite_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
        (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) v)
      (get_pairs_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B4_, B5_)
        (C1_, C2_, C3_, C5_, C6_) v)
      get_separating_traces
  end;

fun contains_distinguishing_trace (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m (MPT t1) q1 q2 =
  bex (ceq_prod B2_ C2_, ccompare_prod B3_ C3_)
    (keys (cenum_prod B1_ C1_, ceq_prod B2_ C2_, ccompare_prod B3_ C3_,
            set_impl_prod B5_ C5_)
      t1)
    (fn (x, y) =>
      (case h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q1 x y
        of NONE =>
          not (is_none (h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q2 x y))
        | SOME q1a =>
          (case h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q2 x y of NONE => true
            | SOME a =>
              contains_distinguishing_trace (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
                (C1_, C2_, C3_, C4_, C5_) m
                (the (lookupa (ccompare_prod B3_ C3_, equal_prod B4_ C4_) t1
                       (x, y)))
                q1a a)));

fun w_method_via_h_framework_2 (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val handlePair =
      (fn w => fn (q, qa) =>
        (if contains_distinguishing_trace (ccompare_cproper_interval A4_, A5_)
              (B2_, B3_, ccompare_cproper_interval B4_, B5_, B8_)
              (C2_, C3_, ccompare_cproper_interval C4_, C5_, C8_) ma w q qa
          then w
          else insertb
                 (ccompare_prod (ccompare_cproper_interval B4_)
                    (ccompare_cproper_interval C4_),
                   equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                 w (distHelper q qa)));
    val distSet =
      foldl handlePair
        (emptyc
          (ccompare_prod (ccompare_cproper_interval B4_)
             (ccompare_cproper_interval C4_),
            mapping_impl_prod B6_ C6_))
        pairs;
    val distFun = (fn _ => fn _ => distSet);
  in
    h_framework_static_with_empty_graph (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun spy_framework_static_with_empty_graph
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m1 dist_fun m =
  spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
    (get_state_cover_assignment
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C7_))
    (handle_state_cover_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) dist_fun)
    (fn _ => fn _ => fn ts => ts)
    (establish_convergence_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun)
    (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
      (ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) false true)
    empty_cg_initial empty_cg_insert empty_cg_lookup empty_cg_merge m;

fun w_method_via_spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distSet =
      from_list
        (ccompare_prod (ccompare_cproper_interval B4_)
           (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
        (mapa (fn (a, b) => distHelper a b) pairs);
    val distFun = (fn _ => fn _ => distSet);
  in
    spy_framework_static_with_empty_graph
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun wp_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distSet =
      from_list
        (ccompare_prod (ccompare_cproper_interval B4_)
           (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
        (mapa (fn (a, b) => distHelper a b) pairs);
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val l =
      suc (minus_nat m
            (card (A1_, A3_, ccompare_cproper_interval A4_)
              (reachable_states
                (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma)));
    val distFun =
      (fn k => fn q =>
        (if equal_nata k l
          then (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
                 then the (hsiMap q)
                 else get_HSI
                        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma
                        q)
          else distSet));
  in
    h_framework_static_with_empty_graph (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun intersection_is_distinguishing (A1_, A2_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) m (MPT t1) q1 (MPT t2) q2 =
  bex (ceq_prod B2_ C2_, ccompare_prod B3_ C3_)
    (inf_seta (ceq_prod B2_ C2_, ccompare_prod B3_ C3_)
      (keys (cenum_prod B1_ C1_, ceq_prod B2_ C2_, ccompare_prod B3_ C3_,
              set_impl_prod B5_ C5_)
        t1)
      (keys (cenum_prod B1_ C1_, ceq_prod B2_ C2_, ccompare_prod B3_ C3_,
              set_impl_prod B5_ C5_)
        t2))
    (fn (x, y) =>
      (case h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q1 x y
        of NONE =>
          not (is_none (h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q2 x y))
        | SOME q1a =>
          (case h_obs (A1_, A2_) (B3_, B4_) (C3_, C4_) m q2 x y of NONE => true
            | SOME a =>
              intersection_is_distinguishing (A1_, A2_)
                (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m
                (the (lookupa (ccompare_prod B3_ C3_, equal_prod B4_ C4_) t1
                       (x, y)))
                q1a (the (lookupa (ccompare_prod B3_ C3_, equal_prod B4_ C4_) t2
                           (x, y)))
                a)));

fun add_distinguishing_sequence_if_required (A1_, A2_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  dist_fun m ((alpha, q1), (beta, q2)) t =
  (if intersection_is_distinguishing (A1_, A2_) (B1_, B2_, B3_, B4_, B7_)
        (C1_, C2_, C3_, C4_, C7_) m
        (aftera
          (ccompare_prod B3_ C3_, equal_prod B4_ C4_, mapping_impl_prod B5_ C5_)
          t alpha)
        q1 (aftera
             (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
               mapping_impl_prod B5_ C5_)
             t beta)
        q2
    then emptyc (ccompare_prod B3_ C3_, mapping_impl_prod B5_ C5_)
    else insertb
           (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
             mapping_impl_prod B5_ C5_)
           (emptyc (ccompare_prod B3_ C3_, mapping_impl_prod B5_ C5_))
           (dist_fun q1 q2));

fun h_method_via_pair_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma m =
  let
    val tables =
      compute_ofsm_tables (A2_, A3_, A4_, A5_, A6_, A7_, A8_) (B2_, B3_, B4_)
        (C2_, C3_, C4_) ma (minus_nat (size (A1_, A3_, A4_) ma) one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
                              (B2_, B3_, B4_, B6_) (C2_, C3_, C4_, C6_) tables
                              ma q1 q2)
                        end
              else NONE))
          (product (states_as_list (A3_, A4_, A5_, A7_) ma)
            (states_as_list (A3_, A4_, A5_, A7_) ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, A4_) q1 (states ma) andalso
              (member (A3_, A4_) q2 (states ma) andalso not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, A4_, A5_, A8_) (B2_, B3_, B4_, B6_)
                 (C2_, C3_, C4_, C6_) ma q1 q2));
    val a =
      add_distinguishing_sequence_if_required (A4_, A5_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
        distHelper;
  in
    pair_framework_h_components (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B2_, B3_, B4_, B5_, B6_) (C2_, C3_, C4_, C5_, C6_, C7_) ma m a
  end;

fun w_method_via_pair_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) ma m =
  let
    val tables =
      compute_ofsm_tables (A2_, A3_, A4_, A5_, A6_, A7_, A8_) (B1_, B2_, B3_)
        (C1_, C2_, C3_) ma (minus_nat (size (A1_, A3_, A4_) ma) one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
                              (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) tables
                              ma q1 q2)
                        end
              else NONE))
          (product (states_as_list (A3_, A4_, A5_, A7_) ma)
            (states_as_list (A3_, A4_, A5_, A7_) ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, A4_) q1 (states ma) andalso
              (member (A3_, A4_) q2 (states ma) andalso not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, A4_, A5_, A8_) (B1_, B2_, B3_, B5_)
                 (C1_, C2_, C3_, C5_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs (states_as_list (A3_, A4_, A5_, A7_) ma));
    val distSet =
      from_list
        (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
        (mapa (fn (a, b) => distHelper a b) pairs);
    val a = (fn _ => fn _ => fn _ => distSet);
  in
    pair_framework_h_components (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) ma m a
  end;

fun hsi_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distFun =
      (fn _ => fn q =>
        (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
          then the (hsiMap q)
          else get_HSI (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma q));
  in
    h_framework_static_with_empty_graph (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun complete_inputs_to_tree_initial (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_, C5_) m xs =
  complete_inputs_to_tree (A1_, A2_, A3_) (B1_, B2_, B3_, B4_)
    (C2_, C3_, C4_, C5_) m (initial m) (outputs_as_list (C1_, C2_, C3_, C5_) m)
    xs;

fun get_initial_test_suite_H_2 (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) c v ma m =
  (if c then get_initial_test_suite_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
               (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
               (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_, C8_) v ma m
    else let
           val ts =
             get_initial_test_suite_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
               (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
               (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_, C8_) v ma m;
           val xss =
             mapa (mapa fst)
               (sorted_list_of_maximal_sequences_in_tree
                 (card_UNIV_prod B1_ C1_, cenum_prod B2_ C2_, ceq_prod B3_ C3_,
                   cproper_interval_prod B4_ C4_, equal_prod B5_ C5_,
                   linorder_prod B7_ C7_, set_impl_prod B8_ C8_)
                 ts);
           val _ =
             outputs_as_list (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma;
         in
           foldl (fn t => fn xs =>
                   combinea
                     (ccompare_prod (ccompare_cproper_interval B4_)
                       (ccompare_cproper_interval C4_))
                     t (complete_inputs_to_tree_initial (A3_, A4_, A6_)
                         (ccompare_cproper_interval B4_, B5_, B6_, B7_)
                         (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma
                         xs))
             ts xss
         end);

fun pair_framework_h_components_2 (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m get_separating_traces c =
  let
    val v =
      get_state_cover_assignment (A1_, A2_, A3_, A4_, A6_, A7_)
        (B3_, ccompare_cproper_interval B4_, B5_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma;
  in
    pair_framework A6_ (ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (ccompare_cproper_interval C4_, C5_, C6_, C7_) ma m
      (get_initial_test_suite_H_2 (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
        (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
        (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) c v)
      (get_pairs_H (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C7_, C8_) v)
      get_separating_traces
  end;

fun h_framework_static_with_simple_graph
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m1 dist_fun m =
  h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
    (get_state_cover_assignment
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C7_))
    (handle_state_cover_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) dist_fun)
    (fn _ => fn _ => fn ts => ts)
    (handleUT_static (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun)
    (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
      (ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) false false)
    (simple_cg_initial (ccompare_cproper_interval A4_, A5_)
      (B1_, B2_, B3_, B4_, B5_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C7_, C8_))
    (simple_cg_insert
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        linorder_prod B7_ C7_))
    (simple_cg_lookup_with_conv
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        equal_prod B5_ C5_, linorder_prod B7_ C7_))
    (simple_cg_merge
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        equal_prod B5_ C5_))
    m;

fun spy_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distFun =
      (fn _ => fn q =>
        (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
          then the (hsiMap q)
          else get_HSI (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma q));
  in
    h_framework_static_with_simple_graph
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun wp_method_via_spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val pairs =
      filtera (fn (x, y) => not (eq A5_ x y))
        (list_ordered_pairs
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distSet =
      from_list
        (ccompare_prod (ccompare_cproper_interval B4_)
           (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
        (mapa (fn (a, b) => distHelper a b) pairs);
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val l =
      suc (minus_nat m
            (card (A1_, A3_, ccompare_cproper_interval A4_)
              (reachable_states
                (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma)));
    val distFun =
      (fn k => fn q =>
        (if equal_nata k l
          then (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
                 then the (hsiMap q)
                 else get_HSI
                        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma
                        q)
          else distSet));
  in
    spy_framework_static_with_empty_graph
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun add_distinguishing_sequence_and_complete_if_required (A1_, A2_, A3_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  distFun completeInputTraces m ((alpha, q1), (beta, q2)) t =
  (if intersection_is_distinguishing (A1_, A2_) (B1_, B2_, B3_, B4_, B7_)
        (C1_, C2_, C3_, C4_, C7_) m
        (aftera
          (ccompare_prod B3_ C3_, equal_prod B4_ C4_, mapping_impl_prod B5_ C5_)
          t alpha)
        q1 (aftera
             (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
               mapping_impl_prod B5_ C5_)
             t beta)
        q2
    then emptyc (ccompare_prod B3_ C3_, mapping_impl_prod B5_ C5_)
    else let
           val w = distFun q1 q2;
           val ta =
             insertb
               (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
                 mapping_impl_prod B5_ C5_)
               (emptyc (ccompare_prod B3_ C3_, mapping_impl_prod B5_ C5_)) w;
         in
           (if completeInputTraces
             then let
                    val t1 =
                      from_list
                        (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
                          mapping_impl_prod B5_ C5_)
                        (language_for_input (A1_, A2_, A3_) (B3_, B4_, B6_)
                          (C2_, C3_, C4_, C6_) m q1 (mapa fst w));
                    val t2 =
                      from_list
                        (ccompare_prod B3_ C3_, equal_prod B4_ C4_,
                          mapping_impl_prod B5_ C5_)
                        (language_for_input (A1_, A2_, A3_) (B3_, B4_, B6_)
                          (C2_, C3_, C4_, C6_) m q2 (mapa fst w));
                  in
                    combinea (ccompare_prod B3_ C3_) ta
                      (combinea (ccompare_prod B3_ C3_) t1 t2)
                  end
             else ta)
         end);

fun h_method_via_pair_framework_2 (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma m c
  = let
      val tables =
        compute_ofsm_tables (A2_, A3_, A4_, A5_, A6_, A7_, A8_) (B2_, B3_, B4_)
          (C2_, C3_, C4_) ma (minus_nat (size (A1_, A3_, A4_) ma) one_nata);
      val distMap =
        map_of (equal_prod A5_ A5_)
          (map_filter
            (fn x =>
              (if not (eq A5_ (fst x) (snd x))
                then SOME let
                            val (q1, q2) = x;
                          in
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
                                (B2_, B3_, B4_, B6_) (C2_, C3_, C4_, C6_) tables
                                ma q1 q2)
                          end
                else NONE))
            (product (states_as_list (A3_, A4_, A5_, A7_) ma)
              (states_as_list (A3_, A4_, A5_, A7_) ma)));
      val distHelper =
        (fn q1 => fn q2 =>
          (if member (A3_, A4_) q1 (states ma) andalso
                (member (A3_, A4_) q2 (states ma) andalso not (eq A5_ q1 q2))
            then the (distMap (q1, q2))
            else get_distinguishing_sequence_from_ofsm_tables
                   (A2_, A3_, A4_, A5_, A8_) (B2_, B3_, B4_, B6_)
                   (C2_, C3_, C4_, C6_) ma q1 q2));
      val a =
        add_distinguishing_sequence_and_complete_if_required (A4_, A5_, A7_)
          (B1_, B2_, B3_, B4_, B5_, B6_, B7_)
          (C1_, C2_, C3_, C4_, C5_, C6_, C7_) distHelper c;
    in
      pair_framework_h_components (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
        (B2_, B3_, B4_, B5_, B6_) (C2_, C3_, C4_, C5_, C6_, C7_) ma m a
    end;

fun find_cheapest_distinguishing_trace (A1_, A2_) (B1_, B2_, B3_, B4_)
  (C1_, C2_, C3_, C4_) m distFun ios (MPT m1) q1 (MPT m2) q2 =
  let
    val f =
      (fn (omega, (l, w)) => fn (x, y) =>
        let
          val w1L =
            (if is_leaf (ccompare_prod B1_ C1_) (MPT m1) then zero_nat
              else one_nata);
          val w1C =
            (if not (is_none
                      (lookupa (ccompare_prod B1_ C1_, equal_prod B2_ C2_) m1
                        (x, y)))
              then zero_nat else one_nata);
          val w1 = min ord_nat w1L w1C;
          val w2L =
            (if is_leaf (ccompare_prod B1_ C1_) (MPT m2) then zero_nat
              else one_nata);
          val w2C =
            (if not (is_none
                      (lookupa (ccompare_prod B1_ C1_, equal_prod B2_ C2_) m2
                        (x, y)))
              then zero_nat else one_nata);
          val w2 = min ord_nat w2L w2C;
          val wa = plus_nat w1 w2;
        in
          (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q1 x y
            of NONE =>
              (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q2 x y
                of NONE => (omega, (l, w))
                | SOME _ =>
                  (if equal_nata wa zero_nat orelse less_eq_nat wa w
                    then ([(x, y)], (plus_nat w1C w2C, wa))
                    else (omega, (l, w))))
            | SOME q1a =>
              (case h_obs (A1_, A2_) (B1_, B2_) (C1_, C2_) m q2 x y
                of NONE =>
                  (if equal_nata wa zero_nat orelse less_eq_nat wa w
                    then ([(x, y)], (plus_nat w1C w2C, wa))
                    else (omega, (l, w)))
                | SOME q2a =>
                  (if eq A2_ q1a q2a then (omega, (l, w))
                    else (case lookupa
                                 (ccompare_prod B1_ C1_, equal_prod B2_ C2_) m1
                                 (x, y)
                           of NONE =>
                             (case lookupa
                                     (ccompare_prod B1_ C1_, equal_prod B2_ C2_)
                                     m2 (x, y)
                               of NONE =>
                                 let
                                   val omegaa = distFun q1a q2a;
                                   val la =
                                     plus_nat (nat_of_integer (2 : IntInf.int))
                                       (times_nata
 (nat_of_integer (2 : IntInf.int)) (size_list omegaa));
                                 in
                                   (if less_nat wa w orelse
 equal_nata wa w andalso less_nat la l
                                     then ((x, y) :: omegaa, (la, wa))
                                     else (omega, (l, w)))
                                 end
                               | SOME t2 =>
                                 let
                                   val (omegaa, (la, waa)) =
                                     find_cheapest_distinguishing_trace
                                       (A1_, A2_) (B1_, B2_, B3_, B4_)
                                       (C1_, C2_, C3_, C4_) m distFun ios
                                       (emptyc
 (ccompare_prod B1_ C1_, mapping_impl_prod B3_ C3_))
                                       q1a t2 q2a;
                                 in
                                   (if less_nat (plus_nat waa w1) w orelse
 equal_nata (plus_nat waa w1) w andalso less_nat (plus_nat la one_nata) l
                                     then ((x, y) :: omegaa,
    (plus_nat la one_nata, plus_nat waa w1))
                                     else (omega, (l, w)))
                                 end)
                           | SOME t1 =>
                             (case lookupa
                                     (ccompare_prod B1_ C1_, equal_prod B2_ C2_)
                                     m2 (x, y)
                               of NONE =>
                                 let
                                   val (omegaa, (la, waa)) =
                                     find_cheapest_distinguishing_trace
                                       (A1_, A2_) (B1_, B2_, B3_, B4_)
                                       (C1_, C2_, C3_, C4_) m distFun ios t1 q1a
                                       (emptyc
 (ccompare_prod B1_ C1_, mapping_impl_prod B3_ C3_))
                                       q2a;
                                 in
                                   (if less_nat (plus_nat waa w2) w orelse
 equal_nata (plus_nat waa w2) w andalso less_nat (plus_nat la one_nata) l
                                     then ((x, y) :: omegaa,
    (plus_nat la one_nata, plus_nat waa w2))
                                     else (omega, (l, w)))
                                 end
                               | SOME t2 =>
                                 let
                                   val (omegaa, (la, waa)) =
                                     find_cheapest_distinguishing_trace
                                       (A1_, A2_) (B1_, B2_, B3_, B4_)
                                       (C1_, C2_, C3_, C4_) m distFun ios t1 q1a
                                       t2 q2a;
                                 in
                                   (if less_nat waa w orelse
 equal_nata waa w andalso less_nat la l
                                     then ((x, y) :: omegaa, (la, waa))
                                     else (omega, (l, w)))
                                 end)))))
        end);
  in
    foldl f (distFun q1 q2, (zero_nat, nat_of_integer (3 : IntInf.int))) ios
  end;

fun add_cheapest_distinguishing_trace (A1_, A2_, A3_) (B1_, B2_, B3_, B4_, B5_)
  (C1_, C2_, C3_, C4_, C5_) distFun completeInputTraces m
  ((alpha, q1), (beta, q2)) t =
  let
    val w =
      fst (find_cheapest_distinguishing_trace (A1_, A2_) (B2_, B3_, B4_, B5_)
            (C2_, C3_, C4_, C5_) m distFun
            (product (inputs_as_list (B1_, B2_, B3_, B5_) m)
              (outputs_as_list (C1_, C2_, C3_, C5_) m))
            (aftera
              (ccompare_prod B2_ C2_, equal_prod B3_ C3_,
                mapping_impl_prod B4_ C4_)
              t alpha)
            q1 (aftera
                 (ccompare_prod B2_ C2_, equal_prod B3_ C3_,
                   mapping_impl_prod B4_ C4_)
                 t beta)
            q2);
    val ta =
      insertb
        (ccompare_prod B2_ C2_, equal_prod B3_ C3_, mapping_impl_prod B4_ C4_)
        (emptyc (ccompare_prod B2_ C2_, mapping_impl_prod B4_ C4_)) w;
  in
    (if completeInputTraces
      then let
             val t1 =
               complete_inputs_to_tree (A1_, A2_, A3_) (B2_, B3_, B4_, B5_)
                 (C2_, C3_, C4_, C5_) m q1
                 (outputs_as_list (C1_, C2_, C3_, C5_) m) (mapa fst w);
             val t2 =
               complete_inputs_to_tree (A1_, A2_, A3_) (B2_, B3_, B4_, B5_)
                 (C2_, C3_, C4_, C5_) m q2
                 (outputs_as_list (C1_, C2_, C3_, C5_) m) (mapa fst w);
           in
             combinea (ccompare_prod B2_ C2_) ta
               (combinea (ccompare_prod B2_ C2_) t1 t2)
           end
      else ta)
  end;

fun h_method_via_pair_framework_3 (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m c1 c2 =
  let
    val tables =
      compute_ofsm_tables (A2_, A3_, A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, A4_) ma) one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product (states_as_list (A3_, A4_, A5_, A7_) ma)
            (states_as_list (A3_, A4_, A5_, A7_) ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, A4_) q1 (states ma) andalso
              (member (A3_, A4_) q2 (states ma) andalso not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val distFun =
      add_cheapest_distinguishing_trace (A4_, A5_, A7_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) distHelper c2;
  in
    pair_framework_h_components_2 (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m distFun c1
  end;

fun apply_method_to_prime m additionalStates isAlreadyPrime f =
  let
    val ma =
      (if isAlreadyPrime then m
        else to_prime
               (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
                 equal_integer, linorder_integer, set_impl_integer,
                 infinite_UNIV_integer)
               (finite_UNIV_integer, cenum_integer, ceq_integer,
                 cproper_interval_integer, equal_integer, mapping_impl_integer,
                 linorder_integer, set_impl_integer)
               (finite_UNIV_integer, cenum_integer, ceq_integer,
                 cproper_interval_integer, equal_integer, mapping_impl_integer,
                 linorder_integer, set_impl_integer)
               m);
    val a =
      plus_nat
        (card (card_UNIV_integer, ceq_integer, ccompare_integer)
          (reachable_states
            (card_UNIV_integer, ceq_integer, ccompare_integer, equal_integer,
              linorder_integer, set_impl_integer)
            (ceq_integer, ccompare_integer, equal_integer, linorder_integer)
            (ceq_integer, ccompare_integer, equal_integer, linorder_integer)
            ma))
        (nat_of_integer additionalStates);
  in
    f ma a
  end;

fun fsm_from_list_integer q ts =
  fsm_from_list
    (card_UNIV_integer, ceq_integer, ccompare_integer, equal_integer,
      mapping_impl_integer, set_impl_integer)
    (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
      set_impl_integer)
    (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
      set_impl_integer)
    q ts;

fun hsi_method_via_spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distFun =
      (fn _ => fn q =>
        (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
          then the (hsiMap q)
          else get_HSI (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma q));
  in
    spy_framework_static_with_empty_graph
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun spyh_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) =
  h_framework_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_)
    (fn _ => fn _ => fn _ => fn _ => fn _ => true);

fun spy_framework_static_with_simple_graph
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m1 dist_fun m =
  spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
    (get_state_cover_assignment
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C7_))
    (handle_state_cover_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) dist_fun)
    (fn _ => fn _ => fn ts => ts)
    (establish_convergence_static
      (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) dist_fun)
    (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
      (ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) false true)
    (simple_cg_initial (ccompare_cproper_interval A4_, A5_)
      (B1_, B2_, B3_, B4_, B5_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C7_, C8_))
    (simple_cg_insert
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        linorder_prod B7_ C7_))
    (simple_cg_lookup_with_conv
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        equal_prod B5_ C5_, linorder_prod B7_ C7_))
    (simple_cg_merge
      (ceq_prod B3_ C3_,
        ccompare_prod (ccompare_cproper_interval B4_)
          (ccompare_cproper_interval C4_),
        equal_prod B5_ C5_))
    m;

fun spy_method_via_spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma m =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) ma
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) ma)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables ma q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states ma) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states ma) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) ma q1 q2));
    val hsiMap =
      map_of A5_
        (mapa (fn q =>
                (q, from_list
                      (ccompare_prod (ccompare_cproper_interval B4_)
                         (ccompare_cproper_interval C4_),
                        equal_prod B5_ C5_, mapping_impl_prod B6_ C6_)
                      (map_filter
                        (fn x =>
                          (if not (eq A5_ q x) then SOME (distHelper q x)
                            else NONE))
                        (states_as_list
                          (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma))))
          (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) ma));
    val distFun =
      (fn _ => fn q =>
        (if member (A3_, ccompare_cproper_interval A4_) q (states ma)
          then the (hsiMap q)
          else get_HSI (A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) ma q));
  in
    spy_framework_static_with_simple_graph
      (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) ma distFun m
  end;

fun hsi_method_via_pair_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) ma m =
  let
    val tables =
      compute_ofsm_tables (A2_, A3_, A4_, A5_, A6_, A7_, A8_) (B1_, B2_, B3_)
        (C1_, C2_, C3_) ma (minus_nat (size (A1_, A3_, A4_) ma) one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
                              (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) tables
                              ma q1 q2)
                        end
              else NONE))
          (product (states_as_list (A3_, A4_, A5_, A7_) ma)
            (states_as_list (A3_, A4_, A5_, A7_) ma)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, A4_) q1 (states ma) andalso
              (member (A3_, A4_) q2 (states ma) andalso not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, A4_, A5_, A8_) (B1_, B2_, B3_, B5_)
                 (C1_, C2_, C3_, C5_) ma q1 q2));
    val a =
      (fn _ => fn (a, b) =>
        let
          val (_, q1) = a;
        in
          (fn (_, q2) => fn _ =>
            insertb
              (ccompare_prod B2_ C2_, equal_prod B3_ C3_,
                mapping_impl_prod B4_ C4_)
              (emptyc (ccompare_prod B2_ C2_, mapping_impl_prod B4_ C4_))
              (distHelper q1 q2))
        end
          b);
  in
    pair_framework_h_components (A1_, A3_, A4_, A5_, A6_, A7_, A8_)
      (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_, C6_) ma m a
  end;

fun test_suite_to_input_sequences (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_) t =
  sorted_list_of_maximal_sequences_in_tree (A1_, A2_, A3_, A4_, A5_, A7_, A8_)
    (from_list (ccompare_cproper_interval A4_, A5_, A6_)
      (mapa (mapa fst)
        (sorted_list_of_maximal_sequences_in_tree
          (card_UNIV_prod A1_ B1_, cenum_prod A2_ B2_, ceq_prod A3_ B3_,
            cproper_interval_prod A4_ B4_, equal_prod A5_ B5_,
            linorder_prod A7_ B6_, set_impl_prod A8_ B7_)
          t)));

fun do_establish_convergence (A1_, A2_, A3_, A4_, A5_, A6_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_) m v t x l =
  not (is_none
        (find (fn ta =>
                distance_at_most (A1_, A2_, A3_, A4_, A5_, A6_)
                  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_) m
                  (snd (snd (snd t))) (fst ta) l)
          x));

fun establish_convergence_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
  (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) completeInputTraces
  useInputHeuristic dist_fun m1 v ta g cg_insert cg_lookup m t =
  distinguish_from_set (A1_, A2_, A3_, A4_, A5_, A6_, A7_)
    (B1_, B2_, B3_, B4_, B5_) (C1_, C2_, C3_, C4_, C5_) m1 v ta g cg_lookup
    cg_insert dist_fun (v (fst t) @ [(fst (snd t), fst (snd (snd t)))])
    (v (snd (snd (snd t))))
    (mapa v
      (reachable_states_as_list (A1_, A3_, A4_, A5_, A6_, A7_)
        (B1_, B2_, B3_, B5_) (C1_, C2_, C3_, C5_) m1))
    (times_nata (nat_of_integer (2 : IntInf.int)) (size (A1_, A3_, A4_) m1))
    (minus_nat m
      (card (A1_, A3_, A4_)
        (reachable_states (A1_, A3_, A4_, A5_, A6_, A7_) (B1_, B2_, B3_, B5_)
          (C1_, C2_, C3_, C5_) m1)))
    completeInputTraces
    (if useInputHeuristic
      then append_heuristic_input A6_ (B2_, B3_, B4_, B5_)
             (C1_, C2_, C3_, C4_, C5_) m1
      else append_heuristic_io (B2_, B3_, B4_) (C2_, C3_, C4_))
    false;

fun spyh_method_via_spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) m1 m completeInputTraces
  useInputHeuristic =
  let
    val tables =
      compute_ofsm_tables
        (A2_, A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_)
        (C3_, ccompare_cproper_interval C4_, C5_) m1
        (minus_nat (size (A1_, A3_, ccompare_cproper_interval A4_) m1)
          one_nata);
    val distMap =
      map_of (equal_prod A5_ A5_)
        (map_filter
          (fn x =>
            (if not (eq A5_ (fst x) (snd x))
              then SOME let
                          val (q1, q2) = x;
                        in
                          ((q1, q2),
                            get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                              (A1_, A2_, A3_, ccompare_cproper_interval A4_,
                                A5_, A7_, A8_)
                              (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                              (C3_, ccompare_cproper_interval C4_, C5_, C7_)
                              tables m1 q1 q2)
                        end
              else NONE))
          (product
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_) m1)
            (states_as_list (A3_, ccompare_cproper_interval A4_, A5_, A7_)
              m1)));
    val distHelper =
      (fn q1 => fn q2 =>
        (if member (A3_, ccompare_cproper_interval A4_) q1 (states m1) andalso
              (member (A3_, ccompare_cproper_interval A4_) q2
                 (states m1) andalso
                not (eq A5_ q1 q2))
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables
                 (A2_, A3_, ccompare_cproper_interval A4_, A5_, A8_)
                 (B3_, ccompare_cproper_interval B4_, B5_, B7_)
                 (C3_, ccompare_cproper_interval C4_, C5_, C7_) m1 q1 q2));
  in
    spy_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
      (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
      (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_, C7_, C8_) m1
      (get_state_cover_assignment
        (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C7_))
      (handle_state_cover_dynamic
        (A1_, A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic distHelper)
      (sort_unverified_transitions_by_state_cover_length
        (A1_, A3_, ccompare_cproper_interval A4_, A5_, A7_) (B5_, B7_)
        (C5_, C7_))
      (establish_convergence_dynamic
        (A1_, A2_, A3_, ccompare_cproper_interval A4_, A5_, A7_, A8_)
        (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic distHelper)
      (handle_io_pair (ccompare_cproper_interval A4_, A5_, A7_)
        (ccompare_cproper_interval B4_, B5_, B6_, B7_)
        (C3_, ccompare_cproper_interval C4_, C5_, C6_, C7_) completeInputTraces
        useInputHeuristic)
      (simple_cg_initial (ccompare_cproper_interval A4_, A5_)
        (B1_, B2_, B3_, B4_, B5_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C7_, C8_))
      (simple_cg_insert
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          linorder_prod B7_ C7_))
      (simple_cg_lookup_with_conv
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_, linorder_prod B7_ C7_))
      (simple_cg_merge
        (ceq_prod B3_ C3_,
          ccompare_prod (ccompare_cproper_interval B4_)
            (ccompare_cproper_interval C4_),
          equal_prod B5_ C5_))
      m
  end;

fun apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime f =
  let
    val ma =
      (if isAlreadyPrime then m
        else to_prime
               (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
                 equal_integer, linorder_integer, set_impl_integer,
                 infinite_UNIV_integer)
               (finite_UNIV_integer, cenum_integer, ceq_integer,
                 cproper_interval_integer, equal_integer, mapping_impl_integer,
                 linorder_integer, set_impl_integer)
               (finite_UNIV_integer, cenum_integer, ceq_integer,
                 cproper_interval_integer, equal_integer, mapping_impl_integer,
                 linorder_integer, set_impl_integer)
               m);
  in
    sorted_list_of_maximal_sequences_in_tree
      (card_UNIV_prod (card_UNIV_prod card_UNIV_integer card_UNIV_integer)
         card_UNIV_bool,
        cenum_prod (cenum_prod cenum_integer cenum_integer) cenum_bool,
        ceq_prod (ceq_prod ceq_integer ceq_integer) ceq_bool,
        cproper_interval_prod
          (cproper_interval_prod cproper_interval_integer
            cproper_interval_integer)
          cproper_interval_bool,
        equal_prod (equal_prod equal_integer equal_integer) equal_bool,
        linorder_prod (linorder_prod linorder_integer linorder_integer)
          linorder_bool,
        set_impl_prod (set_impl_prod set_impl_integer set_impl_integer)
          set_impl_bool)
      (test_suite_from_io_tree (ccompare_integer, equal_integer)
        (ccompare_integer, equal_integer, mapping_impl_integer)
        (ccompare_integer, equal_integer, mapping_impl_integer) ma (initial ma)
        (apply_method_to_prime m additionalStates isAlreadyPrime f))
  end;

fun h_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      h_method_via_h_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun w_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (w_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun wp_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (wp_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun hsi_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (hsi_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun spy_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (spy_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun w_method_via_h_framework_2_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (w_method_via_h_framework_2
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun w_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (w_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime f =
  test_suite_to_input_sequences
    (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
      equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
    (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
      equal_integer, linorder_integer, set_impl_integer)
    (apply_method_to_prime m additionalStates isAlreadyPrime f);

fun h_method_via_h_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      h_method_via_h_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun h_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (h_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
        mapping_impl_integer, linorder_integer, set_impl_integer)
      (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
        mapping_impl_integer, linorder_integer, set_impl_integer));

fun spyh_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      spyh_method_via_h_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun w_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (w_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun w_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (w_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer, set_impl_integer));

fun wp_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (wp_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun hsi_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (hsi_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun spy_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (spy_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun wp_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (wp_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun partial_s_method_via_h_framework (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
  (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_) =
  h_framework_dynamic (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
    (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_)
    (C1_, C2_, C3_, C4_, C5_, C6_, C7_, C8_)
    (do_establish_convergence
      (A3_, ccompare_cproper_interval A4_, A5_, A6_, A7_, A8_)
      (B3_, ccompare_cproper_interval B4_, B5_, B6_, B7_)
      (C3_, ccompare_cproper_interval C4_, C7_, C8_));

fun h_method_via_pair_framework_2_ts m additionalStates isAlreadyPrime c =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      h_method_via_pair_framework_2
        (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
          equal_integer, mapping_impl_integer, linorder_integer,
          set_impl_integer)
        (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
          mapping_impl_integer, linorder_integer, set_impl_integer)
        (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
          mapping_impl_integer, linorder_integer, set_impl_integer)
        ma maa c);

fun h_method_via_pair_framework_3_ts m additionalStates isAlreadyPrime c1 c2 =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      h_method_via_pair_framework_3
        (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
          equal_integer, mapping_impl_integer, linorder_integer,
          set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c1 c2);

fun hsi_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (hsi_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun hsi_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (hsi_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer, set_impl_integer));

fun spy_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (spy_method_via_h_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun spyh_method_via_spy_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      spyh_method_via_spy_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun w_method_via_h_framework_2_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (w_method_via_h_framework_2
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun w_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (w_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun h_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (h_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
        mapping_impl_integer, linorder_integer, set_impl_integer)
      (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
        mapping_impl_integer, linorder_integer, set_impl_integer));

fun spyh_method_via_h_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      spyh_method_via_h_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun w_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (w_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer, set_impl_integer));

fun wp_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (wp_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun hsi_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (hsi_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun spy_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (spy_method_via_spy_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (card_UNIV_integer, cenum_integer, ceq_integer, cproper_interval_integer,
        equal_integer, mapping_impl_integer, linorder_integer,
        set_impl_integer));

fun calculate_test_suite_naive_as_io_sequences_with_assumption_check
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma m =
  (if not (is_empty (B1_, B3_, B4_) (inputs ma))
    then (if observable (A3_, ccompare_cproper_interval A4_, A5_)
               (B3_, ccompare_cproper_interval B4_, B5_)
               (C3_, ccompare_cproper_interval C4_, C5_) ma
           then (if completely_specified
                      (A3_, ccompare_cproper_interval A4_, A5_)
                      (B3_, ccompare_cproper_interval B4_, B5_)
                      (C3_, ccompare_cproper_interval C4_) ma
                  then Inr (test_suite_to_io_maximal
                             (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                             (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_,
                               B6_, B7_, B8_)
                             (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_,
                               C6_, C7_)
                             (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
                               cenum_sum (cenum_prod A2_ A2_) A2_,
                               ceq_sum (ceq_prod A3_ A3_) A3_,
                               cproper_interval_sum
                                 (cproper_interval_prod A4_ A4_) A4_,
                               equal_sum (equal_prod A5_ A5_) A5_,
                               mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_,
                               linorder_sum
                                 (equal_prod A5_ A5_, linorder_prod A7_ A7_)
                                 (A5_, A7_),
                               set_impl_sum (set_impl_prod A8_ A8_) A8_)
                             ma (calculate_test_suite_naive
                                  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                                  (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_,
                                    B5_, B6_, B7_, B8_)
                                  (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

fun generate_reduction_test_suite_naive ma m =
  (case calculate_test_suite_naive_as_io_sequences_with_assumption_check
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            set_impl_integer)
          ma (nat_of_integer m)
    of Inl a => Inl a
    | Inr ts =>
      Inr (sorted_list_of_set
            (ceq_list (ceq_prod ceq_integer ceq_integer),
              ccompare_list (ccompare_prod ccompare_integer ccompare_integer),
              equal_list (equal_prod equal_integer equal_integer),
              linorder_list
                (equal_prod equal_integer equal_integer,
                  linorder_prod linorder_integer linorder_integer))
            ts));

fun h_method_via_pair_framework_2_input m additionalStates isAlreadyPrime c =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      h_method_via_pair_framework_2
        (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
          equal_integer, mapping_impl_integer, linorder_integer,
          set_impl_integer)
        (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
          mapping_impl_integer, linorder_integer, set_impl_integer)
        (cenum_integer, ceq_integer, ccompare_integer, equal_integer,
          mapping_impl_integer, linorder_integer, set_impl_integer)
        ma maa c);

fun h_method_via_pair_framework_3_input m additionalStates isAlreadyPrime c1 c2
  = apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
      (fn ma => fn maa =>
        h_method_via_pair_framework_3
          (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
            equal_integer, mapping_impl_integer, linorder_integer,
            set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          ma maa c1 c2);

fun hsi_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (hsi_method_via_pair_framework
      (card_UNIV_integer, cenum_integer, ceq_integer, ccompare_integer,
        equal_integer, mapping_impl_integer, linorder_integer, set_impl_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer)
      (ceq_integer, ccompare_integer, equal_integer, mapping_impl_integer,
        linorder_integer, set_impl_integer));

fun partial_s_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      partial_s_method_via_h_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun spyh_method_via_spy_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (fn ma => fn maa =>
      spyh_method_via_spy_framework
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        (card_UNIV_integer, cenum_integer, ceq_integer,
          cproper_interval_integer, equal_integer, mapping_impl_integer,
          linorder_integer, set_impl_integer)
        ma maa c b);

fun calculate_test_suite_greedy_as_io_sequences_with_assumption_check
  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
  (B1_, B2_, B3_, B4_, B5_, B6_, B7_, B8_) (C1_, C2_, C3_, C4_, C5_, C6_, C7_)
  ma m =
  (if not (is_empty (B1_, B3_, B4_) (inputs ma))
    then (if observable (A3_, ccompare_cproper_interval A4_, A5_)
               (B3_, ccompare_cproper_interval B4_, B5_)
               (C3_, ccompare_cproper_interval C4_, C5_) ma
           then (if completely_specified
                      (A3_, ccompare_cproper_interval A4_, A5_)
                      (B3_, ccompare_cproper_interval B4_, B5_)
                      (C3_, ccompare_cproper_interval C4_) ma
                  then Inr (test_suite_to_io_maximal
                             (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                             (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_, B5_,
                               B6_, B7_, B8_)
                             (finite_UNIV_card_UNIV C1_, C2_, C3_, C4_, C5_,
                               C6_, C7_)
                             (card_UNIV_sum (card_UNIV_prod A1_ A1_) A1_,
                               cenum_sum (cenum_prod A2_ A2_) A2_,
                               ceq_sum (ceq_prod A3_ A3_) A3_,
                               cproper_interval_sum
                                 (cproper_interval_prod A4_ A4_) A4_,
                               equal_sum (equal_prod A5_ A5_) A5_,
                               mapping_impl_sum (mapping_impl_prod A6_ A6_) A6_,
                               linorder_sum
                                 (equal_prod A5_ A5_, linorder_prod A7_ A7_)
                                 (A5_, A7_),
                               set_impl_sum (set_impl_prod A8_ A8_) A8_)
                             ma (calculate_test_suite_greedy
                                  (A1_, A2_, A3_, A4_, A5_, A6_, A7_, A8_)
                                  (finite_UNIV_card_UNIV B1_, B2_, B3_, B4_,
                                    B5_, B6_, B7_, B8_)
                                  (C1_, C2_, C3_, C4_, C5_, C6_, C7_) ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

fun generate_reduction_test_suite_greedy ma m =
  (case calculate_test_suite_greedy_as_io_sequences_with_assumption_check
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            set_impl_integer)
          ma (nat_of_integer m)
    of Inl a => Inl a
    | Inr ts =>
      Inr (sorted_list_of_set
            (ceq_list (ceq_prod ceq_integer ceq_integer),
              ccompare_list (ccompare_prod ccompare_integer ccompare_integer),
              equal_list (equal_prod equal_integer equal_integer),
              linorder_list
                (equal_prod equal_integer equal_integer,
                  linorder_prod linorder_integer linorder_integer))
            ts));

fun partial_s_method_via_h_framework_input m additionalStates isAlreadyPrime c b
  = apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
      (fn ma => fn maa =>
        partial_s_method_via_h_framework
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          (card_UNIV_integer, cenum_integer, ceq_integer,
            cproper_interval_integer, equal_integer, mapping_impl_integer,
            linorder_integer, set_impl_integer)
          ma maa c b);

end; (*struct GeneratedCode*)
