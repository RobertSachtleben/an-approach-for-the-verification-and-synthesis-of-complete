module Main where

import Options.Applicative
import System.Environment
import System.Exit
import GeneratedCode
import FsmReader
import TestsuiteGenerator

import System.CPUTime
import System.FilePath
import System.IO
import System.Directory
import Control.DeepSeq
import Control.Monad
import System.Timeout

import Text.PrettyPrint.ANSI.Leijen(string)


--------------------------------------------------------------------------------
-- generating test suites for .fsm files
--------------------------------------------------------------------------------



data Opts = Opts { path                    :: String 
                 , additionalStates        :: Integer
                 , strategy                :: TestStrategy
                 , useInputRepresentation  :: Bool 
                 , isPrime                 :: Bool 
                 , testEntireDirectory     :: Bool
                 , doNotStoreTestSuites    :: Bool
                 , collectTimingsAndCounts :: Bool
                 , timeoutInMilliseconds   :: Int }

getOpts :: Parser Opts
getOpts = Opts 
            <$> strOption
                ( short 'p'
                    <> long "path"
                    <> metavar "PATH"
                    <> help "path to .fsm file or directory" )
            <*> option auto 
                ( short 'a'
                    <> long "additional_states"
                    <> metavar "N"
                    <> help "upper bound on the number of states that the SUT may have in addition to the number of states in the specification" 
                    <> showDefault
                    <> value (0::Integer) )         
            <*> (flag' (EquivalenceStrategy W_H) (long "W_H" <> help "W method derived from the H framework") 
                    <|> flag' (EquivalenceStrategy W_H_2) (long "W_H_2" <> help "W method derived from the H framework (variant #2)") 
                    <|> flag' (EquivalenceStrategy W_SPY) (long "W_SPY" <> help "W method derived from the SPY framework") 
                    <|> flag' (EquivalenceStrategy W_Pair) (long "W_Pair" <> help "W method derived from the Pair framework") 
                    <|> flag' (EquivalenceStrategy Wp_H) (long "Wp_H" <> help "Wp method derived from the H framework") 
                    <|> flag' (EquivalenceStrategy Wp_SPY) (long "Wp_SPY" <> help "Wp method derived from the SPY framework") 
                    <|> flag' (EquivalenceStrategy HSI_H) (long "HSI_H" <> help "HSI method derived from the H framework") 
                    <|> flag' (EquivalenceStrategy HSI_SPY) (long "HSI_SPY" <> help "HSI method derived from the SPY framework") 
                    <|> flag' (EquivalenceStrategy HSI_Pair) (long "HSI_Pair" <> help "HSI method derived from the Pair framework") 
                    <|> flag' (EquivalenceStrategy H_Pair) (long "H_Pair" <> help "H method derived from the Pair framework") 
                    <|> flag' (EquivalenceStrategy (H_Pair_2 False)) (long "H_Pair_2_0" <> help "H method derived from the Pair framework (variant #2) without input completion") 
                    <|> flag' (EquivalenceStrategy (H_Pair_2 True)) (long "H_Pair_2_1" <> help "H method derived from the Pair framework (variant #2) with input completion") 
                    <|> flag' (EquivalenceStrategy (H_Pair_3 False False)) (long "H_Pair_3_00" <> help "H method derived from the Pair framework (variant #3) without input completions") 
                    <|> flag' (EquivalenceStrategy (H_Pair_3 False True)) (long "H_Pair_3_01" <> help "H method derived from the Pair framework (variant #3) without initial completion and with later input completions")
                    <|> flag' (EquivalenceStrategy (H_Pair_3 True False)) (long "H_Pair_3_10" <> help "H method derived from the Pair framework (variant #3) with initial completion and without later input completions")
                    <|> flag' (EquivalenceStrategy (H_Pair_3 True True)) (long "H_Pair_3_11" <> help "H method derived from the Pair framework (variant #3) witht initial completion and with later input completions")
                    <|> flag' (EquivalenceStrategy (H_H False False)) (long "H_H_00" <> help "H method derived from the H framework without input completion, using the io heuristic for appending") 
                    <|> flag' (EquivalenceStrategy (H_H False True)) (long "H_H_01" <> help "H method derived from the H framework without input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (H_H True False)) (long "H_H_10" <> help "H method derived from the H framework with input completion, using the io heuristic for appending")
                    <|> flag' (EquivalenceStrategy (H_H True True)) (long "H_H_11" <> help "H method derived from the H framework with input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy SPY_H) (long "SPY_H" <> help "SPY method derived from the H framework") 
                    <|> flag' (EquivalenceStrategy SPY_SPY) (long "SPY_SPY" <> help "SPY method derived from the SPY framework") 
                    <|> flag' (EquivalenceStrategy (SPYH_H False False)) (long "SPYH_H_00" <> help "SPYH method derived from the H framework without input completion, using the io heuristic for appending") 
                    <|> flag' (EquivalenceStrategy (SPYH_H False True)) (long "SPYH_H_01" <> help "SPYH method derived from the H framework without input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (SPYH_H True False)) (long "SPYH_H_10" <> help "SPYH method derived from the H framework with input completion, using the io heuristic for appending")
                    <|> flag' (EquivalenceStrategy (SPYH_H True True)) (long "SPYH_H_11" <> help "SPYH method derived from the H framework with input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (SPYH_SPY False False)) (long "SPYH_SPY_00" <> help "SPYH method derived from the SPY framework without input completion, using the io heuristic for appending") 
                    <|> flag' (EquivalenceStrategy (SPYH_SPY False True)) (long "SPYH_SPY_01" <> help "SPYH method derived from the SPY framework without input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (SPYH_SPY True False)) (long "SPYH_SPY_10" <> help "SPYH method derived from the SPY framework with input completion, using the io heuristic for appending")
                    <|> flag' (EquivalenceStrategy (SPYH_SPY True True)) (long "SPYH_SPY_11" <> help "SPYH method derived from the SPY framework with input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (Partial_S_H False False)) (long "Partial_S_H_00" <> help "Partial S method derived from the H framework without input completion, using the io heuristic for appending") 
                    <|> flag' (EquivalenceStrategy (Partial_S_H False True)) (long "Partial_S_H_01" <> help "Partial S method derived from the H framework without input completion, using the input heuristic for appending")
                    <|> flag' (EquivalenceStrategy (Partial_S_H True False)) (long "Partial_S_H_10" <> help "Partial S method derived from the H framework with input completion, using the io heuristic for appending")
                    <|> flag' (EquivalenceStrategy (Partial_S_H True True)) (long "Partial_S_H_11" <> help "Partial S method derived from the H framework with input completion, using the input heuristic for appending")
                    <|> flag' (ReductionStrategy ASC_Naive) (long "ASC_Naive" <> help "Adaptive state counting method with naive computation of all maximal sets of pairwise r-distinguishable states") 
                    <|> flag' (ReductionStrategy ASC_Greedy) (long "ASC_Greedy" <> help "Adaptive state counting method with greedy computation of some maximal sets of pairwise r-distinguishable states") 
                )
            <*> switch
                ( short 'i'
                    <> long "use_input_representation"
                    <> help "whether to represent test suites as lists of input sequences (affects only strategies for equivalence testing)" )   
            <*> switch
                ( short 'p'
                    <> long "is_prime"
                    <> help "whether input fsms are already prime machines - if not set, then they are transformed into language-equivalent prime FSMs first (applies only to equivalence testing)" )  
            <*> switch
                ( short 'd'
                    <> long "test_entire_directory"
                    <> help "whether to test a single given .fsm file or all .fsm files in a given directory" )        
            <*> switch
                ( short 'n'
                    <> long "do_not_store_test_suites"
                    <> help "whether to store the generated test suites to a file" )        
            <*> switch
                ( short 'c'
                    <> long "collect_timings_and_counts"
                    <> help "if set, a .csv file is generated that collects the number of tests in the test suite generated for each entered .fsm file and the time it took to calculate these" )
            <*> option auto 
                ( short 't'
                    <> long "timeout"
                    <> metavar "T"
                    <> help "timeout for each individual test suite generation in milliseconds; can be set to a negative value to disable" 
                    <> showDefault
                    <> value ((15*60*1000)::Int) ) -- default: 15 minutes            

main :: IO ()
main = performGeneration =<< execParser opts
  where
    opts = info (getOpts <**> helper)
      ( fullDesc
        <> progDescDoc ( Just . string $  "Generates a test suite for a .fsm file.\n"     
                    ++ "If the -d flag is not set, then the path passed by -p must be a .fsm file and the test suite generated is a single file of the same name with extension \".test_suite\".\n"
                    ++ "If the -d flag is set, then the path passed by -p must be a directory and for each .fsm file in this directory a .test_suite file will be generated.\n"
                    ++ "If the -c flag is set, then a .csv file containing details on the size and execution time for each tested .fsm file is stored as \"timings_and_counts.log\"."
                    ++ "If the -d flag is set additionally, then \"timings_and_counts.log\" is stored in the directory given by the path, otherwise it is stored in the current directory.\n"
                    ++ "If the -n flag is set, then test suites are still calculated but not stored.\n"
                    ++ "Any individual test suite calculation is aborted if its calculation is not finished within the time set by -t. If -t specifies a negative value, then no timeout is used.")
        <> header "Test Suite Generator - a generator for m-complete test suite for finite state machines" )



showTestSequence :: [(Integer,Integer)] -> String
showTestSequence [] = ""
showTestSequence ((x,y):[]) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")"
showTestSequence ((x,y):(x',y'):xys) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")" ++ "." ++ (showTestSequence $ (x',y'):xys)


testFSM' :: TestStrategy -> Bool -> Bool -> Integer -> Bool -> FilePath -> IO (Either String (Integer, Integer))
testFSM' strat useInputRepresentation isAlreadyPrime additionalStates doNotStoreTestSuites fsmFilePath = do 
    fsmFile <- readFile fsmFilePath
    case readFSM fsmFile of
        Left err  -> return $ Left err
        Right fsm -> do 
            s1 <- getCPUTime
            let res = generateTestsuite strat useInputRepresentation isAlreadyPrime fsm additionalStates
            case res of 
                (Inr ts) -> do
                    -- force evaluation of ts here
                    s2 <- testsuiteSize ts `deepseq` getCPUTime
                    when (not doNotStoreTestSuites) $ do 
                        let testSuiteFilePath = replaceExtension fsmFilePath ".test_suite"
                        writeTestsuite testSuiteFilePath ts
                    return $ Right ((s2 - s1) `div` 1000000000, toInteger $ testsuiteLength ts) -- convert from ps to ms
                (Inl err) -> do
                    putStrLn $ "Error handling " ++ fsmFilePath ++ ": " ++ err
                    -- do not write into any file
                    return $ Right (-1, -1) 

testFSM :: Opts -> FilePath -> IO ()
testFSM opts fsmFilePath = do
    let tout = (1000 * timeoutInMilliseconds opts)
    result <- timeout tout $ testFSM' (strategy opts) (useInputRepresentation opts) (isPrime opts) (additionalStates opts) (doNotStoreTestSuites opts) fsmFilePath
    let result' = case result of 
                    Just x -> x
                    Nothing -> Left "Timeout"
    when (collectTimingsAndCounts opts) $ do
        appendFile (timingFilePath opts) $ fsmFilePath ++ "," ++ (case result' of
            Left err    -> "Error: " ++ err
            Right (t,n) -> (show $ additionalStates opts) ++ "," ++ (show t) ++ "," ++ (show n)) ++ "\n"
    return ()

timingFilePath :: Opts -> String
timingFilePath opts = "timings_and_counts__" ++ (filter (/=' ') . show $ strategy opts) ++ "_a" ++ (show $ additionalStates opts) ++ ".log"

timingFileHeader :: String
timingFileHeader = "FSM file,additional states,time required for calculation,size of testsuite\n"

performGeneration :: Opts -> IO ()
performGeneration opts = do
    putStrLn (show $ strategy opts)
    if testEntireDirectory opts 
    then do
        isDir <- doesDirectoryExist (path opts) 
        if isDir 
            then do
                setCurrentDirectory (path opts)                 
                files <- listDirectory "." 
                let fsms = reverse $ filter (\file -> takeExtension file == ".fsm") files
                when (collectTimingsAndCounts opts) $ do
                    writeFile (timingFilePath opts) timingFileHeader
                mapM_ (testFSM opts) fsms                
                return ()

            else error $ "Error: Given path " ++ (path opts)  ++ " is not a directory but -d flag is set."
    else do
        isFile <- doesFileExist (path opts) 
        if isFile && takeExtension (path opts) == ".fsm"
            then do 
                testFSM opts (path opts)
                return ()                

            else error $ "Error: Given path " ++ (path opts)  ++ " is not a .fsm file but -d flag is not set."


