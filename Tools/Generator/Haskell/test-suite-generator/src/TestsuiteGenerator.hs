module TestsuiteGenerator where

import GeneratedCode
import System.IO

data EquivalenceStrategy =
    W_H | W_H_2 | W_SPY | W_Pair | 
    Wp_H | Wp_SPY | 
    HSI_H | HSI_SPY | HSI_Pair | 
    H_H Bool Bool | H_Pair | H_Pair_2 Bool | H_Pair_3 Bool Bool | 
    SPY_H | SPY_SPY | 
    SPYH_H Bool Bool | SPYH_SPY Bool Bool | 
    Partial_S_H Bool Bool 
        deriving Show

data ReductionStrategy =
    ASC_Naive | ASC_Greedy deriving Show

data TestStrategy =
    EquivalenceStrategy EquivalenceStrategy | ReductionStrategy ReductionStrategy deriving Show

data Testsuite = InputTestsuite [[Integer]] 
               | IOTestCaseTestsuite [[((Integer, Integer), Bool)]]
               | IOTestsuite [[(Integer, Integer)]] deriving Show

instance Functor (Sum a) where
    fmap f (Inr x) = Inr (f x)
    fmap f (Inl x) = Inl x

generateEquivalenceTestsuiteAsInputs :: EquivalenceStrategy -> Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]]
generateEquivalenceTestsuiteAsInputs W_H = w_method_via_h_framework_input
generateEquivalenceTestsuiteAsInputs W_H_2 = w_method_via_h_framework_2_input
generateEquivalenceTestsuiteAsInputs W_SPY = w_method_via_spy_framework_input
generateEquivalenceTestsuiteAsInputs W_Pair = w_method_via_pair_framework_input
generateEquivalenceTestsuiteAsInputs Wp_H = wp_method_via_h_framework_input
generateEquivalenceTestsuiteAsInputs Wp_SPY = wp_method_via_spy_framework_input
generateEquivalenceTestsuiteAsInputs HSI_H = hsi_method_via_h_framework_input
generateEquivalenceTestsuiteAsInputs HSI_SPY = hsi_method_via_spy_framework_input
generateEquivalenceTestsuiteAsInputs HSI_Pair = hsi_method_via_pair_framework_input
generateEquivalenceTestsuiteAsInputs (H_H x y) = (\spec m -> h_method_via_h_framework_input spec m x y)
generateEquivalenceTestsuiteAsInputs H_Pair = h_method_via_pair_framework_input
generateEquivalenceTestsuiteAsInputs (H_Pair_2 x) = (\spec m -> h_method_via_pair_framework_2_input spec m x)
generateEquivalenceTestsuiteAsInputs (H_Pair_3 x y) = (\spec m -> h_method_via_pair_framework_3_input spec m x y)
generateEquivalenceTestsuiteAsInputs SPY_H = spy_method_via_h_framework_input
generateEquivalenceTestsuiteAsInputs SPY_SPY = spy_method_via_spy_framework_input
generateEquivalenceTestsuiteAsInputs (SPYH_H x y) = (\spec m -> spyh_method_via_h_framework_input spec m x y)
generateEquivalenceTestsuiteAsInputs (SPYH_SPY x y) = (\spec m -> spyh_method_via_spy_framework_input spec m x y)
generateEquivalenceTestsuiteAsInputs (Partial_S_H x y) = (\spec m -> partial_s_method_via_h_framework_input spec m x y)

generateEquivalenceTestsuiteAsIOTestCases :: EquivalenceStrategy -> Fsm Integer Integer Integer -> Integer -> Bool -> [[((Integer, Integer), Bool)]]
generateEquivalenceTestsuiteAsIOTestCases W_H = w_method_via_h_framework_ts
generateEquivalenceTestsuiteAsIOTestCases W_H_2 = w_method_via_h_framework_2_ts
generateEquivalenceTestsuiteAsIOTestCases W_SPY = w_method_via_spy_framework_ts
generateEquivalenceTestsuiteAsIOTestCases W_Pair = w_method_via_pair_framework_ts
generateEquivalenceTestsuiteAsIOTestCases Wp_H = wp_method_via_h_framework_ts
generateEquivalenceTestsuiteAsIOTestCases Wp_SPY = wp_method_via_spy_framework_ts
generateEquivalenceTestsuiteAsIOTestCases HSI_H = hsi_method_via_h_framework_ts
generateEquivalenceTestsuiteAsIOTestCases HSI_SPY = hsi_method_via_spy_framework_ts
generateEquivalenceTestsuiteAsIOTestCases HSI_Pair = hsi_method_via_pair_framework_ts
generateEquivalenceTestsuiteAsIOTestCases (H_H x y) = (\spec m -> h_method_via_h_framework_ts spec m x y)
generateEquivalenceTestsuiteAsIOTestCases H_Pair = h_method_via_pair_framework_ts
generateEquivalenceTestsuiteAsIOTestCases (H_Pair_2 x) = (\spec m -> h_method_via_pair_framework_2_ts spec m x)
generateEquivalenceTestsuiteAsIOTestCases (H_Pair_3 x y) = (\spec m -> h_method_via_pair_framework_3_ts spec m x y)
generateEquivalenceTestsuiteAsIOTestCases SPY_H = spy_method_via_h_framework_ts
generateEquivalenceTestsuiteAsIOTestCases SPY_SPY = spy_method_via_spy_framework_ts
generateEquivalenceTestsuiteAsIOTestCases (SPYH_H x y) = (\spec m -> spyh_method_via_h_framework_ts spec m x y)
generateEquivalenceTestsuiteAsIOTestCases (SPYH_SPY x y) = (\spec m -> spyh_method_via_spy_framework_ts spec m x y)
generateEquivalenceTestsuiteAsIOTestCases (Partial_S_H x y) = (\spec m -> partial_s_method_via_h_framework_ts spec m x y)

generateReductionTestsuiteAsIOTraces :: ReductionStrategy -> Fsm Integer Integer Integer -> Integer -> Sum String [[(Integer, Integer)]]
generateReductionTestsuiteAsIOTraces ASC_Greedy spec additionalStates = generate_reduction_test_suite_greedy spec ((additionalStates +) . integer_of_nat . size $ spec)
generateReductionTestsuiteAsIOTraces ASC_Naive spec additionalStates = generate_reduction_test_suite_naive spec ((additionalStates +) . integer_of_nat . size $ spec)

generateReductionTestsuiteAsInputs :: ReductionStrategy -> Fsm Integer Integer Integer -> Integer -> Sum String [[Integer]]
generateReductionTestsuiteAsInputs = ((.) . (.) . (.)) (fmap (map (map fst))) generateReductionTestsuiteAsIOTraces

generateTestsuite :: TestStrategy -> Bool -> Bool -> Fsm Integer Integer Integer -> Integer -> Sum String Testsuite
generateTestsuite (EquivalenceStrategy strat) True isAlreadyPrime fsm m = Inr $ InputTestsuite $ (generateEquivalenceTestsuiteAsInputs strat) fsm m isAlreadyPrime
generateTestsuite (EquivalenceStrategy strat) False isAlreadyPrime fsm m = Inr $ IOTestCaseTestsuite $ (generateEquivalenceTestsuiteAsIOTestCases strat) fsm m isAlreadyPrime
generateTestsuite (ReductionStrategy strat) True _ fsm m = fmap InputTestsuite $ (generateReductionTestsuiteAsInputs strat) fsm m
generateTestsuite (ReductionStrategy strat) False _ fsm m = fmap IOTestsuite $ (generateReductionTestsuiteAsIOTraces strat) fsm m

showIOTrace :: [(Integer,Integer)] -> String
showIOTrace [] = ""
showIOTrace ((x,y):[]) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")"
showIOTrace ((x,y):(x',y'):xys) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")" ++ "." ++ (showIOTrace $ (x',y'):xys)

showIOTestCase :: [((Integer, Integer), Bool)] -> String
showIOTestCase [] = ""
showIOTestCase (((x,y),b):[]) = "((" ++ (show x) ++ "/" ++ (show y) ++ ")," ++ (if b then "T)" else "F)")
showIOTestCase (((x,y),b):((x',y'),b'):xys) = "((" ++ (show x) ++ "/" ++ (show y) ++ ")," ++ (if b then "T)" else "F)") ++ "." ++ (showIOTestCase $ ((x',y'),b'):xys)

showInputSequence :: [Integer] -> String
showInputSequence [] = ""
showInputSequence (x:[]) = show x
showInputSequence (x:y:xs) = show x ++ "," ++ showInputSequence (y:xs)

writeTestsuite :: String -> Testsuite -> IO ()
writeTestsuite filePath (InputTestsuite ts) = withFile filePath WriteMode $ \file -> do 
                                                mapM_ ((hPutStrLn file) . showInputSequence) ts
writeTestsuite filePath (IOTestCaseTestsuite ts) = withFile filePath WriteMode $ \file -> do 
                                                mapM_ ((hPutStrLn file) . showIOTestCase) ts
writeTestsuite filePath (IOTestsuite ts) = withFile filePath WriteMode $ \file -> do 
                                                mapM_ ((hPutStrLn file) . showIOTrace) ts

testsuiteLength :: Testsuite -> Int
testsuiteLength (InputTestsuite ts) = length ts
testsuiteLength (IOTestCaseTestsuite ts) = length ts
testsuiteLength (IOTestsuite ts) = length ts

testsuiteSize :: Testsuite -> Int
testsuiteSize (InputTestsuite ts) = sum $ map length ts
testsuiteSize (IOTestCaseTestsuite ts) = sum $ map length ts
testsuiteSize (IOTestsuite ts) = sum $ map length ts