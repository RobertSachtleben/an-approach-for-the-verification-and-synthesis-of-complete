{-# LANGUAGE EmptyDataDecls, RankNTypes, ScopedTypeVariables #-}

module
  GeneratedCode(Ccompare, Ceq, Set, Cenum, Fsm_with_precomputations_impl,
                 Fsm_with_precomputations, Fsm_impl, Fsm, Set_impl, Nat,
                 integer_of_nat, Linorder, Finite_UNIV, Card_UNIV, Mapping_impl,
                 Cproper_interval, Fset, Infinite_UNIV, Sum(..), Prefix_tree,
                 size, rename_statesb, index_states, fsm_from_list,
                 make_observable, restrict_to_reachable_states, to_prime,
                 fsm_from_list_integer, h_method_via_h_framework_ts,
                 w_method_via_h_framework_ts, wp_method_via_h_framework_ts,
                 hsi_method_via_h_framework_ts, spy_method_via_h_framework_ts,
                 w_method_via_h_framework_2_ts, w_method_via_spy_framework_ts,
                 h_method_via_h_framework_input, h_method_via_pair_framework_ts,
                 spyh_method_via_h_framework_ts, w_method_via_h_framework_input,
                 w_method_via_pair_framework_ts, wp_method_via_spy_framework_ts,
                 hsi_method_via_spy_framework_ts,
                 spy_method_via_spy_framework_ts,
                 wp_method_via_h_framework_input,
                 h_method_via_pair_framework_2_ts,
                 h_method_via_pair_framework_3_ts,
                 hsi_method_via_h_framework_input,
                 hsi_method_via_pair_framework_ts,
                 spy_method_via_h_framework_input,
                 spyh_method_via_spy_framework_ts,
                 w_method_via_h_framework_2_input,
                 w_method_via_spy_framework_input,
                 h_method_via_pair_framework_input,
                 spyh_method_via_h_framework_input,
                 w_method_via_pair_framework_input,
                 wp_method_via_spy_framework_input,
                 hsi_method_via_spy_framework_input,
                 spy_method_via_spy_framework_input,
                 generate_reduction_test_suite_naive,
                 h_method_via_pair_framework_2_input,
                 h_method_via_pair_framework_3_input,
                 hsi_method_via_pair_framework_input,
                 partial_s_method_via_h_framework_ts,
                 spyh_method_via_spy_framework_input,
                 generate_reduction_test_suite_greedy,
                 partial_s_method_via_h_framework_input)
  where {

import Prelude ((==), (/=), (<), (<=), (>=), (>), (+), (-), (*), (/), (**),
  (>>=), (>>), (=<<), (&&), (||), (^), (^^), (.), ($), ($!), (++), (!!), Eq,
  error, id, return, not, fst, snd, map, filter, concat, concatMap, reverse,
  zip, null, takeWhile, dropWhile, all, any, Integer, negate, abs, divMod,
  String, Bool(True, False), Maybe(Nothing, Just));
import qualified Prelude;

data Ordera = Eqa | Lt | Gt;

class Ccompare a where {
  ccompare :: Maybe (a -> a -> Ordera);
};

equal_order :: Ordera -> Ordera -> Bool;
equal_order Lt Gt = False;
equal_order Gt Lt = False;
equal_order Eqa Gt = False;
equal_order Gt Eqa = False;
equal_order Eqa Lt = False;
equal_order Lt Eqa = False;
equal_order Gt Gt = True;
equal_order Lt Lt = True;
equal_order Eqa Eqa = True;

newtype Generator a b = Generator (b -> Bool, b -> (a, b));

generator :: forall a b. Generator a b -> (b -> Bool, b -> (a, b));
generator (Generator x) = x;

has_next :: forall a b. Generator a b -> b -> Bool;
has_next g = fst (generator g);

next :: forall a b. Generator a b -> b -> (a, b);
next g = snd (generator g);

sorted_list_subset_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
sorted_list_subset_fusion less eq g1 g2 s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             has_next g2 s2 &&
               (case next g2 s2 of {
                 (y, s2a) ->
                   (if eq x y
                     then sorted_list_subset_fusion less eq g1 g2 s1a s2a
                     else less y x &&
                            sorted_list_subset_fusion less eq g1 g2 s1 s2a);
               });
         })
    else True);

list_all_fusion :: forall a b. Generator a b -> (a -> Bool) -> b -> Bool;
list_all_fusion g p s =
  (if has_next g s then (case next g s of {
                          (x, sa) -> p x && list_all_fusion g p sa;
                        })
    else True);

class Ceq a where {
  ceq :: Maybe (a -> a -> Bool);
};

data Color = R | B;

data Rbt a b = Empty | Branch Color (Rbt a b) a b (Rbt a b);

rbt_keys_next ::
  forall a b. ([(a, Rbt a b)], Rbt a b) -> (a, ([(a, Rbt a b)], Rbt a b));
rbt_keys_next ((k, t) : kts, Empty) = (k, (kts, t));
rbt_keys_next (kts, Branch c l k v r) = rbt_keys_next ((k, r) : kts, l);

rbt_has_next :: forall a b c. ([(a, Rbt b c)], Rbt b c) -> Bool;
rbt_has_next ([], Empty) = False;
rbt_has_next (vb : vc, va) = True;
rbt_has_next (v, Branch vb vc vd ve vf) = True;

rbt_keys_generator :: forall a b. Generator a ([(a, Rbt a b)], Rbt a b);
rbt_keys_generator = Generator (rbt_has_next, rbt_keys_next);

lt_of_comp :: forall a. (a -> a -> Ordera) -> a -> a -> Bool;
lt_of_comp acomp x y = (case acomp x y of {
                         Eqa -> False;
                         Lt -> True;
                         Gt -> False;
                       });

newtype Mapping_rbt b a = Mapping_RBTa (Rbt b a);

newtype Set_dlist a = Abs_dlist [a];

data Set a = Collect_set (a -> Bool) | DList_set (Set_dlist a)
  | RBT_set (Mapping_rbt a ()) | Set_Monad [a] | Complement (Set a);

list_of_dlist :: forall a. (Ceq a) => Set_dlist a -> [a];
list_of_dlist (Abs_dlist x) = x;

dlist_all :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Bool;
dlist_all x xc = all x (list_of_dlist xc);

impl_ofa :: forall b a. (Ccompare b) => Mapping_rbt b a -> Rbt b a;
impl_ofa (Mapping_RBTa x) = x;

rbt_init :: forall a b c. Rbt a b -> ([(c, Rbt a b)], Rbt a b);
rbt_init = (\ a -> ([], a));

init ::
  forall a b c. (Ccompare a) => Mapping_rbt a b -> ([(c, Rbt a b)], Rbt a b);
init xa = rbt_init (impl_ofa xa);

class Cenum a where {
  cEnum :: Maybe ([a], ((a -> Bool) -> Bool, (a -> Bool) -> Bool));
};

collect :: forall a. (Cenum a) => (a -> Bool) -> Set a;
collect p = (case cEnum of {
              Nothing -> Collect_set p;
              Just (enum, _) -> Set_Monad (filter p enum);
            });

list_member :: forall a. (a -> a -> Bool) -> [a] -> a -> Bool;
list_member equal (x : xs) y = equal x y || list_member equal xs y;
list_member equal [] y = False;

the :: forall a. Maybe a -> a;
the (Just x2) = x2;

memberc :: forall a. (Ceq a) => Set_dlist a -> a -> Bool;
memberc xa = list_member (the ceq) (list_of_dlist xa);

rbt_comp_lookup :: forall a b. (a -> a -> Ordera) -> Rbt a b -> a -> Maybe b;
rbt_comp_lookup c Empty k = Nothing;
rbt_comp_lookup c (Branch uu l x y r) k = (case c k x of {
    Eqa -> Just y;
    Lt -> rbt_comp_lookup c l k;
    Gt -> rbt_comp_lookup c r k;
  });

lookupb :: forall a b. (Ccompare a) => Mapping_rbt a b -> a -> Maybe b;
lookupb xa = rbt_comp_lookup (the ccompare) (impl_ofa xa);

memberb :: forall a. (Ccompare a) => Mapping_rbt a () -> a -> Bool;
memberb t x = lookupb t x == Just ();

member :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Bool;
member x (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "member Set_Monad: ceq = None" (\ _ -> member x (Set_Monad xs));
    Just eq -> list_member eq xs x;
  });
member xa (Complement x) = not (member xa x);
member x (RBT_set rbt) = memberb rbt x;
member x (DList_set dxs) = memberc dxs x;
member x (Collect_set a) = a x;

less_eq_set :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
less_eq_set (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset RBT_set RBT_set: ccompare = None"
        (\ _ -> less_eq_set (RBT_set rbt1) (RBT_set rbt2));
    Just c ->
      (case ceq of {
        Nothing ->
          sorted_list_subset_fusion (lt_of_comp c)
            (\ x y -> equal_order (c x y) Eqa) rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
        Just eq ->
          sorted_list_subset_fusion (lt_of_comp c) eq rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
      });
  });
less_eq_set (Complement a1) (Complement a2) = less_eq_set a2 a1;
less_eq_set (Collect_set p) (Complement a) =
  less_eq_set a (collect (\ x -> not (p x)));
less_eq_set (Set_Monad xs) c = all (\ x -> member x c) xs;
less_eq_set (DList_set dxs) c =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset DList_set1: ceq = None" (\ _ -> less_eq_set (DList_set dxs) c);
    Just _ -> dlist_all (\ x -> member x c) dxs;
  });
less_eq_set (RBT_set rbt) b =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset RBT_set1: ccompare = None" (\ _ -> less_eq_set (RBT_set rbt) b);
    Just _ -> list_all_fusion rbt_keys_generator (\ x -> member x b) (init rbt);
  });

list_all2_fusion ::
  forall a b c d.
    (a -> b -> Bool) -> Generator a c -> Generator b d -> c -> d -> Bool;
list_all2_fusion p g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 &&
           (case next g1 s1 of {
             (x, s1a) ->
               (case next g2 s2 of {
                 (y, s2a) -> p x y && list_all2_fusion p g1 g2 s1a s2a;
               });
           })
    else not (has_next g2 s2));

set_eq :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
set_eq (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_eq RBT_set RBT_set: ccompare = None"
        (\ _ -> set_eq (RBT_set rbt1) (RBT_set rbt2));
    Just c ->
      (case ceq of {
        Nothing ->
          list_all2_fusion (\ x y -> equal_order (c x y) Eqa) rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
        Just eq ->
          list_all2_fusion eq rbt_keys_generator rbt_keys_generator (init rbt1)
            (init rbt2);
      });
  });
set_eq (Complement a) (Complement b) = set_eq a b;
set_eq a b = less_eq_set a b && less_eq_set b a;

newtype Alist b a = Alist [(b, a)];

data Mapping a b = Assoc_List_Mapping (Alist a b)
  | RBT_Mapping (Mapping_rbt a b) | Mapping (a -> Maybe b);

data Fsm_with_precomputations_impl a b c =
  FSMWPI a (Set a) (Set b) (Set c) (Set (a, (b, (c, a))))
    (Mapping (a, b) (Set (c, a))) (Mapping (a, b) (Mapping c a));

newtype Fsm_with_precomputations c b a = Fsm_with_precomputations
  (Fsm_with_precomputations_impl c b a);

fsm_with_precomputations_impl_of_fsm_with_precomputations ::
  forall c b a.
    Fsm_with_precomputations c b a -> Fsm_with_precomputations_impl c b a;
fsm_with_precomputations_impl_of_fsm_with_precomputations
  (Fsm_with_precomputations x) = x;

transitions_wpi ::
  forall a b c. Fsm_with_precomputations_impl a b c -> Set (a, (b, (c, a)));
transitions_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x5;

transitions_wp ::
  forall a b c. Fsm_with_precomputations a b c -> Set (a, (b, (c, a)));
transitions_wp x =
  transitions_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

newtype Fsm_impl a b c = FSMWP (Fsm_with_precomputations a b c);

transitionsa :: forall a b c. Fsm_impl a b c -> Set (a, (b, (c, a)));
transitionsa (FSMWP m) = transitions_wp m;

newtype Fsm c b a = Abs_fsm (Fsm_impl c b a);

fsm_impl_of_fsm :: forall c b a. Fsm c b a -> Fsm_impl c b a;
fsm_impl_of_fsm (Abs_fsm x) = x;

transitions :: forall a b c. Fsm a b c -> Set (a, (b, (c, a)));
transitions x = transitionsa (fsm_impl_of_fsm x);

outputs_wpi :: forall a b c. Fsm_with_precomputations_impl a b c -> Set c;
outputs_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x4;

outputs_wp :: forall a b c. Fsm_with_precomputations a b c -> Set c;
outputs_wp x =
  outputs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

outputsa :: forall a b c. Fsm_impl a b c -> Set c;
outputsa (FSMWP m) = outputs_wp m;

outputs :: forall a b c. Fsm a b c -> Set c;
outputs x = outputsa (fsm_impl_of_fsm x);

initial_wpi :: forall a b c. Fsm_with_precomputations_impl a b c -> a;
initial_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x1;

initial_wp :: forall a b c. Fsm_with_precomputations a b c -> a;
initial_wp x =
  initial_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

initiala :: forall a b c. Fsm_impl a b c -> a;
initiala (FSMWP m) = initial_wp m;

initial :: forall a b c. Fsm a b c -> a;
initial x = initiala (fsm_impl_of_fsm x);

states_wpi :: forall a b c. Fsm_with_precomputations_impl a b c -> Set a;
states_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x2;

states_wp :: forall a b c. Fsm_with_precomputations a b c -> Set a;
states_wp x =
  states_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

statesa :: forall a b c. Fsm_impl a b c -> Set a;
statesa (FSMWP m) = states_wp m;

states :: forall a b c. Fsm a b c -> Set a;
states x = statesa (fsm_impl_of_fsm x);

inputs_wpi :: forall a b c. Fsm_with_precomputations_impl a b c -> Set b;
inputs_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x3;

inputs_wp :: forall a b c. Fsm_with_precomputations a b c -> Set b;
inputs_wp x =
  inputs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

inputsa :: forall a b c. Fsm_impl a b c -> Set b;
inputsa (FSMWP m) = inputs_wp m;

inputs :: forall a b c. Fsm a b c -> Set b;
inputs x = inputsa (fsm_impl_of_fsm x);

comparator_prod ::
  forall a b.
    (a -> a -> Ordera) -> (b -> b -> Ordera) -> (a, b) -> (a, b) -> Ordera;
comparator_prod comp_a comp_b (x, xa) (y, ya) = (case comp_a x y of {
          Eqa -> comp_b xa ya;
          Lt -> Lt;
          Gt -> Gt;
        });

ccompare_prod ::
  forall a b. (Ccompare a, Ccompare b) => Maybe ((a, b) -> (a, b) -> Ordera);
ccompare_prod =
  (case ccompare of {
    Nothing -> Nothing;
    Just comp_a -> (case ccompare of {
                     Nothing -> Nothing;
                     Just comp_b -> Just (comparator_prod comp_a comp_b);
                   });
  });

instance (Ccompare a, Ccompare b) => Ccompare (a, b) where {
  ccompare = ccompare_prod;
};

product :: forall a b. [a] -> [b] -> [(a, b)];
product [] uu = [];
product (x : xs) ys = map (\ a -> (x, a)) ys ++ product xs ys;

cEnum_prod ::
  forall a b.
    (Cenum a,
      Cenum b) => Maybe ([(a, b)],
                          (((a, b) -> Bool) -> Bool, ((a, b) -> Bool) -> Bool));
cEnum_prod =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (enum_all_a, enum_ex_a)) ->
      (case cEnum of {
        Nothing -> Nothing;
        Just (enum_b, (enum_all_b, enum_ex_b)) ->
          Just (product enum_a enum_b,
                 ((\ p -> enum_all_a (\ x -> enum_all_b (\ y -> p (x, y)))),
                   (\ p -> enum_ex_a (\ x -> enum_ex_b (\ y -> p (x, y))))));
      });
  });

instance (Cenum a, Cenum b) => Cenum (a, b) where {
  cEnum = cEnum_prod;
};

equality_prod ::
  forall a b. (a -> a -> Bool) -> (b -> b -> Bool) -> (a, b) -> (a, b) -> Bool;
equality_prod eq_a eq_b (x, xa) (y, ya) = eq_a x y && eq_b xa ya;

ceq_prod :: forall a b. (Ceq a, Ceq b) => Maybe ((a, b) -> (a, b) -> Bool);
ceq_prod = (case ceq of {
             Nothing -> Nothing;
             Just eq_a -> (case ceq of {
                            Nothing -> Nothing;
                            Just eq_b -> Just (equality_prod eq_a eq_b);
                          });
           });

instance (Ceq a, Ceq b) => Ceq (a, b) where {
  ceq = ceq_prod;
};

equal_fsm ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
      Ceq c, Ccompare c) => Fsm a b c -> Fsm a b c -> Bool;
equal_fsm x y =
  initial x == initial y &&
    set_eq (states x) (states y) &&
      set_eq (inputs x) (inputs y) &&
        set_eq (outputs x) (outputs y) &&
          set_eq (transitions x) (transitions y);

ceq_fsm ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
      Ceq c, Ccompare c) => Maybe (Fsm a b c -> Fsm a b c -> Bool);
ceq_fsm = Just equal_fsm;

instance (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
           Ceq c, Ccompare c) => Ceq (Fsm a b c) where {
  ceq = ceq_fsm;
};

newtype Phantom a b = Phantom b;

data Set_impla = Set_Choose | Set_Collect | Set_DList | Set_RBT | Set_Monada;

set_impl_fsm :: forall a b c. Phantom (Fsm a b c) Set_impla;
set_impl_fsm = Phantom Set_DList;

class Set_impl a where {
  set_impl :: Phantom a Set_impla;
};

instance Set_impl (Fsm a b c) where {
  set_impl = set_impl_fsm;
};

ccompare_fsm :: forall a b c. Maybe (Fsm a b c -> Fsm a b c -> Ordera);
ccompare_fsm = Nothing;

instance Ccompare (Fsm a b c) where {
  ccompare = ccompare_fsm;
};

newtype Nat = Nat Integer;

integer_of_nat :: Nat -> Integer;
integer_of_nat (Nat x) = x;

equal_nat :: Nat -> Nat -> Bool;
equal_nat m n = integer_of_nat m == integer_of_nat n;

instance Eq Nat where {
  a == b = equal_nat a b;
};

data Num = One | Bit0 Num | Bit1 Num;

one_nat :: Nat;
one_nat = Nat (1 :: Integer);

class One a where {
  one :: a;
};

instance One Nat where {
  one = one_nat;
};

times_nat :: Nat -> Nat -> Nat;
times_nat m n = Nat (integer_of_nat m * integer_of_nat n);

class Times a where {
  times :: a -> a -> a;
};

class (One a, Times a) => Power a where {
};

instance Times Nat where {
  times = times_nat;
};

instance Power Nat where {
};

less_eq_nat :: Nat -> Nat -> Bool;
less_eq_nat m n = integer_of_nat m <= integer_of_nat n;

class Ord a where {
  less_eq :: a -> a -> Bool;
  less :: a -> a -> Bool;
};

less_nat :: Nat -> Nat -> Bool;
less_nat m n = integer_of_nat m < integer_of_nat n;

instance Ord Nat where {
  less_eq = less_eq_nat;
  less = less_nat;
};

class (Ord a) => Preorder a where {
};

class (Preorder a) => Order a where {
};

instance Preorder Nat where {
};

instance Order Nat where {
};

ceq_nat :: Maybe (Nat -> Nat -> Bool);
ceq_nat = Just equal_nat;

instance Ceq Nat where {
  ceq = ceq_nat;
};

set_impl_nat :: Phantom Nat Set_impla;
set_impl_nat = Phantom Set_RBT;

instance Set_impl Nat where {
  set_impl = set_impl_nat;
};

class (Order a) => Linorder a where {
};

instance Linorder Nat where {
};

finite_UNIV_nat :: Phantom Nat Bool;
finite_UNIV_nat = Phantom False;

zero_nat :: Nat;
zero_nat = Nat (0 :: Integer);

card_UNIV_nat :: Phantom Nat Nat;
card_UNIV_nat = Phantom zero_nat;

class Finite_UNIV a where {
  finite_UNIV :: Phantom a Bool;
};

class (Finite_UNIV a) => Card_UNIV a where {
  card_UNIV :: Phantom a Nat;
};

instance Finite_UNIV Nat where {
  finite_UNIV = finite_UNIV_nat;
};

instance Card_UNIV Nat where {
  card_UNIV = card_UNIV_nat;
};

cEnum_nat :: Maybe ([Nat], ((Nat -> Bool) -> Bool, (Nat -> Bool) -> Bool));
cEnum_nat = Nothing;

instance Cenum Nat where {
  cEnum = cEnum_nat;
};

comparator_of :: forall a. (Eq a, Linorder a) => a -> a -> Ordera;
comparator_of x y = (if less x y then Lt else (if x == y then Eqa else Gt));

compare_nat :: Nat -> Nat -> Ordera;
compare_nat = comparator_of;

ccompare_nat :: Maybe (Nat -> Nat -> Ordera);
ccompare_nat = Just compare_nat;

instance Ccompare Nat where {
  ccompare = ccompare_nat;
};

data Mapping_impla = Mapping_Choose | Mapping_Assoc_List | Mapping_RBT
  | Mapping_Mapping;

mapping_impl_nat :: Phantom Nat Mapping_impla;
mapping_impl_nat = Phantom Mapping_RBT;

class Mapping_impl a where {
  mapping_impl :: Phantom a Mapping_impla;
};

instance Mapping_impl Nat where {
  mapping_impl = mapping_impl_nat;
};

max :: forall a. (Ord a) => a -> a -> a;
max a b = (if less_eq a b then b else a);

instance Ord Integer where {
  less_eq = (\ a b -> a <= b);
  less = (\ a b -> a < b);
};

minus_nat :: Nat -> Nat -> Nat;
minus_nat m n = Nat (max (0 :: Integer) (integer_of_nat m - integer_of_nat n));

proper_interval_nat :: Maybe Nat -> Maybe Nat -> Bool;
proper_interval_nat no Nothing = True;
proper_interval_nat Nothing (Just x) = less_nat zero_nat x;
proper_interval_nat (Just x) (Just y) = less_nat one_nat (minus_nat y x);

cproper_interval_nat :: Maybe Nat -> Maybe Nat -> Bool;
cproper_interval_nat = proper_interval_nat;

class (Ccompare a) => Cproper_interval a where {
  cproper_interval :: Maybe a -> Maybe a -> Bool;
};

instance Cproper_interval Nat where {
  cproper_interval = cproper_interval_nat;
};

equal_set ::
  forall a. (Cenum a, Ceq a, Ccompare a, Eq a) => Set a -> Set a -> Bool;
equal_set a b = less_eq_set a b && less_eq_set b a;

instance (Cenum a, Ceq a, Ccompare a, Eq a) => Eq (Set a) where {
  a == b = equal_set a b;
};

uminus_set :: forall a. Set a -> Set a;
uminus_set (Complement b) = b;
uminus_set (Collect_set p) = Collect_set (\ x -> not (p x));
uminus_set a = Complement a;

balance :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance (Branch R a w x b) s t (Branch R c y z d) =
  Branch R (Branch B a w x b) s t (Branch B c y z d);
balance (Branch R (Branch R a w x b) s t c) y z Empty =
  Branch R (Branch B a w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch R a w x b) s t c) y z (Branch B va vb vc vd) =
  Branch R (Branch B a w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R Empty w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch B va vb vc vd) w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch R Empty w x (Branch R b s t c)) y z (Branch B va vb vc vd) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R (Branch B ve vf vg vh) w x (Branch R b s t c)) y z
  (Branch B va vb vc vd) =
  Branch R (Branch B (Branch B ve vf vg vh) w x b) s t
    (Branch B c y z (Branch B va vb vc vd));
balance Empty w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z d);
balance (Branch B va vb vc vd) w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z d);
balance Empty w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance Empty w x (Branch R (Branch R b s t c) y z (Branch B va vb vc vd)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch B va vb vc vd) w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch B va vb vc vd) w x
  (Branch R (Branch R b s t c) y z (Branch B ve vf vg vh)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t
    (Branch B c y z (Branch B ve vf vg vh));
balance Empty s t Empty = Branch B Empty s t Empty;
balance Empty s t (Branch B va vb vc vd) =
  Branch B Empty s t (Branch B va vb vc vd);
balance Empty s t (Branch v Empty vb vc Empty) =
  Branch B Empty s t (Branch v Empty vb vc Empty);
balance Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty) =
  Branch B Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty);
balance Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi)) =
  Branch B Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi));
balance Empty s t (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi))
  = Branch B Empty s t
      (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi));
balance (Branch B va vb vc vd) s t Empty =
  Branch B (Branch B va vb vc vd) s t Empty;
balance (Branch B va vb vc vd) s t (Branch B ve vf vg vh) =
  Branch B (Branch B va vb vc vd) s t (Branch B ve vf vg vh);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty) =
  Branch B (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v (Branch B vi vj vk vl) vf vg Empty)
  = Branch B (Branch B va vb vc vd) s t
      (Branch v (Branch B vi vj vk vl) vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg (Branch B vj vk vl vm))
  = Branch B (Branch B va vb vc vd) s t
      (Branch v Empty vf vg (Branch B vj vk vl vm));
balance (Branch B va vb vc vd) s t
  (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm)) =
  Branch B (Branch B va vb vc vd) s t
    (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm));
balance (Branch v Empty vb vc Empty) s t Empty =
  Branch B (Branch v Empty vb vc Empty) s t Empty;
balance (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty =
  Branch B (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty =
  Branch B (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t Empty
  = Branch B (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t
      Empty;
balance (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd) =
  Branch B (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd);
balance (Branch v Empty vf vg (Branch B vi vj vk vl)) s t (Branch B va vb vc vd)
  = Branch B (Branch v Empty vf vg (Branch B vi vj vk vl)) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg Empty) s t (Branch B va vb vc vd)
  = Branch B (Branch v (Branch B vj vk vl vm) vf vg Empty) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
  (Branch B va vb vc vd) =
  Branch B (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
    (Branch B va vb vc vd);

rbt_comp_ins ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_ins c f k v Empty = Branch R Empty k v Empty;
rbt_comp_ins c f k v (Branch B l x y r) =
  (case c k x of {
    Eqa -> Branch B l x (f k y v) r;
    Lt -> balance (rbt_comp_ins c f k v l) x y r;
    Gt -> balance l x y (rbt_comp_ins c f k v r);
  });
rbt_comp_ins c f k v (Branch R l x y r) =
  (case c k x of {
    Eqa -> Branch R l x (f k y v) r;
    Lt -> Branch R (rbt_comp_ins c f k v l) x y r;
    Gt -> Branch R l x y (rbt_comp_ins c f k v r);
  });

paint :: forall a b. Color -> Rbt a b -> Rbt a b;
paint c Empty = Empty;
paint c (Branch uu l k v r) = Branch c l k v r;

rbt_comp_insert_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_insert_with_key c f k v t = paint B (rbt_comp_ins c f k v t);

rbt_comp_insert ::
  forall a b. (a -> a -> Ordera) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_insert c = rbt_comp_insert_with_key c (\ _ _ nv -> nv);

insertc ::
  forall a b. (Ccompare a) => a -> b -> Mapping_rbt a b -> Mapping_rbt a b;
insertc xc xd xe =
  Mapping_RBTa (rbt_comp_insert (the ccompare) xc xd (impl_ofa xe));

rbt_baliR :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_baliR t1 ab bb (Branch R t2 aa ba (Branch R t3 a b t4)) =
  Branch R (Branch B t1 ab bb t2) aa ba (Branch B t3 a b t4);
rbt_baliR t1 ab bb (Branch R (Branch R t2 aa ba t3) a b Empty) =
  Branch R (Branch B t1 ab bb t2) aa ba (Branch B t3 a b Empty);
rbt_baliR t1 ab bb (Branch R (Branch R t2 aa ba t3) a b (Branch B va vb vc vd))
  = Branch R (Branch B t1 ab bb t2) aa ba
      (Branch B t3 a b (Branch B va vb vc vd));
rbt_baliR t1 a b Empty = Branch B t1 a b Empty;
rbt_baliR t1 a b (Branch B va vb vc vd) =
  Branch B t1 a b (Branch B va vb vc vd);
rbt_baliR t1 a b (Branch v Empty vb vc Empty) =
  Branch B t1 a b (Branch v Empty vb vc Empty);
rbt_baliR t1 a b (Branch v (Branch B ve vf vg vh) vb vc Empty) =
  Branch B t1 a b (Branch v (Branch B ve vf vg vh) vb vc Empty);
rbt_baliR t1 a b (Branch v Empty vb vc (Branch B vf vg vh vi)) =
  Branch B t1 a b (Branch v Empty vb vc (Branch B vf vg vh vi));
rbt_baliR t1 a b (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi))
  = Branch B t1 a b
      (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi));

equal_color :: Color -> Color -> Bool;
equal_color R B = False;
equal_color B R = False;
equal_color B B = True;
equal_color R R = True;

plus_nat :: Nat -> Nat -> Nat;
plus_nat m n = Nat (integer_of_nat m + integer_of_nat n);

suc :: Nat -> Nat;
suc n = plus_nat n one_nat;

bheight :: forall a b. Rbt a b -> Nat;
bheight Empty = zero_nat;
bheight (Branch c lt k v rt) =
  (if equal_color c B then suc (bheight lt) else bheight lt);

rbt_joinR :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_joinR l a b r =
  (if less_eq_nat (bheight l) (bheight r) then Branch R l a b r
    else (case l of {
           Branch R la ab ba ra -> Branch R la ab ba (rbt_joinR ra a b r);
           Branch B la ab ba ra -> rbt_baliR la ab ba (rbt_joinR ra a b r);
         }));

rbt_baliL :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_baliL (Branch R (Branch R t1 ab bb t2) aa ba t3) a b t4 =
  Branch R (Branch B t1 ab bb t2) aa ba (Branch B t3 a b t4);
rbt_baliL (Branch R Empty ab bb (Branch R t2 aa ba t3)) a b t4 =
  Branch R (Branch B Empty ab bb t2) aa ba (Branch B t3 a b t4);
rbt_baliL (Branch R (Branch B va vb vc vd) ab bb (Branch R t2 aa ba t3)) a b t4
  = Branch R (Branch B (Branch B va vb vc vd) ab bb t2) aa ba
      (Branch B t3 a b t4);
rbt_baliL Empty a b t2 = Branch B Empty a b t2;
rbt_baliL (Branch B va vb vc vd) a b t2 =
  Branch B (Branch B va vb vc vd) a b t2;
rbt_baliL (Branch v Empty vb vc Empty) a b t2 =
  Branch B (Branch v Empty vb vc Empty) a b t2;
rbt_baliL (Branch v Empty vb vc (Branch B ve vf vg vh)) a b t2 =
  Branch B (Branch v Empty vb vc (Branch B ve vf vg vh)) a b t2;
rbt_baliL (Branch v (Branch B vf vg vh vi) vb vc Empty) a b t2 =
  Branch B (Branch v (Branch B vf vg vh vi) vb vc Empty) a b t2;
rbt_baliL (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) a b t2
  = Branch B (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) a b
      t2;

rbt_joinL :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_joinL l a b r =
  (if less_eq_nat (bheight r) (bheight l) then Branch R l a b r
    else (case r of {
           Branch R la ab ba ra -> Branch R (rbt_joinL l a b la) ab ba ra;
           Branch B la ab ba ra -> rbt_baliL (rbt_joinL l a b la) ab ba ra;
         }));

rbt_join :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_join l a b r =
  let {
    bhl = bheight l;
    bhr = bheight r;
  } in (if less_nat bhr bhl then paint B (rbt_joinR l a b r)
         else (if less_nat bhl bhr then paint B (rbt_joinL l a b r)
                else Branch B l a b r));

rbt_split_comp ::
  forall a b.
    (a -> a -> Ordera) -> Rbt a b -> a -> (Rbt a b, (Maybe b, Rbt a b));
rbt_split_comp c Empty k = (Empty, (Nothing, Empty));
rbt_split_comp c (Branch uu l a b r) x =
  (case c x a of {
    Eqa -> (l, (Just b, r));
    Lt -> (case rbt_split_comp c l x of {
            (l1, (beta, l2)) -> (l1, (beta, rbt_join l2 a b r));
          });
    Gt -> (case rbt_split_comp c r x of {
            (r1, (beta, r2)) -> (rbt_join l a b r1, (beta, r2));
          });
  });

nat_of_integer :: Integer -> Nat;
nat_of_integer k = Nat (max (0 :: Integer) k);

folda :: forall a b c. (a -> b -> c -> c) -> Rbt a b -> c -> c;
folda f (Branch c lt k v rt) x = folda f rt (f k v (folda f lt x));
folda f Empty x = x;

rbt_comp_union_swap_rec ::
  forall a b.
    (a -> a -> Ordera) ->
      (a -> b -> b -> b) -> Bool -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_union_swap_rec c f gamma t1 t2 =
  let {
    bh1 = bheight t1;
    bh2 = bheight t2;
  } in (case (if less_nat bh1 bh2 then (not gamma, (t1, (bh1, (t2, bh2))))
               else (gamma, (t2, (bh2, (t1, bh1)))))
         of {
         (gammaa, (t2a, (bh2a, (t1a, _)))) ->
           let {
             fa = (if gammaa then (\ k v va -> f k va v) else f);
           } in (if less_nat bh2a (nat_of_integer (4 :: Integer))
                  then folda (rbt_comp_insert_with_key c fa) t2a t1a
                  else (case t1a of {
                         Empty -> t2a;
                         Branch _ l1 a b r1 ->
                           (case rbt_split_comp c t2a a of {
                             (l2, (beta, r2)) ->
                               rbt_join
                                 (rbt_comp_union_swap_rec c f gammaa l1 l2) a
                                 (case beta of {
                                   Nothing -> b;
                                   Just ca -> fa a b ca;
                                 })
                                 (rbt_comp_union_swap_rec c f gammaa r1 r2);
                           });
                       }));
       });

rbt_comp_union_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_union_with_key c f t1 t2 =
  paint B (rbt_comp_union_swap_rec c f False t1 t2);

join ::
  forall a b.
    (Ccompare a) => (a -> b -> b -> b) ->
                      Mapping_rbt a b -> Mapping_rbt a b -> Mapping_rbt a b;
join xc xd xe =
  Mapping_RBTa
    (rbt_comp_union_with_key (the ccompare) xc (impl_ofa xd) (impl_ofa xe));

list_insert :: forall a. (a -> a -> Bool) -> a -> [a] -> [a];
list_insert equal x xs = (if list_member equal xs x then xs else x : xs);

inserta :: forall a. (Ceq a) => a -> Set_dlist a -> Set_dlist a;
inserta xb xc = Abs_dlist (list_insert (the ceq) xb (list_of_dlist xc));

fold :: forall a b. (a -> b -> b) -> [a] -> b -> b;
fold f (x : xs) s = fold f xs (f x s);
fold f [] s = s;

foldc :: forall a b. (Ceq a) => (a -> b -> b) -> Set_dlist a -> b -> b;
foldc x xc = fold x (list_of_dlist xc);

union :: forall a. (Ceq a) => Set_dlist a -> Set_dlist a -> Set_dlist a;
union = foldc inserta;

is_none :: forall a. Maybe a -> Bool;
is_none (Just x) = False;
is_none Nothing = True;

inter_list ::
  forall a. (Ccompare a) => Mapping_rbt a () -> [a] -> Mapping_rbt a ();
inter_list xb xc =
  Mapping_RBTa
    (fold (\ k -> rbt_comp_insert (the ccompare) k ())
      (filter
        (\ x -> not (is_none (rbt_comp_lookup (the ccompare) (impl_ofa xb) x)))
        xc)
      Empty);

gen_length :: forall a. Nat -> [a] -> Nat;
gen_length n (x : xs) = gen_length (suc n) xs;
gen_length n [] = n;

size_list :: forall a. [a] -> Nat;
size_list = gen_length zero_nat;

map_prod :: forall a b c d. (a -> b) -> (c -> d) -> (a, c) -> (b, d);
map_prod f g (a, b) = (f a, g b);

divmod_nat :: Nat -> Nat -> (Nat, Nat);
divmod_nat m n =
  let {
    k = integer_of_nat m;
    l = integer_of_nat n;
  } in map_prod nat_of_integer nat_of_integer
         (if k == (0 :: Integer) then ((0 :: Integer), (0 :: Integer))
           else (if l == (0 :: Integer) then ((0 :: Integer), k)
                  else divMod (abs k) (abs l)));

apfst :: forall a b c. (a -> b) -> (a, c) -> (b, c);
apfst f (x, y) = (f x, y);

rbtreeify_g :: forall a b. Nat -> [(a, b)] -> (Rbt a b, [(a, b)]);
rbtreeify_g n kvs =
  (if equal_nat n zero_nat || equal_nat n one_nat then (Empty, kvs)
    else (case divmod_nat n (nat_of_integer (2 :: Integer)) of {
           (na, r) ->
             (if equal_nat r zero_nat
               then (case rbtreeify_g na kvs of {
                      (t1, (k, v) : kvsa) ->
                        apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                    })
               else (case rbtreeify_f na kvs of {
                      (t1, (k, v) : kvsa) ->
                        apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                    }));
         }));

rbtreeify_f :: forall a b. Nat -> [(a, b)] -> (Rbt a b, [(a, b)]);
rbtreeify_f n kvs =
  (if equal_nat n zero_nat then (Empty, kvs)
    else (if equal_nat n one_nat
           then (case kvs of {
                  (k, v) : kvsa -> (Branch R Empty k v Empty, kvsa);
                })
           else (case divmod_nat n (nat_of_integer (2 :: Integer)) of {
                  (na, r) ->
                    (if equal_nat r zero_nat
                      then (case rbtreeify_f na kvs of {
                             (t1, (k, v) : kvsa) ->
                               apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                           })
                      else (case rbtreeify_f na kvs of {
                             (t1, (k, v) : kvsa) ->
                               apfst (Branch B t1 k v) (rbtreeify_f na kvsa);
                           }));
                })));

rbtreeify :: forall a b. [(a, b)] -> Rbt a b;
rbtreeify kvs = fst (rbtreeify_g (suc (size_list kvs)) kvs);

gen_entries :: forall a b. [((a, b), Rbt a b)] -> Rbt a b -> [(a, b)];
gen_entries kvts (Branch c l k v r) = gen_entries (((k, v), r) : kvts) l;
gen_entries ((kv, t) : kvts) Empty = kv : gen_entries kvts t;
gen_entries [] Empty = [];

entries :: forall a b. Rbt a b -> [(a, b)];
entries = gen_entries [];

filtere ::
  forall a b.
    (Ccompare a) => ((a, b) -> Bool) -> Mapping_rbt a b -> Mapping_rbt a b;
filtere xb xc = Mapping_RBTa (rbtreeify (filter xb (entries (impl_ofa xc))));

map_filter :: forall a b. (a -> Maybe b) -> [a] -> [b];
map_filter f [] = [];
map_filter f (x : xs) = (case f x of {
                          Nothing -> map_filter f xs;
                          Just y -> y : map_filter f xs;
                        });

map_filter_comp_inter ::
  forall a b c d.
    (a -> a -> Ordera) -> (a -> b -> c -> d) -> Rbt a b -> Rbt a c -> [(a, d)];
map_filter_comp_inter c f t1 t2 =
  map_filter (\ (k, v) -> (case rbt_comp_lookup c t1 k of {
                            Nothing -> Nothing;
                            Just va -> Just (k, f k va v);
                          }))
    (entries t2);

is_rbt_empty :: forall a b. Rbt a b -> Bool;
is_rbt_empty t = (case t of {
                   Empty -> True;
                   Branch _ _ _ _ _ -> False;
                 });

rbt_split_min :: forall a b. Rbt a b -> (a, (b, Rbt a b));
rbt_split_min Empty = error "undefined";
rbt_split_min (Branch uu l a b r) =
  (if is_rbt_empty l then (a, (b, r))
    else (case rbt_split_min l of {
           (aa, (ba, la)) -> (aa, (ba, rbt_join la a b r));
         }));

rbt_join2 :: forall a b. Rbt a b -> Rbt a b -> Rbt a b;
rbt_join2 l r =
  (if is_rbt_empty r then l else (case rbt_split_min r of {
                                   (a, (b, c)) -> rbt_join l a b c;
                                 }));

rbt_comp_inter_swap_rec ::
  forall a b.
    (a -> a -> Ordera) ->
      (a -> b -> b -> b) -> Bool -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_inter_swap_rec c f gamma t1 t2 =
  let {
    bh1 = bheight t1;
    bh2 = bheight t2;
  } in (case (if less_nat bh1 bh2 then (not gamma, (t1, (bh1, (t2, bh2))))
               else (gamma, (t2, (bh2, (t1, bh1)))))
         of {
         (gammaa, (t2a, (bh2a, (t1a, _)))) ->
           let {
             fa = (if gammaa then (\ k v va -> f k va v) else f);
           } in (if less_nat bh2a (nat_of_integer (4 :: Integer))
                  then rbtreeify (map_filter_comp_inter c fa t1a t2a)
                  else (case t1a of {
                         Empty -> Empty;
                         Branch _ l1 a b r1 ->
                           (case rbt_split_comp c t2a a of {
                             (l2, (beta, r2)) ->
                               let {
                                 l = rbt_comp_inter_swap_rec c f gammaa l1 l2;
                                 r = rbt_comp_inter_swap_rec c f gammaa r1 r2;
                               } in (case beta of {
                                      Nothing -> rbt_join2 l r;
                                      Just ba -> rbt_join l a (fa a b ba) r;
                                    });
                           });
                       }));
       });

rbt_comp_inter_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_inter_with_key c f t1 t2 =
  paint B (rbt_comp_inter_swap_rec c f False t1 t2);

meet ::
  forall a b.
    (Ccompare a) => (a -> b -> b -> b) ->
                      Mapping_rbt a b -> Mapping_rbt a b -> Mapping_rbt a b;
meet xc xd xe =
  Mapping_RBTa
    (rbt_comp_inter_with_key (the ccompare) xc (impl_ofa xd) (impl_ofa xe));

filterd :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Set_dlist a;
filterd xb xc = Abs_dlist (filter xb (list_of_dlist xc));

inf_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
inf_set (RBT_set rbt1) (Set_Monad xs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set Set_Monad: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (Set_Monad xs));
    Just _ -> RBT_set (inter_list rbt1 xs);
  });
inf_set (RBT_set rbt) (DList_set dxs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set DList_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "inter RBT_set DList_set: ceq = None"
            (\ _ -> inf_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (inter_list rbt (list_of_dlist dxs));
      });
  });
inf_set (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set RBT_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (RBT_set rbt2));
    Just _ -> RBT_set (meet (\ _ _ -> id) rbt1 rbt2);
  });
inf_set (DList_set dxs1) (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set Set_Monad: ceq = None"
        (\ _ -> inf_set (DList_set dxs1) (Set_Monad xs));
    Just eq -> DList_set (filterd (list_member eq xs) dxs1);
  });
inf_set (DList_set dxs1) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set DList_set: ceq = None"
        (\ _ -> inf_set (DList_set dxs1) (DList_set dxs2));
    Just _ -> DList_set (filterd (memberc dxs2) dxs1);
  });
inf_set (DList_set dxs) (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set RBT_set: ccompare = None"
        (\ _ -> inf_set (DList_set dxs) (RBT_set rbt));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "inter DList_set RBT_set: ceq = None"
            (\ _ -> inf_set (DList_set dxs) (RBT_set rbt));
        Just _ -> RBT_set (inter_list rbt (list_of_dlist dxs));
      });
  });
inf_set (Set_Monad xs1) (Set_Monad xs2) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad Set_Monad: ceq = None"
        (\ _ -> inf_set (Set_Monad xs1) (Set_Monad xs2));
    Just eq -> Set_Monad (filter (list_member eq xs2) xs1);
  });
inf_set (Set_Monad xs) (DList_set dxs2) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad DList_set: ceq = None"
        (\ _ -> inf_set (Set_Monad xs) (DList_set dxs2));
    Just eq -> DList_set (filterd (list_member eq xs) dxs2);
  });
inf_set (Set_Monad xs) (RBT_set rbt1) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad RBT_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (Set_Monad xs));
    Just _ -> RBT_set (inter_list rbt1 xs);
  });
inf_set (Complement ba) (Complement b) = Complement (sup_set ba b);
inf_set g (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set2: ccompare = None" (\ _ -> inf_set g (RBT_set rbt2));
    Just _ -> RBT_set (filtere ((\ x -> member x g) . fst) rbt2);
  });
inf_set (RBT_set rbt1) g =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set1: ccompare = None" (\ _ -> inf_set (RBT_set rbt1) g);
    Just _ -> RBT_set (filtere ((\ x -> member x g) . fst) rbt1);
  });
inf_set h (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set2: ceq = None" (\ _ -> inf_set h (DList_set dxs2));
    Just _ -> DList_set (filterd (\ x -> member x h) dxs2);
  });
inf_set (DList_set dxs1) h =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set1: ceq = None" (\ _ -> inf_set (DList_set dxs1) h);
    Just _ -> DList_set (filterd (\ x -> member x h) dxs1);
  });
inf_set i (Set_Monad xs) = Set_Monad (filter (\ x -> member x i) xs);
inf_set (Set_Monad xs) i = Set_Monad (filter (\ x -> member x i) xs);
inf_set j (Collect_set a) = Collect_set (\ x -> a x && member x j);
inf_set (Collect_set a) j = Collect_set (\ x -> a x && member x j);

sup_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
sup_set ba (Complement b) = Complement (inf_set (uminus_set ba) b);
sup_set (Complement ba) b = Complement (inf_set ba (uminus_set b));
sup_set b (Collect_set a) = Collect_set (\ x -> a x || member x b);
sup_set (Collect_set a) b = Collect_set (\ x -> a x || member x b);
sup_set (Set_Monad xs) (Set_Monad ys) = Set_Monad (xs ++ ys);
sup_set (DList_set dxs1) (Set_Monad ws) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set Set_Monad: ceq = None"
        (\ _ -> sup_set (DList_set dxs1) (Set_Monad ws));
    Just _ -> DList_set (fold inserta ws dxs1);
  });
sup_set (Set_Monad ws) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union Set_Monad DList_set: ceq = None"
        (\ _ -> sup_set (Set_Monad ws) (DList_set dxs2));
    Just _ -> DList_set (fold inserta ws dxs2);
  });
sup_set (RBT_set rbt1) (Set_Monad zs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set Set_Monad: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt1) (Set_Monad zs));
    Just _ -> RBT_set (fold (\ k -> insertc k ()) zs rbt1);
  });
sup_set (Set_Monad zs) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union Set_Monad RBT_set: ccompare = None"
        (\ _ -> sup_set (Set_Monad zs) (RBT_set rbt2));
    Just _ -> RBT_set (fold (\ k -> insertc k ()) zs rbt2);
  });
sup_set (DList_set dxs1) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set DList_set: ceq = None"
        (\ _ -> sup_set (DList_set dxs1) (DList_set dxs2));
    Just _ -> DList_set (union dxs1 dxs2);
  });
sup_set (DList_set dxs) (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set RBT_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "union DList_set RBT_set: ceq = None"
            (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (foldc (\ k -> insertc k ()) dxs rbt);
      });
  });
sup_set (RBT_set rbt) (DList_set dxs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set DList_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "union RBT_set DList_set: ceq = None"
            (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (foldc (\ k -> insertc k ()) dxs rbt);
      });
  });
sup_set (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set RBT_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt1) (RBT_set rbt2));
    Just _ -> RBT_set (join (\ _ _ -> id) rbt1 rbt2);
  });

class Inf a where {
  inf :: a -> a -> a;
};

instance (Ceq a, Ccompare a) => Inf (Set a) where {
  inf = inf_set;
};

class Sup a where {
  sup :: a -> a -> a;
};

instance (Ceq a, Ccompare a) => Sup (Set a) where {
  sup = sup_set;
};

less_set :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
less_set a b = less_eq_set a b && not (less_eq_set b a);

instance (Cenum a, Ceq a, Ccompare a) => Ord (Set a) where {
  less_eq = less_eq_set;
  less = less_set;
};

instance (Cenum a, Ceq a, Ccompare a) => Preorder (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Order (Set a) where {
};

class (Sup a, Order a) => Semilattice_sup a where {
};

class (Inf a, Order a) => Semilattice_inf a where {
};

class (Semilattice_inf a, Semilattice_sup a) => Lattice a where {
};

instance (Cenum a, Ceq a, Ccompare a) => Semilattice_sup (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Semilattice_inf (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Lattice (Set a) where {
};

ceq_set ::
  forall a. (Cenum a, Ceq a, Ccompare a) => Maybe (Set a -> Set a -> Bool);
ceq_set = (case (ceq :: Maybe (a -> a -> Bool)) of {
            Nothing -> Nothing;
            Just _ -> Just set_eq;
          });

instance (Cenum a, Ceq a, Ccompare a) => Ceq (Set a) where {
  ceq = ceq_set;
};

set_impl_set :: forall a. Phantom (Set a) Set_impla;
set_impl_set = Phantom Set_Choose;

instance Set_impl (Set a) where {
  set_impl = set_impl_set;
};

of_phantom :: forall a b. Phantom a b -> b;
of_phantom (Phantom x) = x;

finite_UNIV_set :: forall a. (Finite_UNIV a) => Phantom (Set a) Bool;
finite_UNIV_set = Phantom (of_phantom (finite_UNIV :: Phantom a Bool));

power :: forall a. (Power a) => a -> Nat -> a;
power a n =
  (if equal_nat n zero_nat then one
    else times a (power a (minus_nat n one_nat)));

card_UNIV_set :: forall a. (Card_UNIV a) => Phantom (Set a) Nat;
card_UNIV_set =
  Phantom
    (let {
       c = of_phantom (card_UNIV :: Phantom a Nat);
     } in (if equal_nat c zero_nat then zero_nat
            else power (nat_of_integer (2 :: Integer)) c));

instance (Finite_UNIV a) => Finite_UNIV (Set a) where {
  finite_UNIV = finite_UNIV_set;
};

instance (Card_UNIV a) => Card_UNIV (Set a) where {
  card_UNIV = card_UNIV_set;
};

emptyd :: forall a b. (Ccompare a) => Mapping_rbt a b;
emptyd = Mapping_RBTa Empty;

emptyb :: forall a. (Ceq a) => Set_dlist a;
emptyb = Abs_dlist [];

set_empty_choose :: forall a. (Ceq a, Ccompare a) => Set a;
set_empty_choose = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                     Nothing -> (case (ceq :: Maybe (a -> a -> Bool)) of {
                                  Nothing -> Set_Monad [];
                                  Just _ -> DList_set emptyb;
                                });
                     Just _ -> RBT_set emptyd;
                   });

set_empty :: forall a. (Ceq a, Ccompare a) => Set_impla -> Set a;
set_empty Set_Choose = set_empty_choose;
set_empty Set_Monada = Set_Monad [];
set_empty Set_RBT = RBT_set emptyd;
set_empty Set_DList = DList_set emptyb;
set_empty Set_Collect = Collect_set (\ _ -> False);

fun_upda :: forall a b. (a -> a -> Bool) -> (a -> b) -> a -> b -> a -> b;
fun_upda equal f aa b a = (if equal aa a then b else f a);

balance_right :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance_right a k x (Branch R b s y c) = Branch R a k x (Branch B b s y c);
balance_right (Branch B a k x b) s y Empty =
  balance (Branch R a k x b) s y Empty;
balance_right (Branch B a k x b) s y (Branch B va vb vc vd) =
  balance (Branch R a k x b) s y (Branch B va vb vc vd);
balance_right (Branch R a k x (Branch B b s y c)) t z Empty =
  Branch R (balance (paint R a) k x b) s y (Branch B c t z Empty);
balance_right (Branch R a k x (Branch B b s y c)) t z (Branch B va vb vc vd) =
  Branch R (balance (paint R a) k x b) s y
    (Branch B c t z (Branch B va vb vc vd));
balance_right Empty k x Empty = Empty;
balance_right (Branch R va vb vc Empty) k x Empty = Empty;
balance_right (Branch R va vb vc (Branch R ve vf vg vh)) k x Empty = Empty;
balance_right Empty k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg Empty) k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg (Branch R vi vj vk vl)) k x
  (Branch B va vb vc vd) = Empty;

balance_left :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance_left (Branch R a k x b) s y c = Branch R (Branch B a k x b) s y c;
balance_left Empty k x (Branch B a s y b) =
  balance Empty k x (Branch R a s y b);
balance_left (Branch B va vb vc vd) k x (Branch B a s y b) =
  balance (Branch B va vb vc vd) k x (Branch R a s y b);
balance_left Empty k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B Empty k x a) s y (balance b t z (paint R c));
balance_left (Branch B va vb vc vd) k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B (Branch B va vb vc vd) k x a) s y
    (balance b t z (paint R c));
balance_left Empty k x Empty = Empty;
balance_left Empty k x (Branch R Empty vb vc vd) = Empty;
balance_left Empty k x (Branch R (Branch R ve vf vg vh) vb vc vd) = Empty;
balance_left (Branch B va vb vc vd) k x Empty = Empty;
balance_left (Branch B va vb vc vd) k x (Branch R Empty vf vg vh) = Empty;
balance_left (Branch B va vb vc vd) k x
  (Branch R (Branch R vi vj vk vl) vf vg vh) = Empty;

combine :: forall a b. Rbt a b -> Rbt a b -> Rbt a b;
combine Empty x = x;
combine (Branch v va vb vc vd) Empty = Branch v va vb vc vd;
combine (Branch R a k x b) (Branch R c s y d) =
  (case combine b c of {
    Empty -> Branch R a k x (Branch R Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch R a k x b2) t z (Branch R c2 s y d);
    Branch B b2 t z c2 -> Branch R a k x (Branch R (Branch B b2 t z c2) s y d);
  });
combine (Branch B a k x b) (Branch B c s y d) =
  (case combine b c of {
    Empty -> balance_left a k x (Branch B Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch B a k x b2) t z (Branch B c2 s y d);
    Branch B b2 t z c2 ->
      balance_left a k x (Branch B (Branch B b2 t z c2) s y d);
  });
combine (Branch B va vb vc vd) (Branch R b k x c) =
  Branch R (combine (Branch B va vb vc vd) b) k x c;
combine (Branch R a k x b) (Branch B va vb vc vd) =
  Branch R a k x (combine b (Branch B va vb vc vd));

rbt_comp_del :: forall a b. (a -> a -> Ordera) -> a -> Rbt a b -> Rbt a b;
rbt_comp_del c x Empty = Empty;
rbt_comp_del c x (Branch uu a y s b) =
  (case c x y of {
    Eqa -> combine a b;
    Lt -> rbt_comp_del_from_left c x a y s b;
    Gt -> rbt_comp_del_from_right c x a y s b;
  });

rbt_comp_del_from_left ::
  forall a b.
    (a -> a -> Ordera) -> a -> Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_del_from_left c x (Branch B lt z v rt) y s b =
  balance_left (rbt_comp_del c x (Branch B lt z v rt)) y s b;
rbt_comp_del_from_left c x Empty y s b =
  Branch R (rbt_comp_del c x Empty) y s b;
rbt_comp_del_from_left c x (Branch R va vb vc vd) y s b =
  Branch R (rbt_comp_del c x (Branch R va vb vc vd)) y s b;

rbt_comp_del_from_right ::
  forall a b.
    (a -> a -> Ordera) -> a -> Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_del_from_right c x a y s (Branch B lt z v rt) =
  balance_right a y s (rbt_comp_del c x (Branch B lt z v rt));
rbt_comp_del_from_right c x a y s Empty =
  Branch R a y s (rbt_comp_del c x Empty);
rbt_comp_del_from_right c x a y s (Branch R va vb vc vd) =
  Branch R a y s (rbt_comp_del c x (Branch R va vb vc vd));

rbt_comp_delete :: forall a b. (a -> a -> Ordera) -> a -> Rbt a b -> Rbt a b;
rbt_comp_delete c k t = paint B (rbt_comp_del c k t);

delete :: forall a b. (Ccompare a) => a -> Mapping_rbt a b -> Mapping_rbt a b;
delete xb xc = Mapping_RBTa (rbt_comp_delete (the ccompare) xb (impl_ofa xc));

list_remove1 :: forall a. (a -> a -> Bool) -> a -> [a] -> [a];
list_remove1 equal x (y : xs) =
  (if equal x y then xs else y : list_remove1 equal x xs);
list_remove1 equal x [] = [];

removea :: forall a. (Ceq a) => a -> Set_dlist a -> Set_dlist a;
removea xb xc = Abs_dlist (list_remove1 (the ceq) xb (list_of_dlist xc));

insert :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Set a;
insert xa (Complement x) = Complement (remove xa x);
insert x (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert RBT_set: ccompare = None" (\ _ -> insert x (RBT_set rbt));
    Just _ -> RBT_set (insertc x () rbt);
  });
insert x (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert DList_set: ceq = None" (\ _ -> insert x (DList_set dxs));
    Just _ -> DList_set (inserta x dxs);
  });
insert x (Set_Monad xs) = Set_Monad (x : xs);
insert x (Collect_set a) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert Collect_set: ceq = None" (\ _ -> insert x (Collect_set a));
    Just eq -> Collect_set (fun_upda eq a x True);
  });

remove :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Set a;
remove x (Complement a) = Complement (insert x a);
remove x (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove RBT_set: ccompare = None" (\ _ -> remove x (RBT_set rbt));
    Just _ -> RBT_set (delete x rbt);
  });
remove x (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove DList_set: ceq = None" (\ _ -> remove x (DList_set dxs));
    Just _ -> DList_set (removea x dxs);
  });
remove x (Collect_set a) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "remove Collect: ceq = None"
        (\ _ -> remove x (Collect_set a));
    Just eq -> Collect_set (fun_upda eq a x False);
  });

foldl :: forall a b. (a -> b -> a) -> a -> [b] -> a;
foldl f a [] = a;
foldl f a (x : xs) = foldl f (f a x) xs;

set_aux :: forall a. (Ceq a, Ccompare a) => Set_impla -> [a] -> Set a;
set_aux Set_Monada = Set_Monad;
set_aux Set_Choose =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing -> (case (ceq :: Maybe (a -> a -> Bool)) of {
                 Nothing -> Set_Monad;
                 Just _ -> foldl (\ s x -> insert x s) (DList_set emptyb);
               });
    Just _ -> foldl (\ s x -> insert x s) (RBT_set emptyd);
  });
set_aux impl = foldl (\ s x -> insert x s) (set_empty impl);

set :: forall a. (Ceq a, Ccompare a, Set_impl a) => [a] -> Set a;
set xs = set_aux (of_phantom (set_impl :: Phantom a Set_impla)) xs;

subseqs :: forall a. [a] -> [[a]];
subseqs [] = [[]];
subseqs (x : xs) = let {
                     xss = subseqs xs;
                   } in map (\ a -> x : a) xss ++ xss;

cEnum_set ::
  forall a.
    (Cenum a, Ceq a, Ccompare a,
      Set_impl a) => Maybe ([Set a],
                             ((Set a -> Bool) -> Bool,
                               (Set a -> Bool) -> Bool));
cEnum_set =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (_, _)) ->
      Just (map set (subseqs enum_a),
             ((\ p -> all p (map set (subseqs enum_a))),
               (\ p -> any p (map set (subseqs enum_a)))));
  });

instance (Cenum a, Ceq a, Ccompare a, Set_impl a) => Cenum (Set a) where {
  cEnum = cEnum_set;
};

set_less_eq_aux_Compl_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
set_less_eq_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then (case next g1 s1 of {
                  (x, s1a) ->
                    (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 set_less_eq_aux_Compl_fusion less
                                   proper_interval g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
set_less_eq_aux_Compl_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else proper_interval ao (Just y)));
                    });
                })
           else True)
    else True);

compl_set_less_eq_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
compl_set_less_eq_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then not (proper_interval ao (Just x)) &&
                                 compl_set_less_eq_aux_fusion less
                                   proper_interval g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then not (proper_interval ao (Just y)) &&
compl_set_less_eq_aux_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else not (proper_interval ao (Just y))));
                    })
               else not (proper_interval ao (Just x)) &&
                      compl_set_less_eq_aux_fusion less proper_interval g1 g2
                        (Just x) s1a s2);
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (y, s2a) ->
                    not (proper_interval ao (Just y)) &&
                      compl_set_less_eq_aux_fusion less proper_interval g1 g2
                        (Just y) s1 s2a;
                })
           else not (proper_interval ao Nothing)));

set_less_eq_aux_Compl ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
set_less_eq_aux_Compl less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           set_less_eq_aux_Compl less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  set_less_eq_aux_Compl less proper_interval (Just y) (x : xs)
                    ys
           else proper_interval ao (Just y)));
set_less_eq_aux_Compl less proper_interval ao xs [] = True;
set_less_eq_aux_Compl less proper_interval ao [] ys = True;

compl_set_less_eq_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
compl_set_less_eq_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           compl_set_less_eq_aux less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  compl_set_less_eq_aux less proper_interval (Just y) (x : xs)
                    ys
           else not (proper_interval ao (Just y))));
compl_set_less_eq_aux less proper_interval ao (x : xs) [] =
  not (proper_interval ao (Just x)) &&
    compl_set_less_eq_aux less proper_interval (Just x) xs [];
compl_set_less_eq_aux less proper_interval ao [] (y : ys) =
  not (proper_interval ao (Just y)) &&
    compl_set_less_eq_aux less proper_interval (Just y) [] ys;
compl_set_less_eq_aux less proper_interval ao [] [] =
  not (proper_interval ao Nothing);

lexord_eq_fusion ::
  forall a b c.
    (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
lexord_eq_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 &&
           (case next g1 s1 of {
             (x, s1a) ->
               (case next g2 s2 of {
                 (y, s2a) ->
                   less x y ||
                     not (less y x) && lexord_eq_fusion less g1 g2 s1a s2a;
               });
           })
    else True);

remdups_sorted :: forall a. (a -> a -> Bool) -> [a] -> [a];
remdups_sorted less (x : y : xs) =
  (if less x y then x : remdups_sorted less (y : xs)
    else remdups_sorted less (y : xs));
remdups_sorted less [x] = [x];
remdups_sorted less [] = [];

quicksort_acc :: forall a. (a -> a -> Bool) -> [a] -> [a] -> [a];
quicksort_acc less ac (x : v : va) = quicksort_part less ac x [] [] [] (v : va);
quicksort_acc less ac [x] = x : ac;
quicksort_acc less ac [] = ac;

quicksort_part ::
  forall a. (a -> a -> Bool) -> [a] -> a -> [a] -> [a] -> [a] -> [a] -> [a];
quicksort_part less ac x lts eqs gts (z : zs) =
  (if less x z then quicksort_part less ac x lts eqs (z : gts) zs
    else (if less z x then quicksort_part less ac x (z : lts) eqs gts zs
           else quicksort_part less ac x lts (z : eqs) gts zs));
quicksort_part less ac x lts eqs gts [] =
  quicksort_acc less (eqs ++ x : quicksort_acc less ac gts) lts;

quicksort :: forall a. (a -> a -> Bool) -> [a] -> [a];
quicksort less = quicksort_acc less [];

gen_keys :: forall a b. [(a, Rbt a b)] -> Rbt a b -> [a];
gen_keys kts (Branch c l k v r) = gen_keys ((k, r) : kts) l;
gen_keys ((k, t) : kts) Empty = k : gen_keys kts t;
gen_keys [] Empty = [];

keysa :: forall a b. Rbt a b -> [a];
keysa = gen_keys [];

keysb :: forall a. (Ccompare a) => Mapping_rbt a () -> [a];
keysb xa = keysa (impl_ofa xa);

csorted_list_of_set :: forall a. (Ceq a, Ccompare a) => Set a -> [a];
csorted_list_of_set (Set_Monad xs) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set Set_Monad: ccompare = None"
        (\ _ -> csorted_list_of_set (Set_Monad xs));
    Just c -> remdups_sorted (lt_of_comp c) (quicksort (lt_of_comp c) xs);
  });
csorted_list_of_set (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set DList_set: ceq = None"
        (\ _ -> csorted_list_of_set (DList_set dxs));
    Just _ ->
      (case ccompare of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "csorted_list_of_set DList_set: ccompare = None"
            (\ _ -> csorted_list_of_set (DList_set dxs));
        Just c -> quicksort (lt_of_comp c) (list_of_dlist dxs);
      });
  });
csorted_list_of_set (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set RBT_set: ccompare = None"
        (\ _ -> csorted_list_of_set (RBT_set rbt));
    Just _ -> keysb rbt;
  });

bot_set :: forall a. (Ceq a, Ccompare a, Set_impl a) => Set a;
bot_set = set_empty (of_phantom (set_impl :: Phantom a Set_impla));

top_set :: forall a. (Ceq a, Ccompare a, Set_impl a) => Set a;
top_set = uminus_set bot_set;

le_of_comp :: forall a. (a -> a -> Ordera) -> a -> a -> Bool;
le_of_comp acomp x y = (case acomp x y of {
                         Eqa -> True;
                         Lt -> True;
                         Gt -> False;
                       });

lexordp_eq :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
lexordp_eq less (x : xs) (y : ys) =
  less x y || not (less y x) && lexordp_eq less xs ys;
lexordp_eq less (x : xs) [] = False;
lexordp_eq less xs [] = null xs;
lexordp_eq less [] ys = True;

finite :: forall a. (Finite_UNIV a, Ceq a, Ccompare a) => Set a -> Bool;
finite (Collect_set p) =
  of_phantom (finite_UNIV :: Phantom a Bool) ||
    (error :: forall a. String -> (() -> a) -> a) "finite Collect_set"
      (\ _ -> finite (Collect_set p));
finite (Set_Monad xs) = True;
finite (Complement a) =
  (if of_phantom (finite_UNIV :: Phantom a Bool) then True
    else (if finite a then False
           else (error :: forall a. String -> (() -> a) -> a)
                  "finite Complement: infinite set"
                  (\ _ -> finite (Complement a))));
finite (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "finite RBT_set: ccompare = None" (\ _ -> finite (RBT_set rbt));
    Just _ -> True;
  });
finite (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "finite DList_set: ceq = None" (\ _ -> finite (DList_set dxs));
    Just _ -> True;
  });

set_less_aux_Compl_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
set_less_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 set_less_aux_Compl_fusion less proper_interval
                                   g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
set_less_aux_Compl_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else proper_interval ao (Just y)));
                    })
               else proper_interval ao (Just x) ||
                      set_less_aux_Compl_fusion less proper_interval g1 g2
                        (Just x) s1a s2);
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (y, s2a) ->
                    proper_interval ao (Just y) ||
                      set_less_aux_Compl_fusion less proper_interval g1 g2
                        (Just y) s1 s2a;
                })
           else proper_interval ao Nothing));

compl_set_less_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
compl_set_less_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 &&
    has_next g2 s2 &&
      (case next g1 s1 of {
        (x, s1a) ->
          (case next g2 s2 of {
            (y, s2a) ->
              (if less x y
                then not (proper_interval ao (Just x)) &&
                       compl_set_less_aux_fusion less proper_interval g1 g2
                         (Just x) s1a s2
                else (if less y x
                       then not (proper_interval ao (Just y)) &&
                              compl_set_less_aux_fusion less proper_interval g1
                                g2 (Just y) s1 s2a
                       else not (proper_interval ao (Just y))));
          });
      });

set_less_aux_Compl ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
set_less_aux_Compl less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           set_less_aux_Compl less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  set_less_aux_Compl less proper_interval (Just y) (x : xs) ys
           else proper_interval ao (Just y)));
set_less_aux_Compl less proper_interval ao (x : xs) [] =
  proper_interval ao (Just x) ||
    set_less_aux_Compl less proper_interval (Just x) xs [];
set_less_aux_Compl less proper_interval ao [] (y : ys) =
  proper_interval ao (Just y) ||
    set_less_aux_Compl less proper_interval (Just y) [] ys;
set_less_aux_Compl less proper_interval ao [] [] = proper_interval ao Nothing;

compl_set_less_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
compl_set_less_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           compl_set_less_aux less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  compl_set_less_aux less proper_interval (Just y) (x : xs) ys
           else not (proper_interval ao (Just y))));
compl_set_less_aux less proper_interval ao xs [] = False;
compl_set_less_aux less proper_interval ao [] ys = False;

lexord_fusion ::
  forall a b c.
    (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
lexord_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then (case next g1 s1 of {
                  (x, s1a) ->
                    (case next g2 s2 of {
                      (y, s2a) ->
                        less x y ||
                          not (less y x) && lexord_fusion less g1 g2 s1a s2a;
                    });
                })
           else False)
    else has_next g2 s2);

lexordp :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
lexordp less (x : xs) (y : ys) =
  less x y || not (less y x) && lexordp less xs ys;
lexordp less xs [] = False;
lexordp less [] ys = not (null ys);

comp_of_ords ::
  forall a. (a -> a -> Bool) -> (a -> a -> Bool) -> a -> a -> Ordera;
comp_of_ords le lt x y = (if lt x y then Lt else (if le x y then Eqa else Gt));

ccompare_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Maybe (Set a -> Set a -> Ordera);
ccompare_set = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                 Nothing -> Nothing;
                 Just _ -> Just (comp_of_ords cless_eq_set cless_set);
               });

cless_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Set a -> Set a -> Bool;
cless_set (Complement (RBT_set rbt1)) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ -> cless_set (Complement (RBT_set rbt1)) (RBT_set rbt2));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        compl_set_less_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cless_set (RBT_set rbt1) (Complement (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set RBT_set (Complement RBT_set): ccompare = None"
        (\ _ -> cless_set (RBT_set rbt1) (Complement (RBT_set rbt2)));
    Just c ->
      (if (finite :: Set a -> Bool) (top_set :: Set a)
        then set_less_aux_Compl_fusion (lt_of_comp c) cproper_interval
               rbt_keys_generator rbt_keys_generator Nothing (init rbt1)
               (init rbt2)
        else True);
  });
cless_set (RBT_set rbta) (RBT_set rbt) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set RBT_set RBT_set: ccompare = None"
        (\ _ -> cless_set (RBT_set rbta) (RBT_set rbt));
    Just c ->
      lexord_fusion (\ x y -> lt_of_comp c y x) rbt_keys_generator
        rbt_keys_generator (init rbta) (init rbt);
  });
cless_set (Complement a) (Complement b) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement Complement: ccompare = None"
        (\ _ -> cless_set (Complement a) (Complement b));
    Just _ -> lt_of_comp (the ccompare_set) b a;
  });
cless_set (Complement a) b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement1: ccompare = None"
        (\ _ -> cless_set (Complement a) b);
    Just c ->
      (if finite a && finite b
        then (finite :: Set a -> Bool) (top_set :: Set a) &&
               compl_set_less_aux (lt_of_comp c) cproper_interval Nothing
                 (csorted_list_of_set a) (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set Complement1: infinite set"
               (\ _ -> cless_set (Complement a) b));
  });
cless_set a (Complement b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement2: ccompare = None"
        (\ _ -> cless_set a (Complement b));
    Just c ->
      (if finite a && finite b
        then (if (finite :: Set a -> Bool) (top_set :: Set a)
               then set_less_aux_Compl (lt_of_comp c) cproper_interval Nothing
                      (csorted_list_of_set a) (csorted_list_of_set b)
               else True)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set Complement2: infinite set"
               (\ _ -> cless_set a (Complement b)));
  });
cless_set a b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "cless_set: ccompare = None"
        (\ _ -> cless_set a b);
    Just c ->
      (if finite a && finite b
        then lexordp (\ x y -> lt_of_comp c y x) (csorted_list_of_set a)
               (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set: infinite set" (\ _ -> cless_set a b));
  });

cless_eq_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Set a -> Set a -> Bool;
cless_eq_set (Complement (RBT_set rbt1)) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ -> cless_eq_set (Complement (RBT_set rbt1)) (RBT_set rbt2));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        compl_set_less_eq_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cless_eq_set (RBT_set rbt1) (Complement (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set RBT_set (Complement RBT_set): ccompare = None"
        (\ _ -> cless_eq_set (RBT_set rbt1) (Complement (RBT_set rbt2)));
    Just c ->
      (if (finite :: Set a -> Bool) (top_set :: Set a)
        then set_less_eq_aux_Compl_fusion (lt_of_comp c) cproper_interval
               rbt_keys_generator rbt_keys_generator Nothing (init rbt1)
               (init rbt2)
        else True);
  });
cless_eq_set (RBT_set rbta) (RBT_set rbt) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set RBT_set RBT_set: ccompare = None"
        (\ _ -> cless_eq_set (RBT_set rbta) (RBT_set rbt));
    Just c ->
      lexord_eq_fusion (\ x y -> lt_of_comp c y x) rbt_keys_generator
        rbt_keys_generator (init rbta) (init rbt);
  });
cless_eq_set (Complement a) (Complement b) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement Complement: ccompare = None"
        (\ _ -> le_of_comp (the ccompare_set) (Complement a) (Complement b));
    Just _ -> cless_eq_set b a;
  });
cless_eq_set (Complement a) b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement1: ccompare = None"
        (\ _ -> cless_eq_set (Complement a) b);
    Just c ->
      (if finite a && finite b
        then (finite :: Set a -> Bool) (top_set :: Set a) &&
               compl_set_less_eq_aux (lt_of_comp c) cproper_interval Nothing
                 (csorted_list_of_set a) (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set Complement1: infinite set"
               (\ _ -> cless_eq_set (Complement a) b));
  });
cless_eq_set a (Complement b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement2: ccompare = None"
        (\ _ -> cless_eq_set a (Complement b));
    Just c ->
      (if finite a && finite b
        then (if (finite :: Set a -> Bool) (top_set :: Set a)
               then set_less_eq_aux_Compl (lt_of_comp c) cproper_interval
                      Nothing (csorted_list_of_set a) (csorted_list_of_set b)
               else True)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set Complement2: infinite set"
               (\ _ -> cless_eq_set a (Complement b)));
  });
cless_eq_set a b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set: ccompare = None" (\ _ -> cless_eq_set a b);
    Just c ->
      (if finite a && finite b
        then lexordp_eq (\ x y -> lt_of_comp c y x) (csorted_list_of_set a)
               (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set: infinite set" (\ _ -> cless_eq_set a b));
  });

instance (Finite_UNIV a, Ceq a, Cproper_interval a,
           Set_impl a) => Ccompare (Set a) where {
  ccompare = ccompare_set;
};

mapping_impl_set :: forall a. Phantom (Set a) Mapping_impla;
mapping_impl_set = Phantom Mapping_Choose;

instance Mapping_impl (Set a) where {
  mapping_impl = mapping_impl_set;
};

fold_fusion :: forall a b c. Generator a b -> (a -> c -> c) -> b -> c -> c;
fold_fusion g f s b =
  (if has_next g s then (case next g s of {
                          (x, sa) -> fold_fusion g f sa (f x b);
                        })
    else b);

length_last_fusion :: forall a b. Generator a b -> b -> (Nat, a);
length_last_fusion g s =
  (if has_next g s
    then (case next g s of {
           (x, sa) ->
             fold_fusion g (\ xa (n, _) -> (plus_nat n one_nat, xa)) sa
               (one_nat, x);
         })
    else (zero_nat, error "undefined"));

gen_length_fusion :: forall a b. Generator a b -> Nat -> b -> Nat;
gen_length_fusion g n s =
  (if has_next g s then gen_length_fusion g (suc n) (snd (next g s)) else n);

length_fusion :: forall a b. Generator a b -> b -> Nat;
length_fusion g = gen_length_fusion g zero_nat;

list_remdups :: forall a. (a -> a -> Bool) -> [a] -> [a];
list_remdups equal (x : xs) =
  (if list_member equal xs x then list_remdups equal xs
    else x : list_remdups equal xs);
list_remdups equal [] = [];

length :: forall a. (Ceq a) => Set_dlist a -> Nat;
length xa = size_list (list_of_dlist xa);

card :: forall a. (Card_UNIV a, Ceq a, Ccompare a) => Set a -> Nat;
card (Complement a) =
  let {
    aa = card a;
    s = of_phantom (card_UNIV :: Phantom a Nat);
  } in (if less_nat zero_nat s then minus_nat s aa
         else (if finite a then zero_nat
                else (error :: forall a. String -> (() -> a) -> a)
                       "card Complement: infinite"
                       (\ _ -> card (Complement a))));
card (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "card Set_Monad: ceq = None"
        (\ _ -> card (Set_Monad xs));
    Just eq -> size_list (list_remdups eq xs);
  });
card (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "card RBT_set: ccompare = None" (\ _ -> card (RBT_set rbt));
    Just _ -> size_list (keysb rbt);
  });
card (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "card DList_set: ceq = None"
        (\ _ -> card (DList_set dxs));
    Just _ -> length dxs;
  });

proper_interval_set_Compl_aux_fusion ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a,
      Set_impl a) => (a -> a -> Bool) ->
                       (Maybe a -> Maybe a -> Bool) ->
                         Generator a b ->
                           Generator a c -> Maybe a -> Nat -> b -> c -> Bool;
proper_interval_set_Compl_aux_fusion less proper_interval g1 g2 ao n s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 proper_interval_set_Compl_aux_fusion less
                                   proper_interval g1 g2 (Just x)
                                   (plus_nat n one_nat) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
proper_interval_set_Compl_aux_fusion less proper_interval g1 g2 (Just y)
  (plus_nat n one_nat) s1 s2a
                                 else proper_interval ao (Just x) &&
let {
  m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a)) n;
} in not (equal_nat (minus_nat m (length_fusion g2 s2a))
           (nat_of_integer (2 :: Integer))) ||
       not (equal_nat (minus_nat m (length_fusion g1 s1a))
             (nat_of_integer (2 :: Integer)))));
                    })
               else let {
                      m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a))
                            n;
                    } in (case length_last_fusion g1 s1 of {
                           (len_x, xa) ->
                             not (equal_nat m len_x) &&
                               (if equal_nat m (plus_nat len_x one_nat)
                                 then not (proper_interval (Just xa) Nothing)
                                 else True);
                         }));
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (_, _) ->
                    let {
                      m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a))
                            n;
                    } in (case length_last_fusion g2 s2 of {
                           (len_y, y) ->
                             not (equal_nat m len_y) &&
                               (if equal_nat m (plus_nat len_y one_nat)
                                 then not (proper_interval (Just y) Nothing)
                                 else True);
                         });
                })
           else less_nat (plus_nat n one_nat)
                  ((card :: Set a -> Nat) (top_set :: Set a))));

proper_interval_Compl_set_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
proper_interval_Compl_set_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 &&
    has_next g2 s2 &&
      (case next g1 s1 of {
        (x, s1a) ->
          (case next g2 s2 of {
            (y, s2a) ->
              (if less x y
                then not (proper_interval ao (Just x)) &&
                       proper_interval_Compl_set_aux_fusion less proper_interval
                         g1 g2 (Just x) s1a s2
                else (if less y x
                       then not (proper_interval ao (Just y)) &&
                              proper_interval_Compl_set_aux_fusion less
                                proper_interval g1 g2 (Just y) s1 s2a
                       else not (proper_interval ao (Just x)) &&
                              (has_next g2 s2a || has_next g1 s1a)));
          });
      });

exhaustive_above_fusion ::
  forall a b. (Maybe a -> Maybe a -> Bool) -> Generator a b -> a -> b -> Bool;
exhaustive_above_fusion proper_interval g y s =
  (if has_next g s
    then (case next g s of {
           (x, sa) ->
             not (proper_interval (Just y) (Just x)) &&
               exhaustive_above_fusion proper_interval g x sa;
         })
    else not (proper_interval (Just y) Nothing));

proper_interval_set_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> b -> c -> Bool;
proper_interval_set_aux_fusion less proper_interval g1 g2 s1 s2 =
  has_next g2 s2 &&
    (case next g2 s2 of {
      (y, s2a) ->
        (if has_next g1 s1
          then (case next g1 s1 of {
                 (x, s1a) ->
                   (if less x y then False
                     else (if less y x
                            then proper_interval (Just y) (Just x) ||
                                   (has_next g2 s2a ||
                                     not (exhaustive_above_fusion
   proper_interval g1 x s1a))
                            else proper_interval_set_aux_fusion less
                                   proper_interval g1 g2 s1a s2a));
               })
          else has_next g2 s2a || proper_interval (Just y) Nothing);
    });

length_last :: forall a. [a] -> (Nat, a);
length_last (x : xs) =
  fold (\ xa (n, _) -> (plus_nat n one_nat, xa)) xs (one_nat, x);
length_last [] = (zero_nat, error "undefined");

proper_interval_set_Compl_aux ::
  forall a.
    (Card_UNIV a, Ceq a, Ccompare a,
      Set_impl a) => (a -> a -> Bool) ->
                       (Maybe a -> Maybe a -> Bool) ->
                         Maybe a -> Nat -> [a] -> [a] -> Bool;
proper_interval_set_Compl_aux less proper_interval ao n (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           proper_interval_set_Compl_aux less proper_interval (Just x)
             (plus_nat n one_nat) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  proper_interval_set_Compl_aux less proper_interval (Just y)
                    (plus_nat n one_nat) (x : xs) ys
           else proper_interval ao (Just x) &&
                  let {
                    m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a)) n;
                  } in not (equal_nat (minus_nat m (size_list ys))
                             (nat_of_integer (2 :: Integer))) ||
                         not (equal_nat (minus_nat m (size_list xs))
                               (nat_of_integer (2 :: Integer)))));
proper_interval_set_Compl_aux less proper_interval ao n (x : xs) [] =
  let {
    m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a)) n;
  } in (case length_last (x : xs) of {
         (len_x, xa) ->
           not (equal_nat m len_x) &&
             (if equal_nat m (plus_nat len_x one_nat)
               then not (proper_interval (Just xa) Nothing) else True);
       });
proper_interval_set_Compl_aux less proper_interval ao n [] (y : ys) =
  let {
    m = minus_nat ((card :: Set a -> Nat) (top_set :: Set a)) n;
  } in (case length_last (y : ys) of {
         (len_y, ya) ->
           not (equal_nat m len_y) &&
             (if equal_nat m (plus_nat len_y one_nat)
               then not (proper_interval (Just ya) Nothing) else True);
       });
proper_interval_set_Compl_aux less proper_interval ao n [] [] =
  less_nat (plus_nat n one_nat) ((card :: Set a -> Nat) (top_set :: Set a));

proper_interval_Compl_set_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
proper_interval_Compl_set_aux less proper_interval ao uu [] = False;
proper_interval_Compl_set_aux less proper_interval ao [] uv = False;
proper_interval_Compl_set_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           proper_interval_Compl_set_aux less proper_interval (Just x) xs
             (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  proper_interval_Compl_set_aux less proper_interval (Just y)
                    (x : xs) ys
           else not (proper_interval ao (Just x)) &&
                  (if null ys then not (null xs) else True)));

exhaustive_above :: forall a. (Maybe a -> Maybe a -> Bool) -> a -> [a] -> Bool;
exhaustive_above proper_interval x (y : ys) =
  not (proper_interval (Just x) (Just y)) &&
    exhaustive_above proper_interval y ys;
exhaustive_above proper_interval x [] = not (proper_interval (Just x) Nothing);

proper_interval_set_aux ::
  forall a.
    (a -> a -> Bool) -> (Maybe a -> Maybe a -> Bool) -> [a] -> [a] -> Bool;
proper_interval_set_aux less proper_interval (x : xs) (y : ys) =
  (if less x y then False
    else (if less y x
           then proper_interval (Just y) (Just x) ||
                  (not (null ys) || not (exhaustive_above proper_interval x xs))
           else proper_interval_set_aux less proper_interval xs ys));
proper_interval_set_aux less proper_interval [] (y : ys) =
  not (null ys) || proper_interval (Just y) Nothing;
proper_interval_set_aux less proper_interval xs [] = False;

exhaustive_fusion ::
  forall a b. (Maybe a -> Maybe a -> Bool) -> Generator a b -> b -> Bool;
exhaustive_fusion proper_interval g s =
  has_next g s &&
    (case next g s of {
      (x, sa) ->
        not (proper_interval Nothing (Just x)) &&
          exhaustive_above_fusion proper_interval g x sa;
    });

is_UNIV :: forall a. (Card_UNIV a, Ceq a, Cproper_interval a) => Set a -> Bool;
is_UNIV (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_UNIV RBT_set: ccompare = None" (\ _ -> is_UNIV (RBT_set rbt));
    Just _ ->
      of_phantom (finite_UNIV :: Phantom a Bool) &&
        exhaustive_fusion cproper_interval rbt_keys_generator (init rbt);
  });
is_UNIV a =
  let {
    aa = of_phantom (card_UNIV :: Phantom a Nat);
    b = card a;
  } in (if less_nat zero_nat aa then equal_nat aa b
         else (if less_nat zero_nat b then False
                else (error :: forall a. String -> (() -> a) -> a)
                       "is_UNIV called on infinite type and set"
                       (\ _ -> is_UNIV a)));

is_emptya :: forall a b. (Ccompare a) => Mapping_rbt a b -> Bool;
is_emptya xa = (case impl_ofa xa of {
                 Empty -> True;
                 Branch _ _ _ _ _ -> False;
               });

nulla :: forall a. (Ceq a) => Set_dlist a -> Bool;
nulla xa = null (list_of_dlist xa);

is_empty :: forall a. (Card_UNIV a, Ceq a, Cproper_interval a) => Set a -> Bool;
is_empty (Complement a) = is_UNIV a;
is_empty (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_empty RBT_set: ccompare = None" (\ _ -> is_empty (RBT_set rbt));
    Just _ -> is_emptya rbt;
  });
is_empty (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_empty DList_set: ceq = None" (\ _ -> is_empty (DList_set dxs));
    Just _ -> nulla dxs;
  });
is_empty (Set_Monad xs) = null xs;

cproper_interval_set ::
  forall a.
    (Card_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Maybe (Set a) -> Maybe (Set a) -> Bool;
cproper_interval_set (Just (Complement (RBT_set rbt1))) (Just (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (Complement (RBT_set rbt1)))
            (Just (RBT_set rbt2)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_Compl_set_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cproper_interval_set (Just (RBT_set rbt1)) (Just (Complement (RBT_set rbt2))) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval RBT_set (Complement RBT_set): ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (RBT_set rbt1))
            (Just (Complement (RBT_set rbt2))));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_Compl_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing zero_nat (init rbt1)
          (init rbt2);
  });
cproper_interval_set (Just (RBT_set rbt1)) (Just (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval RBT_set RBT_set: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (RBT_set rbt1)) (Just (RBT_set rbt2)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator (init rbt1) (init rbt2);
  });
cproper_interval_set (Just (Complement a)) (Just (Complement b)) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement Complement: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (Complement a)) (Just (Complement b)));
    Just _ -> cproper_interval_set (Just b) (Just a);
  });
cproper_interval_set (Just (Complement a)) (Just b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement1: ccompare = None"
        (\ _ -> cproper_interval_set (Just (Complement a)) (Just b));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_Compl_set_aux (lt_of_comp c) cproper_interval Nothing
          (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) (Just (Complement b)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement2: ccompare = None"
        (\ _ -> cproper_interval_set (Just a) (Just (Complement b)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_Compl_aux (lt_of_comp c) cproper_interval Nothing
          zero_nat (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) (Just b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval: ccompare = None"
        (\ _ -> cproper_interval_set (Just a) (Just b));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_aux (lt_of_comp c) cproper_interval
          (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) Nothing = not (is_UNIV a);
cproper_interval_set Nothing (Just b) = not (is_empty b);
cproper_interval_set Nothing Nothing = True;

instance (Card_UNIV a, Ceq a, Cproper_interval a,
           Set_impl a) => Cproper_interval (Set a) where {
  cproper_interval = cproper_interval_set;
};

less_eq_bool :: Bool -> Bool -> Bool;
less_eq_bool True b = b;
less_eq_bool False b = True;

less_bool :: Bool -> Bool -> Bool;
less_bool True b = False;
less_bool False b = b;

instance Ord Bool where {
  less_eq = less_eq_bool;
  less = less_bool;
};

instance Preorder Bool where {
};

instance Order Bool where {
};

ceq_bool :: Maybe (Bool -> Bool -> Bool);
ceq_bool = Just (\ a b -> a == b);

instance Ceq Bool where {
  ceq = ceq_bool;
};

set_impl_bool :: Phantom Bool Set_impla;
set_impl_bool = Phantom Set_DList;

instance Set_impl Bool where {
  set_impl = set_impl_bool;
};

instance Linorder Bool where {
};

finite_UNIV_bool :: Phantom Bool Bool;
finite_UNIV_bool = Phantom True;

card_UNIV_bool :: Phantom Bool Nat;
card_UNIV_bool = Phantom (nat_of_integer (2 :: Integer));

instance Finite_UNIV Bool where {
  finite_UNIV = finite_UNIV_bool;
};

instance Card_UNIV Bool where {
  card_UNIV = card_UNIV_bool;
};

enum_all_bool :: (Bool -> Bool) -> Bool;
enum_all_bool p = p False && p True;

enum_ex_bool :: (Bool -> Bool) -> Bool;
enum_ex_bool p = p False || p True;

enum_bool :: [Bool];
enum_bool = [False, True];

cEnum_bool :: Maybe ([Bool], ((Bool -> Bool) -> Bool, (Bool -> Bool) -> Bool));
cEnum_bool = Just (enum_bool, (enum_all_bool, enum_ex_bool));

instance Cenum Bool where {
  cEnum = cEnum_bool;
};

comparator_bool :: Bool -> Bool -> Ordera;
comparator_bool False False = Eqa;
comparator_bool False True = Lt;
comparator_bool True True = Eqa;
comparator_bool True False = Gt;

compare_bool :: Bool -> Bool -> Ordera;
compare_bool = comparator_bool;

ccompare_bool :: Maybe (Bool -> Bool -> Ordera);
ccompare_bool = Just compare_bool;

instance Ccompare Bool where {
  ccompare = ccompare_bool;
};

mapping_impl_bool :: Phantom Bool Mapping_impla;
mapping_impl_bool = Phantom Mapping_Assoc_List;

instance Mapping_impl Bool where {
  mapping_impl = mapping_impl_bool;
};

proper_interval_bool :: Maybe Bool -> Maybe Bool -> Bool;
proper_interval_bool (Just x) (Just y) = False;
proper_interval_bool (Just x) Nothing = not x;
proper_interval_bool Nothing (Just y) = y;
proper_interval_bool Nothing Nothing = True;

cproper_interval_bool :: Maybe Bool -> Maybe Bool -> Bool;
cproper_interval_bool = proper_interval_bool;

instance Cproper_interval Bool where {
  cproper_interval = cproper_interval_bool;
};

newtype Fset a = Abs_fset (Set a);

fset :: forall a. Fset a -> Set a;
fset (Abs_fset x) = x;

less_eq_fset ::
  forall a. (Cenum a, Ceq a, Ccompare a) => Fset a -> Fset a -> Bool;
less_eq_fset xa xc = less_eq_set (fset xa) (fset xc);

equal_fset ::
  forall a. (Cenum a, Ceq a, Ccompare a, Eq a) => Fset a -> Fset a -> Bool;
equal_fset a b = less_eq_fset a b && less_eq_fset b a;

instance (Cenum a, Ceq a, Ccompare a, Eq a) => Eq (Fset a) where {
  a == b = equal_fset a b;
};

ceq_fset ::
  forall a.
    (Cenum a, Ceq a, Ccompare a, Eq a) => Maybe (Fset a -> Fset a -> Bool);
ceq_fset = Just (\ a b -> a == b);

instance (Cenum a, Ceq a, Ccompare a, Eq a) => Ceq (Fset a) where {
  ceq = ceq_fset;
};

set_impl_fset :: forall a. Phantom (Fset a) Set_impla;
set_impl_fset = Phantom Set_DList;

instance Set_impl (Fset a) where {
  set_impl = set_impl_fset;
};

finite_UNIV_fset :: forall a. (Finite_UNIV a) => Phantom (Fset a) Bool;
finite_UNIV_fset = Phantom (of_phantom (finite_UNIV :: Phantom a Bool));

card_UNIV_fset :: forall a. (Card_UNIV a) => Phantom (Fset a) Nat;
card_UNIV_fset =
  Phantom
    (let {
       c = of_phantom (card_UNIV :: Phantom a Nat);
     } in (if equal_nat c zero_nat then zero_nat
            else power (nat_of_integer (2 :: Integer)) c));

instance (Finite_UNIV a) => Finite_UNIV (Fset a) where {
  finite_UNIV = finite_UNIV_fset;
};

instance (Card_UNIV a) => Card_UNIV (Fset a) where {
  card_UNIV = card_UNIV_fset;
};

cEnum_fset ::
  forall a.
    Maybe ([Fset a], ((Fset a -> Bool) -> Bool, (Fset a -> Bool) -> Bool));
cEnum_fset = Nothing;

instance Cenum (Fset a) where {
  cEnum = cEnum_fset;
};

ccompare_fset :: forall a. Maybe (Fset a -> Fset a -> Ordera);
ccompare_fset = Nothing;

instance Ccompare (Fset a) where {
  ccompare = ccompare_fset;
};

mapping_impl_fset :: forall a. Phantom (Fset a) Mapping_impla;
mapping_impl_fset = Phantom Mapping_Choose;

instance Mapping_impl (Fset a) where {
  mapping_impl = mapping_impl_fset;
};

class Infinite_UNIV a where {
};

cproper_interval_fset ::
  forall a. (Infinite_UNIV a) => Maybe (Fset a) -> Maybe (Fset a) -> Bool;
cproper_interval_fset uu uv = error "undefined";

instance (Infinite_UNIV a) => Cproper_interval (Fset a) where {
  cproper_interval = cproper_interval_fset;
};

less_eq_list :: forall a. (Eq a, Order a) => [a] -> [a] -> Bool;
less_eq_list (x : xs) (y : ys) = less x y || x == y && less_eq_list xs ys;
less_eq_list [] xs = True;
less_eq_list (x : xs) [] = False;

less_list :: forall a. (Eq a, Order a) => [a] -> [a] -> Bool;
less_list (x : xs) (y : ys) = less x y || x == y && less_list xs ys;
less_list [] (x : xs) = True;
less_list xs [] = False;

instance (Eq a, Order a) => Ord [a] where {
  less_eq = less_eq_list;
  less = less_list;
};

instance (Eq a, Order a) => Preorder [a] where {
};

instance (Eq a, Order a) => Order [a] where {
};

equality_list :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
equality_list eq_a (x : xa) (y : ya) = eq_a x y && equality_list eq_a xa ya;
equality_list eq_a (x : xa) [] = False;
equality_list eq_a [] (y : ya) = False;
equality_list eq_a [] [] = True;

ceq_list :: forall a. (Ceq a) => Maybe ([a] -> [a] -> Bool);
ceq_list = (case ceq of {
             Nothing -> Nothing;
             Just eq_a -> Just (equality_list eq_a);
           });

instance (Ceq a) => Ceq [a] where {
  ceq = ceq_list;
};

set_impl_list :: forall a. Phantom [a] Set_impla;
set_impl_list = Phantom Set_Choose;

instance Set_impl [a] where {
  set_impl = set_impl_list;
};

instance (Eq a, Linorder a) => Linorder [a] where {
};

finite_UNIV_list :: forall a. Phantom [a] Bool;
finite_UNIV_list = Phantom False;

card_UNIV_list :: forall a. Phantom [a] Nat;
card_UNIV_list = Phantom zero_nat;

instance Finite_UNIV [a] where {
  finite_UNIV = finite_UNIV_list;
};

instance Card_UNIV [a] where {
  card_UNIV = card_UNIV_list;
};

cEnum_list ::
  forall a. Maybe ([[a]], (([a] -> Bool) -> Bool, ([a] -> Bool) -> Bool));
cEnum_list = Nothing;

instance Cenum [a] where {
  cEnum = cEnum_list;
};

comparator_list :: forall a. (a -> a -> Ordera) -> [a] -> [a] -> Ordera;
comparator_list comp_a (x : xa) (y : ya) =
  (case comp_a x y of {
    Eqa -> comparator_list comp_a xa ya;
    Lt -> Lt;
    Gt -> Gt;
  });
comparator_list comp_a (x : xa) [] = Gt;
comparator_list comp_a [] (y : ya) = Lt;
comparator_list comp_a [] [] = Eqa;

ccompare_list :: forall a. (Ccompare a) => Maybe ([a] -> [a] -> Ordera);
ccompare_list = (case ccompare of {
                  Nothing -> Nothing;
                  Just comp_a -> Just (comparator_list comp_a);
                });

instance (Ccompare a) => Ccompare [a] where {
  ccompare = ccompare_list;
};

mapping_impl_list :: forall a. Phantom [a] Mapping_impla;
mapping_impl_list = Phantom Mapping_Choose;

instance Mapping_impl [a] where {
  mapping_impl = mapping_impl_list;
};

cproper_interval_list ::
  forall a. (Ccompare a) => Maybe [a] -> Maybe [a] -> Bool;
cproper_interval_list xso yso = error "undefined";

instance (Ccompare a) => Cproper_interval [a] where {
  cproper_interval = cproper_interval_list;
};

data Sum a b = Inl a | Inr b;

equal_sum :: forall a b. (Eq a, Eq b) => Sum a b -> Sum a b -> Bool;
equal_sum (Inl x1) (Inr x2) = False;
equal_sum (Inr x2) (Inl x1) = False;
equal_sum (Inr x2) (Inr y2) = x2 == y2;
equal_sum (Inl x1) (Inl y1) = x1 == y1;

instance (Eq a, Eq b) => Eq (Sum a b) where {
  a == b = equal_sum a b;
};

less_eq_sum :: forall a b. (Ord a, Ord b) => Sum a b -> Sum a b -> Bool;
less_eq_sum (Inl a) (Inl b) = less_eq a b;
less_eq_sum (Inl a) (Inr b) = True;
less_eq_sum (Inr a) (Inl b) = False;
less_eq_sum (Inr a) (Inr b) = less_eq a b;

less_sum ::
  forall a b. (Eq a, Ord a, Eq b, Ord b) => Sum a b -> Sum a b -> Bool;
less_sum a b = less_eq_sum a b && not (equal_sum a b);

instance (Eq a, Ord a, Eq b, Ord b) => Ord (Sum a b) where {
  less_eq = less_eq_sum;
  less = less_sum;
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Preorder (Sum a b) where {
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Order (Sum a b) where {
};

equality_sum ::
  forall a b.
    (a -> a -> Bool) -> (b -> b -> Bool) -> Sum a b -> Sum a b -> Bool;
equality_sum eq_a eq_b (Inr x) (Inr ya) = eq_b x ya;
equality_sum eq_a eq_b (Inr x) (Inl y) = False;
equality_sum eq_a eq_b (Inl x) (Inr ya) = False;
equality_sum eq_a eq_b (Inl x) (Inl y) = eq_a x y;

ceq_sum :: forall a b. (Ceq a, Ceq b) => Maybe (Sum a b -> Sum a b -> Bool);
ceq_sum = (case ceq of {
            Nothing -> Nothing;
            Just eq_a -> (case ceq of {
                           Nothing -> Nothing;
                           Just eq_b -> Just (equality_sum eq_a eq_b);
                         });
          });

instance (Ceq a, Ceq b) => Ceq (Sum a b) where {
  ceq = ceq_sum;
};

set_impl_choose2 :: Set_impla -> Set_impla -> Set_impla;
set_impl_choose2 Set_Monada Set_Monada = Set_Monada;
set_impl_choose2 Set_RBT Set_RBT = Set_RBT;
set_impl_choose2 Set_DList Set_DList = Set_DList;
set_impl_choose2 Set_Collect Set_Collect = Set_Collect;
set_impl_choose2 x y = Set_Choose;

set_impl_sum ::
  forall a b. (Set_impl a, Set_impl b) => Phantom (Sum a b) Set_impla;
set_impl_sum =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl :: Phantom a Set_impla))
      (of_phantom (set_impl :: Phantom b Set_impla)));

instance (Set_impl a, Set_impl b) => Set_impl (Sum a b) where {
  set_impl = set_impl_sum;
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Linorder (Sum a b) where {
};

finite_UNIV_sum ::
  forall a b. (Finite_UNIV a, Finite_UNIV b) => Phantom (Sum a b) Bool;
finite_UNIV_sum =
  Phantom
    (of_phantom (finite_UNIV :: Phantom a Bool) &&
      of_phantom (finite_UNIV :: Phantom b Bool));

card_UNIV_sum ::
  forall a b. (Card_UNIV a, Card_UNIV b) => Phantom (Sum a b) Nat;
card_UNIV_sum =
  Phantom
    (let {
       ca = of_phantom (card_UNIV :: Phantom a Nat);
       cb = of_phantom (card_UNIV :: Phantom b Nat);
     } in (if not (equal_nat ca zero_nat) && not (equal_nat cb zero_nat)
            then plus_nat ca cb else zero_nat));

instance (Finite_UNIV a, Finite_UNIV b) => Finite_UNIV (Sum a b) where {
  finite_UNIV = finite_UNIV_sum;
};

instance (Card_UNIV a, Card_UNIV b) => Card_UNIV (Sum a b) where {
  card_UNIV = card_UNIV_sum;
};

cEnum_sum ::
  forall a b.
    (Cenum a,
      Cenum b) => Maybe ([Sum a b],
                          ((Sum a b -> Bool) -> Bool,
                            (Sum a b -> Bool) -> Bool));
cEnum_sum =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (enum_all_a, enum_ex_a)) ->
      (case cEnum of {
        Nothing -> Nothing;
        Just (enum_b, (enum_all_b, enum_ex_b)) ->
          Just (map Inl enum_a ++ map Inr enum_b,
                 ((\ p ->
                    enum_all_a (\ x -> p (Inl x)) &&
                      enum_all_b (\ x -> p (Inr x))),
                   (\ p ->
                     enum_ex_a (\ x -> p (Inl x)) ||
                       enum_ex_b (\ x -> p (Inr x)))));
      });
  });

instance (Cenum a, Cenum b) => Cenum (Sum a b) where {
  cEnum = cEnum_sum;
};

comparator_sum ::
  forall a b.
    (a -> a -> Ordera) -> (b -> b -> Ordera) -> Sum a b -> Sum a b -> Ordera;
comparator_sum comp_a comp_b (Inr x) (Inr ya) = comp_b x ya;
comparator_sum comp_a comp_b (Inr x) (Inl y) = Gt;
comparator_sum comp_a comp_b (Inl x) (Inr ya) = Lt;
comparator_sum comp_a comp_b (Inl x) (Inl y) = comp_a x y;

ccompare_sum ::
  forall a b. (Ccompare a, Ccompare b) => Maybe (Sum a b -> Sum a b -> Ordera);
ccompare_sum =
  (case ccompare of {
    Nothing -> Nothing;
    Just comp_a -> (case ccompare of {
                     Nothing -> Nothing;
                     Just comp_b -> Just (comparator_sum comp_a comp_b);
                   });
  });

instance (Ccompare a, Ccompare b) => Ccompare (Sum a b) where {
  ccompare = ccompare_sum;
};

mapping_impl_choose2 :: Mapping_impla -> Mapping_impla -> Mapping_impla;
mapping_impl_choose2 Mapping_RBT Mapping_RBT = Mapping_RBT;
mapping_impl_choose2 Mapping_Assoc_List Mapping_Assoc_List = Mapping_Assoc_List;
mapping_impl_choose2 Mapping_Mapping Mapping_Mapping = Mapping_Mapping;
mapping_impl_choose2 x y = Mapping_Choose;

mapping_impl_sum ::
  forall a b.
    (Mapping_impl a, Mapping_impl b) => Phantom (Sum a b) Mapping_impla;
mapping_impl_sum =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl :: Phantom a Mapping_impla))
      (of_phantom (mapping_impl :: Phantom b Mapping_impla)));

instance (Mapping_impl a, Mapping_impl b) => Mapping_impl (Sum a b) where {
  mapping_impl = mapping_impl_sum;
};

cproper_interval_sum ::
  forall a b.
    (Cproper_interval a,
      Cproper_interval b) => Maybe (Sum a b) -> Maybe (Sum a b) -> Bool;
cproper_interval_sum Nothing Nothing = True;
cproper_interval_sum Nothing (Just (Inl x)) = cproper_interval Nothing (Just x);
cproper_interval_sum Nothing (Just (Inr y)) = True;
cproper_interval_sum (Just (Inl x)) Nothing = True;
cproper_interval_sum (Just (Inl x)) (Just (Inl y)) =
  cproper_interval (Just x) (Just y);
cproper_interval_sum (Just (Inl x)) (Just (Inr y)) =
  cproper_interval (Just x) Nothing || cproper_interval Nothing (Just y);
cproper_interval_sum (Just (Inr y)) Nothing = cproper_interval (Just y) Nothing;
cproper_interval_sum (Just (Inr y)) (Just (Inl x)) = False;
cproper_interval_sum (Just (Inr x)) (Just (Inr y)) =
  cproper_interval (Just x) (Just y);

instance (Cproper_interval a,
           Cproper_interval b) => Cproper_interval (Sum a b) where {
  cproper_interval = cproper_interval_sum;
};

equality_option :: forall a. (a -> a -> Bool) -> Maybe a -> Maybe a -> Bool;
equality_option eq_a (Just x) (Just y) = eq_a x y;
equality_option eq_a (Just x) Nothing = False;
equality_option eq_a Nothing (Just y) = False;
equality_option eq_a Nothing Nothing = True;

ceq_option :: forall a. (Ceq a) => Maybe (Maybe a -> Maybe a -> Bool);
ceq_option = (case ceq of {
               Nothing -> Nothing;
               Just eq_a -> Just (equality_option eq_a);
             });

instance (Ceq a) => Ceq (Maybe a) where {
  ceq = ceq_option;
};

set_impl_option :: forall a. (Set_impl a) => Phantom (Maybe a) Set_impla;
set_impl_option = Phantom (of_phantom (set_impl :: Phantom a Set_impla));

instance (Set_impl a) => Set_impl (Maybe a) where {
  set_impl = set_impl_option;
};

comparator_option ::
  forall a. (a -> a -> Ordera) -> Maybe a -> Maybe a -> Ordera;
comparator_option comp_a (Just x) (Just y) = comp_a x y;
comparator_option comp_a (Just x) Nothing = Gt;
comparator_option comp_a Nothing (Just y) = Lt;
comparator_option comp_a Nothing Nothing = Eqa;

ccompare_option ::
  forall a. (Ccompare a) => Maybe (Maybe a -> Maybe a -> Ordera);
ccompare_option = (case ccompare of {
                    Nothing -> Nothing;
                    Just comp_a -> Just (comparator_option comp_a);
                  });

instance (Ccompare a) => Ccompare (Maybe a) where {
  ccompare = ccompare_option;
};

less_eq_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_eq_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less_eq y1 y2;

less_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less y1 y2;

instance (Ord a, Ord b) => Ord (a, b) where {
  less_eq = less_eq_prod;
  less = less_prod;
};

instance (Preorder a, Preorder b) => Preorder (a, b) where {
};

instance (Order a, Order b) => Order (a, b) where {
};

set_impl_prod ::
  forall a b. (Set_impl a, Set_impl b) => Phantom (a, b) Set_impla;
set_impl_prod =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl :: Phantom a Set_impla))
      (of_phantom (set_impl :: Phantom b Set_impla)));

instance (Set_impl a, Set_impl b) => Set_impl (a, b) where {
  set_impl = set_impl_prod;
};

instance (Linorder a, Linorder b) => Linorder (a, b) where {
};

finite_UNIV_prod ::
  forall a b. (Finite_UNIV a, Finite_UNIV b) => Phantom (a, b) Bool;
finite_UNIV_prod =
  Phantom
    (of_phantom (finite_UNIV :: Phantom a Bool) &&
      of_phantom (finite_UNIV :: Phantom b Bool));

card_UNIV_prod :: forall a b. (Card_UNIV a, Card_UNIV b) => Phantom (a, b) Nat;
card_UNIV_prod =
  Phantom
    (times_nat (of_phantom (card_UNIV :: Phantom a Nat))
      (of_phantom (card_UNIV :: Phantom b Nat)));

instance (Finite_UNIV a, Finite_UNIV b) => Finite_UNIV (a, b) where {
  finite_UNIV = finite_UNIV_prod;
};

instance (Card_UNIV a, Card_UNIV b) => Card_UNIV (a, b) where {
  card_UNIV = card_UNIV_prod;
};

mapping_impl_prod ::
  forall a b. (Mapping_impl a, Mapping_impl b) => Phantom (a, b) Mapping_impla;
mapping_impl_prod =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl :: Phantom a Mapping_impla))
      (of_phantom (mapping_impl :: Phantom b Mapping_impla)));

instance (Mapping_impl a, Mapping_impl b) => Mapping_impl (a, b) where {
  mapping_impl = mapping_impl_prod;
};

cproper_interval_prod ::
  forall a b.
    (Cproper_interval a,
      Cproper_interval b) => Maybe (a, b) -> Maybe (a, b) -> Bool;
cproper_interval_prod Nothing Nothing = True;
cproper_interval_prod Nothing (Just (y1, y2)) =
  cproper_interval Nothing (Just y1) || cproper_interval Nothing (Just y2);
cproper_interval_prod (Just (x1, x2)) Nothing =
  cproper_interval (Just x1) Nothing || cproper_interval (Just x2) Nothing;
cproper_interval_prod (Just (x1, x2)) (Just (y1, y2)) =
  cproper_interval (Just x1) (Just y1) ||
    (lt_of_comp (the ccompare) x1 y1 &&
       (cproper_interval (Just x2) Nothing ||
         cproper_interval Nothing (Just y2)) ||
      not (lt_of_comp (the ccompare) y1 x1) &&
        cproper_interval (Just x2) (Just y2));

instance (Cproper_interval a,
           Cproper_interval b) => Cproper_interval (a, b) where {
  cproper_interval = cproper_interval_prod;
};

instance Preorder Integer where {
};

instance Order Integer where {
};

ceq_integer :: Maybe (Integer -> Integer -> Bool);
ceq_integer = Just (\ a b -> a == b);

instance Ceq Integer where {
  ceq = ceq_integer;
};

set_impl_integer :: Phantom Integer Set_impla;
set_impl_integer = Phantom Set_RBT;

instance Set_impl Integer where {
  set_impl = set_impl_integer;
};

instance Linorder Integer where {
};

finite_UNIV_integer :: Phantom Integer Bool;
finite_UNIV_integer = Phantom False;

card_UNIV_integer :: Phantom Integer Nat;
card_UNIV_integer = Phantom zero_nat;

instance Finite_UNIV Integer where {
  finite_UNIV = finite_UNIV_integer;
};

instance Card_UNIV Integer where {
  card_UNIV = card_UNIV_integer;
};

cEnum_integer ::
  Maybe ([Integer], ((Integer -> Bool) -> Bool, (Integer -> Bool) -> Bool));
cEnum_integer = Nothing;

instance Cenum Integer where {
  cEnum = cEnum_integer;
};

compare_integer :: Integer -> Integer -> Ordera;
compare_integer = comparator_of;

ccompare_integer :: Maybe (Integer -> Integer -> Ordera);
ccompare_integer = Just compare_integer;

instance Ccompare Integer where {
  ccompare = ccompare_integer;
};

mapping_impl_integer :: Phantom Integer Mapping_impla;
mapping_impl_integer = Phantom Mapping_RBT;

instance Mapping_impl Integer where {
  mapping_impl = mapping_impl_integer;
};

proper_interval_integer :: Maybe Integer -> Maybe Integer -> Bool;
proper_interval_integer xo Nothing = True;
proper_interval_integer Nothing yo = True;
proper_interval_integer (Just x) (Just y) = (1 :: Integer) < y - x;

cproper_interval_integer :: Maybe Integer -> Maybe Integer -> Bool;
cproper_interval_integer = proper_interval_integer;

instance Cproper_interval Integer where {
  cproper_interval = cproper_interval_integer;
};

instance Infinite_UNIV Integer where {
};

newtype Int = Int_of_integer Integer;

data Test_suite a b c d =
  Test_Suite (Set (a, Fsm a b c)) (a -> Set [(a, (b, (c, a)))])
    ((a, [(a, (b, (c, a)))]) -> Set a) ((a, a) -> Set (Fsm d b c, (d, d)));

newtype Comp_fun_idem b a = Abs_comp_fun_idem (b -> a -> a);

newtype Prefix_tree a = MPT (Mapping a (Prefix_tree a));

newtype Mp_trie a = MP_Trie [(a, Mp_trie a)];

impl_of :: forall b a. Alist b a -> [(b, a)];
impl_of (Alist x) = x;

update :: forall a b. (Eq a) => a -> b -> [(a, b)] -> [(a, b)];
update k v [] = [(k, v)];
update k v (p : ps) = (if fst p == k then (k, v) : ps else p : update k v ps);

updatea :: forall a b. (Eq a) => a -> b -> Alist a b -> Alist a b;
updatea xc xd xe = Alist (update xc xd (impl_of xe));

fun_upd :: forall a b. (Eq a) => (a -> b) -> a -> b -> a -> b;
fun_upd f a b = (\ x -> (if x == a then b else f x));

updateb ::
  forall a b. (Ccompare a, Eq a) => a -> b -> Mapping a b -> Mapping a b;
updateb k v (RBT_Mapping t) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "update RBT_Mapping: ccompare = None"
        (\ _ -> updateb k v (RBT_Mapping t));
    Just _ -> RBT_Mapping (insertc k v t);
  });
updateb k v (Assoc_List_Mapping al) = Assoc_List_Mapping (updatea k v al);
updateb k v (Mapping m) = Mapping (fun_upd m k (Just v));

map_of :: forall a b. (Eq a) => [(a, b)] -> a -> Maybe b;
map_of ((l, v) : ps) k = (if l == k then Just v else map_of ps k);
map_of [] k = Nothing;

lookup :: forall a b. (Eq a) => Alist a b -> a -> Maybe b;
lookup xa = map_of (impl_of xa);

lookupa :: forall a b. (Ccompare a, Eq a) => Mapping a b -> a -> Maybe b;
lookupa (RBT_Mapping t) = lookupb t;
lookupa (Assoc_List_Mapping al) = lookup al;

foldb ::
  forall a b. (Ccompare a) => (a -> b -> b) -> Mapping_rbt a () -> b -> b;
foldb x xc = folda (\ a _ -> x a) (impl_ofa xc);

empty :: forall a b. Alist a b;
empty = Alist [];

mapping_empty_choose :: forall a b. (Ccompare a) => Mapping a b;
mapping_empty_choose = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                         Nothing -> Assoc_List_Mapping empty;
                         Just _ -> RBT_Mapping emptyd;
                       });

mapping_empty :: forall a b. (Ccompare a) => Mapping_impla -> Mapping a b;
mapping_empty Mapping_RBT = RBT_Mapping emptyd;
mapping_empty Mapping_Assoc_List = Assoc_List_Mapping empty;
mapping_empty Mapping_Mapping = Mapping (\ _ -> Nothing);
mapping_empty Mapping_Choose = mapping_empty_choose;

emptya :: forall a b. (Ccompare a, Mapping_impl a) => Mapping a b;
emptya = mapping_empty (of_phantom (mapping_impl :: Phantom a Mapping_impla));

set_as_map_image ::
  forall a b c d.
    (Ccompare a, Ccompare b, Ccompare c, Eq c, Mapping_impl c, Ceq d,
      Ccompare d,
      Set_impl d) => Set (a, b) -> ((a, b) -> (c, d)) -> c -> Maybe (Set d);
set_as_map_image (RBT_set t) f1 =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map_image RBT_set: ccompare = None"
        (\ _ -> set_as_map_image (RBT_set t) f1);
    Just _ ->
      lookupa
        (foldb
          (\ kv m1 ->
            (case f1 kv of {
              (x, z) -> (case lookupa m1 x of {
                          Nothing -> updateb x (insert z bot_set) m1;
                          Just zs -> updateb x (insert z zs) m1;
                        });
            }))
          t emptya);
  });

h :: forall a b c.
       (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ccompare b, Eq b,
         Mapping_impl b, Ceq c, Ccompare c,
         Set_impl c) => Fsm a b c -> (a, b) -> Set (c, a);
h m (q, x) =
  let {
    ma = set_as_map_image (transitions m)
           (\ (qb, (xa, (y, qa))) -> ((qb, xa), (y, qa)));
  } in (case ma (q, x) of {
         Nothing -> bot_set;
         Just yqs -> yqs;
       });

dlist_ex :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Bool;
dlist_ex x xc = any x (list_of_dlist xc);

rBT_Impl_rbt_ex :: forall a b. (a -> b -> Bool) -> Rbt a b -> Bool;
rBT_Impl_rbt_ex p (Branch c l k v r) =
  p k v || (rBT_Impl_rbt_ex p l || rBT_Impl_rbt_ex p r);
rBT_Impl_rbt_ex p Empty = False;

ex :: forall a b. (Ccompare a) => (a -> b -> Bool) -> Mapping_rbt a b -> Bool;
ex xb xc = rBT_Impl_rbt_ex xb (impl_ofa xc);

bex :: forall a. (Ceq a, Ccompare a) => Set a -> (a -> Bool) -> Bool;
bex (RBT_set rbt) p =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "Bex RBT_set: ccompare = None" (\ _ -> bex (RBT_set rbt) p);
    Just _ -> ex (\ k _ -> p k) rbt;
  });
bex (DList_set dxs) p =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "Bex DList_set: ceq = None"
        (\ _ -> bex (DList_set dxs) p);
    Just _ -> dlist_ex p dxs;
  });
bex (Set_Monad xs) p = any p xs;

size :: forall a b c. (Card_UNIV a, Ceq a, Ccompare a) => Fsm a b c -> Nat;
size m = card (states m);

nth :: forall a. [a] -> Nat -> a;
nth (x : xs) n =
  (if equal_nat n zero_nat then x else nth xs (minus_nat n one_nat));

upt :: Nat -> Nat -> [Nat];
upt i j = (if less_nat i j then i : upt (suc i) j else []);

rBT_Impl_rbt_all :: forall a b. (a -> b -> Bool) -> Rbt a b -> Bool;
rBT_Impl_rbt_all p (Branch c l k v r) =
  p k v && rBT_Impl_rbt_all p l && rBT_Impl_rbt_all p r;
rBT_Impl_rbt_all p Empty = True;

alla :: forall a b. (Ccompare a) => (a -> b -> Bool) -> Mapping_rbt a b -> Bool;
alla xb xc = rBT_Impl_rbt_all xb (impl_ofa xc);

ball :: forall a. (Ceq a, Ccompare a) => Set a -> (a -> Bool) -> Bool;
ball (RBT_set rbt) p =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "Ball RBT_set: ccompare = None" (\ _ -> ball (RBT_set rbt) p);
    Just _ -> alla (\ k _ -> p k) rbt;
  });
ball (DList_set dxs) p =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "Ball DList_set: ceq = None"
        (\ _ -> ball (DList_set dxs) p);
    Just _ -> dlist_all p dxs;
  });
ball (Set_Monad xs) p = all p xs;

h_obs_wpi ::
  forall a b c.
    Fsm_with_precomputations_impl a b c -> Mapping (a, b) (Mapping c a);
h_obs_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x7;

h_obs_wp ::
  forall a b c. Fsm_with_precomputations a b c -> Mapping (a, b) (Mapping c a);
h_obs_wp x =
  h_obs_wpi (fsm_with_precomputations_impl_of_fsm_with_precomputations x);

h_obsa ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm_impl a b c -> a -> b -> c -> Maybe a;
h_obsa (FSMWP m) q x y = (case lookupa (h_obs_wp m) (q, x) of {
                           Nothing -> Nothing;
                           Just ma -> lookupa ma y;
                         });

h_obs ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm a b c -> a -> b -> c -> Maybe a;
h_obs x = h_obsa (fsm_impl_of_fsm x);

after ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm a b c -> a -> [(b, c)] -> a;
after m q [] = q;
after m q ((x, y) : io) = after m (the (h_obs m q x y)) io;

drop :: forall a. Nat -> [a] -> [a];
drop n [] = [];
drop n (x : xs) =
  (if equal_nat n zero_nat then x : xs else drop (minus_nat n one_nat) xs);

find :: forall a. (a -> Bool) -> [a] -> Maybe a;
find uu [] = Nothing;
find p (x : xs) = (if p x then Just x else find p xs);

last :: forall a. [a] -> a;
last (x : xs) = (if null xs then x else last xs);

take :: forall a. Nat -> [a] -> [a];
take n [] = [];
take n (x : xs) =
  (if equal_nat n zero_nat then [] else x : take (minus_nat n one_nat) xs);

image ::
  forall a b.
    (Ceq a, Ccompare a, Ceq b, Ccompare b,
      Set_impl b) => (a -> b) -> Set a -> Set b;
image h (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "image RBT_set: ccompare = None" (\ _ -> image h (RBT_set rbt));
    Just _ -> foldb (insert . h) rbt bot_set;
  });
image g (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "image DList_set: ceq = None" (\ _ -> image g (DList_set dxs));
    Just _ -> foldc (insert . g) dxs bot_set;
  });
image f (Complement (Complement b)) = image f b;
image f (Collect_set a) =
  (error :: forall a. String -> (() -> a) -> a) "image Collect_set"
    (\ _ -> image f (Collect_set a));
image f (Set_Monad xs) = Set_Monad (map f xs);

set_as_map ::
  forall a b.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Ceq b, Ccompare b,
      Set_impl b) => Set (a, b) -> a -> Maybe (Set b);
set_as_map (DList_set xs) =
  (case (ceq_prod :: Maybe ((a, b) -> (a, b) -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None"
        (\ _ -> set_as_map (DList_set xs));
    Just _ ->
      lookupa
        (foldc (\ (x, z) m -> (case lookupa m x of {
                                Nothing -> updateb x (insert z bot_set) m;
                                Just zs -> updateb x (insert z zs) m;
                              }))
          xs emptya);
  });
set_as_map (RBT_set t) =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None" (\ _ -> set_as_map (RBT_set t));
    Just _ ->
      lookupa
        (foldb (\ (x, z) m -> (case lookupa m x of {
                                Nothing -> updateb x (insert z bot_set) m;
                                Just zs -> updateb x (insert z zs) m;
                              }))
          t emptya);
  });

h_from ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Set (b, (c, a));
h_from m q = let {
               ma = set_as_map (transitions m);
             } in (case ma q of {
                    Nothing -> bot_set;
                    Just yqs -> yqs;
                  });

targeta :: forall a b c. a -> [(a, (b, (c, a)))] -> a;
targeta q [] = q;
targeta q (v : va) = snd (snd (snd (last (v : va))));

target :: forall a b c. a -> [(a, (b, (c, a)))] -> a;
target q p = targeta q p;

foldr :: forall a b. (a -> b -> b) -> [a] -> b -> b;
foldr f [] = id;
foldr f (x : xs) = f x . foldr f xs;

filtera :: forall a. (Ceq a, Ccompare a) => (a -> Bool) -> Set a -> Set a;
filtera p a = inf_set a (Collect_set p);

apsnd :: forall a b c. (a -> b) -> (c, a) -> (c, b);
apsnd f (x, y) = (x, f y);

divmod_integer :: Integer -> Integer -> (Integer, Integer);
divmod_integer k l =
  (if k == (0 :: Integer) then ((0 :: Integer), (0 :: Integer))
    else (if (0 :: Integer) < l
           then (if (0 :: Integer) < k then divMod (abs k) (abs l)
                  else (case divMod (abs k) (abs l) of {
                         (r, s) ->
                           (if s == (0 :: Integer)
                             then (negate r, (0 :: Integer))
                             else (negate r - (1 :: Integer), l - s));
                       }))
           else (if l == (0 :: Integer) then ((0 :: Integer), k)
                  else apsnd negate
                         (if k < (0 :: Integer) then divMod (abs k) (abs l)
                           else (case divMod (abs k) (abs l) of {
                                  (r, s) ->
                                    (if s == (0 :: Integer)
                                      then (negate r, (0 :: Integer))
                                      else (negate r - (1 :: Integer),
     negate l - s));
                                })))));

divide_integer :: Integer -> Integer -> Integer;
divide_integer k l = fst (divmod_integer k l);

divide_nat :: Nat -> Nat -> Nat;
divide_nat m n = Nat (divide_integer (integer_of_nat m) (integer_of_nat n));

part :: forall a b. (Linorder b) => (a -> b) -> b -> [a] -> ([a], ([a], [a]));
part f pivot (x : xs) =
  (case part f pivot xs of {
    (lts, (eqs, gts)) ->
      let {
        xa = f x;
      } in (if less xa pivot then (x : lts, (eqs, gts))
             else (if less pivot xa then (lts, (eqs, x : gts))
                    else (lts, (x : eqs, gts))));
  });
part f pivot [] = ([], ([], []));

sort_key :: forall a b. (Linorder b) => (a -> b) -> [a] -> [a];
sort_key f xs =
  (case xs of {
    [] -> [];
    [_] -> xs;
    [x, y] -> (if less_eq (f x) (f y) then xs else [y, x]);
    _ : _ : _ : _ ->
      (case part f
              (f (nth xs
                   (divide_nat (size_list xs) (nat_of_integer (2 :: Integer)))))
              xs
        of {
        (lts, (eqs, gts)) -> sort_key f lts ++ eqs ++ sort_key f gts;
      });
  });

membera :: forall a. (Eq a) => [a] -> a -> Bool;
membera [] y = False;
membera (x : xs) y = x == y || membera xs y;

remdups :: forall a. (Eq a) => [a] -> [a];
remdups [] = [];
remdups (x : xs) = (if membera xs x then remdups xs else x : remdups xs);

sorted_list_of_set ::
  forall a. (Ceq a, Ccompare a, Eq a, Linorder a) => Set a -> [a];
sorted_list_of_set (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "sorted_list_of_set RBT_set: ccompare = None"
        (\ _ -> sorted_list_of_set (RBT_set rbt));
    Just _ -> sort_key (\ x -> x) (keysb rbt);
  });
sorted_list_of_set (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "sorted_list_of_set DList_set: ceq = None"
        (\ _ -> sorted_list_of_set (DList_set dxs));
    Just _ -> sort_key (\ x -> x) (list_of_dlist dxs);
  });
sorted_list_of_set (Set_Monad xs) = sort_key (\ x -> x) (remdups xs);

inputs_as_list ::
  forall a b c. (Ceq b, Ccompare b, Eq b, Linorder b) => Fsm a b c -> [b];
inputs_as_list m = sorted_list_of_set (inputs m);

fset_of_list :: forall a. (Ceq a, Ccompare a, Set_impl a) => [a] -> Fset a;
fset_of_list xa = Abs_fset (set xa);

finputs ::
  forall a b c.
    (Ceq b, Ccompare b, Eq b, Linorder b, Set_impl b) => Fsm a b c -> Fset b;
finputs m = fset_of_list (inputs_as_list m);

states_as_list ::
  forall a b c. (Ceq a, Ccompare a, Eq a, Linorder a) => Fsm a b c -> [a];
states_as_list m = sorted_list_of_set (states m);

fstates ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a) => Fsm a b c -> Fset a;
fstates m = fset_of_list (states_as_list m);

fimage ::
  forall b a.
    (Ceq b, Ccompare b, Ceq a, Ccompare a,
      Set_impl a) => (b -> a) -> Fset b -> Fset a;
fimage xb xc = Abs_fset (image xb (fset xc));

map_add :: forall a b. (a -> Maybe b) -> (a -> Maybe b) -> a -> Maybe b;
map_add m1 m2 = (\ x -> (case m2 x of {
                          Nothing -> m1 x;
                          Just a -> Just a;
                        }));

outputs_as_list ::
  forall a b c. (Ceq c, Ccompare c, Eq c, Linorder c) => Fsm a b c -> [c];
outputs_as_list m = sorted_list_of_set (outputs m);

foutputs ::
  forall a b c.
    (Ceq c, Ccompare c, Eq c, Linorder c, Set_impl c) => Fsm a b c -> Fset c;
foutputs m = fset_of_list (outputs_as_list m);

h_wpi ::
  forall a b c.
    Fsm_with_precomputations_impl a b c -> Mapping (a, b) (Set (c, a));
h_wpi (FSMWPI x1 x2 x3 x4 x5 x6 x7) = x6;

from_FSMI_impl ::
  forall a b c.
    (Ceq a,
      Ccompare a) => Fsm_with_precomputations_impl a b c ->
                       a -> Fsm_with_precomputations_impl a b c;
from_FSMI_impl m q =
  (if member q (states_wpi m)
    then FSMWPI q (states_wpi m) (inputs_wpi m) (outputs_wpi m)
           (transitions_wpi m) (h_wpi m) (h_obs_wpi m)
    else m);

from_FSMIa ::
  forall a b c.
    (Ceq a,
      Ccompare a) => Fsm_with_precomputations a b c ->
                       a -> Fsm_with_precomputations a b c;
from_FSMIa xb xc =
  Fsm_with_precomputations
    (from_FSMI_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

from_FSMI ::
  forall a b c. (Ceq a, Ccompare a) => Fsm_impl a b c -> a -> Fsm_impl a b c;
from_FSMI (FSMWP m) q = FSMWP (from_FSMIa m q);

from_FSM :: forall a b c. (Ceq a, Ccompare a) => Fsm a b c -> a -> Fsm a b c;
from_FSM xb xc = Abs_fsm (from_FSMI (fsm_impl_of_fsm xb) xc);

comp_fun_idem_apply :: forall b a. Comp_fun_idem b a -> b -> a -> a;
comp_fun_idem_apply (Abs_comp_fun_idem x) = x;

set_fold_cfi ::
  forall a b. (Ceq a, Ccompare a) => Comp_fun_idem a b -> b -> Set a -> b;
set_fold_cfi f b (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_fold_cfi RBT_set: ccompare = None"
        (\ _ -> set_fold_cfi f b (RBT_set rbt));
    Just _ -> foldb (comp_fun_idem_apply f) rbt b;
  });
set_fold_cfi f b (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_fold_cfi DList_set: ceq = None"
        (\ _ -> set_fold_cfi f b (DList_set dxs));
    Just _ -> foldc (comp_fun_idem_apply f) dxs b;
  });
set_fold_cfi f b (Set_Monad xs) = fold (comp_fun_idem_apply f) xs b;
set_fold_cfi f b (Collect_set p) =
  (error :: forall a. String -> (() -> a) -> a)
    "set_fold_cfi not supported on Collect_set"
    (\ _ -> set_fold_cfi f b (Collect_set p));
set_fold_cfi f b (Complement a) =
  (error :: forall a. String -> (() -> a) -> a)
    "set_fold_cfi not supported on Complement"
    (\ _ -> set_fold_cfi f b (Complement a));

sup_cfi :: forall a. (Lattice a) => Comp_fun_idem a a;
sup_cfi = Abs_comp_fun_idem sup;

sup_seta ::
  forall a.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a,
      Set_impl a) => Set (Set a) -> Set a;
sup_seta a =
  (if finite a then set_fold_cfi sup_cfi bot_set a
    else (error :: forall a. String -> (() -> a) -> a) "Sup: infinite"
           (\ _ -> sup_seta a));

ffUnion ::
  forall a.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a,
      Set_impl a) => Fset (Fset a) -> Fset a;
ffUnion xa = Abs_fset (sup_seta (image fset (fset xa)));

ffilter :: forall a. (Ceq a, Ccompare a) => (a -> Bool) -> Fset a -> Fset a;
ffilter xb xc = Abs_fset (filtera xb (fset xc));

finsert :: forall a. (Ceq a, Ccompare a) => a -> Fset a -> Fset a;
finsert xb xc = Abs_fset (insert xb (fset xc));

butlast :: forall a. [a] -> [a];
butlast [] = [];
butlast (x : xs) = (if null xs then [] else x : butlast xs);

hd :: forall a. [a] -> a;
hd (x21 : x22) = x21;

remove1 :: forall a. (Eq a) => a -> [a] -> [a];
remove1 x [] = [];
remove1 x (y : xs) = (if x == y then xs else y : remove1 x xs);

map_upds :: forall a b. (Eq a) => (a -> Maybe b) -> [a] -> [b] -> a -> Maybe b;
map_upds m xs ys = map_add m (map_of (reverse (zip xs ys)));

mapa :: forall a b c. (a -> b -> c) -> Rbt a b -> Rbt a c;
mapa f Empty = Empty;
mapa f (Branch c lt k v rt) = Branch c (mapa f lt) k (f k v) (mapa f rt);

mapb ::
  forall a c b.
    (Ccompare a) => (a -> c -> b) -> Mapping_rbt a c -> Mapping_rbt a b;
mapb xb xc = Mapping_RBTa (mapa xb (impl_ofa xc));

keysc :: forall a b. (Ceq a, Ccompare a, Set_impl a) => Alist a b -> Set a;
keysc xa = set (map fst (impl_of xa));

keys ::
  forall a b. (Cenum a, Ceq a, Ccompare a, Set_impl a) => Mapping a b -> Set a;
keys (RBT_Mapping t) = RBT_set (mapb (\ _ _ -> ()) t);
keys (Assoc_List_Mapping al) = keysc al;
keys (Mapping m) = collect (\ k -> not (is_none (m k)));

the_elem :: forall a. (Ceq a, Ccompare a) => Set a -> a;
the_elem (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "the_elem RBT_set: ccompare = None" (\ _ -> the_elem (RBT_set rbt));
    Just _ ->
      (case impl_ofa rbt of {
        Empty ->
          (error :: forall a. String -> (() -> a) -> a)
            "the_elem RBT_set: not unique" (\ _ -> the_elem (RBT_set rbt));
        Branch _ Empty x _ Empty -> x;
        Branch _ Empty _ _ (Branch _ _ _ _ _) ->
          (error :: forall a. String -> (() -> a) -> a)
            "the_elem RBT_set: not unique" (\ _ -> the_elem (RBT_set rbt));
        Branch _ (Branch _ _ _ _ _) _ _ _ ->
          (error :: forall a. String -> (() -> a) -> a)
            "the_elem RBT_set: not unique" (\ _ -> the_elem (RBT_set rbt));
      });
  });
the_elem (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "the_elem DList_set: ceq = None" (\ _ -> the_elem (DList_set dxs));
    Just _ ->
      (case list_of_dlist dxs of {
        [] -> (error :: forall a. String -> (() -> a) -> a)
                "the_elem DList_set: not unique"
                (\ _ -> the_elem (DList_set dxs));
        [x] -> x;
        _ : _ : _ ->
          (error :: forall a. String -> (() -> a) -> a)
            "the_elem DList_set: not unique" (\ _ -> the_elem (DList_set dxs));
      });
  });
the_elem (Set_Monad [x]) = x;

filterb :: forall a b. ((a, b) -> Bool) -> Alist a b -> Alist a b;
filterb xb xc = Alist (filter xb (impl_of xc));

pow_list :: forall a. [a] -> [[a]];
pow_list [] = [[]];
pow_list (x : xs) = let {
                      pxs = pow_list xs;
                    } in pxs ++ map (\ a -> x : a) pxs;

acyclic_paths_up_to_lengtha ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => [(a, (b, (c, a)))] ->
                       a -> (a -> Set (b, (c, a))) ->
                              Set a -> Nat -> Set [(a, (b, (c, a)))];
acyclic_paths_up_to_lengtha prev q hF visitedStates k =
  (if equal_nat k zero_nat
    then insert prev (set_empty (of_phantom set_impl_list))
    else let {
           tF = filtera (\ (_, (_, qa)) -> not (member qa visitedStates))
                  (hF q);
         } in insert prev
                (sup_seta
                  (image
                    (\ (x, (y, qa)) ->
                      acyclic_paths_up_to_lengtha (prev ++ [(q, (x, (y, qa)))])
                        qa hF (insert qa visitedStates) (minus_nat k one_nat))
                    tF)));

acyclic_paths_up_to_length ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Nat -> Set [(a, (b, (c, a)))];
acyclic_paths_up_to_length m q k =
  (if member q (states m)
    then acyclic_paths_up_to_lengtha [] q
           (\ x -> (case set_as_map (transitions m) x of {
                     Nothing -> bot_set;
                     Just xs -> xs;
                   }))
           (insert q bot_set) k
    else set_empty (of_phantom set_impl_list));

lS_acyclic ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Set [(b, c)];
lS_acyclic m q =
  image (map (\ t -> (fst (snd t), fst (snd (snd t)))))
    (acyclic_paths_up_to_length m q (minus_nat (size m) one_nat));

observable ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Ceq b, Ccompare b, Eq b, Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> Bool;
observable m =
  ball (transitions m)
    (\ t1 ->
      ball (transitions m)
        (\ t2 ->
          (if fst t1 == fst t2 &&
                fst (snd t1) == fst (snd t2) &&
                  fst (snd (snd t1)) == fst (snd (snd t2))
            then snd (snd (snd t1)) == snd (snd (snd t2)) else True)));

removeAll :: forall a. (Eq a) => a -> [a] -> [a];
removeAll x [] = [];
removeAll x (y : xs) = (if x == y then removeAll x xs else y : removeAll x xs);

filterc ::
  forall a b. (Ccompare a) => (a -> b -> Bool) -> Mapping a b -> Mapping a b;
filterc p (RBT_Mapping t) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "filter RBT_Mapping: ccompare = None"
        (\ _ -> filterc p (RBT_Mapping t));
    Just _ -> RBT_Mapping (filtere (\ (a, b) -> p a b) t);
  });
filterc p (Assoc_List_Mapping al) =
  Assoc_List_Mapping (filterb (\ (a, b) -> p a b) al);
filterc p (Mapping m) =
  Mapping (\ k -> (case m k of {
                    Nothing -> Nothing;
                    Just v -> (if p k v then Just v else Nothing);
                  }));

filter_comp_minus ::
  forall a b c. (a -> a -> Ordera) -> Rbt a b -> Rbt a c -> [(a, b)];
filter_comp_minus c t1 t2 =
  filter (\ (k, _) -> is_none (rbt_comp_lookup c t2 k)) (entries t1);

small_rbt :: forall a b. Rbt a b -> Bool;
small_rbt t = less_nat (bheight t) (nat_of_integer (4 :: Integer));

comp_minus :: forall a b. (a -> a -> Ordera) -> Rbt a b -> Rbt a b -> Rbt a b;
comp_minus c t1 t2 =
  (if small_rbt t2 then folda (\ k _ -> rbt_comp_delete c k) t2 t1
    else (if small_rbt t1 then rbtreeify (filter_comp_minus c t1 t2)
           else (case t2 of {
                  Empty -> t1;
                  Branch _ l2 a _ r2 ->
                    (case rbt_split_comp c t1 a of {
                      (l1, (_, r1)) ->
                        rbt_join2 (comp_minus c l1 l2) (comp_minus c r1 r2);
                    });
                })));

rbt_comp_minus ::
  forall a b. (a -> a -> Ordera) -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_minus c t1 t2 = paint B (comp_minus c t1 t2);

minus ::
  forall a.
    (Ccompare a) => Mapping_rbt a () -> Mapping_rbt a () -> Mapping_rbt a ();
minus xb xc =
  Mapping_RBTa (rbt_comp_minus (the ccompare) (impl_ofa xb) (impl_ofa xc));

is_prefix :: forall a. (Eq a) => [a] -> [a] -> Bool;
is_prefix [] uu = True;
is_prefix (x : xs) [] = False;
is_prefix (x : xs) (y : ys) = x == y && is_prefix xs ys;

find_index :: forall a. (a -> Bool) -> [a] -> Maybe Nat;
find_index f [] = Nothing;
find_index f (x : xs) =
  (if f x then Just zero_nat else (case find_index f xs of {
                                    Nothing -> Nothing;
                                    Just k -> Just (suc k);
                                  }));

transitions_as_list ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Ceq b, Ccompare b, Eq b, Linorder b,
      Ceq c, Ccompare c, Eq c, Linorder c) => Fsm a b c -> [(a, (b, (c, a)))];
transitions_as_list m = sorted_list_of_set (transitions m);

ftransitions ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Linorder b, Set_impl b, Ceq c, Ccompare c, Eq c, Linorder c,
      Set_impl c) => Fsm a b c -> Fset (a, (b, (c, a)));
ftransitions m = fset_of_list (transitions_as_list m);

assign_indices ::
  forall a. (Ceq a, Ccompare a, Eq a, Linorder a) => Set a -> a -> Nat;
assign_indices xs =
  (\ x -> the (find_index (\ a -> x == a) (sorted_list_of_set xs)));

set_as_mapping_image ::
  forall a b c d.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ccompare c, Eq c, Mapping_impl c,
      Ceq d, Ccompare d,
      Set_impl d) => Set (a, b) -> ((a, b) -> (c, d)) -> Mapping c (Set d);
set_as_mapping_image (DList_set xs) f2 =
  (case (ceq_prod :: Maybe ((a, b) -> (a, b) -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map_image DList_set: ccompare = None"
        (\ _ -> set_as_mapping_image (DList_set xs) f2);
    Just _ ->
      foldc (\ kv m1 ->
              (case f2 kv of {
                (x, z) -> (case lookupa m1 x of {
                            Nothing -> updateb x (insert z bot_set) m1;
                            Just zs -> updateb x (insert z zs) m1;
                          });
              }))
        xs emptya;
  });
set_as_mapping_image (RBT_set t) f1 =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map_image RBT_set: ccompare = None"
        (\ _ -> set_as_mapping_image (RBT_set t) f1);
    Just _ ->
      foldb (\ kv m1 ->
              (case f1 kv of {
                (x, z) -> (case lookupa m1 x of {
                            Nothing -> updateb x (insert z bot_set) m1;
                            Just zs -> updateb x (insert z zs) m1;
                          });
              }))
        t emptya;
  });

set_as_mapping ::
  forall a b.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Ceq b, Ccompare b,
      Set_impl b) => Set (a, b) -> Mapping a (Set b);
set_as_mapping (DList_set xs) =
  (case (ceq_prod :: Maybe ((a, b) -> (a, b) -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None"
        (\ _ -> set_as_mapping (DList_set xs));
    Just _ ->
      foldc (\ (x, z) m -> (case lookupa m x of {
                             Nothing -> updateb x (insert z bot_set) m;
                             Just zs -> updateb x (insert z zs) m;
                           }))
        xs emptya;
  });
set_as_mapping (RBT_set t) =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None"
        (\ _ -> set_as_mapping (RBT_set t));
    Just _ ->
      foldb (\ (x, z) m -> (case lookupa m x of {
                             Nothing -> updateb x (insert z bot_set) m;
                             Just zs -> updateb x (insert z zs) m;
                           }))
        t emptya;
  });

map_option :: forall a b. (a -> b) -> Maybe a -> Maybe b;
map_option f Nothing = Nothing;
map_option f (Just x2) = Just (f x2);

map_valuesa :: forall a c b. (a -> c -> b) -> Alist a c -> Alist a b;
map_valuesa xb xc = Alist (map (\ (x, y) -> (x, xb x y)) (impl_of xc));

map_values ::
  forall a b c. (Ccompare a) => (a -> b -> c) -> Mapping a b -> Mapping a c;
map_values f (RBT_Mapping t) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "map_values RBT_Mapping: ccompare = None"
        (\ _ -> map_values f (RBT_Mapping t));
    Just _ -> RBT_Mapping (mapb f t);
  });
map_values f (Assoc_List_Mapping al) = Assoc_List_Mapping (map_valuesa f al);
map_values f (Mapping m) = Mapping (\ k -> map_option (f k) (m k));

h_obs_impl_from_h ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Set_impl a, Ccompare b, Ceq c, Ccompare c,
      Eq c,
      Mapping_impl c) => Mapping (a, b) (Set (c, a)) ->
                           Mapping (a, b) (Mapping c a);
h_obs_impl_from_h h =
  map_values
    (\ _ yqs -> let {
                  m = set_as_mapping yqs;
                  ma = filterc (\ _ qs -> equal_nat (card qs) one_nat) m;
                  mb = map_values (\ _ -> the_elem) ma;
                } in mb)
    h;

rename_states_impl ::
  forall a b c d.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Set_impl b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d, Ceq d,
      Ccompare d, Eq d, Mapping_impl d,
      Set_impl d) => Fsm_with_precomputations_impl a b c ->
                       (a -> d) -> Fsm_with_precomputations_impl d b c;
rename_states_impl m f =
  let {
    ts = image (\ t ->
                 (f (fst t),
                   (fst (snd t), (fst (snd (snd t)), f (snd (snd (snd t)))))))
           (transitions_wpi m);
    h = set_as_mapping_image ts (\ (q, (x, (y, qa))) -> ((q, x), (y, qa)));
  } in FSMWPI (f (initial_wpi m)) (image f (states_wpi m)) (inputs_wpi m)
         (outputs_wpi m) ts h (h_obs_impl_from_h h);

rename_statesb ::
  forall d b c a.
    (Ceq d, Ccompare d, Ceq b, Ccompare b, Eq b, Mapping_impl b, Set_impl b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV a, Ceq a,
      Ccompare a, Eq a, Mapping_impl a,
      Set_impl a) => Fsm_with_precomputations d b c ->
                       (d -> a) -> Fsm_with_precomputations a b c;
rename_statesb xb xc =
  Fsm_with_precomputations
    (rename_states_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

rename_statesa ::
  forall a b c d.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Set_impl b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d, Ceq d,
      Ccompare d, Eq d, Mapping_impl d,
      Set_impl d) => Fsm_impl a b c -> (a -> d) -> Fsm_impl d b c;
rename_statesa (FSMWP m) f = FSMWP (rename_statesb m f);

rename_states ::
  forall d b c a.
    (Ceq d, Ccompare d, Ceq b, Ccompare b, Eq b, Mapping_impl b, Set_impl b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV a, Ceq a,
      Ccompare a, Eq a, Mapping_impl a,
      Set_impl a) => Fsm d b c -> (d -> a) -> Fsm a b c;
rename_states xb xc = Abs_fsm (rename_statesa (fsm_impl_of_fsm xb) xc);

index_states ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Ceq b, Ccompare b, Eq b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Fsm Nat b c;
index_states m = rename_states m (assign_indices (states m));

paths_for_ioa ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c, Ccompare c,
      Eq c) => ((a, b) -> Set (c, a)) ->
                 [(b, c)] -> a -> [(a, (b, (c, a)))] -> Set [(a, (b, (c, a)))];
paths_for_ioa f [] q prev = insert prev (set_empty (of_phantom set_impl_list));
paths_for_ioa f ((x, y) : io) q prev =
  sup_seta
    (image
      (\ yq -> paths_for_ioa f io (snd yq) (prev ++ [(q, (x, (y, snd yq)))]))
      (filtera (\ yq -> fst yq == y) (f (q, x))));

paths_for_io ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c -> a -> [(b, c)] -> Set [(a, (b, (c, a)))];
paths_for_io m q io =
  (if member q (states m) then paths_for_ioa (h m) io q []
    else set_empty (of_phantom set_impl_list));

productc ::
  forall a b. (Ceq a, Ceq b) => Set_dlist a -> Set_dlist b -> Set_dlist (a, b);
productc dxs1 dxs2 =
  Abs_dlist (foldc (\ a -> foldc (\ c -> (\ b -> (a, c) : b)) dxs2) dxs1 []);

rbt_product ::
  forall a b c d e.
    (a -> b -> c -> d -> e) -> Rbt a b -> Rbt c d -> Rbt (a, c) e;
rbt_product f rbt1 rbt2 =
  rbtreeify
    (reverse
      (folda (\ a b -> folda (\ c d -> (\ e -> ((a, c), f a b c d) : e)) rbt2)
        rbt1 []));

productf ::
  forall a d b e c.
    (Ccompare a,
      Ccompare b) => (a -> d -> b -> e -> c) ->
                       Mapping_rbt a d ->
                         Mapping_rbt b e -> Mapping_rbt (a, b) c;
productf xc xd xe = Mapping_RBTa (rbt_product xc (impl_ofa xd) (impl_ofa xe));

productb ::
  forall a b.
    (Ccompare a,
      Ccompare b) => Mapping_rbt a () ->
                       Mapping_rbt b () -> Mapping_rbt (a, b) ();
productb rbt1 rbt2 = productf (\ _ _ _ _ -> ()) rbt1 rbt2;

producte ::
  forall a b.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b) => Set a -> Set b -> Set (a, b);
producte (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set RBT_set: ccompare1 = None"
        (\ _ -> producte (RBT_set rbt1) (RBT_set rbt2));
    Just _ ->
      (case (ccompare :: Maybe (b -> b -> Ordera)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "product RBT_set RBT_set: ccompare2 = None"
            (\ _ -> producte (RBT_set rbt1) (RBT_set rbt2));
        Just _ -> RBT_set (productb rbt1 rbt2);
      });
  });
producte a2 (RBT_set rbt2) =
  (case (ccompare :: Maybe (b -> b -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set: ccompare2 = None" (\ _ -> producte a2 (RBT_set rbt2));
    Just _ -> foldb (\ y -> sup_set (image (\ x -> (x, y)) a2)) rbt2 bot_set;
  });
producte (RBT_set rbt1) b2 =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set: ccompare1 = None" (\ _ -> producte (RBT_set rbt1) b2);
    Just _ -> foldb (\ x -> sup_set (image (\ a -> (x, a)) b2)) rbt1 bot_set;
  });
producte (DList_set dxs) (DList_set dys) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set DList_set: ceq1 = None"
        (\ _ -> producte (DList_set dxs) (DList_set dys));
    Just _ ->
      (case (ceq :: Maybe (b -> b -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "product DList_set DList_set: ceq2 = None"
            (\ _ -> producte (DList_set dxs) (DList_set dys));
        Just _ -> DList_set (productc dxs dys);
      });
  });
producte a1 (DList_set dys) =
  (case (ceq :: Maybe (b -> b -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set2: ceq = None" (\ _ -> producte a1 (DList_set dys));
    Just _ -> foldc (\ y -> sup_set (image (\ x -> (x, y)) a1)) dys bot_set;
  });
producte (DList_set dxs) b1 =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set1: ceq = None" (\ _ -> producte (DList_set dxs) b1);
    Just _ -> foldc (\ x -> sup_set (image (\ a -> (x, a)) b1)) dxs bot_set;
  });
producte (Set_Monad xs) (Set_Monad ys) =
  Set_Monad (fold (\ x -> fold (\ y -> (\ a -> (x, y) : a)) ys) xs []);
producte a b = Collect_set (\ (x, y) -> member x a && member y b);

product_impl ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d,
      Cenum d, Ceq d, Cproper_interval d, Eq d, Mapping_impl d,
      Set_impl d) => Fsm_with_precomputations_impl a b c ->
                       Fsm_with_precomputations_impl d b c ->
                         Fsm_with_precomputations_impl (a, d) b c;
product_impl a b =
  let {
    ts = image (\ (aa, ba) ->
                 (case aa of {
                   (qA, (x, (y, qAa))) ->
                     (\ (qB, (_, (_, qBa))) ->
                       ((qA, qB), (x, (y, (qAa, qBa)))));
                 })
                   ba)
           (filtera
             (\ (aa, ba) ->
               (case aa of {
                 (_, (x, (y, _))) ->
                   (\ (_, (xa, (ya, _))) -> x == xa && y == ya);
               })
                 ba)
             (sup_seta
               (image (\ tA -> image (\ aa -> (tA, aa)) (transitions_wpi b))
                 (transitions_wpi a))));
    h = set_as_mapping_image ts (\ (q, (x, (y, qa))) -> ((q, x), (y, qa)));
  } in FSMWPI (initial_wpi a, initial_wpi b)
         (producte (states_wpi a) (states_wpi b))
         (sup_set (inputs_wpi a) (inputs_wpi b))
         (sup_set (outputs_wpi a) (outputs_wpi b)) ts h (h_obs_impl_from_h h);

productg ::
  forall a c d b.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Mapping_impl c, Set_impl c, Finite_UNIV d, Cenum d, Ceq d,
      Cproper_interval d, Eq d, Mapping_impl d, Set_impl d, Card_UNIV b,
      Cenum b, Ceq b, Cproper_interval b, Eq b, Mapping_impl b,
      Set_impl b) => Fsm_with_precomputations a c d ->
                       Fsm_with_precomputations b c d ->
                         Fsm_with_precomputations (a, b) c d;
productg xb xc =
  Fsm_with_precomputations
    (product_impl (fsm_with_precomputations_impl_of_fsm_with_precomputations xb)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xc));

producta ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d,
      Cenum d, Ceq d, Cproper_interval d, Eq d, Mapping_impl d,
      Set_impl d) => Fsm_impl a b c -> Fsm_impl d b c -> Fsm_impl (a, d) b c;
producta (FSMWP a) (FSMWP b) = FSMWP (productg a b);

zip_with_index_from :: forall a. Nat -> [a] -> [(Nat, a)];
zip_with_index_from n (x : xs) = (n, x) : zip_with_index_from (suc n) xs;
zip_with_index_from n [] = [];

rbt_comp_bulkload :: forall a b. (a -> a -> Ordera) -> [(a, b)] -> Rbt a b;
rbt_comp_bulkload c xs = foldr (\ (a, b) -> rbt_comp_insert c a b) xs Empty;

bulkloada :: forall a b. (Ccompare a) => [(a, b)] -> Mapping_rbt a b;
bulkloada xa = Mapping_RBTa (rbt_comp_bulkload (the ccompare) xa);

bulkload :: forall a. [a] -> Mapping Nat a;
bulkload vs = RBT_Mapping (bulkloada (zip_with_index_from zero_nat vs));

tabulate ::
  forall a b.
    (Ccompare a, Eq a, Mapping_impl a) => [a] -> (a -> b) -> Mapping a b;
tabulate xs f = fold (\ k -> updateb k (f k)) xs emptya;

isin :: forall a. (Ccompare a, Eq a) => Prefix_tree a -> [a] -> Bool;
isin (MPT m) xs = (case xs of {
                    [] -> True;
                    x : xsa -> (case lookupa m x of {
                                 Nothing -> False;
                                 Just t -> isin t xsa;
                               });
                  });

prefixes :: forall a. [a] -> [[a]];
prefixes xs =
  reverse
    (snd (foldl (\ (acc1, acc2) x -> (x : acc1, reverse (x : acc1) : acc2))
           ([], [[]]) xs));

find_removea :: forall a. (a -> Bool) -> [a] -> [a] -> Maybe (a, [a]);
find_removea p [] uu = Nothing;
find_removea p (x : xs) prev =
  (if p x then Just (x, prev ++ xs) else find_removea p xs (prev ++ [x]));

find_remove :: forall a. (a -> Bool) -> [a] -> Maybe (a, [a]);
find_remove p xs = find_removea p xs [];

separate_by :: forall a. (a -> Bool) -> [a] -> ([a], [a]);
separate_by p xs =
  foldr (\ x (prevPass, prevFail) ->
          (if p x then (x : prevPass, prevFail) else (prevPass, x : prevFail)))
    xs ([], []);

filter_states_impl ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Ceq c,
      Ccompare c, Eq c,
      Mapping_impl c) => Fsm_with_precomputations_impl a b c ->
                           (a -> Bool) -> Fsm_with_precomputations_impl a b c;
filter_states_impl m p =
  (if p (initial_wpi m)
    then let {
           h = filterc (\ (q, _) _ -> p q) (h_wpi m);
           ha = map_values (\ _ -> filtera (\ (_, a) -> p a)) h;
         } in FSMWPI (initial_wpi m) (filtera p (states_wpi m)) (inputs_wpi m)
                (outputs_wpi m)
                (filtera (\ t -> p (fst t) && p (snd (snd (snd t))))
                  (transitions_wpi m))
                ha (h_obs_impl_from_h ha)
    else m);

filter_statesb ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Ceq c,
      Ccompare c, Eq c,
      Mapping_impl c) => Fsm_with_precomputations a b c ->
                           (a -> Bool) -> Fsm_with_precomputations a b c;
filter_statesb xb xc =
  Fsm_with_precomputations
    (filter_states_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

filter_statesa ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Ceq c,
      Ccompare c, Eq c,
      Mapping_impl c) => Fsm_impl a b c -> (a -> Bool) -> Fsm_impl a b c;
filter_statesa (FSMWP m) p = FSMWP (filter_statesb m p);

filter_states ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Ceq c,
      Ccompare c, Eq c,
      Mapping_impl c) => Fsm a b c -> (a -> Bool) -> Fsm a b c;
filter_states xb xc = Abs_fsm (filter_statesa (fsm_impl_of_fsm xb) xc);

list_as_mapping ::
  forall a b.
    (Ccompare a, Eq a, Mapping_impl a, Ceq b, Ccompare b,
      Set_impl b) => [(a, b)] -> Mapping a (Set b);
list_as_mapping xs =
  foldr (\ (x, z) m -> (case lookupa m x of {
                         Nothing -> updateb x (insert z bot_set) m;
                         Just zs -> updateb x (insert z zs) m;
                       }))
    xs emptya;

fsm_with_precomputations_impl_from_lista ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Set_impl c) => a -> [(a, (b, (c, a)))] ->
                            Fsm_with_precomputations_impl a b c;
fsm_with_precomputations_impl_from_lista q [] =
  FSMWPI q (insert q bot_set) bot_set bot_set bot_set emptya emptya;
fsm_with_precomputations_impl_from_lista q (t : ts) =
  let {
    tsr = remdups (t : ts);
    h = list_as_mapping (map (\ (qb, (x, (y, qa))) -> ((qb, x), (y, qa))) tsr);
  } in FSMWPI (fst t)
         (set (remdups (map fst tsr ++ map (\ a -> snd (snd (snd a))) tsr)))
         (set (remdups (map (\ a -> fst (snd a)) tsr)))
         (set (remdups (map (\ a -> fst (snd (snd a))) tsr))) (set tsr) h
         (h_obs_impl_from_h h);

fsm_with_precomputations_impl_from_list ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Set_impl c) => a -> [(a, (b, (c, a)))] ->
                            Fsm_with_precomputations_impl a b c;
fsm_with_precomputations_impl_from_list q ts =
  fsm_with_precomputations_impl_from_lista q ts;

fsm_with_precomputations_from_list ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm_with_precomputations a b c;
fsm_with_precomputations_from_list q ts =
  Fsm_with_precomputations (fsm_with_precomputations_impl_from_list q ts);

fsm_impl_from_list ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c, Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm_impl a b c;
fsm_impl_from_list q ts = FSMWP (fsm_with_precomputations_from_list q ts);

fsm_from_list ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c, Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm a b c;
fsm_from_list xb xc = Abs_fsm (fsm_impl_from_list xb xc);

emptyc :: forall a. (Ccompare a, Mapping_impl a) => Prefix_tree a;
emptyc = MPT emptya;

aftera ::
  forall a.
    (Ccompare a, Eq a, Mapping_impl a) => Prefix_tree a -> [a] -> Prefix_tree a;
aftera (MPT m) xs = (case xs of {
                      [] -> MPT m;
                      x : xsa -> (case lookupa m x of {
                                   Nothing -> emptyc;
                                   Just t -> aftera t xsa;
                                 });
                    });

prefix_pairs :: forall a. [a] -> [([a], [a])];
prefix_pairs [] = [];
prefix_pairs (v : va) =
  prefix_pairs (butlast (v : va)) ++
    map (\ ys -> (ys, v : va)) (butlast (prefixes (v : va)));

is_in_language ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm a b c -> a -> [(b, c)] -> Bool;
is_in_language m q [] = True;
is_in_language m q ((x, y) : io) = (case h_obs m q x y of {
                                     Nothing -> False;
                                     Just qa -> is_in_language m qa io;
                                   });

insertb ::
  forall a.
    (Ccompare a, Eq a, Mapping_impl a) => Prefix_tree a -> [a] -> Prefix_tree a;
insertb (MPT m) xs =
  (case xs of {
    [] -> MPT m;
    x : xsa -> MPT (updateb x (insertb (case lookupa m x of {
 Nothing -> emptyc;
 Just t -> t;
                                       })
                                xsa)
                     m);
  });

find_remove_2a ::
  forall a b. (a -> b -> Bool) -> [a] -> [b] -> [a] -> Maybe (a, (b, [a]));
find_remove_2a p [] uu uv = Nothing;
find_remove_2a p (x : xs) ys prev =
  (case find (p x) ys of {
    Nothing -> find_remove_2a p xs ys (prev ++ [x]);
    Just y -> Just (x, (y, prev ++ xs));
  });

find_remove_2 ::
  forall a b. (a -> b -> Bool) -> [a] -> [b] -> Maybe (a, (b, [a]));
find_remove_2 p xs ys = find_remove_2a p xs ys [];

add_transitions_impl ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations_impl a b c ->
                       Set (a, (b, (c, a))) ->
                         Fsm_with_precomputations_impl a b c;
add_transitions_impl m ts =
  (if ball ts
        (\ t ->
          member (fst t) (states_wpi m) &&
            member (fst (snd t)) (inputs_wpi m) &&
              member (fst (snd (snd t))) (outputs_wpi m) &&
                member (snd (snd (snd t))) (states_wpi m))
    then let {
           tsa = sup_set (transitions_wpi m) ts;
           h = set_as_mapping_image tsa
                 (\ (q, (x, (y, qa))) -> ((q, x), (y, qa)));
         } in FSMWPI (initial_wpi m) (states_wpi m) (inputs_wpi m)
                (outputs_wpi m) tsa h (h_obs_impl_from_h h)
    else m);

add_transitionsb ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations a b c ->
                       Set (a, (b, (c, a))) -> Fsm_with_precomputations a b c;
add_transitionsb xb xc =
  Fsm_with_precomputations
    (add_transitions_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

add_transitionsa ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_impl a b c -> Set (a, (b, (c, a))) -> Fsm_impl a b c;
add_transitionsa (FSMWP m) ts = FSMWP (add_transitionsb m ts);

add_transitions ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Set (a, (b, (c, a))) -> Fsm a b c;
add_transitions xb xc = Abs_fsm (add_transitionsa (fsm_impl_of_fsm xb) xc);

combinea ::
  forall a. (Ccompare a) => Prefix_tree a -> Prefix_tree a -> Prefix_tree a;
combinea (MPT (RBT_Mapping m1)) (MPT (RBT_Mapping m2)) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "combine_MPT_RBT_Mapping: ccompare = None"
        (\ _ -> combinea (MPT (RBT_Mapping m1)) (MPT (RBT_Mapping m2)));
    Just _ -> MPT (RBT_Mapping (join (\ _ -> combinea) m1 m2));
  });

is_leaf :: forall a. (Ccompare a) => Prefix_tree a -> Bool;
is_leaf (MPT (RBT_Mapping m)) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_leaf_MPT_RBT_Mapping: ccompare = None"
        (\ _ -> is_leaf (MPT (RBT_Mapping m)));
    Just _ -> is_emptya m;
  });

productd ::
  forall a c d b.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Mapping_impl c, Set_impl c, Finite_UNIV d, Cenum d, Ceq d,
      Cproper_interval d, Eq d, Mapping_impl d, Set_impl d, Card_UNIV b,
      Cenum b, Ceq b, Cproper_interval b, Eq b, Mapping_impl b,
      Set_impl b) => Fsm a c d -> Fsm b c d -> Fsm (a, b) c d;
productd xb xc = Abs_fsm (producta (fsm_impl_of_fsm xb) (fsm_impl_of_fsm xc));

remove_subsets :: forall a. (Cenum a, Ceq a, Ccompare a) => [Set a] -> [Set a];
remove_subsets [] = [];
remove_subsets (x : xs) =
  (case find_remove (less_set x) xs of {
    Nothing -> x : remove_subsets (filter (\ y -> not (less_eq_set y x)) xs);
    Just (y, xsa) ->
      remove_subsets (y : filter (\ ya -> not (less_eq_set ya x)) xsa);
  });

does_distinguish ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm a b c -> a -> a -> [(b, c)] -> Bool;
does_distinguish m q1 q2 io =
  not (is_in_language m q1 io == is_in_language m q2 io);

reaching_paths_up_to_depth ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c ->
                       Set a ->
                         Set a ->
                           (a -> Maybe [(a, (b, (c, a)))]) ->
                             Nat -> a -> Maybe [(a, (b, (c, a)))];
reaching_paths_up_to_depth m nexts dones assignment k =
  (if equal_nat k zero_nat then assignment
    else let {
           usable_transitions =
             filter
               (\ t ->
                 member (fst t) nexts &&
                   not (member (snd (snd (snd t))) dones) &&
                     not (member (snd (snd (snd t))) nexts))
               (transitions_as_list m);
           targets = map (\ a -> snd (snd (snd a))) usable_transitions;
           transition_choice =
             map_upds (\ _ -> Nothing) targets usable_transitions;
           assignmenta =
             map_upds assignment targets
               (map (\ q -> (case transition_choice q of {
                              Just t -> (case assignment (fst t) of {
  Just p -> p ++ [t];
});
                            }))
                 targets);
           nextsa = set targets;
           donesa = sup_set nexts dones;
         } in reaching_paths_up_to_depth m nextsa donesa assignmenta
                (minus_nat k one_nat));

reachable_states ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> Set a;
reachable_states m =
  let {
    path_assignments =
      reaching_paths_up_to_depth m (insert (initial m) bot_set) bot_set
        (fun_upd (\ _ -> Nothing) (initial m) (Just []))
        (minus_nat (size m) one_nat);
  } in filtera (\ q -> not (is_none (path_assignments q))) (states m);

entriesa :: forall a b. (Ccompare a) => Mapping_rbt a b -> [(a, b)];
entriesa xa = entries (impl_ofa xa);

mergesort_by_rel_split :: forall a. ([a], [a]) -> [a] -> ([a], [a]);
mergesort_by_rel_split (xs1, xs2) [] = (xs1, xs2);
mergesort_by_rel_split (xs1, xs2) [x] = (x : xs1, xs2);
mergesort_by_rel_split (xs1, xs2) (x1 : x2 : xs) =
  mergesort_by_rel_split (x1 : xs1, x2 : xs2) xs;

mergesort_by_rel_merge :: forall a. (a -> a -> Bool) -> [a] -> [a] -> [a];
mergesort_by_rel_merge r (x : xs) (y : ys) =
  (if r x y then x : mergesort_by_rel_merge r xs (y : ys)
    else y : mergesort_by_rel_merge r (x : xs) ys);
mergesort_by_rel_merge r xs [] = xs;
mergesort_by_rel_merge r [] (v : va) = v : va;

mergesort_by_rel :: forall a. (a -> a -> Bool) -> [a] -> [a];
mergesort_by_rel r (x1 : x2 : xs) =
  (case mergesort_by_rel_split ([x1], [x2]) xs of {
    (xs1, xs2) ->
      mergesort_by_rel_merge r (mergesort_by_rel r xs1)
        (mergesort_by_rel r xs2);
  });
mergesort_by_rel r [x] = [x];
mergesort_by_rel r [] = [];

from_list ::
  forall a. (Ccompare a, Eq a, Mapping_impl a) => [[a]] -> Prefix_tree a;
from_list xs = foldr (\ x t -> insertb t x) xs emptyc;

filter_transitions_impl ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations_impl a b c ->
                       ((a, (b, (c, a))) -> Bool) ->
                         Fsm_with_precomputations_impl a b c;
filter_transitions_impl m p =
  let {
    ts = filtera p (transitions_wpi m);
    h = set_as_mapping_image ts (\ (q, (x, (y, qa))) -> ((q, x), (y, qa)));
  } in FSMWPI (initial_wpi m) (states_wpi m) (inputs_wpi m) (outputs_wpi m) ts h
         (h_obs_impl_from_h h);

filter_transitionsb ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations a b c ->
                       ((a, (b, (c, a))) -> Bool) ->
                         Fsm_with_precomputations a b c;
filter_transitionsb xb xc =
  Fsm_with_precomputations
    (filter_transitions_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xb) xc);

filter_transitionsa ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_impl a b c ->
                       ((a, (b, (c, a))) -> Bool) -> Fsm_impl a b c;
filter_transitionsa (FSMWP m) p = FSMWP (filter_transitionsb m p);

filter_transitions ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> ((a, (b, (c, a))) -> Bool) -> Fsm a b c;
filter_transitions xb xc =
  Abs_fsm (filter_transitionsa (fsm_impl_of_fsm xb) xc);

language_for_input ::
  forall a b c.
    (Ccompare a, Eq a, Linorder a, Ccompare b, Eq b, Linorder b, Ceq c,
      Ccompare c, Eq c, Linorder c) => Fsm a b c -> a -> [b] -> [[(b, c)]];
language_for_input m q [] = [[]];
language_for_input m q (x : xs) =
  let {
    a = outputs_as_list m;
  } in concatMap
         (\ y ->
           (case h_obs m q x y of {
             Nothing -> [];
             Just qa -> map (\ aa -> (x, y) : aa) (language_for_input m qa xs);
           }))
         a;

lookup_default :: forall a b. (Ccompare b, Eq b) => a -> Mapping b a -> b -> a;
lookup_default d m k = (case lookupa m k of {
                         Nothing -> d;
                         Just v -> v;
                       });

covered_transitions ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c ->
                       (a -> [(b, c)]) -> [(b, c)] -> Set (a, (b, (c, a)));
covered_transitions m v alpha =
  let {
    ts = the_elem (paths_for_io m (initial m) alpha);
  } in set (filter
             (\ t ->
               v (fst t) ++ [(fst (snd t), fst (snd (snd t)))] ==
                 v (snd (snd (snd t))))
             ts);

reachable_states_as_list ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> [a];
reachable_states_as_list m = sorted_list_of_set (reachable_states m);

h_framework ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c,
      Ceq c, Cproper_interval c, Eq c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Fsm a b c -> a -> [(b, c)]) ->
                         (Fsm a b c ->
                           (a -> [(b, c)]) ->
                             (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                               (d -> [(b, c)] -> d) ->
                                 (d -> [(b, c)] -> [[(b, c)]]) ->
                                   (Prefix_tree (b, c), d)) ->
                           (Fsm a b c ->
                             (a -> [(b, c)]) ->
                               [(a, (b, (c, a)))] -> [(a, (b, (c, a)))]) ->
                             (Fsm a b c ->
                               (a -> [(b, c)]) ->
                                 Prefix_tree (b, c) ->
                                   d -> (d -> [(b, c)] -> d) ->
  (d -> [(b, c)] -> [[(b, c)]]) ->
    (d -> [(b, c)] -> [(b, c)] -> d) ->
      Nat ->
        (a, (b, (c, a))) ->
          [(a, (b, (c, a)))] ->
            ([(a, (b, (c, a)))], (Prefix_tree (b, c), d))) ->
                               (Fsm a b c ->
                                 (a -> [(b, c)]) ->
                                   Prefix_tree (b, c) ->
                                     d -> (d -> [(b, c)] -> d) ->
    (d -> [(b, c)] -> [[(b, c)]]) -> a -> b -> c -> (Prefix_tree (b, c), d)) ->
                                 (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                                   (d -> [(b, c)] -> d) ->
                                     (d -> [(b, c)] -> [[(b, c)]]) ->
                                       (d -> [(b, c)] -> [(b, c)] -> d) ->
 Nat -> Prefix_tree (b, c);
h_framework ma get_state_cover handle_state_cover sort_transitions
  handle_unverified_transition handle_unverified_io_pair cg_initial cg_insert
  cg_lookup cg_merge m =
  let {
    rstates_set = reachable_states ma;
    rstates = reachable_states_as_list ma;
    rstates_io =
      product rstates (product (inputs_as_list ma) (outputs_as_list ma));
    undefined_io_pairs =
      filter (\ (q, (x, y)) -> is_none (h_obs ma q x y)) rstates_io;
    v = get_state_cover ma;
    tG1 = handle_state_cover ma v cg_initial cg_insert cg_lookup;
    sc_covered_transitions =
      sup_seta (image (\ q -> covered_transitions ma v (v q)) rstates_set);
    unverified_transitions =
      sort_transitions ma v
        (filter
          (\ t ->
            member (fst t) rstates_set && not (member t sc_covered_transitions))
          (transitions_as_list ma));
    verify_transition =
      (\ (x, (t, g)) ta ->
        handle_unverified_transition ma v t g cg_insert cg_lookup cg_merge m ta
          x);
    tG2 = snd (foldl verify_transition (unverified_transitions, tG1)
                unverified_transitions);
    verify_undefined_io_pair =
      (\ t (q, (x, y)) ->
        fst (handle_unverified_io_pair ma v t (snd tG2) cg_insert cg_lookup q x
              y));
  } in foldl verify_undefined_io_pair (fst tG2) undefined_io_pairs;

emptye :: forall a. Mp_trie a;
emptye = MP_Trie [];

paths :: forall a. Mp_trie a -> [[a]];
paths (MP_Trie []) = [[]];
paths (MP_Trie (t : ts)) =
  concatMap (\ (x, ta) -> map (\ a -> x : a) (paths ta)) (t : ts);

ofsm_table ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> (a -> Set a) -> Nat -> a -> Set a;
ofsm_table m f k q =
  (if equal_nat k zero_nat then (if member q (states m) then f q else bot_set)
    else let {
           prev_table = ofsm_table m f (minus_nat k one_nat);
         } in filtera
                (\ qa ->
                  ball (inputs m)
                    (\ x ->
                      ball (outputs m)
                        (\ y ->
                          (case h_obs m q x y of {
                            Nothing -> is_none (h_obs m qa x y);
                            Just qT ->
                              (case h_obs m qa x y of {
                                Nothing -> False;
                                Just qTa ->
                                  set_eq (prev_table qT) (prev_table qTa);
                              });
                          }))))
                (prev_table q));

min :: forall a. (Ord a) => a -> a -> a;
min a b = (if less_eq a b then a else b);

select_inputs ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Card_UNIV c, Ceq c,
      Cproper_interval c) => ((a, b) -> Set (c, a)) ->
                               a -> [b] -> [a] -> Set a -> [(a, b)] -> [(a, b)];
select_inputs f q0 inputList [] stateSet m =
  (case find (\ x ->
               not (is_empty (f (q0, x))) &&
                 ball (f (q0, x)) (\ (_, q) -> member q stateSet))
          inputList
    of {
    Nothing -> m;
    Just x -> m ++ [(q0, x)];
  });
select_inputs f q0 inputList (n : nL) stateSet m =
  (case find (\ x ->
               not (is_empty (f (q0, x))) &&
                 ball (f (q0, x)) (\ (_, q) -> member q stateSet))
          inputList
    of {
    Nothing ->
      (case find_remove_2
              (\ q x ->
                not (is_empty (f (q, x))) &&
                  ball (f (q, x)) (\ (_, qa) -> member qa stateSet))
              (n : nL) inputList
        of {
        Nothing -> m;
        Just (q, (x, stateList)) ->
          select_inputs f q0 inputList stateList (insert q stateSet)
            (m ++ [(q, x)]);
      });
    Just x -> m ++ [(q0, x)];
  });

d_states ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Card_UNIV c, Ceq c, Cproper_interval c,
      Set_impl c) => Fsm a b c -> a -> [(a, b)];
d_states m q =
  (if q == initial m then []
    else select_inputs (h m) (initial m) (inputs_as_list m)
           (removeAll q (removeAll (initial m) (states_as_list m)))
           (insert q bot_set) []);

list_ordered_pairs :: forall a. [a] -> [(a, a)];
list_ordered_pairs [] = [];
list_ordered_pairs (x : xs) = map (\ a -> (x, a)) xs ++ list_ordered_pairs xs;

minus_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
minus_set (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "minus RBT_set RBT_set: ccompare = None"
        (\ _ -> minus_set (RBT_set rbt1) (RBT_set rbt2));
    Just _ -> RBT_set (minus rbt1 rbt2);
  });
minus_set a b = inf_set a (uminus_set b);

distinguishing_transitions ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a,
      Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Set_impl b,
      Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c,
      Set_impl c) => ((a, b) -> Set c) ->
                       a -> a -> Set (a, a) ->
                                   Set b ->
                                     Set (Sum (a, a) a, (b, (c, Sum (a, a) a)));
distinguishing_transitions f q1 q2 stateSet inputSet =
  sup_seta
    (image
      (\ (a, b) ->
        (case a of {
          (q1a, q2a) ->
            (\ x ->
              sup_set
                (image (\ y -> (Inl (q1a, q2a), (x, (y, Inr q1))))
                  (minus_set (f (q1a, x)) (f (q2a, x))))
                (image (\ y -> (Inl (q1a, q2a), (x, (y, Inr q2))))
                  (minus_set (f (q2a, x)) (f (q1a, x)))));
        })
          b)
      (producte stateSet inputSet));

shifted_transitions ::
  forall a b c d.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c,
      Ccompare c, Set_impl c, Ceq d, Ccompare d,
      Set_impl d) => Set ((a, a), (b, (c, (a, a)))) ->
                       Set (Sum (a, a) d, (b, (c, Sum (a, a) d)));
shifted_transitions ts =
  image (\ t ->
          (Inl (fst t),
            (fst (snd t), (fst (snd (snd t)), Inl (snd (snd (snd t)))))))
    ts;

canonical_separator_impl ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations_impl a b c ->
                       Fsm_with_precomputations_impl (a, a) b c ->
                         a -> a -> Fsm_with_precomputations_impl (Sum (a, a) a)
                                     b c;
canonical_separator_impl m p q1 q2 =
  (if initial_wpi p == (q1, q2)
    then let {
           f = set_as_mapping_image (transitions_wpi m)
                 (\ (q, (x, (y, _))) -> ((q, x), y));
           fa = (\ qx -> (case lookupa f qx of {
                           Nothing -> bot_set;
                           Just yqs -> yqs;
                         }));
           shifted_transitionsa = shifted_transitions (transitions_wpi p);
           distinguishing_transitions_lr =
             distinguishing_transitions fa q1 q2 (states_wpi p) (inputs_wpi p);
           ts = sup_set shifted_transitionsa distinguishing_transitions_lr;
           h = set_as_mapping_image ts
                 (\ (q, (x, (y, qa))) -> ((q, x), (y, qa)));
         } in FSMWPI (Inl (q1, q2))
                (sup_set (image Inl (states_wpi p))
                  (insert (Inr q1) (insert (Inr q2) bot_set)))
                (sup_set (inputs_wpi m) (inputs_wpi p))
                (sup_set (outputs_wpi m) (outputs_wpi p)) ts h
                (h_obs_impl_from_h h)
    else FSMWPI (Inl (q1, q2)) (insert (Inl (q1, q2)) bot_set) bot_set bot_set
           bot_set emptya emptya);

canonical_separatorc ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_with_precomputations a b c ->
                       Fsm_with_precomputations (a, a) b c ->
                         a -> a -> Fsm_with_precomputations (Sum (a, a) a) b c;
canonical_separatorc xc xe xf xg =
  Fsm_with_precomputations
    (canonical_separator_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xc)
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xe) xf xg);

canonical_separatora ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm_impl a b c ->
                       Fsm_impl (a, a) b c ->
                         a -> a -> Fsm_impl (Sum (a, a) a) b c;
canonical_separatora (FSMWP m) (FSMWP p) q1 q2 =
  FSMWP (canonical_separatorc m p q1 q2);

canonical_separator ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c ->
                       Fsm (a, a) b c -> a -> a -> Fsm (Sum (a, a) a) b c;
canonical_separator xc xe xf xg =
  Abs_fsm
    (canonical_separatora (fsm_impl_of_fsm xc) (fsm_impl_of_fsm xe) xf xg);

completely_specified ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Ceq b, Ccompare b, Eq b, Ceq c,
      Ccompare c) => Fsm a b c -> Bool;
completely_specified m =
  ball (states m)
    (\ q ->
      ball (inputs m)
        (\ x -> bex (transitions m) (\ t -> fst t == q && fst (snd t) == x)));

index_states_integer ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Linorder a, Ceq b, Ccompare b, Eq b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Fsm Integer b c;
index_states_integer m =
  rename_states m (integer_of_nat . assign_indices (states m));

sorted_list_of_fset ::
  forall a. (Ceq a, Ccompare a, Eq a, Linorder a) => Fset a -> [a];
sorted_list_of_fset xa = sorted_list_of_set (fset xa);

one_int :: Int;
one_int = Int_of_integer (1 :: Integer);

update_assoc_list_with_default ::
  forall a b. (Eq a) => a -> (b -> b) -> b -> [(a, b)] -> [(a, b)];
update_assoc_list_with_default k f d [] = [(k, f d)];
update_assoc_list_with_default k f d ((x, y) : xys) =
  (if k == x then (x, f y) : xys
    else (x, y) : update_assoc_list_with_default k f d xys);

insertd :: forall a. (Eq a) => [a] -> Mp_trie a -> Mp_trie a;
insertd [] t = t;
insertd (x : xs) (MP_Trie ts) =
  MP_Trie (update_assoc_list_with_default x (insertd xs) emptye ts);

integer_of_int :: Int -> Integer;
integer_of_int (Int_of_integer k) = k;

less_int :: Int -> Int -> Bool;
less_int k l = integer_of_int k < integer_of_int l;

combine_after ::
  forall a.
    (Ccompare a, Eq a,
      Mapping_impl a) => Prefix_tree a -> [a] -> Prefix_tree a -> Prefix_tree a;
combine_after (MPT m) xs t =
  (case xs of {
    [] -> combinea (MPT m) t;
    x : xsa -> MPT (updateb x (combine_after (case lookupa m x of {
       Nothing -> emptyc;
       Just ta -> ta;
     })
                                xsa t)
                     m);
  });

is_maximal_in ::
  forall a. (Ccompare a, Eq a, Mapping_impl a) => Prefix_tree a -> [a] -> Bool;
is_maximal_in t alpha = isin t alpha && is_leaf (aftera t alpha);

int_of_nat :: Nat -> Int;
int_of_nat n = Int_of_integer (integer_of_nat n);

initial_singleton_impl ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm_with_precomputations_impl a b c ->
                       Fsm_with_precomputations_impl a b c;
initial_singleton_impl m =
  FSMWPI (initial_wpi m) (insert (initial_wpi m) bot_set) (inputs_wpi m)
    (outputs_wpi m) bot_set emptya emptya;

initial_singletona ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm_with_precomputations a b c ->
                       Fsm_with_precomputations a b c;
initial_singletona xa =
  Fsm_with_precomputations
    (initial_singleton_impl
      (fsm_with_precomputations_impl_of_fsm_with_precomputations xa));

initial_singleton ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm_impl a b c -> Fsm_impl a b c;
initial_singleton (FSMWP m) = FSMWP (initial_singletona m);

zero_int :: Int;
zero_int = Int_of_integer (0 :: Integer);

maximum_prefix :: forall a. (Ccompare a, Eq a) => Prefix_tree a -> [a] -> [a];
maximum_prefix (MPT m) xs = (case xs of {
                              [] -> [];
                              x : xsa -> (case lookupa m x of {
   Nothing -> [];
   Just t -> x : maximum_prefix t xsa;
 });
                            });

bot_fset :: forall a. (Ceq a, Ccompare a, Set_impl a) => Fset a;
bot_fset = Abs_fset bot_set;

sup_fset :: forall a. (Ceq a, Ccompare a) => Fset a -> Fset a -> Fset a;
sup_fset xb xc = Abs_fset (sup_set (fset xb) (fset xc));

from_lista :: forall a. (Eq a) => [[a]] -> Mp_trie a;
from_lista seqs = foldr insertd seqs emptye;

ofsm_table_fix ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> (a -> Set a) -> Nat -> a -> Set a;
ofsm_table_fix m f k =
  let {
    cur_table = ofsm_table m (\ q -> inf_set (f q) (states m)) k;
    next_table = ofsm_table m (\ q -> inf_set (f q) (states m)) (suc k);
  } in (if ball (states m) (\ q -> set_eq (cur_table q) (next_table q))
         then cur_table else ofsm_table_fix m f (suc k));

language_up_to_length_with_extensions ::
  forall a b c.
    a -> (a -> b -> [(c, a)]) -> [b] -> [[(b, c)]] -> Nat -> [[(b, c)]];
language_up_to_length_with_extensions q hM iM ex k =
  (if equal_nat k zero_nat then ex
    else ex ++ concatMap
                 (\ x ->
                   concatMap
                     (\ (y, qa) ->
                       map (\ a -> (x, y) : a)
                         (language_up_to_length_with_extensions qa hM iM ex
                           (minus_nat k one_nat)))
                     (hM q x))
                 iM);

h_extensions ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c, Set_impl c) => Fsm a b c -> a -> Nat -> [[(b, c)]];
h_extensions m q k =
  let {
    iM = inputs_as_list m;
    ex = map (\ xy -> [xy]) (product iM (outputs_as_list m));
    hM = (\ qa x -> sorted_list_of_set (h m (qa, x)));
  } in language_up_to_length_with_extensions q hM iM ex k;

spy_framework ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c,
      Ceq c, Cproper_interval c, Eq c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Fsm a b c -> a -> [(b, c)]) ->
                         (Fsm a b c ->
                           (a -> [(b, c)]) ->
                             (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                               (d -> [(b, c)] -> d) ->
                                 (d -> [(b, c)] -> [[(b, c)]]) ->
                                   (Prefix_tree (b, c), d)) ->
                           (Fsm a b c ->
                             (a -> [(b, c)]) ->
                               [(a, (b, (c, a)))] -> [(a, (b, (c, a)))]) ->
                             (Fsm a b c ->
                               (a -> [(b, c)]) ->
                                 Prefix_tree (b, c) ->
                                   d -> (d -> [(b, c)] -> d) ->
  (d -> [(b, c)] -> [[(b, c)]]) ->
    Nat -> (a, (b, (c, a))) -> (Prefix_tree (b, c), d)) ->
                               (Fsm a b c ->
                                 (a -> [(b, c)]) ->
                                   Prefix_tree (b, c) ->
                                     d -> (d -> [(b, c)] -> d) ->
    (d -> [(b, c)] -> [[(b, c)]]) -> a -> b -> c -> (Prefix_tree (b, c), d)) ->
                                 (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                                   (d -> [(b, c)] -> d) ->
                                     (d -> [(b, c)] -> [[(b, c)]]) ->
                                       (d -> [(b, c)] -> [(b, c)] -> d) ->
 Nat -> Prefix_tree (b, c);
spy_framework ma get_state_cover separate_state_cover
  sort_unverified_transitions establish_convergence append_io_pair cg_initial
  cg_insert cg_lookup cg_merge m =
  let {
    rstates_set = reachable_states ma;
    rstates = reachable_states_as_list ma;
    rstates_io =
      product rstates (product (inputs_as_list ma) (outputs_as_list ma));
    undefined_io_pairs =
      filter (\ (q, (x, y)) -> is_none (h_obs ma q x y)) rstates_io;
    v = get_state_cover ma;
    _ = card (reachable_states ma);
    tG1 = separate_state_cover ma v cg_initial cg_insert cg_lookup;
    sc_covered_transitions =
      sup_seta (image (\ q -> covered_transitions ma v (v q)) rstates_set);
    unverified_transitions =
      sort_unverified_transitions ma v
        (filter
          (\ t ->
            member (fst t) rstates_set && not (member t sc_covered_transitions))
          (transitions_as_list ma));
    verify_transition =
      (\ (t, g) ta ->
        let {
          tGxy =
            append_io_pair ma v t g cg_insert cg_lookup (fst ta) (fst (snd ta))
              (fst (snd (snd ta)));
        } in (case establish_convergence ma v (fst tGxy) (snd tGxy) cg_insert
                     cg_lookup
                     m
                     ta
               of {
               (tb, ga) ->
                 let {
                   a = cg_merge ga
                         (v (fst ta) ++ [(fst (snd ta), fst (snd (snd ta)))])
                         (v (snd (snd (snd ta))));
                 } in (tb, a);
             }));
    tG2 = foldl verify_transition tG1 unverified_transitions;
    verify_undefined_io_pair =
      (\ t (q, (x, y)) ->
        fst (append_io_pair ma v t (snd tG2) cg_insert cg_lookup q x y));
  } in foldl verify_undefined_io_pair (fst tG2) undefined_io_pairs;

remove_proper_prefixes ::
  forall a. (Ceq a, Ccompare a, Eq a) => Set [a] -> Set [a];
remove_proper_prefixes (RBT_set t) =
  (case (ccompare_list :: Maybe ([a] -> [a] -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove_proper_prefixes RBT_set: ccompare = None"
        (\ _ -> remove_proper_prefixes (RBT_set t));
    Just _ ->
      (if is_emptya t then set_empty (of_phantom set_impl_list)
        else set (paths (from_lista (keysb t))));
  });

equal_int :: Int -> Int -> Bool;
equal_int k l = integer_of_int k == integer_of_int l;

minus_fset :: forall a. (Ceq a, Ccompare a) => Fset a -> Fset a -> Fset a;
minus_fset xb xc = Abs_fset (minus_set (fset xb) (fset xc));

make_observable_transitions ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a,
      Infinite_UNIV a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Set_impl b, Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Set_impl c) => Fset (a, (b, (c, a))) ->
                       Fset (Fset a) ->
                         Fset (Fset a) ->
                           Fset (Fset a, (b, (c, Fset a))) ->
                             Fset (Fset a, (b, (c, Fset a)));
make_observable_transitions base_trans nexts dones ts =
  let {
    qtrans =
      ffUnion
        (fimage
          (\ q ->
            let {
              qts = ffilter (\ t -> member (fst t) (fset q)) base_trans;
              a = fimage (\ t -> (fst (snd t), fst (snd (snd t)))) qts;
            } in fimage
                   (\ (x, y) ->
                     (q, (x, (y, fimage (\ aa -> snd (snd (snd aa)))
                                   (ffilter
                                     (\ t ->
                                       (fst (snd t), fst (snd (snd t))) ==
 (x, y))
                                     qts)))))
                   a)
          nexts);
    donesa = sup_fset dones nexts;
    tsa = sup_fset ts qtrans;
    nextsa = minus_fset (fimage (\ a -> snd (snd (snd a))) qtrans) donesa;
  } in (if nextsa == bot_fset then tsa
         else make_observable_transitions base_trans nextsa donesa tsa);

create_unconnected_fsm_from_fsets_impl ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => a -> Fset a ->
                            Fset b ->
                              Fset c -> Fsm_with_precomputations_impl a b c;
create_unconnected_fsm_from_fsets_impl q ns ins outs =
  FSMWPI q (insert q (fset ns)) (fset ins) (fset outs) bot_set emptya emptya;

create_unconnected_fsm_from_fsetsb ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => a -> Fset a ->
                            Fset b -> Fset c -> Fsm_with_precomputations a b c;
create_unconnected_fsm_from_fsetsb q ns ins outs =
  Fsm_with_precomputations
    (create_unconnected_fsm_from_fsets_impl q ns ins outs);

create_unconnected_fsm_from_fsetsa ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => a -> Fset a -> Fset b -> Fset c -> Fsm_impl a b c;
create_unconnected_fsm_from_fsetsa q ns ins outs =
  FSMWP (create_unconnected_fsm_from_fsetsb q ns ins outs);

create_unconnected_fsm_from_fsets ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => a -> Fset a -> Fset b -> Fset c -> Fsm a b c;
create_unconnected_fsm_from_fsets xc xd xe xf =
  Abs_fsm (create_unconnected_fsm_from_fsetsa xc xd xe xf);

make_observable ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Infinite_UNIV a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Fsm (Fset a) b c;
make_observable m =
  let {
    initial_trans =
      let {
        qts = ffilter (\ t -> fst t == initial m) (ftransitions m);
        a = fimage (\ t -> (fst (snd t), fst (snd (snd t)))) qts;
      } in fimage
             (\ (x, y) ->
               (finsert (initial m) bot_fset,
                 (x, (y, fimage (\ aa -> snd (snd (snd aa)))
                           (ffilter
                             (\ t -> (fst (snd t), fst (snd (snd t))) == (x, y))
                             qts)))))
             a;
    nexts =
      minus_fset (fimage (\ a -> snd (snd (snd a))) initial_trans)
        (finsert (finsert (initial m) bot_fset) bot_fset);
    ptransitions =
      make_observable_transitions (ftransitions m) nexts
        (finsert (finsert (initial m) bot_fset) bot_fset) initial_trans;
    pstates =
      finsert (finsert (initial m) bot_fset)
        (fimage (\ a -> snd (snd (snd a))) ptransitions);
    ma = create_unconnected_fsm_from_fsets (finsert (initial m) bot_fset)
           pstates (finputs m) (foutputs m);
  } in add_transitions ma (fset ptransitions);

pair_framework ::
  forall a b c.
    (Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ccompare c, Eq c,
      Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       Nat ->
                         (Fsm a b c -> Nat -> Prefix_tree (b, c)) ->
                           (Fsm a b c ->
                             Nat -> [(([(b, c)], a), ([(b, c)], a))]) ->
                             (Fsm a b c ->
                               (([(b, c)], a), ([(b, c)], a)) ->
                                 Prefix_tree (b, c) -> Prefix_tree (b, c)) ->
                               Prefix_tree (b, c);
pair_framework ma m get_initial_test_suite get_pairs get_separating_traces =
  let {
    ts = get_initial_test_suite ma m;
    d = get_pairs ma m;
    dist_extension =
      (\ t (a, b) ->
        (case a of {
          (alpha, q) ->
            (\ (beta, qa) ->
              let {
                tDist = get_separating_traces ma ((alpha, q), (beta, qa)) t;
              } in combine_after (combine_after t alpha tDist) beta tDist);
        })
          b);
  } in foldl dist_extension ts d;

fset_states_to_list_states ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Ceq b, Ccompare b, Eq b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm (Fset a) b c -> Fsm [a] b c;
fset_states_to_list_states m = rename_states m sorted_list_of_fset;

set_states_to_list_states ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm (Set a) b c -> Fsm [a] b c;
set_states_to_list_states m = rename_states m sorted_list_of_set;

initial_ofsm_table ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a,
      Linorder a) => Fsm a b c -> Mapping a (Set a);
initial_ofsm_table m = tabulate (states_as_list m) (\ _ -> states m);

next_ofsm_table ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> Mapping a (Set a) -> Mapping a (Set a);
next_ofsm_table m prev_table =
  tabulate (states_as_list m)
    (\ q ->
      filtera
        (\ qa ->
          ball (inputs m)
            (\ x ->
              ball (outputs m)
                (\ y ->
                  (case h_obs m q x y of {
                    Nothing -> is_none (h_obs m qa x y);
                    Just qT ->
                      (case h_obs m qa x y of {
                        Nothing -> False;
                        Just qTa ->
                          set_eq (lookup_default bot_set prev_table qT)
                            (lookup_default bot_set prev_table qTa);
                      });
                  }))))
        (lookup_default bot_set prev_table q));

compute_ofsm_table_list ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> Nat -> [Mapping a (Set a)];
compute_ofsm_table_list m k =
  reverse
    (foldr (\ _ prev -> next_ofsm_table m (hd prev) : prev) (upt zero_nat k)
      [initial_ofsm_table m]);

compute_ofsm_tables ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> Nat -> Mapping Nat (Mapping a (Set a));
compute_ofsm_tables m k = bulkload (compute_ofsm_table_list m k);

minimise_refined ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b,
      Linorder b, Set_impl b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c, Set_impl c) => Fsm a b c -> Fsm (Set a) b c;
minimise_refined m =
  let {
    tables = compute_ofsm_tables m (minus_nat (size m) one_nat);
    eq_class =
      lookup_default bot_set
        (the (lookupa tables (minus_nat (size m) one_nat)));
    ts = image (\ t ->
                 (eq_class (fst t),
                   (fst (snd t),
                     (fst (snd (snd t)), eq_class (snd (snd (snd t)))))))
           (transitions m);
    q0 = eq_class (initial m);
    eq_states = fimage eq_class (fstates m);
    ma = create_unconnected_fsm_from_fsets q0 eq_states (finputs m)
           (foutputs m);
  } in add_transitions ma ts;

restrict_to_reachable_states ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c -> Fsm a b c;
restrict_to_reachable_states m =
  let {
    path_assignments =
      reaching_paths_up_to_depth m (insert (initial m) bot_set) bot_set
        (fun_upd (\ _ -> Nothing) (initial m) (Just []))
        (minus_nat (size m) one_nat);
  } in filter_states m (\ q -> not (is_none (path_assignments q)));

to_prime ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Infinite_UNIV a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Fsm Integer b c;
to_prime m =
  restrict_to_reachable_states
    (index_states_integer
      (set_states_to_list_states
        (minimise_refined
          (index_states
            (fset_states_to_list_states
              (make_observable (restrict_to_reachable_states m)))))));

maximal_prefix_in_language ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Ccompare c,
      Eq c) => Fsm a b c -> a -> [(b, c)] -> [(b, c)];
maximal_prefix_in_language m q [] = [];
maximal_prefix_in_language m q ((x, y) : io) =
  (case h_obs m q x y of {
    Nothing -> [];
    Just qa -> (x, y) : maximal_prefix_in_language m qa io;
  });

uminus_int :: Int -> Int;
uminus_int k = Int_of_integer (negate (integer_of_int k));

initial_preamble ::
  forall a b c.
    (Ceq a, Ccompare a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Mapping_impl b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> Fsm a b c;
initial_preamble xa = Abs_fsm (initial_singleton (fsm_impl_of_fsm xa));

maximum_fst_prefixes ::
  forall a b.
    (Ccompare a, Eq a, Ccompare b,
      Eq b) => Prefix_tree (a, b) -> [a] -> [b] -> [[(a, b)]];
maximum_fst_prefixes (MPT m) xs ys =
  (case xs of {
    [] -> (if is_leaf (MPT m) then [[]] else []);
    x : xsa ->
      (if is_leaf (MPT m) then [[]]
        else concat
               (map_filter
                 (\ xa ->
                   (if not (is_none (lookupa m (x, xa)))
                     then Just (map (\ a -> (x, xa) : a)
                                 (maximum_fst_prefixes (the (lookupa m (x, xa)))
                                   xsa ys))
                     else Nothing))
                 ys));
  });

pairs_to_distinguish ::
  forall a b c.
    (Eq a, Linorder a, Linorder b,
      Linorder c) => Fsm a b c ->
                       (a -> [(b, c)]) ->
                         (a -> [([(a, (b, (c, a)))], a)]) ->
                           [a] -> [(([(b, c)], a), ([(b, c)], a))];
pairs_to_distinguish m v x rstates =
  let {
    pi = map (\ q -> (v q, q)) rstates;
    a = product pi pi;
    b = product pi
          (concatMap
            (\ q ->
              map (\ (tau, aa) ->
                    (v q ++ map (\ t -> (fst (snd t), fst (snd (snd t)))) tau,
                      aa))
                (x q))
            rstates);
    c = concatMap
          (\ q ->
            concatMap
              (\ (tau, qa) ->
                map (\ taua ->
                      ((v q ++
                          map (\ t -> (fst (snd t), fst (snd (snd t)))) taua,
                         target q taua),
                        (v q ++
                           map (\ t -> (fst (snd t), fst (snd (snd t)))) tau,
                          qa)))
                  (prefixes tau))
              (x q))
          rstates;
  } in filter (\ (aa, ba) -> (case aa of {
                               (_, q) -> (\ (_, qa) -> not (q == qa));
                             })
                               ba)
         (a ++ b ++ c);

canonical_separatorb ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> a -> a -> Fsm (Sum (a, a) a) b c;
canonical_separatorb m q1 q2 =
  canonical_separator m (productd (from_FSM m q1) (from_FSM m q2)) q1 q2;

select_diverging_ofsm_table_io ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> a -> a -> Nat -> ((b, c), (Maybe a, Maybe a));
select_diverging_ofsm_table_io m q1 q2 k =
  let {
    ins = inputs_as_list m;
    outs = outputs_as_list m;
    table = ofsm_table m (\ _ -> states m) (minus_nat k one_nat);
    f = (\ (x, y) ->
          (case (h_obs m q1 x y, h_obs m q2 x y) of {
            (Nothing, Nothing) -> Nothing;
            (Nothing, Just q2a) -> Just ((x, y), (Nothing, Just q2a));
            (Just q1a, Nothing) -> Just ((x, y), (Just q1a, Nothing));
            (Just q1a, Just q2a) ->
              (if not (set_eq (table q1a) (table q2a))
                then Just ((x, y), (Just q1a, Just q2a)) else Nothing);
          }));
  } in hd (map_filter f (product ins outs));

assemble_distinguishing_sequence_from_ofsm_table ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> a -> a -> Nat -> [(b, c)];
assemble_distinguishing_sequence_from_ofsm_table m q1 q2 k =
  (if equal_nat k zero_nat then []
    else (case select_diverging_ofsm_table_io m q1 q2
                 (suc (minus_nat k one_nat))
           of {
           ((x, y), (Nothing, _)) -> [(x, y)];
           ((x, y), (Just _, Nothing)) -> [(x, y)];
           ((x, y), (Just q1a, Just q2a)) ->
             (x, y) :
               assemble_distinguishing_sequence_from_ofsm_table m q1a q2a
                 (minus_nat k one_nat);
         }));

find_first_distinct_ofsm_table_no_check ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Ceq c, Ccompare c, Eq c) => Fsm a b c -> a -> a -> Nat -> Nat;
find_first_distinct_ofsm_table_no_check m q1 q2 k =
  (if not (set_eq (ofsm_table m (\ _ -> states m) k q1)
            (ofsm_table m (\ _ -> states m) k q2))
    then k else find_first_distinct_ofsm_table_no_check m q1 q2 (suc k));

find_first_distinct_ofsm_table_gta ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Ceq c, Ccompare c, Eq c) => Fsm a b c -> a -> a -> Nat -> Nat;
find_first_distinct_ofsm_table_gta m q1 q2 k =
  (if member q1 (states m) &&
        member q2 (states m) &&
          not (member q2 (ofsm_table_fix m (\ _ -> states m) zero_nat q1))
    then find_first_distinct_ofsm_table_no_check m q1 q2 k else zero_nat);

find_first_distinct_ofsm_table_gt ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Ceq c, Ccompare c, Eq c) => Fsm a b c -> a -> a -> Nat -> Nat;
find_first_distinct_ofsm_table_gt m q1 q2 k =
  find_first_distinct_ofsm_table_gta m q1 q2 k;

get_distinguishing_sequence_from_ofsm_tables ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b,
      Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> a -> a -> [(b, c)];
get_distinguishing_sequence_from_ofsm_tables m q1 q2 =
  let {
    a = find_first_distinct_ofsm_table_gt m q1 q2 zero_nat;
  } in assemble_distinguishing_sequence_from_ofsm_table m q1 q2 a;

get_HSI ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c, Linorder c) => Fsm a b c -> a -> Prefix_tree (b, c);
get_HSI m q =
  from_list
    (map_filter
      (\ x ->
        (if not (q == x)
          then Just (get_distinguishing_sequence_from_ofsm_tables m q x)
          else Nothing))
      (states_as_list m));

has_leaf ::
  forall a b c.
    (Ccompare a, Eq a, Mapping_impl a, Ccompare b, Eq b,
      Mapping_impl b) => Prefix_tree (a, b) ->
                           c -> (c -> [(a, b)] -> [[(a, b)]]) ->
                                  [(a, b)] -> Bool;
has_leaf t g cg_lookup alpha =
  not (is_none (find (is_maximal_in t) (alpha : cg_lookup g alpha)));

empty_cg_empty :: ();
empty_cg_empty = ();

empty_cg_merge :: forall a b. () -> [(a, b)] -> [(a, b)] -> ();
empty_cg_merge g u v = empty_cg_empty;

get_state_cover_assignment ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Fsm a b c -> a -> [(b, c)];
get_state_cover_assignment m =
  let {
    path_assignments =
      reaching_paths_up_to_depth m (insert (initial m) bot_set) bot_set
        (fun_upd (\ _ -> Nothing) (initial m) (Just []))
        (minus_nat (size m) one_nat);
  } in (\ q -> (case path_assignments q of {
                 Nothing -> [];
                 Just a -> map (\ t -> (fst (snd t), fst (snd (snd t)))) a;
               }));

acyclic_language_intersection ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d,
      Cenum d, Ceq d, Cproper_interval d, Eq d, Mapping_impl d,
      Set_impl d) => Fsm a b c -> Fsm d b c -> Set [(b, c)];
acyclic_language_intersection m a =
  let {
    p = productd m a;
  } in image (map (\ t -> (fst (snd t), fst (snd (snd t)))))
         (acyclic_paths_up_to_length p (initial p)
           (minus_nat (size a) one_nat));

test_suite_to_io_maximal ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c,
      Ceq c, Cproper_interval c, Eq c, Mapping_impl c, Set_impl c, Card_UNIV d,
      Cenum d, Ceq d, Cproper_interval d, Eq d, Mapping_impl d, Linorder d,
      Set_impl d) => Fsm a b c -> Test_suite a b c d -> Set [(b, c)];
test_suite_to_io_maximal m (Test_Suite prs tps rd_targets atcs) =
  remove_proper_prefixes
    (sup_seta
      (image
        (\ (q, p) ->
          sup_set (lS_acyclic p (initial p))
            (sup_seta
              (image
                (\ ioP ->
                  sup_seta
                    (image
                      (\ pt ->
                        insert
                          (ioP ++
                            map (\ t -> (fst (snd t), fst (snd (snd t)))) pt)
                          (sup_seta
                            (image
                              (\ qa ->
                                sup_seta
                                  (image
                                    (\ (a, (_, _)) ->
                                      image
(\ io_atc -> ioP ++ map (\ t -> (fst (snd t), fst (snd (snd t)))) pt ++ io_atc)
(remove_proper_prefixes
  (acyclic_language_intersection (from_FSM m (target q pt)) a)))
                                    (atcs (target q pt, qa))))
                              (rd_targets (q, pt)))))
                      (tps q)))
                (remove_proper_prefixes (lS_acyclic p (initial p))))))
        prs));

empty_cg_insert :: forall a b. () -> [(a, b)] -> ();
empty_cg_insert g v = empty_cg_empty;

empty_cg_lookup :: forall a b. () -> [(a, b)] -> [[(a, b)]];
empty_cg_lookup g v = [v];

extend_until_conflict ::
  forall a. (Ceq a, Ccompare a) => Set (a, a) -> [a] -> [a] -> Nat -> [a];
extend_until_conflict non_confl_set candidates xs k =
  (if equal_nat k zero_nat then xs
    else (case dropWhile
                 (\ x ->
                   not (is_none
                         (find (\ y -> not (member (x, y) non_confl_set)) xs)))
                 candidates
           of {
           [] -> xs;
           c : cs ->
             extend_until_conflict non_confl_set cs (c : xs)
               (minus_nat k one_nat);
         }));

empty_cg_initial :: forall a b c. Fsm a b c -> Prefix_tree (b, c) -> ();
empty_cg_initial m t = empty_cg_empty;

paths_up_to_length_with_targets ::
  forall a b c.
    a -> (a -> b -> [(a, (b, (c, a)))]) ->
           [b] -> Nat -> [([(a, (b, (c, a)))], a)];
paths_up_to_length_with_targets q hM iM k =
  (if equal_nat k zero_nat then [([], q)]
    else ([], q) :
           concatMap
             (\ x ->
               concatMap
                 (\ t ->
                   map (\ (p, a) -> (t : p, a))
                     (paths_up_to_length_with_targets (snd (snd (snd t))) hM iM
                       (minus_nat k one_nat)))
                 (hM q x))
             iM);

get_pairs_H ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Linorder c,
      Set_impl c) => (a -> [(b, c)]) ->
                       Fsm a b c -> Nat -> [(([(b, c)], a), ([(b, c)], a))];
get_pairs_H v ma m =
  let {
    rstates = reachable_states_as_list ma;
    n = card (reachable_states ma);
    iM = inputs_as_list ma;
    hMap =
      map_of
        (map (\ (q, x) ->
               ((q, x),
                 map (\ (y, qa) -> (q, (x, (y, qa))))
                   (sorted_list_of_set (h ma (q, x)))))
          (product (states_as_list ma) iM));
    hM = (\ q x -> (case hMap (q, x) of {
                     Nothing -> [];
                     Just ts -> ts;
                   }));
    pairs =
      pairs_to_distinguish ma v
        (\ q ->
          paths_up_to_length_with_targets q hM iM
            (plus_nat (minus_nat m n) one_nat))
        rstates;
  } in pairs;

simple_cg_empty :: forall a. [Fset [a]];
simple_cg_empty = [];

can_merge_by_intersection ::
  forall a. (Ceq a, Ccompare a) => Fset [a] -> Fset [a] -> Bool;
can_merge_by_intersection x1 x2 =
  bex (fset x1) (\ alpha -> member alpha (fset x2));

simple_cg_closure_phase_2_helper ::
  forall a.
    (Ceq a,
      Ccompare a) => Fset [a] -> [Fset [a]] -> (Bool, (Fset [a], [Fset [a]]));
simple_cg_closure_phase_2_helper x1 xs =
  (case separate_by (can_merge_by_intersection x1) xs of {
    (x2s, others) -> let {
                       x1Union = foldl sup_fset x1 x2s;
                     } in (not (null x2s), (x1Union, others));
  });

simple_cg_closure_phase_2a ::
  forall a.
    (Ceq a,
      Ccompare a) => [Fset [a]] -> (Bool, [Fset [a]]) -> (Bool, [Fset [a]]);
simple_cg_closure_phase_2a [] (b, done) = (b, done);
simple_cg_closure_phase_2a (x : xs) (b, done) =
  (case simple_cg_closure_phase_2_helper x xs of {
    (True, (xa, xsa)) -> simple_cg_closure_phase_2a xsa (True, xa : done);
    (False, (_, _)) -> simple_cg_closure_phase_2a xs (b, x : done);
  });

simple_cg_closure_phase_2 ::
  forall a. (Ceq a, Ccompare a) => [Fset [a]] -> (Bool, [Fset [a]]);
simple_cg_closure_phase_2 xs = simple_cg_closure_phase_2a xs (False, []);

can_merge_by_suffix ::
  forall a.
    (Ceq a, Ccompare a, Eq a) => Fset [a] -> Fset [a] -> Fset [a] -> Bool;
can_merge_by_suffix x x1 x2 =
  bex (fset x)
    (\ ys ->
      bex (fset x1)
        (\ ys1 ->
          is_prefix ys ys1 &&
            bex (fset x)
              (\ ysa -> member (ysa ++ drop (size_list ys) ys1) (fset x2))));

simple_cg_closure_phase_1_helpera ::
  forall a.
    (Ceq a, Ccompare a,
      Eq a) => Fset [a] ->
                 Fset [a] -> [Fset [a]] -> (Bool, (Fset [a], [Fset [a]]));
simple_cg_closure_phase_1_helpera x x1 xs =
  (case separate_by (can_merge_by_suffix x x1) xs of {
    (x2s, others) -> let {
                       x1Union = foldl sup_fset x1 x2s;
                     } in (not (null x2s), (x1Union, others));
  });

simple_cg_closure_phase_1_helper ::
  forall a.
    (Ceq a, Ccompare a,
      Eq a) => Fset [a] ->
                 [Fset [a]] -> (Bool, [Fset [a]]) -> (Bool, [Fset [a]]);
simple_cg_closure_phase_1_helper x [] (b, done) = (b, done);
simple_cg_closure_phase_1_helper x (x1 : xs) (b, done) =
  (case simple_cg_closure_phase_1_helpera x x1 xs of {
    (hasChanged, (x1a, xsa)) ->
      simple_cg_closure_phase_1_helper x xsa (b || hasChanged, x1a : done);
  });

simple_cg_closure_phase_1 ::
  forall a. (Ceq a, Ccompare a, Eq a) => [Fset [a]] -> (Bool, [Fset [a]]);
simple_cg_closure_phase_1 xs =
  foldl (\ (b, xsa) x ->
          (case simple_cg_closure_phase_1_helper x xsa (False, []) of {
            (ba, a) -> (b || ba, a);
          }))
    (False, xs) xs;

simple_cg_closure ::
  forall a. (Ceq a, Ccompare a, Eq a) => [Fset [a]] -> [Fset [a]];
simple_cg_closure g =
  (case simple_cg_closure_phase_1 g of {
    (hasChanged1, g1) ->
      (case simple_cg_closure_phase_2 g1 of {
        (hasChanged2, g2) ->
          (if hasChanged1 || hasChanged2 then simple_cg_closure g2 else g2);
      });
  });

simple_cg_merge ::
  forall a. (Ceq a, Ccompare a, Eq a) => [Fset [a]] -> [a] -> [a] -> [Fset [a]];
simple_cg_merge g ys1 ys2 =
  simple_cg_closure (finsert ys1 (finsert ys2 bot_fset) : g);

prefix_pair_tests ::
  forall a b c.
    (Finite_UNIV a, Ceq a, Cproper_interval a, Eq a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => a -> Set ([(a, (b, (c, a)))], (Set a, Set a)) ->
                            Set (a, ([(a, (b, (c, a)))], a));
prefix_pair_tests q (RBT_set t) =
  (case (ccompare_prod ::
          Maybe (([(a, (b, (c, a)))], (Set a, Set a)) ->
                  ([(a, (b, (c, a)))], (Set a, Set a)) -> Ordera))
    of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "prefix_pair_tests RBT_set: ccompare = None"
        (\ _ -> prefix_pair_tests q (RBT_set t));
    Just _ ->
      set (concatMap
            (\ (p, (rd, _)) ->
              concat
                (map_filter
                  (\ x ->
                    (if (case x of {
                          (p1, p2) ->
                            not (target q p1 == target q p2) &&
                              member (target q p1) rd &&
                                member (target q p2) rd;
                        })
                      then Just (case x of {
                                  (p1, p2) ->
                                    [(q, (p1, target q p2)),
                                      (q, (p2, target q p1))];
                                })
                      else Nothing))
                  (prefix_pairs p)))
            (keysb t));
  });

simple_cg_inserta ::
  forall a. (Ceq a, Ccompare a, Linorder a) => [Fset [a]] -> [a] -> [Fset [a]];
simple_cg_inserta xs ys = (case find (\ x -> member ys (fset x)) xs of {
                            Nothing -> finsert ys bot_fset : xs;
                            Just _ -> xs;
                          });

simple_cg_insert ::
  forall a. (Ceq a, Ccompare a, Linorder a) => [Fset [a]] -> [a] -> [Fset [a]];
simple_cg_insert xs ys = foldl simple_cg_inserta xs (prefixes ys);

dual_set_as_map_image ::
  forall a b c d e f.
    (Ccompare a, Ccompare b, Ccompare c, Eq c, Mapping_impl c, Ceq d,
      Ccompare d, Set_impl d, Ccompare e, Eq e, Mapping_impl e, Ceq f,
      Ccompare f,
      Set_impl f) => Set (a, b) ->
                       ((a, b) -> (c, d)) ->
                         ((a, b) -> (e, f)) ->
                           (c -> Maybe (Set d), e -> Maybe (Set f));
dual_set_as_map_image (RBT_set t) f1 f2 =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "dual_set_as_map_image RBT_set: ccompare = None"
        (\ _ -> dual_set_as_map_image (RBT_set t) f1 f2);
    Just _ ->
      let {
        mm = foldb (\ kv (m1, m2) ->
                     ((case f1 kv of {
                        (x, z) -> (case lookupa m1 x of {
                                    Nothing -> updateb x (insert z bot_set) m1;
                                    Just zs -> updateb x (insert z zs) m1;
                                  });
                      }),
                       (case f2 kv of {
                         (x, z) ->
                           (case lookupa m2 x of {
                             Nothing -> updateb x (insert z bot_set) m2;
                             Just zs -> updateb x (insert z zs) m2;
                           });
                       })))
               t (emptya, emptya);
      } in (lookupa (fst mm), lookupa (snd mm));
  });

paths_up_to_length_or_condition_with_witnessa ::
  forall a b c d.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c, Ccompare c, Finite_UNIV d,
      Cenum d, Ceq d, Cproper_interval d,
      Set_impl d) => (a -> Set (b, (c, a))) ->
                       ([(a, (b, (c, a)))] -> Maybe d) ->
                         [(a, (b, (c, a)))] ->
                           Nat -> a -> Set ([(a, (b, (c, a)))], d);
paths_up_to_length_or_condition_with_witnessa f p prev k q =
  (if equal_nat k zero_nat then (case p prev of {
                                  Nothing -> bot_set;
                                  Just w -> insert (prev, w) bot_set;
                                })
    else (case p prev of {
           Nothing ->
             sup_seta
               (image
                 (\ (x, (y, qa)) ->
                   paths_up_to_length_or_condition_with_witnessa f p
                     (prev ++ [(q, (x, (y, qa)))]) (minus_nat k one_nat) qa)
                 (f q));
           Just w -> insert (prev, w) bot_set;
         }));

paths_up_to_length_or_condition_with_witness ::
  forall a b c d.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c, Set_impl c, Finite_UNIV d, Cenum d, Ceq d,
      Cproper_interval d,
      Set_impl d) => Fsm a b c ->
                       ([(a, (b, (c, a)))] -> Maybe d) ->
                         Nat -> a -> Set ([(a, (b, (c, a)))], d);
paths_up_to_length_or_condition_with_witness m p k q =
  (if member q (states m)
    then paths_up_to_length_or_condition_with_witnessa (h_from m) p [] k q
    else bot_set);

m_traversal_paths_with_witness_up_to_length ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c ->
                       a -> [(Set a, Set a)] ->
                              Nat ->
                                Nat -> Set ([(a, (b, (c, a)))], (Set a, Set a));
m_traversal_paths_with_witness_up_to_length ma q d m k =
  paths_up_to_length_or_condition_with_witness ma
    (\ p ->
      find (\ da ->
             less_eq_nat (suc (minus_nat m (card (snd da))))
               (size_list
                 (filter (\ t -> member (snd (snd (snd t))) (fst da)) p)))
        d)
    k q;

m_traversal_paths_with_witness ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c ->
                       a -> [(Set a, Set a)] ->
                              Nat -> Set ([(a, (b, (c, a)))], (Set a, Set a));
m_traversal_paths_with_witness ma q d m =
  m_traversal_paths_with_witness_up_to_length ma q d m
    (suc (times_nat (size ma) m));

preamble_prefix_tests ::
  forall a b c.
    (Finite_UNIV a, Ceq a, Cproper_interval a, Eq a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => a -> Set ([(a, (b, (c, a)))], (Set a, Set a)) ->
                            Set a -> Set (a, ([(a, (b, (c, a)))], a));
preamble_prefix_tests q (RBT_set t1) (RBT_set t2) =
  (case (ccompare_prod ::
          Maybe (([(a, (b, (c, a)))], (Set a, Set a)) ->
                  ([(a, (b, (c, a)))], (Set a, Set a)) -> Ordera))
    of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "prefix_pair_tests RBT_set: ccompare = None"
        (\ _ -> preamble_prefix_tests q (RBT_set t1) (RBT_set t2));
    Just _ ->
      (case (ccompare :: Maybe (a -> a -> Ordera)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "prefix_pair_tests RBT_set: ccompare = None"
            (\ _ -> preamble_prefix_tests q (RBT_set t1) (RBT_set t2));
        Just _ ->
          set (concatMap
                (\ (p, (rd, _)) ->
                  concat
                    (map_filter
                      (\ x ->
                        (if (case x of {
                              (p1, q2) ->
                                not (target q p1 == q2) &&
                                  member (target q p1) rd && member q2 rd;
                            })
                          then Just (case x of {
                                      (p1, q2) ->
[(q, (p1, q2)), (q2, ([], target q p1))];
                                    })
                          else Nothing))
                      (product (prefixes p) (keysb t2))))
                (keysb t1));
      });
  });

preamble_pair_tests ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => Set (Set a) ->
                       Set (a, a) -> Set (a, ([(a, (b, (c, a)))], a));
preamble_pair_tests drss rds =
  sup_seta
    (image
      (\ drs ->
        image (\ (q1, q2) -> (q1, ([], q2))) (inf_set (producte drs drs) rds))
      drss);

calculate_test_paths ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         Set a ->
                           Set (a, a) ->
                             [(Set a, Set a)] ->
                               (a -> Set [(a, (b, (c, a)))],
                                 (a, [(a, (b, (c, a)))]) -> Set a);
calculate_test_paths ma m d_reachable_states r_distinguishable_pairs
  repetition_sets =
  let {
    paths_with_witnesses =
      image (\ q -> (q, m_traversal_paths_with_witness ma q repetition_sets m))
        d_reachable_states;
    get_paths = (\ x -> (case set_as_map paths_with_witnesses x of {
                          Nothing -> set_empty (of_phantom set_impl_set);
                          Just xs -> xs;
                        }));
    prefixPairTests =
      sup_seta
        (image (\ q -> sup_seta (image (prefix_pair_tests q) (get_paths q)))
          d_reachable_states);
    preamblePrefixTests =
      sup_seta
        (image
          (\ q ->
            sup_seta
              (image
                (\ mrsps -> preamble_prefix_tests q mrsps d_reachable_states)
                (get_paths q)))
          d_reachable_states);
    preamblePairTests =
      preamble_pair_tests
        (sup_seta
          (image (\ (_, a) -> image (\ (_, (_, dr)) -> dr) a)
            paths_with_witnesses))
        r_distinguishable_pairs;
    tests =
      sup_set (sup_set prefixPairTests preamblePrefixTests) preamblePairTests;
    tps = (\ x ->
            (case set_as_map_image paths_with_witnesses
                    (\ (q, p) -> (q, image fst p)) x
              of {
              Nothing -> sup_seta (set_empty (of_phantom set_impl_set));
              Just a -> sup_seta a;
            }));
    dual_maps =
      dual_set_as_map_image tests (\ (q, (p, _)) -> (q, p))
        (\ (q, (p, a)) -> ((q, p), a));
    tpsa = (\ x -> (case fst dual_maps x of {
                     Nothing -> set_empty (of_phantom set_impl_list);
                     Just xs -> xs;
                   }));
    tpsb = (\ q -> sup_set (tps q) (tpsa q));
    a = (\ x -> (case snd dual_maps x of {
                  Nothing -> bot_set;
                  Just xs -> xs;
                }));
  } in (tpsb, a);

combine_test_suite ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Set_impl b, Cenum c, Ceq c,
      Ccompare c, Eq c, Set_impl c, Cenum d, Ceq d, Ccompare d, Eq d,
      Set_impl d) => Fsm a b c ->
                       Nat ->
                         Set (a, Fsm a b c) ->
                           Set ((a, a), (Fsm d b c, (d, d))) ->
                             [(Set a, Set a)] -> Test_suite a b c d;
combine_test_suite ma m states_with_preambles pairs_with_separators
  repetition_sets =
  let {
    drs = image fst states_with_preambles;
    rds = image fst pairs_with_separators;
    tps_and_targets = calculate_test_paths ma m drs rds repetition_sets;
    a = (\ x -> (case set_as_map pairs_with_separators x of {
                  Nothing -> bot_set;
                  Just xs -> xs;
                }));
  } in Test_Suite states_with_preambles (fst tps_and_targets)
         (snd tps_and_targets) a;

get_extension ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b,
      Eq b) => Prefix_tree (a, b) ->
                 c -> (c -> [(a, b)] -> [[(a, b)]]) ->
                        [(a, b)] -> a -> b -> Maybe [(a, b)];
get_extension t g cg_lookup alpha x y =
  find (\ beta -> isin t (beta ++ [(x, y)])) (alpha : cg_lookup g alpha);

sorted_list_of_sequences_in_tree ::
  forall a.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Linorder a,
      Set_impl a) => Prefix_tree a -> [[a]];
sorted_list_of_sequences_in_tree (MPT m) =
  (if is_empty (keys m) then [[]]
    else [] : concatMap
                (\ k ->
                  map (\ a -> k : a)
                    (sorted_list_of_sequences_in_tree (the (lookupa m k))))
                (sorted_list_of_set (keys m)));

simple_cg_initial ::
  forall a b c.
    (Ccompare a, Eq a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c, Cproper_interval c,
      Eq c, Linorder c,
      Set_impl c) => Fsm a b c -> Prefix_tree (b, c) -> [Fset [(b, c)]];
simple_cg_initial m1 t =
  foldl simple_cg_inserta simple_cg_empty
    (filter (is_in_language m1 (initial m1))
      (sorted_list_of_sequences_in_tree t));

append_heuristic_input ::
  forall a b c.
    (Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       Prefix_tree (b, c) ->
                         [(b, c)] ->
                           ([(b, c)], Int) -> [(b, c)] -> ([(b, c)], Int);
append_heuristic_input m t w (uBest, lBest) u =
  let {
    ta = aftera t u;
    ws = maximum_fst_prefixes ta (map fst w) (outputs_as_list m);
  } in foldr (\ wa (uBesta, lBesta) ->
               (if wa == w then (u, zero_int)
                 else (if less_int lBesta (int_of_nat (size_list wa)) ||
                            equal_int (int_of_nat (size_list wa)) lBesta &&
                              less_nat (size_list u) (size_list uBesta)
                        then (u, int_of_nat (size_list wa))
                        else (uBesta, lBesta))))
         ws (uBest, lBest);

shortest_list_in_tree_or_default ::
  forall a. (Ccompare a, Eq a) => [[a]] -> Prefix_tree a -> [a] -> [a];
shortest_list_in_tree_or_default xs t x =
  foldl (\ a b ->
          (if isin t a && less_nat (size_list a) (size_list b) then a else b))
    x xs;

complete_inputs_to_tree ::
  forall a b c.
    (Ccompare a, Eq a, Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c -> a -> [c] -> [b] -> Prefix_tree (b, c);
complete_inputs_to_tree m q ys [] = emptyc;
complete_inputs_to_tree m q ys (x : xs) =
  foldl (\ t y ->
          (case h_obs m q x y of {
            Nothing -> insertb t [(x, y)];
            Just qa ->
              combine_after t [(x, y)] (complete_inputs_to_tree m qa ys xs);
          }))
    emptyc ys;

distribute_extension ::
  forall a b c d.
    (Ccompare a, Eq a, Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       Prefix_tree (b, c) ->
                         d -> (d -> [(b, c)] -> [[(b, c)]]) ->
                                (d -> [(b, c)] -> d) ->
                                  [(b, c)] ->
                                    [(b, c)] ->
                                      Bool ->
(Prefix_tree (b, c) ->
  [(b, c)] -> ([(b, c)], Int) -> [(b, c)] -> ([(b, c)], Int)) ->
  (Prefix_tree (b, c), d);
distribute_extension m t g cg_lookup cg_insert u w completeInputTraces
  append_heuristic =
  let {
    cu = cg_lookup g u;
    u0 = shortest_list_in_tree_or_default cu t u;
    l0 = uminus_int one_int;
    ua = fst (foldl (append_heuristic t w) (u0, l0) (filter (isin t) cu));
    ta = insertb t (ua ++ w);
    ga = cg_insert g (maximal_prefix_in_language m (initial m) (ua ++ w));
  } in (if completeInputTraces
         then let {
                tc = complete_inputs_to_tree m (initial m) (outputs_as_list m)
                       (map fst (ua ++ w));
                tb = combinea ta tc;
              } in (tb, ga)
         else (ta, ga));

append_heuristic_io ::
  forall a b.
    (Ccompare a, Eq a, Mapping_impl a, Ccompare b, Eq b,
      Mapping_impl b) => Prefix_tree (a, b) ->
                           [(a, b)] ->
                             ([(a, b)], Int) -> [(a, b)] -> ([(a, b)], Int);
append_heuristic_io t w (uBest, lBest) u =
  let {
    ta = aftera t u;
    wa = maximum_prefix ta w;
  } in (if wa == w then (u, zero_int)
         else (if is_maximal_in ta wa &&
                    (less_int lBest (int_of_nat (size_list wa)) ||
                      equal_int (int_of_nat (size_list wa)) lBest &&
                        less_nat (size_list u) (size_list uBest))
                then (u, int_of_nat (size_list wa)) else (uBest, lBest)));

handle_io_pair ::
  forall a b c d.
    (Ccompare a, Eq a, Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Bool ->
                       Bool ->
                         Fsm a b c ->
                           (a -> [(b, c)]) ->
                             Prefix_tree (b, c) ->
                               d -> (d -> [(b, c)] -> d) ->
                                      (d -> [(b, c)] -> [[(b, c)]]) ->
a -> b -> c -> (Prefix_tree (b, c), d);
handle_io_pair completeInputTraces useInputHeuristic m v t g cg_insert cg_lookup
  q x y =
  distribute_extension m t g cg_lookup cg_insert (v q) [(x, y)]
    completeInputTraces
    (if useInputHeuristic then append_heuristic_input m
      else append_heuristic_io);

estimate_growth ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ccompare b, Eq b,
      Linorder b, Ccompare c, Eq c,
      Linorder c) => Fsm a b c ->
                       (a -> a -> [(b, c)]) -> a -> a -> b -> c -> Nat -> Nat;
estimate_growth m dist_fun q1 q2 x y errorValue =
  (case h_obs m q1 x y of {
    Nothing -> (case h_obs m q1 x y of {
                 Nothing -> errorValue;
                 Just _ -> one_nat;
               });
    Just q1a ->
      (case h_obs m q2 x y of {
        Nothing -> one_nat;
        Just q2a ->
          (if q1a == q2a ||
                set_eq (insert q1a (insert q2a bot_set))
                  (insert q1 (insert q2 bot_set))
            then errorValue
            else plus_nat one_nat
                   (times_nat (nat_of_integer (2 :: Integer))
                     (size_list (dist_fun q1 q2))));
      });
  });

sorted_list_of_maximal_sequences_in_tree ::
  forall a.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Linorder a,
      Set_impl a) => Prefix_tree a -> [[a]];
sorted_list_of_maximal_sequences_in_tree (MPT m) =
  (if is_empty (keys m) then [[]]
    else concatMap
           (\ k ->
             map (\ a -> k : a)
               (sorted_list_of_maximal_sequences_in_tree (the (lookupa m k))))
           (sorted_list_of_set (keys m)));

traces_to_check ::
  forall a b c.
    (Ccompare a, Eq a, Ceq b, Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c,
      Eq c, Linorder c) => Fsm a b c -> a -> Nat -> [[(b, c)]];
traces_to_check m q k =
  (if equal_nat k zero_nat then []
    else let {
           a = product (inputs_as_list m) (outputs_as_list m);
         } in concatMap
                (\ (x, y) ->
                  (case h_obs m q x y of {
                    Nothing -> [[(x, y)]];
                    Just qa ->
                      [(x, y)] :
                        map (\ aa -> (x, y) : aa)
                          (traces_to_check m qa (minus_nat k one_nat));
                  }))
                a);

establish_convergence_static ::
  forall a b c d.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Card_UNIV b,
      Cenum b, Ceq b, Cproper_interval b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Card_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Mapping_impl c, Linorder c,
      Set_impl c) => (Nat -> a -> Prefix_tree (b, c)) ->
                       Fsm a b c ->
                         (a -> [(b, c)]) ->
                           Prefix_tree (b, c) ->
                             d -> (d -> [(b, c)] -> d) ->
                                    (d -> [(b, c)] -> [[(b, c)]]) ->
                                      Nat ->
(a, (b, (c, a))) -> (Prefix_tree (b, c), d);
establish_convergence_static dist_fun ma v ta g cg_insert cg_lookup m t =
  let {
    alpha = v (fst t);
    xy = (fst (snd t), fst (snd (snd t)));
    beta = v (snd (snd (snd t)));
    _ = after ma (initial ma) (v (fst t));
    qTarget = after ma (initial ma) (v (snd (snd (snd t))));
    k = minus_nat m (card (reachable_states ma));
    ttc = [] : traces_to_check ma qTarget k;
    handleTrace =
      (\ (tb, ga) u ->
        (if is_in_language ma qTarget u
          then let {
                 qu = after ma qTarget u;
                 ws = sorted_list_of_maximal_sequences_in_tree
                        (dist_fun (suc (size_list u)) qu);
                 appendDistTrace =
                   (\ (tc, gb) w ->
                     (case distribute_extension ma tc gb cg_lookup cg_insert
                             alpha (xy : u ++ w) False
                             (append_heuristic_input ma)
                       of {
                       (td, gc) ->
                         distribute_extension ma td gc cg_lookup cg_insert beta
                           (u ++ w) False (append_heuristic_input ma);
                     }));
               } in foldl appendDistTrace (tb, ga) ws
          else (case distribute_extension ma tb ga cg_lookup cg_insert alpha
                       (xy : u) False (append_heuristic_input ma)
                 of {
                 (tc, gb) ->
                   distribute_extension ma tc gb cg_lookup cg_insert beta u
                     False (append_heuristic_input ma);
               })));
  } in foldl handleTrace (ta, g) ttc;

handleUT_static ::
  forall a b c d.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Card_UNIV b,
      Cenum b, Ceq b, Cproper_interval b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Card_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Mapping_impl c, Linorder c,
      Set_impl c) => (Nat -> a -> Prefix_tree (b, c)) ->
                       Fsm a b c ->
                         (a -> [(b, c)]) ->
                           Prefix_tree (b, c) ->
                             d -> (d -> [(b, c)] -> d) ->
                                    (d -> [(b, c)] -> [[(b, c)]]) ->
                                      (d -> [(b, c)] -> [(b, c)] -> d) ->
Nat ->
  (a, (b, (c, a))) ->
    [(a, (b, (c, a)))] -> ([(a, (b, (c, a)))], (Prefix_tree (b, c), d));
handleUT_static dist_fun m v ta g cg_insert cg_lookup cg_merge l t x =
  (case handle_io_pair False False m v ta g cg_insert cg_lookup (fst t)
          (fst (snd t)) (fst (snd (snd t)))
    of {
    (t1, g1) ->
      (case establish_convergence_static dist_fun m v t1 g1 cg_insert cg_lookup
              l t
        of {
        (t2, g2) ->
          let {
            g3 = cg_merge g2 (v (fst t) ++ [(fst (snd t), fst (snd (snd t)))])
                   (v (snd (snd (snd t))));
          } in (x, (t2, g3));
      });
  });

shortest_list_or_default :: forall a. [[a]] -> [a] -> [a];
shortest_list_or_default xs x =
  foldl (\ a b -> (if less_nat (size_list a) (size_list b) then a else b)) x xs;

get_prefix_of_separating_sequence ::
  forall a b c d.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       Prefix_tree (b, c) ->
                         d -> (d -> [(b, c)] -> [[(b, c)]]) ->
                                (a -> a -> [(b, c)]) ->
                                  [(b, c)] ->
                                    [(b, c)] -> Nat -> (Nat, [(b, c)]);
get_prefix_of_separating_sequence m t g cg_lookup get_distinguishing_trace u v k
  = (if equal_nat k zero_nat then (one_nat, [])
      else let {
             ua = shortest_list_or_default (cg_lookup g u) u;
             va = shortest_list_or_default (cg_lookup g v) v;
             su = after m (initial m) u;
             sv = after m (initial m) v;
             bestPrefix0 = get_distinguishing_trace su sv;
             minEst0 =
               plus_nat
                 (plus_nat (size_list bestPrefix0)
                   (if has_leaf t g cg_lookup ua then zero_nat
                     else size_list ua))
                 (if has_leaf t g cg_lookup va then zero_nat else size_list va);
             errorValue = suc minEst0;
             xy = product (inputs_as_list m) (outputs_as_list m);
             tryIO =
               (\ (minEst, bestPrefix) (x, y) ->
                 (if equal_nat minEst zero_nat then (minEst, bestPrefix)
                   else (case get_extension t g cg_lookup ua x y of {
                          Nothing ->
                            (case get_extension t g cg_lookup va x y of {
                              Nothing -> (minEst, bestPrefix);
                              Just vb ->
                                let {
                                  e = estimate_growth m get_distinguishing_trace
su sv x y errorValue;
                                  ea = (if not (equal_nat e one_nat)
 then (if has_leaf t g cg_lookup vb then plus_nat e one_nat
        else (if not (has_leaf t g cg_lookup (vb ++ [(x, y)]))
               then plus_nat (plus_nat e (size_list va)) one_nat else e))
 else e);
                                  eb = plus_nat ea
 (if not (has_leaf t g cg_lookup ua) then size_list ua else zero_nat);
                                } in (if less_eq_nat eb minEst
                                       then (eb, [(x, y)])
                                       else (minEst, bestPrefix));
                            });
                          Just ub ->
                            (case get_extension t g cg_lookup va x y of {
                              Nothing ->
                                let {
                                  e = estimate_growth m get_distinguishing_trace
su sv x y errorValue;
                                  ea = (if not (equal_nat e one_nat)
 then (if has_leaf t g cg_lookup ub then plus_nat e one_nat
        else (if not (has_leaf t g cg_lookup (ub ++ [(x, y)]))
               then plus_nat (plus_nat e (size_list ua)) one_nat else e))
 else e);
                                  eb = plus_nat ea
 (if not (has_leaf t g cg_lookup va) then size_list va else zero_nat);
                                } in (if less_eq_nat eb minEst
                                       then (eb, [(x, y)])
                                       else (minEst, bestPrefix));
                              Just vb ->
                                (if not (is_none (h_obs m su x y) ==
  is_none (h_obs m sv x y))
                                  then (zero_nat, [])
                                  else (if h_obs m su x y == h_obs m sv x y
 then (minEst, bestPrefix)
 else (case get_prefix_of_separating_sequence m t g cg_lookup
              get_distinguishing_trace (ub ++ [(x, y)]) (vb ++ [(x, y)])
              (minus_nat k one_nat)
        of {
        (e, w) ->
          (if equal_nat e zero_nat then (zero_nat, [])
            else (if less_eq_nat e minEst then (e, (x, y) : w)
                   else (minEst, bestPrefix)));
      })));
                            });
                        })));
           } in (if not (isin t ua) || not (isin t va) then (errorValue, [])
                  else foldl tryIO (minEst0, []) xy));

spyh_distinguish ::
  forall a b c d.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       Prefix_tree (b, c) ->
                         d -> (d -> [(b, c)] -> [[(b, c)]]) ->
                                (d -> [(b, c)] -> d) ->
                                  (a -> a -> [(b, c)]) ->
                                    [(b, c)] ->
                                      [[(b, c)]] ->
Nat ->
  Bool ->
    (Prefix_tree (b, c) ->
      [(b, c)] -> ([(b, c)], Int) -> [(b, c)] -> ([(b, c)], Int)) ->
      (Prefix_tree (b, c), d);
spyh_distinguish m t g cg_lookup cg_insert get_distinguishing_trace u x k
  completeInputTraces append_heuristic =
  let {
    dist_helper =
      (\ (ta, ga) v ->
        (if after m (initial m) u == after m (initial m) v then (ta, ga)
          else let {
                 ew = get_prefix_of_separating_sequence m ta ga cg_lookup
                        get_distinguishing_trace u v k;
               } in (if equal_nat (fst ew) zero_nat then (ta, ga)
                      else let {
                             ua = u ++ snd ew;
                             va = v ++ snd ew;
                             w = (if does_distinguish m (after m (initial m) u)
                                       (after m (initial m) v) (snd ew)
                                   then snd ew
                                   else snd ew ++
  get_distinguishing_trace (after m (initial m) ua) (after m (initial m) va));
                             tg = distribute_extension m ta ga cg_lookup
                                    cg_insert u w completeInputTraces
                                    append_heuristic;
                           } in distribute_extension m (fst tg) (snd tg)
                                  cg_lookup cg_insert v w completeInputTraces
                                  append_heuristic)));
  } in foldl dist_helper (t, g) x;

distinguish_from_set ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       (a -> [(b, c)]) ->
                         Prefix_tree (b, c) ->
                           d -> (d -> [(b, c)] -> [[(b, c)]]) ->
                                  (d -> [(b, c)] -> d) ->
                                    (a -> a -> [(b, c)]) ->
                                      [(b, c)] ->
[(b, c)] ->
  [[(b, c)]] ->
    Nat ->
      Nat ->
        Bool ->
          (Prefix_tree (b, c) ->
            [(b, c)] -> ([(b, c)], Int) -> [(b, c)] -> ([(b, c)], Int)) ->
            Bool -> (Prefix_tree (b, c), d);
distinguish_from_set m va t g cg_lookup cg_insert get_distinguishing_trace u v x
  k depth completeInputTraces append_heuristic u_is_v =
  let {
    tg = spyh_distinguish m t g cg_lookup cg_insert get_distinguishing_trace u x
           k completeInputTraces append_heuristic;
    vClass = insert v (set (cg_lookup (snd tg) v));
    notReferenced =
      not u_is_v &&
        ball (reachable_states m) (\ q -> not (member (va q) vClass));
    tga = (if notReferenced
            then spyh_distinguish m (fst tg) (snd tg) cg_lookup cg_insert
                   get_distinguishing_trace v x k completeInputTraces
                   append_heuristic
            else tg);
  } in (if less_nat zero_nat depth
         then let {
                xa = (if notReferenced then v : u : x else u : x);
                xy = product (inputs_as_list m) (outputs_as_list m);
                handleIO =
                  (\ (ta, ga) (xaa, y) ->
                    let {
                      tGu = distribute_extension m ta ga cg_lookup cg_insert u
                              [(xaa, y)] completeInputTraces append_heuristic;
                      tGv = (if u_is_v then tGu
                              else distribute_extension m (fst tGu) (snd tGu)
                                     cg_lookup cg_insert v [(xaa, y)]
                                     completeInputTraces append_heuristic);
                    } in (if is_in_language m (initial m) (u ++ [(xaa, y)])
                           then distinguish_from_set m va (fst tGv) (snd tGv)
                                  cg_lookup cg_insert get_distinguishing_trace
                                  (u ++ [(xaa, y)]) (v ++ [(xaa, y)]) xa k
                                  (minus_nat depth one_nat) completeInputTraces
                                  append_heuristic u_is_v
                           else tGv));
              } in foldl handleIO tga xy
         else tga);

handleUT_dynamic ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Eq c, Mapping_impl c,
      Linorder c) => Bool ->
                       Bool ->
                         (a -> a -> [(b, c)]) ->
                           (Fsm a b c ->
                             (a -> [(b, c)]) ->
                               (a, (b, (c, a))) ->
                                 [(a, (b, (c, a)))] -> Nat -> Bool) ->
                             Fsm a b c ->
                               (a -> [(b, c)]) ->
                                 Prefix_tree (b, c) ->
                                   d -> (d -> [(b, c)] -> d) ->
  (d -> [(b, c)] -> [[(b, c)]]) ->
    (d -> [(b, c)] -> [(b, c)] -> d) ->
      Nat ->
        (a, (b, (c, a))) ->
          [(a, (b, (c, a)))] -> ([(a, (b, (c, a)))], (Prefix_tree (b, c), d));
handleUT_dynamic complete_input_traces use_input_heuristic dist_fun
  do_establish_convergence ma v ta g cg_insert cg_lookup cg_merge m t x =
  let {
    k = times_nat (nat_of_integer (2 :: Integer)) (size ma);
    l = minus_nat m (card (reachable_states ma));
    heuristic =
      (if use_input_heuristic then append_heuristic_input ma
        else append_heuristic_io);
    rstates = map v (reachable_states_as_list ma);
  } in (case handle_io_pair complete_input_traces use_input_heuristic ma v ta g
               cg_insert cg_lookup (fst t) (fst (snd t)) (fst (snd (snd t)))
         of {
         (t1, g1) ->
           let {
             u = v (fst t) ++ [(fst (snd t), fst (snd (snd t)))];
             va = v (snd (snd (snd t)));
             xa = butlast x;
           } in (if do_establish_convergence ma v t xa l
                  then (case distinguish_from_set ma v t1 g1 cg_lookup cg_insert
                               dist_fun u va rstates k l complete_input_traces
                               heuristic False
                         of {
                         (t2, g2) -> let {
                                       g3 = cg_merge g2 u va;
                                     } in (xa, (t2, g3));
                       })
                  else (xa, distinguish_from_set ma v t1 g1 cg_lookup cg_insert
                              dist_fun u u rstates k l complete_input_traces
                              heuristic True));
       });

state_separator_from_input_choices ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Set_impl c) => Fsm a b c ->
                       Fsm (Sum (a, a) a) b c ->
                         a -> a -> [(Sum (a, a) a, b)] ->
                                     Fsm (Sum (a, a) a) b c;
state_separator_from_input_choices m cSep q1 q2 cs =
  let {
    css = set cs;
    cssQ =
      sup_set (set (map fst cs)) (insert (Inr q1) (insert (Inr q2) bot_set));
    s0 = filter_states cSep (\ q -> member q cssQ);
    s1 = filter_transitions s0 (\ t -> member (fst t, fst (snd t)) css);
  } in s1;

state_separator_from_s_states ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> a -> a -> Maybe (Fsm (Sum (a, a) a) b c);
state_separator_from_s_states m q1 q2 =
  let {
    c = canonical_separatorb m q1 q2;
    cs = select_inputs (h c) (initial c) (inputs_as_list c)
           (remove1 (Inl (q1, q2))
             (remove1 (Inr q1) (remove1 (Inr q2) (states_as_list c))))
           (insert (Inr q1) (insert (Inr q2) bot_set)) [];
  } in (if equal_nat (size_list cs) zero_nat then Nothing
         else (if equal_sum (fst (last cs)) (Inl (q1, q2))
                then Just (state_separator_from_input_choices m c q1 q2 cs)
                else Nothing));

select_diverging_ofsm_table_io_with_provided_tables ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Mapping Nat (Mapping a (Set a)) ->
                       Fsm a b c ->
                         a -> a -> Nat -> ((b, c), (Maybe a, Maybe a));
select_diverging_ofsm_table_io_with_provided_tables tables m q1 q2 k =
  let {
    ins = inputs_as_list m;
    outs = outputs_as_list m;
    table = lookup_default bot_set (the (lookupa tables (minus_nat k one_nat)));
    f = (\ (x, y) ->
          (case (h_obs m q1 x y, h_obs m q2 x y) of {
            (Nothing, Nothing) -> Nothing;
            (Nothing, Just q2a) -> Just ((x, y), (Nothing, Just q2a));
            (Just q1a, Nothing) -> Just ((x, y), (Just q1a, Nothing));
            (Just q1a, Just q2a) ->
              (if not (set_eq (table q1a) (table q2a))
                then Just ((x, y), (Just q1a, Just q2a)) else Nothing);
          }));
  } in hd (map_filter f (product ins outs));

assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Mapping Nat (Mapping a (Set a)) ->
                       Fsm a b c -> a -> a -> Nat -> [(b, c)];
assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables tables m
  q1 q2 k =
  (if equal_nat k zero_nat then []
    else (case select_diverging_ofsm_table_io_with_provided_tables tables m q1
                 q2 (suc (minus_nat k one_nat))
           of {
           ((x, y), (Nothing, _)) -> [(x, y)];
           ((x, y), (Just _, Nothing)) -> [(x, y)];
           ((x, y), (Just q1a, Just q2a)) ->
             (x, y) :
               assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables
                 tables m q1a q2a (minus_nat k one_nat);
         }));

get_distinguishing_sequence_from_ofsm_tables_with_provided_tables ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Linorder b, Ceq c, Ccompare c, Eq c,
      Linorder c) => Mapping Nat (Mapping a (Set a)) ->
                       Fsm a b c -> a -> a -> [(b, c)];
get_distinguishing_sequence_from_ofsm_tables_with_provided_tables tables m q1 q2
  = let {
      a = (if member q1 (states m) &&
                member q2 (states m) &&
                  not (set_eq
                        (lookup_default bot_set
                          (the (lookupa tables (minus_nat (size m) one_nat)))
                          q1)
                        (lookup_default bot_set
                          (the (lookupa tables (minus_nat (size m) one_nat)))
                          q2))
            then the (find_index
                       (\ i ->
                         not (set_eq
                               (lookup_default bot_set (the (lookupa tables i))
                                 q1)
                               (lookup_default bot_set (the (lookupa tables i))
                                 q2)))
                       (upt zero_nat (size m)))
            else zero_nat);
    } in assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables
           tables m q1 q2 a;

sort_unverified_transitions_by_state_cover_length ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Eq b, Linorder b, Eq c,
      Linorder c) => Fsm a b c ->
                       (a -> [(b, c)]) ->
                         [(a, (b, (c, a)))] -> [(a, (b, (c, a)))];
sort_unverified_transitions_by_state_cover_length m v ts =
  let {
    default_weight = times_nat (nat_of_integer (2 :: Integer)) (size m);
    weights =
      map_of
        (map (\ t ->
               (t, plus_nat (size_list (v (fst t)))
                     (size_list (v (snd (snd (snd t)))))))
          ts);
    weight = (\ q -> (case weights q of {
                       Nothing -> default_weight;
                       Just w -> w;
                     }));
  } in mergesort_by_rel (\ t1 t2 -> less_eq_nat (weight t1) (weight t2)) ts;

handleUT_dynamic_with_precomputed_state_cover ::
  forall a b c d.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Ceq b, Ccompare b,
      Eq b, Mapping_impl b, Linorder b, Card_UNIV c, Cenum c, Ceq c, Ccompare c,
      Eq c, Linorder c,
      Set_impl c) => [[(a, b)]] ->
                       Bool ->
                         Bool ->
                           (c -> c -> [(a, b)]) ->
                             (Fsm c a b ->
                               (c -> [(a, b)]) ->
                                 (c, (a, (b, c))) ->
                                   [(c, (a, (b, c)))] -> Nat -> Bool) ->
                               Fsm c a b ->
                                 (c -> [(a, b)]) ->
                                   Prefix_tree (a, b) ->
                                     d -> (d -> [(a, b)] -> d) ->
    (d -> [(a, b)] -> [[(a, b)]]) ->
      (d -> [(a, b)] -> [(a, b)] -> d) ->
        Nat ->
          (c, (a, (b, c))) ->
            [(c, (a, (b, c)))] -> ([(c, (a, (b, c)))], (Prefix_tree (a, b), d));
handleUT_dynamic_with_precomputed_state_cover rstates complete_input_traces
  use_input_heuristic dist_fun do_establish_convergence ma v ta g cg_insert
  cg_lookup cg_merge m t x =
  let {
    k = times_nat (nat_of_integer (2 :: Integer)) (size ma);
    l = minus_nat m (card (reachable_states ma));
    heuristic =
      (if use_input_heuristic then append_heuristic_input ma
        else append_heuristic_io);
  } in (case handle_io_pair complete_input_traces use_input_heuristic ma v ta g
               cg_insert cg_lookup (fst t) (fst (snd t)) (fst (snd (snd t)))
         of {
         (t1, g1) ->
           let {
             u = v (fst t) ++ [(fst (snd t), fst (snd (snd t)))];
             va = v (snd (snd (snd t)));
             xa = butlast x;
           } in (if do_establish_convergence ma v t xa l
                  then (case distinguish_from_set ma v t1 g1 cg_lookup cg_insert
                               dist_fun u va rstates k l complete_input_traces
                               heuristic False
                         of {
                         (t2, g2) -> let {
                                       g3 = cg_merge g2 u va;
                                     } in (xa, (t2, g3));
                       })
                  else (xa, distinguish_from_set ma v t1 g1 cg_lookup cg_insert
                              dist_fun u u rstates k l complete_input_traces
                              heuristic True));
       });

handle_state_cover_dynamic ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Eq c, Mapping_impl c,
      Linorder c) => Bool ->
                       Bool ->
                         (a -> a -> [(b, c)]) ->
                           Fsm a b c ->
                             (a -> [(b, c)]) ->
                               (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                                 (d -> [(b, c)] -> d) ->
                                   (d -> [(b, c)] -> [[(b, c)]]) ->
                                     (Prefix_tree (b, c), d);
handle_state_cover_dynamic completeInputTraces useInputHeuristic
  get_distinguishing_trace m v cg_initial cg_insert cg_lookup =
  let {
    k = times_nat (nat_of_integer (2 :: Integer)) (size m);
    heuristic =
      (if useInputHeuristic then append_heuristic_input m
        else append_heuristic_io);
    rstates = reachable_states_as_list m;
    t0 = from_list (map v rstates);
    t0a = (if completeInputTraces
            then combinea t0
                   (from_list
                     (concatMap
                       (\ q -> language_for_input m (initial m) (map fst (v q)))
                       rstates))
            else t0);
    g0 = cg_initial m t0a;
    separate_state =
      (\ (x, (t, g)) q ->
        let {
          u = v q;
          tg = spyh_distinguish m t g cg_lookup cg_insert
                 get_distinguishing_trace u x k completeInputTraces heuristic;
          xa = u : x;
        } in (xa, tg));
  } in snd (foldl separate_state ([], (t0a, g0)) rstates);

h_framework_dynamic_with_empty_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c,
      Ceq c, Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => (Fsm a b c ->
                       (a -> [(b, c)]) ->
                         (a, (b, (c, a))) ->
                           [(a, (b, (c, a)))] -> Nat -> Bool) ->
                       Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
h_framework_dynamic_with_empty_graph convergence_decisision m1 m
  completeInputTraces useInputHeuristic =
  let {
    tables = compute_ofsm_tables m1 (minus_nat (size m1) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables m1 q1 q2);
                        })
              else Nothing))
          (product (states_as_list m1) (states_as_list m1)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states m1) && member q2 (states m1) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables m1 q1 q2));
    v = get_state_cover_assignment m1;
    rstates = map v (reachable_states_as_list m1);
  } in h_framework m1 (\ _ -> v)
         (handle_state_cover_dynamic completeInputTraces useInputHeuristic
           distHelper)
         sort_unverified_transitions_by_state_cover_length
         (handleUT_dynamic_with_precomputed_state_cover rstates
           completeInputTraces useInputHeuristic distHelper
           convergence_decisision)
         (handle_io_pair completeInputTraces useInputHeuristic) empty_cg_initial
         empty_cg_insert empty_cg_lookup empty_cg_merge m;

h_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c,
      Ceq c, Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
h_method_via_h_framework =
  h_framework_dynamic_with_empty_graph (\ _ _ _ _ _ -> False);

distance_at_most ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Linorder c, Set_impl c) => Fsm a b c -> a -> a -> Nat -> Bool;
distance_at_most m q1 q2 k =
  (if equal_nat k zero_nat then q1 == q2
    else q1 == q2 ||
           bex (inputs m)
             (\ x ->
               bex (h m (q1, x))
                 (\ (_, q1a) ->
                   distance_at_most m q1a q2 (minus_nat k one_nat))));

r_distinguishable_state_pairs_with_separators ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Set ((a, a), Fsm (Sum (a, a) a) b c);
r_distinguishable_state_pairs_with_separators m =
  set (concat
        (map_filter
          (\ x ->
            (if (case x of {
                  (_, a) -> not (is_none a);
                })
              then Just (case x of {
                          (a, b) ->
                            (case a of {
                              (q1, q2) ->
                                (\ aa ->
                                  [((q1, q2), the aa), ((q2, q1), the aa)]);
                            })
                              b;
                        })
              else Nothing))
          (map_filter
            (\ x ->
              (if (case x of {
                    (a, b) -> less a b;
                  })
                then Just (case x of {
                            (q1, q2) ->
                              ((q1, q2), state_separator_from_s_states m q1 q2);
                          })
                else Nothing))
            (product (states_as_list m) (states_as_list m)))));

pairwise_r_distinguishable_state_sets_from_separators_list ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> [Set a];
pairwise_r_distinguishable_state_sets_from_separators_list m =
  let {
    rds = image fst (r_distinguishable_state_pairs_with_separators m);
  } in filter
         (\ s ->
           ball s
             (\ q1 ->
               ball s
                 (\ q2 ->
                   (if not (q1 == q2) then member (q1, q2) rds else True))))
         (map set (pow_list (states_as_list m)));

maximal_pairwise_r_distinguishable_state_sets_from_separators_list ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> [Set a];
maximal_pairwise_r_distinguishable_state_sets_from_separators_list m =
  remove_subsets (pairwise_r_distinguishable_state_sets_from_separators_list m);

calculate_state_preamble_from_input_choices ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Card_UNIV c, Ceq c, Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> a -> Maybe (Fsm a b c);
calculate_state_preamble_from_input_choices m q =
  (if q == initial m then Just (initial_preamble m)
    else let {
           ds = d_states m q;
           dss = set ds;
         } in (case ds of {
                [] -> Nothing;
                _ : _ ->
                  (if fst (last ds) == initial m
                    then Just (filter_transitions m
                                (\ t -> member (fst t, fst (snd t)) dss))
                    else Nothing);
              }));

d_reachable_states_with_preambles ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Mapping_impl b,
      Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c, Cproper_interval c,
      Eq c, Mapping_impl c, Set_impl c) => Fsm a b c -> Set (a, Fsm a b c);
d_reachable_states_with_preambles m =
  image (\ qp -> (fst qp, the (snd qp)))
    (filtera (\ qp -> not (is_none (snd qp)))
      (image (\ q -> (q, calculate_state_preamble_from_input_choices m q))
        (states m)));

maximal_repetition_sets_from_separators_list_naive ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> [(Set a, Set a)];
maximal_repetition_sets_from_separators_list_naive m =
  let {
    dr = image fst (d_reachable_states_with_preambles m);
  } in map (\ s -> (s, inf_set s dr))
         (maximal_pairwise_r_distinguishable_state_sets_from_separators_list m);

calculate_test_suite_for_repetition_sets ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         [(Set a, Set a)] -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_for_repetition_sets ma m repetition_sets =
  let {
    states_with_preambles = d_reachable_states_with_preambles ma;
    pairs_with_separators =
      image (\ (a, b) ->
              (case a of {
                (q1, q2) -> (\ aa -> ((q1, q2), (aa, (Inr q1, Inr q2))));
              })
                b)
        (r_distinguishable_state_pairs_with_separators ma);
  } in combine_test_suite ma m states_with_preambles pairs_with_separators
         repetition_sets;

calculate_test_suite_naive ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Nat -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_naive ma m =
  calculate_test_suite_for_repetition_sets ma m
    (maximal_repetition_sets_from_separators_list_naive ma);

handle_state_cover_static ::
  forall a b c d.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c, Eq c,
      Mapping_impl c,
      Linorder c) => (Nat -> a -> Prefix_tree (b, c)) ->
                       Fsm a b c ->
                         (a -> [(b, c)]) ->
                           (Fsm a b c -> Prefix_tree (b, c) -> d) ->
                             (d -> [(b, c)] -> d) ->
                               (d -> [(b, c)] -> [[(b, c)]]) ->
                                 (Prefix_tree (b, c), d);
handle_state_cover_static dist_set m v cg_initial cg_insert cg_lookup =
  let {
    separate_state = (\ t q -> combine_after t (v q) (dist_set zero_nat q));
    t = foldl separate_state emptyc (reachable_states_as_list m);
    a = cg_initial m t;
  } in (t, a);

h_framework_static_with_empty_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Nat -> a -> Prefix_tree (b, c)) ->
                         Nat -> Prefix_tree (b, c);
h_framework_static_with_empty_graph m1 dist_fun m =
  h_framework m1 get_state_cover_assignment (handle_state_cover_static dist_fun)
    (\ _ _ ts -> ts) (handleUT_static dist_fun) (handle_io_pair False False)
    empty_cg_initial empty_cg_insert empty_cg_lookup empty_cg_merge m;

w_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
w_method_via_h_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    distSet = from_list (map (\ (a, b) -> distHelper a b) pairs);
    distFun = (\ _ _ -> distSet);
  } in h_framework_static_with_empty_graph ma distFun m;

greedy_pairwise_r_distinguishable_state_sets_from_separators ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> [Set a];
greedy_pairwise_r_distinguishable_state_sets_from_separators m =
  let {
    pwrds = image fst (r_distinguishable_state_pairs_with_separators m);
    k = size m;
    nL = states_as_list m;
  } in map (\ q -> set (extend_until_conflict pwrds (remove1 q nL) [q] k)) nL;

maximal_repetition_sets_from_separators_list_greedy ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> [(Set a, Set a)];
maximal_repetition_sets_from_separators_list_greedy m =
  let {
    dr = image fst (d_reachable_states_with_preambles m);
  } in remdups
         (map (\ s -> (s, inf_set s dr))
           (greedy_pairwise_r_distinguishable_state_sets_from_separators m));

calculate_test_suite_greedy ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Nat -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_greedy ma m =
  calculate_test_suite_for_repetition_sets ma m
    (maximal_repetition_sets_from_separators_list_greedy ma);

test_suite_from_io_tree ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Mapping_impl b, Ccompare c, Eq c,
      Mapping_impl c) => Fsm a b c ->
                           a -> Prefix_tree (b, c) ->
                                  Prefix_tree ((b, c), Bool);
test_suite_from_io_tree ma q (MPT (RBT_Mapping m)) =
  (case (ccompare_prod :: Maybe ((b, c) -> (b, c) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "test_suite_from_io_tree RBT_set: ccompare = None"
        (\ _ -> test_suite_from_io_tree ma q (MPT (RBT_Mapping m)));
    Just _ ->
      MPT (tabulate
            (map (\ (a, b) ->
                   (case a of {
                     (x, y) ->
                       (\ _ -> ((x, y), not (is_none (h_obs ma q x y))));
                   })
                     b)
              (entriesa m))
            (\ (a, b) ->
              (case a of {
                (x, y) ->
                  (\ _ ->
                    (case h_obs ma q x y of {
                      Nothing -> emptyc;
                      Just qa ->
                        test_suite_from_io_tree ma qa
                          (case lookupb m (x, y) of {
                            Just t -> t;
                          });
                    }));
              })
                b));
  });

get_initial_test_suite_H ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => (a -> [(b, c)]) -> Fsm a b c -> Nat -> Prefix_tree (b, c);
get_initial_test_suite_H v ma m =
  let {
    rstates = reachable_states_as_list ma;
    n = card (reachable_states ma);
    iM = inputs_as_list ma;
    hMap =
      map_of
        (map (\ (q, x) ->
               ((q, x),
                 map (\ (y, qa) -> (q, (x, (y, qa))))
                   (sorted_list_of_set (h ma (q, x)))))
          (product (states_as_list ma) iM));
    _ = (\ q x -> (case hMap (q, x) of {
                    Nothing -> [];
                    Just ts -> ts;
                  }));
    t = from_list
          (concatMap
            (\ q -> map (\ a -> v q ++ a) (h_extensions ma q (minus_nat m n)))
            rstates);
  } in t;

pair_framework_h_components ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         (Fsm a b c ->
                           (([(b, c)], a), ([(b, c)], a)) ->
                             Prefix_tree (b, c) -> Prefix_tree (b, c)) ->
                           Prefix_tree (b, c);
pair_framework_h_components ma m get_separating_traces =
  let {
    v = get_state_cover_assignment ma;
  } in pair_framework ma m (get_initial_test_suite_H v) (get_pairs_H v)
         get_separating_traces;

simple_cg_lookup_with_conv ::
  forall a. (Ceq a, Ccompare a, Eq a, Linorder a) => [Fset [a]] -> [a] -> [[a]];
simple_cg_lookup_with_conv g ys =
  let {
    lookup_for_prefix =
      (\ i ->
        let {
          pref = take i ys;
          suff = drop i ys;
          a = foldl sup_fset bot_fset (filter (\ x -> member pref (fset x)) g);
        } in fimage (\ prefa -> prefa ++ suff) a);
  } in sorted_list_of_fset
         (finsert ys
           (foldl (\ cs i -> sup_fset (lookup_for_prefix i) cs) bot_fset
             (upt zero_nat (suc (size_list ys)))));

contains_distinguishing_trace ::
  forall a b c.
    (Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Eq b, Set_impl b, Cenum c,
      Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c -> Prefix_tree (b, c) -> a -> a -> Bool;
contains_distinguishing_trace m (MPT t1) q1 q2 =
  bex (keys t1)
    (\ (x, y) ->
      (case h_obs m q1 x y of {
        Nothing -> not (is_none (h_obs m q2 x y));
        Just q1a ->
          (case h_obs m q2 x y of {
            Nothing -> True;
            Just a ->
              contains_distinguishing_trace m (the (lookupa t1 (x, y))) q1a a;
          });
      }));

w_method_via_h_framework_2 ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
w_method_via_h_framework_2 ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    handlePair =
      (\ w (q, qa) ->
        (if contains_distinguishing_trace ma w q qa then w
          else insertb w (distHelper q qa)));
    distSet = foldl handlePair emptyc pairs;
    distFun = (\ _ _ -> distSet);
  } in h_framework_static_with_empty_graph ma distFun m;

spy_framework_static_with_empty_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Nat -> a -> Prefix_tree (b, c)) ->
                         Nat -> Prefix_tree (b, c);
spy_framework_static_with_empty_graph m1 dist_fun m =
  spy_framework m1 get_state_cover_assignment
    (handle_state_cover_static dist_fun) (\ _ _ ts -> ts)
    (establish_convergence_static dist_fun) (handle_io_pair False True)
    empty_cg_initial empty_cg_insert empty_cg_lookup empty_cg_merge m;

w_method_via_spy_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
w_method_via_spy_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    distSet = from_list (map (\ (a, b) -> distHelper a b) pairs);
    distFun = (\ _ _ -> distSet);
  } in spy_framework_static_with_empty_graph ma distFun m;

wp_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
wp_method_via_h_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    distSet = from_list (map (\ (a, b) -> distHelper a b) pairs);
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    l = suc (minus_nat m (card (reachable_states ma)));
    distFun =
      (\ k q ->
        (if equal_nat k l
          then (if member q (states ma) then the (hsiMap q) else get_HSI ma q)
          else distSet));
  } in h_framework_static_with_empty_graph ma distFun m;

intersection_is_distinguishing ::
  forall a b c.
    (Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Eq b, Set_impl b, Cenum c,
      Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c ->
                       Prefix_tree (b, c) ->
                         a -> Prefix_tree (b, c) -> a -> Bool;
intersection_is_distinguishing m (MPT t1) q1 (MPT t2) q2 =
  bex (inf_set (keys t1) (keys t2))
    (\ (x, y) ->
      (case h_obs m q1 x y of {
        Nothing -> not (is_none (h_obs m q2 x y));
        Just q1a ->
          (case h_obs m q2 x y of {
            Nothing -> True;
            Just a ->
              intersection_is_distinguishing m (the (lookupa t1 (x, y))) q1a
                (the (lookupa t2 (x, y))) a;
          });
      }));

add_distinguishing_sequence_if_required ::
  forall a b c.
    (Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Eq b, Mapping_impl b,
      Linorder b, Set_impl b, Cenum c, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c,
      Set_impl c) => (a -> a -> [(b, c)]) ->
                       Fsm a b c ->
                         (([(b, c)], a), ([(b, c)], a)) ->
                           Prefix_tree (b, c) -> Prefix_tree (b, c);
add_distinguishing_sequence_if_required dist_fun m ((alpha, q1), (beta, q2)) t =
  (if intersection_is_distinguishing m (aftera t alpha) q1 (aftera t beta) q2
    then emptyc else insertb emptyc (dist_fun q1 q2));

h_method_via_pair_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Cenum c, Ceq c, Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
h_method_via_pair_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    a = add_distinguishing_sequence_if_required distHelper;
  } in pair_framework_h_components ma m a;

w_method_via_pair_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
w_method_via_pair_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    distSet = from_list (map (\ (a, b) -> distHelper a b) pairs);
    a = (\ _ _ _ -> distSet);
  } in pair_framework_h_components ma m a;

hsi_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
hsi_method_via_h_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    distFun =
      (\ _ q ->
        (if member q (states ma) then the (hsiMap q) else get_HSI ma q));
  } in h_framework_static_with_empty_graph ma distFun m;

complete_inputs_to_tree_initial ::
  forall a b c.
    (Ccompare a, Eq a, Linorder a, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c -> [b] -> Prefix_tree (b, c);
complete_inputs_to_tree_initial m xs =
  complete_inputs_to_tree m (initial m) (outputs_as_list m) xs;

get_initial_test_suite_H_2 ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Bool ->
                       (a -> [(b, c)]) ->
                         Fsm a b c -> Nat -> Prefix_tree (b, c);
get_initial_test_suite_H_2 c v ma m =
  (if c then get_initial_test_suite_H v ma m
    else let {
           ts = get_initial_test_suite_H v ma m;
           xss = map (map fst) (sorted_list_of_maximal_sequences_in_tree ts);
           _ = outputs_as_list ma;
         } in foldl (\ t xs ->
                      combinea t (complete_inputs_to_tree_initial ma xs))
                ts xss);

pair_framework_h_components_2 ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         (Fsm a b c ->
                           (([(b, c)], a), ([(b, c)], a)) ->
                             Prefix_tree (b, c) -> Prefix_tree (b, c)) ->
                           Bool -> Prefix_tree (b, c);
pair_framework_h_components_2 ma m get_separating_traces c =
  let {
    v = get_state_cover_assignment ma;
  } in pair_framework ma m (get_initial_test_suite_H_2 c v) (get_pairs_H v)
         get_separating_traces;

h_framework_static_with_simple_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Nat -> a -> Prefix_tree (b, c)) ->
                         Nat -> Prefix_tree (b, c);
h_framework_static_with_simple_graph m1 dist_fun m =
  h_framework m1 get_state_cover_assignment (handle_state_cover_static dist_fun)
    (\ _ _ ts -> ts) (handleUT_static dist_fun) (handle_io_pair False False)
    simple_cg_initial simple_cg_insert simple_cg_lookup_with_conv
    simple_cg_merge m;

spy_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
spy_method_via_h_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    distFun =
      (\ _ q ->
        (if member q (states ma) then the (hsiMap q) else get_HSI ma q));
  } in h_framework_static_with_simple_graph ma distFun m;

wp_method_via_spy_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
wp_method_via_spy_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    pairs =
      filter (\ (x, y) -> not (x == y))
        (list_ordered_pairs (states_as_list ma));
    distSet = from_list (map (\ (a, b) -> distHelper a b) pairs);
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    l = suc (minus_nat m (card (reachable_states ma)));
    distFun =
      (\ k q ->
        (if equal_nat k l
          then (if member q (states ma) then the (hsiMap q) else get_HSI ma q)
          else distSet));
  } in spy_framework_static_with_empty_graph ma distFun m;

add_distinguishing_sequence_and_complete_if_required ::
  forall a b c.
    (Ccompare a, Eq a, Linorder a, Cenum b, Ceq b, Ccompare b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Cenum c, Ceq c, Ccompare c, Eq c,
      Mapping_impl c, Linorder c,
      Set_impl c) => (a -> a -> [(b, c)]) ->
                       Bool ->
                         Fsm a b c ->
                           (([(b, c)], a), ([(b, c)], a)) ->
                             Prefix_tree (b, c) -> Prefix_tree (b, c);
add_distinguishing_sequence_and_complete_if_required distFun completeInputTraces
  m ((alpha, q1), (beta, q2)) t =
  (if intersection_is_distinguishing m (aftera t alpha) q1 (aftera t beta) q2
    then emptyc
    else let {
           w = distFun q1 q2;
           ta = insertb emptyc w;
         } in (if completeInputTraces
                then let {
                       t1 = from_list (language_for_input m q1 (map fst w));
                       t2 = from_list (language_for_input m q2 (map fst w));
                     } in combinea ta (combinea t1 t2)
                else ta));

h_method_via_pair_framework_2 ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Cenum c, Ceq c, Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Prefix_tree (b, c);
h_method_via_pair_framework_2 ma m c =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    a = add_distinguishing_sequence_and_complete_if_required distHelper c;
  } in pair_framework_h_components ma m a;

find_cheapest_distinguishing_trace ::
  forall a b c.
    (Ccompare a, Eq a, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ccompare c,
      Eq c, Mapping_impl c,
      Linorder c) => Fsm a b c ->
                       (a -> a -> [(b, c)]) ->
                         [(b, c)] ->
                           Prefix_tree (b, c) ->
                             a -> Prefix_tree (b, c) ->
                                    a -> ([(b, c)], (Nat, Nat));
find_cheapest_distinguishing_trace m distFun ios (MPT m1) q1 (MPT m2) q2 =
  let {
    f = (\ (omega, (l, w)) (x, y) ->
          let {
            w1L = (if is_leaf (MPT m1) then zero_nat else one_nat);
            w1C = (if not (is_none (lookupa m1 (x, y))) then zero_nat
                    else one_nat);
            w1 = min w1L w1C;
            w2L = (if is_leaf (MPT m2) then zero_nat else one_nat);
            w2C = (if not (is_none (lookupa m2 (x, y))) then zero_nat
                    else one_nat);
            w2 = min w2L w2C;
            wa = plus_nat w1 w2;
          } in (case h_obs m q1 x y of {
                 Nothing ->
                   (case h_obs m q2 x y of {
                     Nothing -> (omega, (l, w));
                     Just _ ->
                       (if equal_nat wa zero_nat || less_eq_nat wa w
                         then ([(x, y)], (plus_nat w1C w2C, wa))
                         else (omega, (l, w)));
                   });
                 Just q1a ->
                   (case h_obs m q2 x y of {
                     Nothing ->
                       (if equal_nat wa zero_nat || less_eq_nat wa w
                         then ([(x, y)], (plus_nat w1C w2C, wa))
                         else (omega, (l, w)));
                     Just q2a ->
                       (if q1a == q2a then (omega, (l, w))
                         else (case lookupa m1 (x, y) of {
                                Nothing ->
                                  (case lookupa m2 (x, y) of {
                                    Nothing ->
                                      let {
omegaa = distFun q1a q2a;
la = plus_nat (nat_of_integer (2 :: Integer))
       (times_nat (nat_of_integer (2 :: Integer)) (size_list omegaa));
                                      } in
(if less_nat wa w || equal_nat wa w && less_nat la l
  then ((x, y) : omegaa, (la, wa)) else (omega, (l, w)));
                                    Just t2 ->
                                      (case
find_cheapest_distinguishing_trace m distFun ios emptyc q1a t2 q2a of {
(omegaa, (la, waa)) ->
  (if less_nat (plus_nat waa w1) w ||
        equal_nat (plus_nat waa w1) w && less_nat (plus_nat la one_nat) l
    then ((x, y) : omegaa, (plus_nat la one_nat, plus_nat waa w1))
    else (omega, (l, w)));
                                      });
                                  });
                                Just t1 ->
                                  (case lookupa m2 (x, y) of {
                                    Nothing ->
                                      (case
find_cheapest_distinguishing_trace m distFun ios t1 q1a emptyc q2a of {
(omegaa, (la, waa)) ->
  (if less_nat (plus_nat waa w2) w ||
        equal_nat (plus_nat waa w2) w && less_nat (plus_nat la one_nat) l
    then ((x, y) : omegaa, (plus_nat la one_nat, plus_nat waa w2))
    else (omega, (l, w)));
                                      });
                                    Just t2 ->
                                      (case
find_cheapest_distinguishing_trace m distFun ios t1 q1a t2 q2a of {
(omegaa, (la, waa)) ->
  (if less_nat waa w || equal_nat waa w && less_nat la l
    then ((x, y) : omegaa, (la, waa)) else (omega, (l, w)));
                                      });
                                  });
                              }));
                   });
               }));
  } in foldl f (distFun q1 q2, (zero_nat, nat_of_integer (3 :: Integer))) ios;

add_cheapest_distinguishing_trace ::
  forall a b c.
    (Ccompare a, Eq a, Linorder a, Ceq b, Ccompare b, Eq b, Mapping_impl b,
      Linorder b, Ceq c, Ccompare c, Eq c, Mapping_impl c,
      Linorder c) => (a -> a -> [(b, c)]) ->
                       Bool ->
                         Fsm a b c ->
                           (([(b, c)], a), ([(b, c)], a)) ->
                             Prefix_tree (b, c) -> Prefix_tree (b, c);
add_cheapest_distinguishing_trace distFun completeInputTraces m
  ((alpha, q1), (beta, q2)) t =
  let {
    w = fst (find_cheapest_distinguishing_trace m distFun
              (product (inputs_as_list m) (outputs_as_list m)) (aftera t alpha)
              q1 (aftera t beta) q2);
    ta = insertb emptyc w;
  } in (if completeInputTraces
         then let {
                t1 = complete_inputs_to_tree m q1 (outputs_as_list m)
                       (map fst w);
                t2 = complete_inputs_to_tree m q2 (outputs_as_list m)
                       (map fst w);
              } in combinea ta (combinea t1 t2)
         else ta);

h_method_via_pair_framework_3 ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
h_method_via_pair_framework_3 ma m c1 c2 =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    distFun = add_cheapest_distinguishing_trace distHelper c2;
  } in pair_framework_h_components_2 ma m distFun c1;

apply_method_to_prime ::
  Fsm Integer Integer Integer ->
    Integer ->
      Bool ->
        (Fsm Integer Integer Integer ->
          Nat -> Prefix_tree (Integer, Integer)) ->
          Prefix_tree (Integer, Integer);
apply_method_to_prime m additionalStates isAlreadyPrime f =
  let {
    ma = (if isAlreadyPrime then m else to_prime m);
    a = plus_nat (card (reachable_states ma)) (nat_of_integer additionalStates);
  } in f ma a;

fsm_from_list_integer ::
  Integer ->
    [(Integer, (Integer, (Integer, Integer)))] -> Fsm Integer Integer Integer;
fsm_from_list_integer q ts = fsm_from_list q ts;

hsi_method_via_spy_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
hsi_method_via_spy_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    distFun =
      (\ _ q ->
        (if member q (states ma) then the (hsiMap q) else get_HSI ma q));
  } in spy_framework_static_with_empty_graph ma distFun m;

h_framework_dynamic_with_simple_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => (Fsm a b c ->
                       (a -> [(b, c)]) ->
                         (a, (b, (c, a))) ->
                           [(a, (b, (c, a)))] -> Nat -> Bool) ->
                       Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
h_framework_dynamic_with_simple_graph convergence_decisision m1 m
  completeInputTraces useInputHeuristic =
  h_framework m1 get_state_cover_assignment
    (handle_state_cover_dynamic completeInputTraces useInputHeuristic
      (get_distinguishing_sequence_from_ofsm_tables m1))
    sort_unverified_transitions_by_state_cover_length
    (handleUT_dynamic completeInputTraces useInputHeuristic
      (get_distinguishing_sequence_from_ofsm_tables m1) convergence_decisision)
    (handle_io_pair completeInputTraces useInputHeuristic) simple_cg_initial
    simple_cg_insert simple_cg_lookup_with_conv simple_cg_merge m;

spyh_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
spyh_method_via_h_framework =
  h_framework_dynamic_with_simple_graph (\ _ _ _ _ _ -> True);

spy_framework_static_with_simple_graph ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c ->
                       (Nat -> a -> Prefix_tree (b, c)) ->
                         Nat -> Prefix_tree (b, c);
spy_framework_static_with_simple_graph m1 dist_fun m =
  spy_framework m1 get_state_cover_assignment
    (handle_state_cover_static dist_fun) (\ _ _ ts -> ts)
    (establish_convergence_static dist_fun) (handle_io_pair False True)
    simple_cg_initial simple_cg_insert simple_cg_lookup_with_conv
    simple_cg_merge m;

spy_method_via_spy_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
spy_method_via_spy_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    hsiMap =
      map_of
        (map (\ q ->
               (q, from_list
                     (map_filter
                       (\ x ->
                         (if not (q == x) then Just (distHelper q x)
                           else Nothing))
                       (states_as_list ma))))
          (states_as_list ma));
    distFun =
      (\ _ q ->
        (if member q (states ma) then the (hsiMap q) else get_HSI ma q));
  } in spy_framework_static_with_simple_graph ma distFun m;

hsi_method_via_pair_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c,
      Ccompare c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Prefix_tree (b, c);
hsi_method_via_pair_framework ma m =
  let {
    tables = compute_ofsm_tables ma (minus_nat (size ma) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables ma q1 q2);
                        })
              else Nothing))
          (product (states_as_list ma) (states_as_list ma)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states ma) && member q2 (states ma) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables ma q1 q2));
    a = (\ _ (a, b) ->
          (case a of {
            (_, q1) -> (\ (_, q2) _ -> insertb emptyc (distHelper q1 q2));
          })
            b);
  } in pair_framework_h_components ma m a;

test_suite_to_input_sequences ::
  forall a b.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Linorder b, Set_impl b) => Prefix_tree (a, b) -> [[a]];
test_suite_to_input_sequences t =
  sorted_list_of_maximal_sequences_in_tree
    (from_list (map (map fst) (sorted_list_of_maximal_sequences_in_tree t)));

do_establish_convergence ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Linorder a, Set_impl a, Ceq b,
      Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Linorder c,
      Set_impl c) => Fsm a b c ->
                       (a -> [(b, c)]) ->
                         (a, (b, (c, a))) -> [(a, (b, (c, a)))] -> Nat -> Bool;
do_establish_convergence m v t x l =
  not (is_none
        (find (\ ta -> distance_at_most m (snd (snd (snd t))) (fst ta) l) x));

establish_convergence_dynamic ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Ccompare a, Eq a, Linorder a, Set_impl a,
      Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b, Ceq c, Ccompare c,
      Eq c, Mapping_impl c,
      Linorder c) => Bool ->
                       Bool ->
                         (a -> a -> [(b, c)]) ->
                           Fsm a b c ->
                             (a -> [(b, c)]) ->
                               Prefix_tree (b, c) ->
                                 d -> (d -> [(b, c)] -> d) ->
(d -> [(b, c)] -> [[(b, c)]]) ->
  Nat -> (a, (b, (c, a))) -> (Prefix_tree (b, c), d);
establish_convergence_dynamic completeInputTraces useInputHeuristic dist_fun m1
  v ta g cg_insert cg_lookup m t =
  distinguish_from_set m1 v ta g cg_lookup cg_insert dist_fun
    (v (fst t) ++ [(fst (snd t), fst (snd (snd t)))]) (v (snd (snd (snd t))))
    (map v (reachable_states_as_list m1))
    (times_nat (nat_of_integer (2 :: Integer)) (size m1))
    (minus_nat m (card (reachable_states m1))) completeInputTraces
    (if useInputHeuristic then append_heuristic_input m1
      else append_heuristic_io)
    False;

spyh_method_via_spy_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
spyh_method_via_spy_framework m1 m completeInputTraces useInputHeuristic =
  let {
    tables = compute_ofsm_tables m1 (minus_nat (size m1) one_nat);
    distMap =
      map_of
        (map_filter
          (\ x ->
            (if not (fst x == snd x)
              then Just (case x of {
                          (q1, q2) ->
                            ((q1, q2),
                              get_distinguishing_sequence_from_ofsm_tables_with_provided_tables
                                tables m1 q1 q2);
                        })
              else Nothing))
          (product (states_as_list m1) (states_as_list m1)));
    distHelper =
      (\ q1 q2 ->
        (if member q1 (states m1) && member q2 (states m1) && not (q1 == q2)
          then the (distMap (q1, q2))
          else get_distinguishing_sequence_from_ofsm_tables m1 q1 q2));
  } in spy_framework m1 get_state_cover_assignment
         (handle_state_cover_dynamic completeInputTraces useInputHeuristic
           distHelper)
         sort_unverified_transitions_by_state_cover_length
         (establish_convergence_dynamic completeInputTraces useInputHeuristic
           distHelper)
         (handle_io_pair completeInputTraces useInputHeuristic)
         simple_cg_initial simple_cg_insert simple_cg_lookup_with_conv
         simple_cg_merge m;

apply_to_prime_and_return_io_lists ::
  Fsm Integer Integer Integer ->
    Integer ->
      Bool ->
        (Fsm Integer Integer Integer ->
          Nat -> Prefix_tree (Integer, Integer)) ->
          [[((Integer, Integer), Bool)]];
apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime f =
  let {
    ma = (if isAlreadyPrime then m else to_prime m);
  } in sorted_list_of_maximal_sequences_in_tree
         (test_suite_from_io_tree ma (initial ma)
           (apply_method_to_prime m additionalStates isAlreadyPrime f));

h_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
h_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_h_framework ma maa c b);

w_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
w_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    w_method_via_h_framework;

wp_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
wp_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    wp_method_via_h_framework;

hsi_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
hsi_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    hsi_method_via_h_framework;

spy_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
spy_method_via_h_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    spy_method_via_h_framework;

w_method_via_h_framework_2_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
w_method_via_h_framework_2_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    w_method_via_h_framework_2;

w_method_via_spy_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
w_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    w_method_via_spy_framework;

apply_to_prime_and_return_input_lists ::
  Fsm Integer Integer Integer ->
    Integer ->
      Bool ->
        (Fsm Integer Integer Integer ->
          Nat -> Prefix_tree (Integer, Integer)) ->
          [[Integer]];
apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime f =
  test_suite_to_input_sequences
    (apply_method_to_prime m additionalStates isAlreadyPrime f);

h_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> Bool -> [[Integer]];
h_method_via_h_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_h_framework ma maa c b);

h_method_via_pair_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
h_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    h_method_via_pair_framework;

spyh_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
spyh_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> spyh_method_via_h_framework ma maa c b);

w_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
w_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    w_method_via_h_framework;

w_method_via_pair_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
w_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    w_method_via_pair_framework;

wp_method_via_spy_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
wp_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    wp_method_via_spy_framework;

hsi_method_via_spy_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
hsi_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    hsi_method_via_spy_framework;

spy_method_via_spy_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
spy_method_via_spy_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    spy_method_via_spy_framework;

wp_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
wp_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    wp_method_via_h_framework;

partial_s_method_via_h_framework ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c, Linorder c,
      Set_impl c) => Fsm a b c -> Nat -> Bool -> Bool -> Prefix_tree (b, c);
partial_s_method_via_h_framework =
  h_framework_dynamic_with_simple_graph do_establish_convergence;

h_method_via_pair_framework_2_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
h_method_via_pair_framework_2_ts m additionalStates isAlreadyPrime c =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_pair_framework_2 ma maa c);

h_method_via_pair_framework_3_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
h_method_via_pair_framework_3_ts m additionalStates isAlreadyPrime c1 c2 =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_pair_framework_3 ma maa c1 c2);

hsi_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
hsi_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    hsi_method_via_h_framework;

hsi_method_via_pair_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> [[((Integer, Integer), Bool)]];
hsi_method_via_pair_framework_ts m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    hsi_method_via_pair_framework;

spy_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
spy_method_via_h_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    spy_method_via_h_framework;

spyh_method_via_spy_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
spyh_method_via_spy_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> spyh_method_via_spy_framework ma maa c b);

w_method_via_h_framework_2_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
w_method_via_h_framework_2_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    w_method_via_h_framework_2;

w_method_via_spy_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
w_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    w_method_via_spy_framework;

h_method_via_pair_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
h_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    h_method_via_pair_framework;

spyh_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> Bool -> [[Integer]];
spyh_method_via_h_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> spyh_method_via_h_framework ma maa c b);

w_method_via_pair_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
w_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    w_method_via_pair_framework;

wp_method_via_spy_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
wp_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    wp_method_via_spy_framework;

hsi_method_via_spy_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
hsi_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    hsi_method_via_spy_framework;

spy_method_via_spy_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
spy_method_via_spy_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    spy_method_via_spy_framework;

calculate_test_suite_naive_as_io_sequences_with_assumption_check ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Nat -> Sum String (Set [(b, c)]);
calculate_test_suite_naive_as_io_sequences_with_assumption_check ma m =
  (if not (is_empty (inputs ma))
    then (if observable ma
           then (if completely_specified ma
                  then Inr (test_suite_to_io_maximal ma
                             (calculate_test_suite_naive ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

generate_reduction_test_suite_naive ::
  Fsm Integer Integer Integer -> Integer -> Sum String [[(Integer, Integer)]];
generate_reduction_test_suite_naive ma m =
  (case calculate_test_suite_naive_as_io_sequences_with_assumption_check ma
          (nat_of_integer m)
    of {
    Inl a -> Inl a;
    Inr ts -> Inr (sorted_list_of_set ts);
  });

h_method_via_pair_framework_2_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> [[Integer]];
h_method_via_pair_framework_2_input m additionalStates isAlreadyPrime c =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_pair_framework_2 ma maa c);

h_method_via_pair_framework_3_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> Bool -> [[Integer]];
h_method_via_pair_framework_3_input m additionalStates isAlreadyPrime c1 c2 =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> h_method_via_pair_framework_3 ma maa c1 c2);

hsi_method_via_pair_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> [[Integer]];
hsi_method_via_pair_framework_input m additionalStates isAlreadyPrime =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    hsi_method_via_pair_framework;

partial_s_method_via_h_framework_ts ::
  Fsm Integer Integer Integer ->
    Integer -> Bool -> Bool -> Bool -> [[((Integer, Integer), Bool)]];
partial_s_method_via_h_framework_ts m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_io_lists m additionalStates isAlreadyPrime
    (\ ma maa -> partial_s_method_via_h_framework ma maa c b);

spyh_method_via_spy_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> Bool -> [[Integer]];
spyh_method_via_spy_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> spyh_method_via_spy_framework ma maa c b);

calculate_test_suite_greedy_as_io_sequences_with_assumption_check ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Mapping_impl c,
      Set_impl c) => Fsm a b c -> Nat -> Sum String (Set [(b, c)]);
calculate_test_suite_greedy_as_io_sequences_with_assumption_check ma m =
  (if not (is_empty (inputs ma))
    then (if observable ma
           then (if completely_specified ma
                  then Inr (test_suite_to_io_maximal ma
                             (calculate_test_suite_greedy ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

generate_reduction_test_suite_greedy ::
  Fsm Integer Integer Integer -> Integer -> Sum String [[(Integer, Integer)]];
generate_reduction_test_suite_greedy ma m =
  (case calculate_test_suite_greedy_as_io_sequences_with_assumption_check ma
          (nat_of_integer m)
    of {
    Inl a -> Inl a;
    Inr ts -> Inr (sorted_list_of_set ts);
  });

partial_s_method_via_h_framework_input ::
  Fsm Integer Integer Integer -> Integer -> Bool -> Bool -> Bool -> [[Integer]];
partial_s_method_via_h_framework_input m additionalStates isAlreadyPrime c b =
  apply_to_prime_and_return_input_lists m additionalStates isAlreadyPrime
    (\ ma maa -> partial_s_method_via_h_framework ma maa c b);

}
