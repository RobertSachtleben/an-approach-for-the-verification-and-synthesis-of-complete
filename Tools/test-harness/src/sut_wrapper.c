#include "sut_example.h"

void sut_init() {
  example_reset();
}

void sut_reset() {
  example_reset();
}

int sut(int n) {
  example_apply(n);
}