#include "sut_example.h"
#include <stdlib.h>

typedef enum {
    S0,
    S1,
    S2,
    S3
} example_state_t;

example_state_t current_state = S0;

void example_reset() {
  current_state = S0;
}

int handle_state_S0(int input) {
  int result = 0;
  switch (input) {
    case 0 : 
      result = 2 + (rand() % 2);
      current_state = S2;
      break;
    case 1 : 
    default : 
      result = rand() % 2;
      current_state = S3;  // set to S2 to introduce a transition fault
      break;
  }
  return result;
}

int handle_state_S1(int input) {
  int result = 0;
  switch (input) {
    case 0 : 
      result = 3;
      current_state = S2;
      break;
    case 1 : 
    default : 
      result = 1;
      current_state = S3;
      break;  
  }
  return result;
}

int handle_state_S2(int input) {
  int result = 0;
  switch (input) {
    case 0 : 
      result = 2;
      current_state = S2;
      break;
    case 1 : 
    default : 
      result = 3;
      current_state = S3;
      break;  
  }
  return result;
}

int handle_state_S3(int input) {
  int result = 0;
  switch (input) {
    case 0 : 
      result = 2;
      current_state = S2;
      break;
    case 1 : 
    default :
      if (rand() % 2 == 0) {
        result = 1;
        current_state = S1;
      } else {
        result = 0;
        current_state = S2;
      }
      break;  
  }
  return result;
}


int example_apply(int input) {
  switch (current_state) {
    case(S0): return handle_state_S0(input);
    case(S1): return handle_state_S1(input);
    case(S2): return handle_state_S2(input);
    case(S3): return handle_state_S3(input);
  }
}
