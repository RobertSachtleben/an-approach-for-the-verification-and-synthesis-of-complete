{-# LANGUAGE ForeignFunctionInterface #-}

module Main where
    
import qualified Data.List.Extra as DLE
import qualified Text.Read as TR
import Data.Maybe
import Options.Applicative
import Data.List
import Data.Char
import Control.Monad.Extra

foreign import ccall "sut_init" sut_init :: IO ()
foreign import ccall "sut_reset" sut_reset :: IO ()
foreign import ccall "sut" sut :: Int -> IO Int

data Verdict = PASS | FAIL | INCONCLUSIVE deriving (Show,Eq)

showIOTrace :: [(Int,Int)] -> String
showIOTrace [] = ""
showIOTrace ((x,y):[]) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")"
showIOTrace ((x,y):(x',y'):xys) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")" ++ "." ++ (showIOTrace $ (x',y'):xys)

showIOBTestCase :: [((Int, Int), Bool)] -> String
showIOBTestCase [] = ""
showIOBTestCase (((x,y),b):[]) = "((" ++ (show x) ++ "/" ++ (show y) ++ ")," ++ (if b then "T)" else "F)")
showIOBTestCase (((x,y),b):((x',y'),b'):xys) = "((" ++ (show x) ++ "/" ++ (show y) ++ ")," ++ (if b then "T)" else "F)") ++ "." ++ (showIOBTestCase $ ((x',y'),b'):xys)

showInputSequence :: [Int] -> String
showInputSequence [] = ""
showInputSequence (x:[]) = show x
showInputSequence (x:y:xs) = show x ++ "," ++ showInputSequence (y:xs)

readIOPair :: String -> Either String (Int,Int)
readIOPair x@('(':rem) = if length rem' == 2 && last rem'' == ')' && isJust input && isJust output
                            then Right (fromJust input, fromJust output) 
                            else Left $ "not an IO pair: " ++ x  
    where 
        rem' = DLE.split (=='/') rem    
        input = TR.readMaybe (rem' !! 0) :: Maybe Int
        rem'' = rem' !! 1
        output = TR.readMaybe (init rem'') :: Maybe Int
readIOPair x = Left $ "not an IO pair: " ++ x    

readTestCaseIO :: String -> Either String [(Int,Int)]
readTestCaseIO str = case mapM readIOPair $ DLE.split (== '.') str of
  Left err -> Left $ "Error parsing test case " ++ str ++ " : " ++ err
  Right tc -> Right tc

readIOBTriple :: String -> Either String ((Int,Int),Bool)
readIOBTriple x@('(':'(':rem) = if length rem' == 2 
                               && length rem'' == 2
                               && not (null rem''') && last rem''' == ')'
                               && isJust input && isJust output && isJust flag
                            then Right ((fromJust input, fromJust output), fromJust flag) 
                            else Left $ "not an IOB triple: " ++ x  
    where 
        rem' = DLE.split (=='/') rem    
        input = TR.readMaybe (rem' !! 0) :: Maybe Int
        rem'' = DLE.split (==',') $ rem' !! 1
        rem''' = rem'' !! 0
        rem'''' = rem'' !! 1
        output = TR.readMaybe (init rem''') :: Maybe Int
        flag = case rem'''' of "T)" -> Just True 
                               "F)" -> Just False
                               _    -> Nothing
readIOBTriple x = Left $ "not an IOB triple: " ++ x    

readTestCaseIOB :: String -> Either String [((Int,Int),Bool)]
readTestCaseIOB str = case mapM readIOBTriple $ DLE.split (== '.') str of
  Left err -> Left $ "Error parsing test case " ++ str ++ " : " ++ err
  Right tc -> Right tc  

readTestCaseI :: String -> Either String [Int]
readTestCaseI str = if all (all isDigit) withoutSeperators then Right $ map read withoutSeperators 
                                                           else Left $ "Error parsing test case " ++ str  
  where 
    withoutSeperators = DLE.split (== ',') str

readTestSuite :: TestSuiteType -> FilePath -> IO (Either String TestSuite)
readTestSuite I file = do 
    content <- readFile file
    return $ fmap TS_I $ mapM readTestCaseI $ lines content
readTestSuite IO file = do 
    content <- readFile file
    return $ fmap TS_IO $ mapM readTestCaseIO $ lines content
readTestSuite IOB file = do 
    content <- readFile file
    return $ fmap TS_IOB $ mapM readTestCaseIOB $ lines content         


runTestCaseIOUntilDeviation :: [(Int,Int)] -> IO [(Int,Int)]
runTestCaseIOUntilDeviation [] = return []
runTestCaseIOUntilDeviation ((x,y):ts) = do
  y' <- sut x
  if y == y'
    then do
      remainder <- runTestCaseIOUntilDeviation ts
      return $ (x,y):remainder
    else
      return [(x,y')] 

{-
  a test case in the form of a list of IO-pairs is applied as follows:
  1.) the SUT is reset 
  2.) the input of the head of the test case is applied to the SUT
      - if the observed output is equal to the output in the head of the test case,
        then the next input in the test case is applied, repeating this process
        until either the test case has been fully processed or an output is observed
        that deviates from the corresponding output in the test case
      - if the observed output does not equal the corresponding output in the test case,
        then test case execution stops
  3.) the observed response of the SUT to the test case is checked against the test suite:      
      - if the observed response is a prefix of some test case in the test suite, then
        the application reports PASS, else FAIL
-}
applyTestCaseIO :: [[(Int,Int)]] -> String -> [(Int,Int)] -> IO Verdict
applyTestCaseIO testSuite outputFile testCase = do
  sut_reset
  appendFile outputFile $     "Applying              : " ++ (showIOTrace testCase) ++ "\n"
  observedBehaviour <- runTestCaseIOUntilDeviation testCase
  appendFile outputFile $     "    observed reaction : " ++ (showIOTrace observedBehaviour) ++ "\n"
  if testCase == observedBehaviour 
    then do
      appendFile outputFile   "        verdict       : PASS\n"
      return PASS
    else if any (isPrefixOf observedBehaviour) testSuite
      then do
        appendFile outputFile "        verdict       : PASS (observed reaction is prefix of another test case in the test suite)\n"
        return PASS
      else do 
        appendFile outputFile "        verdict       : FAIL\n"
        return FAIL

repeatedlyapplyTestCaseIO :: [[(Int,Int)]] -> String -> Int -> [(Int,Int)] -> IO (String,Verdict)
repeatedlyapplyTestCaseIO testSuite outputFile k testCase = do
  results <- mapM (\_ -> applyTestCaseIO testSuite outputFile testCase) [1..k]
  return ( showIOTrace testCase, if elem FAIL results then FAIL else PASS )

runTestCaseIOBUntilDeviation :: [((Int,Int),Bool)] -> IO ([(Int,Int)],Verdict)
runTestCaseIOBUntilDeviation [] = return ([], PASS)
runTestCaseIOBUntilDeviation (((x,y),b):ts) = do
  y' <- sut x
  if y == y'
    then if b 
      then do (remainder,v) <- runTestCaseIOBUntilDeviation ts
              return ((x,y):remainder, v)
      else return ([(x,y')], FAIL)
    else return ([(x,y')], if b then INCONCLUSIVE else PASS)

{-
  Applies an IO trace analogous to applyTestCaseIO, but also stops
  for current triple ((x/y),b) and observed output y' of the SUT if
  a) y=y' and b=False - in this case a failure has been observed 
                        and FAIL is returned
  b) y!=y' and b=False - in this case all prefixes of the test case
                         in L(M) have been observed and PASS
                         is returned
  c) y!=y' and b=True - in this case the test is inconclusive
                        and INCONCLUSIVE is returned.  
-}
applyTestCaseIOB :: String -> [((Int,Int),Bool)] -> IO Verdict
applyTestCaseIOB outputFile testCase = do
  sut_reset
  appendFile outputFile $ "Applying              : " ++ (showIOBTestCase testCase) ++ "\n"
  (observedBehaviour,pass) <- runTestCaseIOBUntilDeviation testCase
  appendFile outputFile $ "    observed reaction : " ++ (showIOTrace observedBehaviour) ++ "\n"
  appendFile outputFile $ "        verdict       : " ++ (case pass of INCONCLUSIVE -> "INCONCLUSIVE (observed different response)\n"
                                                                      PASS         -> "PASS\n"
                                                                      FAIL         -> "FAIL (observed response not in reference model)\n")
  return pass

repeatedlyapplyTestCaseIOB :: String -> Int -> [((Int,Int),Bool)] -> IO (String,Verdict)
repeatedlyapplyTestCaseIOB outputFile k testCase = do
  results <- mapM (\_ -> applyTestCaseIOB outputFile testCase) [1..k]
  return ( showIOBTestCase testCase
         , if elem FAIL results then FAIL else if elem PASS results then PASS else INCONCLUSIVE )


data TestSuiteType = I | IO | IOB deriving (Show,Eq)
data TestSuite = TS_I [[Int]] | TS_IO [[(Int,Int)]] | TS_IOB [[((Int, Int), Bool)]]

data Opts = Opts { testSuiteFilePath       :: String 
                 , resultFilePath          :: String
                 , repetitions             :: Int
                 , testAgainstReduction    :: Bool
                 , testSuiteType           :: TestSuiteType }

getOpts :: Parser Opts
getOpts = Opts 
            <$> strArgument
                ( metavar "TESTSUITE_PATH"
                  <> help "path to a test suite" )
            <*> strOption
                ( short 'o'
                    <> long "output"
                    <> metavar "PATH"
                    <> help "File into which the results are written (will be cleared if it already exists)." 
                    <> showDefault
                    <> value "results.txt" )                   
            <*> option auto 
                ( short 'n'
                    <> long "repetitions"
                    <> metavar "N"
                    <> help "Number of times each line of the test suite is applied to the SUT to try to observe all reactions." 
                    <> showDefault
                    <> value (100::Int) )
            <*> switch
                ( short 'r'
                    <> long "reduction"
                    <> help "If set, then the reduction conformance relation is tested against (instead of language-equivalence)." )
            <*> (flag' IO (long "IO" <> help "Test suite is given as a list of IO traces (currently only supported for reduction testing).") 
                    <|> flag' IOB (long "IOB" <> help "Test suite is given as a list of IO traces with containment flags (currently only supported for equivalence testing).") )

main :: IO ()
main = testSUT =<< execParser opts
  where
    opts = info (getOpts <**> helper)
      ( fullDesc
        <> progDesc (  "Applies a test suite to an SUT (establishing an interface via sut_wrapper.c) and checks whether the SUT passes the test suite. " )
                       -- ++ "An SUT passes a test suite if for every IO sequence (x1/y1)...(xn/yn) in the test suite the SUT does not exhibit some response (x1,y1)...(xi/yi') with i <= n such that yi' is the first output that deviates from the corresponding output in (x1/y1)...(xn/yn) and such that (x1,y1)...(xi/yi') is not prefix of any test case in the test suite.")
        <> header "Test Harness" )


testSUT :: Opts -> IO () 
testSUT opts = if not $ elem (testAgainstReduction opts, testSuiteType opts) [(True,IO),(False,IOB)]
  then putStrLn "Selected combination of conformance relation and test suite format currently not supported."
  else do 
    testSuite' <- readTestSuite (testSuiteType opts) $ testSuiteFilePath opts 
    case testSuite' of
      Left err -> error $ "Error parsing test suite: " ++ err
      Right testSuite -> do
        sut_init
        writeFile (resultFilePath opts) ""
        results <- case testSuite of 
          (TS_IO  ts) -> mapM (repeatedlyapplyTestCaseIO ts (resultFilePath opts) (repetitions opts)) ts
          (TS_IOB ts) -> mapM (repeatedlyapplyTestCaseIOB (resultFilePath opts) (repetitions opts)) ts
          _           -> error "unsupported test suite type" 
        let verdict = case find ((== FAIL) . snd) results of 
                        Just (tc,_) -> "FAIL (observed response not in reference model) -- see test case: " ++ tc
                        Nothing -> if testAgainstReduction opts 
                          then "PASS"
                          else case find ((== INCONCLUSIVE) . snd) results of  
                            Just (tc,_) ->  "FAIL (failed to observe response required by reference model) -- see test case: " ++ tc
                            Nothing -> "PASS"
        appendFile (resultFilePath opts) $ "\n\n verdict: " ++ verdict


