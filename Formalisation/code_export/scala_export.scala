object GeneratedCode {

abstract sealed class ordera
final case class Eqa() extends ordera
final case class Lt() extends ordera
final case class Gt() extends ordera

trait ccompare[A] {
  val `GeneratedCode.ccompare`: Option[A => A => ordera]
}
def ccompare[A](implicit A: ccompare[A]): Option[A => A => ordera] =
  A.`GeneratedCode.ccompare`
object ccompare {
  implicit def `GeneratedCode.ccompare_integer`: ccompare[BigInt] = new
    ccompare[BigInt] {
    val `GeneratedCode.ccompare` = ccompare_integera
  }
  implicit def
    `GeneratedCode.ccompare_prod`[A : ccompare, B : ccompare]: ccompare[(A, B)]
    = new ccompare[(A, B)] {
    val `GeneratedCode.ccompare` = ccompare_proda[A, B]
  }
  implicit def
    `GeneratedCode.ccompare_option`[A : ccompare]: ccompare[Option[A]] = new
    ccompare[Option[A]] {
    val `GeneratedCode.ccompare` = ccompare_optiona[A]
  }
  implicit def
    `GeneratedCode.ccompare_sum`[A : ccompare, B : ccompare]:
      ccompare[sum[A, B]]
    = new ccompare[sum[A, B]] {
    val `GeneratedCode.ccompare` = ccompare_suma[A, B]
  }
  implicit def `GeneratedCode.ccompare_list`[A : ccompare]: ccompare[List[A]] =
    new ccompare[List[A]] {
    val `GeneratedCode.ccompare` = ccompare_lista[A]
  }
  implicit def `GeneratedCode.ccompare_fset`[A]: ccompare[fset[A]] = new
    ccompare[fset[A]] {
    val `GeneratedCode.ccompare` = ccompare_fseta[A]
  }
  implicit def `GeneratedCode.ccompare_bool`: ccompare[Boolean] = new
    ccompare[Boolean] {
    val `GeneratedCode.ccompare` = ccompare_boola
  }
  implicit def
    `GeneratedCode.ccompare_set`[A : finite_univ : ceq : cproper_interval : set_impl]:
      ccompare[set[A]]
    = new ccompare[set[A]] {
    val `GeneratedCode.ccompare` = ccompare_seta[A]
  }
  implicit def `GeneratedCode.ccompare_nat`: ccompare[nat] = new ccompare[nat] {
    val `GeneratedCode.ccompare` = ccompare_nata
  }
  implicit def `GeneratedCode.ccompare_fsm`[A, B, C]: ccompare[fsm[A, B, C]] =
    new ccompare[fsm[A, B, C]] {
    val `GeneratedCode.ccompare` = ccompare_fsma[A, B, C]
  }
}

def equal_order(x0: ordera, x1: ordera): Boolean = (x0, x1) match {
  case (Lt(), Gt()) => false
  case (Gt(), Lt()) => false
  case (Eqa(), Gt()) => false
  case (Gt(), Eqa()) => false
  case (Eqa(), Lt()) => false
  case (Lt(), Eqa()) => false
  case (Gt(), Gt()) => true
  case (Lt(), Lt()) => true
  case (Eqa(), Eqa()) => true
}

abstract sealed class generator[A, B]
final case class Generatora[B, A](a: (B => Boolean, B => (A, B))) extends
  generator[A, B]

def generator[A, B](x0: generator[A, B]): (B => Boolean, B => (A, B)) = x0 match
  {
  case Generatora(x) => x
}

def fst[A, B](x0: (A, B)): A = x0 match {
  case (x1, x2) => x1
}

def has_next[A, B](g: generator[A, B]): B => Boolean =
  fst[B => Boolean, B => (A, B)](generator[A, B](g))

def snd[A, B](x0: (A, B)): B = x0 match {
  case (x1, x2) => x2
}

def next[A, B](g: generator[A, B]): B => (A, B) =
  snd[B => Boolean, B => (A, B)](generator[A, B](g))

def sorted_list_subset_fusion[A, B,
                               C](less: A => A => Boolean,
                                   eq: A => A => Boolean, g1: generator[A, B],
                                   g2: generator[A, C], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    {
      val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B));
      (has_next[A, C](g2)).apply(s2) &&
        {
          val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
          (if ((eq(x))(y))
            sorted_list_subset_fusion[A, B, C](less, eq, g1, g2, s1a, s2a)
            else (less(y))(x) &&
                   sorted_list_subset_fusion[A, B,
      C](less, eq, g1, g2, s1, s2a))
        }
    }
    else true)

def list_all_fusion[A, B](g: generator[A, B], p: A => Boolean, s: B): Boolean =
  (if ((has_next[A, B](g)).apply(s))
    {
      val (x, sa) = (next[A, B](g)).apply(s): ((A, B));
      p(x) && list_all_fusion[A, B](g, p, sa)
    }
    else true)

trait ceq[A] {
  val `GeneratedCode.ceq`: Option[A => A => Boolean]
}
def ceq[A](implicit A: ceq[A]): Option[A => A => Boolean] =
  A.`GeneratedCode.ceq`
object ceq {
  implicit def `GeneratedCode.ceq_integer`: ceq[BigInt] = new ceq[BigInt] {
    val `GeneratedCode.ceq` = ceq_integera
  }
  implicit def `GeneratedCode.ceq_prod`[A : ceq, B : ceq]: ceq[(A, B)] = new
    ceq[(A, B)] {
    val `GeneratedCode.ceq` = ceq_proda[A, B]
  }
  implicit def `GeneratedCode.ceq_option`[A : ceq]: ceq[Option[A]] = new
    ceq[Option[A]] {
    val `GeneratedCode.ceq` = ceq_optiona[A]
  }
  implicit def `GeneratedCode.ceq_sum`[A : ceq, B : ceq]: ceq[sum[A, B]] = new
    ceq[sum[A, B]] {
    val `GeneratedCode.ceq` = ceq_suma[A, B]
  }
  implicit def `GeneratedCode.ceq_list`[A : ceq]: ceq[List[A]] = new
    ceq[List[A]] {
    val `GeneratedCode.ceq` = ceq_lista[A]
  }
  implicit def
    `GeneratedCode.ceq_fset`[A : cenum : ceq : ccompare : equal]: ceq[fset[A]] =
    new ceq[fset[A]] {
    val `GeneratedCode.ceq` = ceq_fseta[A]
  }
  implicit def `GeneratedCode.ceq_bool`: ceq[Boolean] = new ceq[Boolean] {
    val `GeneratedCode.ceq` = ceq_boola
  }
  implicit def `GeneratedCode.ceq_set`[A : cenum : ceq : ccompare]: ceq[set[A]]
    = new ceq[set[A]] {
    val `GeneratedCode.ceq` = ceq_seta[A]
  }
  implicit def `GeneratedCode.ceq_nat`: ceq[nat] = new ceq[nat] {
    val `GeneratedCode.ceq` = ceq_nata
  }
  implicit def
    `GeneratedCode.ceq_fsm`[A : cenum : ceq : ccompare : equal,
                             B : cenum : ceq : ccompare,
                             C : cenum : ceq : ccompare]:
      ceq[fsm[A, B, C]]
    = new ceq[fsm[A, B, C]] {
    val `GeneratedCode.ceq` = ceq_fsma[A, B, C]
  }
}

abstract sealed class color
final case class R() extends color
final case class B() extends color

abstract sealed class rbt[A, B]
final case class Emptyd[A, B]() extends rbt[A, B]
final case class Branch[A, B](a: color, b: rbt[A, B], c: A, d: B, e: rbt[A, B])
  extends rbt[A, B]

def rbt_keys_next[A, B](x0: (List[(A, rbt[A, B])], rbt[A, B])):
      (A, (List[(A, rbt[A, B])], rbt[A, B]))
  =
  x0 match {
  case ((k, t) :: kts, Emptyd()) => (k, (kts, t))
  case (kts, Branch(c, l, k, v, r)) => rbt_keys_next[A, B](((k, r) :: kts, l))
}

def rbt_has_next[A, B, C](x0: (List[(A, rbt[B, C])], rbt[B, C])): Boolean = x0
  match {
  case (Nil, Emptyd()) => false
  case (vb :: vc, va) => true
  case (v, Branch(vb, vc, vd, ve, vf)) => true
}

def rbt_keys_generator[A, B]: generator[A, (List[(A, rbt[A, B])], rbt[A, B])] =
  Generatora[(List[(A, rbt[A, B])], rbt[A, B]),
              A]((((a: (List[(A, rbt[A, B])], rbt[A, B])) =>
                    rbt_has_next[A, A, B](a)),
                   ((a: (List[(A, rbt[A, B])], rbt[A, B])) =>
                     rbt_keys_next[A, B](a))))

def lt_of_comp[A](acomp: A => A => ordera, x: A, y: A): Boolean =
  ((acomp(x))(y) match {
     case Eqa() => false
     case Lt() => true
     case Gt() => false
   })

abstract sealed class mapping_rbt[B, A]
final case class Mapping_rbtb[B, A](a: rbt[B, A]) extends mapping_rbt[B, A]

abstract sealed class set_dlist[A]
final case class Abs_dlist[A](a: List[A]) extends set_dlist[A]

abstract sealed class set[A]
final case class Collect_set[A](a: A => Boolean) extends set[A]
final case class Dlist_set[A](a: set_dlist[A]) extends set[A]
final case class Rbt_set[A](a: mapping_rbt[A, Unit]) extends set[A]
final case class Set_monad[A](a: List[A]) extends set[A]
final case class Complement[A](a: set[A]) extends set[A]

def list_of_dlist[A : ceq](x0: set_dlist[A]): List[A] = x0 match {
  case Abs_dlist(x) => x
}

def list_all[A](p: A => Boolean, x1: List[A]): Boolean = (p, x1) match {
  case (p, Nil) => true
  case (p, x :: xs) => p(x) && list_all[A](p, xs)
}

def dlist_all[A : ceq](x: A => Boolean, xc: set_dlist[A]): Boolean =
  list_all[A](x, list_of_dlist[A](xc))

def impl_ofa[B : ccompare, A](x0: mapping_rbt[B, A]): rbt[B, A] = x0 match {
  case Mapping_rbtb(x) => x
}

def rbt_init[A, B, C]: (rbt[A, B]) => (List[(C, rbt[A, B])], rbt[A, B]) =
  ((a: rbt[A, B]) => (Nil, a))

def init[A : ccompare, B, C](xa: mapping_rbt[A, B]):
      (List[(C, rbt[A, B])], rbt[A, B])
  =
  rbt_init[A, B, C].apply(impl_ofa[A, B](xa))

trait cenum[A] {
  val `GeneratedCode.cenum`:
        Option[(List[A],
                 ((A => Boolean) => Boolean, (A => Boolean) => Boolean))]
}
def cenum[A](implicit A: cenum[A]):
      Option[(List[A], ((A => Boolean) => Boolean, (A => Boolean) => Boolean))]
  = A.`GeneratedCode.cenum`
object cenum {
  implicit def `GeneratedCode.cenum_integer`: cenum[BigInt] = new cenum[BigInt]
    {
    val `GeneratedCode.cenum` = cenum_integera
  }
  implicit def `GeneratedCode.cenum_prod`[A : cenum, B : cenum]: cenum[(A, B)] =
    new cenum[(A, B)] {
    val `GeneratedCode.cenum` = cenum_proda[A, B]
  }
  implicit def `GeneratedCode.cenum_sum`[A : cenum, B : cenum]: cenum[sum[A, B]]
    = new cenum[sum[A, B]] {
    val `GeneratedCode.cenum` = cenum_suma[A, B]
  }
  implicit def `GeneratedCode.cenum_list`[A]: cenum[List[A]] = new
    cenum[List[A]] {
    val `GeneratedCode.cenum` = cenum_lista[A]
  }
  implicit def `GeneratedCode.cenum_fset`[A]: cenum[fset[A]] = new
    cenum[fset[A]] {
    val `GeneratedCode.cenum` = cenum_fseta[A]
  }
  implicit def `GeneratedCode.cenum_bool`: cenum[Boolean] = new cenum[Boolean] {
    val `GeneratedCode.cenum` = cenum_boola
  }
  implicit def
    `GeneratedCode.cenum_set`[A : cenum : ceq : ccompare : set_impl]:
      cenum[set[A]]
    = new cenum[set[A]] {
    val `GeneratedCode.cenum` = cenum_seta[A]
  }
  implicit def `GeneratedCode.cenum_nat`: cenum[nat] = new cenum[nat] {
    val `GeneratedCode.cenum` = cenum_nata
  }
}

def filtera[A](p: A => Boolean, x1: List[A]): List[A] = (p, x1) match {
  case (p, Nil) => Nil
  case (p, x :: xs) => (if (p(x)) x :: filtera[A](p, xs) else filtera[A](p, xs))
}

def Collect[A : cenum](p: A => Boolean): set[A] =
  (cenum[A] match {
     case None => Collect_set[A](p)
     case Some((enuma, _)) => Set_monad[A](filtera[A](p, enuma))
   })

def list_member[A](equal: A => A => Boolean, x1: List[A], y: A): Boolean =
  (equal, x1, y) match {
  case (equal, x :: xs, y) => (equal(x))(y) || list_member[A](equal, xs, y)
  case (equal, Nil, y) => false
}

def the[A](x0: Option[A]): A = x0 match {
  case Some(x2) => x2
}

def memberc[A : ceq](xa: set_dlist[A]): A => Boolean =
  ((a: A) =>
    list_member[A](the[A => A => Boolean](ceq[A]), list_of_dlist[A](xa), a))

trait equal[A] {
  val `GeneratedCode.equal`: (A, A) => Boolean
}
def equal[A](a: A, b: A)(implicit A: equal[A]): Boolean =
  A.`GeneratedCode.equal`(a, b)
object equal {
  implicit def `GeneratedCode.equal_integer`: equal[BigInt] = new equal[BigInt]
    {
    val `GeneratedCode.equal` = (a: BigInt, b: BigInt) => a == b
  }
  implicit def `GeneratedCode.equal_unit`: equal[Unit] = new equal[Unit] {
    val `GeneratedCode.equal` = (a: Unit, b: Unit) => equal_unita(a, b)
  }
  implicit def `GeneratedCode.equal_prod`[A : equal, B : equal]: equal[(A, B)] =
    new equal[(A, B)] {
    val `GeneratedCode.equal` = (a: (A, B), b: (A, B)) =>
      equal_proda[A, B](a, b)
  }
  implicit def `GeneratedCode.equal_sum`[A : equal, B : equal]: equal[sum[A, B]]
    = new equal[sum[A, B]] {
    val `GeneratedCode.equal` = (a: sum[A, B], b: sum[A, B]) =>
      equal_suma[A, B](a, b)
  }
  implicit def `GeneratedCode.equal_list`[A : equal]: equal[List[A]] = new
    equal[List[A]] {
    val `GeneratedCode.equal` = (a: List[A], b: List[A]) => equal_lista[A](a, b)
  }
  implicit def
    `GeneratedCode.equal_fset`[A : cenum : ceq : ccompare : equal]:
      equal[fset[A]]
    = new equal[fset[A]] {
    val `GeneratedCode.equal` = (a: fset[A], b: fset[A]) => equal_fseta[A](a, b)
  }
  implicit def `GeneratedCode.equal_bool`: equal[Boolean] = new equal[Boolean] {
    val `GeneratedCode.equal` = (a: Boolean, b: Boolean) => equal_boola(a, b)
  }
  implicit def
    `GeneratedCode.equal_set`[A : cenum : ceq : ccompare : equal]: equal[set[A]]
    = new equal[set[A]] {
    val `GeneratedCode.equal` = (a: set[A], b: set[A]) => equal_seta[A](a, b)
  }
  implicit def `GeneratedCode.equal_nat`: equal[nat] = new equal[nat] {
    val `GeneratedCode.equal` = (a: nat, b: nat) => equal_nata(a, b)
  }
}

def eq[A : equal](a: A, b: A): Boolean = equal[A](a, b)

def equal_option[A : equal](x0: Option[A], x1: Option[A]): Boolean = (x0, x1)
  match {
  case (None, Some(x2)) => false
  case (Some(x2), None) => false
  case (Some(x2), Some(y2)) => eq[A](x2, y2)
  case (None, None) => true
}

def rbt_comp_lookup[A, B](c: A => A => ordera, x1: rbt[A, B], k: A): Option[B] =
  (c, x1, k) match {
  case (c, Emptyd(), k) => None
  case (c, Branch(uu, l, x, y, r), k) =>
    ((c(k))(x) match {
       case Eqa() => Some[B](y)
       case Lt() => rbt_comp_lookup[A, B](c, l, k)
       case Gt() => rbt_comp_lookup[A, B](c, r, k)
     })
}

def lookupb[A : ccompare, B](xa: mapping_rbt[A, B]): A => Option[B] =
  ((a: A) =>
    rbt_comp_lookup[A, B](the[A => A => ordera](ccompare[A]),
                           impl_ofa[A, B](xa), a))

def equal_unita(u: Unit, v: Unit): Boolean = true

def memberb[A : ccompare](t: mapping_rbt[A, Unit], x: A): Boolean =
  equal_option[Unit]((lookupb[A, Unit](t)).apply(x), Some[Unit](()))

def member[A : ceq : ccompare](x: A, xa1: set[A]): Boolean = (x, xa1) match {
  case (x, Set_monad(xs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("member Set_Monad: ceq = None");
           (((_: Unit) => member[A](x, Set_monad[A](xs)))).apply(()) }
       case Some(eq) => list_member[A](eq, xs, x)
     })
  case (xa, Complement(x)) => ! (member[A](xa, x))
  case (x, Rbt_set(rbt)) => memberb[A](rbt, x)
  case (x, Dlist_set(dxs)) => (memberc[A](dxs)).apply(x)
  case (x, Collect_set(a)) => a(x)
}

def less_eq_set[A : cenum : ceq : ccompare](x0: set[A], c: set[A]): Boolean =
  (x0, c) match {
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("subset RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              less_eq_set[A](Rbt_set[A](rbt1), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(c) =>
         (ceq[A] match {
            case None =>
              sorted_list_subset_fusion[A,
 (List[(A, rbt[A, Unit])], rbt[A, Unit]),
 (List[(A, rbt[A, Unit])],
   rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                   ((x: A) => (y: A) => equal_order((c(x))(y), Eqa())),
                   rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
                   init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
            case Some(eq) =>
              sorted_list_subset_fusion[A,
 (List[(A, rbt[A, Unit])], rbt[A, Unit]),
 (List[(A, rbt[A, Unit])],
   rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)), eq,
                   rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
                   init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
          })
     })
  case (Complement(a1), Complement(a2)) => less_eq_set[A](a2, a1)
  case (Collect_set(p), Complement(a)) =>
    less_eq_set[A](a, Collect[A](((x: A) => ! (p(x)))))
  case (Set_monad(xs), c) => list_all[A](((x: A) => member[A](x, c)), xs)
  case (Dlist_set(dxs), c) =>
    (ceq[A] match {
       case None =>
         { sys.error("subset DList_set1: ceq = None");
           (((_: Unit) => less_eq_set[A](Dlist_set[A](dxs), c))).apply(()) }
       case Some(_) => dlist_all[A](((x: A) => member[A](x, c)), dxs)
     })
  case (Rbt_set(rbt), b) =>
    (ccompare[A] match {
       case None =>
         { sys.error("subset RBT_set1: ccompare = None");
           (((_: Unit) => less_eq_set[A](Rbt_set[A](rbt), b))).apply(()) }
       case Some(_) =>
         list_all_fusion[A, (List[(A, rbt[A, Unit])],
                              rbt[A, Unit])](rbt_keys_generator[A, Unit],
      ((x: A) => member[A](x, b)), init[A, Unit, A](rbt))
     })
}

def list_all2_fusion[A, B, C,
                      D](p: A => B => Boolean, g1: generator[A, C],
                          g2: generator[B, D], s1: C, s2: D):
      Boolean
  =
  (if ((has_next[A, C](g1)).apply(s1))
    (has_next[B, D](g2)).apply(s2) &&
      {
        val (x, s1a) = (next[A, C](g1)).apply(s1): ((A, C))
        val (y, s2a) = (next[B, D](g2)).apply(s2): ((B, D));
        (p(x))(y) && list_all2_fusion[A, B, C, D](p, g1, g2, s1a, s2a)
      }
    else ! (has_next[B, D](g2)).apply(s2))

def set_eq[A : cenum : ceq : ccompare](a: set[A], b: set[A]): Boolean = (a, b)
  match {
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("set_eq RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              set_eq[A](Rbt_set[A](rbt1), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(c) =>
         (ceq[A] match {
            case None =>
              list_all2_fusion[A, A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                                (List[(A, rbt[A, Unit])],
                                  rbt[A, Unit])](((x: A) => (y: A) =>
           equal_order((c(x))(y), Eqa())),
          rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
          init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
            case Some(eq) =>
              list_all2_fusion[A, A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                                (List[(A, rbt[A, Unit])],
                                  rbt[A, Unit])](eq,
          rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
          init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
          })
     })
  case (Complement(a), Complement(b)) => set_eq[A](a, b)
  case (a, b) => less_eq_set[A](a, b) && less_eq_set[A](b, a)
}

abstract sealed class alist[B, A]
final case class Alista[B, A](a: List[(B, A)]) extends alist[B, A]

abstract sealed class mapping[A, B]
final case class Assoc_list_mapping[A, B](a: alist[A, B]) extends mapping[A, B]
final case class Rbt_mapping[A, B](a: mapping_rbt[A, B]) extends mapping[A, B]
final case class Mappinga[A, B](a: A => Option[B]) extends mapping[A, B]

abstract sealed class fsm_with_precomputations_impl[A, B, C]
final case class FSMWPI[A, B,
                         C](a: A, b: set[A], c: set[B], d: set[C],
                             e: set[(A, (B, (C, A)))],
                             f: mapping[(A, B), set[(C, A)]],
                             g: mapping[(A, B), mapping[C, A]])
  extends fsm_with_precomputations_impl[A, B, C]

abstract sealed class fsm_with_precomputations[C, B, A]
final case class Fsm_with_precomputationsa[C, B,
    A](a: fsm_with_precomputations_impl[C, B, A])
  extends fsm_with_precomputations[C, B, A]

def fsm_with_precomputations_impl_of_fsm_with_precomputations[C, B,
                       A](x0: fsm_with_precomputations[C, B, A]):
      fsm_with_precomputations_impl[C, B, A]
  =
  x0 match {
  case Fsm_with_precomputationsa(x) => x
}

def transitions_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]):
      set[(A, (B, (C, A)))]
  =
  x0 match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x5
}

def transitions_wp[A, B, C](x: fsm_with_precomputations[A, B, C]):
      set[(A, (B, (C, A)))]
  =
  transitions_wpi[A, B,
                   C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
 B, C](x))

abstract sealed class fsm_impl[A, B, C]
final case class FSMWP[A, B, C](a: fsm_with_precomputations[A, B, C]) extends
  fsm_impl[A, B, C]

def transitionsa[A, B, C](x0: fsm_impl[A, B, C]): set[(A, (B, (C, A)))] = x0
  match {
  case FSMWP(m) => transitions_wp[A, B, C](m)
}

abstract sealed class fsm[C, B, A]
final case class Abs_fsm[C, B, A](a: fsm_impl[C, B, A]) extends fsm[C, B, A]

def fsm_impl_of_fsm[C, B, A](x0: fsm[C, B, A]): fsm_impl[C, B, A] = x0 match {
  case Abs_fsm(x) => x
}

def transitions[A, B, C](x: fsm[A, B, C]): set[(A, (B, (C, A)))] =
  transitionsa[A, B, C](fsm_impl_of_fsm[A, B, C](x))

def outputs_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]): set[C] =
  x0 match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x4
}

def outputs_wp[A, B, C](x: fsm_with_precomputations[A, B, C]): set[C] =
  outputs_wpi[A, B,
               C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
                                     B, C](x))

def outputsa[A, B, C](x0: fsm_impl[A, B, C]): set[C] = x0 match {
  case FSMWP(m) => outputs_wp[A, B, C](m)
}

def outputs[A, B, C](x: fsm[A, B, C]): set[C] =
  outputsa[A, B, C](fsm_impl_of_fsm[A, B, C](x))

def initial_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]): A = x0
  match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x1
}

def initial_wp[A, B, C](x: fsm_with_precomputations[A, B, C]): A =
  initial_wpi[A, B,
               C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
                                     B, C](x))

def initiala[A, B, C](x0: fsm_impl[A, B, C]): A = x0 match {
  case FSMWP(m) => initial_wp[A, B, C](m)
}

def initial[A, B, C](x: fsm[A, B, C]): A =
  initiala[A, B, C](fsm_impl_of_fsm[A, B, C](x))

def states_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]): set[A] = x0
  match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x2
}

def states_wp[A, B, C](x: fsm_with_precomputations[A, B, C]): set[A] =
  states_wpi[A, B,
              C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                                    C](x))

def statesa[A, B, C](x0: fsm_impl[A, B, C]): set[A] = x0 match {
  case FSMWP(m) => states_wp[A, B, C](m)
}

def states[A, B, C](x: fsm[A, B, C]): set[A] =
  statesa[A, B, C](fsm_impl_of_fsm[A, B, C](x))

def inputs_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]): set[B] = x0
  match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x3
}

def inputs_wp[A, B, C](x: fsm_with_precomputations[A, B, C]): set[B] =
  inputs_wpi[A, B,
              C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                                    C](x))

def inputsa[A, B, C](x0: fsm_impl[A, B, C]): set[B] = x0 match {
  case FSMWP(m) => inputs_wp[A, B, C](m)
}

def inputs[A, B, C](x: fsm[A, B, C]): set[B] =
  inputsa[A, B, C](fsm_impl_of_fsm[A, B, C](x))

def comparator_prod[A, B](comp_a: A => A => ordera, comp_b: B => B => ordera,
                           x2: (A, B), x3: (A, B)):
      ordera
  =
  (comp_a, comp_b, x2, x3) match {
  case (comp_a, comp_b, (x, xa), (y, ya)) => ((comp_a(x))(y) match {
        case Eqa() => (comp_b(xa))(ya)
        case Lt() => Lt()
        case Gt() => Gt()
      })
}

def ccompare_proda[A : ccompare, B : ccompare]:
      Option[((A, B)) => ((A, B)) => ordera]
  =
  (ccompare[A] match {
     case None => None
     case Some(comp_a) =>
       (ccompare[B] match {
          case None => None
          case Some(comp_b) =>
            Some[((A, B)) =>
                   ((A, B)) =>
                     ordera](((a: (A, B)) => (b: (A, B)) =>
                               comparator_prod[A, B](comp_a, comp_b, a, b)))
        })
   })

def mapa[A, B](f: A => B, x1: List[A]): List[B] = (f, x1) match {
  case (f, Nil) => Nil
  case (f, x21 :: x22) => f(x21) :: mapa[A, B](f, x22)
}

def product[A, B](x0: List[A], uu: List[B]): List[(A, B)] = (x0, uu) match {
  case (Nil, uu) => Nil
  case (x :: xs, ys) =>
    mapa[B, (A, B)](((a: B) => (x, a)), ys) ++ product[A, B](xs, ys)
}

def cenum_proda[A : cenum, B : cenum]:
      Option[(List[(A, B)],
               ((((A, B)) => Boolean) => Boolean,
                 (((A, B)) => Boolean) => Boolean))]
  =
  (cenum[A] match {
     case None => None
     case Some((enum_a, (enum_all_a, enum_ex_a))) =>
       (cenum[B] match {
          case None => None
          case Some((enum_b, (enum_all_b, enum_ex_b))) =>
            Some[(List[(A, B)],
                   ((((A, B)) => Boolean) => Boolean,
                     (((A, B)) => Boolean) =>
                       Boolean))]((product[A, B](enum_a, enum_b),
                                    (((p: ((A, B)) => Boolean) =>
                                       enum_all_a(((x: A) =>
            enum_all_b(((y: B) => p((x, y))))))),
                                      ((p: ((A, B)) => Boolean) =>
enum_ex_a(((x: A) => enum_ex_b(((y: B) => p((x, y))))))))))
        })
   })

def equality_prod[A, B](eq_a: A => A => Boolean, eq_b: B => B => Boolean,
                         x2: (A, B), x3: (A, B)):
      Boolean
  =
  (eq_a, eq_b, x2, x3) match {
  case (eq_a, eq_b, (x, xa), (y, ya)) => (eq_a(x))(y) && (eq_b(xa))(ya)
}

def ceq_proda[A : ceq, B : ceq]: Option[((A, B)) => ((A, B)) => Boolean] =
  (ceq[A] match {
     case None => None
     case Some(eq_a) =>
       (ceq[B] match {
          case None => None
          case Some(eq_b) =>
            Some[((A, B)) =>
                   ((A, B)) =>
                     Boolean](((a: (A, B)) => (b: (A, B)) =>
                                equality_prod[A, B](eq_a, eq_b, a, b)))
        })
   })

def equal_fsm[A : cenum : ceq : ccompare : equal, B : cenum : ceq : ccompare,
               C : cenum : ceq : ccompare](x: fsm[A, B, C], y: fsm[A, B, C]):
      Boolean
  =
  eq[A](initial[A, B, C](x), initial[A, B, C](y)) &&
    (set_eq[A](states[A, B, C](x), states[A, B, C](y)) &&
      (set_eq[B](inputs[A, B, C](x), inputs[A, B, C](y)) &&
        (set_eq[C](outputs[A, B, C](x), outputs[A, B, C](y)) &&
          set_eq[(A, (B, (C, A)))](transitions[A, B, C](x),
                                    transitions[A, B, C](y)))))

def ceq_fsma[A : cenum : ceq : ccompare : equal, B : cenum : ceq : ccompare,
              C : cenum : ceq : ccompare]:
      Option[(fsm[A, B, C]) => (fsm[A, B, C]) => Boolean]
  =
  Some[(fsm[A, B, C]) =>
         (fsm[A, B, C]) =>
           Boolean](((a: fsm[A, B, C]) => (b: fsm[A, B, C]) =>
                      equal_fsm[A, B, C](a, b)))

abstract sealed class phantom[A, B]
final case class phantoma[B, A](a: B) extends phantom[A, B]

abstract sealed class set_impla
final case class set_choose() extends set_impla
final case class set_collect() extends set_impla
final case class set_dlista() extends set_impla
final case class set_rbt() extends set_impla
final case class set_monada() extends set_impla

def set_impl_fsma[A, B, C]: phantom[fsm[A, B, C], set_impla] =
  phantoma[set_impla, fsm[A, B, C]](set_dlista())

trait set_impl[A] {
  val `GeneratedCode.set_impl`: phantom[A, set_impla]
}
def set_impl[A](implicit A: set_impl[A]): phantom[A, set_impla] =
  A.`GeneratedCode.set_impl`
object set_impl {
  implicit def `GeneratedCode.set_impl_integer`: set_impl[BigInt] = new
    set_impl[BigInt] {
    val `GeneratedCode.set_impl` = set_impl_integera
  }
  implicit def
    `GeneratedCode.set_impl_prod`[A : set_impl, B : set_impl]: set_impl[(A, B)]
    = new set_impl[(A, B)] {
    val `GeneratedCode.set_impl` = set_impl_proda[A, B]
  }
  implicit def
    `GeneratedCode.set_impl_option`[A : set_impl]: set_impl[Option[A]] = new
    set_impl[Option[A]] {
    val `GeneratedCode.set_impl` = set_impl_optiona[A]
  }
  implicit def
    `GeneratedCode.set_impl_sum`[A : set_impl, B : set_impl]:
      set_impl[sum[A, B]]
    = new set_impl[sum[A, B]] {
    val `GeneratedCode.set_impl` = set_impl_suma[A, B]
  }
  implicit def `GeneratedCode.set_impl_list`[A]: set_impl[List[A]] = new
    set_impl[List[A]] {
    val `GeneratedCode.set_impl` = set_impl_lista[A]
  }
  implicit def `GeneratedCode.set_impl_fset`[A]: set_impl[fset[A]] = new
    set_impl[fset[A]] {
    val `GeneratedCode.set_impl` = set_impl_fseta[A]
  }
  implicit def `GeneratedCode.set_impl_bool`: set_impl[Boolean] = new
    set_impl[Boolean] {
    val `GeneratedCode.set_impl` = set_impl_boola
  }
  implicit def `GeneratedCode.set_impl_set`[A]: set_impl[set[A]] = new
    set_impl[set[A]] {
    val `GeneratedCode.set_impl` = set_impl_seta[A]
  }
  implicit def `GeneratedCode.set_impl_nat`: set_impl[nat] = new set_impl[nat] {
    val `GeneratedCode.set_impl` = set_impl_nata
  }
  implicit def `GeneratedCode.set_impl_fsm`[A, B, C]: set_impl[fsm[A, B, C]] =
    new set_impl[fsm[A, B, C]] {
    val `GeneratedCode.set_impl` = set_impl_fsma[A, B, C]
  }
}

def ccompare_fsma[A, B, C]: Option[(fsm[A, B, C]) => (fsm[A, B, C]) => ordera] =
  None

abstract sealed class nat
final case class Nata(a: BigInt) extends nat

def integer_of_nat(x0: nat): BigInt = x0 match {
  case Nata(x) => x
}

def equal_nata(m: nat, n: nat): Boolean = integer_of_nat(m) == integer_of_nat(n)

abstract sealed class num
final case class Onea() extends num
final case class Bit0(a: num) extends num
final case class Bit1(a: num) extends num

def one_nata: nat = Nata(BigInt(1))

trait one[A] {
  val `GeneratedCode.one`: A
}
def one[A](implicit A: one[A]): A = A.`GeneratedCode.one`
object one {
  implicit def `GeneratedCode.one_nat`: one[nat] = new one[nat] {
    val `GeneratedCode.one` = one_nata
  }
}

def times_nata(m: nat, n: nat): nat =
  Nata(integer_of_nat(m) * integer_of_nat(n))

trait times[A] {
  val `GeneratedCode.times`: (A, A) => A
}
def times[A](a: A, b: A)(implicit A: times[A]): A =
  A.`GeneratedCode.times`(a, b)
object times {
  implicit def `GeneratedCode.times_nat`: times[nat] = new times[nat] {
    val `GeneratedCode.times` = (a: nat, b: nat) => times_nata(a, b)
  }
}

trait power[A] extends one[A] with times[A] {
}
object power {
  implicit def `GeneratedCode.power_nat`: power[nat] = new power[nat] {
    val `GeneratedCode.times` = (a: nat, b: nat) => times_nata(a, b)
    val `GeneratedCode.one` = one_nata
  }
}

def less_eq_nat(m: nat, n: nat): Boolean =
  integer_of_nat(m) <= integer_of_nat(n)

trait ord[A] {
  val `GeneratedCode.less_eq`: (A, A) => Boolean
  val `GeneratedCode.less`: (A, A) => Boolean
}
def less_eq[A](a: A, b: A)(implicit A: ord[A]): Boolean =
  A.`GeneratedCode.less_eq`(a, b)
def less[A](a: A, b: A)(implicit A: ord[A]): Boolean =
  A.`GeneratedCode.less`(a, b)
object ord {
  implicit def `GeneratedCode.ord_integer`: ord[BigInt] = new ord[BigInt] {
    val `GeneratedCode.less_eq` = (a: BigInt, b: BigInt) => a <= b
    val `GeneratedCode.less` = (a: BigInt, b: BigInt) => a < b
  }
  implicit def `GeneratedCode.ord_prod`[A : ord, B : ord]: ord[(A, B)] = new
    ord[(A, B)] {
    val `GeneratedCode.less_eq` = (a: (A, B), b: (A, B)) =>
      less_eq_prod[A, B](a, b)
    val `GeneratedCode.less` = (a: (A, B), b: (A, B)) => less_prod[A, B](a, b)
  }
  implicit def
    `GeneratedCode.ord_sum`[A : equal : ord, B : equal : ord]: ord[sum[A, B]] =
    new ord[sum[A, B]] {
    val `GeneratedCode.less_eq` = (a: sum[A, B], b: sum[A, B]) =>
      less_eq_sum[A, B](a, b)
    val `GeneratedCode.less` = (a: sum[A, B], b: sum[A, B]) =>
      less_sum[A, B](a, b)
  }
  implicit def `GeneratedCode.ord_list`[A : equal : order]: ord[List[A]] = new
    ord[List[A]] {
    val `GeneratedCode.less_eq` = (a: List[A], b: List[A]) =>
      less_eq_list[A](a, b)
    val `GeneratedCode.less` = (a: List[A], b: List[A]) => less_list[A](a, b)
  }
  implicit def `GeneratedCode.ord_bool`: ord[Boolean] = new ord[Boolean] {
    val `GeneratedCode.less_eq` = (a: Boolean, b: Boolean) => less_eq_bool(a, b)
    val `GeneratedCode.less` = (a: Boolean, b: Boolean) => less_bool(a, b)
  }
  implicit def `GeneratedCode.ord_set`[A : cenum : ceq : ccompare]: ord[set[A]]
    = new ord[set[A]] {
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
  }
  implicit def `GeneratedCode.ord_nat`: ord[nat] = new ord[nat] {
    val `GeneratedCode.less_eq` = (a: nat, b: nat) => less_eq_nat(a, b)
    val `GeneratedCode.less` = (a: nat, b: nat) => less_nat(a, b)
  }
}

def less_nat(m: nat, n: nat): Boolean = integer_of_nat(m) < integer_of_nat(n)

trait preorder[A] extends ord[A] {
}
object preorder {
  implicit def `GeneratedCode.preorder_integer`: preorder[BigInt] = new
    preorder[BigInt] {
    val `GeneratedCode.less_eq` = (a: BigInt, b: BigInt) => a <= b
    val `GeneratedCode.less` = (a: BigInt, b: BigInt) => a < b
  }
  implicit def
    `GeneratedCode.preorder_prod`[A : preorder, B : preorder]: preorder[(A, B)]
    = new preorder[(A, B)] {
    val `GeneratedCode.less_eq` = (a: (A, B), b: (A, B)) =>
      less_eq_prod[A, B](a, b)
    val `GeneratedCode.less` = (a: (A, B), b: (A, B)) => less_prod[A, B](a, b)
  }
  implicit def
    `GeneratedCode.preorder_sum`[A : equal : linorder, B : equal : linorder]:
      preorder[sum[A, B]]
    = new preorder[sum[A, B]] {
    val `GeneratedCode.less_eq` = (a: sum[A, B], b: sum[A, B]) =>
      less_eq_sum[A, B](a, b)
    val `GeneratedCode.less` = (a: sum[A, B], b: sum[A, B]) =>
      less_sum[A, B](a, b)
  }
  implicit def
    `GeneratedCode.preorder_list`[A : equal : order]: preorder[List[A]] = new
    preorder[List[A]] {
    val `GeneratedCode.less_eq` = (a: List[A], b: List[A]) =>
      less_eq_list[A](a, b)
    val `GeneratedCode.less` = (a: List[A], b: List[A]) => less_list[A](a, b)
  }
  implicit def `GeneratedCode.preorder_bool`: preorder[Boolean] = new
    preorder[Boolean] {
    val `GeneratedCode.less_eq` = (a: Boolean, b: Boolean) => less_eq_bool(a, b)
    val `GeneratedCode.less` = (a: Boolean, b: Boolean) => less_bool(a, b)
  }
  implicit def
    `GeneratedCode.preorder_set`[A : cenum : ceq : ccompare]: preorder[set[A]] =
    new preorder[set[A]] {
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
  }
  implicit def `GeneratedCode.preorder_nat`: preorder[nat] = new preorder[nat] {
    val `GeneratedCode.less_eq` = (a: nat, b: nat) => less_eq_nat(a, b)
    val `GeneratedCode.less` = (a: nat, b: nat) => less_nat(a, b)
  }
}

trait order[A] extends preorder[A] {
}
object order {
  implicit def `GeneratedCode.order_integer`: order[BigInt] = new order[BigInt]
    {
    val `GeneratedCode.less_eq` = (a: BigInt, b: BigInt) => a <= b
    val `GeneratedCode.less` = (a: BigInt, b: BigInt) => a < b
  }
  implicit def `GeneratedCode.order_prod`[A : order, B : order]: order[(A, B)] =
    new order[(A, B)] {
    val `GeneratedCode.less_eq` = (a: (A, B), b: (A, B)) =>
      less_eq_prod[A, B](a, b)
    val `GeneratedCode.less` = (a: (A, B), b: (A, B)) => less_prod[A, B](a, b)
  }
  implicit def
    `GeneratedCode.order_sum`[A : equal : linorder, B : equal : linorder]:
      order[sum[A, B]]
    = new order[sum[A, B]] {
    val `GeneratedCode.less_eq` = (a: sum[A, B], b: sum[A, B]) =>
      less_eq_sum[A, B](a, b)
    val `GeneratedCode.less` = (a: sum[A, B], b: sum[A, B]) =>
      less_sum[A, B](a, b)
  }
  implicit def `GeneratedCode.order_list`[A : equal : order]: order[List[A]] =
    new order[List[A]] {
    val `GeneratedCode.less_eq` = (a: List[A], b: List[A]) =>
      less_eq_list[A](a, b)
    val `GeneratedCode.less` = (a: List[A], b: List[A]) => less_list[A](a, b)
  }
  implicit def `GeneratedCode.order_bool`: order[Boolean] = new order[Boolean] {
    val `GeneratedCode.less_eq` = (a: Boolean, b: Boolean) => less_eq_bool(a, b)
    val `GeneratedCode.less` = (a: Boolean, b: Boolean) => less_bool(a, b)
  }
  implicit def
    `GeneratedCode.order_set`[A : cenum : ceq : ccompare]: order[set[A]] = new
    order[set[A]] {
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
  }
  implicit def `GeneratedCode.order_nat`: order[nat] = new order[nat] {
    val `GeneratedCode.less_eq` = (a: nat, b: nat) => less_eq_nat(a, b)
    val `GeneratedCode.less` = (a: nat, b: nat) => less_nat(a, b)
  }
}

def ceq_nata: Option[nat => nat => Boolean] =
  Some[nat => nat => Boolean](((a: nat) => (b: nat) => equal_nata(a, b)))

def set_impl_nata: phantom[nat, set_impla] = phantoma[set_impla, nat](set_rbt())

trait linorder[A] extends order[A] {
}
object linorder {
  implicit def `GeneratedCode.linorder_integer`: linorder[BigInt] = new
    linorder[BigInt] {
    val `GeneratedCode.less_eq` = (a: BigInt, b: BigInt) => a <= b
    val `GeneratedCode.less` = (a: BigInt, b: BigInt) => a < b
  }
  implicit def
    `GeneratedCode.linorder_prod`[A : linorder, B : linorder]: linorder[(A, B)]
    = new linorder[(A, B)] {
    val `GeneratedCode.less_eq` = (a: (A, B), b: (A, B)) =>
      less_eq_prod[A, B](a, b)
    val `GeneratedCode.less` = (a: (A, B), b: (A, B)) => less_prod[A, B](a, b)
  }
  implicit def
    `GeneratedCode.linorder_sum`[A : equal : linorder, B : equal : linorder]:
      linorder[sum[A, B]]
    = new linorder[sum[A, B]] {
    val `GeneratedCode.less_eq` = (a: sum[A, B], b: sum[A, B]) =>
      less_eq_sum[A, B](a, b)
    val `GeneratedCode.less` = (a: sum[A, B], b: sum[A, B]) =>
      less_sum[A, B](a, b)
  }
  implicit def
    `GeneratedCode.linorder_list`[A : equal : linorder]: linorder[List[A]] = new
    linorder[List[A]] {
    val `GeneratedCode.less_eq` = (a: List[A], b: List[A]) =>
      less_eq_list[A](a, b)
    val `GeneratedCode.less` = (a: List[A], b: List[A]) => less_list[A](a, b)
  }
  implicit def `GeneratedCode.linorder_bool`: linorder[Boolean] = new
    linorder[Boolean] {
    val `GeneratedCode.less_eq` = (a: Boolean, b: Boolean) => less_eq_bool(a, b)
    val `GeneratedCode.less` = (a: Boolean, b: Boolean) => less_bool(a, b)
  }
  implicit def `GeneratedCode.linorder_nat`: linorder[nat] = new linorder[nat] {
    val `GeneratedCode.less_eq` = (a: nat, b: nat) => less_eq_nat(a, b)
    val `GeneratedCode.less` = (a: nat, b: nat) => less_nat(a, b)
  }
}

def finite_univ_nata: phantom[nat, Boolean] = phantoma[Boolean, nat](false)

def zero_nat: nat = Nata(BigInt(0))

def card_univ_nata: phantom[nat, nat] = phantoma[nat, nat](zero_nat)

trait finite_univ[A] {
  val `GeneratedCode.finite_univ`: phantom[A, Boolean]
}
def finite_univ[A](implicit A: finite_univ[A]): phantom[A, Boolean] =
  A.`GeneratedCode.finite_univ`
object finite_univ {
  implicit def `GeneratedCode.finite_univ_integer`: finite_univ[BigInt] = new
    finite_univ[BigInt] {
    val `GeneratedCode.finite_univ` = finite_univ_integera
  }
  implicit def
    `GeneratedCode.finite_univ_prod`[A : finite_univ, B : finite_univ]:
      finite_univ[(A, B)]
    = new finite_univ[(A, B)] {
    val `GeneratedCode.finite_univ` = finite_univ_proda[A, B]
  }
  implicit def
    `GeneratedCode.finite_univ_sum`[A : finite_univ, B : finite_univ]:
      finite_univ[sum[A, B]]
    = new finite_univ[sum[A, B]] {
    val `GeneratedCode.finite_univ` = finite_univ_suma[A, B]
  }
  implicit def `GeneratedCode.finite_univ_list`[A]: finite_univ[List[A]] = new
    finite_univ[List[A]] {
    val `GeneratedCode.finite_univ` = finite_univ_lista[A]
  }
  implicit def
    `GeneratedCode.finite_univ_fset`[A : finite_univ]: finite_univ[fset[A]] =
    new finite_univ[fset[A]] {
    val `GeneratedCode.finite_univ` = finite_univ_fseta[A]
  }
  implicit def `GeneratedCode.finite_univ_bool`: finite_univ[Boolean] = new
    finite_univ[Boolean] {
    val `GeneratedCode.finite_univ` = finite_univ_boola
  }
  implicit def
    `GeneratedCode.finite_univ_set`[A : finite_univ]: finite_univ[set[A]] = new
    finite_univ[set[A]] {
    val `GeneratedCode.finite_univ` = finite_univ_seta[A]
  }
  implicit def `GeneratedCode.finite_univ_nat`: finite_univ[nat] = new
    finite_univ[nat] {
    val `GeneratedCode.finite_univ` = finite_univ_nata
  }
}

trait card_univ[A] extends finite_univ[A] {
  val `GeneratedCode.card_univ`: phantom[A, nat]
}
def card_univ[A](implicit A: card_univ[A]): phantom[A, nat] =
  A.`GeneratedCode.card_univ`
object card_univ {
  implicit def `GeneratedCode.card_univ_integer`: card_univ[BigInt] = new
    card_univ[BigInt] {
    val `GeneratedCode.card_univ` = card_univ_integera
    val `GeneratedCode.finite_univ` = finite_univ_integera
  }
  implicit def
    `GeneratedCode.card_univ_prod`[A : card_univ, B : card_univ]:
      card_univ[(A, B)]
    = new card_univ[(A, B)] {
    val `GeneratedCode.card_univ` = card_univ_proda[A, B]
    val `GeneratedCode.finite_univ` = finite_univ_proda[A, B]
  }
  implicit def
    `GeneratedCode.card_univ_sum`[A : card_univ, B : card_univ]:
      card_univ[sum[A, B]]
    = new card_univ[sum[A, B]] {
    val `GeneratedCode.card_univ` = card_univ_suma[A, B]
    val `GeneratedCode.finite_univ` = finite_univ_suma[A, B]
  }
  implicit def `GeneratedCode.card_univ_list`[A]: card_univ[List[A]] = new
    card_univ[List[A]] {
    val `GeneratedCode.card_univ` = card_univ_lista[A]
    val `GeneratedCode.finite_univ` = finite_univ_lista[A]
  }
  implicit def `GeneratedCode.card_univ_fset`[A : card_univ]: card_univ[fset[A]]
    = new card_univ[fset[A]] {
    val `GeneratedCode.card_univ` = card_univ_fseta[A]
    val `GeneratedCode.finite_univ` = finite_univ_fseta[A]
  }
  implicit def `GeneratedCode.card_univ_bool`: card_univ[Boolean] = new
    card_univ[Boolean] {
    val `GeneratedCode.card_univ` = card_univ_boola
    val `GeneratedCode.finite_univ` = finite_univ_boola
  }
  implicit def `GeneratedCode.card_univ_set`[A : card_univ]: card_univ[set[A]] =
    new card_univ[set[A]] {
    val `GeneratedCode.card_univ` = card_univ_seta[A]
    val `GeneratedCode.finite_univ` = finite_univ_seta[A]
  }
  implicit def `GeneratedCode.card_univ_nat`: card_univ[nat] = new
    card_univ[nat] {
    val `GeneratedCode.card_univ` = card_univ_nata
    val `GeneratedCode.finite_univ` = finite_univ_nata
  }
}

def cenum_nata:
      Option[(List[nat],
               ((nat => Boolean) => Boolean, (nat => Boolean) => Boolean))]
  =
  None

def comparator_of[A : equal : linorder](x: A, y: A): ordera =
  (if (less[A](x, y)) Lt() else (if (eq[A](x, y)) Eqa() else Gt()))

def compare_nat: nat => nat => ordera =
  ((a: nat) => (b: nat) => comparator_of[nat](a, b))

def ccompare_nata: Option[nat => nat => ordera] =
  Some[nat => nat => ordera](compare_nat)

abstract sealed class mapping_impla
final case class mapping_choose() extends mapping_impla
final case class mapping_assoc_list() extends mapping_impla
final case class mapping_rbta() extends mapping_impla
final case class mapping_mapping() extends mapping_impla

def mapping_impl_nata: phantom[nat, mapping_impla] =
  phantoma[mapping_impla, nat](mapping_rbta())

trait mapping_impl[A] {
  val `GeneratedCode.mapping_impl`: phantom[A, mapping_impla]
}
def mapping_impl[A](implicit A: mapping_impl[A]): phantom[A, mapping_impla] =
  A.`GeneratedCode.mapping_impl`
object mapping_impl {
  implicit def `GeneratedCode.mapping_impl_integer`: mapping_impl[BigInt] = new
    mapping_impl[BigInt] {
    val `GeneratedCode.mapping_impl` = mapping_impl_integera
  }
  implicit def
    `GeneratedCode.mapping_impl_prod`[A : mapping_impl, B : mapping_impl]:
      mapping_impl[(A, B)]
    = new mapping_impl[(A, B)] {
    val `GeneratedCode.mapping_impl` = mapping_impl_proda[A, B]
  }
  implicit def
    `GeneratedCode.mapping_impl_sum`[A : mapping_impl, B : mapping_impl]:
      mapping_impl[sum[A, B]]
    = new mapping_impl[sum[A, B]] {
    val `GeneratedCode.mapping_impl` = mapping_impl_suma[A, B]
  }
  implicit def `GeneratedCode.mapping_impl_list`[A]: mapping_impl[List[A]] = new
    mapping_impl[List[A]] {
    val `GeneratedCode.mapping_impl` = mapping_impl_lista[A]
  }
  implicit def `GeneratedCode.mapping_impl_fset`[A]: mapping_impl[fset[A]] = new
    mapping_impl[fset[A]] {
    val `GeneratedCode.mapping_impl` = mapping_impl_fseta[A]
  }
  implicit def `GeneratedCode.mapping_impl_bool`: mapping_impl[Boolean] = new
    mapping_impl[Boolean] {
    val `GeneratedCode.mapping_impl` = mapping_impl_boola
  }
  implicit def `GeneratedCode.mapping_impl_set`[A]: mapping_impl[set[A]] = new
    mapping_impl[set[A]] {
    val `GeneratedCode.mapping_impl` = mapping_impl_seta[A]
  }
  implicit def `GeneratedCode.mapping_impl_nat`: mapping_impl[nat] = new
    mapping_impl[nat] {
    val `GeneratedCode.mapping_impl` = mapping_impl_nata
  }
}

def max[A : ord](a: A, b: A): A = (if (less_eq[A](a, b)) b else a)

def minus_nat(m: nat, n: nat): nat =
  Nata(max[BigInt](BigInt(0), integer_of_nat(m) - integer_of_nat(n)))

def proper_interval_nat(no: Option[nat], x1: Option[nat]): Boolean = (no, x1)
  match {
  case (no, None) => true
  case (None, Some(x)) => less_nat(zero_nat, x)
  case (Some(x), Some(y)) => less_nat(one_nata, minus_nat(y, x))
}

def cproper_interval_nata: Option[nat] => Option[nat] => Boolean =
  ((a: Option[nat]) => (b: Option[nat]) => proper_interval_nat(a, b))

trait cproper_interval[A] extends ccompare[A] {
  val `GeneratedCode.cproper_interval`: (Option[A], Option[A]) => Boolean
}
def cproper_interval[A](a: Option[A],
                         b: Option[A])(implicit A: cproper_interval[A]):
      Boolean
  = A.`GeneratedCode.cproper_interval`(a, b)
object cproper_interval {
  implicit def
    `GeneratedCode.cproper_interval_integer`: cproper_interval[BigInt] = new
    cproper_interval[BigInt] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[BigInt], b: Option[BigInt]) =>
      cproper_interval_integera.apply(a).apply(b)
    val `GeneratedCode.ccompare` = ccompare_integera
  }
  implicit def
    `GeneratedCode.cproper_interval_prod`[A : cproper_interval,
   B : cproper_interval]:
      cproper_interval[(A, B)]
    = new cproper_interval[(A, B)] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[(A, B)], b: Option[(A, B)]) =>
      cproper_interval_proda[A, B](a, b)
    val `GeneratedCode.ccompare` = ccompare_proda[A, B]
  }
  implicit def
    `GeneratedCode.cproper_interval_sum`[A : cproper_interval,
  B : cproper_interval]:
      cproper_interval[sum[A, B]]
    = new cproper_interval[sum[A, B]] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[sum[A, B]], b: Option[sum[A, B]]) =>
      cproper_interval_suma[A, B](a, b)
    val `GeneratedCode.ccompare` = ccompare_suma[A, B]
  }
  implicit def
    `GeneratedCode.cproper_interval_list`[A : ccompare]:
      cproper_interval[List[A]]
    = new cproper_interval[List[A]] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[List[A]], b: Option[List[A]]) =>
      cproper_interval_lista[A](a, b)
    val `GeneratedCode.ccompare` = ccompare_lista[A]
  }
  implicit def
    `GeneratedCode.cproper_interval_fset`[A : infinite_univ]:
      cproper_interval[fset[A]]
    = new cproper_interval[fset[A]] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[fset[A]], b: Option[fset[A]]) =>
      cproper_interval_fseta[A](a, b)
    val `GeneratedCode.ccompare` = ccompare_fseta[A]
  }
  implicit def `GeneratedCode.cproper_interval_bool`: cproper_interval[Boolean]
    = new cproper_interval[Boolean] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[Boolean], b: Option[Boolean]) =>
      cproper_interval_boola.apply(a).apply(b)
    val `GeneratedCode.ccompare` = ccompare_boola
  }
  implicit def
    `GeneratedCode.cproper_interval_set`[A : card_univ : ceq : cproper_interval : set_impl]:
      cproper_interval[set[A]]
    = new cproper_interval[set[A]] {
    val `GeneratedCode.cproper_interval` =
      (a: Option[set[A]], b: Option[set[A]]) => cproper_interval_seta[A](a, b)
    val `GeneratedCode.ccompare` = ccompare_seta[A]
  }
  implicit def `GeneratedCode.cproper_interval_nat`: cproper_interval[nat] = new
    cproper_interval[nat] {
    val `GeneratedCode.cproper_interval` = (a: Option[nat], b: Option[nat]) =>
      cproper_interval_nata.apply(a).apply(b)
    val `GeneratedCode.ccompare` = ccompare_nata
  }
}

def equal_seta[A : cenum : ceq : ccompare : equal](a: set[A], b: set[A]):
      Boolean
  =
  less_eq_set[A](a, b) && less_eq_set[A](b, a)

def uminus_set[A](a: set[A]): set[A] = a match {
  case Complement(b) => b
  case Collect_set(p) => Collect_set[A](((x: A) => ! (p(x))))
  case a => Complement[A](a)
}

def balance[A, B](x0: rbt[A, B], s: A, t: B, x3: rbt[A, B]): rbt[A, B] =
  (x0, s, t, x3) match {
  case (Branch(R(), a, w, x, b), s, t, Branch(R(), c, y, z, d)) =>
    Branch[A, B](R(), Branch[A, B](B(), a, w, x, b), s, t,
                  Branch[A, B](B(), c, y, z, d))
  case (Branch(R(), Branch(R(), a, w, x, b), s, t, c), y, z, Emptyd()) =>
    Branch[A, B](R(), Branch[A, B](B(), a, w, x, b), s, t,
                  Branch[A, B](B(), c, y, z, Emptyd[A, B]()))
  case (Branch(R(), Branch(R(), a, w, x, b), s, t, c), y, z,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](R(), Branch[A, B](B(), a, w, x, b), s, t,
                     Branch[A, B](B(), c, y, z,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (Branch(R(), Emptyd(), w, x, Branch(R(), b, s, t, c)), y, z, Emptyd()) =>
    Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), w, x, b), s, t,
                  Branch[A, B](B(), c, y, z, Emptyd[A, B]()))
  case (Branch(R(), Branch(B(), va, vb, vc, vd), w, x, Branch(R(), b, s, t, c)),
         y, z, Emptyd())
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), w,
                                       x, b),
                     s, t, Branch[A, B](B(), c, y, z, Emptyd[A, B]()))
  case (Branch(R(), Emptyd(), w, x, Branch(R(), b, s, t, c)), y, z,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), w, x, b), s, t,
                     Branch[A, B](B(), c, y, z,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (Branch(R(), Branch(B(), ve, vf, vg, vh), w, x, Branch(R(), b, s, t, c)),
         y, z, Branch(B(), va, vb, vc, vd))
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), ve, vf, vg, vh), w,
                                       x, b),
                     s, t,
                     Branch[A, B](B(), c, y, z,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (Emptyd(), w, x, Branch(R(), b, s, t, Branch(R(), c, y, z, d))) =>
    Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), w, x, b), s, t,
                  Branch[A, B](B(), c, y, z, d))
  case (Branch(B(), va, vb, vc, vd), w, x,
         Branch(R(), b, s, t, Branch(R(), c, y, z, d)))
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), w,
                                       x, b),
                     s, t, Branch[A, B](B(), c, y, z, d))
  case (Emptyd(), w, x, Branch(R(), Branch(R(), b, s, t, c), y, z, Emptyd())) =>
    Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), w, x, b), s, t,
                  Branch[A, B](B(), c, y, z, Emptyd[A, B]()))
  case (Emptyd(), w, x,
         Branch(R(), Branch(R(), b, s, t, c), y, z,
                 Branch(B(), va, vb, vc, vd)))
    => Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), w, x, b), s, t,
                     Branch[A, B](B(), c, y, z,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (Branch(B(), va, vb, vc, vd), w, x,
         Branch(R(), Branch(R(), b, s, t, c), y, z, Emptyd()))
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), w,
                                       x, b),
                     s, t, Branch[A, B](B(), c, y, z, Emptyd[A, B]()))
  case (Branch(B(), va, vb, vc, vd), w, x,
         Branch(R(), Branch(R(), b, s, t, c), y, z,
                 Branch(B(), ve, vf, vg, vh)))
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), w,
                                       x, b),
                     s, t,
                     Branch[A, B](B(), c, y, z,
                                   Branch[A, B](B(), ve, vf, vg, vh)))
  case (Emptyd(), s, t, Emptyd()) =>
    Branch[A, B](B(), Emptyd[A, B](), s, t, Emptyd[A, B]())
  case (Emptyd(), s, t, Branch(B(), va, vb, vc, vd)) =>
    Branch[A, B](B(), Emptyd[A, B](), s, t, Branch[A, B](B(), va, vb, vc, vd))
  case (Emptyd(), s, t, Branch(v, Emptyd(), vb, vc, Emptyd())) =>
    Branch[A, B](B(), Emptyd[A, B](), s, t,
                  Branch[A, B](v, Emptyd[A, B](), vb, vc, Emptyd[A, B]()))
  case (Emptyd(), s, t,
         Branch(v, Branch(B(), ve, vf, vg, vh), vb, vc, Emptyd()))
    => Branch[A, B](B(), Emptyd[A, B](), s, t,
                     Branch[A, B](v, Branch[A, B](B(), ve, vf, vg, vh), vb, vc,
                                   Emptyd[A, B]()))
  case (Emptyd(), s, t,
         Branch(v, Emptyd(), vb, vc, Branch(B(), vf, vg, vh, vi)))
    => Branch[A, B](B(), Emptyd[A, B](), s, t,
                     Branch[A, B](v, Emptyd[A, B](), vb, vc,
                                   Branch[A, B](B(), vf, vg, vh, vi)))
  case (Emptyd(), s, t,
         Branch(v, Branch(B(), ve, vj, vk, vl), vb, vc,
                 Branch(B(), vf, vg, vh, vi)))
    => Branch[A, B](B(), Emptyd[A, B](), s, t,
                     Branch[A, B](v, Branch[A, B](B(), ve, vj, vk, vl), vb, vc,
                                   Branch[A, B](B(), vf, vg, vh, vi)))
  case (Branch(B(), va, vb, vc, vd), s, t, Emptyd()) =>
    Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t, Emptyd[A, B]())
  case (Branch(B(), va, vb, vc, vd), s, t, Branch(B(), ve, vf, vg, vh)) =>
    Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t,
                  Branch[A, B](B(), ve, vf, vg, vh))
  case (Branch(B(), va, vb, vc, vd), s, t,
         Branch(v, Emptyd(), vf, vg, Emptyd()))
    => Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t,
                     Branch[A, B](v, Emptyd[A, B](), vf, vg, Emptyd[A, B]()))
  case (Branch(B(), va, vb, vc, vd), s, t,
         Branch(v, Branch(B(), vi, vj, vk, vl), vf, vg, Emptyd()))
    => Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t,
                     Branch[A, B](v, Branch[A, B](B(), vi, vj, vk, vl), vf, vg,
                                   Emptyd[A, B]()))
  case (Branch(B(), va, vb, vc, vd), s, t,
         Branch(v, Emptyd(), vf, vg, Branch(B(), vj, vk, vl, vm)))
    => Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t,
                     Branch[A, B](v, Emptyd[A, B](), vf, vg,
                                   Branch[A, B](B(), vj, vk, vl, vm)))
  case (Branch(B(), va, vb, vc, vd), s, t,
         Branch(v, Branch(B(), vi, vn, vo, vp), vf, vg,
                 Branch(B(), vj, vk, vl, vm)))
    => Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), s, t,
                     Branch[A, B](v, Branch[A, B](B(), vi, vn, vo, vp), vf, vg,
                                   Branch[A, B](B(), vj, vk, vl, vm)))
  case (Branch(v, Emptyd(), vb, vc, Emptyd()), s, t, Emptyd()) =>
    Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vb, vc, Emptyd[A, B]()),
                  s, t, Emptyd[A, B]())
  case (Branch(v, Emptyd(), vb, vc, Branch(B(), ve, vf, vg, vh)), s, t,
         Emptyd())
    => Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vb, vc,
                                       Branch[A, B](B(), ve, vf, vg, vh)),
                     s, t, Emptyd[A, B]())
  case (Branch(v, Branch(B(), vf, vg, vh, vi), vb, vc, Emptyd()), s, t,
         Emptyd())
    => Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vf, vg, vh, vi), vb,
                                       vc, Emptyd[A, B]()),
                     s, t, Emptyd[A, B]())
  case (Branch(v, Branch(B(), vf, vg, vh, vi), vb, vc,
                Branch(B(), ve, vj, vk, vl)),
         s, t, Emptyd())
    => Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vf, vg, vh, vi), vb,
                                       vc, Branch[A, B](B(), ve, vj, vk, vl)),
                     s, t, Emptyd[A, B]())
  case (Branch(v, Emptyd(), vf, vg, Emptyd()), s, t,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vf, vg,
                                       Emptyd[A, B]()),
                     s, t, Branch[A, B](B(), va, vb, vc, vd))
  case (Branch(v, Emptyd(), vf, vg, Branch(B(), vi, vj, vk, vl)), s, t,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vf, vg,
                                       Branch[A, B](B(), vi, vj, vk, vl)),
                     s, t, Branch[A, B](B(), va, vb, vc, vd))
  case (Branch(v, Branch(B(), vj, vk, vl, vm), vf, vg, Emptyd()), s, t,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vj, vk, vl, vm), vf,
                                       vg, Emptyd[A, B]()),
                     s, t, Branch[A, B](B(), va, vb, vc, vd))
  case (Branch(v, Branch(B(), vj, vk, vl, vm), vf, vg,
                Branch(B(), vi, vn, vo, vp)),
         s, t, Branch(B(), va, vb, vc, vd))
    => Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vj, vk, vl, vm), vf,
                                       vg, Branch[A, B](B(), vi, vn, vo, vp)),
                     s, t, Branch[A, B](B(), va, vb, vc, vd))
}

def rbt_comp_ins[A, B](c: A => A => ordera, f: A => B => B => B, k: A, v: B,
                        x4: rbt[A, B]):
      rbt[A, B]
  =
  (c, f, k, v, x4) match {
  case (c, f, k, v, Emptyd()) =>
    Branch[A, B](R(), Emptyd[A, B](), k, v, Emptyd[A, B]())
  case (c, f, k, v, Branch(B(), l, x, y, r)) =>
    ((c(k))(x) match {
       case Eqa() => Branch[A, B](B(), l, x, ((f(k))(y))(v), r)
       case Lt() => balance[A, B](rbt_comp_ins[A, B](c, f, k, v, l), x, y, r)
       case Gt() => balance[A, B](l, x, y, rbt_comp_ins[A, B](c, f, k, v, r))
     })
  case (c, f, k, v, Branch(R(), l, x, y, r)) =>
    ((c(k))(x) match {
       case Eqa() => Branch[A, B](R(), l, x, ((f(k))(y))(v), r)
       case Lt() =>
         Branch[A, B](R(), rbt_comp_ins[A, B](c, f, k, v, l), x, y, r)
       case Gt() =>
         Branch[A, B](R(), l, x, y, rbt_comp_ins[A, B](c, f, k, v, r))
     })
}

def paint[A, B](c: color, x1: rbt[A, B]): rbt[A, B] = (c, x1) match {
  case (c, Emptyd()) => Emptyd[A, B]()
  case (c, Branch(uu, l, k, v, r)) => Branch[A, B](c, l, k, v, r)
}

def rbt_comp_insert_with_key[A, B](c: A => A => ordera, f: A => B => B => B,
                                    k: A, v: B, t: rbt[A, B]):
      rbt[A, B]
  =
  paint[A, B](B(), rbt_comp_ins[A, B](c, f, k, v, t))

def rbt_comp_insert[A, B](c: A => A => ordera):
      A => B => (rbt[A, B]) => rbt[A, B]
  =
  ((a: A) => (b: B) => (d: rbt[A, B]) =>
    rbt_comp_insert_with_key[A, B](c, ((_: A) => (_: B) => (nv: B) => nv), a, b,
                                    d))

def insertc[A : ccompare, B](xc: A, xd: B, xe: mapping_rbt[A, B]):
      mapping_rbt[A, B]
  =
  Mapping_rbtb[A, B]((rbt_comp_insert[A, B](the[A =>
          A => ordera](ccompare[A]))).apply(xc).apply(xd).apply(impl_ofa[A,
                                  B](xe)))

def rbt_balir[A, B](t1: rbt[A, B], ab: A, bb: B, x3: rbt[A, B]): rbt[A, B] =
  (t1, ab, bb, x3) match {
  case (t1, ab, bb, Branch(R(), t2, aa, ba, Branch(R(), t3, a, b, t4))) =>
    Branch[A, B](R(), Branch[A, B](B(), t1, ab, bb, t2), aa, ba,
                  Branch[A, B](B(), t3, a, b, t4))
  case (t1, ab, bb, Branch(R(), Branch(R(), t2, aa, ba, t3), a, b, Emptyd())) =>
    Branch[A, B](R(), Branch[A, B](B(), t1, ab, bb, t2), aa, ba,
                  Branch[A, B](B(), t3, a, b, Emptyd[A, B]()))
  case (t1, ab, bb,
         Branch(R(), Branch(R(), t2, aa, ba, t3), a, b,
                 Branch(B(), va, vb, vc, vd)))
    => Branch[A, B](R(), Branch[A, B](B(), t1, ab, bb, t2), aa, ba,
                     Branch[A, B](B(), t3, a, b,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (t1, a, b, Emptyd()) => Branch[A, B](B(), t1, a, b, Emptyd[A, B]())
  case (t1, a, b, Branch(B(), va, vb, vc, vd)) =>
    Branch[A, B](B(), t1, a, b, Branch[A, B](B(), va, vb, vc, vd))
  case (t1, a, b, Branch(v, Emptyd(), vb, vc, Emptyd())) =>
    Branch[A, B](B(), t1, a, b,
                  Branch[A, B](v, Emptyd[A, B](), vb, vc, Emptyd[A, B]()))
  case (t1, a, b, Branch(v, Branch(B(), ve, vf, vg, vh), vb, vc, Emptyd())) =>
    Branch[A, B](B(), t1, a, b,
                  Branch[A, B](v, Branch[A, B](B(), ve, vf, vg, vh), vb, vc,
                                Emptyd[A, B]()))
  case (t1, a, b, Branch(v, Emptyd(), vb, vc, Branch(B(), vf, vg, vh, vi))) =>
    Branch[A, B](B(), t1, a, b,
                  Branch[A, B](v, Emptyd[A, B](), vb, vc,
                                Branch[A, B](B(), vf, vg, vh, vi)))
  case (t1, a, b,
         Branch(v, Branch(B(), ve, vj, vk, vl), vb, vc,
                 Branch(B(), vf, vg, vh, vi)))
    => Branch[A, B](B(), t1, a, b,
                     Branch[A, B](v, Branch[A, B](B(), ve, vj, vk, vl), vb, vc,
                                   Branch[A, B](B(), vf, vg, vh, vi)))
}

def equal_color(x0: color, x1: color): Boolean = (x0, x1) match {
  case (R(), B()) => false
  case (B(), R()) => false
  case (B(), B()) => true
  case (R(), R()) => true
}

def plus_nat(m: nat, n: nat): nat = Nata(integer_of_nat(m) + integer_of_nat(n))

def Suc(n: nat): nat = plus_nat(n, one_nata)

def bheight[A, B](x0: rbt[A, B]): nat = x0 match {
  case Emptyd() => zero_nat
  case Branch(c, lt, k, v, rt) =>
    (if (equal_color(c, B())) Suc(bheight[A, B](lt)) else bheight[A, B](lt))
}

def rbt_joinr[A, B](l: rbt[A, B], a: A, b: B, r: rbt[A, B]): rbt[A, B] =
  (if (less_eq_nat(bheight[A, B](l), bheight[A, B](r)))
    Branch[A, B](R(), l, a, b, r)
    else (l match {
            case Branch(R(), la, ab, ba, ra) =>
              Branch[A, B](R(), la, ab, ba, rbt_joinr[A, B](ra, a, b, r))
            case Branch(B(), la, ab, ba, ra) =>
              rbt_balir[A, B](la, ab, ba, rbt_joinr[A, B](ra, a, b, r))
          }))

def rbt_balil[A, B](x0: rbt[A, B], a: A, b: B, t4: rbt[A, B]): rbt[A, B] =
  (x0, a, b, t4) match {
  case (Branch(R(), Branch(R(), t1, ab, bb, t2), aa, ba, t3), a, b, t4) =>
    Branch[A, B](R(), Branch[A, B](B(), t1, ab, bb, t2), aa, ba,
                  Branch[A, B](B(), t3, a, b, t4))
  case (Branch(R(), Emptyd(), ab, bb, Branch(R(), t2, aa, ba, t3)), a, b, t4) =>
    Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), ab, bb, t2), aa, ba,
                  Branch[A, B](B(), t3, a, b, t4))
  case (Branch(R(), Branch(B(), va, vb, vc, vd), ab, bb,
                Branch(R(), t2, aa, ba, t3)),
         a, b, t4)
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd),
                                       ab, bb, t2),
                     aa, ba, Branch[A, B](B(), t3, a, b, t4))
  case (Emptyd(), a, b, t2) => Branch[A, B](B(), Emptyd[A, B](), a, b, t2)
  case (Branch(B(), va, vb, vc, vd), a, b, t2) =>
    Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), a, b, t2)
  case (Branch(v, Emptyd(), vb, vc, Emptyd()), a, b, t2) =>
    Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vb, vc, Emptyd[A, B]()),
                  a, b, t2)
  case (Branch(v, Emptyd(), vb, vc, Branch(B(), ve, vf, vg, vh)), a, b, t2) =>
    Branch[A, B](B(), Branch[A, B](v, Emptyd[A, B](), vb, vc,
                                    Branch[A, B](B(), ve, vf, vg, vh)),
                  a, b, t2)
  case (Branch(v, Branch(B(), vf, vg, vh, vi), vb, vc, Emptyd()), a, b, t2) =>
    Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vf, vg, vh, vi), vb, vc,
                                    Emptyd[A, B]()),
                  a, b, t2)
  case (Branch(v, Branch(B(), vf, vg, vh, vi), vb, vc,
                Branch(B(), ve, vj, vk, vl)),
         a, b, t2)
    => Branch[A, B](B(), Branch[A, B](v, Branch[A, B](B(), vf, vg, vh, vi), vb,
                                       vc, Branch[A, B](B(), ve, vj, vk, vl)),
                     a, b, t2)
}

def rbt_joinl[A, B](l: rbt[A, B], a: A, b: B, r: rbt[A, B]): rbt[A, B] =
  (if (less_eq_nat(bheight[A, B](r), bheight[A, B](l)))
    Branch[A, B](R(), l, a, b, r)
    else (r match {
            case Branch(R(), la, ab, ba, ra) =>
              Branch[A, B](R(), rbt_joinl[A, B](l, a, b, la), ab, ba, ra)
            case Branch(B(), la, ab, ba, ra) =>
              rbt_balil[A, B](rbt_joinl[A, B](l, a, b, la), ab, ba, ra)
          }))

def rbt_join[A, B](l: rbt[A, B], a: A, b: B, r: rbt[A, B]): rbt[A, B] =
  {
    val bhl = bheight[A, B](l): nat
    val bhr = bheight[A, B](r): nat;
    (if (less_nat(bhr, bhl)) paint[A, B](B(), rbt_joinr[A, B](l, a, b, r))
      else (if (less_nat(bhl, bhr))
             paint[A, B](B(), rbt_joinl[A, B](l, a, b, r))
             else Branch[A, B](B(), l, a, b, r)))
  }

def rbt_split_comp[A, B](c: A => A => ordera, x1: rbt[A, B], k: A):
      (rbt[A, B], (Option[B], rbt[A, B]))
  =
  (c, x1, k) match {
  case (c, Emptyd(), k) => (Emptyd[A, B](), (None, Emptyd[A, B]()))
  case (c, Branch(uu, l, a, b, r), x) =>
    ((c(x))(a) match {
       case Eqa() => (l, (Some[B](b), r))
       case Lt() =>
         {
           val (l1, (beta, l2)) =
             rbt_split_comp[A, B](c, l, x):
               ((rbt[A, B], (Option[B], rbt[A, B])));
           (l1, (beta, rbt_join[A, B](l2, a, b, r)))
         }
       case Gt() =>
         {
           val (r1, (beta, r2)) =
             rbt_split_comp[A, B](c, r, x):
               ((rbt[A, B], (Option[B], rbt[A, B])));
           (rbt_join[A, B](l, a, b, r1), (beta, r2))
         }
     })
}

def nat_of_integer(k: BigInt): nat = Nata(max[BigInt](BigInt(0), k))

def folda[A, B, C](f: A => B => C => C, xa1: rbt[A, B], x: C): C = (f, xa1, x)
  match {
  case (f, Branch(c, lt, k, v, rt), x) =>
    folda[A, B, C](f, rt, ((f(k))(v))(folda[A, B, C](f, lt, x)))
  case (f, Emptyd(), x) => x
}

def rbt_comp_union_swap_rec[A, B](c: A => A => ordera, f: A => B => B => B,
                                   gamma: Boolean, t1: rbt[A, B],
                                   t2: rbt[A, B]):
      rbt[A, B]
  =
  {
    val bh1 = bheight[A, B](t1): nat
    val bh2 = bheight[A, B](t2): nat
    val (gammaa, (t2a, (bh2a, (t1a, _)))) =
      (if (less_nat(bh1, bh2)) (! gamma, (t1, (bh1, (t2, bh2))))
        else (gamma, (t2, (bh2, (t1, bh1))))):
        ((Boolean, (rbt[A, B], (nat, (rbt[A, B], nat)))))
    val fa =
      (if (gammaa) ((k: A) => (v: B) => (va: B) => ((f(k))(va))(v)) else f):
        (A => B => B => B);
    (if (less_nat(bh2a, nat_of_integer(BigInt(4))))
      folda[A, B,
             rbt[A, B]](((a: A) => (b: B) => (d: rbt[A, B]) =>
                          rbt_comp_insert_with_key[A, B](c, fa, a, b, d)),
                         t2a, t1a)
      else (t1a match {
              case Emptyd() => t2a
              case Branch(_, l1, a, b, r1) =>
                {
                  val (l2, (beta, r2)) =
                    rbt_split_comp[A, B](c, t2a, a):
                      ((rbt[A, B], (Option[B], rbt[A, B])));
                  rbt_join[A, B](rbt_comp_union_swap_rec[A,
                  B](c, f, gammaa, l1, l2),
                                  a, (beta match {
case None => b
case Some(ca) => ((fa(a))(b))(ca)
                                      }),
                                  rbt_comp_union_swap_rec[A,
                   B](c, f, gammaa, r1, r2))
                }
            }))
  }

def rbt_comp_union_with_key[A, B](c: A => A => ordera, f: A => B => B => B,
                                   t1: rbt[A, B], t2: rbt[A, B]):
      rbt[A, B]
  =
  paint[A, B](B(), rbt_comp_union_swap_rec[A, B](c, f, false, t1, t2))

def join[A : ccompare,
          B](xc: A => B => B => B, xd: mapping_rbt[A, B],
              xe: mapping_rbt[A, B]):
      mapping_rbt[A, B]
  =
  Mapping_rbtb[A, B](rbt_comp_union_with_key[A,
      B](the[A => A => ordera](ccompare[A]), xc, impl_ofa[A, B](xd),
          impl_ofa[A, B](xe)))

def list_insert[A](equal: A => A => Boolean, x: A, xs: List[A]): List[A] =
  (if (list_member[A](equal, xs, x)) xs else x :: xs)

def inserta[A : ceq](xb: A, xc: set_dlist[A]): set_dlist[A] =
  Abs_dlist[A](list_insert[A](the[A => A => Boolean](ceq[A]), xb,
                               list_of_dlist[A](xc)))

def fold[A, B](f: A => B => B, x1: List[A], s: B): B = (f, x1, s) match {
  case (f, x :: xs, s) => fold[A, B](f, xs, (f(x))(s))
  case (f, Nil, s) => s
}

def foldc[A : ceq, B](x: A => B => B, xc: set_dlist[A]): B => B =
  ((a: B) => fold[A, B](x, list_of_dlist[A](xc), a))

def union[A : ceq]: (set_dlist[A]) => (set_dlist[A]) => set_dlist[A] =
  ((a: set_dlist[A]) =>
    foldc[A, set_dlist[A]](((aa: A) => (b: set_dlist[A]) => inserta[A](aa, b)),
                            a))

def id[A]: A => A = ((x: A) => x)

def is_none[A](x0: Option[A]): Boolean = x0 match {
  case Some(x) => false
  case None => true
}

def inter_list[A : ccompare](xb: mapping_rbt[A, Unit], xc: List[A]):
      mapping_rbt[A, Unit]
  =
  Mapping_rbtb[A, Unit](fold[A, rbt[A, Unit]](((k: A) =>
        (rbt_comp_insert[A, Unit](the[A =>
A => ordera](ccompare[A]))).apply(k).apply(())),
       filtera[A](((x: A) =>
                    ! (is_none[Unit](rbt_comp_lookup[A,
              Unit](the[A => A => ordera](ccompare[A]), impl_ofa[A, Unit](xb),
                     x)))),
                   xc),
       Emptyd[A, Unit]()))

def gen_length[A](n: nat, x1: List[A]): nat = (n, x1) match {
  case (n, x :: xs) => gen_length[A](Suc(n), xs)
  case (n, Nil) => n
}

def size_list[A]: (List[A]) => nat =
  ((a: List[A]) => gen_length[A](zero_nat, a))

def map_prod[A, B, C, D](f: A => B, g: C => D, x2: (A, C)): (B, D) = (f, g, x2)
  match {
  case (f, g, (a, b)) => (f(a), g(b))
}

def divmod_nat(m: nat, n: nat): (nat, nat) =
  {
    val k = integer_of_nat(m): BigInt
    val l = integer_of_nat(n): BigInt;
    map_prod[BigInt, nat, BigInt,
              nat](((a: BigInt) => nat_of_integer(a)),
                    ((a: BigInt) => nat_of_integer(a)),
                    (if (k == BigInt(0)) (BigInt(0), BigInt(0))
                      else (if (l == BigInt(0)) (BigInt(0), k)
                             else ((k: BigInt) => (l: BigInt) => if (l == 0)
                                    (BigInt(0), k) else
                                    (k.abs /% l.abs)).apply(k).apply(l))))
  }

def apfst[A, B, C](f: A => B, x1: (A, C)): (B, C) = (f, x1) match {
  case (f, (x, y)) => (f(x), y)
}

def rbtreeify_f[A, B](n: nat, kvs: List[(A, B)]): (rbt[A, B], List[(A, B)]) =
  (if (equal_nata(n, zero_nat)) (Emptyd[A, B](), kvs)
    else (if (equal_nata(n, one_nata))
           {
             val ((k, v) :: kvsa) = kvs: (List[(A, B)]);
             (Branch[A, B](R(), Emptyd[A, B](), k, v, Emptyd[A, B]()), kvsa)
           }
           else {
                  val (na, r) =
                    divmod_nat(n, nat_of_integer(BigInt(2))): ((nat, nat));
                  (if (equal_nata(r, zero_nat))
                    {
                      val (t1, (k, v) :: kvsa) =
                        rbtreeify_f[A, B](na, kvs): ((rbt[A, B], List[(A, B)]));
                      apfst[rbt[A, B], rbt[A, B],
                             List[(A, B)]](((a: rbt[A, B]) =>
     Branch[A, B](B(), t1, k, v, a)),
    rbtreeify_g[A, B](na, kvsa))
                    }
                    else {
                           val (t1, (k, v) :: kvsa) =
                             rbtreeify_f[A, B](na, kvs):
                               ((rbt[A, B], List[(A, B)]));
                           apfst[rbt[A, B], rbt[A, B],
                                  List[(A,
 B)]](((a: rbt[A, B]) => Branch[A, B](B(), t1, k, v, a)),
       rbtreeify_f[A, B](na, kvsa))
                         })
                }))

def rbtreeify_g[A, B](n: nat, kvs: List[(A, B)]): (rbt[A, B], List[(A, B)]) =
  (if (equal_nata(n, zero_nat) || equal_nata(n, one_nata)) (Emptyd[A, B](), kvs)
    else {
           val (na, r) = divmod_nat(n, nat_of_integer(BigInt(2))): ((nat, nat));
           (if (equal_nata(r, zero_nat))
             {
               val (t1, (k, v) :: kvsa) =
                 rbtreeify_g[A, B](na, kvs): ((rbt[A, B], List[(A, B)]));
               apfst[rbt[A, B], rbt[A, B],
                      List[(A, B)]](((a: rbt[A, B]) =>
                                      Branch[A, B](B(), t1, k, v, a)),
                                     rbtreeify_g[A, B](na, kvsa))
             }
             else {
                    val (t1, (k, v) :: kvsa) =
                      rbtreeify_f[A, B](na, kvs): ((rbt[A, B], List[(A, B)]));
                    apfst[rbt[A, B], rbt[A, B],
                           List[(A, B)]](((a: rbt[A, B]) =>
   Branch[A, B](B(), t1, k, v, a)),
  rbtreeify_g[A, B](na, kvsa))
                  })
         })

def rbtreeify[A, B](kvs: List[(A, B)]): rbt[A, B] =
  fst[rbt[A, B],
       List[(A, B)]](rbtreeify_g[A, B](Suc(size_list[(A, B)].apply(kvs)), kvs))

def gen_entries[A, B](kvts: List[((A, B), rbt[A, B])], x1: rbt[A, B]):
      List[(A, B)]
  =
  (kvts, x1) match {
  case (kvts, Branch(c, l, k, v, r)) =>
    gen_entries[A, B](((k, v), r) :: kvts, l)
  case ((kv, t) :: kvts, Emptyd()) => kv :: gen_entries[A, B](kvts, t)
  case (Nil, Emptyd()) => Nil
}

def entries[A, B]: (rbt[A, B]) => List[(A, B)] =
  ((a: rbt[A, B]) => gen_entries[A, B](Nil, a))

def filtere[A : ccompare, B](xb: ((A, B)) => Boolean, xc: mapping_rbt[A, B]):
      mapping_rbt[A, B]
  =
  Mapping_rbtb[A, B](rbtreeify[A, B](filtera[(A,
       B)](xb, entries[A, B].apply(impl_ofa[A, B](xc)))))

def map_filter[A, B](f: A => Option[B], x1: List[A]): List[B] = (f, x1) match {
  case (f, Nil) => Nil
  case (f, x :: xs) => (f(x) match {
                          case None => map_filter[A, B](f, xs)
                          case Some(y) => y :: map_filter[A, B](f, xs)
                        })
}

def map_filter_comp_inter[A, B, C,
                           D](c: A => A => ordera, f: A => B => C => D,
                               t1: rbt[A, B], t2: rbt[A, C]):
      List[(A, D)]
  =
  map_filter[(A, C),
              (A, D)](((a: (A, C)) =>
                        {
                          val (k, v) = a: ((A, C));
                          (rbt_comp_lookup[A, B](c, t1, k) match {
                             case None => None
                             case Some(va) => Some[(A, D)]((k, ((f(k))(va))(v)))
                           })
                        }),
                       entries[A, C].apply(t2))

def is_rbt_empty[A, B](t: rbt[A, B]): Boolean =
  (t match {
     case Emptyd() => true
     case Branch(_, _, _, _, _) => false
   })

def rbt_split_min[A, B](x0: rbt[A, B]): (A, (B, rbt[A, B])) = x0 match {
  case Emptyd() => sys.error("undefined")
  case Branch(uu, l, a, b, r) =>
    (if (is_rbt_empty[A, B](l)) (a, (b, r))
      else {
             val (aa, (ba, la)) = rbt_split_min[A, B](l): ((A, (B, rbt[A, B])));
             (aa, (ba, rbt_join[A, B](la, a, b, r)))
           })
}

def rbt_join2[A, B](l: rbt[A, B], r: rbt[A, B]): rbt[A, B] =
  (if (is_rbt_empty[A, B](r)) l
    else {
           val (a, (b, c)) = rbt_split_min[A, B](r): ((A, (B, rbt[A, B])));
           rbt_join[A, B](l, a, b, c)
         })

def rbt_comp_inter_swap_rec[A, B](c: A => A => ordera, f: A => B => B => B,
                                   gamma: Boolean, t1: rbt[A, B],
                                   t2: rbt[A, B]):
      rbt[A, B]
  =
  {
    val bh1 = bheight[A, B](t1): nat
    val bh2 = bheight[A, B](t2): nat
    val (gammaa, (t2a, (bh2a, (t1a, _)))) =
      (if (less_nat(bh1, bh2)) (! gamma, (t1, (bh1, (t2, bh2))))
        else (gamma, (t2, (bh2, (t1, bh1))))):
        ((Boolean, (rbt[A, B], (nat, (rbt[A, B], nat)))))
    val fa =
      (if (gammaa) ((k: A) => (v: B) => (va: B) => ((f(k))(va))(v)) else f):
        (A => B => B => B);
    (if (less_nat(bh2a, nat_of_integer(BigInt(4))))
      rbtreeify[A, B](map_filter_comp_inter[A, B, B, B](c, fa, t1a, t2a))
      else (t1a match {
              case Emptyd() => Emptyd[A, B]()
              case Branch(_, l1, a, b, r1) =>
                {
                  val (l2, (beta, r2)) =
                    rbt_split_comp[A, B](c, t2a, a):
                      ((rbt[A, B], (Option[B], rbt[A, B])))
                  val l =
                    rbt_comp_inter_swap_rec[A, B](c, f, gammaa, l1, l2):
                      (rbt[A, B])
                  val r =
                    rbt_comp_inter_swap_rec[A, B](c, f, gammaa, r1, r2):
                      (rbt[A, B]);
                  (beta match {
                     case None => rbt_join2[A, B](l, r)
                     case Some(ba) => rbt_join[A, B](l, a, ((fa(a))(b))(ba), r)
                   })
                }
            }))
  }

def rbt_comp_inter_with_key[A, B](c: A => A => ordera, f: A => B => B => B,
                                   t1: rbt[A, B], t2: rbt[A, B]):
      rbt[A, B]
  =
  paint[A, B](B(), rbt_comp_inter_swap_rec[A, B](c, f, false, t1, t2))

def meet[A : ccompare,
          B](xc: A => B => B => B, xd: mapping_rbt[A, B],
              xe: mapping_rbt[A, B]):
      mapping_rbt[A, B]
  =
  Mapping_rbtb[A, B](rbt_comp_inter_with_key[A,
      B](the[A => A => ordera](ccompare[A]), xc, impl_ofa[A, B](xd),
          impl_ofa[A, B](xe)))

def filterd[A : ceq](xb: A => Boolean, xc: set_dlist[A]): set_dlist[A] =
  Abs_dlist[A](filtera[A](xb, list_of_dlist[A](xc)))

def comp[A, B, C](f: A => B, g: C => A): C => B = ((x: C) => f(g(x)))

def sup_seta[A : ceq : ccompare](ba: set[A], b: set[A]): set[A] = (ba, b) match
  {
  case (ba, Complement(b)) => Complement[A](inf_seta[A](uminus_set[A](ba), b))
  case (Complement(ba), b) => Complement[A](inf_seta[A](ba, uminus_set[A](b)))
  case (b, Collect_set(a)) =>
    Collect_set[A](((x: A) => a(x) || member[A](x, b)))
  case (Collect_set(a), b) =>
    Collect_set[A](((x: A) => a(x) || member[A](x, b)))
  case (Set_monad(xs), Set_monad(ys)) => Set_monad[A](xs ++ ys)
  case (Dlist_set(dxs1), Set_monad(ws)) =>
    (ceq[A] match {
       case None =>
         { sys.error("union DList_set Set_Monad: ceq = None");
           (((_: Unit) =>
              sup_seta[A](Dlist_set[A](dxs1), Set_monad[A](ws)))).apply(())
           }
       case Some(_) =>
         Dlist_set[A](fold[A, set_dlist[A]](((a: A) => (b: set_dlist[A]) =>
      inserta[A](a, b)),
     ws, dxs1))
     })
  case (Set_monad(ws), Dlist_set(dxs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("union Set_Monad DList_set: ceq = None");
           (((_: Unit) =>
              sup_seta[A](Set_monad[A](ws), Dlist_set[A](dxs2)))).apply(())
           }
       case Some(_) =>
         Dlist_set[A](fold[A, set_dlist[A]](((a: A) => (b: set_dlist[A]) =>
      inserta[A](a, b)),
     ws, dxs2))
     })
  case (Rbt_set(rbt1), Set_monad(zs)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("union RBT_set Set_Monad: ccompare = None");
           (((_: Unit) =>
              sup_seta[A](Rbt_set[A](rbt1), Set_monad[A](zs)))).apply(())
           }
       case Some(_) =>
         Rbt_set[A](fold[A, mapping_rbt[A,
 Unit]](((k: A) => ((a: mapping_rbt[A, Unit]) => insertc[A, Unit](k, (), a))),
         zs, rbt1))
     })
  case (Set_monad(zs), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("union Set_Monad RBT_set: ccompare = None");
           (((_: Unit) =>
              sup_seta[A](Set_monad[A](zs), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(_) =>
         Rbt_set[A](fold[A, mapping_rbt[A,
 Unit]](((k: A) => ((a: mapping_rbt[A, Unit]) => insertc[A, Unit](k, (), a))),
         zs, rbt2))
     })
  case (Dlist_set(dxs1), Dlist_set(dxs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("union DList_set DList_set: ceq = None");
           (((_: Unit) =>
              sup_seta[A](Dlist_set[A](dxs1), Dlist_set[A](dxs2)))).apply(())
           }
       case Some(_) => Dlist_set[A](union[A].apply(dxs1).apply(dxs2))
     })
  case (Dlist_set(dxs), Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("union DList_set RBT_set: ccompare = None");
           (((_: Unit) =>
              sup_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
           }
       case Some(_) =>
         (ceq[A] match {
            case None =>
              { sys.error("union DList_set RBT_set: ceq = None");
                (((_: Unit) =>
                   sup_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
                }
            case Some(_) =>
              Rbt_set[A]((foldc[A, mapping_rbt[A,
        Unit]](((k: A) =>
                 ((a: mapping_rbt[A, Unit]) => insertc[A, Unit](k, (), a))),
                dxs)).apply(rbt))
          })
     })
  case (Rbt_set(rbt), Dlist_set(dxs)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("union RBT_set DList_set: ccompare = None");
           (((_: Unit) =>
              sup_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
           }
       case Some(_) =>
         (ceq[A] match {
            case None =>
              { sys.error("union RBT_set DList_set: ceq = None");
                (((_: Unit) =>
                   sup_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
                }
            case Some(_) =>
              Rbt_set[A]((foldc[A, mapping_rbt[A,
        Unit]](((k: A) =>
                 ((a: mapping_rbt[A, Unit]) => insertc[A, Unit](k, (), a))),
                dxs)).apply(rbt))
          })
     })
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("union RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              sup_seta[A](Rbt_set[A](rbt1), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(_) =>
         Rbt_set[A](join[A, Unit](((_: A) => (_: Unit) => id[Unit]), rbt1,
                                   rbt2))
     })
}

def inf_seta[A : ceq : ccompare](g: set[A], ga: set[A]): set[A] = (g, ga) match
  {
  case (Rbt_set(rbt1), Set_monad(xs)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter RBT_set Set_Monad: ccompare = None");
           (((_: Unit) =>
              inf_seta[A](Rbt_set[A](rbt1), Set_monad[A](xs)))).apply(())
           }
       case Some(_) => Rbt_set[A](inter_list[A](rbt1, xs))
     })
  case (Rbt_set(rbt), Dlist_set(dxs)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter RBT_set DList_set: ccompare = None");
           (((_: Unit) =>
              inf_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
           }
       case Some(_) =>
         (ceq[A] match {
            case None =>
              { sys.error("inter RBT_set DList_set: ceq = None");
                (((_: Unit) =>
                   inf_seta[A](Rbt_set[A](rbt), Dlist_set[A](dxs)))).apply(())
                }
            case Some(_) =>
              Rbt_set[A](inter_list[A](rbt, list_of_dlist[A](dxs)))
          })
     })
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              inf_seta[A](Rbt_set[A](rbt1), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(_) =>
         Rbt_set[A](meet[A, Unit](((_: A) => (_: Unit) => id[Unit]), rbt1,
                                   rbt2))
     })
  case (Dlist_set(dxs1), Set_monad(xs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter DList_set Set_Monad: ceq = None");
           (((_: Unit) =>
              inf_seta[A](Dlist_set[A](dxs1), Set_monad[A](xs)))).apply(())
           }
       case Some(eq) =>
         Dlist_set[A](filterd[A](((a: A) => list_member[A](eq, xs, a)), dxs1))
     })
  case (Dlist_set(dxs1), Dlist_set(dxs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter DList_set DList_set: ceq = None");
           (((_: Unit) =>
              inf_seta[A](Dlist_set[A](dxs1), Dlist_set[A](dxs2)))).apply(())
           }
       case Some(_) => Dlist_set[A](filterd[A](memberc[A](dxs2), dxs1))
     })
  case (Dlist_set(dxs), Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter DList_set RBT_set: ccompare = None");
           (((_: Unit) =>
              inf_seta[A](Dlist_set[A](dxs), Rbt_set[A](rbt)))).apply(())
           }
       case Some(_) =>
         (ceq[A] match {
            case None =>
              { sys.error("inter DList_set RBT_set: ceq = None");
                (((_: Unit) =>
                   inf_seta[A](Dlist_set[A](dxs), Rbt_set[A](rbt)))).apply(())
                }
            case Some(_) =>
              Rbt_set[A](inter_list[A](rbt, list_of_dlist[A](dxs)))
          })
     })
  case (Set_monad(xs1), Set_monad(xs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter Set_Monad Set_Monad: ceq = None");
           (((_: Unit) =>
              inf_seta[A](Set_monad[A](xs1), Set_monad[A](xs2)))).apply(())
           }
       case Some(eq) =>
         Set_monad[A](filtera[A](((a: A) => list_member[A](eq, xs2, a)), xs1))
     })
  case (Set_monad(xs), Dlist_set(dxs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter Set_Monad DList_set: ceq = None");
           (((_: Unit) =>
              inf_seta[A](Set_monad[A](xs), Dlist_set[A](dxs2)))).apply(())
           }
       case Some(eq) =>
         Dlist_set[A](filterd[A](((a: A) => list_member[A](eq, xs, a)), dxs2))
     })
  case (Set_monad(xs), Rbt_set(rbt1)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter Set_Monad RBT_set: ccompare = None");
           (((_: Unit) =>
              inf_seta[A](Rbt_set[A](rbt1), Set_monad[A](xs)))).apply(())
           }
       case Some(_) => Rbt_set[A](inter_list[A](rbt1, xs))
     })
  case (Complement(ba), Complement(b)) => Complement[A](sup_seta[A](ba, b))
  case (g, Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter RBT_set2: ccompare = None");
           (((_: Unit) => inf_seta[A](g, Rbt_set[A](rbt2)))).apply(()) }
       case Some(_) =>
         Rbt_set[A](filtere[A, Unit](comp[A, Boolean,
   (A, Unit)](((x: A) => member[A](x, g)), ((a: (A, Unit)) => fst[A, Unit](a))),
                                      rbt2))
     })
  case (Rbt_set(rbt1), g) =>
    (ccompare[A] match {
       case None =>
         { sys.error("inter RBT_set1: ccompare = None");
           (((_: Unit) => inf_seta[A](Rbt_set[A](rbt1), g))).apply(()) }
       case Some(_) =>
         Rbt_set[A](filtere[A, Unit](comp[A, Boolean,
   (A, Unit)](((x: A) => member[A](x, g)), ((a: (A, Unit)) => fst[A, Unit](a))),
                                      rbt1))
     })
  case (h, Dlist_set(dxs2)) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter DList_set2: ceq = None");
           (((_: Unit) => inf_seta[A](h, Dlist_set[A](dxs2)))).apply(()) }
       case Some(_) =>
         Dlist_set[A](filterd[A](((x: A) => member[A](x, h)), dxs2))
     })
  case (Dlist_set(dxs1), h) =>
    (ceq[A] match {
       case None =>
         { sys.error("inter DList_set1: ceq = None");
           (((_: Unit) => inf_seta[A](Dlist_set[A](dxs1), h))).apply(()) }
       case Some(_) =>
         Dlist_set[A](filterd[A](((x: A) => member[A](x, h)), dxs1))
     })
  case (i, Set_monad(xs)) =>
    Set_monad[A](filtera[A](((x: A) => member[A](x, i)), xs))
  case (Set_monad(xs), i) =>
    Set_monad[A](filtera[A](((x: A) => member[A](x, i)), xs))
  case (j, Collect_set(a)) =>
    Collect_set[A](((x: A) => a(x) && member[A](x, j)))
  case (Collect_set(a), j) =>
    Collect_set[A](((x: A) => a(x) && member[A](x, j)))
}

trait inf[A] {
  val `GeneratedCode.inf`: (A, A) => A
}
def inf[A](a: A, b: A)(implicit A: inf[A]): A = A.`GeneratedCode.inf`(a, b)
object inf {
  implicit def `GeneratedCode.inf_set`[A : ceq : ccompare]: inf[set[A]] = new
    inf[set[A]] {
    val `GeneratedCode.inf` = (a: set[A], b: set[A]) => inf_seta[A](a, b)
  }
}

trait sup[A] {
  val `GeneratedCode.sup`: (A, A) => A
}
def sup[A](a: A, b: A)(implicit A: sup[A]): A = A.`GeneratedCode.sup`(a, b)
object sup {
  implicit def `GeneratedCode.sup_set`[A : ceq : ccompare]: sup[set[A]] = new
    sup[set[A]] {
    val `GeneratedCode.sup` = (a: set[A], b: set[A]) => sup_seta[A](a, b)
  }
}

def less_set[A : cenum : ceq : ccompare](a: set[A], b: set[A]): Boolean =
  less_eq_set[A](a, b) && ! (less_eq_set[A](b, a))

trait semilattice_sup[A] extends sup[A] with order[A] {
}
object semilattice_sup {
  implicit def
    `GeneratedCode.semilattice_sup_set`[A : cenum : ceq : ccompare]:
      semilattice_sup[set[A]]
    = new semilattice_sup[set[A]] {
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
    val `GeneratedCode.sup` = (a: set[A], b: set[A]) => sup_seta[A](a, b)
  }
}

trait semilattice_inf[A] extends inf[A] with order[A] {
}
object semilattice_inf {
  implicit def
    `GeneratedCode.semilattice_inf_set`[A : cenum : ceq : ccompare]:
      semilattice_inf[set[A]]
    = new semilattice_inf[set[A]] {
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
    val `GeneratedCode.inf` = (a: set[A], b: set[A]) => inf_seta[A](a, b)
  }
}

trait lattice[A] extends semilattice_inf[A] with semilattice_sup[A] {
}
object lattice {
  implicit def
    `GeneratedCode.lattice_set`[A : cenum : ceq : ccompare]: lattice[set[A]] =
    new lattice[set[A]] {
    val `GeneratedCode.sup` = (a: set[A], b: set[A]) => sup_seta[A](a, b)
    val `GeneratedCode.inf` = (a: set[A], b: set[A]) => inf_seta[A](a, b)
    val `GeneratedCode.less_eq` = (a: set[A], b: set[A]) => less_eq_set[A](a, b)
    val `GeneratedCode.less` = (a: set[A], b: set[A]) => less_set[A](a, b)
  }
}

def ceq_seta[A : cenum : ceq : ccompare]:
      Option[(set[A]) => (set[A]) => Boolean]
  =
  (ceq[A] match {
     case None => None
     case Some(_) =>
       Some[(set[A]) =>
              (set[A]) =>
                Boolean](((a: set[A]) => (b: set[A]) => set_eq[A](a, b)))
   })

def set_impl_seta[A]: phantom[set[A], set_impla] =
  phantoma[set_impla, set[A]](set_choose())

def of_phantom[A, B](x0: phantom[A, B]): B = x0 match {
  case phantoma(x) => x
}

def finite_univ_seta[A : finite_univ]: phantom[set[A], Boolean] =
  phantoma[Boolean, set[A]](of_phantom[A, Boolean](finite_univ[A]))

def power[A : power](a: A, n: nat): A =
  (if (equal_nata(n, zero_nat)) one[A]
    else times[A](a, power[A](a, minus_nat(n, one_nata))))

def card_univ_seta[A : card_univ]: phantom[set[A], nat] =
  phantoma[nat, set[A]]({
                          val c = of_phantom[A, nat](card_univ[A]): nat;
                          (if (equal_nata(c, zero_nat)) zero_nat
                            else power[nat](nat_of_integer(BigInt(2)), c))
                        })

def emptyd[A : ccompare, B]: mapping_rbt[A, B] =
  Mapping_rbtb[A, B](Emptyd[A, B]())

def emptyb[A : ceq]: set_dlist[A] = Abs_dlist[A](Nil)

def set_empty_choose[A : ceq : ccompare]: set[A] =
  (ccompare[A] match {
     case None => (ceq[A] match {
                     case None => Set_monad[A](Nil)
                     case Some(_) => Dlist_set[A](emptyb[A])
                   })
     case Some(_) => Rbt_set[A](emptyd[A, Unit])
   })

def set_empty[A : ceq : ccompare](x0: set_impla): set[A] = x0 match {
  case set_choose() => set_empty_choose[A]
  case set_monada() => Set_monad[A](Nil)
  case set_rbt() => Rbt_set[A](emptyd[A, Unit])
  case set_dlista() => Dlist_set[A](emptyb[A])
  case set_collect() => Collect_set[A](((_: A) => false))
}

def fun_upda[A, B](equal: A => A => Boolean, f: A => B, aa: A, b: B, a: A): B =
  (if ((equal(aa))(a)) b else f(a))

def balance_right[A, B](a: rbt[A, B], k: A, x: B, xa3: rbt[A, B]): rbt[A, B] =
  (a, k, x, xa3) match {
  case (a, k, x, Branch(R(), b, s, y, c)) =>
    Branch[A, B](R(), a, k, x, Branch[A, B](B(), b, s, y, c))
  case (Branch(B(), a, k, x, b), s, y, Emptyd()) =>
    balance[A, B](Branch[A, B](R(), a, k, x, b), s, y, Emptyd[A, B]())
  case (Branch(B(), a, k, x, b), s, y, Branch(B(), va, vb, vc, vd)) =>
    balance[A, B](Branch[A, B](R(), a, k, x, b), s, y,
                   Branch[A, B](B(), va, vb, vc, vd))
  case (Branch(R(), a, k, x, Branch(B(), b, s, y, c)), t, z, Emptyd()) =>
    Branch[A, B](R(), balance[A, B](paint[A, B](R(), a), k, x, b), s, y,
                  Branch[A, B](B(), c, t, z, Emptyd[A, B]()))
  case (Branch(R(), a, k, x, Branch(B(), b, s, y, c)), t, z,
         Branch(B(), va, vb, vc, vd))
    => Branch[A, B](R(), balance[A, B](paint[A, B](R(), a), k, x, b), s, y,
                     Branch[A, B](B(), c, t, z,
                                   Branch[A, B](B(), va, vb, vc, vd)))
  case (Emptyd(), k, x, Emptyd()) => Emptyd[A, B]()
  case (Branch(R(), va, vb, vc, Emptyd()), k, x, Emptyd()) => Emptyd[A, B]()
  case (Branch(R(), va, vb, vc, Branch(R(), ve, vf, vg, vh)), k, x, Emptyd()) =>
    Emptyd[A, B]()
  case (Emptyd(), k, x, Branch(B(), va, vb, vc, vd)) => Emptyd[A, B]()
  case (Branch(R(), ve, vf, vg, Emptyd()), k, x, Branch(B(), va, vb, vc, vd)) =>
    Emptyd[A, B]()
  case (Branch(R(), ve, vf, vg, Branch(R(), vi, vj, vk, vl)), k, x,
         Branch(B(), va, vb, vc, vd))
    => Emptyd[A, B]()
}

def balance_left[A, B](x0: rbt[A, B], s: A, y: B, c: rbt[A, B]): rbt[A, B] =
  (x0, s, y, c) match {
  case (Branch(R(), a, k, x, b), s, y, c) =>
    Branch[A, B](R(), Branch[A, B](B(), a, k, x, b), s, y, c)
  case (Emptyd(), k, x, Branch(B(), a, s, y, b)) =>
    balance[A, B](Emptyd[A, B](), k, x, Branch[A, B](R(), a, s, y, b))
  case (Branch(B(), va, vb, vc, vd), k, x, Branch(B(), a, s, y, b)) =>
    balance[A, B](Branch[A, B](B(), va, vb, vc, vd), k, x,
                   Branch[A, B](R(), a, s, y, b))
  case (Emptyd(), k, x, Branch(R(), Branch(B(), a, s, y, b), t, z, c)) =>
    Branch[A, B](R(), Branch[A, B](B(), Emptyd[A, B](), k, x, a), s, y,
                  balance[A, B](b, t, z, paint[A, B](R(), c)))
  case (Branch(B(), va, vb, vc, vd), k, x,
         Branch(R(), Branch(B(), a, s, y, b), t, z, c))
    => Branch[A, B](R(), Branch[A, B](B(), Branch[A, B](B(), va, vb, vc, vd), k,
                                       x, a),
                     s, y, balance[A, B](b, t, z, paint[A, B](R(), c)))
  case (Emptyd(), k, x, Emptyd()) => Emptyd[A, B]()
  case (Emptyd(), k, x, Branch(R(), Emptyd(), vb, vc, vd)) => Emptyd[A, B]()
  case (Emptyd(), k, x, Branch(R(), Branch(R(), ve, vf, vg, vh), vb, vc, vd)) =>
    Emptyd[A, B]()
  case (Branch(B(), va, vb, vc, vd), k, x, Emptyd()) => Emptyd[A, B]()
  case (Branch(B(), va, vb, vc, vd), k, x, Branch(R(), Emptyd(), vf, vg, vh)) =>
    Emptyd[A, B]()
  case (Branch(B(), va, vb, vc, vd), k, x,
         Branch(R(), Branch(R(), vi, vj, vk, vl), vf, vg, vh))
    => Emptyd[A, B]()
}

def combine[A, B](xa0: rbt[A, B], x: rbt[A, B]): rbt[A, B] = (xa0, x) match {
  case (Emptyd(), x) => x
  case (Branch(v, va, vb, vc, vd), Emptyd()) => Branch[A, B](v, va, vb, vc, vd)
  case (Branch(R(), a, k, x, b), Branch(R(), c, s, y, d)) =>
    (combine[A, B](b, c) match {
       case Emptyd() =>
         Branch[A, B](R(), a, k, x, Branch[A, B](R(), Emptyd[A, B](), s, y, d))
       case Branch(R(), b2, t, z, c2) =>
         Branch[A, B](R(), Branch[A, B](R(), a, k, x, b2), t, z,
                       Branch[A, B](R(), c2, s, y, d))
       case Branch(B(), b2, t, z, c2) =>
         Branch[A, B](R(), a, k, x,
                       Branch[A, B](R(), Branch[A, B](B(), b2, t, z, c2), s, y,
                                     d))
     })
  case (Branch(B(), a, k, x, b), Branch(B(), c, s, y, d)) =>
    (combine[A, B](b, c) match {
       case Emptyd() =>
         balance_left[A, B](a, k, x, Branch[A, B](B(), Emptyd[A, B](), s, y, d))
       case Branch(R(), b2, t, z, c2) =>
         Branch[A, B](R(), Branch[A, B](B(), a, k, x, b2), t, z,
                       Branch[A, B](B(), c2, s, y, d))
       case Branch(B(), b2, t, z, c2) =>
         balance_left[A, B](a, k, x,
                             Branch[A, B](B(), Branch[A, B](B(), b2, t, z, c2),
   s, y, d))
     })
  case (Branch(B(), va, vb, vc, vd), Branch(R(), b, k, x, c)) =>
    Branch[A, B](R(), combine[A, B](Branch[A, B](B(), va, vb, vc, vd), b), k, x,
                  c)
  case (Branch(R(), a, k, x, b), Branch(B(), va, vb, vc, vd)) =>
    Branch[A, B](R(), a, k, x,
                  combine[A, B](b, Branch[A, B](B(), va, vb, vc, vd)))
}

def rbt_comp_del_from_right[A, B](c: A => A => ordera, x: A, a: rbt[A, B], y: A,
                                   s: B, xa5: rbt[A, B]):
      rbt[A, B]
  =
  (c, x, a, y, s, xa5) match {
  case (c, x, a, y, s, Branch(B(), lt, z, v, rt)) =>
    balance_right[A, B](a, y, s,
                         rbt_comp_del[A, B](c, x,
     Branch[A, B](B(), lt, z, v, rt)))
  case (c, x, a, y, s, Emptyd()) =>
    Branch[A, B](R(), a, y, s, rbt_comp_del[A, B](c, x, Emptyd[A, B]()))
  case (c, x, a, y, s, Branch(R(), va, vb, vc, vd)) =>
    Branch[A, B](R(), a, y, s,
                  rbt_comp_del[A, B](c, x, Branch[A, B](R(), va, vb, vc, vd)))
}

def rbt_comp_del_from_left[A, B](c: A => A => ordera, x: A, xa2: rbt[A, B],
                                  y: A, s: B, b: rbt[A, B]):
      rbt[A, B]
  =
  (c, x, xa2, y, s, b) match {
  case (c, x, Branch(B(), lt, z, v, rt), y, s, b) =>
    balance_left[A, B](rbt_comp_del[A, B](c, x,
   Branch[A, B](B(), lt, z, v, rt)),
                        y, s, b)
  case (c, x, Emptyd(), y, s, b) =>
    Branch[A, B](R(), rbt_comp_del[A, B](c, x, Emptyd[A, B]()), y, s, b)
  case (c, x, Branch(R(), va, vb, vc, vd), y, s, b) =>
    Branch[A, B](R(), rbt_comp_del[A, B](c, x,
  Branch[A, B](R(), va, vb, vc, vd)),
                  y, s, b)
}

def rbt_comp_del[A, B](c: A => A => ordera, x: A, xa2: rbt[A, B]): rbt[A, B] =
  (c, x, xa2) match {
  case (c, x, Emptyd()) => Emptyd[A, B]()
  case (c, x, Branch(uu, a, y, s, b)) =>
    ((c(x))(y) match {
       case Eqa() => combine[A, B](a, b)
       case Lt() => rbt_comp_del_from_left[A, B](c, x, a, y, s, b)
       case Gt() => rbt_comp_del_from_right[A, B](c, x, a, y, s, b)
     })
}

def rbt_comp_delete[A, B](c: A => A => ordera, k: A, t: rbt[A, B]): rbt[A, B] =
  paint[A, B](B(), rbt_comp_del[A, B](c, k, t))

def delete[A : ccompare, B](xb: A, xc: mapping_rbt[A, B]): mapping_rbt[A, B] =
  Mapping_rbtb[A, B](rbt_comp_delete[A, B](the[A => A => ordera](ccompare[A]),
    xb, impl_ofa[A, B](xc)))

def list_remove1[A](equal: A => A => Boolean, x: A, xa2: List[A]): List[A] =
  (equal, x, xa2) match {
  case (equal, x, y :: xs) =>
    (if ((equal(x))(y)) xs else y :: list_remove1[A](equal, x, xs))
  case (equal, x, Nil) => Nil
}

def removea[A : ceq](xb: A, xc: set_dlist[A]): set_dlist[A] =
  Abs_dlist[A](list_remove1[A](the[A => A => Boolean](ceq[A]), xb,
                                list_of_dlist[A](xc)))

def remove[A : ceq : ccompare](x: A, xa1: set[A]): set[A] = (x, xa1) match {
  case (x, Complement(a)) => Complement[A](insert[A](x, a))
  case (x, Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("remove RBT_set: ccompare = None");
           (((_: Unit) => remove[A](x, Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => Rbt_set[A](delete[A, Unit](x, rbt))
     })
  case (x, Dlist_set(dxs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("remove DList_set: ceq = None");
           (((_: Unit) => remove[A](x, Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => Dlist_set[A](removea[A](x, dxs))
     })
  case (x, Collect_set(a)) =>
    (ceq[A] match {
       case None =>
         { sys.error("remove Collect: ceq = None");
           (((_: Unit) => remove[A](x, Collect_set[A](a)))).apply(()) }
       case Some(eq) =>
         Collect_set[A](((b: A) => fun_upda[A, Boolean](eq, a, x, false, b)))
     })
}

def insert[A : ceq : ccompare](xa: A, x1: set[A]): set[A] = (xa, x1) match {
  case (xa, Complement(x)) => Complement[A](remove[A](xa, x))
  case (x, Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("insert RBT_set: ccompare = None");
           (((_: Unit) => insert[A](x, Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => Rbt_set[A](insertc[A, Unit](x, (), rbt))
     })
  case (x, Dlist_set(dxs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("insert DList_set: ceq = None");
           (((_: Unit) => insert[A](x, Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => Dlist_set[A](inserta[A](x, dxs))
     })
  case (x, Set_monad(xs)) => Set_monad[A](x :: xs)
  case (x, Collect_set(a)) =>
    (ceq[A] match {
       case None =>
         { sys.error("insert Collect_set: ceq = None");
           (((_: Unit) => insert[A](x, Collect_set[A](a)))).apply(()) }
       case Some(eq) =>
         Collect_set[A](((b: A) => fun_upda[A, Boolean](eq, a, x, true, b)))
     })
}

def foldl[A, B](f: A => B => A, a: A, x2: List[B]): A = (f, a, x2) match {
  case (f, a, Nil) => a
  case (f, a, x :: xs) => foldl[A, B](f, (f(a))(x), xs)
}

def set_aux[A : ceq : ccompare](impl: set_impla): (List[A]) => set[A] = impl
  match {
  case set_monada() => ((a: List[A]) => Set_monad[A](a))
  case set_choose() =>
    (ccompare[A] match {
       case None =>
         (ceq[A] match {
            case None => ((a: List[A]) => Set_monad[A](a))
            case Some(_) =>
              ((a: List[A]) =>
                foldl[set[A],
                       A](((s: set[A]) => (x: A) => insert[A](x, s)),
                           Dlist_set[A](emptyb[A]), a))
          })
       case Some(_) =>
         ((a: List[A]) =>
           foldl[set[A],
                  A](((s: set[A]) => (x: A) => insert[A](x, s)),
                      Rbt_set[A](emptyd[A, Unit]), a))
     })
  case impl =>
    ((a: List[A]) =>
      foldl[set[A],
             A](((s: set[A]) => (x: A) => insert[A](x, s)), set_empty[A](impl),
                 a))
}

def set[A : ceq : ccompare : set_impl](xs: List[A]): set[A] =
  (set_aux[A](of_phantom[A, set_impla](set_impl[A]))).apply(xs)

def subseqs[A](x0: List[A]): List[List[A]] = x0 match {
  case Nil => List(Nil)
  case x :: xs => {
                    val xss = subseqs[A](xs): (List[List[A]]);
                    mapa[List[A], List[A]](((a: List[A]) => x :: a), xss) ++ xss
                  }
}

def list_ex[A](p: A => Boolean, x1: List[A]): Boolean = (p, x1) match {
  case (p, Nil) => false
  case (p, x :: xs) => p(x) || list_ex[A](p, xs)
}

def cenum_seta[A : cenum : ceq : ccompare : set_impl]:
      Option[(List[set[A]],
               (((set[A]) => Boolean) => Boolean,
                 ((set[A]) => Boolean) => Boolean))]
  =
  (cenum[A] match {
     case None => None
     case Some((enum_a, (_, _))) =>
       Some[(List[set[A]],
              (((set[A]) => Boolean) => Boolean,
                ((set[A]) => Boolean) =>
                  Boolean))]((mapa[List[A],
                                    set[A]](((a: List[A]) => set[A](a)),
     subseqs[A](enum_a)),
                               (((p: (set[A]) => Boolean) =>
                                  list_all[set[A]](p,
            mapa[List[A],
                  set[A]](((a: List[A]) => set[A](a)), subseqs[A](enum_a)))),
                                 ((p: (set[A]) => Boolean) =>
                                   list_ex[set[A]](p,
            mapa[List[A],
                  set[A]](((a: List[A]) => set[A](a)), subseqs[A](enum_a)))))))
   })

def set_less_eq_aux_compl_fusion[A, B,
                                  C](less: A => A => Boolean,
                                      proper_interval:
Option[A] => Option[A] => Boolean,
                                      g1: generator[A, B], g2: generator[A, C],
                                      ao: Option[A], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    (if ((has_next[A, C](g2)).apply(s2))
      {
        val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B))
        val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
        (if ((less(x))(y))
          (proper_interval(ao))(Some[A](x)) ||
            set_less_eq_aux_compl_fusion[A, B,
  C](less, proper_interval, g1, g2, Some[A](x), s1a, s2)
          else (if ((less(y))(x))
                 (proper_interval(ao))(Some[A](y)) ||
                   set_less_eq_aux_compl_fusion[A, B,
         C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
                 else (proper_interval(ao))(Some[A](y))))
      }
      else true)
    else true)

def Compl_set_less_eq_aux_fusion[A, B,
                                  C](less: A => A => Boolean,
                                      proper_interval:
Option[A] => Option[A] => Boolean,
                                      g1: generator[A, B], g2: generator[A, C],
                                      ao: Option[A], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    {
      val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B));
      (if ((has_next[A, C](g2)).apply(s2))
        {
          val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
          (if ((less(x))(y))
            ! ((proper_interval(ao))(Some[A](x))) &&
              Compl_set_less_eq_aux_fusion[A, B,
    C](less, proper_interval, g1, g2, Some[A](x), s1a, s2)
            else (if ((less(y))(x))
                   ! ((proper_interval(ao))(Some[A](y))) &&
                     Compl_set_less_eq_aux_fusion[A, B,
           C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
                   else ! ((proper_interval(ao))(Some[A](y)))))
        }
        else ! ((proper_interval(ao))(Some[A](x))) &&
               Compl_set_less_eq_aux_fusion[A, B,
     C](less, proper_interval, g1, g2, Some[A](x), s1a, s2))
    }
    else (if ((has_next[A, C](g2)).apply(s2))
           {
             val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
             ! ((proper_interval(ao))(Some[A](y))) &&
               Compl_set_less_eq_aux_fusion[A, B,
     C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
           }
           else ! ((proper_interval(ao))(None))))

def set_less_eq_aux_compl[A](less: A => A => Boolean,
                              proper_interval:
                                Option[A] => Option[A] => Boolean,
                              ao: Option[A], xs: List[A], ys: List[A]):
      Boolean
  =
  (less, proper_interval, ao, xs, ys) match {
  case (less, proper_interval, ao, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      (proper_interval(ao))(Some[A](x)) ||
        set_less_eq_aux_compl[A](less, proper_interval, Some[A](x), xs, y :: ys)
      else (if ((less(y))(x))
             (proper_interval(ao))(Some[A](y)) ||
               set_less_eq_aux_compl[A](less, proper_interval, Some[A](y),
 x :: xs, ys)
             else (proper_interval(ao))(Some[A](y))))
  case (less, proper_interval, ao, xs, Nil) => true
  case (less, proper_interval, ao, Nil, ys) => true
}

def Compl_set_less_eq_aux[A](less: A => A => Boolean,
                              proper_interval:
                                Option[A] => Option[A] => Boolean,
                              ao: Option[A], x3: List[A], x4: List[A]):
      Boolean
  =
  (less, proper_interval, ao, x3, x4) match {
  case (less, proper_interval, ao, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      ! ((proper_interval(ao))(Some[A](x))) &&
        Compl_set_less_eq_aux[A](less, proper_interval, Some[A](x), xs, y :: ys)
      else (if ((less(y))(x))
             ! ((proper_interval(ao))(Some[A](y))) &&
               Compl_set_less_eq_aux[A](less, proper_interval, Some[A](y),
 x :: xs, ys)
             else ! ((proper_interval(ao))(Some[A](y)))))
  case (less, proper_interval, ao, x :: xs, Nil) =>
    ! ((proper_interval(ao))(Some[A](x))) &&
      Compl_set_less_eq_aux[A](less, proper_interval, Some[A](x), xs, Nil)
  case (less, proper_interval, ao, Nil, y :: ys) =>
    ! ((proper_interval(ao))(Some[A](y))) &&
      Compl_set_less_eq_aux[A](less, proper_interval, Some[A](y), Nil, ys)
  case (less, proper_interval, ao, Nil, Nil) => ! ((proper_interval(ao))(None))
}

def lexord_eq_fusion[A, B,
                      C](less: A => A => Boolean, g1: generator[A, B],
                          g2: generator[A, C], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    (has_next[A, C](g2)).apply(s2) &&
      {
        val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B))
        val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
        (less(x))(y) ||
          ! ((less(y))(x)) && lexord_eq_fusion[A, B, C](less, g1, g2, s1a, s2a)
      }
    else true)

def remdups_sorted[A](less: A => A => Boolean, x1: List[A]): List[A] =
  (less, x1) match {
  case (less, x :: y :: xs) =>
    (if ((less(x))(y)) x :: remdups_sorted[A](less, y :: xs)
      else remdups_sorted[A](less, y :: xs))
  case (less, List(x)) => List(x)
  case (less, Nil) => Nil
}

def quicksort_part[A](less: A => A => Boolean, ac: List[A], x: A, lts: List[A],
                       eqs: List[A], gts: List[A], xa6: List[A]):
      List[A]
  =
  (less, ac, x, lts, eqs, gts, xa6) match {
  case (less, ac, x, lts, eqs, gts, z :: zs) =>
    (if ((less(x))(z)) quicksort_part[A](less, ac, x, lts, eqs, z :: gts, zs)
      else (if ((less(z))(x))
             quicksort_part[A](less, ac, x, z :: lts, eqs, gts, zs)
             else quicksort_part[A](less, ac, x, lts, z :: eqs, gts, zs)))
  case (less, ac, x, lts, eqs, gts, Nil) =>
    quicksort_acc[A](less, eqs ++ (x :: quicksort_acc[A](less, ac, gts)), lts)
}

def quicksort_acc[A](less: A => A => Boolean, ac: List[A], x2: List[A]): List[A]
  =
  (less, ac, x2) match {
  case (less, ac, x :: v :: va) =>
    quicksort_part[A](less, ac, x, Nil, Nil, Nil, v :: va)
  case (less, ac, List(x)) => x :: ac
  case (less, ac, Nil) => ac
}

def quicksort[A](less: A => A => Boolean): (List[A]) => List[A] =
  ((a: List[A]) => quicksort_acc[A](less, Nil, a))

def gen_keys[A, B](kts: List[(A, rbt[A, B])], x1: rbt[A, B]): List[A] =
  (kts, x1) match {
  case (kts, Branch(c, l, k, v, r)) => gen_keys[A, B]((k, r) :: kts, l)
  case ((k, t) :: kts, Emptyd()) => k :: gen_keys[A, B](kts, t)
  case (Nil, Emptyd()) => Nil
}

def keysa[A, B]: (rbt[A, B]) => List[A] =
  ((a: rbt[A, B]) => gen_keys[A, B](Nil, a))

def keysb[A : ccompare](xa: mapping_rbt[A, Unit]): List[A] =
  keysa[A, Unit].apply(impl_ofa[A, Unit](xa))

def csorted_list_of_set[A : ceq : ccompare](x0: set[A]): List[A] = x0 match {
  case Set_monad(xs) =>
    (ccompare[A] match {
       case None =>
         { sys.error("csorted_list_of_set Set_Monad: ccompare = None");
           (((_: Unit) => csorted_list_of_set[A](Set_monad[A](xs)))).apply(()) }
       case Some(c) =>
         remdups_sorted[A](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                            (quicksort[A](((a: A) => (b: A) =>
    lt_of_comp[A](c, a, b)))).apply(xs))
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("csorted_list_of_set DList_set: ceq = None");
           (((_: Unit) => csorted_list_of_set[A](Dlist_set[A](dxs)))).apply(())
           }
       case Some(_) =>
         (ccompare[A] match {
            case None =>
              { sys.error("csorted_list_of_set DList_set: ccompare = None");
                (((_: Unit) =>
                   csorted_list_of_set[A](Dlist_set[A](dxs)))).apply(())
                }
            case Some(c) =>
              (quicksort[A](((a: A) => (b: A) =>
                              lt_of_comp[A](c, a,
     b)))).apply(list_of_dlist[A](dxs))
          })
     })
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("csorted_list_of_set RBT_set: ccompare = None");
           (((_: Unit) => csorted_list_of_set[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => keysb[A](rbt)
     })
}

def bot_set[A : ceq : ccompare : set_impl]: set[A] =
  set_empty[A](of_phantom[A, set_impla](set_impl[A]))

def top_set[A : ceq : ccompare : set_impl]: set[A] = uminus_set[A](bot_set[A])

def le_of_comp[A](acomp: A => A => ordera, x: A, y: A): Boolean =
  ((acomp(x))(y) match {
     case Eqa() => true
     case Lt() => true
     case Gt() => false
   })

def nulla[A](x0: List[A]): Boolean = x0 match {
  case Nil => true
  case x :: xs => false
}

def lexordp_eq[A](less: A => A => Boolean, xs: List[A], ys: List[A]): Boolean =
  (less, xs, ys) match {
  case (less, x :: xs, y :: ys) =>
    (less(x))(y) || ! ((less(y))(x)) && lexordp_eq[A](less, xs, ys)
  case (less, x :: xs, Nil) => false
  case (less, xs, Nil) => nulla[A](xs)
  case (less, Nil, ys) => true
}

def finite[A : finite_univ : ceq : ccompare](x0: set[A]): Boolean = x0 match {
  case Collect_set(p) =>
    of_phantom[A, Boolean](finite_univ[A]) ||
      { sys.error("finite Collect_set");
        (((_: Unit) => finite[A](Collect_set[A](p)))).apply(()) }
  case Set_monad(xs) => true
  case Complement(a) =>
    (if (of_phantom[A, Boolean](finite_univ[A])) true
      else (if (finite[A](a)) false
             else { sys.error("finite Complement: infinite set");
                    (((_: Unit) => finite[A](Complement[A](a)))).apply(()) }))
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("finite RBT_set: ccompare = None");
           (((_: Unit) => finite[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => true
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("finite DList_set: ceq = None");
           (((_: Unit) => finite[A](Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => true
     })
}

def set_less_aux_compl_fusion[A, B,
                               C](less: A => A => Boolean,
                                   proper_interval:
                                     Option[A] => Option[A] => Boolean,
                                   g1: generator[A, B], g2: generator[A, C],
                                   ao: Option[A], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    {
      val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B));
      (if ((has_next[A, C](g2)).apply(s2))
        {
          val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
          (if ((less(x))(y))
            (proper_interval(ao))(Some[A](x)) ||
              set_less_aux_compl_fusion[A, B,
 C](less, proper_interval, g1, g2, Some[A](x), s1a, s2)
            else (if ((less(y))(x))
                   (proper_interval(ao))(Some[A](y)) ||
                     set_less_aux_compl_fusion[A, B,
        C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
                   else (proper_interval(ao))(Some[A](y))))
        }
        else (proper_interval(ao))(Some[A](x)) ||
               set_less_aux_compl_fusion[A, B,
  C](less, proper_interval, g1, g2, Some[A](x), s1a, s2))
    }
    else (if ((has_next[A, C](g2)).apply(s2))
           {
             val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
             (proper_interval(ao))(Some[A](y)) ||
               set_less_aux_compl_fusion[A, B,
  C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
           }
           else (proper_interval(ao))(None)))

def Compl_set_less_aux_fusion[A, B,
                               C](less: A => A => Boolean,
                                   proper_interval:
                                     Option[A] => Option[A] => Boolean,
                                   g1: generator[A, B], g2: generator[A, C],
                                   ao: Option[A], s1: B, s2: C):
      Boolean
  =
  (has_next[A, B](g1)).apply(s1) &&
    ((has_next[A, C](g2)).apply(s2) &&
      {
        val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B))
        val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
        (if ((less(x))(y))
          ! ((proper_interval(ao))(Some[A](x))) &&
            Compl_set_less_aux_fusion[A, B,
                                       C](less, proper_interval, g1, g2,
   Some[A](x), s1a, s2)
          else (if ((less(y))(x))
                 ! ((proper_interval(ao))(Some[A](y))) &&
                   Compl_set_less_aux_fusion[A, B,
      C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
                 else ! ((proper_interval(ao))(Some[A](y)))))
      })

def set_less_aux_compl[A](less: A => A => Boolean,
                           proper_interval: Option[A] => Option[A] => Boolean,
                           ao: Option[A], x3: List[A], x4: List[A]):
      Boolean
  =
  (less, proper_interval, ao, x3, x4) match {
  case (less, proper_interval, ao, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      (proper_interval(ao))(Some[A](x)) ||
        set_less_aux_compl[A](less, proper_interval, Some[A](x), xs, y :: ys)
      else (if ((less(y))(x))
             (proper_interval(ao))(Some[A](y)) ||
               set_less_aux_compl[A](less, proper_interval, Some[A](y), x :: xs,
                                      ys)
             else (proper_interval(ao))(Some[A](y))))
  case (less, proper_interval, ao, x :: xs, Nil) =>
    (proper_interval(ao))(Some[A](x)) ||
      set_less_aux_compl[A](less, proper_interval, Some[A](x), xs, Nil)
  case (less, proper_interval, ao, Nil, y :: ys) =>
    (proper_interval(ao))(Some[A](y)) ||
      set_less_aux_compl[A](less, proper_interval, Some[A](y), Nil, ys)
  case (less, proper_interval, ao, Nil, Nil) => (proper_interval(ao))(None)
}

def Compl_set_less_aux[A](less: A => A => Boolean,
                           proper_interval: Option[A] => Option[A] => Boolean,
                           ao: Option[A], xs: List[A], ys: List[A]):
      Boolean
  =
  (less, proper_interval, ao, xs, ys) match {
  case (less, proper_interval, ao, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      ! ((proper_interval(ao))(Some[A](x))) &&
        Compl_set_less_aux[A](less, proper_interval, Some[A](x), xs, y :: ys)
      else (if ((less(y))(x))
             ! ((proper_interval(ao))(Some[A](y))) &&
               Compl_set_less_aux[A](less, proper_interval, Some[A](y), x :: xs,
                                      ys)
             else ! ((proper_interval(ao))(Some[A](y)))))
  case (less, proper_interval, ao, xs, Nil) => false
  case (less, proper_interval, ao, Nil, ys) => false
}

def lexord_fusion[A, B,
                   C](less: A => A => Boolean, g1: generator[A, B],
                       g2: generator[A, C], s1: B, s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    (if ((has_next[A, C](g2)).apply(s2))
      {
        val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B))
        val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
        (less(x))(y) ||
          ! ((less(y))(x)) && lexord_fusion[A, B, C](less, g1, g2, s1a, s2a)
      }
      else false)
    else (has_next[A, C](g2)).apply(s2))

def lexordp[A](less: A => A => Boolean, xs: List[A], ys: List[A]): Boolean =
  (less, xs, ys) match {
  case (less, x :: xs, y :: ys) =>
    (less(x))(y) || ! ((less(y))(x)) && lexordp[A](less, xs, ys)
  case (less, xs, Nil) => false
  case (less, Nil, ys) => ! (nulla[A](ys))
}

def comp_of_ords[A](le: A => A => Boolean, lt: A => A => Boolean, x: A, y: A):
      ordera
  =
  (if ((lt(x))(y)) Lt() else (if ((le(x))(y)) Eqa() else Gt()))

def cless_eq_set[A : finite_univ : ceq : cproper_interval : set_impl](a: set[A],
                               b: set[A]):
      Boolean
  =
  (a, b) match {
  case (Complement(Rbt_set(rbt1)), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set (Complement RBT_set) RBT_set: ccompare = None");
           (((_: Unit) =>
              cless_eq_set[A](Complement[A](Rbt_set[A](rbt1)),
                               Rbt_set[A](rbt2)))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           Compl_set_less_eq_aux_fusion[A,
 (List[(A, rbt[A, Unit])], rbt[A, Unit]),
 (List[(A, rbt[A, Unit])],
   rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                   ((a: Option[A]) => (b: Option[A]) =>
                     cproper_interval[A](a, b)),
                   rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
                   None, init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
     })
  case (Rbt_set(rbt1), Complement(Rbt_set(rbt2))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set RBT_set (Complement RBT_set): ccompare = None");
           (((_: Unit) =>
              cless_eq_set[A](Rbt_set[A](rbt1),
                               Complement[A](Rbt_set[A](rbt2))))).apply(())
           }
       case Some(c) =>
         (if (finite[A](top_set[A]))
           set_less_eq_aux_compl_fusion[A,
 (List[(A, rbt[A, Unit])], rbt[A, Unit]),
 (List[(A, rbt[A, Unit])],
   rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                   ((a: Option[A]) => (b: Option[A]) =>
                     cproper_interval[A](a, b)),
                   rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
                   None, init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
           else true)
     })
  case (Rbt_set(rbta), Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              cless_eq_set[A](Rbt_set[A](rbta), Rbt_set[A](rbt)))).apply(())
           }
       case Some(c) =>
         lexord_eq_fusion[A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                           (List[(A, rbt[A, Unit])],
                             rbt[A, Unit])](((x: A) => (y: A) =>
      lt_of_comp[A](c, y, x)),
     rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
     init[A, Unit, A](rbta), init[A, Unit, A](rbt))
     })
  case (Complement(a), Complement(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set Complement Complement: ccompare = None");
           (((_: Unit) =>
              le_of_comp[set[A]](the[(set[A]) =>
                                       (set[A]) => ordera](ccompare_seta[A]),
                                  Complement[A](a),
                                  Complement[A](b)))).apply(())
           }
       case Some(_) => cless_eq_set[A](b, a)
     })
  case (Complement(a), b) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set Complement1: ccompare = None");
           (((_: Unit) => cless_eq_set[A](Complement[A](a), b))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           finite[A](top_set[A]) &&
             Compl_set_less_eq_aux[A](((aa: A) => (ba: A) =>
lt_of_comp[A](c, aa, ba)),
                                       ((aa: Option[A]) => (ba: Option[A]) =>
 cproper_interval[A](aa, ba)),
                                       None, csorted_list_of_set[A](a),
                                       csorted_list_of_set[A](b))
           else { sys.error("cless_eq_set Complement1: infinite set");
                  (((_: Unit) =>
                     cless_eq_set[A](Complement[A](a), b))).apply(())
                  })
     })
  case (a, Complement(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set Complement2: ccompare = None");
           (((_: Unit) => cless_eq_set[A](a, Complement[A](b)))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           (if (finite[A](top_set[A]))
             set_less_eq_aux_compl[A](((aa: A) => (ba: A) =>
lt_of_comp[A](c, aa, ba)),
                                       ((aa: Option[A]) => (ba: Option[A]) =>
 cproper_interval[A](aa, ba)),
                                       None, csorted_list_of_set[A](a),
                                       csorted_list_of_set[A](b))
             else true)
           else { sys.error("cless_eq_set Complement2: infinite set");
                  (((_: Unit) =>
                     cless_eq_set[A](a, Complement[A](b)))).apply(())
                  })
     })
  case (a, b) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_eq_set: ccompare = None");
           (((_: Unit) => cless_eq_set[A](a, b))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           lexordp_eq[A](((x: A) => (y: A) => lt_of_comp[A](c, y, x)),
                          csorted_list_of_set[A](a), csorted_list_of_set[A](b))
           else { sys.error("cless_eq_set: infinite set");
                  (((_: Unit) => cless_eq_set[A](a, b))).apply(()) })
     })
}

def cless_set[A : finite_univ : ceq : cproper_interval : set_impl](a: set[A],
                            b: set[A]):
      Boolean
  =
  (a, b) match {
  case (Complement(Rbt_set(rbt1)), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set (Complement RBT_set) RBT_set: ccompare = None");
           (((_: Unit) =>
              cless_set[A](Complement[A](Rbt_set[A](rbt1)),
                            Rbt_set[A](rbt2)))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           Compl_set_less_aux_fusion[A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                                      (List[(A, rbt[A, Unit])],
rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                ((a: Option[A]) => (b: Option[A]) => cproper_interval[A](a, b)),
                rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit], None,
                init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
     })
  case (Rbt_set(rbt1), Complement(Rbt_set(rbt2))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set RBT_set (Complement RBT_set): ccompare = None");
           (((_: Unit) =>
              cless_set[A](Rbt_set[A](rbt1),
                            Complement[A](Rbt_set[A](rbt2))))).apply(())
           }
       case Some(c) =>
         (if (finite[A](top_set[A]))
           set_less_aux_compl_fusion[A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                                      (List[(A, rbt[A, Unit])],
rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                ((a: Option[A]) => (b: Option[A]) => cproper_interval[A](a, b)),
                rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit], None,
                init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
           else true)
     })
  case (Rbt_set(rbta), Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              cless_set[A](Rbt_set[A](rbta), Rbt_set[A](rbt)))).apply(())
           }
       case Some(c) =>
         lexord_fusion[A, (List[(A, rbt[A, Unit])], rbt[A, Unit]),
                        (List[(A, rbt[A, Unit])],
                          rbt[A, Unit])](((x: A) => (y: A) =>
   lt_of_comp[A](c, y, x)),
  rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
  init[A, Unit, A](rbta), init[A, Unit, A](rbt))
     })
  case (Complement(a), Complement(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set Complement Complement: ccompare = None");
           (((_: Unit) =>
              cless_set[A](Complement[A](a), Complement[A](b)))).apply(())
           }
       case Some(_) =>
         lt_of_comp[set[A]](the[(set[A]) =>
                                  (set[A]) => ordera](ccompare_seta[A]),
                             b, a)
     })
  case (Complement(a), b) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set Complement1: ccompare = None");
           (((_: Unit) => cless_set[A](Complement[A](a), b))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           finite[A](top_set[A]) &&
             Compl_set_less_aux[A](((aa: A) => (ba: A) =>
                                     lt_of_comp[A](c, aa, ba)),
                                    ((aa: Option[A]) => (ba: Option[A]) =>
                                      cproper_interval[A](aa, ba)),
                                    None, csorted_list_of_set[A](a),
                                    csorted_list_of_set[A](b))
           else { sys.error("cless_set Complement1: infinite set");
                  (((_: Unit) => cless_set[A](Complement[A](a), b))).apply(())
                  })
     })
  case (a, Complement(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set Complement2: ccompare = None");
           (((_: Unit) => cless_set[A](a, Complement[A](b)))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           (if (finite[A](top_set[A]))
             set_less_aux_compl[A](((aa: A) => (ba: A) =>
                                     lt_of_comp[A](c, aa, ba)),
                                    ((aa: Option[A]) => (ba: Option[A]) =>
                                      cproper_interval[A](aa, ba)),
                                    None, csorted_list_of_set[A](a),
                                    csorted_list_of_set[A](b))
             else true)
           else { sys.error("cless_set Complement2: infinite set");
                  (((_: Unit) => cless_set[A](a, Complement[A](b)))).apply(())
                  })
     })
  case (a, b) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cless_set: ccompare = None");
           (((_: Unit) => cless_set[A](a, b))).apply(()) }
       case Some(c) =>
         (if (finite[A](a) && finite[A](b))
           lexordp[A](((x: A) => (y: A) => lt_of_comp[A](c, y, x)),
                       csorted_list_of_set[A](a), csorted_list_of_set[A](b))
           else { sys.error("cless_set: infinite set");
                  (((_: Unit) => cless_set[A](a, b))).apply(()) })
     })
}

def ccompare_seta[A : finite_univ : ceq : cproper_interval : set_impl]:
      Option[(set[A]) => (set[A]) => ordera]
  =
  (ccompare[A] match {
     case None => None
     case Some(_) =>
       Some[(set[A]) =>
              (set[A]) =>
                ordera](((a: set[A]) => (b: set[A]) =>
                          comp_of_ords[set[A]](((aa: set[A]) => (ba: set[A]) =>
         cless_eq_set[A](aa, ba)),
        ((aa: set[A]) => (ba: set[A]) => cless_set[A](aa, ba)), a, b)))
   })

def mapping_impl_seta[A]: phantom[set[A], mapping_impla] =
  phantoma[mapping_impla, set[A]](mapping_choose())

def fold_fusion[A, B, C](g: generator[A, B], f: A => C => C, s: B, b: C): C =
  (if ((has_next[A, B](g)).apply(s))
    {
      val (x, sa) = (next[A, B](g)).apply(s): ((A, B));
      fold_fusion[A, B, C](g, f, sa, (f(x))(b))
    }
    else b)

def length_last_fusion[A, B](g: generator[A, B], s: B): (nat, A) =
  (if ((has_next[A, B](g)).apply(s))
    {
      val (x, sa) = (next[A, B](g)).apply(s): ((A, B));
      fold_fusion[A, B,
                   (nat, A)](g, ((xa: A) => (a: (nat, A)) =>
                                  {
                                    val (n, _) = a: ((nat, A));
                                    (plus_nat(n, one_nata), xa)
                                  }),
                              sa, (one_nata, x))
    }
    else (zero_nat, sys.error("undefined")))

def gen_length_fusion[A, B](g: generator[A, B], n: nat, s: B): nat =
  (if ((has_next[A, B](g)).apply(s))
    gen_length_fusion[A, B](g, Suc(n), snd[A, B]((next[A, B](g)).apply(s)))
    else n)

def length_fusion[A, B](g: generator[A, B]): B => nat =
  ((a: B) => gen_length_fusion[A, B](g, zero_nat, a))

def list_remdups[A](equal: A => A => Boolean, x1: List[A]): List[A] =
  (equal, x1) match {
  case (equal, x :: xs) =>
    (if (list_member[A](equal, xs, x)) list_remdups[A](equal, xs)
      else x :: list_remdups[A](equal, xs))
  case (equal, Nil) => Nil
}

def length[A : ceq](xa: set_dlist[A]): nat =
  size_list[A].apply(list_of_dlist[A](xa))

def card[A : card_univ : ceq : ccompare](x0: set[A]): nat = x0 match {
  case Complement(a) =>
    {
      val aa = card[A](a): nat
      val s = of_phantom[A, nat](card_univ[A]): nat;
      (if (less_nat(zero_nat, s)) minus_nat(s, aa)
        else (if (finite[A](a)) zero_nat
               else { sys.error("card Complement: infinite");
                      (((_: Unit) => card[A](Complement[A](a)))).apply(()) }))
    }
  case Set_monad(xs) =>
    (ceq[A] match {
       case None =>
         { sys.error("card Set_Monad: ceq = None");
           (((_: Unit) => card[A](Set_monad[A](xs)))).apply(()) }
       case Some(eq) => size_list[A].apply(list_remdups[A](eq, xs))
     })
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("card RBT_set: ccompare = None");
           (((_: Unit) => card[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => size_list[A].apply(keysb[A](rbt))
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("card DList_set: ceq = None");
           (((_: Unit) => card[A](Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => length[A](dxs)
     })
}

def proper_interval_set_compl_aux_fusion[A : card_univ : ceq : ccompare : set_impl,
  B, C](less: A => A => Boolean,
         proper_interval: Option[A] => Option[A] => Boolean,
         g1: generator[A, B], g2: generator[A, C], ao: Option[A], n: nat, s1: B,
         s2: C):
      Boolean
  =
  (if ((has_next[A, B](g1)).apply(s1))
    {
      val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B));
      (if ((has_next[A, C](g2)).apply(s2))
        {
          val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
          (if ((less(x))(y))
            (proper_interval(ao))(Some[A](x)) ||
              proper_interval_set_compl_aux_fusion[A, B,
            C](less, proper_interval, g1, g2, Some[A](x), plus_nat(n, one_nata),
                s1a, s2)
            else (if ((less(y))(x))
                   (proper_interval(ao))(Some[A](y)) ||
                     proper_interval_set_compl_aux_fusion[A, B,
                   C](less, proper_interval, g1, g2, Some[A](y),
                       plus_nat(n, one_nata), s1, s2a)
                   else (proper_interval(ao))(Some[A](x)) &&
                          {
                            val m = minus_nat(card[A](top_set[A]), n): nat;
                            ! (equal_nata(minus_nat(m,
             (length_fusion[A, C](g2)).apply(s2a)),
   nat_of_integer(BigInt(2)))) ||
                              ! (equal_nata(minus_nat(m,
               (length_fusion[A, B](g1)).apply(s1a)),
     nat_of_integer(BigInt(2))))
                          }))
        }
        else {
               val m = minus_nat(card[A](top_set[A]), n): nat
               val (len_x, xa) = length_last_fusion[A, B](g1, s1): ((nat, A));
               ! (equal_nata(m, len_x)) &&
                 (if (equal_nata(m, plus_nat(len_x, one_nata)))
                   ! ((proper_interval(Some[A](xa)))(None)) else true)
             })
    }
    else (if ((has_next[A, C](g2)).apply(s2))
           {
             val (_, _) = (next[A, C](g2)).apply(s2): ((A, C))
             val m = minus_nat(card[A](top_set[A]), n): nat
             val (len_y, y) = length_last_fusion[A, C](g2, s2): ((nat, A));
             ! (equal_nata(m, len_y)) &&
               (if (equal_nata(m, plus_nat(len_y, one_nata)))
                 ! ((proper_interval(Some[A](y)))(None)) else true)
           }
           else less_nat(plus_nat(n, one_nata), card[A](top_set[A]))))

def proper_interval_compl_set_aux_fusion[A, B,
  C](less: A => A => Boolean,
      proper_interval: Option[A] => Option[A] => Boolean, g1: generator[A, B],
      g2: generator[A, C], ao: Option[A], s1: B, s2: C):
      Boolean
  =
  (has_next[A, B](g1)).apply(s1) &&
    ((has_next[A, C](g2)).apply(s2) &&
      {
        val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B))
        val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
        (if ((less(x))(y))
          ! ((proper_interval(ao))(Some[A](x))) &&
            proper_interval_compl_set_aux_fusion[A, B,
          C](less, proper_interval, g1, g2, Some[A](x), s1a, s2)
          else (if ((less(y))(x))
                 ! ((proper_interval(ao))(Some[A](y))) &&
                   proper_interval_compl_set_aux_fusion[A, B,
                 C](less, proper_interval, g1, g2, Some[A](y), s1, s2a)
                 else ! ((proper_interval(ao))(Some[A](x))) &&
                        ((has_next[A, C](g2)).apply(s2a) ||
                          (has_next[A, B](g1)).apply(s1a))))
      })

def exhaustive_above_fusion[A, B](proper_interval:
                                    Option[A] => Option[A] => Boolean,
                                   g: generator[A, B], y: A, s: B):
      Boolean
  =
  (if ((has_next[A, B](g)).apply(s))
    {
      val (x, sa) = (next[A, B](g)).apply(s): ((A, B));
      ! ((proper_interval(Some[A](y)))(Some[A](x))) &&
        exhaustive_above_fusion[A, B](proper_interval, g, x, sa)
    }
    else ! ((proper_interval(Some[A](y)))(None)))

def proper_interval_set_aux_fusion[A, B,
                                    C](less: A => A => Boolean,
proper_interval: Option[A] => Option[A] => Boolean, g1: generator[A, B],
g2: generator[A, C], s1: B, s2: C):
      Boolean
  =
  (has_next[A, C](g2)).apply(s2) &&
    {
      val (y, s2a) = (next[A, C](g2)).apply(s2): ((A, C));
      (if ((has_next[A, B](g1)).apply(s1))
        {
          val (x, s1a) = (next[A, B](g1)).apply(s1): ((A, B));
          (if ((less(x))(y)) false
            else (if ((less(y))(x))
                   (proper_interval(Some[A](y)))(Some[A](x)) ||
                     ((has_next[A, C](g2)).apply(s2a) ||
                       ! (exhaustive_above_fusion[A,
           B](proper_interval, g1, x, s1a)))
                   else proper_interval_set_aux_fusion[A, B,
                C](less, proper_interval, g1, g2, s1a, s2a)))
        }
        else (has_next[A, C](g2)).apply(s2a) ||
               (proper_interval(Some[A](y)))(None))
    }

def length_last[A](x0: List[A]): (nat, A) = x0 match {
  case x :: xs =>
    fold[A, (nat, A)](((xa: A) => (a: (nat, A)) =>
                        {
                          val (n, _) = a: ((nat, A));
                          (plus_nat(n, one_nata), xa)
                        }),
                       xs, (one_nata, x))
  case Nil => (zero_nat, sys.error("undefined"))
}

def proper_interval_set_compl_aux[A : card_univ : ceq : ccompare : set_impl](less:
                                       A => A => Boolean,
                                      proper_interval:
Option[A] => Option[A] => Boolean,
                                      ao: Option[A], n: nat, x4: List[A],
                                      x5: List[A]):
      Boolean
  =
  (less, proper_interval, ao, n, x4, x5) match {
  case (less, proper_interval, ao, n, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      (proper_interval(ao))(Some[A](x)) ||
        proper_interval_set_compl_aux[A](less, proper_interval, Some[A](x),
  plus_nat(n, one_nata), xs, y :: ys)
      else (if ((less(y))(x))
             (proper_interval(ao))(Some[A](y)) ||
               proper_interval_set_compl_aux[A](less, proper_interval,
         Some[A](y), plus_nat(n, one_nata), x :: xs, ys)
             else (proper_interval(ao))(Some[A](x)) &&
                    {
                      val m = minus_nat(card[A](top_set[A]), n): nat;
                      ! (equal_nata(minus_nat(m, size_list[A].apply(ys)),
                                     nat_of_integer(BigInt(2)))) ||
                        ! (equal_nata(minus_nat(m, size_list[A].apply(xs)),
                                       nat_of_integer(BigInt(2))))
                    }))
  case (less, proper_interval, ao, n, x :: xs, Nil) =>
    {
      val m = minus_nat(card[A](top_set[A]), n): nat
      val (len_x, xa) = length_last[A](x :: xs): ((nat, A));
      ! (equal_nata(m, len_x)) &&
        (if (equal_nata(m, plus_nat(len_x, one_nata)))
          ! ((proper_interval(Some[A](xa)))(None)) else true)
    }
  case (less, proper_interval, ao, n, Nil, y :: ys) =>
    {
      val m = minus_nat(card[A](top_set[A]), n): nat
      val (len_y, ya) = length_last[A](y :: ys): ((nat, A));
      ! (equal_nata(m, len_y)) &&
        (if (equal_nata(m, plus_nat(len_y, one_nata)))
          ! ((proper_interval(Some[A](ya)))(None)) else true)
    }
  case (less, proper_interval, ao, n, Nil, Nil) =>
    less_nat(plus_nat(n, one_nata), card[A](top_set[A]))
}

def proper_interval_compl_set_aux[A](less: A => A => Boolean,
                                      proper_interval:
Option[A] => Option[A] => Boolean,
                                      ao: Option[A], uu: List[A], uv: List[A]):
      Boolean
  =
  (less, proper_interval, ao, uu, uv) match {
  case (less, proper_interval, ao, uu, Nil) => false
  case (less, proper_interval, ao, Nil, uv) => false
  case (less, proper_interval, ao, x :: xs, y :: ys) =>
    (if ((less(x))(y))
      ! ((proper_interval(ao))(Some[A](x))) &&
        proper_interval_compl_set_aux[A](less, proper_interval, Some[A](x), xs,
  y :: ys)
      else (if ((less(y))(x))
             ! ((proper_interval(ao))(Some[A](y))) &&
               proper_interval_compl_set_aux[A](less, proper_interval,
         Some[A](y), x :: xs, ys)
             else ! ((proper_interval(ao))(Some[A](x))) &&
                    (if (nulla[A](ys)) ! (nulla[A](xs)) else true)))
}

def exhaustive_above[A](proper_interval: Option[A] => Option[A] => Boolean,
                         x: A, xa2: List[A]):
      Boolean
  =
  (proper_interval, x, xa2) match {
  case (proper_interval, x, y :: ys) =>
    ! ((proper_interval(Some[A](x)))(Some[A](y))) &&
      exhaustive_above[A](proper_interval, y, ys)
  case (proper_interval, x, Nil) => ! ((proper_interval(Some[A](x)))(None))
}

def proper_interval_set_aux[A](less: A => A => Boolean,
                                proper_interval:
                                  Option[A] => Option[A] => Boolean,
                                xs: List[A], x3: List[A]):
      Boolean
  =
  (less, proper_interval, xs, x3) match {
  case (less, proper_interval, x :: xs, y :: ys) =>
    (if ((less(x))(y)) false
      else (if ((less(y))(x))
             (proper_interval(Some[A](y)))(Some[A](x)) ||
               (! (nulla[A](ys)) ||
                 ! (exhaustive_above[A](proper_interval, x, xs)))
             else proper_interval_set_aux[A](less, proper_interval, xs, ys)))
  case (less, proper_interval, Nil, y :: ys) =>
    ! (nulla[A](ys)) || (proper_interval(Some[A](y)))(None)
  case (less, proper_interval, xs, Nil) => false
}

def exhaustive_fusion[A, B](proper_interval: Option[A] => Option[A] => Boolean,
                             g: generator[A, B], s: B):
      Boolean
  =
  (has_next[A, B](g)).apply(s) &&
    {
      val (x, sa) = (next[A, B](g)).apply(s): ((A, B));
      ! ((proper_interval(None))(Some[A](x))) &&
        exhaustive_above_fusion[A, B](proper_interval, g, x, sa)
    }

def is_univ[A : card_univ : ceq : cproper_interval](a: set[A]): Boolean = a
  match {
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("is_UNIV RBT_set: ccompare = None");
           (((_: Unit) => is_univ[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) =>
         of_phantom[A, Boolean](finite_univ[A]) &&
           exhaustive_fusion[A, (List[(A, rbt[A, Unit])],
                                  rbt[A, Unit])](((a: Option[A]) =>
           (b: Option[A]) => cproper_interval[A](a, b)),
          rbt_keys_generator[A, Unit], init[A, Unit, A](rbt))
     })
  case a =>
    {
      val aa = of_phantom[A, nat](card_univ[A]): nat
      val b = card[A](a): nat;
      (if (less_nat(zero_nat, aa)) equal_nata(aa, b)
        else (if (less_nat(zero_nat, b)) false
               else { sys.error("is_UNIV called on infinite type and set");
                      (((_: Unit) => is_univ[A](a))).apply(()) }))
    }
}

def is_emptya[A : ccompare, B](xa: mapping_rbt[A, B]): Boolean =
  (impl_ofa[A, B](xa) match {
     case Emptyd() => true
     case Branch(_, _, _, _, _) => false
   })

def nullb[A : ceq](xa: set_dlist[A]): Boolean = nulla[A](list_of_dlist[A](xa))

def is_empty[A : card_univ : ceq : cproper_interval](x0: set[A]): Boolean = x0
  match {
  case Complement(a) => is_univ[A](a)
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("is_empty RBT_set: ccompare = None");
           (((_: Unit) => is_empty[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => is_emptya[A, Unit](rbt)
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("is_empty DList_set: ceq = None");
           (((_: Unit) => is_empty[A](Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => nullb[A](dxs)
     })
  case Set_monad(xs) => nulla[A](xs)
}

def cproper_interval_seta[A : card_univ : ceq : cproper_interval : set_impl](x0:
                                       Option[set[A]],
                                      x1: Option[set[A]]):
      Boolean
  =
  (x0, x1) match {
  case (Some(Complement(Rbt_set(rbt1))), Some(Rbt_set(rbt2))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval (Complement RBT_set) RBT_set: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](Complement[A](Rbt_set[A](rbt1))),
Some[set[A]](Rbt_set[A](rbt2))))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_compl_set_aux_fusion[A,
         (List[(A, rbt[A, Unit])], rbt[A, Unit]),
         (List[(A, rbt[A, Unit])],
           rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                           ((a: Option[A]) => (b: Option[A]) =>
                             cproper_interval[A](a, b)),
                           rbt_keys_generator[A, Unit],
                           rbt_keys_generator[A, Unit], None,
                           init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
     })
  case (Some(Rbt_set(rbt1)), Some(Complement(Rbt_set(rbt2)))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval RBT_set (Complement RBT_set): ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](Rbt_set[A](rbt1)),
Some[set[A]](Complement[A](Rbt_set[A](rbt2)))))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_set_compl_aux_fusion[A,
         (List[(A, rbt[A, Unit])], rbt[A, Unit]),
         (List[(A, rbt[A, Unit])],
           rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                           ((a: Option[A]) => (b: Option[A]) =>
                             cproper_interval[A](a, b)),
                           rbt_keys_generator[A, Unit],
                           rbt_keys_generator[A, Unit], None, zero_nat,
                           init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
     })
  case (Some(Rbt_set(rbt1)), Some(Rbt_set(rbt2))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](Rbt_set[A](rbt1)),
Some[set[A]](Rbt_set[A](rbt2))))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_set_aux_fusion[A,
   (List[(A, rbt[A, Unit])], rbt[A, Unit]),
   (List[(A, rbt[A, Unit])],
     rbt[A, Unit])](((a: A) => (b: A) => lt_of_comp[A](c, a, b)),
                     ((a: Option[A]) => (b: Option[A]) =>
                       cproper_interval[A](a, b)),
                     rbt_keys_generator[A, Unit], rbt_keys_generator[A, Unit],
                     init[A, Unit, A](rbt1), init[A, Unit, A](rbt2))
     })
  case (Some(Complement(a)), Some(Complement(b))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval Complement Complement: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](Complement[A](a)),
Some[set[A]](Complement[A](b))))).apply(())
           }
       case Some(_) =>
         cproper_interval_seta[A](Some[set[A]](b), Some[set[A]](a))
     })
  case (Some(Complement(a)), Some(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval Complement1: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](Complement[A](a)),
Some[set[A]](b)))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_compl_set_aux[A](((aa: A) => (ba: A) =>
      lt_of_comp[A](c, aa, ba)),
     ((aa: Option[A]) => (ba: Option[A]) => cproper_interval[A](aa, ba)), None,
     csorted_list_of_set[A](a), csorted_list_of_set[A](b))
     })
  case (Some(a), Some(Complement(b))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval Complement2: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](a),
Some[set[A]](Complement[A](b))))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_set_compl_aux[A](((aa: A) => (ba: A) =>
      lt_of_comp[A](c, aa, ba)),
     ((aa: Option[A]) => (ba: Option[A]) => cproper_interval[A](aa, ba)), None,
     zero_nat, csorted_list_of_set[A](a), csorted_list_of_set[A](b))
     })
  case (Some(a), Some(b)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("cproper_interval: ccompare = None");
           (((_: Unit) =>
              cproper_interval_seta[A](Some[set[A]](a),
Some[set[A]](b)))).apply(())
           }
       case Some(c) =>
         finite[A](top_set[A]) &&
           proper_interval_set_aux[A](((aa: A) => (ba: A) =>
lt_of_comp[A](c, aa, ba)),
                                       ((aa: Option[A]) => (ba: Option[A]) =>
 cproper_interval[A](aa, ba)),
                                       csorted_list_of_set[A](a),
                                       csorted_list_of_set[A](b))
     })
  case (Some(a), None) => ! (is_univ[A](a))
  case (None, Some(b)) => ! (is_empty[A](b))
  case (None, None) => true
}

def equal_boola(p: Boolean, pa: Boolean): Boolean = (p, pa) match {
  case (p, true) => p
  case (p, false) => ! p
  case (true, p) => p
  case (false, p) => ! p
}

def less_eq_bool(x0: Boolean, b: Boolean): Boolean = (x0, b) match {
  case (true, b) => b
  case (false, b) => true
}

def less_bool(x0: Boolean, b: Boolean): Boolean = (x0, b) match {
  case (true, b) => false
  case (false, b) => b
}

def ceq_boola: Option[Boolean => Boolean => Boolean] =
  Some[Boolean =>
         Boolean =>
           Boolean](((a: Boolean) => (b: Boolean) => equal_boola(a, b)))

def set_impl_boola: phantom[Boolean, set_impla] =
  phantoma[set_impla, Boolean](set_dlista())

def finite_univ_boola: phantom[Boolean, Boolean] =
  phantoma[Boolean, Boolean](true)

def card_univ_boola: phantom[Boolean, nat] =
  phantoma[nat, Boolean](nat_of_integer(BigInt(2)))

def enum_all_bool(p: Boolean => Boolean): Boolean = p(false) && p(true)

def enum_ex_bool(p: Boolean => Boolean): Boolean = p(false) || p(true)

def enum_bool: List[Boolean] = List(false, true)

def cenum_boola:
      Option[(List[Boolean],
               ((Boolean => Boolean) => Boolean,
                 (Boolean => Boolean) => Boolean))]
  =
  Some[(List[Boolean],
         ((Boolean => Boolean) => Boolean,
           (Boolean => Boolean) =>
             Boolean))]((enum_bool,
                          (((a: Boolean => Boolean) => enum_all_bool(a)),
                            ((a: Boolean => Boolean) => enum_ex_bool(a)))))

def comparator_bool(x0: Boolean, x1: Boolean): ordera = (x0, x1) match {
  case (false, false) => Eqa()
  case (false, true) => Lt()
  case (true, true) => Eqa()
  case (true, false) => Gt()
}

def compare_bool: Boolean => Boolean => ordera =
  ((a: Boolean) => (b: Boolean) => comparator_bool(a, b))

def ccompare_boola: Option[Boolean => Boolean => ordera] =
  Some[Boolean => Boolean => ordera](compare_bool)

def mapping_impl_boola: phantom[Boolean, mapping_impla] =
  phantoma[mapping_impla, Boolean](mapping_assoc_list())

def proper_interval_bool(x0: Option[Boolean], x1: Option[Boolean]): Boolean =
  (x0, x1) match {
  case (Some(x), Some(y)) => false
  case (Some(x), None) => ! x
  case (None, Some(y)) => y
  case (None, None) => true
}

def cproper_interval_boola: Option[Boolean] => Option[Boolean] => Boolean =
  ((a: Option[Boolean]) => (b: Option[Boolean]) => proper_interval_bool(a, b))

abstract sealed class fset[A]
final case class Abs_fset[A](a: set[A]) extends fset[A]

def fset[A](x0: fset[A]): set[A] = x0 match {
  case Abs_fset(x) => x
}

def less_eq_fset[A : cenum : ceq : ccompare](xa: fset[A], xc: fset[A]): Boolean
  =
  less_eq_set[A](fset[A](xa), fset[A](xc))

def equal_fseta[A : cenum : ceq : ccompare : equal](a: fset[A], b: fset[A]):
      Boolean
  =
  less_eq_fset[A](a, b) && less_eq_fset[A](b, a)

def ceq_fseta[A : cenum : ceq : ccompare : equal]:
      Option[(fset[A]) => (fset[A]) => Boolean]
  =
  Some[(fset[A]) =>
         (fset[A]) =>
           Boolean](((a: fset[A]) => (b: fset[A]) => eq[fset[A]](a, b)))

def set_impl_fseta[A]: phantom[fset[A], set_impla] =
  phantoma[set_impla, fset[A]](set_dlista())

def finite_univ_fseta[A : finite_univ]: phantom[fset[A], Boolean] =
  phantoma[Boolean, fset[A]](of_phantom[A, Boolean](finite_univ[A]))

def card_univ_fseta[A : card_univ]: phantom[fset[A], nat] =
  phantoma[nat, fset[A]]({
                           val c = of_phantom[A, nat](card_univ[A]): nat;
                           (if (equal_nata(c, zero_nat)) zero_nat
                             else power[nat](nat_of_integer(BigInt(2)), c))
                         })

def cenum_fseta[A]:
      Option[(List[fset[A]],
               (((fset[A]) => Boolean) => Boolean,
                 ((fset[A]) => Boolean) => Boolean))]
  =
  None

def ccompare_fseta[A]: Option[(fset[A]) => (fset[A]) => ordera] = None

def mapping_impl_fseta[A]: phantom[fset[A], mapping_impla] =
  phantoma[mapping_impla, fset[A]](mapping_choose())

trait infinite_univ[A] {
}
object infinite_univ {
  implicit def `GeneratedCode.infinite_univ_integer`: infinite_univ[BigInt] =
    new infinite_univ[BigInt] {
  }
}

def cproper_interval_fseta[A : infinite_univ](uu: Option[fset[A]],
       uv: Option[fset[A]]):
      Boolean
  =
  sys.error("undefined")

def equal_lista[A : equal](x0: List[A], x1: List[A]): Boolean = (x0, x1) match {
  case (Nil, x21 :: x22) => false
  case (x21 :: x22, Nil) => false
  case (x21 :: x22, y21 :: y22) => eq[A](x21, y21) && equal_lista[A](x22, y22)
  case (Nil, Nil) => true
}

def less_eq_list[A : equal : order](x0: List[A], xs: List[A]): Boolean =
  (x0, xs) match {
  case (x :: xs, y :: ys) =>
    less[A](x, y) || eq[A](x, y) && less_eq_list[A](xs, ys)
  case (Nil, xs) => true
  case (x :: xs, Nil) => false
}

def less_list[A : equal : order](xs: List[A], x1: List[A]): Boolean = (xs, x1)
  match {
  case (x :: xs, y :: ys) =>
    less[A](x, y) || eq[A](x, y) && less_list[A](xs, ys)
  case (Nil, x :: xs) => true
  case (xs, Nil) => false
}

def equality_list[A](eq_a: A => A => Boolean, x1: List[A], x2: List[A]): Boolean
  =
  (eq_a, x1, x2) match {
  case (eq_a, x :: xa, y :: ya) =>
    (eq_a(x))(y) && equality_list[A](eq_a, xa, ya)
  case (eq_a, x :: xa, Nil) => false
  case (eq_a, Nil, y :: ya) => false
  case (eq_a, Nil, Nil) => true
}

def ceq_lista[A : ceq]: Option[(List[A]) => (List[A]) => Boolean] =
  (ceq[A] match {
     case None => None
     case Some(eq_a) =>
       Some[(List[A]) =>
              (List[A]) =>
                Boolean](((a: List[A]) => (b: List[A]) =>
                           equality_list[A](eq_a, a, b)))
   })

def set_impl_lista[A]: phantom[List[A], set_impla] =
  phantoma[set_impla, List[A]](set_choose())

def finite_univ_lista[A]: phantom[List[A], Boolean] =
  phantoma[Boolean, List[A]](false)

def card_univ_lista[A]: phantom[List[A], nat] = phantoma[nat, List[A]](zero_nat)

def cenum_lista[A]:
      Option[(List[List[A]],
               (((List[A]) => Boolean) => Boolean,
                 ((List[A]) => Boolean) => Boolean))]
  =
  None

def comparator_list[A](comp_a: A => A => ordera, x1: List[A], x2: List[A]):
      ordera
  =
  (comp_a, x1, x2) match {
  case (comp_a, x :: xa, y :: ya) =>
    ((comp_a(x))(y) match {
       case Eqa() => comparator_list[A](comp_a, xa, ya)
       case Lt() => Lt()
       case Gt() => Gt()
     })
  case (comp_a, x :: xa, Nil) => Gt()
  case (comp_a, Nil, y :: ya) => Lt()
  case (comp_a, Nil, Nil) => Eqa()
}

def ccompare_lista[A : ccompare]: Option[(List[A]) => (List[A]) => ordera] =
  (ccompare[A] match {
     case None => None
     case Some(comp_a) =>
       Some[(List[A]) =>
              (List[A]) =>
                ordera](((a: List[A]) => (b: List[A]) =>
                          comparator_list[A](comp_a, a, b)))
   })

def mapping_impl_lista[A]: phantom[List[A], mapping_impla] =
  phantoma[mapping_impla, List[A]](mapping_choose())

def cproper_interval_lista[A : ccompare](xso: Option[List[A]],
  yso: Option[List[A]]):
      Boolean
  =
  sys.error("undefined")

abstract sealed class sum[A, B]
final case class Inl[A, B](a: A) extends sum[A, B]
final case class Inr[B, A](a: B) extends sum[A, B]

def equal_suma[A : equal, B : equal](x0: sum[A, B], x1: sum[A, B]): Boolean =
  (x0, x1) match {
  case (Inl(x1), Inr(x2)) => false
  case (Inr(x2), Inl(x1)) => false
  case (Inr(x2), Inr(y2)) => eq[B](x2, y2)
  case (Inl(x1), Inl(y1)) => eq[A](x1, y1)
}

def less_eq_sum[A : ord, B : ord](x0: sum[A, B], x1: sum[A, B]): Boolean =
  (x0, x1) match {
  case (Inl(a), Inl(b)) => less_eq[A](a, b)
  case (Inl(a), Inr(b)) => true
  case (Inr(a), Inl(b)) => false
  case (Inr(a), Inr(b)) => less_eq[B](a, b)
}

def less_sum[A : equal : ord, B : equal : ord](a: sum[A, B], b: sum[A, B]):
      Boolean
  =
  less_eq_sum[A, B](a, b) && ! (equal_suma[A, B](a, b))

def equality_sum[A, B](eq_a: A => A => Boolean, eq_b: B => B => Boolean,
                        x2: sum[A, B], x3: sum[A, B]):
      Boolean
  =
  (eq_a, eq_b, x2, x3) match {
  case (eq_a, eq_b, Inr(x), Inr(ya)) => (eq_b(x))(ya)
  case (eq_a, eq_b, Inr(x), Inl(y)) => false
  case (eq_a, eq_b, Inl(x), Inr(ya)) => false
  case (eq_a, eq_b, Inl(x), Inl(y)) => (eq_a(x))(y)
}

def ceq_suma[A : ceq, B : ceq]: Option[(sum[A, B]) => (sum[A, B]) => Boolean] =
  (ceq[A] match {
     case None => None
     case Some(eq_a) =>
       (ceq[B] match {
          case None => None
          case Some(eq_b) =>
            Some[(sum[A, B]) =>
                   (sum[A, B]) =>
                     Boolean](((a: sum[A, B]) => (b: sum[A, B]) =>
                                equality_sum[A, B](eq_a, eq_b, a, b)))
        })
   })

def set_impl_choose2(x: set_impla, y: set_impla): set_impla = (x, y) match {
  case (set_monada(), set_monada()) => set_monada()
  case (set_rbt(), set_rbt()) => set_rbt()
  case (set_dlista(), set_dlista()) => set_dlista()
  case (set_collect(), set_collect()) => set_collect()
  case (x, y) => set_choose()
}

def set_impl_suma[A : set_impl, B : set_impl]: phantom[sum[A, B], set_impla] =
  phantoma[set_impla,
            sum[A, B]](set_impl_choose2(of_phantom[A, set_impla](set_impl[A]),
 of_phantom[B, set_impla](set_impl[B])))

def finite_univ_suma[A : finite_univ, B : finite_univ]:
      phantom[sum[A, B], Boolean]
  =
  phantoma[Boolean,
            sum[A, B]](of_phantom[A, Boolean](finite_univ[A]) &&
                         of_phantom[B, Boolean](finite_univ[B]))

def card_univ_suma[A : card_univ, B : card_univ]: phantom[sum[A, B], nat] =
  phantoma[nat, sum[A, B]]({
                             val ca = of_phantom[A, nat](card_univ[A]): nat
                             val cb = of_phantom[B, nat](card_univ[B]): nat;
                             (if (! (equal_nata(ca, zero_nat)) &&
                                    ! (equal_nata(cb, zero_nat)))
                               plus_nat(ca, cb) else zero_nat)
                           })

def cenum_suma[A : cenum, B : cenum]:
      Option[(List[sum[A, B]],
               (((sum[A, B]) => Boolean) => Boolean,
                 ((sum[A, B]) => Boolean) => Boolean))]
  =
  (cenum[A] match {
     case None => None
     case Some((enum_a, (enum_all_a, enum_ex_a))) =>
       (cenum[B] match {
          case None => None
          case Some((enum_b, (enum_all_b, enum_ex_b))) =>
            Some[(List[sum[A, B]],
                   (((sum[A, B]) => Boolean) => Boolean,
                     ((sum[A, B]) => Boolean) =>
                       Boolean))]((mapa[A,
 sum[A, B]](((a: A) => Inl[A, B](a)), enum_a) ++
                                     mapa[B,
   sum[A, B]](((a: B) => Inr[B, A](a)), enum_b),
                                    (((p: (sum[A, B]) => Boolean) =>
                                       enum_all_a(((x: A) =>
            p(Inl[A, B](x)))) &&
 enum_all_b(((x: B) => p(Inr[B, A](x))))),
                                      ((p: (sum[A, B]) => Boolean) =>
enum_ex_a(((x: A) => p(Inl[A, B](x)))) ||
  enum_ex_b(((x: B) => p(Inr[B, A](x))))))))
        })
   })

def comparator_sum[A, B](comp_a: A => A => ordera, comp_b: B => B => ordera,
                          x2: sum[A, B], x3: sum[A, B]):
      ordera
  =
  (comp_a, comp_b, x2, x3) match {
  case (comp_a, comp_b, Inr(x), Inr(ya)) => (comp_b(x))(ya)
  case (comp_a, comp_b, Inr(x), Inl(y)) => Gt()
  case (comp_a, comp_b, Inl(x), Inr(ya)) => Lt()
  case (comp_a, comp_b, Inl(x), Inl(y)) => (comp_a(x))(y)
}

def ccompare_suma[A : ccompare, B : ccompare]:
      Option[(sum[A, B]) => (sum[A, B]) => ordera]
  =
  (ccompare[A] match {
     case None => None
     case Some(comp_a) =>
       (ccompare[B] match {
          case None => None
          case Some(comp_b) =>
            Some[(sum[A, B]) =>
                   (sum[A, B]) =>
                     ordera](((a: sum[A, B]) => (b: sum[A, B]) =>
                               comparator_sum[A, B](comp_a, comp_b, a, b)))
        })
   })

def mapping_impl_choose2(x: mapping_impla, y: mapping_impla): mapping_impla =
  (x, y) match {
  case (mapping_rbta(), mapping_rbta()) => mapping_rbta()
  case (mapping_assoc_list(), mapping_assoc_list()) => mapping_assoc_list()
  case (mapping_mapping(), mapping_mapping()) => mapping_mapping()
  case (x, y) => mapping_choose()
}

def mapping_impl_suma[A : mapping_impl, B : mapping_impl]:
      phantom[sum[A, B], mapping_impla]
  =
  phantoma[mapping_impla,
            sum[A, B]](mapping_impl_choose2(of_phantom[A,
                mapping_impla](mapping_impl[A]),
     of_phantom[B, mapping_impla](mapping_impl[B])))

def cproper_interval_suma[A : cproper_interval,
                           B : cproper_interval](x0: Option[sum[A, B]],
          x1: Option[sum[A, B]]):
      Boolean
  =
  (x0, x1) match {
  case (None, None) => true
  case (None, Some(Inl(x))) => cproper_interval[A](None, Some[A](x))
  case (None, Some(Inr(y))) => true
  case (Some(Inl(x)), None) => true
  case (Some(Inl(x)), Some(Inl(y))) =>
    cproper_interval[A](Some[A](x), Some[A](y))
  case (Some(Inl(x)), Some(Inr(y))) =>
    cproper_interval[A](Some[A](x), None) ||
      cproper_interval[B](None, Some[B](y))
  case (Some(Inr(y)), None) => cproper_interval[B](Some[B](y), None)
  case (Some(Inr(y)), Some(Inl(x))) => false
  case (Some(Inr(x)), Some(Inr(y))) =>
    cproper_interval[B](Some[B](x), Some[B](y))
}

def equality_option[A](eq_a: A => A => Boolean, x1: Option[A], x2: Option[A]):
      Boolean
  =
  (eq_a, x1, x2) match {
  case (eq_a, Some(x), Some(y)) => (eq_a(x))(y)
  case (eq_a, Some(x), None) => false
  case (eq_a, None, Some(y)) => false
  case (eq_a, None, None) => true
}

def ceq_optiona[A : ceq]: Option[Option[A] => Option[A] => Boolean] =
  (ceq[A] match {
     case None => None
     case Some(eq_a) =>
       Some[Option[A] =>
              Option[A] =>
                Boolean](((a: Option[A]) => (b: Option[A]) =>
                           equality_option[A](eq_a, a, b)))
   })

def set_impl_optiona[A : set_impl]: phantom[Option[A], set_impla] =
  phantoma[set_impla, Option[A]](of_phantom[A, set_impla](set_impl[A]))

def comparator_option[A](comp_a: A => A => ordera, x1: Option[A],
                          x2: Option[A]):
      ordera
  =
  (comp_a, x1, x2) match {
  case (comp_a, Some(x), Some(y)) => (comp_a(x))(y)
  case (comp_a, Some(x), None) => Gt()
  case (comp_a, None, Some(y)) => Lt()
  case (comp_a, None, None) => Eqa()
}

def ccompare_optiona[A : ccompare]: Option[Option[A] => Option[A] => ordera] =
  (ccompare[A] match {
     case None => None
     case Some(comp_a) =>
       Some[Option[A] =>
              Option[A] =>
                ordera](((a: Option[A]) => (b: Option[A]) =>
                          comparator_option[A](comp_a, a, b)))
   })

def equal_proda[A : equal, B : equal](x0: (A, B), x1: (A, B)): Boolean =
  (x0, x1) match {
  case ((x1, x2), (y1, y2)) => eq[A](x1, y1) && eq[B](x2, y2)
}

def less_eq_prod[A : ord, B : ord](x0: (A, B), x1: (A, B)): Boolean = (x0, x1)
  match {
  case ((x1, y1), (x2, y2)) =>
    less[A](x1, x2) || less_eq[A](x1, x2) && less_eq[B](y1, y2)
}

def less_prod[A : ord, B : ord](x0: (A, B), x1: (A, B)): Boolean = (x0, x1)
  match {
  case ((x1, y1), (x2, y2)) =>
    less[A](x1, x2) || less_eq[A](x1, x2) && less[B](y1, y2)
}

def set_impl_proda[A : set_impl, B : set_impl]: phantom[(A, B), set_impla] =
  phantoma[set_impla,
            (A, B)](set_impl_choose2(of_phantom[A, set_impla](set_impl[A]),
                                      of_phantom[B, set_impla](set_impl[B])))

def finite_univ_proda[A : finite_univ, B : finite_univ]:
      phantom[(A, B), Boolean]
  =
  phantoma[Boolean,
            (A, B)](of_phantom[A, Boolean](finite_univ[A]) &&
                      of_phantom[B, Boolean](finite_univ[B]))

def card_univ_proda[A : card_univ, B : card_univ]: phantom[(A, B), nat] =
  phantoma[nat, (A, B)](times_nata(of_phantom[A, nat](card_univ[A]),
                                    of_phantom[B, nat](card_univ[B])))

def mapping_impl_proda[A : mapping_impl, B : mapping_impl]:
      phantom[(A, B), mapping_impla]
  =
  phantoma[mapping_impla,
            (A, B)](mapping_impl_choose2(of_phantom[A,
             mapping_impla](mapping_impl[A]),
  of_phantom[B, mapping_impla](mapping_impl[B])))

def cproper_interval_proda[A : cproper_interval,
                            B : cproper_interval](x0: Option[(A, B)],
           x1: Option[(A, B)]):
      Boolean
  =
  (x0, x1) match {
  case (None, None) => true
  case (None, Some((y1, y2))) =>
    cproper_interval[A](None, Some[A](y1)) ||
      cproper_interval[B](None, Some[B](y2))
  case (Some((x1, x2)), None) =>
    cproper_interval[A](Some[A](x1), None) ||
      cproper_interval[B](Some[B](x2), None)
  case (Some((x1, x2)), Some((y1, y2))) =>
    cproper_interval[A](Some[A](x1), Some[A](y1)) ||
      (lt_of_comp[A](the[A => A => ordera](ccompare[A]), x1, y1) &&
         (cproper_interval[B](Some[B](x2), None) ||
           cproper_interval[B](None, Some[B](y2))) ||
        ! (lt_of_comp[A](the[A => A => ordera](ccompare[A]), y1, x1)) &&
          cproper_interval[B](Some[B](x2), Some[B](y2)))
}

def ceq_integera: Option[BigInt => BigInt => Boolean] =
  Some[BigInt => BigInt => Boolean](((a: BigInt) => (b: BigInt) => a == b))

def set_impl_integera: phantom[BigInt, set_impla] =
  phantoma[set_impla, BigInt](set_rbt())

def finite_univ_integera: phantom[BigInt, Boolean] =
  phantoma[Boolean, BigInt](false)

def card_univ_integera: phantom[BigInt, nat] = phantoma[nat, BigInt](zero_nat)

def cenum_integera:
      Option[(List[BigInt],
               ((BigInt => Boolean) => Boolean,
                 (BigInt => Boolean) => Boolean))]
  =
  None

def compare_integer: BigInt => BigInt => ordera =
  ((a: BigInt) => (b: BigInt) => comparator_of[BigInt](a, b))

def ccompare_integera: Option[BigInt => BigInt => ordera] =
  Some[BigInt => BigInt => ordera](compare_integer)

def mapping_impl_integera: phantom[BigInt, mapping_impla] =
  phantoma[mapping_impla, BigInt](mapping_rbta())

def proper_interval_integer(xo: Option[BigInt], yo: Option[BigInt]): Boolean =
  (xo, yo) match {
  case (xo, None) => true
  case (None, yo) => true
  case (Some(x), Some(y)) => BigInt(1) < y - x
}

def cproper_interval_integera: Option[BigInt] => Option[BigInt] => Boolean =
  ((a: Option[BigInt]) => (b: Option[BigInt]) => proper_interval_integer(a, b))

abstract sealed class int
final case class int_of_integer(a: BigInt) extends int

abstract sealed class test_suite[A, B, C, D]
final case class Test_suitea[A, B, C,
                              D](a: set[(A, fsm[A, B, C])],
                                  b: A => set[List[(A, (B, (C, A)))]],
                                  c: ((A, List[(A, (B, (C, A)))])) => set[A],
                                  d: ((A, A)) => set[(fsm[D, B, C], (D, D))])
  extends test_suite[A, B, C, D]

abstract sealed class comp_fun_idem[B, A]
final case class Abs_comp_fun_idem[B, A](a: B => A => A) extends
  comp_fun_idem[B, A]

abstract sealed class prefix_tree[A]
final case class MPT[A](a: mapping[A, prefix_tree[A]]) extends prefix_tree[A]

abstract sealed class mp_trie[A]
final case class Mp_triea[A](a: List[(A, mp_trie[A])]) extends mp_trie[A]

def impl_of[B, A](x0: alist[B, A]): List[(B, A)] = x0 match {
  case Alista(x) => x
}

def update[A : equal, B](k: A, v: B, x2: List[(A, B)]): List[(A, B)] =
  (k, v, x2) match {
  case (k, v, Nil) => List((k, v))
  case (k, v, p :: ps) =>
    (if (eq[A](fst[A, B](p), k)) (k, v) :: ps else p :: update[A, B](k, v, ps))
}

def updatea[A : equal, B](xc: A, xd: B, xe: alist[A, B]): alist[A, B] =
  Alista[A, B](update[A, B](xc, xd, impl_of[A, B](xe)))

def fun_upd[A : equal, B](f: A => B, a: A, b: B): A => B =
  ((x: A) => (if (eq[A](x, a)) b else f(x)))

def updateb[A : ccompare : equal, B](k: A, v: B, x2: mapping[A, B]):
      mapping[A, B]
  =
  (k, v, x2) match {
  case (k, v, Rbt_mapping(t)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("update RBT_Mapping: ccompare = None");
           (((_: Unit) => updateb[A, B](k, v, Rbt_mapping[A, B](t)))).apply(())
           }
       case Some(_) => Rbt_mapping[A, B](insertc[A, B](k, v, t))
     })
  case (k, v, Assoc_list_mapping(al)) =>
    Assoc_list_mapping[A, B](updatea[A, B](k, v, al))
  case (k, v, Mappinga(m)) =>
    Mappinga[A, B](fun_upd[A, Option[B]](m, k, Some[B](v)))
}

def map_of[A : equal, B](x0: List[(A, B)], k: A): Option[B] = (x0, k) match {
  case ((l, v) :: ps, k) =>
    (if (eq[A](l, k)) Some[B](v) else map_of[A, B](ps, k))
  case (Nil, k) => None
}

def lookup[A : equal, B](xa: alist[A, B]): A => Option[B] =
  ((a: A) => map_of[A, B](impl_of[A, B](xa), a))

def lookupa[A : ccompare : equal, B](x0: mapping[A, B]): A => Option[B] = x0
  match {
  case Rbt_mapping(t) => lookupb[A, B](t)
  case Assoc_list_mapping(al) => lookup[A, B](al)
}

def foldb[A : ccompare, B](x: A => B => B, xc: mapping_rbt[A, Unit]): B => B =
  ((a: B) =>
    folda[A, Unit,
           B](((aa: A) => (_: Unit) => x(aa)), impl_ofa[A, Unit](xc), a))

def empty[A, B]: alist[A, B] = Alista[A, B](Nil)

def mapping_empty_choose[A : ccompare, B]: mapping[A, B] =
  (ccompare[A] match {
     case None => Assoc_list_mapping[A, B](empty[A, B])
     case Some(_) => Rbt_mapping[A, B](emptyd[A, B])
   })

def mapping_empty[A : ccompare, B](x0: mapping_impla): mapping[A, B] = x0 match
  {
  case mapping_rbta() => Rbt_mapping[A, B](emptyd[A, B])
  case mapping_assoc_list() => Assoc_list_mapping[A, B](empty[A, B])
  case mapping_mapping() => Mappinga[A, B](((_: A) => None))
  case mapping_choose() => mapping_empty_choose[A, B]
}

def emptya[A : ccompare : mapping_impl, B]: mapping[A, B] =
  mapping_empty[A, B](of_phantom[A, mapping_impla](mapping_impl[A]))

def set_as_map_image[A : ccompare, B : ccompare,
                      C : ccompare : equal : mapping_impl,
                      D : ceq : ccompare : set_impl](x0: set[(A, B)],
              f1: ((A, B)) => (C, D)):
      C => Option[set[D]]
  =
  (x0, f1) match {
  case (Rbt_set(t), f1) =>
    (ccompare_proda[A, B] match {
       case None =>
         { sys.error("set_as_map_image RBT_set: ccompare = None");
           (((_: Unit) =>
              set_as_map_image[A, B, C, D](Rbt_set[(A, B)](t), f1))).apply(())
           }
       case Some(_) =>
         lookupa[C, set[D]]((foldb[(A, B),
                                    mapping[C,
     set[D]]](((kv: (A, B)) => (m1: mapping[C, set[D]]) =>
                {
                  val (x, z) = f1(kv): ((C, D));
                  ((lookupa[C, set[D]](m1)).apply(x) match {
                     case None =>
                       updateb[C, set[D]](x, insert[D](z, bot_set[D]), m1)
                     case Some(zs) =>
                       updateb[C, set[D]](x, insert[D](z, zs), m1)
                   })
                }),
               t)).apply(emptya[C, set[D]]))
     })
}

def h[A : ceq : ccompare : equal : mapping_impl : set_impl,
       B : ccompare : equal : mapping_impl,
       C : ceq : ccompare : set_impl](m: fsm[A, B, C], x1: (A, B)):
      set[(C, A)]
  =
  (m, x1) match {
  case (m, (q, x)) =>
    {
      val ma =
        set_as_map_image[A, (B, (C, A)), (A, B),
                          (C, A)](transitions[A, B, C](m),
                                   ((a: (A, (B, (C, A)))) =>
                                     {
                                       val (qb, (xa, (y, qa))) =
 a: ((A, (B, (C, A))));
                                       ((qb, xa), (y, qa))
                                     })):
          (((A, B)) => Option[set[(C, A)]]);
      (ma((q, x)) match {
         case None => bot_set[(C, A)]
         case Some(yqs) => yqs
       })
    }
}

def dlist_ex[A : ceq](x: A => Boolean, xc: set_dlist[A]): Boolean =
  list_ex[A](x, list_of_dlist[A](xc))

def Rbt_impl_rbt_ex[A, B](p: A => B => Boolean, x1: rbt[A, B]): Boolean =
  (p, x1) match {
  case (p, Branch(c, l, k, v, r)) =>
    (p(k))(v) || (Rbt_impl_rbt_ex[A, B](p, l) || Rbt_impl_rbt_ex[A, B](p, r))
  case (p, Emptyd()) => false
}

def ex[A : ccompare, B](xb: A => B => Boolean, xc: mapping_rbt[A, B]): Boolean =
  Rbt_impl_rbt_ex[A, B](xb, impl_ofa[A, B](xc))

def Bex[A : ceq : ccompare](x0: set[A], p: A => Boolean): Boolean = (x0, p)
  match {
  case (Rbt_set(rbt), p) =>
    (ccompare[A] match {
       case None =>
         { sys.error("Bex RBT_set: ccompare = None");
           (((_: Unit) => Bex[A](Rbt_set[A](rbt), p))).apply(()) }
       case Some(_) => ex[A, Unit](((k: A) => (_: Unit) => p(k)), rbt)
     })
  case (Dlist_set(dxs), p) =>
    (ceq[A] match {
       case None =>
         { sys.error("Bex DList_set: ceq = None");
           (((_: Unit) => Bex[A](Dlist_set[A](dxs), p))).apply(()) }
       case Some(_) => dlist_ex[A](p, dxs)
     })
  case (Set_monad(xs), p) => list_ex[A](p, xs)
}

def size[A : card_univ : ceq : ccompare, B, C](m: fsm[A, B, C]): nat =
  card[A](states[A, B, C](m))

def nth[A](x0: List[A], n: nat): A = (x0, n) match {
  case (x :: xs, n) =>
    (if (equal_nata(n, zero_nat)) x else nth[A](xs, minus_nat(n, one_nata)))
}

def rev[A](xs: List[A]): List[A] =
  fold[A, List[A]](((a: A) => (b: List[A]) => a :: b), xs, Nil)

def upt(i: nat, j: nat): List[nat] =
  (if (less_nat(i, j)) i :: upt(Suc(i), j) else Nil)

def zip[A, B](xs: List[A], ys: List[B]): List[(A, B)] = (xs, ys) match {
  case (x :: xs, y :: ys) => (x, y) :: zip[A, B](xs, ys)
  case (xs, Nil) => Nil
  case (Nil, ys) => Nil
}

def Rbt_impl_rbt_all[A, B](p: A => B => Boolean, x1: rbt[A, B]): Boolean =
  (p, x1) match {
  case (p, Branch(c, l, k, v, r)) =>
    (p(k))(v) && (Rbt_impl_rbt_all[A, B](p, l) && Rbt_impl_rbt_all[A, B](p, r))
  case (p, Emptyd()) => true
}

def all[A : ccompare, B](xb: A => B => Boolean, xc: mapping_rbt[A, B]): Boolean
  =
  Rbt_impl_rbt_all[A, B](xb, impl_ofa[A, B](xc))

def Ball[A : ceq : ccompare](x0: set[A], p: A => Boolean): Boolean = (x0, p)
  match {
  case (Rbt_set(rbt), p) =>
    (ccompare[A] match {
       case None =>
         { sys.error("Ball RBT_set: ccompare = None");
           (((_: Unit) => Ball[A](Rbt_set[A](rbt), p))).apply(()) }
       case Some(_) => all[A, Unit](((k: A) => (_: Unit) => p(k)), rbt)
     })
  case (Dlist_set(dxs), p) =>
    (ceq[A] match {
       case None =>
         { sys.error("Ball DList_set: ceq = None");
           (((_: Unit) => Ball[A](Dlist_set[A](dxs), p))).apply(()) }
       case Some(_) => dlist_all[A](p, dxs)
     })
  case (Set_monad(xs), p) => list_all[A](p, xs)
}

def h_obs_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]):
      mapping[(A, B), mapping[C, A]]
  =
  x0 match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x7
}

def h_obs_wp[A, B, C](x: fsm_with_precomputations[A, B, C]):
      mapping[(A, B), mapping[C, A]]
  =
  h_obs_wpi[A, B,
             C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                                   C](x))

def h_obsa[A : ccompare : equal, B : ccompare : equal,
            C : ccompare : equal](xa0: fsm_impl[A, B, C], q: A, x: B, y: C):
      Option[A]
  =
  (xa0, q, x, y) match {
  case (FSMWP(m), q, x, y) =>
    ((lookupa[(A, B), mapping[C, A]](h_obs_wp[A, B, C](m))).apply((q, x)) match
       {
       case None => None
       case Some(ma) => (lookupa[C, A](ma)).apply(y)
     })
}

def h_obs[A : ccompare : equal, B : ccompare : equal,
           C : ccompare : equal](x: fsm[A, B, C]):
      A => B => C => Option[A]
  =
  ((a: A) => (b: B) => (c: C) =>
    h_obsa[A, B, C](fsm_impl_of_fsm[A, B, C](x), a, b, c))

def after[A : ccompare : equal, B : ccompare : equal,
           C : ccompare : equal](m: fsm[A, B, C], q: A, x2: List[(B, C)]):
      A
  =
  (m, q, x2) match {
  case (m, q, Nil) => q
  case (m, q, (x, y) :: io) =>
    after[A, B,
           C](m, the[A]((h_obs[A, B, C](m)).apply(q).apply(x).apply(y)), io)
}

def drop[A](n: nat, x1: List[A]): List[A] = (n, x1) match {
  case (n, Nil) => Nil
  case (n, x :: xs) =>
    (if (equal_nata(n, zero_nat)) x :: xs
      else drop[A](minus_nat(n, one_nata), xs))
}

def find[A](uu: A => Boolean, x1: List[A]): Option[A] = (uu, x1) match {
  case (uu, Nil) => None
  case (p, x :: xs) => (if (p(x)) Some[A](x) else find[A](p, xs))
}

def last[A](x0: List[A]): A = x0 match {
  case x :: xs => (if (nulla[A](xs)) x else last[A](xs))
}

def maps[A, B](f: A => List[B], x1: List[A]): List[B] = (f, x1) match {
  case (f, Nil) => Nil
  case (f, x :: xs) => f(x) ++ maps[A, B](f, xs)
}

def take[A](n: nat, x1: List[A]): List[A] = (n, x1) match {
  case (n, Nil) => Nil
  case (n, x :: xs) =>
    (if (equal_nata(n, zero_nat)) Nil
      else x :: take[A](minus_nat(n, one_nata), xs))
}

def image[A : ceq : ccompare,
           B : ceq : ccompare : set_impl](h: A => B, x1: set[A]):
      set[B]
  =
  (h, x1) match {
  case (h, Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("image RBT_set: ccompare = None");
           (((_: Unit) => image[A, B](h, Rbt_set[A](rbt)))).apply(()) }
       case Some(_) =>
         (foldb[A, set[B]](comp[B, (set[B]) => set[B],
                                 A](((a: B) => (b: set[B]) => insert[B](a, b)),
                                     h),
                            rbt)).apply(bot_set[B])
     })
  case (g, Dlist_set(dxs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("image DList_set: ceq = None");
           (((_: Unit) => image[A, B](g, Dlist_set[A](dxs)))).apply(()) }
       case Some(_) =>
         (foldc[A, set[B]](comp[B, (set[B]) => set[B],
                                 A](((a: B) => (b: set[B]) => insert[B](a, b)),
                                     g),
                            dxs)).apply(bot_set[B])
     })
  case (f, Complement(Complement(b))) => image[A, B](f, b)
  case (f, Collect_set(a)) =>
    { sys.error("image Collect_set");
      (((_: Unit) => image[A, B](f, Collect_set[A](a)))).apply(()) }
  case (f, Set_monad(xs)) => Set_monad[B](mapa[A, B](f, xs))
}

def set_as_map[A : ceq : ccompare : equal : mapping_impl,
                B : ceq : ccompare : set_impl](x0: set[(A, B)]):
      A => Option[set[B]]
  =
  x0 match {
  case Dlist_set(xs) =>
    (ceq_proda[A, B] match {
       case None =>
         { sys.error("set_as_map RBT_set: ccompare = None");
           (((_: Unit) => set_as_map[A, B](Dlist_set[(A, B)](xs)))).apply(()) }
       case Some(_) =>
         lookupa[A, set[B]]((foldc[(A, B),
                                    mapping[A,
     set[B]]](((a: (A, B)) =>
                {
                  val (x, z) = a: ((A, B));
                  ((m: mapping[A, set[B]]) =>
                    ((lookupa[A, set[B]](m)).apply(x) match {
                       case None =>
                         updateb[A, set[B]](x, insert[B](z, bot_set[B]), m)
                       case Some(zs) =>
                         updateb[A, set[B]](x, insert[B](z, zs), m)
                     }))
                }),
               xs)).apply(emptya[A, set[B]]))
     })
  case Rbt_set(t) =>
    (ccompare_proda[A, B] match {
       case None =>
         { sys.error("set_as_map RBT_set: ccompare = None");
           (((_: Unit) => set_as_map[A, B](Rbt_set[(A, B)](t)))).apply(()) }
       case Some(_) =>
         lookupa[A, set[B]]((foldb[(A, B),
                                    mapping[A,
     set[B]]](((a: (A, B)) =>
                {
                  val (x, z) = a: ((A, B));
                  ((m: mapping[A, set[B]]) =>
                    ((lookupa[A, set[B]](m)).apply(x) match {
                       case None =>
                         updateb[A, set[B]](x, insert[B](z, bot_set[B]), m)
                       case Some(zs) =>
                         updateb[A, set[B]](x, insert[B](z, zs), m)
                     }))
                }),
               t)).apply(emptya[A, set[B]]))
     })
}

def h_from[A : ceq : ccompare : equal : mapping_impl : set_impl,
            B : ceq : ccompare : set_impl,
            C : ceq : ccompare : set_impl](m: fsm[A, B, C], q: A):
      set[(B, (C, A))]
  =
  {
    val ma =
      set_as_map[A, (B, (C, A))](transitions[A, B, C](m)):
        (A => Option[set[(B, (C, A))]]);
    (ma(q) match {
       case None => bot_set[(B, (C, A))]
       case Some(yqs) => yqs
     })
  }

def targeta[A, B, C](q: A, x1: List[(A, (B, (C, A)))]): A = (q, x1) match {
  case (q, Nil) => q
  case (q, v :: va) =>
    snd[C, A](snd[B, (C, A)](snd[A, (B, (C,
  A))](last[(A, (B, (C, A)))](v :: va))))
}

def target[A, B, C](q: A, p: List[(A, (B, (C, A)))]): A = targeta[A, B, C](q, p)

def foldr[A, B](f: A => B => B, x1: List[A]): B => B = (f, x1) match {
  case (f, Nil) => id[B]
  case (f, x :: xs) => comp[B, B, B](f(x), foldr[A, B](f, xs))
}

def filter[A : ceq : ccompare](p: A => Boolean, a: set[A]): set[A] =
  inf_seta[A](a, Collect_set[A](p))

def apsnd[A, B, C](f: A => B, x1: (C, A)): (C, B) = (f, x1) match {
  case (f, (x, y)) => (x, f(y))
}

def divmod_integer(k: BigInt, l: BigInt): (BigInt, BigInt) =
  (if (k == BigInt(0)) (BigInt(0), BigInt(0))
    else (if (BigInt(0) < l)
           (if (BigInt(0) < k)
             ((k: BigInt) => (l: BigInt) => if (l == 0) (BigInt(0), k) else
               (k.abs /% l.abs)).apply(k).apply(l)
             else {
                    val (r, s) =
                      ((k: BigInt) => (l: BigInt) => if (l == 0)
                        (BigInt(0), k) else (k.abs /% l.abs)).apply(k).apply(l):
                        ((BigInt, BigInt));
                    (if (s == BigInt(0)) ((- r), BigInt(0))
                      else ((- r) - BigInt(1), l - s))
                  })
           else (if (l == BigInt(0)) (BigInt(0), k)
                  else apsnd[BigInt, BigInt,
                              BigInt](((a: BigInt) => (- a)),
                                       (if (k < BigInt(0))
 ((k: BigInt) => (l: BigInt) => if (l == 0) (BigInt(0), k) else
   (k.abs /% l.abs)).apply(k).apply(l)
 else {
        val (r, s) =
          ((k: BigInt) => (l: BigInt) => if (l == 0) (BigInt(0), k) else
            (k.abs /% l.abs)).apply(k).apply(l):
            ((BigInt, BigInt));
        (if (s == BigInt(0)) ((- r), BigInt(0))
          else ((- r) - BigInt(1), (- l) - s))
      })))))

def divide_integer(k: BigInt, l: BigInt): BigInt =
  fst[BigInt, BigInt](divmod_integer(k, l))

def divide_nat(m: nat, n: nat): nat =
  Nata(divide_integer(integer_of_nat(m), integer_of_nat(n)))

def part[A, B : linorder](f: A => B, pivot: B, x2: List[A]):
      (List[A], (List[A], List[A]))
  =
  (f, pivot, x2) match {
  case (f, pivot, x :: xs) =>
    {
      val (lts, (eqs, gts)) =
        part[A, B](f, pivot, xs): ((List[A], (List[A], List[A])))
      val xa = f(x): B;
      (if (less[B](xa, pivot)) (x :: lts, (eqs, gts))
        else (if (less[B](pivot, xa)) (lts, (eqs, x :: gts))
               else (lts, (x :: eqs, gts))))
    }
  case (f, pivot, Nil) => (Nil, (Nil, Nil))
}

def sort_key[A, B : linorder](f: A => B, xs: List[A]): List[A] =
  (xs match {
     case Nil => Nil
     case List(_) => xs
     case List(x, y) => (if (less_eq[B](f(x), f(y))) xs else List(y, x))
     case _ :: _ :: _ :: _ =>
       {
         val (lts, (eqs, gts)) =
           part[A, B](f, f(nth[A](xs, divide_nat(size_list[A].apply(xs),
          nat_of_integer(BigInt(2))))),
                       xs):
             ((List[A], (List[A], List[A])));
         sort_key[A, B](f, lts) ++ (eqs ++ sort_key[A, B](f, gts))
       }
   })

def membera[A : equal](x0: List[A], y: A): Boolean = (x0, y) match {
  case (Nil, y) => false
  case (x :: xs, y) => eq[A](x, y) || membera[A](xs, y)
}

def remdups[A : equal](x0: List[A]): List[A] = x0 match {
  case Nil => Nil
  case x :: xs =>
    (if (membera[A](xs, x)) remdups[A](xs) else x :: remdups[A](xs))
}

def sorted_list_of_set[A : ceq : ccompare : equal : linorder](x0: set[A]):
      List[A]
  =
  x0 match {
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("sorted_list_of_set RBT_set: ccompare = None");
           (((_: Unit) => sorted_list_of_set[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) => sort_key[A, A](((x: A) => x), keysb[A](rbt))
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("sorted_list_of_set DList_set: ceq = None");
           (((_: Unit) => sorted_list_of_set[A](Dlist_set[A](dxs)))).apply(()) }
       case Some(_) => sort_key[A, A](((x: A) => x), list_of_dlist[A](dxs))
     })
  case Set_monad(xs) => sort_key[A, A](((x: A) => x), remdups[A](xs))
}

def inputs_as_list[A, B : ceq : ccompare : equal : linorder,
                    C](m: fsm[A, B, C]):
      List[B]
  =
  sorted_list_of_set[B](inputs[A, B, C](m))

def fset_of_list[A : ceq : ccompare : set_impl](xa: List[A]): fset[A] =
  Abs_fset[A](set[A](xa))

def finputs[A, B : ceq : ccompare : equal : linorder : set_impl,
             C](m: fsm[A, B, C]):
      fset[B]
  =
  fset_of_list[B](inputs_as_list[A, B, C](m))

def states_as_list[A : ceq : ccompare : equal : linorder, B,
                    C](m: fsm[A, B, C]):
      List[A]
  =
  sorted_list_of_set[A](states[A, B, C](m))

def fstates[A : ceq : ccompare : equal : linorder : set_impl, B,
             C](m: fsm[A, B, C]):
      fset[A]
  =
  fset_of_list[A](states_as_list[A, B, C](m))

def fimage[B : ceq : ccompare,
            A : ceq : ccompare : set_impl](xb: B => A, xc: fset[B]):
      fset[A]
  =
  Abs_fset[A](image[B, A](xb, fset[B](xc)))

def concat[A](xss: List[List[A]]): List[A] =
  (foldr[List[A],
          List[A]](((a: List[A]) => (b: List[A]) => a ++ b), xss)).apply(Nil)

def map_add[A, B](m1: A => Option[B], m2: A => Option[B]): A => Option[B] =
  ((x: A) => (m2(x) match {
                case None => m1(x)
                case Some(a) => Some[B](a)
              }))

def outputs_as_list[A, B,
                     C : ceq : ccompare : equal : linorder](m: fsm[A, B, C]):
      List[C]
  =
  sorted_list_of_set[C](outputs[A, B, C](m))

def foutputs[A, B,
              C : ceq : ccompare : equal : linorder : set_impl](m:
                          fsm[A, B, C]):
      fset[C]
  =
  fset_of_list[C](outputs_as_list[A, B, C](m))

def h_wpi[A, B, C](x0: fsm_with_precomputations_impl[A, B, C]):
      mapping[(A, B), set[(C, A)]]
  =
  x0 match {
  case FSMWPI(x1, x2, x3, x4, x5, x6, x7) => x6
}

def from_fsmi_impl[A : ceq : ccompare, B,
                    C](m: fsm_with_precomputations_impl[A, B, C], q: A):
      fsm_with_precomputations_impl[A, B, C]
  =
  (if (member[A](q, states_wpi[A, B, C](m)))
    FSMWPI[A, B,
            C](q, states_wpi[A, B, C](m), inputs_wpi[A, B, C](m),
                outputs_wpi[A, B, C](m), transitions_wpi[A, B, C](m),
                h_wpi[A, B, C](m), h_obs_wpi[A, B, C](m))
    else m)

def from_fsmia[A : ceq : ccompare, B,
                C](xb: fsm_with_precomputations[A, B, C], xc: A):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](from_fsmi_impl[A, B,
        C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                              C](xb),
            xc))

def from_fsmi[A : ceq : ccompare, B, C](x0: fsm_impl[A, B, C], q: A):
      fsm_impl[A, B, C]
  =
  (x0, q) match {
  case (FSMWP(m), q) => FSMWP[A, B, C](from_fsmia[A, B, C](m, q))
}

def from_fsm[A : ceq : ccompare, B, C](xb: fsm[A, B, C], xc: A): fsm[A, B, C] =
  Abs_fsm[A, B, C](from_fsmi[A, B, C](fsm_impl_of_fsm[A, B, C](xb), xc))

def comp_fun_idem_apply[B, A](x0: comp_fun_idem[B, A]): B => A => A = x0 match {
  case Abs_comp_fun_idem(x) => x
}

def set_fold_cfi[A : ceq : ccompare,
                  B](f: comp_fun_idem[A, B], b: B, x2: set[A]):
      B
  =
  (f, b, x2) match {
  case (f, b, Rbt_set(rbt)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("set_fold_cfi RBT_set: ccompare = None");
           (((_: Unit) => set_fold_cfi[A, B](f, b, Rbt_set[A](rbt)))).apply(())
           }
       case Some(_) => (foldb[A, B](comp_fun_idem_apply[A, B](f), rbt)).apply(b)
     })
  case (f, b, Dlist_set(dxs)) =>
    (ceq[A] match {
       case None =>
         { sys.error("set_fold_cfi DList_set: ceq = None");
           (((_: Unit) =>
              set_fold_cfi[A, B](f, b, Dlist_set[A](dxs)))).apply(())
           }
       case Some(_) => (foldc[A, B](comp_fun_idem_apply[A, B](f), dxs)).apply(b)
     })
  case (f, b, Set_monad(xs)) => fold[A, B](comp_fun_idem_apply[A, B](f), xs, b)
  case (f, b, Collect_set(p)) =>
    { sys.error("set_fold_cfi not supported on Collect_set");
      (((_: Unit) => set_fold_cfi[A, B](f, b, Collect_set[A](p)))).apply(()) }
  case (f, b, Complement(a)) =>
    { sys.error("set_fold_cfi not supported on Complement");
      (((_: Unit) => set_fold_cfi[A, B](f, b, Complement[A](a)))).apply(()) }
}

def sup_cfi[A : lattice]: comp_fun_idem[A, A] =
  Abs_comp_fun_idem[A, A](((a: A) => (b: A) => sup[A](a, b)))

def Sup_setb[A : finite_univ : cenum : ceq : cproper_interval : set_impl](a:
                                    set[set[A]]):
      set[A]
  =
  (if (finite[set[A]](a))
    set_fold_cfi[set[A], set[A]](sup_cfi[set[A]], bot_set[A], a)
    else { sys.error("Sup: infinite"); (((_: Unit) => Sup_setb[A](a))).apply(())
           })

def ffunion[A : finite_univ : cenum : ceq : cproper_interval : equal : set_impl](xa:
   fset[fset[A]]):
      fset[A]
  =
  Abs_fset[A](Sup_setb[A](image[fset[A],
                                 set[A]](((a: fset[A]) => fset[A](a)),
  fset[fset[A]](xa))))

def ffilter[A : ceq : ccompare](xb: A => Boolean, xc: fset[A]): fset[A] =
  Abs_fset[A](filter[A](xb, fset[A](xc)))

def finsert[A : ceq : ccompare](xb: A, xc: fset[A]): fset[A] =
  Abs_fset[A](insert[A](xb, fset[A](xc)))

def butlast[A](x0: List[A]): List[A] = x0 match {
  case Nil => Nil
  case x :: xs => (if (nulla[A](xs)) Nil else x :: butlast[A](xs))
}

def hd[A](x0: List[A]): A = x0 match {
  case x21 :: x22 => x21
}

def remove1[A : equal](x: A, xa1: List[A]): List[A] = (x, xa1) match {
  case (x, Nil) => Nil
  case (x, y :: xs) => (if (eq[A](x, y)) xs else y :: remove1[A](x, xs))
}

def map_upds[A : equal, B](m: A => Option[B], xs: List[A], ys: List[B]):
      A => Option[B]
  =
  map_add[A, B](m, ((a: A) => map_of[A, B](rev[(A, B)](zip[A, B](xs, ys)), a)))

def map[A, B, C](f: A => B => C, x1: rbt[A, B]): rbt[A, C] = (f, x1) match {
  case (f, Emptyd()) => Emptyd[A, C]()
  case (f, Branch(c, lt, k, v, rt)) =>
    Branch[A, C](c, map[A, B, C](f, lt), k, (f(k))(v), map[A, B, C](f, rt))
}

def mapb[A : ccompare, C, B](xb: A => C => B, xc: mapping_rbt[A, C]):
      mapping_rbt[A, B]
  =
  Mapping_rbtb[A, B](map[A, C, B](xb, impl_ofa[A, C](xc)))

def keysc[A : ceq : ccompare : set_impl, B](xa: alist[A, B]): set[A] =
  set[A](mapa[(A, B), A](((a: (A, B)) => fst[A, B](a)), impl_of[A, B](xa)))

def keys[A : cenum : ceq : ccompare : set_impl, B](x0: mapping[A, B]): set[A] =
  x0 match {
  case Rbt_mapping(t) =>
    Rbt_set[A](mapb[A, B, Unit](((_: A) => (_: B) => ()), t))
  case Assoc_list_mapping(al) => keysc[A, B](al)
  case Mappinga(m) => Collect[A](((k: A) => ! (is_none[B](m(k)))))
}

def the_elem[A : ceq : ccompare](x0: set[A]): A = x0 match {
  case Rbt_set(rbt) =>
    (ccompare[A] match {
       case None =>
         { sys.error("the_elem RBT_set: ccompare = None");
           (((_: Unit) => the_elem[A](Rbt_set[A](rbt)))).apply(()) }
       case Some(_) =>
         (impl_ofa[A, Unit](rbt) match {
            case Emptyd() =>
              { sys.error("the_elem RBT_set: not unique");
                (((_: Unit) => the_elem[A](Rbt_set[A](rbt)))).apply(()) }
            case Branch(_, Emptyd(), x, _, Emptyd()) => x
            case Branch(_, Emptyd(), _, _, Branch(_, _, _, _, _)) =>
              { sys.error("the_elem RBT_set: not unique");
                (((_: Unit) => the_elem[A](Rbt_set[A](rbt)))).apply(()) }
            case Branch(_, Branch(_, _, _, _, _), _, _, _) =>
              { sys.error("the_elem RBT_set: not unique");
                (((_: Unit) => the_elem[A](Rbt_set[A](rbt)))).apply(()) }
          })
     })
  case Dlist_set(dxs) =>
    (ceq[A] match {
       case None =>
         { sys.error("the_elem DList_set: ceq = None");
           (((_: Unit) => the_elem[A](Dlist_set[A](dxs)))).apply(()) }
       case Some(_) =>
         (list_of_dlist[A](dxs) match {
            case Nil =>
              { sys.error("the_elem DList_set: not unique");
                (((_: Unit) => the_elem[A](Dlist_set[A](dxs)))).apply(()) }
            case List(x) => x
            case _ :: _ :: _ =>
              { sys.error("the_elem DList_set: not unique");
                (((_: Unit) => the_elem[A](Dlist_set[A](dxs)))).apply(()) }
          })
     })
  case Set_monad(List(x)) => x
}

def filterb[A, B](xb: ((A, B)) => Boolean, xc: alist[A, B]): alist[A, B] =
  Alista[A, B](filtera[(A, B)](xb, impl_of[A, B](xc)))

def pow_list[A](x0: List[A]): List[List[A]] = x0 match {
  case Nil => List(Nil)
  case x :: xs => {
                    val pxs = pow_list[A](xs): (List[List[A]]);
                    pxs ++ mapa[List[A], List[A]](((a: List[A]) => x :: a), pxs)
                  }
}

def acyclic_paths_up_to_lengtha[A : ceq : ccompare, B : ceq : ccompare,
                                 C : ceq : ccompare](prev:
               List[(A, (B, (C, A)))],
              q: A, hF: A => set[(B, (C, A))], visitedStates: set[A], k: nat):
      set[List[(A, (B, (C, A)))]]
  =
  (if (equal_nata(k, zero_nat))
    insert[List[(A, (B, (C, A)))]](prev,
                                    set_empty[List[(A,
             (B, (C, A)))]](of_phantom[List[(A, (B, (C, A)))],
set_impla](set_impl_lista[(A, (B, (C, A)))])))
    else {
           val tF =
             filter[(B, (C, A))](((a: (B, (C, A))) =>
                                   {
                                     val (_, (_, qa)) = a: ((B, (C, A)));
                                     ! (member[A](qa, visitedStates))
                                   }),
                                  hF(q)):
               (set[(B, (C, A))]);
           insert[List[(A, (B, (C, A)))]](prev,
   Sup_setb[List[(A, (B, (C, A)))]](image[(B, (C, A)),
   set[List[(A, (B, (C, A)))]]](((a: (B, (C, A))) =>
                                  {
                                    val (x, (y, qa)) = a: ((B, (C, A)));
                                    acyclic_paths_up_to_lengtha[A, B,
                         C](prev ++ List((q, (x, (y, qa)))), qa, hF,
                             insert[A](qa, visitedStates),
                             minus_nat(k, one_nata))
                                  }),
                                 tF)))
         })

def acyclic_paths_up_to_length[A : ceq : ccompare : equal : mapping_impl : set_impl,
                                B : ceq : ccompare : set_impl,
                                C : ceq : ccompare : set_impl](m: fsm[A, B, C],
                        q: A, k: nat):
      set[List[(A, (B, (C, A)))]]
  =
  (if (member[A](q, states[A, B, C](m)))
    acyclic_paths_up_to_lengtha[A, B,
                                 C](Nil, q,
                                     ((x: A) =>
                                       ((set_as_map[A,
             (B, (C, A))](transitions[A, B, C](m))).apply(x)
  match {
  case None => bot_set[(B, (C, A))]
  case Some(xs) => xs
})),
                                     insert[A](q, bot_set[A]), k)
    else set_empty[List[(A, (B, (C, A)))]](of_phantom[List[(A, (B, (C, A)))],
               set_impla](set_impl_lista[(A, (B, (C, A)))])))

def Ls_acyclic[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                B : ceq : ccompare : set_impl,
                C : ceq : ccompare : set_impl](m: fsm[A, B, C], q: A):
      set[List[(B, C)]]
  =
  image[List[(A, (B, (C, A)))],
         List[(B, C)]](((a: List[(A, (B, (C, A)))]) =>
                         mapa[(A, (B, (C, A))),
                               (B, C)](((t: (A, (B, (C, A)))) =>
 (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
   fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
a)),
                        acyclic_paths_up_to_length[A, B,
            C](m, q, minus_nat(size[A, B, C](m), one_nata)))

def observable[A : ceq : ccompare : equal, B : ceq : ccompare : equal,
                C : ceq : ccompare : equal](m: fsm[A, B, C]):
      Boolean
  =
  Ball[(A, (B, (C, A)))](transitions[A, B, C](m),
                          ((t1: (A, (B, (C, A)))) =>
                            Ball[(A, (B, (C,
   A)))](transitions[A, B, C](m),
          ((t2: (A, (B, (C, A)))) =>
            (if (eq[A](fst[A, (B, (C, A))](t1), fst[A, (B, (C, A))](t2)) &&
                   (eq[B](fst[B, (C, A)](snd[A, (B, (C, A))](t1)),
                           fst[B, (C, A)](snd[A, (B, (C, A))](t2))) &&
                     eq[C](fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t1))),
                            fst[C, A](snd[B,
   (C, A)](snd[A, (B, (C, A))](t2))))))
              eq[A](snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t1))),
                     snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t2))))
              else true)))))

def dropwhile[A](p: A => Boolean, x1: List[A]): List[A] = (p, x1) match {
  case (p, Nil) => Nil
  case (p, x :: xs) => (if (p(x)) dropwhile[A](p, xs) else x :: xs)
}

def removeall[A : equal](x: A, xa1: List[A]): List[A] = (x, xa1) match {
  case (x, Nil) => Nil
  case (x, y :: xs) =>
    (if (eq[A](x, y)) removeall[A](x, xs) else y :: removeall[A](x, xs))
}

def filterc[A : ccompare, B](p: A => B => Boolean, x1: mapping[A, B]):
      mapping[A, B]
  =
  (p, x1) match {
  case (p, Rbt_mapping(t)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("filter RBT_Mapping: ccompare = None");
           (((_: Unit) => filterc[A, B](p, Rbt_mapping[A, B](t)))).apply(()) }
       case Some(_) =>
         Rbt_mapping[A, B](filtere[A, B](((a: (A, B)) =>
   {
     val (aa, b) = a: ((A, B));
     (p(aa))(b)
   }),
  t))
     })
  case (p, Assoc_list_mapping(al)) =>
    Assoc_list_mapping[A, B](filterb[A, B](((a: (A, B)) =>
     {
       val (aa, b) = a: ((A, B));
       (p(aa))(b)
     }),
    al))
  case (p, Mappinga(m)) =>
    Mappinga[A, B](((k: A) =>
                     (m(k) match {
                        case None => None
                        case Some(v) => (if ((p(k))(v)) Some[B](v) else None)
                      })))
}

def filter_comp_minus[A, B,
                       C](c: A => A => ordera, t1: rbt[A, B], t2: rbt[A, C]):
      List[(A, B)]
  =
  filtera[(A, B)](((a: (A, B)) =>
                    {
                      val (k, _) = a: ((A, B));
                      is_none[C](rbt_comp_lookup[A, C](c, t2, k))
                    }),
                   entries[A, B].apply(t1))

def small_rbt[A, B](t: rbt[A, B]): Boolean =
  less_nat(bheight[A, B](t), nat_of_integer(BigInt(4)))

def comp_minus[A, B](c: A => A => ordera, t1: rbt[A, B], t2: rbt[A, B]):
      rbt[A, B]
  =
  (if (small_rbt[A, B](t2))
    folda[A, B,
           rbt[A, B]](((k: A) => (_: B) =>
                        ((a: rbt[A, B]) => rbt_comp_delete[A, B](c, k, a))),
                       t2, t1)
    else (if (small_rbt[A, B](t1))
           rbtreeify[A, B](filter_comp_minus[A, B, B](c, t1, t2))
           else (t2 match {
                   case Emptyd() => t1
                   case Branch(_, l2, a, _, r2) =>
                     {
                       val (l1, (_, r1)) =
                         rbt_split_comp[A, B](c, t1, a):
                           ((rbt[A, B], (Option[B], rbt[A, B])));
                       rbt_join2[A, B](comp_minus[A, B](c, l1, l2),
comp_minus[A, B](c, r1, r2))
                     }
                 })))

def rbt_comp_minus[A, B](c: A => A => ordera, t1: rbt[A, B], t2: rbt[A, B]):
      rbt[A, B]
  =
  paint[A, B](B(), comp_minus[A, B](c, t1, t2))

def minus[A : ccompare](xb: mapping_rbt[A, Unit], xc: mapping_rbt[A, Unit]):
      mapping_rbt[A, Unit]
  =
  Mapping_rbtb[A, Unit](rbt_comp_minus[A,
Unit](the[A => A => ordera](ccompare[A]), impl_ofa[A, Unit](xb),
       impl_ofa[A, Unit](xc)))

def is_prefix[A : equal](x0: List[A], uu: List[A]): Boolean = (x0, uu) match {
  case (Nil, uu) => true
  case (x :: xs, Nil) => false
  case (x :: xs, y :: ys) => eq[A](x, y) && is_prefix[A](xs, ys)
}

def find_index[A](f: A => Boolean, x1: List[A]): Option[nat] = (f, x1) match {
  case (f, Nil) => None
  case (f, x :: xs) =>
    (if (f(x)) Some[nat](zero_nat) else (find_index[A](f, xs) match {
   case None => None
   case Some(k) => Some[nat](Suc(k))
 }))
}

def transitions_as_list[A : ceq : ccompare : equal : linorder,
                         B : ceq : ccompare : equal : linorder,
                         C : ceq : ccompare : equal : linorder](m:
                          fsm[A, B, C]):
      List[(A, (B, (C, A)))]
  =
  sorted_list_of_set[(A, (B, (C, A)))](transitions[A, B, C](m))

def ftransitions[A : ceq : ccompare : equal : linorder : set_impl,
                  B : ceq : ccompare : equal : linorder : set_impl,
                  C : ceq : ccompare : equal : linorder : set_impl](m:
                              fsm[A, B, C]):
      fset[(A, (B, (C, A)))]
  =
  fset_of_list[(A, (B, (C, A)))](transitions_as_list[A, B, C](m))

def assign_indices[A : ceq : ccompare : equal : linorder](xs: set[A]): A => nat
  =
  ((x: A) =>
    the[nat](find_index[A](((a: A) => eq[A](x, a)), sorted_list_of_set[A](xs))))

def set_as_mapping_image[A : ceq : ccompare, B : ceq : ccompare,
                          C : ccompare : equal : mapping_impl,
                          D : ceq : ccompare : set_impl](x0: set[(A, B)],
                  f2: ((A, B)) => (C, D)):
      mapping[C, set[D]]
  =
  (x0, f2) match {
  case (Dlist_set(xs), f2) =>
    (ceq_proda[A, B] match {
       case None =>
         { sys.error("set_as_map_image DList_set: ccompare = None");
           (((_: Unit) =>
              set_as_mapping_image[A, B, C,
                                    D](Dlist_set[(A, B)](xs), f2))).apply(())
           }
       case Some(_) =>
         (foldc[(A, B),
                 mapping[C, set[D]]](((kv: (A, B)) =>
                                       (m1: mapping[C, set[D]]) =>
                                       {
 val (x, z) = f2(kv): ((C, D));
 ((lookupa[C, set[D]](m1)).apply(x) match {
    case None => updateb[C, set[D]](x, insert[D](z, bot_set[D]), m1)
    case Some(zs) => updateb[C, set[D]](x, insert[D](z, zs), m1)
  })
                                       }),
                                      xs)).apply(emptya[C, set[D]])
     })
  case (Rbt_set(t), f1) =>
    (ccompare_proda[A, B] match {
       case None =>
         { sys.error("set_as_map_image RBT_set: ccompare = None");
           (((_: Unit) =>
              set_as_mapping_image[A, B, C,
                                    D](Rbt_set[(A, B)](t), f1))).apply(())
           }
       case Some(_) =>
         (foldb[(A, B),
                 mapping[C, set[D]]](((kv: (A, B)) =>
                                       (m1: mapping[C, set[D]]) =>
                                       {
 val (x, z) = f1(kv): ((C, D));
 ((lookupa[C, set[D]](m1)).apply(x) match {
    case None => updateb[C, set[D]](x, insert[D](z, bot_set[D]), m1)
    case Some(zs) => updateb[C, set[D]](x, insert[D](z, zs), m1)
  })
                                       }),
                                      t)).apply(emptya[C, set[D]])
     })
}

def set_as_mapping[A : ceq : ccompare : equal : mapping_impl,
                    B : ceq : ccompare : set_impl](x0: set[(A, B)]):
      mapping[A, set[B]]
  =
  x0 match {
  case Dlist_set(xs) =>
    (ceq_proda[A, B] match {
       case None =>
         { sys.error("set_as_map RBT_set: ccompare = None");
           (((_: Unit) =>
              set_as_mapping[A, B](Dlist_set[(A, B)](xs)))).apply(())
           }
       case Some(_) =>
         (foldc[(A, B),
                 mapping[A, set[B]]](((a: (A, B)) =>
                                       {
 val (x, z) = a: ((A, B));
 ((m: mapping[A, set[B]]) =>
   ((lookupa[A, set[B]](m)).apply(x) match {
      case None => updateb[A, set[B]](x, insert[B](z, bot_set[B]), m)
      case Some(zs) => updateb[A, set[B]](x, insert[B](z, zs), m)
    }))
                                       }),
                                      xs)).apply(emptya[A, set[B]])
     })
  case Rbt_set(t) =>
    (ccompare_proda[A, B] match {
       case None =>
         { sys.error("set_as_map RBT_set: ccompare = None");
           (((_: Unit) => set_as_mapping[A, B](Rbt_set[(A, B)](t)))).apply(()) }
       case Some(_) =>
         (foldb[(A, B),
                 mapping[A, set[B]]](((a: (A, B)) =>
                                       {
 val (x, z) = a: ((A, B));
 ((m: mapping[A, set[B]]) =>
   ((lookupa[A, set[B]](m)).apply(x) match {
      case None => updateb[A, set[B]](x, insert[B](z, bot_set[B]), m)
      case Some(zs) => updateb[A, set[B]](x, insert[B](z, zs), m)
    }))
                                       }),
                                      t)).apply(emptya[A, set[B]])
     })
}

def map_option[A, B](f: A => B, x1: Option[A]): Option[B] = (f, x1) match {
  case (f, None) => None
  case (f, Some(x2)) => Some[B](f(x2))
}

def map_valuesa[A, C, B](xb: A => C => B, xc: alist[A, C]): alist[A, B] =
  Alista[A, B](mapa[(A, C),
                     (A, B)](((a: (A, C)) => {
       val (x, y) = a: ((A, C));
       (x, (xb(x))(y))
     }),
                              impl_of[A, C](xc)))

def map_values[A : ccompare, B, C](f: A => B => C, x1: mapping[A, B]):
      mapping[A, C]
  =
  (f, x1) match {
  case (f, Rbt_mapping(t)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("map_values RBT_Mapping: ccompare = None");
           (((_: Unit) =>
              map_values[A, B, C](f, Rbt_mapping[A, B](t)))).apply(())
           }
       case Some(_) => Rbt_mapping[A, C](mapb[A, B, C](f, t))
     })
  case (f, Assoc_list_mapping(al)) =>
    Assoc_list_mapping[A, C](map_valuesa[A, B, C](f, al))
  case (f, Mappinga(m)) =>
    Mappinga[A, C](((k: A) => map_option[B, C](f(k), m(k))))
}

def h_obs_impl_from_h[A : card_univ : ceq : ccompare : set_impl, B : ccompare,
                       C : ceq : ccompare : equal : mapping_impl](h:
                            mapping[(A, B), set[(C, A)]]):
      mapping[(A, B), mapping[C, A]]
  =
  map_values[(A, B), set[(C, A)],
              mapping[C, A]](((_: (A, B)) => (yqs: set[(C, A)]) =>
                               {
                                 val m =
                                   set_as_mapping[C, A](yqs):
                                     (mapping[C, set[A]])
                                 val ma =
                                   filterc[C,
    set[A]](((_: C) => (qs: set[A]) => equal_nata(card[A](qs), one_nata)), m):
                                     (mapping[C, set[A]])
                                 val mb =
                                   map_values[C, set[A],
       A](((_: C) => ((a: set[A]) => the_elem[A](a))), ma):
                                     (mapping[C, A]);
                                 mb
                               }),
                              h)

def rename_states_impl[A : ceq : ccompare,
                        B : ceq : ccompare : equal : mapping_impl : set_impl,
                        C : ceq : ccompare : equal : mapping_impl : set_impl,
                        D : card_univ : ceq : ccompare : equal : mapping_impl : set_impl](m:
            fsm_with_precomputations_impl[A, B, C],
           f: A => D):
      fsm_with_precomputations_impl[D, B, C]
  =
  {
    val ts =
      image[(A, (B, (C, A))),
             (D, (B, (C, D)))](((t: (A, (B, (C, A)))) =>
                                 (f(fst[A, (B, (C, A))](t)),
                                   (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                                     (fst[C,
   A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
                                       f(snd[C,
      A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))))))),
                                transitions_wpi[A, B, C](m)):
        (set[(D, (B, (C, D)))])
    val h =
      set_as_mapping_image[D, (B, (C, D)), (D, B),
                            (C, D)](ts, ((a: (D, (B, (C, D)))) =>
  {
    val (q, (x, (y, qa))) = a: ((D, (B, (C, D))));
    ((q, x), (y, qa))
  })):
        (mapping[(D, B), set[(C, D)]]);
    FSMWPI[D, B,
            C](f(initial_wpi[A, B, C](m)),
                image[A, D](f, states_wpi[A, B, C](m)), inputs_wpi[A, B, C](m),
                outputs_wpi[A, B, C](m), ts, h, h_obs_impl_from_h[D, B, C](h))
  }

def rename_statesb[D : ceq : ccompare,
                    B : ceq : ccompare : equal : mapping_impl : set_impl,
                    C : ceq : ccompare : equal : mapping_impl : set_impl,
                    A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl](xb:
        fsm_with_precomputations[D, B, C],
       xc: D => A):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](rename_states_impl[D, B, C,
            A](fsm_with_precomputations_impl_of_fsm_with_precomputations[D, B,
                                  C](xb),
                xc))

def rename_statesa[A : ceq : ccompare,
                    B : ceq : ccompare : equal : mapping_impl : set_impl,
                    C : ceq : ccompare : equal : mapping_impl : set_impl,
                    D : card_univ : ceq : ccompare : equal : mapping_impl : set_impl](x0:
        fsm_impl[A, B, C],
       f: A => D):
      fsm_impl[D, B, C]
  =
  (x0, f) match {
  case (FSMWP(m), f) => FSMWP[D, B, C](rename_statesb[A, B, C, D](m, f))
}

def rename_states[D : ceq : ccompare,
                   B : ceq : ccompare : equal : mapping_impl : set_impl,
                   C : ceq : ccompare : equal : mapping_impl : set_impl,
                   A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl](xb:
       fsm[D, B, C],
      xc: D => A):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](rename_statesa[D, B, C, A](fsm_impl_of_fsm[D, B, C](xb), xc))

def index_states[A : ceq : ccompare : equal : linorder,
                  B : ceq : ccompare : equal : mapping_impl : set_impl,
                  C : ceq : ccompare : equal : mapping_impl : set_impl](m:
                                  fsm[A, B, C]):
      fsm[nat, B, C]
  =
  rename_states[A, B, C, nat](m, assign_indices[A](states[A, B, C](m)))

def paths_for_ioa[A : ceq : ccompare, B : ceq : ccompare,
                   C : ceq : ccompare : equal](f: ((A, B)) => set[(C, A)],
        x1: List[(B, C)], q: A, prev: List[(A, (B, (C, A)))]):
      set[List[(A, (B, (C, A)))]]
  =
  (f, x1, q, prev) match {
  case (f, Nil, q, prev) =>
    insert[List[(A, (B, (C, A)))]](prev,
                                    set_empty[List[(A,
             (B, (C, A)))]](of_phantom[List[(A, (B, (C, A)))],
set_impla](set_impl_lista[(A, (B, (C, A)))])))
  case (f, (x, y) :: io, q, prev) =>
    Sup_setb[List[(A, (B, (C, A)))]](image[(C, A),
    set[List[(A, (B, (C, A)))]]](((yq: (C, A)) =>
                                   paths_for_ioa[A, B,
          C](f, io, snd[C, A](yq), prev ++ List((q, (x, (y, snd[C, A](yq))))))),
                                  filter[(C,
   A)](((yq: (C, A)) => eq[C](fst[C, A](yq), y)), f((q, x)))))
}

def paths_for_io[A : ceq : ccompare : equal : mapping_impl : set_impl,
                  B : ceq : ccompare : equal : mapping_impl,
                  C : ceq : ccompare : equal : set_impl](m: fsm[A, B, C], q: A,
                  io: List[(B, C)]):
      set[List[(A, (B, (C, A)))]]
  =
  (if (member[A](q, states[A, B, C](m)))
    paths_for_ioa[A, B, C](((a: (A, B)) => h[A, B, C](m, a)), io, q, Nil)
    else set_empty[List[(A, (B, (C, A)))]](of_phantom[List[(A, (B, (C, A)))],
               set_impla](set_impl_lista[(A, (B, (C, A)))])))

def productc[A : ceq, B : ceq](dxs1: set_dlist[A], dxs2: set_dlist[B]):
      set_dlist[(A, B)]
  =
  Abs_dlist[(A, B)]((foldc[A, List[(A, B)]](((a: A) =>
      foldc[B, List[(A, B)]](((c: B) => ((b: List[(A, B)]) => (a, c) :: b)),
                              dxs2)),
     dxs1)).apply(Nil))

def rbt_product[A, B, C, D,
                 E](f: A => B => C => D => E, rbt1: rbt[A, B], rbt2: rbt[C, D]):
      rbt[(A, C), E]
  =
  rbtreeify[(A, C),
             E](rev[((A, C),
                      E)](folda[A, B,
                                 List[((A, C),
E)]](((a: A) => (b: B) =>
       ((c: List[((A, C), E)]) =>
         folda[C, D,
                List[((A, C),
                       E)]](((ca: C) => (d: D) =>
                              ((e: List[((A, C), E)]) =>
                                ((a, ca), (((f(a))(b))(ca))(d)) :: e)),
                             rbt2, c))),
      rbt1, Nil)))

def productf[A : ccompare, D, B : ccompare, E,
              C](xc: A => D => B => E => C, xd: mapping_rbt[A, D],
                  xe: mapping_rbt[B, E]):
      mapping_rbt[(A, B), C]
  =
  Mapping_rbtb[(A, B),
                C](rbt_product[A, D, B, E,
                                C](xc, impl_ofa[A, D](xd), impl_ofa[B, E](xe)))

def productb[A : ccompare,
              B : ccompare](rbt1: mapping_rbt[A, Unit],
                             rbt2: mapping_rbt[B, Unit]):
      mapping_rbt[(A, B), Unit]
  =
  productf[A, Unit, B, Unit,
            Unit](((_: A) => (_: Unit) => (_: B) => (_: Unit) => ()), rbt1,
                   rbt2)

def producte[A : ceq : ccompare : set_impl,
              B : ceq : ccompare : set_impl](a2: set[A], b2: set[B]):
      set[(A, B)]
  =
  (a2, b2) match {
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("product RBT_set RBT_set: ccompare1 = None");
           (((_: Unit) =>
              producte[A, B](Rbt_set[A](rbt1), Rbt_set[B](rbt2)))).apply(())
           }
       case Some(_) =>
         (ccompare[B] match {
            case None =>
              { sys.error("product RBT_set RBT_set: ccompare2 = None");
                (((_: Unit) =>
                   producte[A, B](Rbt_set[A](rbt1),
                                   Rbt_set[B](rbt2)))).apply(())
                }
            case Some(_) => Rbt_set[(A, B)](productb[A, B](rbt1, rbt2))
          })
     })
  case (a2, Rbt_set(rbt2)) =>
    (ccompare[B] match {
       case None =>
         { sys.error("product RBT_set: ccompare2 = None");
           (((_: Unit) => producte[A, B](a2, Rbt_set[B](rbt2)))).apply(()) }
       case Some(_) =>
         (foldb[B, set[(A, B)]](((y: B) =>
                                  ((a: set[(A, B)]) =>
                                    sup_seta[(A,
       B)](image[A, (A, B)](((x: A) => (x, y)), a2), a))),
                                 rbt2)).apply(bot_set[(A, B)])
     })
  case (Rbt_set(rbt1), b2) =>
    (ccompare[A] match {
       case None =>
         { sys.error("product RBT_set: ccompare1 = None");
           (((_: Unit) => producte[A, B](Rbt_set[A](rbt1), b2))).apply(()) }
       case Some(_) =>
         (foldb[A, set[(A, B)]](((x: A) =>
                                  ((a: set[(A, B)]) =>
                                    sup_seta[(A,
       B)](image[B, (A, B)](((aa: B) => (x, aa)), b2), a))),
                                 rbt1)).apply(bot_set[(A, B)])
     })
  case (Dlist_set(dxs), Dlist_set(dys)) =>
    (ceq[A] match {
       case None =>
         { sys.error("product DList_set DList_set: ceq1 = None");
           (((_: Unit) =>
              producte[A, B](Dlist_set[A](dxs), Dlist_set[B](dys)))).apply(())
           }
       case Some(_) =>
         (ceq[B] match {
            case None =>
              { sys.error("product DList_set DList_set: ceq2 = None");
                (((_: Unit) =>
                   producte[A, B](Dlist_set[A](dxs),
                                   Dlist_set[B](dys)))).apply(())
                }
            case Some(_) => Dlist_set[(A, B)](productc[A, B](dxs, dys))
          })
     })
  case (a1, Dlist_set(dys)) =>
    (ceq[B] match {
       case None =>
         { sys.error("product DList_set2: ceq = None");
           (((_: Unit) => producte[A, B](a1, Dlist_set[B](dys)))).apply(()) }
       case Some(_) =>
         (foldc[B, set[(A, B)]](((y: B) =>
                                  ((a: set[(A, B)]) =>
                                    sup_seta[(A,
       B)](image[A, (A, B)](((x: A) => (x, y)), a1), a))),
                                 dys)).apply(bot_set[(A, B)])
     })
  case (Dlist_set(dxs), b1) =>
    (ceq[A] match {
       case None =>
         { sys.error("product DList_set1: ceq = None");
           (((_: Unit) => producte[A, B](Dlist_set[A](dxs), b1))).apply(()) }
       case Some(_) =>
         (foldc[A, set[(A, B)]](((x: A) =>
                                  ((a: set[(A, B)]) =>
                                    sup_seta[(A,
       B)](image[B, (A, B)](((aa: B) => (x, aa)), b1), a))),
                                 dxs)).apply(bot_set[(A, B)])
     })
  case (Set_monad(xs), Set_monad(ys)) =>
    Set_monad[(A, B)](fold[A, List[(A, B)]](((x: A) =>
      ((a: List[(A, B)]) =>
        fold[B, List[(A, B)]](((y: B) => ((aa: List[(A, B)]) => (x, y) :: aa)),
                               ys, a))),
     xs, Nil))
  case (a, b) =>
    Collect_set[(A, B)](((c: (A, B)) => {
  val (x, y) = c: ((A, B));
  member[A](x, a) && member[B](y, b)
}))
}

def product_impl[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                  B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                  C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                  D : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](a:
                      fsm_with_precomputations_impl[A, B, C],
                     b: fsm_with_precomputations_impl[D, B, C]):
      fsm_with_precomputations_impl[(A, D), B, C]
  =
  {
    val ts =
      image[((A, (B, (C, A))), (D, (B, (C, D)))),
             ((A, D),
               (B, (C, (A, D))))](((aa: ((A, (B, (C, A))), (D, (B, (C, D))))) =>
                                    {
                                      val (ab, ba) =
aa: (((A, (B, (C, A))), (D, (B, (C, D)))));
                                      ({
 val (qA, (x, (y, qAa))) = ab: ((A, (B, (C, A))));
 ((ac: (D, (B, (C, D)))) => {
                              val (qB, (_, (_, qBa))) = ac: ((D, (B, (C, D))));
                              ((qA, qB), (x, (y, (qAa, qBa))))
                            })
                                       })(ba)
                                    }),
                                   filter[((A, (B, (C, A))),
    (D, (B, (C, D))))](((aa: ((A, (B, (C, A))), (D, (B, (C, D))))) =>
                         {
                           val (ab, ba) =
                             aa: (((A, (B, (C, A))), (D, (B, (C, D)))));
                           ({
                              val (_, (x, (y, _))) = ab: ((A, (B, (C, A))));
                              ((ac: (D, (B, (C, D)))) =>
                                {
                                  val (_, (xa, (ya, _))) =
                                    ac: ((D, (B, (C, D))));
                                  eq[B](x, xa) && eq[C](y, ya)
                                })
                            })(ba)
                         }),
                        Sup_setb[((A, (B, (C, A))),
                                   (D, (B,
 (C, D))))](image[(A, (B, (C, A))),
                   set[((A, (B, (C, A))),
                         (D, (B, (C, D))))]](((tA: (A, (B, (C, A)))) =>
       image[(D, (B, (C, D))),
              ((A, (B, (C, A))),
                (D, (B, (C, D))))](((aa: (D, (B, (C, D)))) => (tA, aa)),
                                    transitions_wpi[D, B, C](b))),
      transitions_wpi[A, B, C](a))))):
        (set[((A, D), (B, (C, (A, D))))])
    val h =
      set_as_mapping_image[(A, D), (B, (C, (A, D))), ((A, D), B),
                            (C, (A, D))](ts,
  ((aa: ((A, D), (B, (C, (A, D))))) =>
    {
      val (q, (x, (y, qa))) = aa: (((A, D), (B, (C, (A, D)))));
      ((q, x), (y, qa))
    })):
        (mapping[((A, D), B), set[(C, (A, D))]]);
    FSMWPI[(A, D), B,
            C]((initial_wpi[A, B, C](a), initial_wpi[D, B, C](b)),
                producte[A, D](states_wpi[A, B, C](a), states_wpi[D, B, C](b)),
                sup_seta[B](inputs_wpi[A, B, C](a), inputs_wpi[D, B, C](b)),
                sup_seta[C](outputs_wpi[A, B, C](a), outputs_wpi[D, B, C](b)),
                ts, h, h_obs_impl_from_h[(A, D), B, C](h))
  }

def productg[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              D : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](xb:
                  fsm_with_precomputations[A, C, D],
                 xc: fsm_with_precomputations[B, C, D]):
      fsm_with_precomputations[(A, B), C, D]
  =
  Fsm_with_precomputationsa[(A, B), C,
                             D](product_impl[A, C, D,
      B](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, C, D](xb),
          fsm_with_precomputations_impl_of_fsm_with_precomputations[B, C,
                             D](xc)))

def producta[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              D : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](x0:
                  fsm_impl[A, B, C],
                 x1: fsm_impl[D, B, C]):
      fsm_impl[(A, D), B, C]
  =
  (x0, x1) match {
  case (FSMWP(a), FSMWP(b)) => FSMWP[(A, D), B, C](productg[A, B, C, D](a, b))
}

def zip_with_index_from[A](n: nat, x1: List[A]): List[(nat, A)] = (n, x1) match
  {
  case (n, x :: xs) => (n, x) :: zip_with_index_from[A](Suc(n), xs)
  case (n, Nil) => Nil
}

def rbt_comp_bulkload[A, B](c: A => A => ordera, xs: List[(A, B)]): rbt[A, B] =
  (foldr[(A, B),
          rbt[A, B]](((a: (A, B)) =>
                       {
                         val (aa, b) = a: ((A, B));
                         (rbt_comp_insert[A, B](c)).apply(aa).apply(b)
                       }),
                      xs)).apply(Emptyd[A, B]())

def bulkloada[A : ccompare, B](xa: List[(A, B)]): mapping_rbt[A, B] =
  Mapping_rbtb[A, B](rbt_comp_bulkload[A,
B](the[A => A => ordera](ccompare[A]), xa))

def bulkload[A](vs: List[A]): mapping[nat, A] =
  Rbt_mapping[nat, A](bulkloada[nat, A](zip_with_index_from[A](zero_nat, vs)))

def tabulate[A : ccompare : equal : mapping_impl, B](xs: List[A], f: A => B):
      mapping[A, B]
  =
  fold[A, mapping[A, B]](((k: A) =>
                           ((a: mapping[A, B]) => updateb[A, B](k, f(k), a))),
                          xs, emptya[A, B])

def isin[A : ccompare : equal](x0: prefix_tree[A], xs: List[A]): Boolean =
  (x0, xs) match {
  case (MPT(m), xs) =>
    (xs match {
       case Nil => true
       case x :: xsa => ((lookupa[A, prefix_tree[A]](m)).apply(x) match {
                           case None => false
                           case Some(t) => isin[A](t, xsa)
                         })
     })
}

def prefixes[A](xs: List[A]): List[List[A]] =
  rev[List[A]](snd[List[A],
                    List[List[A]]](foldl[(List[A], List[List[A]]),
  A](((a: (List[A], List[List[A]])) =>
       {
         val (acc1, acc2) = a: ((List[A], List[List[A]]));
         ((x: A) => (x :: acc1, rev[A](x :: acc1) :: acc2))
       }),
      (Nil, List(Nil)), xs)))

def find_removea[A](p: A => Boolean, x1: List[A], uu: List[A]):
      Option[(A, List[A])]
  =
  (p, x1, uu) match {
  case (p, Nil, uu) => None
  case (p, x :: xs, prev) =>
    (if (p(x)) Some[(A, List[A])]((x, prev ++ xs))
      else find_removea[A](p, xs, prev ++ List(x)))
}

def find_remove[A](p: A => Boolean, xs: List[A]): Option[(A, List[A])] =
  find_removea[A](p, xs, Nil)

def separate_by[A](p: A => Boolean, xs: List[A]): (List[A], List[A]) =
  (foldr[A, (List[A],
              List[A])](((x: A) => (a: (List[A], List[A])) =>
                          {
                            val (prevPass, prevFail) = a: ((List[A], List[A]));
                            (if (p(x)) (x :: prevPass, prevFail)
                              else (prevPass, x :: prevFail))
                          }),
                         xs)).apply((Nil, Nil))

def filter_states_impl[A : card_univ : ceq : ccompare : set_impl,
                        B : ceq : ccompare,
                        C : ceq : ccompare : equal : mapping_impl](m:
                             fsm_with_precomputations_impl[A, B, C],
                            p: A => Boolean):
      fsm_with_precomputations_impl[A, B, C]
  =
  (if (p(initial_wpi[A, B, C](m)))
    {
      val h =
        filterc[(A, B),
                 set[(C, A)]](((a: (A, B)) => {
        val (q, _) = a: ((A, B));
        ((_: set[(C, A)]) => p(q))
      }),
                               h_wpi[A, B, C](m)):
          (mapping[(A, B), set[(C, A)]])
      val ha =
        map_values[(A, B), set[(C, A)],
                    set[(C, A)]](((_: (A, B)) =>
                                   ((a: set[(C, A)]) =>
                                     filter[(C,
      A)](((aa: (C, A)) => {
                             val (_, ab) = aa: ((C, A));
                             p(ab)
                           }),
           a))),
                                  h):
          (mapping[(A, B), set[(C, A)]]);
      FSMWPI[A, B,
              C](initial_wpi[A, B, C](m), filter[A](p, states_wpi[A, B, C](m)),
                  inputs_wpi[A, B, C](m), outputs_wpi[A, B, C](m),
                  filter[(A, (B, (C, A)))](((t: (A, (B, (C, A)))) =>
     p(fst[A, (B, (C, A))](t)) &&
       p(snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
    transitions_wpi[A, B, C](m)),
                  ha, h_obs_impl_from_h[A, B, C](ha))
    }
    else m)

def filter_statesb[A : card_univ : ceq : ccompare : set_impl,
                    B : ceq : ccompare,
                    C : ceq : ccompare : equal : mapping_impl](xb:
                         fsm_with_precomputations[A, B, C],
                        xc: A => Boolean):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](filter_states_impl[A, B,
            C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                                  C](xb),
                xc))

def filter_statesa[A : card_univ : ceq : ccompare : set_impl,
                    B : ceq : ccompare,
                    C : ceq : ccompare : equal : mapping_impl](x0:
                         fsm_impl[A, B, C],
                        p: A => Boolean):
      fsm_impl[A, B, C]
  =
  (x0, p) match {
  case (FSMWP(m), p) => FSMWP[A, B, C](filter_statesb[A, B, C](m, p))
}

def filter_states[A : card_univ : ceq : ccompare : set_impl, B : ceq : ccompare,
                   C : ceq : ccompare : equal : mapping_impl](xb: fsm[A, B, C],
                       xc: A => Boolean):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](filter_statesa[A, B, C](fsm_impl_of_fsm[A, B, C](xb), xc))

def list_as_mapping[A : ccompare : equal : mapping_impl,
                     B : ceq : ccompare : set_impl](xs: List[(A, B)]):
      mapping[A, set[B]]
  =
  (foldr[(A, B),
          mapping[A, set[B]]](((a: (A, B)) =>
                                {
                                  val (x, z) = a: ((A, B));
                                  ((m: mapping[A, set[B]]) =>
                                    ((lookupa[A, set[B]](m)).apply(x) match {
                                       case None =>
 updateb[A, set[B]](x, insert[B](z, bot_set[B]), m)
                                       case Some(zs) =>
 updateb[A, set[B]](x, insert[B](z, zs), m)
                                     }))
                                }),
                               xs)).apply(emptya[A, set[B]])

def fsm_with_precomputations_impl_from_lista[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
      B : ceq : ccompare : equal : mapping_impl : set_impl,
      C : ceq : ccompare : equal : mapping_impl : set_impl](q: A,
                     x1: List[(A, (B, (C, A)))]):
      fsm_with_precomputations_impl[A, B, C]
  =
  (q, x1) match {
  case (q, Nil) =>
    FSMWPI[A, B,
            C](q, insert[A](q, bot_set[A]), bot_set[B], bot_set[C],
                bot_set[(A, (B, (C, A)))], emptya[(A, B), set[(C, A)]],
                emptya[(A, B), mapping[C, A]])
  case (q, t :: ts) =>
    {
      val tsr = remdups[(A, (B, (C, A)))](t :: ts): (List[(A, (B, (C, A)))])
      val h =
        list_as_mapping[(A, B),
                         (C, A)](mapa[(A, (B, (C, A))),
                                       ((A, B),
 (C, A))](((a: (A, (B, (C, A)))) =>
            {
              val (qb, (x, (y, qa))) = a: ((A, (B, (C, A))));
              ((qb, x), (y, qa))
            }),
           tsr)):
          (mapping[(A, B), set[(C, A)]]);
      FSMWPI[A, B,
              C](fst[A, (B, (C, A))](t),
                  set[A](remdups[A](mapa[(A, (B, (C, A))),
  A](((a: (A, (B, (C, A)))) => fst[A, (B, (C, A))](a)), tsr) ++
                                      mapa[(A, (B, (C, A))),
    A](((a: (A, (B, (C, A)))) =>
         snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](a)))),
        tsr))),
                  set[B](remdups[B](mapa[(A, (B, (C, A))),
  B](((a: (A, (B, (C, A)))) => fst[B, (C, A)](snd[A, (B, (C, A))](a))), tsr))),
                  set[C](remdups[C](mapa[(A, (B, (C, A))),
  C](((a: (A, (B, (C, A)))) =>
       fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](a)))),
      tsr))),
                  set[(A, (B, (C, A)))](tsr), h, h_obs_impl_from_h[A, B, C](h))
    }
}

def fsm_with_precomputations_impl_from_list[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
     B : ceq : ccompare : equal : mapping_impl : set_impl,
     C : ceq : ccompare : equal : mapping_impl : set_impl](q: A,
                    ts: List[(A, (B, (C, A)))]):
      fsm_with_precomputations_impl[A, B, C]
  =
  fsm_with_precomputations_impl_from_lista[A, B, C](q, ts)

def fsm_with_precomputations_from_list[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
B : ceq : ccompare : equal : mapping_impl : set_impl,
C : ceq : ccompare : equal : mapping_impl : set_impl](q: A,
               ts: List[(A, (B, (C, A)))]):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](fsm_with_precomputations_impl_from_list[A, B,
                                 C](q, ts))

def fsm_impl_from_list[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                        B : ceq : ccompare : equal : mapping_impl : set_impl,
                        C : ceq : ccompare : equal : mapping_impl : set_impl](q:
A,
                                       ts: List[(A, (B, (C, A)))]):
      fsm_impl[A, B, C]
  =
  FSMWP[A, B, C](fsm_with_precomputations_from_list[A, B, C](q, ts))

def fsm_from_list[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                   B : ceq : ccompare : equal : mapping_impl : set_impl,
                   C : ceq : ccompare : equal : mapping_impl : set_impl](xb: A,
                                  xc: List[(A, (B, (C, A)))]):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](fsm_impl_from_list[A, B, C](xb, xc))

def emptyc[A : ccompare : mapping_impl]: prefix_tree[A] =
  MPT[A](emptya[A, prefix_tree[A]])

def aftera[A : ccompare : equal : mapping_impl](x0: prefix_tree[A],
         xs: List[A]):
      prefix_tree[A]
  =
  (x0, xs) match {
  case (MPT(m), xs) =>
    (xs match {
       case Nil => MPT[A](m)
       case x :: xsa => ((lookupa[A, prefix_tree[A]](m)).apply(x) match {
                           case None => emptyc[A]
                           case Some(t) => aftera[A](t, xsa)
                         })
     })
}

def prefix_pairs[A](x0: List[A]): List[(List[A], List[A])] = x0 match {
  case Nil => Nil
  case v :: va =>
    prefix_pairs[A](butlast[A](v :: va)) ++
      mapa[List[A],
            (List[A],
              List[A])](((ys: List[A]) => (ys, v :: va)),
                         butlast[List[A]](prefixes[A](v :: va)))
}

def is_in_language[A : ccompare : equal, B : ccompare : equal,
                    C : ccompare : equal](m: fsm[A, B, C], q: A,
   x2: List[(B, C)]):
      Boolean
  =
  (m, q, x2) match {
  case (m, q, Nil) => true
  case (m, q, (x, y) :: io) =>
    ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y) match {
       case None => false
       case Some(qa) => is_in_language[A, B, C](m, qa, io)
     })
}

def insertb[A : ccompare : equal : mapping_impl](x0: prefix_tree[A],
          xs: List[A]):
      prefix_tree[A]
  =
  (x0, xs) match {
  case (MPT(m), xs) =>
    (xs match {
       case Nil => MPT[A](m)
       case x :: xsa =>
         MPT[A](updateb[A, prefix_tree[A]](x,
    insertb[A](((lookupa[A, prefix_tree[A]](m)).apply(x) match {
                  case None => emptyc[A]
                  case Some(t) => t
                }),
                xsa),
    m))
     })
}

def find_remove_2a[A, B](p: A => B => Boolean, x1: List[A], uu: List[B],
                          uv: List[A]):
      Option[(A, (B, List[A]))]
  =
  (p, x1, uu, uv) match {
  case (p, Nil, uu, uv) => None
  case (p, x :: xs, ys, prev) =>
    (find[B](p(x), ys) match {
       case None => find_remove_2a[A, B](p, xs, ys, prev ++ List(x))
       case Some(y) => Some[(A, (B, List[A]))]((x, (y, prev ++ xs)))
     })
}

def find_remove_2[A, B](p: A => B => Boolean, xs: List[A], ys: List[B]):
      Option[(A, (B, List[A]))]
  =
  find_remove_2a[A, B](p, xs, ys, Nil)

def add_transitions_impl[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                          B : ceq : ccompare : equal : mapping_impl,
                          C : ceq : ccompare : equal : mapping_impl : set_impl](m:
  fsm_with_precomputations_impl[A, B, C],
 ts: set[(A, (B, (C, A)))]):
      fsm_with_precomputations_impl[A, B, C]
  =
  (if (Ball[(A, (B, (C, A)))](ts, ((t: (A, (B, (C, A)))) =>
                                    member[A](fst[A, (B, (C, A))](t),
       states_wpi[A, B, C](m)) &&
                                      (member[B](fst[B,
              (C, A)](snd[A, (B, (C, A))](t)),
          inputs_wpi[A, B, C](m)) &&
(member[C](fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
            outputs_wpi[A, B, C](m)) &&
  member[A](snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
             states_wpi[A, B, C](m)))))))
    {
      val tsa =
        sup_seta[(A, (B, (C, A)))](transitions_wpi[A, B, C](m), ts):
          (set[(A, (B, (C, A)))])
      val h =
        set_as_mapping_image[A, (B, (C, A)), (A, B),
                              (C, A)](tsa, ((a: (A, (B, (C, A)))) =>
     {
       val (q, (x, (y, qa))) = a: ((A, (B, (C, A))));
       ((q, x), (y, qa))
     })):
          (mapping[(A, B), set[(C, A)]]);
      FSMWPI[A, B,
              C](initial_wpi[A, B, C](m), states_wpi[A, B, C](m),
                  inputs_wpi[A, B, C](m), outputs_wpi[A, B, C](m), tsa, h,
                  h_obs_impl_from_h[A, B, C](h))
    }
    else m)

def add_transitionsb[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                      B : ceq : ccompare : equal : mapping_impl,
                      C : ceq : ccompare : equal : mapping_impl : set_impl](xb:
                                      fsm_with_precomputations[A, B, C],
                                     xc: set[(A, (B, (C, A)))]):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](add_transitions_impl[A, B,
              C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A, B,
                                    C](xb),
                  xc))

def add_transitionsa[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                      B : ceq : ccompare : equal : mapping_impl,
                      C : ceq : ccompare : equal : mapping_impl : set_impl](x0:
                                      fsm_impl[A, B, C],
                                     ts: set[(A, (B, (C, A)))]):
      fsm_impl[A, B, C]
  =
  (x0, ts) match {
  case (FSMWP(m), ts) => FSMWP[A, B, C](add_transitionsb[A, B, C](m, ts))
}

def add_transitions[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                     B : ceq : ccompare : equal : mapping_impl,
                     C : ceq : ccompare : equal : mapping_impl : set_impl](xb:
                                     fsm[A, B, C],
                                    xc: set[(A, (B, (C, A)))]):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](add_transitionsa[A, B, C](fsm_impl_of_fsm[A, B, C](xb), xc))

def combinea[A : ccompare](x0: prefix_tree[A], x1: prefix_tree[A]):
      prefix_tree[A]
  =
  (x0, x1) match {
  case (MPT(Rbt_mapping(m1)), MPT(Rbt_mapping(m2))) =>
    (ccompare[A] match {
       case None =>
         { sys.error("combine_MPT_RBT_Mapping: ccompare = None");
           (((_: Unit) =>
              combinea[A](MPT[A](Rbt_mapping[A, prefix_tree[A]](m1)),
                           MPT[A](Rbt_mapping[A,
       prefix_tree[A]](m2))))).apply(())
           }
       case Some(_) =>
         MPT[A](Rbt_mapping[A, prefix_tree[A]](join[A,
             prefix_tree[A]](((_: A) =>
                               ((a: prefix_tree[A]) => (b: prefix_tree[A]) =>
                                 combinea[A](a, b))),
                              m1, m2)))
     })
}

def is_leaf[A : ccompare](x0: prefix_tree[A]): Boolean = x0 match {
  case MPT(Rbt_mapping(m)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("is_leaf_MPT_RBT_Mapping: ccompare = None");
           (((_: Unit) =>
              is_leaf[A](MPT[A](Rbt_mapping[A, prefix_tree[A]](m))))).apply(())
           }
       case Some(_) => is_emptya[A, prefix_tree[A]](m)
     })
}

def productd[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              D : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
              B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](xb:
                  fsm[A, C, D],
                 xc: fsm[B, C, D]):
      fsm[(A, B), C, D]
  =
  Abs_fsm[(A, B), C,
           D](producta[A, C, D,
                        B](fsm_impl_of_fsm[A, C, D](xb),
                            fsm_impl_of_fsm[B, C, D](xc)))

def remove_subsets[A : cenum : ceq : ccompare](x0: List[set[A]]): List[set[A]] =
  x0 match {
  case Nil => Nil
  case x :: xs =>
    (find_remove[set[A]](((a: set[A]) => less_set[A](x, a)), xs) match {
       case None =>
         x :: remove_subsets[A](filtera[set[A]](((y: set[A]) =>
          ! (less_eq_set[A](y, x))),
         xs))
       case Some((y, xsa)) =>
         remove_subsets[A](y :: filtera[set[A]](((ya: set[A]) =>
          ! (less_eq_set[A](ya, x))),
         xsa))
     })
}

def does_distinguish[A : ccompare : equal, B : ccompare : equal,
                      C : ccompare : equal](m: fsm[A, B, C], q1: A, q2: A,
     io: List[(B, C)]):
      Boolean
  =
  ! (equal_boola(is_in_language[A, B, C](m, q1, io),
                  is_in_language[A, B, C](m, q2, io)))

def reaching_paths_up_to_depth[A : ceq : ccompare : equal : linorder : set_impl,
                                B : ceq : ccompare : equal : linorder,
                                C : ceq : ccompare : equal : linorder](m:
                                 fsm[A, B, C],
                                nexts: set[A], dones: set[A],
                                assignment: A => Option[List[(A, (B, (C, A)))]],
                                k: nat):
      A => Option[List[(A, (B, (C, A)))]]
  =
  (if (equal_nata(k, zero_nat)) assignment
    else {
           val usable_transitions =
             filtera[(A, (B, (C, A)))](((t: (A, (B, (C, A)))) =>
 member[A](fst[A, (B, (C, A))](t), nexts) &&
   (! (member[A](snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))), dones)) &&
     ! (member[A](snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))), nexts)))),
transitions_as_list[A, B, C](m)):
               (List[(A, (B, (C, A)))])
           val targets =
             mapa[(A, (B, (C, A))),
                   A](((a: (A, (B, (C, A)))) =>
                        snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](a)))),
                       usable_transitions):
               (List[A])
           val transition_choice =
             map_upds[A, (A, (B, (C, A)))](((_: A) => None), targets,
    usable_transitions):
               (A => Option[(A, (B, (C, A)))])
           val assignmenta =
             map_upds[A, List[(A, (B, (C,
A)))]](assignment, targets,
        mapa[A, List[(A, (B, (C, A)))]](((q: A) =>
  {
    val (Some(t)) = transition_choice(q): Option[(A, (B, (C, A)))]
    val (Some(p)) =
      assignment(fst[A, (B, (C, A))](t)): Option[List[(A, (B, (C, A)))]];
    p ++ List(t)
  }),
 targets)):
               (A => Option[List[(A, (B, (C, A)))]])
           val nextsa = set[A](targets): (set[A])
           val donesa = sup_seta[A](nexts, dones): (set[A]);
           reaching_paths_up_to_depth[A, B,
                                       C](m, nextsa, donesa, assignmenta,
   minus_nat(k, one_nata))
         })

def reachable_states[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                      B : ceq : ccompare : equal : linorder,
                      C : ceq : ccompare : equal : linorder](m: fsm[A, B, C]):
      set[A]
  =
  {
    val path_assignments =
      reaching_paths_up_to_depth[A, B,
                                  C](m, insert[A](initial[A, B, C](m),
           bot_set[A]),
                                      bot_set[A],
                                      fun_upd[A,
       Option[List[(A, (B, (C, A)))]]](((_: A) => None), initial[A, B, C](m),
Some[List[(A, (B, (C, A)))]](Nil)),
                                      minus_nat(size[A, B, C](m), one_nata)):
        (A => Option[List[(A, (B, (C, A)))]]);
    filter[A](((q: A) =>
                ! (is_none[List[(A, (B, (C, A)))]](path_assignments(q)))),
               states[A, B, C](m))
  }

def entriesa[A : ccompare, B](xa: mapping_rbt[A, B]): List[(A, B)] =
  entries[A, B].apply(impl_ofa[A, B](xa))

def mergesort_by_rel_split[A](x0: (List[A], List[A]), x1: List[A]):
      (List[A], List[A])
  =
  (x0, x1) match {
  case ((xs1, xs2), Nil) => (xs1, xs2)
  case ((xs1, xs2), List(x)) => (x :: xs1, xs2)
  case ((xs1, xs2), x1 :: x2 :: xs) =>
    mergesort_by_rel_split[A]((x1 :: xs1, x2 :: xs2), xs)
}

def mergesort_by_rel_merge[A](r: A => A => Boolean, xs: List[A], x2: List[A]):
      List[A]
  =
  (r, xs, x2) match {
  case (r, x :: xs, y :: ys) =>
    (if ((r(x))(y)) x :: mergesort_by_rel_merge[A](r, xs, y :: ys)
      else y :: mergesort_by_rel_merge[A](r, x :: xs, ys))
  case (r, xs, Nil) => xs
  case (r, Nil, v :: va) => v :: va
}

def mergesort_by_rel[A](r: A => A => Boolean, x1: List[A]): List[A] = (r, x1)
  match {
  case (r, x1 :: x2 :: xs) =>
    {
      val (xs1, xs2) =
        mergesort_by_rel_split[A]((List(x1), List(x2)), xs):
          ((List[A], List[A]));
      mergesort_by_rel_merge[A](r, mergesort_by_rel[A](r, xs1),
                                 mergesort_by_rel[A](r, xs2))
    }
  case (r, List(x)) => List(x)
  case (r, Nil) => Nil
}

def from_list[A : ccompare : equal : mapping_impl](xs: List[List[A]]):
      prefix_tree[A]
  =
  (foldr[List[A],
          prefix_tree[A]](((x: List[A]) => (t: prefix_tree[A]) =>
                            insertb[A](t, x)),
                           xs)).apply(emptyc[A])

def filter_transitions_impl[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                             B : ceq : ccompare : equal : mapping_impl,
                             C : ceq : ccompare : equal : mapping_impl : set_impl](m:
     fsm_with_precomputations_impl[A, B, C],
    p: ((A, (B, (C, A)))) => Boolean):
      fsm_with_precomputations_impl[A, B, C]
  =
  {
    val ts =
      filter[(A, (B, (C, A)))](p, transitions_wpi[A, B, C](m)):
        (set[(A, (B, (C, A)))])
    val h =
      set_as_mapping_image[A, (B, (C, A)), (A, B),
                            (C, A)](ts, ((a: (A, (B, (C, A)))) =>
  {
    val (q, (x, (y, qa))) = a: ((A, (B, (C, A))));
    ((q, x), (y, qa))
  })):
        (mapping[(A, B), set[(C, A)]]);
    FSMWPI[A, B,
            C](initial_wpi[A, B, C](m), states_wpi[A, B, C](m),
                inputs_wpi[A, B, C](m), outputs_wpi[A, B, C](m), ts, h,
                h_obs_impl_from_h[A, B, C](h))
  }

def filter_transitionsb[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                         B : ceq : ccompare : equal : mapping_impl,
                         C : ceq : ccompare : equal : mapping_impl : set_impl](xb:
 fsm_with_precomputations[A, B, C],
xc: ((A, (B, (C, A)))) => Boolean):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](filter_transitions_impl[A, B,
                 C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
                                       B, C](xb),
                     xc))

def filter_transitionsa[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                         B : ceq : ccompare : equal : mapping_impl,
                         C : ceq : ccompare : equal : mapping_impl : set_impl](x0:
 fsm_impl[A, B, C],
p: ((A, (B, (C, A)))) => Boolean):
      fsm_impl[A, B, C]
  =
  (x0, p) match {
  case (FSMWP(m), p) => FSMWP[A, B, C](filter_transitionsb[A, B, C](m, p))
}

def filter_transitions[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
                        B : ceq : ccompare : equal : mapping_impl,
                        C : ceq : ccompare : equal : mapping_impl : set_impl](xb:
fsm[A, B, C],
                                       xc: ((A, (B, (C, A)))) => Boolean):
      fsm[A, B, C]
  =
  Abs_fsm[A, B,
           C](filter_transitionsa[A, B, C](fsm_impl_of_fsm[A, B, C](xb), xc))

def language_for_input[A : ccompare : equal : linorder,
                        B : ccompare : equal : linorder,
                        C : ceq : ccompare : equal : linorder](m: fsm[A, B, C],
                        q: A, x2: List[B]):
      List[List[(B, C)]]
  =
  (m, q, x2) match {
  case (m, q, Nil) => List(Nil)
  case (m, q, x :: xs) =>
    {
      val a = outputs_as_list[A, B, C](m): (List[C]);
      maps[C, List[(B, C)]](((y: C) =>
                              ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y)
                                 match {
                                 case None => Nil
                                 case Some(qa) =>
                                   mapa[List[(B, C)],
 List[(B, C)]](((aa: List[(B, C)]) => (x, y) :: aa),
                language_for_input[A, B, C](m, qa, xs))
                               })),
                             a)
    }
}

def lookup_default[A, B : ccompare : equal](d: A, m: mapping[B, A], k: B): A =
  ((lookupa[B, A](m)).apply(k) match {
     case None => d
     case Some(v) => v
   })

def covered_transitions[A : ceq : ccompare : equal : mapping_impl : set_impl,
                         B : ceq : ccompare : equal : mapping_impl : set_impl,
                         C : ceq : ccompare : equal : set_impl](m: fsm[A, B, C],
                         v: A => List[(B, C)], alpha: List[(B, C)]):
      set[(A, (B, (C, A)))]
  =
  {
    val ts =
      the_elem[List[(A, (B, (C, A)))]](paths_for_io[A, B,
             C](m, initial[A, B, C](m), alpha)):
        (List[(A, (B, (C, A)))]);
    set[(A, (B, (C, A)))](filtera[(A, (B,
(C, A)))](((t: (A, (B, (C, A)))) =>
            equal_lista[(B, C)](v(fst[A, (B, (C, A))](t)) ++
                                  List((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
 fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
                                 v(snd[C,
A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))))),
           ts))
  }

def reachable_states_as_list[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                              B : ceq : ccompare : equal : linorder,
                              C : ceq : ccompare : equal : linorder](m:
                               fsm[A, B, C]):
      List[A]
  =
  sorted_list_of_set[A](reachable_states[A, B, C](m))

def h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                 B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                 C : finite_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl,
                 D](ma: fsm[A, B, C],
                     get_state_cover: (fsm[A, B, C]) => A => List[(B, C)],
                     handle_state_cover:
                       (fsm[A, B, C]) =>
                         (A => List[(B, C)]) =>
                           ((fsm[A, B, C]) => (prefix_tree[(B, C)]) => D) =>
                             (D => (List[(B, C)]) => D) =>
                               (D => (List[(B, C)]) => List[List[(B, C)]]) =>
                                 (prefix_tree[(B, C)], D),
                     sort_transitions:
                       (fsm[A, B, C]) =>
                         (A => List[(B, C)]) =>
                           (List[(A, (B, (C, A)))]) => List[(A, (B, (C, A)))],
                     handle_unverified_transition:
                       (fsm[A, B, C]) =>
                         (A => List[(B, C)]) =>
                           (prefix_tree[(B, C)]) =>
                             D => (D => (List[(B, C)]) => D) =>
                                    (D => (List[(B, C)]) =>
    List[List[(B, C)]]) =>
                                      (D =>
(List[(B, C)]) => (List[(B, C)]) => D) =>
nat =>
  ((A, (B, (C, A)))) =>
    (List[(A, (B, (C, A)))]) =>
      (List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D)),
                     handle_unverified_io_pair:
                       (fsm[A, B, C]) =>
                         (A => List[(B, C)]) =>
                           (prefix_tree[(B, C)]) =>
                             D => (D => (List[(B, C)]) => D) =>
                                    (D => (List[(B, C)]) =>
    List[List[(B, C)]]) =>
                                      A => B => C => (prefix_tree[(B, C)], D),
                     cg_initial: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => D,
                     cg_insert: D => (List[(B, C)]) => D,
                     cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                     cg_merge: D => (List[(B, C)]) => (List[(B, C)]) => D,
                     m: nat):
      prefix_tree[(B, C)]
  =
  {
    val rstates_set = reachable_states[A, B, C](ma): (set[A])
    val rstates = reachable_states_as_list[A, B, C](ma): (List[A])
    val rstates_io =
      product[A, (B, C)](rstates,
                          product[B, C](inputs_as_list[A, B, C](ma),
 outputs_as_list[A, B, C](ma))):
        (List[(A, (B, C))])
    val undefined_io_pairs =
      filtera[(A, (B, C))](((a: (A, (B, C))) =>
                             {
                               val (q, (x, y)) = a: ((A, (B, C)));
                               is_none[A]((h_obs[A, B,
          C](ma)).apply(q).apply(x).apply(y))
                             }),
                            rstates_io):
        (List[(A, (B, C))])
    val v = get_state_cover(ma): (A => List[(B, C)])
    val tG1 =
      ((((handle_state_cover(ma))(v))(cg_initial))(cg_insert))(cg_lookup):
        ((prefix_tree[(B, C)], D))
    val sc_covered_transitions =
      Sup_setb[(A, (B, (C, A)))](image[A,
set[(A, (B, (C, A)))]](((q: A) => covered_transitions[A, B, C](ma, v, v(q))),
                        rstates_set)):
        (set[(A, (B, (C, A)))])
    val unverified_transitions =
      ((sort_transitions(ma))(v))(filtera[(A,
    (B, (C, A)))](((t: (A, (B, (C, A)))) =>
                    member[A](fst[A, (B, (C, A))](t), rstates_set) &&
                      ! (member[(A, (B, (C, A)))](t, sc_covered_transitions))),
                   transitions_as_list[A, B, C](ma))):
        (List[(A, (B, (C, A)))])
    val verify_transition =
      ((a: (List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D))) =>
        {
          val (x, (t, g)) =
            a: ((List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D)));
          ((ta: (A, (B, (C, A)))) =>
            (((((((((handle_unverified_transition(ma))(v))(t))(g))(cg_insert))(cg_lookup))(cg_merge))(m))(ta))(x))
        }):
        (((List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D))) =>
          ((A, (B, (C, A)))) =>
            (List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D)))
    val tG2 =
      snd[List[(A, (B, (C, A)))],
           (prefix_tree[(B, C)],
             D)](foldl[(List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D)),
                        (A, (B, (C, A)))](verify_transition,
   (unverified_transitions, tG1), unverified_transitions)):
        ((prefix_tree[(B, C)], D))
    val verify_undefined_io_pair =
      ((t: prefix_tree[(B, C)]) => (a: (A, (B, C))) =>
        {
          val (q, (x, y)) = a: ((A, (B, C)));
          fst[prefix_tree[(B, C)],
               D](((((((((handle_unverified_io_pair(ma))(v))(t))(snd[prefix_tree[(B,
   C)],
                              D](tG2)))(cg_insert))(cg_lookup))(q))(x))(y))
        }):
        ((prefix_tree[(B, C)]) => ((A, (B, C))) => prefix_tree[(B, C)]);
    foldl[prefix_tree[(B, C)],
           (A, (B, C))](verify_undefined_io_pair,
                         fst[prefix_tree[(B, C)], D](tG2), undefined_io_pairs)
  }

def emptye[A]: mp_trie[A] = Mp_triea[A](Nil)

def paths[A](x0: mp_trie[A]): List[List[A]] = x0 match {
  case Mp_triea(Nil) => List(Nil)
  case Mp_triea(t :: ts) =>
    maps[(A, mp_trie[A]),
          List[A]](((a: (A, mp_trie[A])) =>
                     {
                       val (x, ta) = a: ((A, mp_trie[A]));
                       mapa[List[A],
                             List[A]](((aa: List[A]) => x :: aa), paths[A](ta))
                     }),
                    t :: ts)
}

def ofsm_table[A : cenum : ceq : ccompare : equal : set_impl,
                B : ceq : ccompare : equal,
                C : ceq : ccompare : equal](m: fsm[A, B, C], f: A => set[A],
     k: nat, q: A):
      set[A]
  =
  (if (equal_nata(k, zero_nat))
    (if (member[A](q, states[A, B, C](m))) f(q) else bot_set[A])
    else {
           val prev_table =
             ((a: A) => ofsm_table[A, B, C](m, f, minus_nat(k, one_nata), a)):
               (A => set[A]);
           filter[A](((qa: A) =>
                       Ball[B](inputs[A, B, C](m),
                                ((x: B) =>
                                  Ball[C](outputs[A, B, C](m),
   ((y: C) =>
     ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y) match {
        case None => is_none[A]((h_obs[A, B, C](m)).apply(qa).apply(x).apply(y))
        case Some(qT) =>
          ((h_obs[A, B, C](m)).apply(qa).apply(x).apply(y) match {
             case None => false
             case Some(qTa) => set_eq[A](prev_table(qT), prev_table(qTa))
           })
      })))))),
                      prev_table(q))
         })

def min[A : ord](a: A, b: A): A = (if (less_eq[A](a, b)) a else b)

def select_inputs[A : card_univ : ceq : cproper_interval, B,
                   C : card_univ : ceq : cproper_interval](f:
                     ((A, B)) => set[(C, A)],
                    q0: A, inputList: List[B], x3: List[A], stateSet: set[A],
                    m: List[(A, B)]):
      List[(A, B)]
  =
  (f, q0, inputList, x3, stateSet, m) match {
  case (f, q0, inputList, Nil, stateSet, m) =>
    (find[B](((x: B) =>
               ! (is_empty[(C, A)](f((q0, x)))) &&
                 Ball[(C, A)](f((q0, x)),
                               ((a: (C, A)) => {
         val (_, q) = a: ((C, A));
         member[A](q, stateSet)
       }))),
              inputList)
       match {
       case None => m
       case Some(x) => m ++ List((q0, x))
     })
  case (f, q0, inputList, n :: nL, stateSet, m) =>
    (find[B](((x: B) =>
               ! (is_empty[(C, A)](f((q0, x)))) &&
                 Ball[(C, A)](f((q0, x)),
                               ((a: (C, A)) => {
         val (_, q) = a: ((C, A));
         member[A](q, stateSet)
       }))),
              inputList)
       match {
       case None =>
         (find_remove_2[A, B](((q: A) => (x: B) =>
                                ! (is_empty[(C, A)](f((q, x)))) &&
                                  Ball[(C,
 A)](f((q, x)), ((a: (C, A)) => {
                                  val (_, qa) = a: ((C, A));
                                  member[A](qa, stateSet)
                                }))),
                               n :: nL, inputList)
            match {
            case None => m
            case Some((q, (x, stateList))) =>
              select_inputs[A, B,
                             C](f, q0, inputList, stateList,
                                 insert[A](q, stateSet), m ++ List((q, x)))
          })
       case Some(x) => m ++ List((q0, x))
     })
}

def d_states[A : card_univ : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
              B : ceq : ccompare : equal : mapping_impl : linorder,
              C : card_univ : ceq : cproper_interval : set_impl](m:
                           fsm[A, B, C],
                          q: A):
      List[(A, B)]
  =
  (if (eq[A](q, initial[A, B, C](m))) Nil
    else select_inputs[A, B,
                        C](((a: (A, B)) => h[A, B, C](m, a)),
                            initial[A, B, C](m), inputs_as_list[A, B, C](m),
                            removeall[A](q,
  removeall[A](initial[A, B, C](m), states_as_list[A, B, C](m))),
                            insert[A](q, bot_set[A]), Nil))

def list_ordered_pairs[A](x0: List[A]): List[(A, A)] = x0 match {
  case Nil => Nil
  case x :: xs =>
    mapa[A, (A, A)](((a: A) => (x, a)), xs) ++ list_ordered_pairs[A](xs)
}

def minus_set[A : ceq : ccompare](a: set[A], b: set[A]): set[A] = (a, b) match {
  case (Rbt_set(rbt1), Rbt_set(rbt2)) =>
    (ccompare[A] match {
       case None =>
         { sys.error("minus RBT_set RBT_set: ccompare = None");
           (((_: Unit) =>
              minus_set[A](Rbt_set[A](rbt1), Rbt_set[A](rbt2)))).apply(())
           }
       case Some(_) => Rbt_set[A](minus[A](rbt1, rbt2))
     })
  case (a, b) => inf_seta[A](a, uminus_set[A](b))
}

def distinguishing_transitions[A : finite_univ : cenum : ceq : cproper_interval : set_impl,
                                B : finite_univ : cenum : ceq : cproper_interval : set_impl,
                                C : finite_univ : cenum : ceq : cproper_interval : set_impl](f:
               ((A, B)) => set[C],
              q1: A, q2: A, stateSet: set[(A, A)], inputSet: set[B]):
      set[(sum[(A, A), A], (B, (C, sum[(A, A), A])))]
  =
  Sup_setb[(sum[(A, A), A],
             (B, (C, sum[(A, A),
                          A])))](image[((A, A), B),
set[(sum[(A, A), A],
      (B, (C, sum[(A, A),
                   A])))]](((a: ((A, A), B)) =>
                             {
                               val (aa, b) = a: (((A, A), B));
                               ({
                                  val (q1a, q2a) = aa: ((A, A));
                                  ((x: B) =>
                                    sup_seta[(sum[(A, A), A],
       (B, (C, sum[(A, A),
                    A])))](image[C, (sum[(A, A), A],
                                      (B,
(C, sum[(A, A),
         A])))](((y: C) =>
                  (Inl[(A, A), A]((q1a, q2a)), (x, (y, Inr[A, (A, A)](q1))))),
                 minus_set[C](f((q1a, x)), f((q2a, x)))),
                            image[C, (sum[(A, A), A],
                                       (B,
 (C, sum[(A, A),
          A])))](((y: C) =>
                   (Inl[(A, A), A]((q1a, q2a)), (x, (y, Inr[A, (A, A)](q2))))),
                  minus_set[C](f((q2a, x)), f((q1a, x))))))
                                })(b)
                             }),
                            producte[(A, A), B](stateSet, inputSet)))

def shifted_transitions[A : ceq : ccompare : set_impl,
                         B : ceq : ccompare : set_impl,
                         C : ceq : ccompare : set_impl,
                         D : ceq : ccompare : set_impl](ts:
                  set[((A, A), (B, (C, (A, A))))]):
      set[(sum[(A, A), D], (B, (C, sum[(A, A), D])))]
  =
  image[((A, A), (B, (C, (A, A)))),
         (sum[(A, A), D],
           (B, (C, sum[(A, A),
                        D])))](((t: ((A, A), (B, (C, (A, A))))) =>
                                 (Inl[(A, A),
                                       D](fst[(A, A), (B, (C, (A, A)))](t)),
                                   (fst[B,
 (C, (A, A))](snd[(A, A), (B, (C, (A, A)))](t)),
                                     (fst[C,
   (A, A)](snd[B, (C, (A, A))](snd[(A, A), (B, (C, (A, A)))](t))),
                                       Inl[(A, A),
    D](snd[C, (A, A)](snd[B, (C, (A, A))](snd[(A, A),
       (B, (C, (A, A)))](t)))))))),
                                ts)

def canonical_separator_impl[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                              B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                                    fsm_with_precomputations_impl[A, B, C],
                                   p: fsm_with_precomputations_impl[(A, A), B,
                             C],
                                   q1: A, q2: A):
      fsm_with_precomputations_impl[sum[(A, A), A], B, C]
  =
  (if (equal_proda[A, A](initial_wpi[(A, A), B, C](p), (q1, q2)))
    {
      val f =
        set_as_mapping_image[A, (B, (C, A)), (A, B),
                              C](transitions_wpi[A, B, C](m),
                                  ((a: (A, (B, (C, A)))) =>
                                    {
                                      val (q, (x, (y, _))) =
a: ((A, (B, (C, A))));
                                      ((q, x), y)
                                    })):
          (mapping[(A, B), set[C]])
      val fa =
        ((qx: (A, B)) => ((lookupa[(A, B), set[C]](f)).apply(qx) match {
                            case None => bot_set[C]
                            case Some(yqs) => yqs
                          })):
          (((A, B)) => set[C])
      val shifted_transitionsa =
        shifted_transitions[A, B, C, A](transitions_wpi[(A, A), B, C](p)):
          (set[(sum[(A, A), A], (B, (C, sum[(A, A), A])))])
      val distinguishing_transitions_lr =
        distinguishing_transitions[A, B,
                                    C](fa, q1, q2, states_wpi[(A, A), B, C](p),
inputs_wpi[(A, A), B, C](p)):
          (set[(sum[(A, A), A], (B, (C, sum[(A, A), A])))])
      val ts =
        sup_seta[(sum[(A, A), A],
                   (B, (C, sum[(A, A),
                                A])))](shifted_transitionsa,
distinguishing_transitions_lr):
          (set[(sum[(A, A), A], (B, (C, sum[(A, A), A])))])
      val h =
        set_as_mapping_image[sum[(A, A), A], (B, (C, sum[(A, A), A])),
                              (sum[(A, A), A], B),
                              (C, sum[(A, A),
                                       A])](ts,
     ((a: (sum[(A, A), A], (B, (C, sum[(A, A), A])))) =>
       {
         val (q, (x, (y, qa))) =
           a: ((sum[(A, A), A], (B, (C, sum[(A, A), A]))));
         ((q, x), (y, qa))
       })):
          (mapping[(sum[(A, A), A], B), set[(C, sum[(A, A), A])]]);
      FSMWPI[sum[(A, A), A], B,
              C](Inl[(A, A), A]((q1, q2)),
                  sup_seta[sum[(A, A),
                                A]](image[(A, A),
   sum[(A, A),
        A]](((a: (A, A)) => Inl[(A, A), A](a)), states_wpi[(A, A), B, C](p)),
                                     insert[sum[(A, A),
         A]](Inr[A, (A, A)](q1),
              insert[sum[(A, A),
                          A]](Inr[A, (A, A)](q2), bot_set[sum[(A, A), A]]))),
                  sup_seta[B](inputs_wpi[A, B, C](m),
                               inputs_wpi[(A, A), B, C](p)),
                  sup_seta[C](outputs_wpi[A, B, C](m),
                               outputs_wpi[(A, A), B, C](p)),
                  ts, h, h_obs_impl_from_h[sum[(A, A), A], B, C](h))
    }
    else FSMWPI[sum[(A, A), A], B,
                 C](Inl[(A, A), A]((q1, q2)),
                     insert[sum[(A, A),
                                 A]](Inl[(A, A), A]((q1, q2)),
                                      bot_set[sum[(A, A), A]]),
                     bot_set[B], bot_set[C],
                     bot_set[(sum[(A, A), A], (B, (C, sum[(A, A), A])))],
                     emptya[(sum[(A, A), A], B), set[(C, sum[(A, A), A])]],
                     emptya[(sum[(A, A), A], B), mapping[C, sum[(A, A), A]]]))

def canonical_separatorc[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](xc:
                                fsm_with_precomputations[A, B, C],
                               xe: fsm_with_precomputations[(A, A), B, C],
                               xf: A, xg: A):
      fsm_with_precomputations[sum[(A, A), A], B, C]
  =
  Fsm_with_precomputationsa[sum[(A, A), A], B,
                             C](canonical_separator_impl[A, B,
                  C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
B, C](xc),
                      fsm_with_precomputations_impl_of_fsm_with_precomputations[(A,
  A),
 B, C](xe),
                      xf, xg))

def canonical_separatora[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](x0:
                                fsm_impl[A, B, C],
                               x1: fsm_impl[(A, A), B, C], q1: A, q2: A):
      fsm_impl[sum[(A, A), A], B, C]
  =
  (x0, x1, q1, q2) match {
  case (FSMWP(m), FSMWP(p), q1, q2) =>
    FSMWP[sum[(A, A), A], B, C](canonical_separatorc[A, B, C](m, p, q1, q2))
}

def canonical_separator[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                         B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                         C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](xc:
                               fsm[A, B, C],
                              xe: fsm[(A, A), B, C], xf: A, xg: A):
      fsm[sum[(A, A), A], B, C]
  =
  Abs_fsm[sum[(A, A), A], B,
           C](canonical_separatora[A, B,
                                    C](fsm_impl_of_fsm[A, B, C](xc),
fsm_impl_of_fsm[(A, A), B, C](xe), xf, xg))

def completely_specified[A : ceq : ccompare : equal, B : ceq : ccompare : equal,
                          C : ceq : ccompare](m: fsm[A, B, C]):
      Boolean
  =
  Ball[A](states[A, B, C](m),
           ((q: A) =>
             Ball[B](inputs[A, B, C](m),
                      ((x: B) =>
                        Bex[(A, (B, (C, A)))](transitions[A, B, C](m),
       ((t: (A, (B, (C, A)))) =>
         eq[A](fst[A, (B, (C, A))](t), q) &&
           eq[B](fst[B, (C, A)](snd[A, (B, (C, A))](t)), x)))))))

def index_states_integer[A : ceq : ccompare : equal : linorder,
                          B : ceq : ccompare : equal : mapping_impl : set_impl,
                          C : ceq : ccompare : equal : mapping_impl : set_impl](m:
  fsm[A, B, C]):
      fsm[BigInt, B, C]
  =
  rename_states[A, B, C,
                 BigInt](m, comp[nat, BigInt,
                                  A](((a: nat) => integer_of_nat(a)),
                                      assign_indices[A](states[A, B, C](m))))

def sorted_list_of_fset[A : ceq : ccompare : equal : linorder](xa: fset[A]):
      List[A]
  =
  sorted_list_of_set[A](fset[A](xa))

def one_int: int = int_of_integer(BigInt(1))

def update_assoc_list_with_default[A : equal,
                                    B](k: A, f: B => B, d: B, x3: List[(A, B)]):
      List[(A, B)]
  =
  (k, f, d, x3) match {
  case (k, f, d, Nil) => List((k, f(d)))
  case (k, f, d, (x, y) :: xys) =>
    (if (eq[A](k, x)) (x, f(y)) :: xys
      else (x, y) :: update_assoc_list_with_default[A, B](k, f, d, xys))
}

def insertd[A : equal](x0: List[A], t: mp_trie[A]): mp_trie[A] = (x0, t) match {
  case (Nil, t) => t
  case (x :: xs, Mp_triea(ts)) =>
    Mp_triea[A](update_assoc_list_with_default[A,
        mp_trie[A]](x, ((a: mp_trie[A]) => insertd[A](xs, a)), emptye[A], ts))
}

def integer_of_int(x0: int): BigInt = x0 match {
  case int_of_integer(k) => k
}

def less_int(k: int, l: int): Boolean = integer_of_int(k) < integer_of_int(l)

def combine_after[A : ccompare : equal : mapping_impl](x0: prefix_tree[A],
                xs: List[A], t: prefix_tree[A]):
      prefix_tree[A]
  =
  (x0, xs, t) match {
  case (MPT(m), xs, t) =>
    (xs match {
       case Nil => combinea[A](MPT[A](m), t)
       case x :: xsa =>
         MPT[A](updateb[A, prefix_tree[A]](x,
    combine_after[A](((lookupa[A, prefix_tree[A]](m)).apply(x) match {
                        case None => emptyc[A]
                        case Some(ta) => ta
                      }),
                      xsa, t),
    m))
     })
}

def is_maximal_in[A : ccompare : equal : mapping_impl](t: prefix_tree[A],
                alpha: List[A]):
      Boolean
  =
  isin[A](t, alpha) && is_leaf[A](aftera[A](t, alpha))

def int_of_nat(n: nat): int = int_of_integer(integer_of_nat(n))

def initial_singleton_impl[A : ceq : ccompare : mapping_impl : set_impl,
                            B : ceq : ccompare : mapping_impl : set_impl,
                            C : ceq : ccompare : set_impl](m:
                     fsm_with_precomputations_impl[A, B, C]):
      fsm_with_precomputations_impl[A, B, C]
  =
  FSMWPI[A, B,
          C](initial_wpi[A, B, C](m),
              insert[A](initial_wpi[A, B, C](m), bot_set[A]),
              inputs_wpi[A, B, C](m), outputs_wpi[A, B, C](m),
              bot_set[(A, (B, (C, A)))], emptya[(A, B), set[(C, A)]],
              emptya[(A, B), mapping[C, A]])

def initial_singletona[A : ceq : ccompare : mapping_impl : set_impl,
                        B : ceq : ccompare : mapping_impl : set_impl,
                        C : ceq : ccompare : set_impl](xa:
                 fsm_with_precomputations[A, B, C]):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](initial_singleton_impl[A, B,
                C](fsm_with_precomputations_impl_of_fsm_with_precomputations[A,
                                      B, C](xa)))

def initial_singleton[A : ceq : ccompare : mapping_impl : set_impl,
                       B : ceq : ccompare : mapping_impl : set_impl,
                       C : ceq : ccompare : set_impl](x0: fsm_impl[A, B, C]):
      fsm_impl[A, B, C]
  =
  x0 match {
  case FSMWP(m) => FSMWP[A, B, C](initial_singletona[A, B, C](m))
}

def zero_int: int = int_of_integer(BigInt(0))

def maximum_prefix[A : ccompare : equal](x0: prefix_tree[A], xs: List[A]):
      List[A]
  =
  (x0, xs) match {
  case (MPT(m), xs) =>
    (xs match {
       case Nil => Nil
       case x :: xsa => ((lookupa[A, prefix_tree[A]](m)).apply(x) match {
                           case None => Nil
                           case Some(t) => x :: maximum_prefix[A](t, xsa)
                         })
     })
}

def bot_fset[A : ceq : ccompare : set_impl]: fset[A] = Abs_fset[A](bot_set[A])

def sup_fset[A : ceq : ccompare](xb: fset[A], xc: fset[A]): fset[A] =
  Abs_fset[A](sup_seta[A](fset[A](xb), fset[A](xc)))

def from_lista[A : equal](seqs: List[List[A]]): mp_trie[A] =
  (foldr[List[A],
          mp_trie[A]](((a: List[A]) => (b: mp_trie[A]) => insertd[A](a, b)),
                       seqs)).apply(emptye[A])

def ofsm_table_fix[A : cenum : ceq : ccompare : equal : set_impl,
                    B : ceq : ccompare : equal,
                    C : ceq : ccompare : equal](m: fsm[A, B, C], f: A => set[A],
         k: nat):
      A => set[A]
  =
  {
    val cur_table =
      ((a: A) =>
        ofsm_table[A, B,
                    C](m, ((q: A) => inf_seta[A](f(q), states[A, B, C](m))), k,
                        a)):
        (A => set[A])
    val next_table =
      ((a: A) =>
        ofsm_table[A, B,
                    C](m, ((q: A) => inf_seta[A](f(q), states[A, B, C](m))),
                        Suc(k), a)):
        (A => set[A]);
    (if (Ball[A](states[A, B, C](m),
                  ((q: A) => set_eq[A](cur_table(q), next_table(q)))))
      cur_table else ofsm_table_fix[A, B, C](m, f, Suc(k)))
  }

def language_up_to_length_with_extensions[A, B,
   C](q: A, hM: A => B => List[(C, A)], iM: List[B], ex: List[List[(B, C)]],
       k: nat):
      List[List[(B, C)]]
  =
  (if (equal_nata(k, zero_nat)) ex
    else ex ++ maps[B, List[(B, C)]](((x: B) =>
                                       maps[(C, A),
     List[(B, C)]](((a: (C, A)) =>
                     {
                       val (y, qa) = a: ((C, A));
                       mapa[List[(B, C)],
                             List[(B, C)]](((aa: List[(B, C)]) => (x, y) :: aa),
    language_up_to_length_with_extensions[A, B,
   C](qa, hM, iM, ex, minus_nat(k, one_nata)))
                     }),
                    (hM(q))(x))),
                                      iM))

def h_extensions[A : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                  B : ceq : ccompare : equal : mapping_impl : linorder,
                  C : ceq : ccompare : equal : linorder : set_impl](m:
                              fsm[A, B, C],
                             q: A, k: nat):
      List[List[(B, C)]]
  =
  {
    val iM = inputs_as_list[A, B, C](m): (List[B])
    val ex =
      mapa[(B, C),
            List[(B, C)]](((xy: (B, C)) => List(xy)),
                           product[B, C](iM, outputs_as_list[A, B, C](m))):
        (List[List[(B, C)]])
    val hM =
      ((qa: A) => (x: B) => sorted_list_of_set[(C, A)](h[A, B, C](m, (qa, x)))):
        (A => B => List[(C, A)]);
    language_up_to_length_with_extensions[A, B, C](q, hM, iM, ex, k)
  }

def spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                   B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                   C : finite_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl,
                   D](ma: fsm[A, B, C],
                       get_state_cover: (fsm[A, B, C]) => A => List[(B, C)],
                       separate_state_cover:
                         (fsm[A, B, C]) =>
                           (A => List[(B, C)]) =>
                             ((fsm[A, B, C]) => (prefix_tree[(B, C)]) => D) =>
                               (D => (List[(B, C)]) => D) =>
                                 (D => (List[(B, C)]) => List[List[(B, C)]]) =>
                                   (prefix_tree[(B, C)], D),
                       sort_unverified_transitions:
                         (fsm[A, B, C]) =>
                           (A => List[(B, C)]) =>
                             (List[(A, (B, (C, A)))]) => List[(A, (B, (C, A)))],
                       establish_convergence:
                         (fsm[A, B, C]) =>
                           (A => List[(B, C)]) =>
                             (prefix_tree[(B, C)]) =>
                               D => (D => (List[(B, C)]) => D) =>
                                      (D =>
(List[(B, C)]) => List[List[(B, C)]]) =>
nat => ((A, (B, (C, A)))) => (prefix_tree[(B, C)], D),
                       append_io_pair:
                         (fsm[A, B, C]) =>
                           (A => List[(B, C)]) =>
                             (prefix_tree[(B, C)]) =>
                               D => (D => (List[(B, C)]) => D) =>
                                      (D =>
(List[(B, C)]) => List[List[(B, C)]]) =>
A => B => C => (prefix_tree[(B, C)], D),
                       cg_initial: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => D,
                       cg_insert: D => (List[(B, C)]) => D,
                       cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                       cg_merge: D => (List[(B, C)]) => (List[(B, C)]) => D,
                       m: nat):
      prefix_tree[(B, C)]
  =
  {
    val rstates_set = reachable_states[A, B, C](ma): (set[A])
    val rstates = reachable_states_as_list[A, B, C](ma): (List[A])
    val rstates_io =
      product[A, (B, C)](rstates,
                          product[B, C](inputs_as_list[A, B, C](ma),
 outputs_as_list[A, B, C](ma))):
        (List[(A, (B, C))])
    val undefined_io_pairs =
      filtera[(A, (B, C))](((a: (A, (B, C))) =>
                             {
                               val (q, (x, y)) = a: ((A, (B, C)));
                               is_none[A]((h_obs[A, B,
          C](ma)).apply(q).apply(x).apply(y))
                             }),
                            rstates_io):
        (List[(A, (B, C))])
    val v = get_state_cover(ma): (A => List[(B, C)]);
    card[A](reachable_states[A, B, C](ma))
    val tG1 =
      ((((separate_state_cover(ma))(v))(cg_initial))(cg_insert))(cg_lookup):
        ((prefix_tree[(B, C)], D))
    val sc_covered_transitions =
      Sup_setb[(A, (B, (C, A)))](image[A,
set[(A, (B, (C, A)))]](((q: A) => covered_transitions[A, B, C](ma, v, v(q))),
                        rstates_set)):
        (set[(A, (B, (C, A)))])
    val unverified_transitions =
      ((sort_unverified_transitions(ma))(v))(filtera[(A,
               (B, (C, A)))](((t: (A, (B, (C, A)))) =>
                               member[A](fst[A, (B, (C, A))](t), rstates_set) &&
                                 ! (member[(A,
     (B, (C, A)))](t, sc_covered_transitions))),
                              transitions_as_list[A, B, C](ma))):
        (List[(A, (B, (C, A)))])
    val verify_transition =
      ((a: (prefix_tree[(B, C)], D)) =>
        {
          val (t, g) = a: ((prefix_tree[(B, C)], D));
          ((ta: (A, (B, (C, A)))) =>
            {
              val tGxy =
                ((((((((append_io_pair(ma))(v))(t))(g))(cg_insert))(cg_lookup))(fst[A,
     (B, (C, A))](ta)))(fst[B, (C, A)](snd[A,
    (B, (C, A))](ta))))(fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](ta)))):
                  ((prefix_tree[(B, C)], D))
              val (tb, ga) =
                (((((((establish_convergence(ma))(v))(fst[prefix_tree[(B, C)],
                   D](tGxy)))(snd[prefix_tree[(B, C)],
                                   D](tGxy)))(cg_insert))(cg_lookup))(m))(ta):
                  ((prefix_tree[(B, C)], D))
              val aa =
                ((cg_merge(ga))(v(fst[A, (B, (C, A))](ta)) ++
                                  List((fst[B, (C, A)](snd[A, (B, (C, A))](ta)),
 fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](ta)))))))(v(snd[C,
                       A](snd[B, (C, A)](snd[A, (B, (C, A))](ta))))):
                  D;
              (tb, aa)
            })
        }):
        (((prefix_tree[(B, C)], D)) =>
          ((A, (B, (C, A)))) => (prefix_tree[(B, C)], D))
    val tG2 =
      foldl[(prefix_tree[(B, C)], D),
             (A, (B, (C, A)))](verify_transition, tG1, unverified_transitions):
        ((prefix_tree[(B, C)], D))
    val verify_undefined_io_pair =
      ((t: prefix_tree[(B, C)]) => (a: (A, (B, C))) =>
        {
          val (q, (x, y)) = a: ((A, (B, C)));
          fst[prefix_tree[(B, C)],
               D](((((((((append_io_pair(ma))(v))(t))(snd[prefix_tree[(B, C)],
                   D](tG2)))(cg_insert))(cg_lookup))(q))(x))(y))
        }):
        ((prefix_tree[(B, C)]) => ((A, (B, C))) => prefix_tree[(B, C)]);
    foldl[prefix_tree[(B, C)],
           (A, (B, C))](verify_undefined_io_pair,
                         fst[prefix_tree[(B, C)], D](tG2), undefined_io_pairs)
  }

def remove_proper_prefixes[A : ceq : ccompare : equal](x0: set[List[A]]):
      set[List[A]]
  =
  x0 match {
  case Rbt_set(t) =>
    (ccompare_lista[A] match {
       case None =>
         { sys.error("remove_proper_prefixes RBT_set: ccompare = None");
           (((_: Unit) =>
              remove_proper_prefixes[A](Rbt_set[List[A]](t)))).apply(())
           }
       case Some(_) =>
         (if (is_emptya[List[A], Unit](t))
           set_empty[List[A]](of_phantom[List[A], set_impla](set_impl_lista[A]))
           else set[List[A]](paths[A](from_lista[A](keysb[List[A]](t)))))
     })
}

def equal_int(k: int, l: int): Boolean = integer_of_int(k) == integer_of_int(l)

def minus_fset[A : ceq : ccompare](xb: fset[A], xc: fset[A]): fset[A] =
  Abs_fset[A](minus_set[A](fset[A](xb), fset[A](xc)))

def make_observable_transitions[A : finite_univ : cenum : ceq : ccompare : equal : set_impl : infinite_univ,
                                 B : finite_univ : cenum : ceq : cproper_interval : equal : set_impl,
                                 C : finite_univ : cenum : ceq : cproper_interval : equal : set_impl](base_trans:
                        fset[(A, (B, (C, A)))],
                       nexts: fset[fset[A]], dones: fset[fset[A]],
                       ts: fset[(fset[A], (B, (C, fset[A])))]):
      fset[(fset[A], (B, (C, fset[A])))]
  =
  {
    val qtrans =
      ffunion[(fset[A],
                (B, (C, fset[A])))](fimage[fset[A],
    fset[(fset[A],
           (B, (C, fset[A])))]](((q: fset[A]) =>
                                  {
                                    val qts =
                                      ffilter[(A,
        (B, (C, A)))](((t: (A, (B, (C, A)))) =>
                        member[A](fst[A, (B, (C, A))](t), fset[A](q))),
                       base_trans):
(fset[(A, (B, (C, A)))])
                                    val a =
                                      fimage[(A, (B, (C, A))),
      (B, C)](((t: (A, (B, (C, A)))) =>
                (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                  fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
               qts):
(fset[(B, C)]);
                                    fimage[(B, C),
    (fset[A],
      (B, (C, fset[A])))](((aa: (B, C)) =>
                            {
                              val (x, y) = aa: ((B, C));
                              (q, (x, (y,
fimage[(A, (B, (C, A))),
        A](((ab: (A, (B, (C, A)))) =>
             snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](ab)))),
            ffilter[(A, (B, (C, A)))](((t: (A, (B, (C, A)))) =>
equal_proda[B, C]((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                    fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))),
                   (x, y))),
                                       qts)))))
                            }),
                           a)
                                  }),
                                 nexts)):
        (fset[(fset[A], (B, (C, fset[A])))])
    val donesa = sup_fset[fset[A]](dones, nexts): (fset[fset[A]])
    val tsa =
      sup_fset[(fset[A], (B, (C, fset[A])))](ts, qtrans):
        (fset[(fset[A], (B, (C, fset[A])))])
    val nextsa =
      minus_fset[fset[A]](fimage[(fset[A], (B, (C, fset[A]))),
                                  fset[A]](((a: (fset[A], (B, (C, fset[A])))) =>
     snd[C, fset[A]](snd[B, (C, fset[A])](snd[fset[A], (B, (C, fset[A]))](a)))),
    qtrans),
                           donesa):
        (fset[fset[A]]);
    (if (eq[fset[fset[A]]](nextsa, bot_fset[fset[A]])) tsa
      else make_observable_transitions[A, B,
C](base_trans, nextsa, donesa, tsa))
  }

def create_unconnected_fsm_from_fsets_impl[A : ceq : ccompare : mapping_impl : set_impl,
    B : ceq : ccompare : mapping_impl : set_impl,
    C : ceq : ccompare : set_impl](q: A, ns: fset[A], ins: fset[B],
                                    outs: fset[C]):
      fsm_with_precomputations_impl[A, B, C]
  =
  FSMWPI[A, B,
          C](q, insert[A](q, fset[A](ns)), fset[B](ins), fset[C](outs),
              bot_set[(A, (B, (C, A)))], emptya[(A, B), set[(C, A)]],
              emptya[(A, B), mapping[C, A]])

def create_unconnected_fsm_from_fsetsb[A : ceq : ccompare : mapping_impl : set_impl,
B : ceq : ccompare : mapping_impl : set_impl,
C : ceq : ccompare : set_impl](q: A, ns: fset[A], ins: fset[B], outs: fset[C]):
      fsm_with_precomputations[A, B, C]
  =
  Fsm_with_precomputationsa[A, B,
                             C](create_unconnected_fsm_from_fsets_impl[A, B,
                                C](q, ns, ins, outs))

def create_unconnected_fsm_from_fsetsa[A : ceq : ccompare : mapping_impl : set_impl,
B : ceq : ccompare : mapping_impl : set_impl,
C : ceq : ccompare : set_impl](q: A, ns: fset[A], ins: fset[B], outs: fset[C]):
      fsm_impl[A, B, C]
  =
  FSMWP[A, B, C](create_unconnected_fsm_from_fsetsb[A, B, C](q, ns, ins, outs))

def create_unconnected_fsm_from_fsets[A : ceq : ccompare : mapping_impl : set_impl,
                                       B : ceq : ccompare : mapping_impl : set_impl,
                                       C : ceq : ccompare : set_impl](xc: A,
                               xd: fset[A], xe: fset[B], xf: fset[C]):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](create_unconnected_fsm_from_fsetsa[A, B, C](xc, xd, xe, xf))

def make_observable[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl : infinite_univ,
                     B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                     C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m:
                                      fsm[A, B, C]):
      fsm[fset[A], B, C]
  =
  {
    val initial_trans =
      {
        val qts =
          ffilter[(A, (B, (C, A)))](((t: (A, (B, (C, A)))) =>
                                      eq[A](fst[A, (B, (C, A))](t),
     initial[A, B, C](m))),
                                     ftransitions[A, B, C](m)):
            (fset[(A, (B, (C, A)))])
        val a =
          fimage[(A, (B, (C, A))),
                  (B, C)](((t: (A, (B, (C, A)))) =>
                            (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                              fst[C, A](snd[B,
     (C, A)](snd[A, (B, (C, A))](t))))),
                           qts):
            (fset[(B, C)]);
        fimage[(B, C),
                (fset[A],
                  (B, (C, fset[A])))](((aa: (B, C)) =>
{
  val (x, y) = aa: ((B, C));
  (finsert[A](initial[A, B, C](m), bot_fset[A]),
    (x, (y, fimage[(A, (B, (C, A))),
                    A](((ab: (A, (B, (C, A)))) =>
                         snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](ab)))),
                        ffilter[(A, (B, (C,
  A)))](((t: (A, (B, (C, A)))) =>
          equal_proda[B, C]((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                              fst[C, A](snd[B,
     (C, A)](snd[A, (B, (C, A))](t)))),
                             (x, y))),
         qts)))))
}),
                                       a)
      }:
        (fset[(fset[A], (B, (C, fset[A])))])
    val nexts =
      minus_fset[fset[A]](fimage[(fset[A], (B, (C, fset[A]))),
                                  fset[A]](((a: (fset[A], (B, (C, fset[A])))) =>
     snd[C, fset[A]](snd[B, (C, fset[A])](snd[fset[A], (B, (C, fset[A]))](a)))),
    initial_trans),
                           finsert[fset[A]](finsert[A](initial[A, B, C](m),
                bot_fset[A]),
     bot_fset[fset[A]])):
        (fset[fset[A]])
    val ptransitions =
      make_observable_transitions[A, B,
                                   C](ftransitions[A, B, C](m), nexts,
                                       finsert[fset[A]](finsert[A](initial[A, B,
                                    C](m),
                            bot_fset[A]),
                 bot_fset[fset[A]]),
                                       initial_trans):
        (fset[(fset[A], (B, (C, fset[A])))])
    val pstates =
      finsert[fset[A]](finsert[A](initial[A, B, C](m), bot_fset[A]),
                        fimage[(fset[A], (B, (C, fset[A]))),
                                fset[A]](((a: (fset[A], (B, (C, fset[A])))) =>
   snd[C, fset[A]](snd[B, (C, fset[A])](snd[fset[A], (B, (C, fset[A]))](a)))),
  ptransitions)):
        (fset[fset[A]])
    val ma =
      create_unconnected_fsm_from_fsets[fset[A], B,
 C](finsert[A](initial[A, B, C](m), bot_fset[A]), pstates, finputs[A, B, C](m),
     foutputs[A, B, C](m)):
        (fsm[fset[A], B, C]);
    add_transitions[fset[A], B,
                     C](ma, fset[(fset[A], (B, (C, fset[A])))](ptransitions))
  }

def pair_framework[A : linorder, B : ccompare : equal : mapping_impl : linorder,
                    C : ccompare : equal : mapping_impl : linorder](ma:
                              fsm[A, B, C],
                             m: nat,
                             get_initial_test_suite:
                               (fsm[A, B, C]) => nat => prefix_tree[(B, C)],
                             get_pairs:
                               (fsm[A, B, C]) =>
                                 nat =>
                                   List[((List[(B, C)], A), (List[(B, C)], A))],
                             get_separating_traces:
                               (fsm[A, B, C]) =>
                                 (((List[(B, C)], A), (List[(B, C)], A))) =>
                                   (prefix_tree[(B, C)]) =>
                                     prefix_tree[(B, C)]):
      prefix_tree[(B, C)]
  =
  {
    val ts = (get_initial_test_suite(ma))(m): (prefix_tree[(B, C)])
    val d = (get_pairs(ma))(m): (List[((List[(B, C)], A), (List[(B, C)], A))])
    val dist_extension =
      ((t: prefix_tree[(B, C)]) =>
        (a: ((List[(B, C)], A), (List[(B, C)], A))) =>
        {
          val (aa, b) = a: (((List[(B, C)], A), (List[(B, C)], A)));
          ({
             val (alpha, q) = aa: ((List[(B, C)], A));
             ((ab: (List[(B, C)], A)) =>
               {
                 val (beta, qa) = ab: ((List[(B, C)], A))
                 val tDist =
                   ((get_separating_traces(ma))(((alpha, q), (beta, qa))))(t):
                     (prefix_tree[(B, C)]);
                 combine_after[(B, C)](combine_after[(B, C)](t, alpha, tDist),
beta, tDist)
               })
           })(b)
        }):
        ((prefix_tree[(B, C)]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) => prefix_tree[(B, C)]);
    foldl[prefix_tree[(B, C)],
           ((List[(B, C)], A), (List[(B, C)], A))](dist_extension, ts, d)
  }

def fset_states_to_list_states[A : cenum : ceq : ccompare : equal : linorder,
                                B : ceq : ccompare : equal : mapping_impl : set_impl,
                                C : ceq : ccompare : equal : mapping_impl : set_impl](m:
        fsm[fset[A], B, C]):
      fsm[List[A], B, C]
  =
  rename_states[fset[A], B, C,
                 List[A]](m, ((a: fset[A]) => sorted_list_of_fset[A](a)))

def set_states_to_list_states[A : finite_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl,
                               B : ceq : ccompare : equal : mapping_impl : set_impl,
                               C : ceq : ccompare : equal : mapping_impl : set_impl](m:
       fsm[set[A], B, C]):
      fsm[List[A], B, C]
  =
  rename_states[set[A], B, C,
                 List[A]](m, ((a: set[A]) => sorted_list_of_set[A](a)))

def initial_ofsm_table[A : ceq : ccompare : equal : mapping_impl : linorder, B,
                        C](m: fsm[A, B, C]):
      mapping[A, set[A]]
  =
  tabulate[A, set[A]](states_as_list[A, B, C](m),
                       ((_: A) => states[A, B, C](m)))

def next_ofsm_table[A : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                     B : ceq : ccompare : equal,
                     C : ceq : ccompare : equal](m: fsm[A, B, C],
          prev_table: mapping[A, set[A]]):
      mapping[A, set[A]]
  =
  tabulate[A, set[A]](states_as_list[A, B, C](m),
                       ((q: A) =>
                         filter[A](((qa: A) =>
                                     Ball[B](inputs[A, B, C](m),
      ((x: B) =>
        Ball[C](outputs[A, B, C](m),
                 ((y: C) =>
                   ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y) match {
                      case None =>
                        is_none[A]((h_obs[A, B,
   C](m)).apply(qa).apply(x).apply(y))
                      case Some(qT) =>
                        ((h_obs[A, B, C](m)).apply(qa).apply(x).apply(y) match {
                           case None => false
                           case Some(qTa) =>
                             set_eq[A](lookup_default[set[A],
               A](bot_set[A], prev_table, qT),
lookup_default[set[A], A](bot_set[A], prev_table, qTa))
                         })
                    })))))),
                                    lookup_default[set[A],
            A](bot_set[A], prev_table, q))))

def compute_ofsm_table_list[A : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                             B : ceq : ccompare : equal,
                             C : ceq : ccompare : equal](m: fsm[A, B, C],
                  k: nat):
      List[mapping[A, set[A]]]
  =
  rev[mapping[A, set[A]]]((foldr[nat, List[mapping[A,
            set[A]]]](((_: nat) => (prev: List[mapping[A, set[A]]]) =>
                        next_ofsm_table[A, B,
 C](m, hd[mapping[A, set[A]]](prev)) ::
                          prev),
                       upt(zero_nat,
                            k))).apply(List(initial_ofsm_table[A, B, C](m))))

def compute_ofsm_tables[A : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                         B : ceq : ccompare : equal,
                         C : ceq : ccompare : equal](m: fsm[A, B, C], k: nat):
      mapping[nat, mapping[A, set[A]]]
  =
  bulkload[mapping[A, set[A]]](compute_ofsm_table_list[A, B, C](m, k))

def minimise_refined[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                      B : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                      C : ceq : ccompare : equal : mapping_impl : linorder : set_impl](m:
         fsm[A, B, C]):
      fsm[set[A], B, C]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](m, minus_nat(size[A, B, C](m), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val eq_class =
      ((a: A) =>
        lookup_default[set[A],
                        A](bot_set[A],
                            the[mapping[A,
 set[A]]]((lookupa[nat, mapping[A, set[A]]](tables)).apply(minus_nat(size[A, B,
                                   C](m),
                              one_nata))),
                            a)):
        (A => set[A])
    val ts =
      image[(A, (B, (C, A))),
             (set[A],
               (B, (C, set[A])))](((t: (A, (B, (C, A)))) =>
                                    (eq_class(fst[A, (B, (C, A))](t)),
                                      (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
(fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
  eq_class(snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))))))),
                                   transitions[A, B, C](m)):
        (set[(set[A], (B, (C, set[A])))])
    val q0 = eq_class(initial[A, B, C](m)): (set[A])
    val eq_states =
      fimage[A, set[A]](eq_class, fstates[A, B, C](m)): (fset[set[A]])
    val ma =
      create_unconnected_fsm_from_fsets[set[A], B,
 C](q0, eq_states, finputs[A, B, C](m), foutputs[A, B, C](m)):
        (fsm[set[A], B, C]);
    add_transitions[set[A], B, C](ma, ts)
  }

def restrict_to_reachable_states[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                                  B : ceq : ccompare : equal : linorder,
                                  C : ceq : ccompare : equal : mapping_impl : linorder](m:
          fsm[A, B, C]):
      fsm[A, B, C]
  =
  {
    val path_assignments =
      reaching_paths_up_to_depth[A, B,
                                  C](m, insert[A](initial[A, B, C](m),
           bot_set[A]),
                                      bot_set[A],
                                      fun_upd[A,
       Option[List[(A, (B, (C, A)))]]](((_: A) => None), initial[A, B, C](m),
Some[List[(A, (B, (C, A)))]](Nil)),
                                      minus_nat(size[A, B, C](m), one_nata)):
        (A => Option[List[(A, (B, (C, A)))]]);
    filter_states[A, B,
                   C](m, ((q: A) =>
                           ! (is_none[List[(A,
     (B, (C, A)))]](path_assignments(q)))))
  }

def to_prime[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl : infinite_univ,
              B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m:
                               fsm[A, B, C]):
      fsm[BigInt, B, C]
  =
  restrict_to_reachable_states[BigInt, B,
                                C](index_states_integer[List[nat], B,
                 C](set_states_to_list_states[nat, B,
       C](minimise_refined[nat, B,
                            C](index_states[List[A], B,
     C](fset_states_to_list_states[A, B,
                                    C](make_observable[A, B,
                C](restrict_to_reachable_states[A, B, C](m))))))))

def maximal_prefix_in_language[A : ccompare : equal, B : ccompare : equal,
                                C : ccompare : equal](m: fsm[A, B, C], q: A,
               x2: List[(B, C)]):
      List[(B, C)]
  =
  (m, q, x2) match {
  case (m, q, Nil) => Nil
  case (m, q, (x, y) :: io) =>
    ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y) match {
       case None => Nil
       case Some(qa) => (x, y) :: maximal_prefix_in_language[A, B, C](m, qa, io)
     })
}

def uminus_int(k: int): int = int_of_integer((- (integer_of_int(k))))

def initial_preamble[A : ceq : ccompare : mapping_impl : set_impl,
                      B : ceq : ccompare : mapping_impl : set_impl,
                      C : ceq : ccompare : set_impl](xa: fsm[A, B, C]):
      fsm[A, B, C]
  =
  Abs_fsm[A, B, C](initial_singleton[A, B, C](fsm_impl_of_fsm[A, B, C](xa)))

def maximum_fst_prefixes[A : ccompare : equal,
                          B : ccompare : equal](x0: prefix_tree[(A, B)],
         xs: List[A], ys: List[B]):
      List[List[(A, B)]]
  =
  (x0, xs, ys) match {
  case (MPT(m), xs, ys) =>
    (xs match {
       case Nil => (if (is_leaf[(A, B)](MPT[(A, B)](m))) List(Nil) else Nil)
       case x :: xsa =>
         (if (is_leaf[(A, B)](MPT[(A, B)](m))) List(Nil)
           else concat[List[(A, B)]](map_filter[B,
         List[List[(A, B)]]](((xa: B) =>
                               (if (! (is_none[prefix_tree[(A,
                     B)]]((lookupa[(A, B),
                                    prefix_tree[(A, B)]](m)).apply((x, xa)))))
                                 Some[List[List[(A,
          B)]]](mapa[List[(A, B)],
                      List[(A, B)]](((a: List[(A, B)]) => (x, xa) :: a),
                                     maximum_fst_prefixes[A,
                   B](the[prefix_tree[(A,
B)]]((lookupa[(A, B), prefix_tree[(A, B)]](m)).apply((x, xa))),
                       xsa, ys)))
                                 else None)),
                              ys)))
     })
}

def pairs_to_distinguish[A : equal : linorder, B : linorder,
                          C : linorder](m: fsm[A, B, C], v: A => List[(B, C)],
 x: A => List[(List[(A, (B, (C, A)))], A)], rstates: List[A]):
      List[((List[(B, C)], A), (List[(B, C)], A))]
  =
  {
    val pi =
      mapa[A, (List[(B, C)], A)](((q: A) => (v(q), q)), rstates):
        (List[(List[(B, C)], A)])
    val a =
      product[(List[(B, C)], A), (List[(B, C)], A)](pi, pi):
        (List[((List[(B, C)], A), (List[(B, C)], A))])
    val b =
      product[(List[(B, C)], A),
               (List[(B, C)],
                 A)](pi, maps[A, (List[(B, C)],
                                   A)](((q: A) =>
 mapa[(List[(A, (B, (C, A)))], A),
       (List[(B, C)],
         A)](((aa: (List[(A, (B, (C, A)))], A)) =>
               {
                 val (tau, ab) = aa: ((List[(A, (B, (C, A)))], A));
                 (v(q) ++
                    mapa[(A, (B, (C, A))),
                          (B, C)](((t: (A, (B, (C, A)))) =>
                                    (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                                      fst[C,
   A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
                                   tau),
                   ab)
               }),
              x(q))),
rstates)):
        (List[((List[(B, C)], A), (List[(B, C)], A))])
    val c =
      maps[A, ((List[(B, C)], A),
                (List[(B, C)],
                  A))](((q: A) =>
                         maps[(List[(A, (B, (C, A)))], A),
                               ((List[(B, C)], A),
                                 (List[(B, C)],
                                   A))](((aa: (List[(A, (B, (C, A)))], A)) =>
  {
    val (tau, qa) = aa: ((List[(A, (B, (C, A)))], A));
    mapa[List[(A, (B, (C, A)))],
          ((List[(B, C)], A),
            (List[(B, C)],
              A))](((taua: List[(A, (B, (C, A)))]) =>
                     ((v(q) ++
                         mapa[(A, (B, (C, A))),
                               (B, C)](((t: (A, (B, (C, A)))) =>
 (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
   fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
taua),
                        target[A, B, C](q, taua)),
                       (v(q) ++
                          mapa[(A, (B, (C, A))),
                                (B, C)](((t: (A, (B, (C, A)))) =>
  (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
    fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
 tau),
                         qa))),
                    prefixes[(A, (B, (C, A)))](tau))
  }),
 x(q))),
                        rstates):
        (List[((List[(B, C)], A), (List[(B, C)], A))]);
    filtera[((List[(B, C)], A),
              (List[(B, C)],
                A))](((aa: ((List[(B, C)], A), (List[(B, C)], A))) =>
                       {
                         val (ab, ba) =
                           aa: (((List[(B, C)], A), (List[(B, C)], A)));
                         ({
                            val (_, q) = ab: ((List[(B, C)], A));
                            ((ac: (List[(B, C)], A)) =>
                              {
                                val (_, qa) = ac: ((List[(B, C)], A));
                                ! (eq[A](q, qa))
                              })
                          })(ba)
                       }),
                      a ++ (b ++ c))
  }

def canonical_separatorb[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                                fsm[A, B, C],
                               q1: A, q2: A):
      fsm[sum[(A, A), A], B, C]
  =
  canonical_separator[A, B,
                       C](m, productd[A, B, C,
                                       A](from_fsm[A, B, C](m, q1),
   from_fsm[A, B, C](m, q2)),
                           q1, q2)

def select_diverging_ofsm_table_io[A : cenum : ceq : ccompare : equal : set_impl,
                                    B : ceq : ccompare : equal : linorder,
                                    C : ceq : ccompare : equal : linorder](m:
                                     fsm[A, B, C],
                                    q1: A, q2: A, k: nat):
      ((B, C), (Option[A], Option[A]))
  =
  {
    val ins = inputs_as_list[A, B, C](m): (List[B])
    val outs = outputs_as_list[A, B, C](m): (List[C])
    val table =
      ((a: A) =>
        ofsm_table[A, B,
                    C](m, ((_: A) => states[A, B, C](m)),
                        minus_nat(k, one_nata), a)):
        (A => set[A])
    val f =
      ((a: (B, C)) =>
        {
          val (x, y) = a: ((B, C));
          (((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y),
             (h_obs[A, B, C](m)).apply(q2).apply(x).apply(y))
             match {
             case (None, None) => None
             case (None, Some(q2a)) =>
               Some[((B, C),
                      (Option[A], Option[A]))](((x, y), (None, Some[A](q2a))))
             case (Some(q1a), None) =>
               Some[((B, C),
                      (Option[A], Option[A]))](((x, y), (Some[A](q1a), None)))
             case (Some(q1a), Some(q2a)) =>
               (if (! (set_eq[A](table(q1a), table(q2a))))
                 Some[((B, C),
                        (Option[A],
                          Option[A]))](((x, y), (Some[A](q1a), Some[A](q2a))))
                 else None)
           })
        }):
        (((B, C)) => Option[((B, C), (Option[A], Option[A]))]);
    hd[((B, C),
         (Option[A],
           Option[A]))](map_filter[(B, C),
                                    ((B, C),
                                      (Option[A],
Option[A]))](f, product[B, C](ins, outs)))
  }

def assemble_distinguishing_sequence_from_ofsm_table[A : cenum : ceq : ccompare : equal : set_impl,
              B : ceq : ccompare : equal : linorder,
              C : ceq : ccompare : equal : linorder](m: fsm[A, B, C], q1: A,
              q2: A, k: nat):
      List[(B, C)]
  =
  (if (equal_nata(k, zero_nat)) Nil
    else (select_diverging_ofsm_table_io[A, B,
  C](m, q1, q2, Suc(minus_nat(k, one_nata)))
            match {
            case ((x, y), (None, _)) => List((x, y))
            case ((x, y), (Some(_), None)) => List((x, y))
            case ((x, y), (Some(q1a), Some(q2a))) =>
              (x, y) ::
                assemble_distinguishing_sequence_from_ofsm_table[A, B,
                          C](m, q1a, q2a, minus_nat(k, one_nata))
          }))

def find_first_distinct_ofsm_table_no_check[A : cenum : ceq : ccompare : equal : set_impl,
     B : ceq : ccompare : equal,
     C : ceq : ccompare : equal](m: fsm[A, B, C], q1: A, q2: A, k: nat):
      nat
  =
  (if (! (set_eq[A](ofsm_table[A, B,
                                C](m, ((_: A) => states[A, B, C](m)), k, q1),
                     ofsm_table[A, B,
                                 C](m, ((_: A) => states[A, B, C](m)), k, q2))))
    k else find_first_distinct_ofsm_table_no_check[A, B, C](m, q1, q2, Suc(k)))

def find_first_distinct_ofsm_table_gta[A : cenum : ceq : ccompare : equal : set_impl,
B : ceq : ccompare : equal,
C : ceq : ccompare : equal](m: fsm[A, B, C], q1: A, q2: A, k: nat):
      nat
  =
  (if (member[A](q1, states[A, B, C](m)) &&
         (member[A](q2, states[A, B, C](m)) &&
           ! (member[A](q2, (ofsm_table_fix[A, B,
     C](m, ((_: A) => states[A, B, C](m)), zero_nat)).apply(q1)))))
    find_first_distinct_ofsm_table_no_check[A, B, C](m, q1, q2, k)
    else zero_nat)

def find_first_distinct_ofsm_table_gt[A : cenum : ceq : ccompare : equal : set_impl,
                                       B : ceq : ccompare : equal,
                                       C : ceq : ccompare : equal](m:
                             fsm[A, B, C],
                            q1: A, q2: A, k: nat):
      nat
  =
  find_first_distinct_ofsm_table_gta[A, B, C](m, q1, q2, k)

def get_distinguishing_sequence_from_ofsm_tables[A : cenum : ceq : ccompare : equal : set_impl,
          B : ceq : ccompare : equal : linorder,
          C : ceq : ccompare : equal : linorder](m: fsm[A, B, C], q1: A, q2: A):
      List[(B, C)]
  =
  {
    val a =
      find_first_distinct_ofsm_table_gt[A, B, C](m, q1, q2, zero_nat): nat;
    assemble_distinguishing_sequence_from_ofsm_table[A, B, C](m, q1, q2, a)
  }

def get_hsi[A : cenum : ceq : ccompare : equal : linorder : set_impl,
             B : ceq : ccompare : equal : mapping_impl : linorder,
             C : ceq : ccompare : equal : mapping_impl : linorder](m:
                             fsm[A, B, C],
                            q: A):
      prefix_tree[(B, C)]
  =
  from_list[(B, C)](map_filter[A, List[(B,
 C)]](((x: A) =>
        (if (! (eq[A](q, x)))
          Some[List[(B, C)]](get_distinguishing_sequence_from_ofsm_tables[A, B,
                                   C](m, q, x))
          else None)),
       states_as_list[A, B, C](m)))

def has_leaf[A : ccompare : equal : mapping_impl,
              B : ccompare : equal : mapping_impl,
              C](t: prefix_tree[(A, B)], g: C,
                  cg_lookup: C => (List[(A, B)]) => List[List[(A, B)]],
                  alpha: List[(A, B)]):
      Boolean
  =
  ! (is_none[List[(A, B)]](find[List[(A, B)]](((a: List[(A, B)]) =>
        is_maximal_in[(A, B)](t, a)),
       alpha :: (cg_lookup(g))(alpha))))

def empty_cg_empty: Unit = ()

def empty_cg_merge[A, B](g: Unit, u: List[(A, B)], v: List[(A, B)]): Unit =
  empty_cg_empty

def get_state_cover_assignment[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                                B : ceq : ccompare : equal : linorder,
                                C : ceq : ccompare : equal : linorder](m:
                                 fsm[A, B, C]):
      A => List[(B, C)]
  =
  {
    val path_assignments =
      reaching_paths_up_to_depth[A, B,
                                  C](m, insert[A](initial[A, B, C](m),
           bot_set[A]),
                                      bot_set[A],
                                      fun_upd[A,
       Option[List[(A, (B, (C, A)))]]](((_: A) => None), initial[A, B, C](m),
Some[List[(A, (B, (C, A)))]](Nil)),
                                      minus_nat(size[A, B, C](m), one_nata)):
        (A => Option[List[(A, (B, (C, A)))]]);
    ((q: A) =>
      (path_assignments(q) match {
         case None => Nil
         case Some(a) =>
           mapa[(A, (B, (C, A))),
                 (B, C)](((t: (A, (B, (C, A)))) =>
                           (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                             fst[C, A](snd[B,
    (C, A)](snd[A, (B, (C, A))](t))))),
                          a)
       }))
  }

def acyclic_language_intersection[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                                   B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                                   C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                                   D : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                                       fsm[A, B, C],
                                      a: fsm[D, B, C]):
      set[List[(B, C)]]
  =
  {
    val p = productd[A, B, C, D](m, a): (fsm[(A, D), B, C]);
    image[List[((A, D), (B, (C, (A, D))))],
           List[(B, C)]](((aa: List[((A, D), (B, (C, (A, D))))]) =>
                           mapa[((A, D), (B, (C, (A, D)))),
                                 (B, C)](((t: ((A, D), (B, (C, (A, D))))) =>
   (fst[B, (C, (A, D))](snd[(A, D), (B, (C, (A, D)))](t)),
     fst[C, (A, D)](snd[B, (C, (A, D))](snd[(A, D), (B, (C, (A, D)))](t))))),
  aa)),
                          acyclic_paths_up_to_length[(A, D), B,
              C](p, initial[(A, D), B, C](p),
                  minus_nat(size[D, B, C](a), one_nata)))
  }

def test_suite_to_io_maximal[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                              D : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m:
     fsm[A, B, C],
    x1: test_suite[A, B, C, D]):
      set[List[(B, C)]]
  =
  (m, x1) match {
  case (m, Test_suitea(prs, tps, rd_targets, atcs)) =>
    remove_proper_prefixes[(B, C)](Sup_setb[List[(B,
           C)]](image[(A, fsm[A, B, C]),
                       set[List[(B, C)]]](((a: (A, fsm[A, B, C])) =>
    {
      val (q, p) = a: ((A, fsm[A, B, C]));
      sup_seta[List[(B, C)]](Ls_acyclic[A, B, C](p, initial[A, B, C](p)),
                              Sup_setb[List[(B,
      C)]](image[List[(B, C)],
                  set[List[(B, C)]]](((ioP: List[(B, C)]) =>
                                       Sup_setb[List[(B,
               C)]](image[List[(A, (B, (C, A)))],
                           set[List[(B, C)]]](((pt: List[(A, (B, (C, A)))]) =>
        insert[List[(B, C)]](ioP ++
                               mapa[(A, (B, (C, A))),
                                     (B, C)](((t: (A, (B, (C, A)))) =>
       (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
         fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
      pt),
                              Sup_setb[List[(B,
      C)]](image[A, set[List[(B, C)]]](((qa: A) =>
 Sup_setb[List[(B, C)]](image[(fsm[D, B, C], (D, D)),
                               set[List[(B,
  C)]]](((aa: (fsm[D, B, C], (D, D))) =>
          {
            val (ab, (_, _)) = aa: ((fsm[D, B, C], (D, D)));
            image[List[(B, C)],
                   List[(B, C)]](((io_atc: List[(B, C)]) =>
                                   ioP ++
                                     (mapa[(A, (B, (C, A))),
    (B, C)](((t: (A, (B, (C, A)))) =>
              (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
             pt) ++
                                       io_atc)),
                                  remove_proper_prefixes[(B,
                   C)](acyclic_language_intersection[A, B, C,
              D](from_fsm[A, B, C](m, target[A, B, C](q, pt)), ab)))
          }),
         atcs((target[A, B, C](q, pt), qa))))),
rd_targets((q, pt)))))),
       tps(q)))),
                                      remove_proper_prefixes[(B,
                       C)](Ls_acyclic[A, B, C](p, initial[A, B, C](p))))))
    }),
   prs)))
}

def empty_cg_insert[A, B](g: Unit, v: List[(A, B)]): Unit = empty_cg_empty

def empty_cg_lookup[A, B](g: Unit, v: List[(A, B)]): List[List[(A, B)]] =
  List(v)

def extend_until_conflict[A : ceq : ccompare](non_confl_set: set[(A, A)],
       candidates: List[A], xs: List[A], k: nat):
      List[A]
  =
  (if (equal_nata(k, zero_nat)) xs
    else (dropwhile[A](((x: A) =>
                         ! (is_none[A](find[A](((y: A) =>
         ! (member[(A, A)]((x, y), non_confl_set))),
        xs)))),
                        candidates)
            match {
            case Nil => xs
            case c :: cs =>
              extend_until_conflict[A](non_confl_set, cs, c :: xs,
minus_nat(k, one_nata))
          }))

def empty_cg_initial[A, B, C](m: fsm[A, B, C], t: prefix_tree[(B, C)]): Unit =
  empty_cg_empty

def paths_up_to_length_with_targets[A, B,
                                     C](q: A,
 hM: A => B => List[(A, (B, (C, A)))], iM: List[B], k: nat):
      List[(List[(A, (B, (C, A)))], A)]
  =
  (if (equal_nata(k, zero_nat)) List((Nil, q))
    else (Nil, q) ::
           maps[B, (List[(A, (B, (C, A)))],
                     A)](((x: B) =>
                           maps[(A, (B, (C, A))),
                                 (List[(A, (B, (C, A)))],
                                   A)](((t: (A, (B, (C, A)))) =>
 mapa[(List[(A, (B, (C, A)))], A),
       (List[(A, (B, (C, A)))],
         A)](((a: (List[(A, (B, (C, A)))], A)) =>
               {
                 val (p, aa) = a: ((List[(A, (B, (C, A)))], A));
                 (t :: p, aa)
               }),
              paths_up_to_length_with_targets[A, B,
       C](snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))), hM, iM,
           minus_nat(k, one_nata)))),
(hM(q))(x))),
                          iM))

def get_pairs_h[A : card_univ : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                 B : ceq : ccompare : equal : mapping_impl : linorder,
                 C : ceq : ccompare : equal : linorder : set_impl](v:
                             A => List[(B, C)],
                            ma: fsm[A, B, C], m: nat):
      List[((List[(B, C)], A), (List[(B, C)], A))]
  =
  {
    val rstates = reachable_states_as_list[A, B, C](ma): (List[A])
    val n = card[A](reachable_states[A, B, C](ma)): nat
    val iM = inputs_as_list[A, B, C](ma): (List[B])
    val hMap =
      ((a: (A, B)) =>
        map_of[(A, B),
                List[(A, (B, (C, A)))]](mapa[(A, B),
      ((A, B),
        List[(A, (B, (C, A)))])](((aa: (A, B)) =>
                                   {
                                     val (q, x) = aa: ((A, B));
                                     ((q, x),
                                       mapa[(C, A),
     (A, (B, (C, A)))](((ab: (C, A)) => {
  val (y, qa) = ab: ((C, A));
  (q, (x, (y, qa)))
}),
                        sorted_list_of_set[(C, A)](h[A, B, C](ma, (q, x)))))
                                   }),
                                  product[A,
   B](states_as_list[A, B, C](ma), iM)),
 a)):
        (((A, B)) => Option[List[(A, (B, (C, A)))]])
    val hM =
      ((q: A) => (x: B) => (hMap((q, x)) match {
                              case None => Nil
                              case Some(ts) => ts
                            })):
        (A => B => List[(A, (B, (C, A)))])
    val pairs =
      pairs_to_distinguish[A, B,
                            C](ma, v,
                                ((q: A) =>
                                  paths_up_to_length_with_targets[A, B,
                           C](q, hM, iM, plus_nat(minus_nat(m, n), one_nata))),
                                rstates):
        (List[((List[(B, C)], A), (List[(B, C)], A))]);
    pairs
  }

def simple_cg_empty[A]: List[fset[List[A]]] = Nil

def can_merge_by_intersection[A : ceq : ccompare](x1: fset[List[A]],
           x2: fset[List[A]]):
      Boolean
  =
  Bex[List[A]](fset[List[A]](x1),
                ((alpha: List[A]) => member[List[A]](alpha, fset[List[A]](x2))))

def simple_cg_closure_phase_2_helper[A : ceq : ccompare](x1: fset[List[A]],
                  xs: List[fset[List[A]]]):
      (Boolean, (fset[List[A]], List[fset[List[A]]]))
  =
  {
    val (x2s, others) =
      separate_by[fset[List[A]]](((a: fset[List[A]]) =>
                                   can_merge_by_intersection[A](x1, a)),
                                  xs):
        ((List[fset[List[A]]], List[fset[List[A]]]))
    val x1Union =
      foldl[fset[List[A]],
             fset[List[A]]](((a: fset[List[A]]) => (b: fset[List[A]]) =>
                              sup_fset[List[A]](a, b)),
                             x1, x2s):
        (fset[List[A]]);
    (! (nulla[fset[List[A]]](x2s)), (x1Union, others))
  }

def simple_cg_closure_phase_2a[A : ceq : ccompare](x0: List[fset[List[A]]],
            x1: (Boolean, List[fset[List[A]]])):
      (Boolean, List[fset[List[A]]])
  =
  (x0, x1) match {
  case (Nil, (b, done)) => (b, done)
  case (x :: xs, (b, done)) =>
    (simple_cg_closure_phase_2_helper[A](x, xs) match {
       case (true, (xa, xsa)) =>
         simple_cg_closure_phase_2a[A](xsa, (true, xa :: done))
       case (false, (_, _)) => simple_cg_closure_phase_2a[A](xs, (b, x :: done))
     })
}

def simple_cg_closure_phase_2[A : ceq : ccompare](xs: List[fset[List[A]]]):
      (Boolean, List[fset[List[A]]])
  =
  simple_cg_closure_phase_2a[A](xs, (false, Nil))

def can_merge_by_suffix[A : ceq : ccompare : equal](x: fset[List[A]],
             x1: fset[List[A]], x2: fset[List[A]]):
      Boolean
  =
  Bex[List[A]](fset[List[A]](x),
                ((ys: List[A]) =>
                  Bex[List[A]](fset[List[A]](x1),
                                ((ys1: List[A]) =>
                                  is_prefix[A](ys, ys1) &&
                                    Bex[List[A]](fset[List[A]](x),
          ((ysa: List[A]) =>
            member[List[A]](ysa ++ drop[A](size_list[A].apply(ys), ys1),
                             fset[List[A]](x2))))))))

def simple_cg_closure_phase_1_helpera[A : ceq : ccompare : equal](x:
                            fset[List[A]],
                           x1: fset[List[A]], xs: List[fset[List[A]]]):
      (Boolean, (fset[List[A]], List[fset[List[A]]]))
  =
  {
    val (x2s, others) =
      separate_by[fset[List[A]]](((a: fset[List[A]]) =>
                                   can_merge_by_suffix[A](x, x1, a)),
                                  xs):
        ((List[fset[List[A]]], List[fset[List[A]]]))
    val x1Union =
      foldl[fset[List[A]],
             fset[List[A]]](((a: fset[List[A]]) => (b: fset[List[A]]) =>
                              sup_fset[List[A]](a, b)),
                             x1, x2s):
        (fset[List[A]]);
    (! (nulla[fset[List[A]]](x2s)), (x1Union, others))
  }

def simple_cg_closure_phase_1_helper[A : ceq : ccompare : equal](x:
                           fset[List[A]],
                          xa1: List[fset[List[A]]],
                          xa2: (Boolean, List[fset[List[A]]])):
      (Boolean, List[fset[List[A]]])
  =
  (x, xa1, xa2) match {
  case (x, Nil, (b, done)) => (b, done)
  case (x, x1 :: xs, (b, done)) =>
    {
      val (hasChanged, (x1a, xsa)) =
        simple_cg_closure_phase_1_helpera[A](x, x1, xs):
          ((Boolean, (fset[List[A]], List[fset[List[A]]])));
      simple_cg_closure_phase_1_helper[A](x, xsa,
   (b || hasChanged, x1a :: done))
    }
}

def simple_cg_closure_phase_1[A : ceq : ccompare : equal](xs:
                    List[fset[List[A]]]):
      (Boolean, List[fset[List[A]]])
  =
  foldl[(Boolean, List[fset[List[A]]]),
         fset[List[A]]](((a: (Boolean, List[fset[List[A]]])) =>
                          {
                            val (b, xsa) = a: ((Boolean, List[fset[List[A]]]));
                            ((x: fset[List[A]]) =>
                              {
                                val (ba, aa) =
                                  simple_cg_closure_phase_1_helper[A](x, xsa,
                               (false, Nil)):
                                    ((Boolean, List[fset[List[A]]]));
                                (b || ba, aa)
                              })
                          }),
                         (false, xs), xs)

def simple_cg_closure[A : ceq : ccompare : equal](g: List[fset[List[A]]]):
      List[fset[List[A]]]
  =
  {
    val (hasChanged1, g1) =
      simple_cg_closure_phase_1[A](g): ((Boolean, List[fset[List[A]]]))
    val (hasChanged2, g2) =
      simple_cg_closure_phase_2[A](g1): ((Boolean, List[fset[List[A]]]));
    (if (hasChanged1 || hasChanged2) simple_cg_closure[A](g2) else g2)
  }

def simple_cg_merge[A : ceq : ccompare : equal](g: List[fset[List[A]]],
         ys1: List[A], ys2: List[A]):
      List[fset[List[A]]]
  =
  simple_cg_closure[A](finsert[List[A]](ys1,
 finsert[List[A]](ys2, bot_fset[List[A]])) ::
                         g)

def prefix_pair_tests[A : finite_univ : ceq : cproper_interval : equal : set_impl,
                       B : ceq : ccompare,
                       C : ceq : ccompare](q: A,
    x1: set[(List[(A, (B, (C, A)))], (set[A], set[A]))]):
      set[(A, (List[(A, (B, (C, A)))], A))]
  =
  (q, x1) match {
  case (q, Rbt_set(t)) =>
    (ccompare_proda[List[(A, (B, (C, A)))], (set[A], set[A])] match {
       case None =>
         { sys.error("prefix_pair_tests RBT_set: ccompare = None");
           (((_: Unit) =>
              prefix_pair_tests[A, B,
                                 C](q, Rbt_set[(List[(A, (B, (C, A)))],
         (set[A], set[A]))](t)))).apply(())
           }
       case Some(_) =>
         set[(A, (List[(A, (B, (C, A)))],
                   A))](maps[(List[(A, (B, (C, A)))], (set[A], set[A])),
                              (A, (List[(A, (B, (C, A)))],
                                    A))](((a:
     (List[(A, (B, (C, A)))], (set[A], set[A])))
    =>
   {
     val (p, (rd, _)) = a: ((List[(A, (B, (C, A)))], (set[A], set[A])));
     concat[(A, (List[(A, (B, (C, A)))],
                  A))](map_filter[(List[(A, (B, (C, A)))],
                                    List[(A, (B, (C, A)))]),
                                   List[(A,
  (List[(A, (B, (C, A)))],
    A))]](((x: (List[(A, (B, (C, A)))], List[(A, (B, (C, A)))])) =>
            (if ({
                   val (p1, p2) =
                     x: ((List[(A, (B, (C, A)))], List[(A, (B, (C, A)))]));
                   ! (eq[A](target[A, B, C](q, p1), target[A, B, C](q, p2))) &&
                     (member[A](target[A, B, C](q, p1), rd) &&
                       member[A](target[A, B, C](q, p2), rd))
                 })
              Some[List[(A, (List[(A, (B, (C, A)))],
                              A))]]({
                                      val (p1, p2) =
x: ((List[(A, (B, (C, A)))], List[(A, (B, (C, A)))]));
                                      List((q, (p1, target[A, B, C](q, p2))),
    (q, (p2, target[A, B, C](q, p1))))
                                    })
              else None)),
           prefix_pairs[(A, (B, (C, A)))](p)))
   }),
  keysb[(List[(A, (B, (C, A)))], (set[A], set[A]))](t)))
     })
}

def simple_cg_inserta[A : ceq : ccompare : linorder](xs: List[fset[List[A]]],
              ys: List[A]):
      List[fset[List[A]]]
  =
  (find[fset[List[A]]](((x: fset[List[A]]) =>
                         member[List[A]](ys, fset[List[A]](x))),
                        xs)
     match {
     case None => finsert[List[A]](ys, bot_fset[List[A]]) :: xs
     case Some(_) => xs
   })

def simple_cg_insert[A : ceq : ccompare : linorder](xs: List[fset[List[A]]],
             ys: List[A]):
      List[fset[List[A]]]
  =
  foldl[List[fset[List[A]]],
         List[A]](((a: List[fset[List[A]]]) => (b: List[A]) =>
                    simple_cg_inserta[A](a, b)),
                   xs, prefixes[A](ys))

def dual_set_as_map_image[A : ccompare, B : ccompare,
                           C : ccompare : equal : mapping_impl,
                           D : ceq : ccompare : set_impl,
                           E : ccompare : equal : mapping_impl,
                           F : ceq : ccompare : set_impl](x0: set[(A, B)],
                   f1: ((A, B)) => (C, D), f2: ((A, B)) => (E, F)):
      (C => Option[set[D]], E => Option[set[F]])
  =
  (x0, f1, f2) match {
  case (Rbt_set(t), f1, f2) =>
    (ccompare_proda[A, B] match {
       case None =>
         { sys.error("dual_set_as_map_image RBT_set: ccompare = None");
           (((_: Unit) =>
              dual_set_as_map_image[A, B, C, D, E,
                                     F](Rbt_set[(A, B)](t), f1, f2))).apply(())
           }
       case Some(_) =>
         {
           val mm =
             (foldb[(A, B),
                     (mapping[C, set[D]],
                       mapping[E, set[F]])](((kv: (A, B)) =>
      (a: (mapping[C, set[D]], mapping[E, set[F]])) =>
      {
        val (m1, m2) = a: ((mapping[C, set[D]], mapping[E, set[F]]));
        ({
           val (x, z) = f1(kv): ((C, D));
           ((lookupa[C, set[D]](m1)).apply(x) match {
              case None => updateb[C, set[D]](x, insert[D](z, bot_set[D]), m1)
              case Some(zs) => updateb[C, set[D]](x, insert[D](z, zs), m1)
            })
         },
          {
            val (x, z) = f2(kv): ((E, F));
            ((lookupa[E, set[F]](m2)).apply(x) match {
               case None => updateb[E, set[F]](x, insert[F](z, bot_set[F]), m2)
               case Some(zs) => updateb[E, set[F]](x, insert[F](z, zs), m2)
             })
          })
      }),
     t)).apply((emptya[C, set[D]], emptya[E, set[F]])):
               ((mapping[C, set[D]], mapping[E, set[F]]));
           (lookupa[C, set[D]](fst[mapping[C, set[D]], mapping[E, set[F]]](mm)),
             lookupa[E, set[F]](snd[mapping[C, set[D]],
                                     mapping[E, set[F]]](mm)))
         }
     })
}

def paths_up_to_length_or_condition_with_witnessa[A : ceq : ccompare,
           B : ceq : ccompare, C : ceq : ccompare,
           D : finite_univ : cenum : ceq : cproper_interval : set_impl](f:
                                  A => set[(B, (C, A))],
                                 p: (List[(A, (B, (C, A)))]) => Option[D],
                                 prev: List[(A, (B, (C, A)))], k: nat, q: A):
      set[(List[(A, (B, (C, A)))], D)]
  =
  (if (equal_nata(k, zero_nat))
    (p(prev) match {
       case None => bot_set[(List[(A, (B, (C, A)))], D)]
       case Some(w) =>
         insert[(List[(A, (B, (C, A)))],
                  D)]((prev, w), bot_set[(List[(A, (B, (C, A)))], D)])
     })
    else (p(prev) match {
            case None =>
              Sup_setb[(List[(A, (B, (C, A)))],
                         D)](image[(B, (C, A)),
                                    set[(List[(A, (B, (C, A)))],
  D)]](((a: (B, (C, A))) =>
         {
           val (x, (y, qa)) = a: ((B, (C, A)));
           paths_up_to_length_or_condition_with_witnessa[A, B, C,
                  D](f, p, prev ++ List((q, (x, (y, qa)))),
                      minus_nat(k, one_nata), qa)
         }),
        f(q)))
            case Some(w) =>
              insert[(List[(A, (B, (C, A)))],
                       D)]((prev, w), bot_set[(List[(A, (B, (C, A)))], D)])
          }))

def paths_up_to_length_or_condition_with_witness[A : ceq : ccompare : equal : mapping_impl : set_impl,
          B : ceq : ccompare : set_impl, C : ceq : ccompare : set_impl,
          D : finite_univ : cenum : ceq : cproper_interval : set_impl](m:
                                 fsm[A, B, C],
                                p: (List[(A, (B, (C, A)))]) => Option[D],
                                k: nat, q: A):
      set[(List[(A, (B, (C, A)))], D)]
  =
  (if (member[A](q, states[A, B, C](m)))
    paths_up_to_length_or_condition_with_witnessa[A, B, C,
           D](((a: A) => h_from[A, B, C](m, a)), p, Nil, k, q)
    else bot_set[(List[(A, (B, (C, A)))], D)])

def m_traversal_paths_with_witness_up_to_length[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
         B : ceq : ccompare : set_impl,
         C : ceq : ccompare : set_impl](ma: fsm[A, B, C], q: A,
 d: List[(set[A], set[A])], m: nat, k: nat):
      set[(List[(A, (B, (C, A)))], (set[A], set[A]))]
  =
  paths_up_to_length_or_condition_with_witness[A, B, C,
        (set[A],
          set[A])](ma, ((p: List[(A, (B, (C, A)))]) =>
                         find[(set[A],
                                set[A])](((da: (set[A], set[A])) =>
   less_eq_nat(Suc(minus_nat(m, card[A](snd[set[A], set[A]](da)))),
                size_list[(A, (B, (C, A)))].apply(filtera[(A,
                    (B, (C, A)))](((t: (A, (B, (C, A)))) =>
                                    member[A](snd[C,
           A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
       fst[set[A], set[A]](da))),
                                   p)))),
  d)),
                    k, q)

def m_traversal_paths_with_witness[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                                    B : ceq : ccompare : set_impl,
                                    C : ceq : ccompare : set_impl](ma:
                             fsm[A, B, C],
                            q: A, d: List[(set[A], set[A])], m: nat):
      set[(List[(A, (B, (C, A)))], (set[A], set[A]))]
  =
  m_traversal_paths_with_witness_up_to_length[A, B,
       C](ma, q, d, m, Suc(times_nata(size[A, B, C](ma), m)))

def preamble_prefix_tests[A : finite_univ : ceq : cproper_interval : equal : set_impl,
                           B : ceq : ccompare,
                           C : ceq : ccompare](q: A,
        x1: set[(List[(A, (B, (C, A)))], (set[A], set[A]))], x2: set[A]):
      set[(A, (List[(A, (B, (C, A)))], A))]
  =
  (q, x1, x2) match {
  case (q, Rbt_set(t1), Rbt_set(t2)) =>
    (ccompare_proda[List[(A, (B, (C, A)))], (set[A], set[A])] match {
       case None =>
         { sys.error("prefix_pair_tests RBT_set: ccompare = None");
           (((_: Unit) =>
              preamble_prefix_tests[A, B,
                                     C](q,
 Rbt_set[(List[(A, (B, (C, A)))], (set[A], set[A]))](t1),
 Rbt_set[A](t2)))).apply(())
           }
       case Some(_) =>
         (ccompare[A] match {
            case None =>
              { sys.error("prefix_pair_tests RBT_set: ccompare = None");
                (((_: Unit) =>
                   preamble_prefix_tests[A, B,
  C](q, Rbt_set[(List[(A, (B, (C, A)))], (set[A], set[A]))](t1),
      Rbt_set[A](t2)))).apply(())
                }
            case Some(_) =>
              set[(A, (List[(A, (B, (C, A)))],
                        A))](maps[(List[(A, (B, (C, A)))], (set[A], set[A])),
                                   (A, (List[(A, (B, (C, A)))],
 A))](((a: (List[(A, (B, (C, A)))], (set[A], set[A]))) =>
        {
          val (p, (rd, _)) = a: ((List[(A, (B, (C, A)))], (set[A], set[A])));
          concat[(A, (List[(A, (B, (C, A)))],
                       A))](map_filter[(List[(A, (B, (C, A)))], A),
List[(A, (List[(A, (B, (C, A)))],
           A))]](((x: (List[(A, (B, (C, A)))], A)) =>
                   (if ({
                          val (p1, q2) = x: ((List[(A, (B, (C, A)))], A));
                          ! (eq[A](target[A, B, C](q, p1), q2)) &&
                            (member[A](target[A, B, C](q, p1), rd) &&
                              member[A](q2, rd))
                        })
                     Some[List[(A, (List[(A, (B, (C, A)))],
                                     A))]]({
     val (p1, q2) = x: ((List[(A, (B, (C, A)))], A));
     List((q, (p1, q2)), (q2, (Nil, target[A, B, C](q, p1))))
   })
                     else None)),
                  product[List[(A, (B, (C, A)))],
                           A](prefixes[(A, (B, (C, A)))](p), keysb[A](t2))))
        }),
       keysb[(List[(A, (B, (C, A)))], (set[A], set[A]))](t1)))
          })
     })
}

def preamble_pair_tests[A : finite_univ : cenum : ceq : cproper_interval : set_impl,
                         B : ceq : ccompare,
                         C : ceq : ccompare](drss: set[set[A]],
      rds: set[(A, A)]):
      set[(A, (List[(A, (B, (C, A)))], A))]
  =
  Sup_setb[(A, (List[(A, (B, (C, A)))],
                 A))](image[set[A],
                             set[(A, (List[(A, (B, (C, A)))],
                                       A))]](((drs: set[A]) =>
       image[(A, A),
              (A, (List[(A, (B, (C, A)))],
                    A))](((a: (A, A)) => {
   val (q1, q2) = a: ((A, A));
   (q1, (Nil, q2))
 }),
                          inf_seta[(A, A)](producte[A, A](drs, drs), rds))),
      drss))

def calculate_test_paths[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                          B : ceq : ccompare : equal : set_impl,
                          C : ceq : ccompare : equal : set_impl](ma:
                           fsm[A, B, C],
                          m: nat, d_reachable_states: set[A],
                          r_distinguishable_pairs: set[(A, A)],
                          repetition_sets: List[(set[A], set[A])]):
      (A => set[List[(A, (B, (C, A)))]],
        ((A, List[(A, (B, (C, A)))])) => set[A])
  =
  {
    val paths_with_witnesses =
      image[A, (A, set[(List[(A, (B, (C, A)))],
                         (set[A],
                           set[A]))])](((q: A) =>
 (q, m_traversal_paths_with_witness[A, B, C](ma, q, repetition_sets, m))),
d_reachable_states):
        (set[(A, set[(List[(A, (B, (C, A)))], (set[A], set[A]))])])
    val get_paths =
      ((x: A) =>
        ((set_as_map[A, set[(List[(A, (B, (C, A)))],
                              (set[A],
                                set[A]))]](paths_with_witnesses)).apply(x)
           match {
           case None =>
             set_empty[set[(List[(A, (B, (C, A)))],
                             (set[A],
                               set[A]))]](of_phantom[set[(List[(A,
                         (B, (C, A)))],
                   (set[A], set[A]))],
              set_impla](set_impl_seta[(List[(A, (B, (C, A)))],
 (set[A], set[A]))]))
           case Some(xs) => xs
         })):
        (A => set[set[(List[(A, (B, (C, A)))], (set[A], set[A]))]])
    val prefixPairTests =
      Sup_setb[(A, (List[(A, (B, (C, A)))],
                     A))](image[A, set[(A,
 (List[(A, (B, (C, A)))],
   A))]](((q: A) =>
           Sup_setb[(A, (List[(A, (B, (C, A)))],
                          A))](image[set[(List[(A, (B, (C, A)))],
   (set[A], set[A]))],
                                      set[(A,
    (List[(A, (B, (C, A)))],
      A))]](((a: set[(List[(A, (B, (C, A)))], (set[A], set[A]))]) =>
              prefix_pair_tests[A, B, C](q, a)),
             get_paths(q)))),
          d_reachable_states)):
        (set[(A, (List[(A, (B, (C, A)))], A))])
    val preamblePrefixTests =
      Sup_setb[(A, (List[(A, (B, (C, A)))],
                     A))](image[A, set[(A,
 (List[(A, (B, (C, A)))],
   A))]](((q: A) =>
           Sup_setb[(A, (List[(A, (B, (C, A)))],
                          A))](image[set[(List[(A, (B, (C, A)))],
   (set[A], set[A]))],
                                      set[(A,
    (List[(A, (B, (C, A)))],
      A))]](((mrsps: set[(List[(A, (B, (C, A)))], (set[A], set[A]))]) =>
              preamble_prefix_tests[A, B, C](q, mrsps, d_reachable_states)),
             get_paths(q)))),
          d_reachable_states)):
        (set[(A, (List[(A, (B, (C, A)))], A))])
    val preamblePairTests =
      preamble_pair_tests[A, B,
                           C](Sup_setb[set[A]](image[(A,
               set[(List[(A, (B, (C, A)))], (set[A], set[A]))]),
              set[set[A]]](((a: (A, set[(List[(A, (B, (C, A)))],
  (set[A], set[A]))]))
                              =>
                             {
                               val (_, aa) =
                                 a: ((A, set[(List[(A, (B, (C, A)))],
       (set[A], set[A]))]));
                               image[(List[(A, (B, (C, A)))], (set[A], set[A])),
                                      set[A]](((ab:
          (List[(A, (B, (C, A)))], (set[A], set[A])))
         =>
        {
          val (_, (_, dr)) = ab: ((List[(A, (B, (C, A)))], (set[A], set[A])));
          dr
        }),
       aa)
                             }),
                            paths_with_witnesses)),
                               r_distinguishable_pairs):
        (set[(A, (List[(A, (B, (C, A)))], A))])
    val tests =
      sup_seta[(A, (List[(A, (B, (C, A)))],
                     A))](sup_seta[(A, (List[(A, (B, (C, A)))],
 A))](prefixPairTests, preamblePrefixTests),
                           preamblePairTests):
        (set[(A, (List[(A, (B, (C, A)))], A))])
    val tps =
      ((x: A) =>
        ((set_as_map_image[A, set[(List[(A, (B, (C, A)))], (set[A], set[A]))],
                            A, set[List[(A,
  (B, (C, A)))]]](paths_with_witnesses,
                   ((a: (A, set[(List[(A, (B, (C, A)))], (set[A], set[A]))])) =>
                     {
                       val (q, p) =
                         a: ((A, set[(List[(A, (B, (C, A)))],
                                       (set[A], set[A]))]));
                       (q, image[(List[(A, (B, (C, A)))], (set[A], set[A])),
                                  List[(A,
 (B, (C, A)))]](((aa: (List[(A, (B, (C, A)))], (set[A], set[A]))) =>
                  fst[List[(A, (B, (C, A)))], (set[A], set[A])](aa)),
                 p))
                     }))).apply(x)
           match {
           case None =>
             Sup_setb[List[(A, (B, (C, A)))]](set_empty[set[List[(A,
                           (B, (C, A)))]]](of_phantom[set[List[(A,
                         (B, (C, A)))]],
               set_impla](set_impl_seta[List[(A, (B, (C, A)))]])))
           case Some(a) => Sup_setb[List[(A, (B, (C, A)))]](a)
         })):
        (A => set[List[(A, (B, (C, A)))]])
    val dual_maps =
      dual_set_as_map_image[A, (List[(A, (B, (C, A)))], A), A,
                             List[(A, (B, (C, A)))],
                             (A, List[(A, (B, (C, A)))]),
                             A](tests,
                                 ((a: (A, (List[(A, (B, (C, A)))], A))) =>
                                   {
                                     val (q, (p, _)) =
                                       a: ((A, (List[(A, (B, (C, A)))], A)));
                                     (q, p)
                                   }),
                                 ((a: (A, (List[(A, (B, (C, A)))], A))) =>
                                   {
                                     val (q, (p, aa)) =
                                       a: ((A, (List[(A, (B, (C, A)))], A)));
                                     ((q, p), aa)
                                   })):
        ((A => Option[set[List[(A, (B, (C, A)))]]],
          ((A, List[(A, (B, (C, A)))])) => Option[set[A]]))
    val tpsa =
      ((x: A) =>
        ((fst[A => Option[set[List[(A, (B, (C, A)))]]],
               ((A, List[(A, (B, (C, A)))])) =>
                 Option[set[A]]](dual_maps)).apply(x)
           match {
           case None =>
             set_empty[List[(A, (B, (C, A)))]](of_phantom[List[(A,
                         (B, (C, A)))],
                   set_impla](set_impl_lista[(A, (B, (C, A)))]))
           case Some(xs) => xs
         })):
        (A => set[List[(A, (B, (C, A)))]])
    val tpsb =
      ((q: A) => sup_seta[List[(A, (B, (C, A)))]](tps(q), tpsa(q))):
        (A => set[List[(A, (B, (C, A)))]])
    val a =
      ((x: (A, List[(A, (B, (C, A)))])) =>
        ((snd[A => Option[set[List[(A, (B, (C, A)))]]],
               ((A, List[(A, (B, (C, A)))])) =>
                 Option[set[A]]](dual_maps)).apply(x)
           match {
           case None => bot_set[A]
           case Some(xs) => xs
         })):
        (((A, List[(A, (B, (C, A)))])) => set[A]);
    (tpsb, a)
  }

def combine_test_suite[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl,
                        B : cenum : ceq : ccompare : equal : set_impl,
                        C : cenum : ceq : ccompare : equal : set_impl,
                        D : cenum : ceq : ccompare : equal : set_impl](ma:
                                 fsm[A, B, C],
                                m: nat,
                                states_with_preambles: set[(A, fsm[A, B, C])],
                                pairs_with_separators:
                                  set[((A, A), (fsm[D, B, C], (D, D)))],
                                repetition_sets: List[(set[A], set[A])]):
      test_suite[A, B, C, D]
  =
  {
    val drs =
      image[(A, fsm[A, B, C]),
             A](((a: (A, fsm[A, B, C])) => fst[A, fsm[A, B, C]](a)),
                 states_with_preambles):
        (set[A])
    val rds =
      image[((A, A), (fsm[D, B, C], (D, D))),
             (A, A)](((a: ((A, A), (fsm[D, B, C], (D, D)))) =>
                       fst[(A, A), (fsm[D, B, C], (D, D))](a)),
                      pairs_with_separators):
        (set[(A, A)])
    val tps_and_targets =
      calculate_test_paths[A, B, C](ma, m, drs, rds, repetition_sets):
        ((A => set[List[(A, (B, (C, A)))]],
          ((A, List[(A, (B, (C, A)))])) => set[A]))
    val a =
      ((x: (A, A)) =>
        ((set_as_map[(A, A),
                      (fsm[D, B, C], (D, D))](pairs_with_separators)).apply(x)
           match {
           case None => bot_set[(fsm[D, B, C], (D, D))]
           case Some(xs) => xs
         })):
        (((A, A)) => set[(fsm[D, B, C], (D, D))]);
    Test_suitea[A, B, C,
                 D](states_with_preambles,
                     fst[A => set[List[(A, (B, (C, A)))]],
                          ((A, List[(A, (B, (C, A)))])) =>
                            set[A]](tps_and_targets),
                     snd[A => set[List[(A, (B, (C, A)))]],
                          ((A, List[(A, (B, (C, A)))])) =>
                            set[A]](tps_and_targets),
                     a)
  }

def get_extension[A : ccompare : equal, B : ccompare : equal,
                   C](t: prefix_tree[(A, B)], g: C,
                       cg_lookup: C => (List[(A, B)]) => List[List[(A, B)]],
                       alpha: List[(A, B)], x: A, y: B):
      Option[List[(A, B)]]
  =
  find[List[(A, B)]](((beta: List[(A, B)]) =>
                       isin[(A, B)](t, beta ++ List((x, y)))),
                      alpha :: (cg_lookup(g))(alpha))

def sorted_list_of_sequences_in_tree[A : card_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl](x0:
                                     prefix_tree[A]):
      List[List[A]]
  =
  x0 match {
  case MPT(m) =>
    (if (is_empty[A](keys[A, prefix_tree[A]](m))) List(Nil)
      else Nil ::
             maps[A, List[A]](((k: A) =>
                                mapa[List[A],
                                      List[A]](((a: List[A]) => k :: a),
        sorted_list_of_sequences_in_tree[A](the[prefix_tree[A]]((lookupa[A,
                                  prefix_tree[A]](m)).apply(k))))),
                               sorted_list_of_set[A](keys[A,
                   prefix_tree[A]](m))))
}

def simple_cg_initial[A : ccompare : equal,
                       B : card_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl,
                       C : card_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl](m1:
                       fsm[A, B, C],
                      t: prefix_tree[(B, C)]):
      List[fset[List[(B, C)]]]
  =
  foldl[List[fset[List[(B, C)]]],
         List[(B, C)]](((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
                         simple_cg_inserta[(B, C)](a, b)),
                        simple_cg_empty[(B, C)],
                        filtera[List[(B, C)]](((a: List[(B, C)]) =>
        is_in_language[A, B, C](m1, initial[A, B, C](m1), a)),
       sorted_list_of_sequences_in_tree[(B, C)](t)))

def append_heuristic_input[A : linorder,
                            B : ccompare : equal : mapping_impl : linorder,
                            C : ceq : ccompare : equal : mapping_impl : linorder](m:
    fsm[A, B, C],
   t: prefix_tree[(B, C)], w: List[(B, C)], x3: (List[(B, C)], int),
   u: List[(B, C)]):
      (List[(B, C)], int)
  =
  (m, t, w, x3, u) match {
  case (m, t, w, (uBest, lBest), u) =>
    {
      val ta = aftera[(B, C)](t, u): (prefix_tree[(B, C)])
      val ws =
        maximum_fst_prefixes[B, C](ta, mapa[(B, C),
     B](((a: (B, C)) => fst[B, C](a)), w),
                                    outputs_as_list[A, B, C](m)):
          (List[List[(B, C)]]);
      (foldr[List[(B, C)],
              (List[(B, C)],
                int)](((wa: List[(B, C)]) => (a: (List[(B, C)], int)) =>
                        {
                          val (uBesta, lBesta) = a: ((List[(B, C)], int));
                          (if (equal_lista[(B, C)](wa, w)) (u, zero_int)
                            else (if (less_int(lBesta,
        int_of_nat(size_list[(B, C)].apply(wa))) ||
equal_int(int_of_nat(size_list[(B, C)].apply(wa)), lBesta) &&
  less_nat(size_list[(B, C)].apply(u), size_list[(B, C)].apply(uBesta)))
                                   (u, int_of_nat(size_list[(B, C)].apply(wa)))
                                   else (uBesta, lBesta)))
                        }),
                       ws)).apply((uBest, lBest))
    }
}

def shortest_list_in_tree_or_default[A : ccompare : equal](xs: List[List[A]],
                    t: prefix_tree[A], x: List[A]):
      List[A]
  =
  foldl[List[A],
         List[A]](((a: List[A]) => (b: List[A]) =>
                    (if (isin[A](t, a) &&
                           less_nat(size_list[A].apply(a),
                                     size_list[A].apply(b)))
                      a else b)),
                   x, xs)

def complete_inputs_to_tree[A : ccompare : equal : linorder,
                             B : ccompare : equal : mapping_impl : linorder,
                             C : ccompare : equal : mapping_impl : linorder](m:
                                       fsm[A, B, C],
                                      q: A, ys: List[C], x3: List[B]):
      prefix_tree[(B, C)]
  =
  (m, q, ys, x3) match {
  case (m, q, ys, Nil) => emptyc[(B, C)]
  case (m, q, ys, x :: xs) =>
    foldl[prefix_tree[(B, C)],
           C](((t: prefix_tree[(B, C)]) => (y: C) =>
                ((h_obs[A, B, C](m)).apply(q).apply(x).apply(y) match {
                   case None => insertb[(B, C)](t, List((x, y)))
                   case Some(qa) =>
                     combine_after[(B, C)](t, List((x, y)),
    complete_inputs_to_tree[A, B, C](m, qa, ys, xs))
                 })),
               emptyc[(B, C)], ys)
}

def distribute_extension[A : ccompare : equal : linorder,
                          B : ccompare : equal : mapping_impl : linorder,
                          C : ceq : ccompare : equal : mapping_impl : linorder,
                          D](m: fsm[A, B, C], t: prefix_tree[(B, C)], g: D,
                              cg_lookup:
                                D => (List[(B, C)]) => List[List[(B, C)]],
                              cg_insert: D => (List[(B, C)]) => D,
                              u: List[(B, C)], w: List[(B, C)],
                              completeInputTraces: Boolean,
                              append_heuristic:
                                (prefix_tree[(B, C)]) =>
                                  (List[(B, C)]) =>
                                    ((List[(B, C)], int)) =>
                                      (List[(B, C)]) => (List[(B, C)], int)):
      (prefix_tree[(B, C)], D)
  =
  {
    val cu = (cg_lookup(g))(u): (List[List[(B, C)]])
    val u0 = shortest_list_in_tree_or_default[(B, C)](cu, t, u): (List[(B, C)])
    val l0 = uminus_int(one_int): int
    val ua =
      fst[List[(B, C)],
           int](foldl[(List[(B, C)], int),
                       List[(B, C)]]((append_heuristic(t))(w), (u0, l0),
                                      filtera[List[(B,
             C)]](((a: List[(B, C)]) => isin[(B, C)](t, a)), cu))):
        (List[(B, C)])
    val ta = insertb[(B, C)](t, ua ++ w): (prefix_tree[(B, C)])
    val ga =
      (cg_insert(g))(maximal_prefix_in_language[A, B,
         C](m, initial[A, B, C](m), ua ++ w)):
        D;
    (if (completeInputTraces)
      {
        val tc =
          complete_inputs_to_tree[A, B,
                                   C](m, initial[A, B, C](m),
                                       outputs_as_list[A, B, C](m),
                                       mapa[(B, C),
     B](((a: (B, C)) => fst[B, C](a)), ua ++ w)):
            (prefix_tree[(B, C)])
        val tb = combinea[(B, C)](ta, tc): (prefix_tree[(B, C)]);
        (tb, ga)
      }
      else (ta, ga))
  }

def append_heuristic_io[A : ccompare : equal : mapping_impl,
                         B : ccompare : equal : mapping_impl](t:
                        prefix_tree[(A, B)],
                       w: List[(A, B)], x2: (List[(A, B)], int),
                       u: List[(A, B)]):
      (List[(A, B)], int)
  =
  (t, w, x2, u) match {
  case (t, w, (uBest, lBest), u) =>
    {
      val ta = aftera[(A, B)](t, u): (prefix_tree[(A, B)])
      val wa = maximum_prefix[(A, B)](ta, w): (List[(A, B)]);
      (if (equal_lista[(A, B)](wa, w)) (u, zero_int)
        else (if (is_maximal_in[(A, B)](ta, wa) &&
                    (less_int(lBest, int_of_nat(size_list[(A, B)].apply(wa))) ||
                      equal_int(int_of_nat(size_list[(A, B)].apply(wa)),
                                 lBest) &&
                        less_nat(size_list[(A, B)].apply(u),
                                  size_list[(A, B)].apply(uBest))))
               (u, int_of_nat(size_list[(A, B)].apply(wa)))
               else (uBest, lBest)))
    }
}

def handle_io_pair[A : ccompare : equal : linorder,
                    B : ccompare : equal : mapping_impl : linorder,
                    C : ceq : ccompare : equal : mapping_impl : linorder,
                    D](completeInputTraces: Boolean, useInputHeuristic: Boolean,
                        m: fsm[A, B, C], v: A => List[(B, C)],
                        t: prefix_tree[(B, C)], g: D,
                        cg_insert: D => (List[(B, C)]) => D,
                        cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                        q: A, x: B, y: C):
      (prefix_tree[(B, C)], D)
  =
  distribute_extension[A, B, C,
                        D](m, t, g, cg_lookup, cg_insert, v(q), List((x, y)),
                            completeInputTraces,
                            (if (useInputHeuristic)
                              ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
                                (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
                                append_heuristic_input[A, B, C](m, a, b, c, d))
                              else ((a: prefix_tree[(B, C)]) =>
                                     (b: List[(B, C)]) =>
                                     (c: (List[(B, C)], int)) =>
                                     (d: List[(B, C)]) =>
                                     append_heuristic_io[B, C](a, b, c, d))))

def estimate_growth[A : cenum : ceq : ccompare : equal : linorder : set_impl,
                     B : ccompare : equal : linorder,
                     C : ccompare : equal : linorder](m: fsm[A, B, C],
               dist_fun: A => A => List[(B, C)], q1: A, q2: A, x: B, y: C,
               errorValue: nat):
      nat
  =
  ((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y) match {
     case None => ((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y) match {
                     case None => errorValue
                     case Some(_) => one_nata
                   })
     case Some(q1a) =>
       ((h_obs[A, B, C](m)).apply(q2).apply(x).apply(y) match {
          case None => one_nata
          case Some(q2a) =>
            (if (eq[A](q1a, q2a) ||
                   set_eq[A](insert[A](q1a, insert[A](q2a, bot_set[A])),
                              insert[A](q1, insert[A](q2, bot_set[A]))))
              errorValue
              else plus_nat(one_nata,
                             times_nata(nat_of_integer(BigInt(2)),
 size_list[(B, C)].apply((dist_fun(q1))(q2)))))
        })
   })

def sorted_list_of_maximal_sequences_in_tree[A : card_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl](x0:
     prefix_tree[A]):
      List[List[A]]
  =
  x0 match {
  case MPT(m) =>
    (if (is_empty[A](keys[A, prefix_tree[A]](m))) List(Nil)
      else maps[A, List[A]](((k: A) =>
                              mapa[List[A],
                                    List[A]](((a: List[A]) => k :: a),
      sorted_list_of_maximal_sequences_in_tree[A](the[prefix_tree[A]]((lookupa[A,
prefix_tree[A]](m)).apply(k))))),
                             sorted_list_of_set[A](keys[A, prefix_tree[A]](m))))
}

def traces_to_check[A : ccompare : equal, B : ceq : ccompare : equal : linorder,
                     C : ceq : ccompare : equal : linorder](m: fsm[A, B, C],
                     q: A, k: nat):
      List[List[(B, C)]]
  =
  (if (equal_nata(k, zero_nat)) Nil
    else {
           val a =
             product[B, C](inputs_as_list[A, B, C](m),
                            outputs_as_list[A, B, C](m)):
               (List[(B, C)]);
           maps[(B, C),
                 List[(B, C)]](((aa: (B, C)) =>
                                 {
                                   val (x, y) = aa: ((B, C));
                                   ((h_obs[A, B,
    C](m)).apply(q).apply(x).apply(y)
                                      match {
                                      case None => List(List((x, y)))
                                      case Some(qa) =>
List((x, y)) ::
  mapa[List[(B, C)],
        List[(B, C)]](((ab: List[(B, C)]) => (x, y) :: ab),
                       traces_to_check[A, B, C](m, qa, minus_nat(k, one_nata)))
                                    })
                                 }),
                                a)
         })

def establish_convergence_static[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                                  B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  D](dist_fun: nat => A => prefix_tree[(B, C)],
                                      ma: fsm[A, B, C], v: A => List[(B, C)],
                                      ta: prefix_tree[(B, C)], g: D,
                                      cg_insert: D => (List[(B, C)]) => D,
                                      cg_lookup:
D => (List[(B, C)]) => List[List[(B, C)]],
                                      m: nat, t: (A, (B, (C, A)))):
      (prefix_tree[(B, C)], D)
  =
  {
    val alpha = v(fst[A, (B, (C, A))](t)): (List[(B, C)])
    val xy =
      (fst[B, (C, A)](snd[A, (B, (C, A))](t)),
        fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))):
        ((B, C))
    val beta =
      v(snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))): (List[(B, C)]);
    after[A, B, C](ma, initial[A, B, C](ma), v(fst[A, (B, (C, A))](t)))
    val qTarget =
      after[A, B,
             C](ma, initial[A, B, C](ma),
                 v(snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))):
        A
    val k = minus_nat(m, card[A](reachable_states[A, B, C](ma))): nat
    val ttc =
      Nil :: traces_to_check[A, B, C](ma, qTarget, k): (List[List[(B, C)]])
    val handleTrace =
      ((a: (prefix_tree[(B, C)], D)) =>
        {
          val (tb, ga) = a: ((prefix_tree[(B, C)], D));
          ((u: List[(B, C)]) =>
            (if (is_in_language[A, B, C](ma, qTarget, u))
              {
                val qu = after[A, B, C](ma, qTarget, u): A
                val ws =
                  sorted_list_of_maximal_sequences_in_tree[(B,
                     C)]((dist_fun(Suc(size_list[(B, C)].apply(u))))(qu)):
                    (List[List[(B, C)]])
                val appendDistTrace =
                  ((aa: (prefix_tree[(B, C)], D)) =>
                    {
                      val (tc, gb) = aa: ((prefix_tree[(B, C)], D));
                      ((w: List[(B, C)]) =>
                        {
                          val (td, gc) =
                            distribute_extension[A, B, C,
          D](ma, tc, gb, cg_lookup, cg_insert, alpha, xy :: u ++ w, false,
              ((ab: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
                (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
                append_heuristic_input[A, B, C](ma, ab, b, c, d))):
                              ((prefix_tree[(B, C)], D));
                          distribute_extension[A, B, C,
        D](ma, td, gc, cg_lookup, cg_insert, beta, u ++ w, false,
            ((ab: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
              (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
              append_heuristic_input[A, B, C](ma, ab, b, c, d)))
                        })
                    }):
                    (((prefix_tree[(B, C)], D)) =>
                      (List[(B, C)]) => (prefix_tree[(B, C)], D));
                foldl[(prefix_tree[(B, C)], D),
                       List[(B, C)]](appendDistTrace, (tb, ga), ws)
              }
              else {
                     val (tc, gb) =
                       distribute_extension[A, B, C,
     D](ma, tb, ga, cg_lookup, cg_insert, alpha, xy :: u, false,
         ((aa: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
           (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
           append_heuristic_input[A, B, C](ma, aa, b, c, d))):
                         ((prefix_tree[(B, C)], D));
                     distribute_extension[A, B, C,
   D](ma, tc, gb, cg_lookup, cg_insert, beta, u, false,
       ((aa: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
         (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
         append_heuristic_input[A, B, C](ma, aa, b, c, d)))
                   }))
        }):
        (((prefix_tree[(B, C)], D)) =>
          (List[(B, C)]) => (prefix_tree[(B, C)], D));
    foldl[(prefix_tree[(B, C)], D), List[(B, C)]](handleTrace, (ta, g), ttc)
  }

def handleut_static[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                     B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                     C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                     D](dist_fun: nat => A => prefix_tree[(B, C)],
                         m: fsm[A, B, C], v: A => List[(B, C)],
                         ta: prefix_tree[(B, C)], g: D,
                         cg_insert: D => (List[(B, C)]) => D,
                         cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                         cg_merge: D => (List[(B, C)]) => (List[(B, C)]) => D,
                         l: nat, t: (A, (B, (C, A))),
                         x: List[(A, (B, (C, A)))]):
      (List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D))
  =
  {
    val (t1, g1) =
      handle_io_pair[A, B, C,
                      D](false, false, m, v, ta, g, cg_insert, cg_lookup,
                          fst[A, (B, (C, A))](t),
                          fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                          fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))):
        ((prefix_tree[(B, C)], D))
    val (t2, g2) =
      establish_convergence_static[A, B, C,
                                    D](dist_fun, m, v, t1, g1, cg_insert,
cg_lookup, l, t):
        ((prefix_tree[(B, C)], D))
    val g3 =
      ((cg_merge(g2))(v(fst[A, (B, (C, A))](t)) ++
                        List((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                               fst[C, A](snd[B,
      (C, A)](snd[A, (B, (C, A))](t)))))))(v(snd[C,
          A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))):
        D;
    (x, (t2, g3))
  }

def shortest_list_or_default[A](xs: List[List[A]], x: List[A]): List[A] =
  foldl[List[A],
         List[A]](((a: List[A]) => (b: List[A]) =>
                    (if (less_nat(size_list[A].apply(a), size_list[A].apply(b)))
                      a else b)),
                   x, xs)

def get_prefix_of_separating_sequence[A : cenum : ceq : ccompare : equal : linorder : set_impl,
                                       B : ceq : ccompare : equal : mapping_impl : linorder,
                                       C : ceq : ccompare : equal : mapping_impl : linorder,
                                       D](m: fsm[A, B, C],
   t: prefix_tree[(B, C)], g: D,
   cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
   get_distinguishing_trace: A => A => List[(B, C)], u: List[(B, C)],
   v: List[(B, C)], k: nat):
      (nat, List[(B, C)])
  =
  (if (equal_nata(k, zero_nat)) (one_nata, Nil)
    else {
           val ua =
             shortest_list_or_default[(B, C)]((cg_lookup(g))(u), u):
               (List[(B, C)])
           val va =
             shortest_list_or_default[(B, C)]((cg_lookup(g))(v), v):
               (List[(B, C)])
           val su = after[A, B, C](m, initial[A, B, C](m), u): A
           val sv = after[A, B, C](m, initial[A, B, C](m), v): A
           val bestPrefix0 = (get_distinguishing_trace(su))(sv): (List[(B, C)])
           val minEst0 =
             plus_nat(plus_nat(size_list[(B, C)].apply(bestPrefix0),
                                (if (has_leaf[B, C, D](t, g, cg_lookup, ua))
                                  zero_nat else size_list[(B, C)].apply(ua))),
                       (if (has_leaf[B, C, D](t, g, cg_lookup, va)) zero_nat
                         else size_list[(B, C)].apply(va))):
               nat
           val errorValue = Suc(minEst0): nat
           val xy =
             product[B, C](inputs_as_list[A, B, C](m),
                            outputs_as_list[A, B, C](m)):
               (List[(B, C)])
           val tryIO =
             ((a: (nat, List[(B, C)])) =>
               {
                 val (minEst, bestPrefix) = a: ((nat, List[(B, C)]));
                 ((aa: (B, C)) =>
                   {
                     val (x, y) = aa: ((B, C));
                     (if (equal_nata(minEst, zero_nat)) (minEst, bestPrefix)
                       else (get_extension[B, C, D](t, g, cg_lookup, ua, x, y)
                               match {
                               case None =>
                                 (get_extension[B, C,
         D](t, g, cg_lookup, va, x, y)
                                    match {
                                    case None => (minEst, bestPrefix)
                                    case Some(vb) =>
                                      {
val e =
  estimate_growth[A, B,
                   C](m, get_distinguishing_trace, su, sv, x, y, errorValue):
    nat
val ea =
  (if (! (equal_nata(e, one_nata)))
    (if (has_leaf[B, C, D](t, g, cg_lookup, vb)) plus_nat(e, one_nata)
      else (if (! (has_leaf[B, C, D](t, g, cg_lookup, vb ++ List((x, y)))))
             plus_nat(plus_nat(e, size_list[(B, C)].apply(va)), one_nata)
             else e))
    else e):
    nat
val eb =
  plus_nat(ea, (if (! (has_leaf[B, C, D](t, g, cg_lookup, ua)))
                 size_list[(B, C)].apply(ua) else zero_nat)):
    nat;
(if (less_eq_nat(eb, minEst)) (eb, List((x, y))) else (minEst, bestPrefix))
                                      }
                                  })
                               case Some(ub) =>
                                 (get_extension[B, C,
         D](t, g, cg_lookup, va, x, y)
                                    match {
                                    case None =>
                                      {
val e =
  estimate_growth[A, B,
                   C](m, get_distinguishing_trace, su, sv, x, y, errorValue):
    nat
val ea =
  (if (! (equal_nata(e, one_nata)))
    (if (has_leaf[B, C, D](t, g, cg_lookup, ub)) plus_nat(e, one_nata)
      else (if (! (has_leaf[B, C, D](t, g, cg_lookup, ub ++ List((x, y)))))
             plus_nat(plus_nat(e, size_list[(B, C)].apply(ua)), one_nata)
             else e))
    else e):
    nat
val eb =
  plus_nat(ea, (if (! (has_leaf[B, C, D](t, g, cg_lookup, va)))
                 size_list[(B, C)].apply(va) else zero_nat)):
    nat;
(if (less_eq_nat(eb, minEst)) (eb, List((x, y))) else (minEst, bestPrefix))
                                      }
                                    case Some(vb) =>
                                      (if (! (equal_boola(is_none[A]((h_obs[A,
                                     B, C](m)).apply(su).apply(x).apply(y)),
                   is_none[A]((h_obs[A, B,
                                      C](m)).apply(sv).apply(x).apply(y)))))
(zero_nat, Nil)
else (if (equal_option[A]((h_obs[A, B, C](m)).apply(su).apply(x).apply(y),
                           (h_obs[A, B, C](m)).apply(sv).apply(x).apply(y)))
       (minEst, bestPrefix)
       else {
              val (e, w) =
                get_prefix_of_separating_sequence[A, B, C,
           D](m, t, g, cg_lookup, get_distinguishing_trace, ub ++ List((x, y)),
               vb ++ List((x, y)), minus_nat(k, one_nata)):
                  ((nat, List[(B, C)]));
              (if (equal_nata(e, zero_nat)) (zero_nat, Nil)
                else (if (less_eq_nat(e, minEst)) (e, (x, y) :: w)
                       else (minEst, bestPrefix)))
            }))
                                  })
                             }))
                   })
               }):
               (((nat, List[(B, C)])) => ((B, C)) => (nat, List[(B, C)]));
           (if (! (isin[(B, C)](t, ua)) || ! (isin[(B, C)](t, va)))
             (errorValue, Nil)
             else foldl[(nat, List[(B, C)]), (B, C)](tryIO, (minEst0, Nil), xy))
         })

def spyh_distinguish[A : cenum : ceq : ccompare : equal : linorder : set_impl,
                      B : ceq : ccompare : equal : mapping_impl : linorder,
                      C : ceq : ccompare : equal : mapping_impl : linorder,
                      D](m: fsm[A, B, C], t: prefix_tree[(B, C)], g: D,
                          cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                          cg_insert: D => (List[(B, C)]) => D,
                          get_distinguishing_trace: A => A => List[(B, C)],
                          u: List[(B, C)], x: List[List[(B, C)]], k: nat,
                          completeInputTraces: Boolean,
                          append_heuristic:
                            (prefix_tree[(B, C)]) =>
                              (List[(B, C)]) =>
                                ((List[(B, C)], int)) =>
                                  (List[(B, C)]) => (List[(B, C)], int)):
      (prefix_tree[(B, C)], D)
  =
  {
    val dist_helper =
      ((a: (prefix_tree[(B, C)], D)) =>
        {
          val (ta, ga) = a: ((prefix_tree[(B, C)], D));
          ((v: List[(B, C)]) =>
            (if (eq[A](after[A, B, C](m, initial[A, B, C](m), u),
                        after[A, B, C](m, initial[A, B, C](m), v)))
              (ta, ga)
              else {
                     val ew =
                       get_prefix_of_separating_sequence[A, B, C,
                  D](m, ta, ga, cg_lookup, get_distinguishing_trace, u, v, k):
                         ((nat, List[(B, C)]));
                     (if (equal_nata(fst[nat, List[(B, C)]](ew), zero_nat))
                       (ta, ga)
                       else {
                              val ua =
                                u ++ snd[nat, List[(B, C)]](ew): (List[(B, C)])
                              val va =
                                v ++ snd[nat, List[(B, C)]](ew): (List[(B, C)])
                              val w =
                                (if (does_distinguish[A, B,
               C](m, after[A, B, C](m, initial[A, B, C](m), u),
                   after[A, B, C](m, initial[A, B, C](m), v),
                   snd[nat, List[(B, C)]](ew)))
                                  snd[nat, List[(B, C)]](ew)
                                  else snd[nat, List[(B, C)]](ew) ++
 (get_distinguishing_trace(after[A, B,
                                  C](m, initial[A, B, C](m),
                                      ua)))(after[A, B,
           C](m, initial[A, B, C](m), va))):
                                  (List[(B, C)])
                              val tg =
                                distribute_extension[A, B, C,
              D](m, ta, ga, cg_lookup, cg_insert, u, w, completeInputTraces,
                  append_heuristic):
                                  ((prefix_tree[(B, C)], D));
                              distribute_extension[A, B, C,
            D](m, fst[prefix_tree[(B, C)], D](tg),
                snd[prefix_tree[(B, C)], D](tg), cg_lookup, cg_insert, v, w,
                completeInputTraces, append_heuristic)
                            })
                   }))
        }):
        (((prefix_tree[(B, C)], D)) =>
          (List[(B, C)]) => (prefix_tree[(B, C)], D));
    foldl[(prefix_tree[(B, C)], D), List[(B, C)]](dist_helper, (t, g), x)
  }

def distinguish_from_set[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
                          B : ceq : ccompare : equal : mapping_impl : linorder,
                          C : ceq : ccompare : equal : mapping_impl : linorder,
                          D](m: fsm[A, B, C], va: A => List[(B, C)],
                              t: prefix_tree[(B, C)], g: D,
                              cg_lookup:
                                D => (List[(B, C)]) => List[List[(B, C)]],
                              cg_insert: D => (List[(B, C)]) => D,
                              get_distinguishing_trace: A => A => List[(B, C)],
                              u: List[(B, C)], v: List[(B, C)],
                              x: List[List[(B, C)]], k: nat, depth: nat,
                              completeInputTraces: Boolean,
                              append_heuristic:
                                (prefix_tree[(B, C)]) =>
                                  (List[(B, C)]) =>
                                    ((List[(B, C)], int)) =>
                                      (List[(B, C)]) => (List[(B, C)], int),
                              u_is_v: Boolean):
      (prefix_tree[(B, C)], D)
  =
  {
    val tg =
      spyh_distinguish[A, B, C,
                        D](m, t, g, cg_lookup, cg_insert,
                            get_distinguishing_trace, u, x, k,
                            completeInputTraces, append_heuristic):
        ((prefix_tree[(B, C)], D))
    val vClass =
      insert[List[(B, C)]](v, set[List[(B,
 C)]]((cg_lookup(snd[prefix_tree[(B, C)], D](tg)))(v))):
        (set[List[(B, C)]])
    val notReferenced =
      ! u_is_v &&
        Ball[A](reachable_states[A, B, C](m),
                 ((q: A) => ! (member[List[(B, C)]](va(q), vClass)))):
        Boolean
    val tga =
      (if (notReferenced)
        spyh_distinguish[A, B, C,
                          D](m, fst[prefix_tree[(B, C)], D](tg),
                              snd[prefix_tree[(B, C)], D](tg), cg_lookup,
                              cg_insert, get_distinguishing_trace, v, x, k,
                              completeInputTraces, append_heuristic)
        else tg):
        ((prefix_tree[(B, C)], D));
    (if (less_nat(zero_nat, depth))
      {
        val xa =
          (if (notReferenced) v :: u :: x else u :: x): (List[List[(B, C)]])
        val xy =
          product[B, C](inputs_as_list[A, B, C](m),
                         outputs_as_list[A, B, C](m)):
            (List[(B, C)])
        val handleIO =
          ((a: (prefix_tree[(B, C)], D)) =>
            {
              val (ta, ga) = a: ((prefix_tree[(B, C)], D));
              ((aa: (B, C)) =>
                {
                  val (xaa, y) = aa: ((B, C))
                  val tGu =
                    distribute_extension[A, B, C,
  D](m, ta, ga, cg_lookup, cg_insert, u, List((xaa, y)), completeInputTraces,
      append_heuristic):
                      ((prefix_tree[(B, C)], D))
                  val tGv =
                    (if (u_is_v) tGu
                      else distribute_extension[A, B, C,
         D](m, fst[prefix_tree[(B, C)], D](tGu),
             snd[prefix_tree[(B, C)], D](tGu), cg_lookup, cg_insert, v,
             List((xaa, y)), completeInputTraces, append_heuristic)):
                      ((prefix_tree[(B, C)], D));
                  (if (is_in_language[A, B,
                                       C](m, initial[A, B, C](m),
   u ++ List((xaa, y))))
                    distinguish_from_set[A, B, C,
  D](m, va, fst[prefix_tree[(B, C)], D](tGv), snd[prefix_tree[(B, C)], D](tGv),
      cg_lookup, cg_insert, get_distinguishing_trace, u ++ List((xaa, y)),
      v ++ List((xaa, y)), xa, k, minus_nat(depth, one_nata),
      completeInputTraces, append_heuristic, u_is_v)
                    else tGv)
                })
            }):
            (((prefix_tree[(B, C)], D)) =>
              ((B, C)) => (prefix_tree[(B, C)], D));
        foldl[(prefix_tree[(B, C)], D), (B, C)](handleIO, tga, xy)
      }
      else tga)
  }

def handleut_dynamic[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
                      B : ceq : ccompare : equal : mapping_impl : linorder,
                      C : ceq : ccompare : equal : mapping_impl : linorder,
                      D](complete_input_traces: Boolean,
                          use_input_heuristic: Boolean,
                          dist_fun: A => A => List[(B, C)],
                          do_establish_convergence:
                            (fsm[A, B, C]) =>
                              (A => List[(B, C)]) =>
                                ((A, (B, (C, A)))) =>
                                  (List[(A, (B, (C, A)))]) => nat => Boolean,
                          ma: fsm[A, B, C], v: A => List[(B, C)],
                          ta: prefix_tree[(B, C)], g: D,
                          cg_insert: D => (List[(B, C)]) => D,
                          cg_lookup: D => (List[(B, C)]) => List[List[(B, C)]],
                          cg_merge: D => (List[(B, C)]) => (List[(B, C)]) => D,
                          m: nat, t: (A, (B, (C, A))),
                          x: List[(A, (B, (C, A)))]):
      (List[(A, (B, (C, A)))], (prefix_tree[(B, C)], D))
  =
  {
    val k = times_nata(nat_of_integer(BigInt(2)), size[A, B, C](ma)): nat
    val l = minus_nat(m, card[A](reachable_states[A, B, C](ma))): nat
    val heuristic =
      (if (use_input_heuristic)
        ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
          (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
          append_heuristic_input[A, B, C](ma, a, b, c, d))
        else ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
               (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
               append_heuristic_io[B, C](a, b, c, d))):
        ((prefix_tree[(B, C)]) =>
          (List[(B, C)]) =>
            ((List[(B, C)], int)) => (List[(B, C)]) => (List[(B, C)], int))
    val rstates =
      mapa[A, List[(B, C)]](v, reachable_states_as_list[A, B, C](ma)):
        (List[List[(B, C)]])
    val (t1, g1) =
      handle_io_pair[A, B, C,
                      D](complete_input_traces, use_input_heuristic, ma, v, ta,
                          g, cg_insert, cg_lookup, fst[A, (B, (C, A))](t),
                          fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                          fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))):
        ((prefix_tree[(B, C)], D))
    val u =
      v(fst[A, (B, (C, A))](t)) ++
        List((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
               fst[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))):
        (List[(B, C)])
    val va =
      v(snd[C, A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))): (List[(B, C)])
    val xa = butlast[(A, (B, (C, A)))](x): (List[(A, (B, (C, A)))]);
    (if (((((do_establish_convergence(ma))(v))(t))(xa))(l))
      {
        val (t2, g2) =
          distinguish_from_set[A, B, C,
                                D](ma, v, t1, g1, cg_lookup, cg_insert,
                                    dist_fun, u, va, rstates, k, l,
                                    complete_input_traces, heuristic, false):
            ((prefix_tree[(B, C)], D))
        val g3 = ((cg_merge(g2))(u))(va): D;
        (xa, (t2, g3))
      }
      else (xa, distinguish_from_set[A, B, C,
                                      D](ma, v, t1, g1, cg_lookup, cg_insert,
  dist_fun, u, u, rstates, k, l, complete_input_traces, heuristic, true)))
  }

def state_separator_from_input_choices[A : card_univ : ceq : ccompare : equal : mapping_impl : set_impl,
B : ceq : ccompare : equal : mapping_impl : set_impl,
C : ceq : ccompare : equal : mapping_impl : set_impl](m: fsm[A, B, C],
               cSep: fsm[sum[(A, A), A], B, C], q1: A, q2: A,
               cs: List[(sum[(A, A), A], B)]):
      fsm[sum[(A, A), A], B, C]
  =
  {
    val css = set[(sum[(A, A), A], B)](cs): (set[(sum[(A, A), A], B)])
    val cssQ =
      sup_seta[sum[(A, A),
                    A]](set[sum[(A, A),
                                 A]](mapa[(sum[(A, A), A], B),
   sum[(A, A),
        A]](((a: (sum[(A, A), A], B)) => fst[sum[(A, A), A], B](a)), cs)),
                         insert[sum[(A, A),
                                     A]](Inr[A, (A, A)](q1),
  insert[sum[(A, A), A]](Inr[A, (A, A)](q2), bot_set[sum[(A, A), A]]))):
        (set[sum[(A, A), A]])
    val s0 =
      filter_states[sum[(A, A), A], B,
                     C](cSep,
                         ((q: sum[(A, A), A]) =>
                           member[sum[(A, A), A]](q, cssQ))):
        (fsm[sum[(A, A), A], B, C])
    val s1 =
      filter_transitions[sum[(A, A), A], B,
                          C](s0, ((t: (sum[(A, A), A],
(B, (C, sum[(A, A), A]))))
                                    =>
                                   member[(sum[(A, A), A],
    B)]((fst[sum[(A, A), A], (B, (C, sum[(A, A), A]))](t),
          fst[B, (C, sum[(A, A),
                          A])](snd[sum[(A, A), A],
                                    (B, (C, sum[(A, A), A]))](t))),
         css))):
        (fsm[sum[(A, A), A], B, C]);
    s1
  }

def state_separator_from_s_states[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                                       fsm[A, B, C],
                                      q1: A, q2: A):
      Option[fsm[sum[(A, A), A], B, C]]
  =
  {
    val c =
      canonical_separatorb[A, B, C](m, q1, q2): (fsm[sum[(A, A), A], B, C])
    val cs =
      select_inputs[sum[(A, A), A], B,
                     C](((a: (sum[(A, A), A], B)) =>
                          h[sum[(A, A), A], B, C](c, a)),
                         initial[sum[(A, A), A], B, C](c),
                         inputs_as_list[sum[(A, A), A], B, C](c),
                         remove1[sum[(A, A),
                                      A]](Inl[(A, A), A]((q1, q2)),
   remove1[sum[(A, A),
                A]](Inr[A, (A, A)](q1),
                     remove1[sum[(A, A),
                                  A]](Inr[A, (A, A)](q2),
                                       states_as_list[sum[(A, A), A], B,
               C](c)))),
                         insert[sum[(A, A),
                                     A]](Inr[A, (A, A)](q1),
  insert[sum[(A, A), A]](Inr[A, (A, A)](q2), bot_set[sum[(A, A), A]])),
                         Nil):
        (List[(sum[(A, A), A], B)]);
    (if (equal_nata(size_list[(sum[(A, A), A], B)].apply(cs), zero_nat)) None
      else (if (equal_suma[(A, A),
                            A](fst[sum[(A, A), A],
                                    B](last[(sum[(A, A), A], B)](cs)),
                                Inl[(A, A), A]((q1, q2))))
             Some[fsm[sum[(A, A), A], B,
                       C]](state_separator_from_input_choices[A, B,
                       C](m, c, q1, q2, cs))
             else None))
  }

def select_diverging_ofsm_table_io_with_provided_tables[A : cenum : ceq : ccompare : equal : linorder : set_impl,
                 B : ceq : ccompare : equal : linorder,
                 C : ceq : ccompare : equal : linorder](tables:
                  mapping[nat, mapping[A, set[A]]],
                 m: fsm[A, B, C], q1: A, q2: A, k: nat):
      ((B, C), (Option[A], Option[A]))
  =
  {
    val ins = inputs_as_list[A, B, C](m): (List[B])
    val outs = outputs_as_list[A, B, C](m): (List[C])
    val table =
      ((a: A) =>
        lookup_default[set[A],
                        A](bot_set[A],
                            the[mapping[A,
 set[A]]]((lookupa[nat, mapping[A, set[A]]](tables)).apply(minus_nat(k,
                              one_nata))),
                            a)):
        (A => set[A])
    val f =
      ((a: (B, C)) =>
        {
          val (x, y) = a: ((B, C));
          (((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y),
             (h_obs[A, B, C](m)).apply(q2).apply(x).apply(y))
             match {
             case (None, None) => None
             case (None, Some(q2a)) =>
               Some[((B, C),
                      (Option[A], Option[A]))](((x, y), (None, Some[A](q2a))))
             case (Some(q1a), None) =>
               Some[((B, C),
                      (Option[A], Option[A]))](((x, y), (Some[A](q1a), None)))
             case (Some(q1a), Some(q2a)) =>
               (if (! (set_eq[A](table(q1a), table(q2a))))
                 Some[((B, C),
                        (Option[A],
                          Option[A]))](((x, y), (Some[A](q1a), Some[A](q2a))))
                 else None)
           })
        }):
        (((B, C)) => Option[((B, C), (Option[A], Option[A]))]);
    hd[((B, C),
         (Option[A],
           Option[A]))](map_filter[(B, C),
                                    ((B, C),
                                      (Option[A],
Option[A]))](f, product[B, C](ins, outs)))
  }

def assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables[A : cenum : ceq : ccompare : equal : linorder : set_impl,
                                   B : ceq : ccompare : equal : linorder,
                                   C : ceq : ccompare : equal : linorder](tables:
                                    mapping[nat, mapping[A, set[A]]],
                                   m: fsm[A, B, C], q1: A, q2: A, k: nat):
      List[(B, C)]
  =
  (if (equal_nata(k, zero_nat)) Nil
    else (select_diverging_ofsm_table_io_with_provided_tables[A, B,
                       C](tables, m, q1, q2, Suc(minus_nat(k, one_nata)))
            match {
            case ((x, y), (None, _)) => List((x, y))
            case ((x, y), (Some(_), None)) => List((x, y))
            case ((x, y), (Some(q1a), Some(q2a))) =>
              (x, y) ::
                assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables[A,
       B, C](tables, m, q1a, q2a, minus_nat(k, one_nata))
          }))

def get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
                               B : ceq : ccompare : equal : linorder,
                               C : ceq : ccompare : equal : linorder](tables:
                                mapping[nat, mapping[A, set[A]]],
                               m: fsm[A, B, C], q1: A, q2: A):
      List[(B, C)]
  =
  {
    val a =
      (if (member[A](q1, states[A, B, C](m)) &&
             (member[A](q2, states[A, B, C](m)) &&
               ! (set_eq[A](lookup_default[set[A],
    A](bot_set[A],
        the[mapping[A, set[A]]]((lookupa[nat,
  mapping[A, set[A]]](tables)).apply(minus_nat(size[A, B, C](m), one_nata))),
        q1),
                             lookup_default[set[A],
     A](bot_set[A],
         the[mapping[A, set[A]]]((lookupa[nat,
   mapping[A, set[A]]](tables)).apply(minus_nat(size[A, B, C](m), one_nata))),
         q2)))))
        the[nat](find_index[nat](((i: nat) =>
                                   ! (set_eq[A](lookup_default[set[A],
                        A](bot_set[A],
                            the[mapping[A,
 set[A]]]((lookupa[nat, mapping[A, set[A]]](tables)).apply(i)),
                            q1),
         lookup_default[set[A],
                         A](bot_set[A],
                             the[mapping[A,
  set[A]]]((lookupa[nat, mapping[A, set[A]]](tables)).apply(i)),
                             q2)))),
                                  upt(zero_nat, size[A, B, C](m))))
        else zero_nat):
        nat;
    assemble_distinguishing_sequence_from_ofsm_table_with_provided_tables[A, B,
                                   C](tables, m, q1, q2, a)
  }

def sort_unverified_transitions_by_state_cover_length[A : card_univ : ceq : ccompare : equal : linorder,
               B : equal : linorder,
               C : equal : linorder](m: fsm[A, B, C], v: A => List[(B, C)],
                                      ts: List[(A, (B, (C, A)))]):
      List[(A, (B, (C, A)))]
  =
  {
    val default_weight =
      times_nata(nat_of_integer(BigInt(2)), size[A, B, C](m)): nat
    val weights =
      ((a: (A, (B, (C, A)))) =>
        map_of[(A, (B, (C, A))),
                nat](mapa[(A, (B, (C, A))),
                           ((A, (B, (C, A))),
                             nat)](((t: (A, (B, (C, A)))) =>
                                     (t, plus_nat(size_list[(B,
                      C)].apply(v(fst[A, (B, (C, A))](t))),
           size_list[(B, C)].apply(v(snd[C,
  A](snd[B, (C, A)](snd[A, (B, (C, A))](t)))))))),
                                    ts),
                      a)):
        (((A, (B, (C, A)))) => Option[nat])
    val weight =
      ((q: (A, (B, (C, A)))) => (weights(q) match {
                                   case None => default_weight
                                   case Some(w) => w
                                 })):
        (((A, (B, (C, A)))) => nat);
    mergesort_by_rel[(A, (B, (C, A)))](((t1: (A, (B, (C, A)))) =>
 (t2: (A, (B, (C, A)))) => less_eq_nat(weight(t1), weight(t2))),
ts)
  }

def handleut_dynamic_with_precomputed_state_cover[A : ceq : ccompare : equal : mapping_impl : linorder,
           B : ceq : ccompare : equal : mapping_impl : linorder,
           C : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
           D](rstates: List[List[(A, B)]], complete_input_traces: Boolean,
               use_input_heuristic: Boolean, dist_fun: C => C => List[(A, B)],
               do_establish_convergence:
                 (fsm[C, A, B]) =>
                   (C => List[(A, B)]) =>
                     ((C, (A, (B, C)))) =>
                       (List[(C, (A, (B, C)))]) => nat => Boolean,
               ma: fsm[C, A, B], v: C => List[(A, B)], ta: prefix_tree[(A, B)],
               g: D, cg_insert: D => (List[(A, B)]) => D,
               cg_lookup: D => (List[(A, B)]) => List[List[(A, B)]],
               cg_merge: D => (List[(A, B)]) => (List[(A, B)]) => D, m: nat,
               t: (C, (A, (B, C))), x: List[(C, (A, (B, C)))]):
      (List[(C, (A, (B, C)))], (prefix_tree[(A, B)], D))
  =
  {
    val k = times_nata(nat_of_integer(BigInt(2)), size[C, A, B](ma)): nat
    val l = minus_nat(m, card[C](reachable_states[C, A, B](ma))): nat
    val heuristic =
      (if (use_input_heuristic)
        ((a: prefix_tree[(A, B)]) => (b: List[(A, B)]) =>
          (c: (List[(A, B)], int)) => (d: List[(A, B)]) =>
          append_heuristic_input[C, A, B](ma, a, b, c, d))
        else ((a: prefix_tree[(A, B)]) => (b: List[(A, B)]) =>
               (c: (List[(A, B)], int)) => (d: List[(A, B)]) =>
               append_heuristic_io[A, B](a, b, c, d))):
        ((prefix_tree[(A, B)]) =>
          (List[(A, B)]) =>
            ((List[(A, B)], int)) => (List[(A, B)]) => (List[(A, B)], int))
    val (t1, g1) =
      handle_io_pair[C, A, B,
                      D](complete_input_traces, use_input_heuristic, ma, v, ta,
                          g, cg_insert, cg_lookup, fst[C, (A, (B, C))](t),
                          fst[A, (B, C)](snd[C, (A, (B, C))](t)),
                          fst[B, C](snd[A, (B, C)](snd[C, (A, (B, C))](t)))):
        ((prefix_tree[(A, B)], D))
    val u =
      v(fst[C, (A, (B, C))](t)) ++
        List((fst[A, (B, C)](snd[C, (A, (B, C))](t)),
               fst[B, C](snd[A, (B, C)](snd[C, (A, (B, C))](t))))):
        (List[(A, B)])
    val va =
      v(snd[B, C](snd[A, (B, C)](snd[C, (A, (B, C))](t)))): (List[(A, B)])
    val xa = butlast[(C, (A, (B, C)))](x): (List[(C, (A, (B, C)))]);
    (if (((((do_establish_convergence(ma))(v))(t))(xa))(l))
      {
        val (t2, g2) =
          distinguish_from_set[C, A, B,
                                D](ma, v, t1, g1, cg_lookup, cg_insert,
                                    dist_fun, u, va, rstates, k, l,
                                    complete_input_traces, heuristic, false):
            ((prefix_tree[(A, B)], D))
        val g3 = ((cg_merge(g2))(u))(va): D;
        (xa, (t2, g3))
      }
      else (xa, distinguish_from_set[C, A, B,
                                      D](ma, v, t1, g1, cg_lookup, cg_insert,
  dist_fun, u, u, rstates, k, l, complete_input_traces, heuristic, true)))
  }

def handle_state_cover_dynamic[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
                                B : ceq : ccompare : equal : mapping_impl : linorder,
                                C : ceq : ccompare : equal : mapping_impl : linorder,
                                D](completeInputTraces: Boolean,
                                    useInputHeuristic: Boolean,
                                    get_distinguishing_trace:
                                      A => A => List[(B, C)],
                                    m: fsm[A, B, C], v: A => List[(B, C)],
                                    cg_initial:
                                      (fsm[A, B, C]) =>
(prefix_tree[(B, C)]) => D,
                                    cg_insert: D => (List[(B, C)]) => D,
                                    cg_lookup:
                                      D =>
(List[(B, C)]) => List[List[(B, C)]]):
      (prefix_tree[(B, C)], D)
  =
  {
    val k = times_nata(nat_of_integer(BigInt(2)), size[A, B, C](m)): nat
    val heuristic =
      (if (useInputHeuristic)
        ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
          (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
          append_heuristic_input[A, B, C](m, a, b, c, d))
        else ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
               (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
               append_heuristic_io[B, C](a, b, c, d))):
        ((prefix_tree[(B, C)]) =>
          (List[(B, C)]) =>
            ((List[(B, C)], int)) => (List[(B, C)]) => (List[(B, C)], int))
    val rstates = reachable_states_as_list[A, B, C](m): (List[A])
    val t0 =
      from_list[(B, C)](mapa[A, List[(B, C)]](v, rstates)):
        (prefix_tree[(B, C)])
    val t0a =
      (if (completeInputTraces)
        combinea[(B, C)](t0, from_list[(B,
 C)](maps[A, List[(B, C)]](((q: A) =>
                             language_for_input[A, B,
         C](m, initial[A, B, C](m),
             mapa[(B, C), B](((a: (B, C)) => fst[B, C](a)), v(q)))),
                            rstates)))
        else t0):
        (prefix_tree[(B, C)])
    val g0 = (cg_initial(m))(t0a): D
    val separate_state =
      ((a: (List[List[(B, C)]], (prefix_tree[(B, C)], D))) =>
        {
          val (x, (t, g)) = a: ((List[List[(B, C)]], (prefix_tree[(B, C)], D)));
          ((q: A) =>
            {
              val u = v(q): (List[(B, C)])
              val tg =
                spyh_distinguish[A, B, C,
                                  D](m, t, g, cg_lookup, cg_insert,
                                      get_distinguishing_trace, u, x, k,
                                      completeInputTraces, heuristic):
                  ((prefix_tree[(B, C)], D))
              val xa = u :: x: (List[List[(B, C)]]);
              (xa, tg)
            })
        }):
        (((List[List[(B, C)]], (prefix_tree[(B, C)], D))) =>
          A => (List[List[(B, C)]], (prefix_tree[(B, C)], D)));
    snd[List[List[(B, C)]],
         (prefix_tree[(B, C)],
           D)](foldl[(List[List[(B, C)]], (prefix_tree[(B, C)], D)),
                      A](separate_state, (Nil, (t0a, g0)), rstates))
  }

def h_framework_dynamic_with_empty_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
  B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
  C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](convergence_decisision:
                   (fsm[A, B, C]) =>
                     (A => List[(B, C)]) =>
                       ((A, (B, (C, A)))) =>
                         (List[(A, (B, (C, A)))]) => nat => Boolean,
                  m1: fsm[A, B, C], m: nat, completeInputTraces: Boolean,
                  useInputHeuristic: Boolean):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](m1, minus_nat(size[A, B, C](m1), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, m1, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](m1),
                                   states_as_list[A, B, C](m1))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](m1)) &&
               (member[A](q2, states[A, B, C](m1)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](m1, q1, q2))):
        (A => A => List[(B, C)])
    val v = get_state_cover_assignment[A, B, C](m1): (A => List[(B, C)])
    val rstates =
      mapa[A, List[(B, C)]](v, reachable_states_as_list[A, B, C](m1)):
        (List[List[(B, C)]]);
    h_framework[A, B, C,
                 Unit](m1, ((_: fsm[A, B, C]) => v),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => Unit)
                            =>
                          (d: Unit => (List[(B, C)]) => Unit) =>
                          (e: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          handle_state_cover_dynamic[A, B, C,
              Unit](completeInputTraces, useInputHeuristic, distHelper, a, b, c,
                     d, e)),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: List[(A, (B, (C, A)))]) =>
                          sort_unverified_transitions_by_state_cover_length[A,
                                     B, C](a, b, c)),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: prefix_tree[(B, C)]) => (d: Unit) =>
                          (e: Unit => (List[(B, C)]) => Unit) =>
                          (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          (g: Unit => (List[(B, C)]) => (List[(B, C)]) => Unit)
                            =>
                          (h: nat) => (i: (A, (B, (C, A)))) =>
                          (j: List[(A, (B, (C, A)))]) =>
                          handleut_dynamic_with_precomputed_state_cover[B, C, A,
                                 Unit](rstates, completeInputTraces,
useInputHeuristic, distHelper, convergence_decisision, a, b, c, d, e, f, g, h,
i, j)),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: prefix_tree[(B, C)]) => (d: Unit) =>
                          (e: Unit => (List[(B, C)]) => Unit) =>
                          (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          (g: A) => (h: B) => (i: C) =>
                          handle_io_pair[A, B, C,
  Unit](completeInputTraces, useInputHeuristic, a, b, c, d, e, f, g, h, i)),
                        ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
                          empty_cg_initial[A, B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) =>
                          empty_cg_insert[B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) =>
                          empty_cg_lookup[B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
                          empty_cg_merge[B, C](a, b, c)),
                        m)
  }

def h_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              C : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl]:
      (fsm[A, B, C]) => nat => Boolean => Boolean => prefix_tree[(B, C)]
  =
  ((a: fsm[A, B, C]) => (b: nat) => (c: Boolean) => (d: Boolean) =>
    h_framework_dynamic_with_empty_graph[A, B,
  C](((_: fsm[A, B, C]) => (_: A => List[(B, C)]) => (_: (A, (B, (C, A)))) =>
       (_: List[(A, (B, (C, A)))]) => (_: nat) => false),
      a, b, c, d))

def distance_at_most[A : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                      B : ceq : ccompare : equal : mapping_impl : linorder,
                      C : ceq : ccompare : linorder : set_impl](m: fsm[A, B, C],
                         q1: A, q2: A, k: nat):
      Boolean
  =
  (if (equal_nata(k, zero_nat)) eq[A](q1, q2)
    else eq[A](q1, q2) ||
           Bex[B](inputs[A, B, C](m),
                   ((x: B) =>
                     Bex[(C, A)](h[A, B, C](m, (q1, x)),
                                  ((a: (C, A)) =>
                                    {
                                      val (_, q1a) = a: ((C, A));
                                      distance_at_most[A, B,
                C](m, q1a, q2, minus_nat(k, one_nata))
                                    })))))

def r_distinguishable_state_pairs_with_separators[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
           B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
           C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
               fsm[A, B, C]):
      set[((A, A), fsm[sum[(A, A), A], B, C])]
  =
  set[((A, A),
        fsm[sum[(A, A), A], B,
             C])](concat[((A, A),
                           fsm[sum[(A, A), A], B,
                                C])](map_filter[((A, A),
          Option[fsm[sum[(A, A), A], B, C]]),
         List[((A, A),
                fsm[sum[(A, A), A], B,
                     C])]](((x: ((A, A), Option[fsm[sum[(A, A), A], B, C]])) =>
                             (if ({
                                    val (_, a) =
                                      x:
(((A, A), Option[fsm[sum[(A, A), A], B, C]]));
                                    ! (is_none[fsm[sum[(A, A), A], B, C]](a))
                                  })
                               Some[List[((A, A),
   fsm[sum[(A, A), A], B,
        C])]]({
                val (a, b) = x: (((A, A), Option[fsm[sum[(A, A), A], B, C]]));
                ({
                   val (q1, q2) = a: ((A, A));
                   ((aa: Option[fsm[sum[(A, A), A], B, C]]) =>
                     List(((q1, q2), the[fsm[sum[(A, A), A], B, C]](aa)),
                           ((q2, q1), the[fsm[sum[(A, A), A], B, C]](aa))))
                 })(b)
              })
                               else None)),
                            map_filter[(A, A),
((A, A),
  Option[fsm[sum[(A, A), A], B,
              C]])](((x: (A, A)) =>
                      (if ({
                             val (a, b) = x: ((A, A));
                             less[A](a, b)
                           })
                        Some[((A, A),
                               Option[fsm[sum[(A, A), A], B,
   C]])]({
           val (q1, q2) = x: ((A, A));
           ((q1, q2), state_separator_from_s_states[A, B, C](m, q1, q2))
         })
                        else None)),
                     product[A, A](states_as_list[A, B, C](m),
                                    states_as_list[A, B, C](m))))))

def pairwise_r_distinguishable_state_sets_from_separators_list[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                        B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                        C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                            fsm[A, B, C]):
      List[set[A]]
  =
  {
    val rds =
      image[((A, A), fsm[sum[(A, A), A], B, C]),
             (A, A)](((a: ((A, A), fsm[sum[(A, A), A], B, C])) =>
                       fst[(A, A), fsm[sum[(A, A), A], B, C]](a)),
                      r_distinguishable_state_pairs_with_separators[A, B,
                             C](m)):
        (set[(A, A)]);
    filtera[set[A]](((s: set[A]) =>
                      Ball[A](s, ((q1: A) =>
                                   Ball[A](s,
    ((q2: A) =>
      (if (! (eq[A](q1, q2))) member[(A, A)]((q1, q2), rds) else true)))))),
                     mapa[List[A],
                           set[A]](((a: List[A]) => set[A](a)),
                                    pow_list[A](states_as_list[A, B, C](m))))
  }

def maximal_pairwise_r_distinguishable_state_sets_from_separators_list[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                                    fsm[A, B, C]):
      List[set[A]]
  =
  remove_subsets[A](pairwise_r_distinguishable_state_sets_from_separators_list[A,
B, C](m))

def calculate_state_preamble_from_input_choices[A : card_univ : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
         B : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
         C : card_univ : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
     fsm[A, B, C],
    q: A):
      Option[fsm[A, B, C]]
  =
  (if (eq[A](q, initial[A, B, C](m)))
    Some[fsm[A, B, C]](initial_preamble[A, B, C](m))
    else {
           val ds = d_states[A, B, C](m, q): (List[(A, B)])
           val dss = set[(A, B)](ds): (set[(A, B)]);
           (ds match {
              case Nil => None
              case _ :: _ =>
                (if (eq[A](fst[A, B](last[(A, B)](ds)), initial[A, B, C](m)))
                  Some[fsm[A, B,
                            C]](filter_transitions[A, B,
            C](m, ((t: (A, (B, (C, A)))) =>
                    member[(A, B)]((fst[A, (B, (C, A))](t),
                                     fst[B, (C, A)](snd[A, (B, (C, A))](t))),
                                    dss))))
                  else None)
            })
         })

def d_reachable_states_with_preambles[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                       B : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                       C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
   fsm[A, B, C]):
      set[(A, fsm[A, B, C])]
  =
  image[(A, Option[fsm[A, B, C]]),
         (A, fsm[A, B,
                  C])](((qp: (A, Option[fsm[A, B, C]])) =>
                         (fst[A, Option[fsm[A, B, C]]](qp),
                           the[fsm[A, B,
                                    C]](snd[A, Option[fsm[A, B, C]]](qp)))),
                        filter[(A, Option[fsm[A, B,
       C]])](((qp: (A, Option[fsm[A, B, C]])) =>
               ! (is_none[fsm[A, B, C]](snd[A, Option[fsm[A, B, C]]](qp)))),
              image[A, (A, Option[fsm[A, B,
                                       C]])](((q: A) =>
       (q, calculate_state_preamble_from_input_choices[A, B, C](m, q))),
      states[A, B, C](m))))

def maximal_repetition_sets_from_separators_list_naive[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                    fsm[A, B, C]):
      List[(set[A], set[A])]
  =
  {
    val dr =
      image[(A, fsm[A, B, C]),
             A](((a: (A, fsm[A, B, C])) => fst[A, fsm[A, B, C]](a)),
                 d_reachable_states_with_preambles[A, B, C](m)):
        (set[A]);
    mapa[set[A],
          (set[A],
            set[A])](((s: set[A]) => (s, inf_seta[A](s, dr))),
                      maximal_pairwise_r_distinguishable_state_sets_from_separators_list[A,
          B, C](m))
  }

def calculate_test_suite_for_repetition_sets[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
      B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
      C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](ma:
          fsm[A, B, C],
         m: nat, repetition_sets: List[(set[A], set[A])]):
      test_suite[A, B, C, sum[(A, A), A]]
  =
  {
    val states_with_preambles =
      d_reachable_states_with_preambles[A, B, C](ma): (set[(A, fsm[A, B, C])])
    val pairs_with_separators =
      image[((A, A), fsm[sum[(A, A), A], B, C]),
             ((A, A),
               (fsm[sum[(A, A), A], B, C],
                 (sum[(A, A), A],
                   sum[(A, A),
                        A])))](((a: ((A, A), fsm[sum[(A, A), A], B, C])) =>
                                 {
                                   val (aa, b) =
                                     a: (((A, A), fsm[sum[(A, A), A], B, C]));
                                   ({
                                      val (q1, q2) = aa: ((A, A));
                                      ((ab: fsm[sum[(A, A), A], B, C]) =>
((q1, q2), (ab, (Inr[A, (A, A)](q1), Inr[A, (A, A)](q2)))))
                                    })(b)
                                 }),
                                r_distinguishable_state_pairs_with_separators[A,
                                       B, C](ma)):
        (set[((A, A),
               (fsm[sum[(A, A), A], B, C], (sum[(A, A), A], sum[(A, A), A])))]);
    combine_test_suite[A, B, C,
                        sum[(A, A),
                             A]](ma, m, states_with_preambles,
                                  pairs_with_separators, repetition_sets)
  }

def calculate_test_suite_naive[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](ma:
                                    fsm[A, B, C],
                                   m: nat):
      test_suite[A, B, C, sum[(A, A), A]]
  =
  calculate_test_suite_for_repetition_sets[A, B,
    C](ma, m, maximal_repetition_sets_from_separators_list_naive[A, B, C](ma))

def handle_state_cover_static[A : card_univ : ceq : ccompare : equal : linorder : set_impl,
                               B : ceq : ccompare : equal : mapping_impl : linorder,
                               C : ceq : ccompare : equal : mapping_impl : linorder,
                               D](dist_set: nat => A => prefix_tree[(B, C)],
                                   m: fsm[A, B, C], v: A => List[(B, C)],
                                   cg_initial:
                                     (fsm[A, B, C]) =>
                                       (prefix_tree[(B, C)]) => D,
                                   cg_insert: D => (List[(B, C)]) => D,
                                   cg_lookup:
                                     D => (List[(B, C)]) => List[List[(B, C)]]):
      (prefix_tree[(B, C)], D)
  =
  {
    val separate_state =
      ((t: prefix_tree[(B, C)]) => (q: A) =>
        combine_after[(B, C)](t, v(q), (dist_set(zero_nat))(q))):
        ((prefix_tree[(B, C)]) => A => prefix_tree[(B, C)])
    val t =
      foldl[prefix_tree[(B, C)],
             A](separate_state, emptyc[(B, C)],
                 reachable_states_as_list[A, B, C](m)):
        (prefix_tree[(B, C)])
    val a = (cg_initial(m))(t): D;
    (t, a)
  }

def h_framework_static_with_empty_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
 B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
 C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m1:
                fsm[A, B, C],
               dist_fun: nat => A => prefix_tree[(B, C)], m: nat):
      prefix_tree[(B, C)]
  =
  h_framework[A, B, C,
               Unit](m1, ((a: fsm[A, B, C]) =>
                           get_state_cover_assignment[A, B, C](a)),
                      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                        (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => Unit) =>
                        (d: Unit => (List[(B, C)]) => Unit) =>
                        (e: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                        handle_state_cover_static[A, B, C,
           Unit](dist_fun, a, b, c, d, e)),
                      ((_: fsm[A, B, C]) => (_: A => List[(B, C)]) =>
                        (ts: List[(A, (B, (C, A)))]) => ts),
                      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                        (c: prefix_tree[(B, C)]) => (d: Unit) =>
                        (e: Unit => (List[(B, C)]) => Unit) =>
                        (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                        (g: Unit => (List[(B, C)]) => (List[(B, C)]) => Unit) =>
                        (h: nat) => (i: (A, (B, (C, A)))) =>
                        (j: List[(A, (B, (C, A)))]) =>
                        handleut_static[A, B, C,
 Unit](dist_fun, a, b, c, d, e, f, g, h, i, j)),
                      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                        (c: prefix_tree[(B, C)]) => (d: Unit) =>
                        (e: Unit => (List[(B, C)]) => Unit) =>
                        (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                        (g: A) => (h: B) => (i: C) =>
                        handle_io_pair[A, B, C,
Unit](false, false, a, b, c, d, e, f, g, h, i)),
                      ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
                        empty_cg_initial[A, B, C](a, b)),
                      ((a: Unit) => (b: List[(B, C)]) =>
                        empty_cg_insert[B, C](a, b)),
                      ((a: Unit) => (b: List[(B, C)]) =>
                        empty_cg_lookup[B, C](a, b)),
                      ((a: Unit) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
                        empty_cg_merge[B, C](a, b, c)),
                      m)

def w_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
     fsm[A, B, C],
    m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val distSet =
      from_list[(B, C)](mapa[(A, A),
                              List[(B, C)]](((a: (A, A)) =>
      {
        val (aa, b) = a: ((A, A));
        (distHelper(aa))(b)
      }),
     pairs)):
        (prefix_tree[(B, C)])
    val distFun =
      ((_: nat) => (_: A) => distSet): (nat => A => prefix_tree[(B, C)]);
    h_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def greedy_pairwise_r_distinguishable_state_sets_from_separators[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                          B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                          C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                              fsm[A, B, C]):
      List[set[A]]
  =
  {
    val pwrds =
      image[((A, A), fsm[sum[(A, A), A], B, C]),
             (A, A)](((a: ((A, A), fsm[sum[(A, A), A], B, C])) =>
                       fst[(A, A), fsm[sum[(A, A), A], B, C]](a)),
                      r_distinguishable_state_pairs_with_separators[A, B,
                             C](m)):
        (set[(A, A)])
    val k = size[A, B, C](m): nat
    val nL = states_as_list[A, B, C](m): (List[A]);
    mapa[A, set[A]](((q: A) =>
                      set[A](extend_until_conflict[A](pwrds, remove1[A](q, nL),
               List(q), k))),
                     nL)
  }

def maximal_repetition_sets_from_separators_list_greedy[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                 B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                 C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](m:
                     fsm[A, B, C]):
      List[(set[A], set[A])]
  =
  {
    val dr =
      image[(A, fsm[A, B, C]),
             A](((a: (A, fsm[A, B, C])) => fst[A, fsm[A, B, C]](a)),
                 d_reachable_states_with_preambles[A, B, C](m)):
        (set[A]);
    remdups[(set[A],
              set[A])](mapa[set[A],
                             (set[A],
                               set[A])](((s: set[A]) =>
  (s, inf_seta[A](s, dr))),
 greedy_pairwise_r_distinguishable_state_sets_from_separators[A, B, C](m)))
  }

def calculate_test_suite_greedy[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 B : finite_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](ma:
                                     fsm[A, B, C],
                                    m: nat):
      test_suite[A, B, C, sum[(A, A), A]]
  =
  calculate_test_suite_for_repetition_sets[A, B,
    C](ma, m, maximal_repetition_sets_from_separators_list_greedy[A, B, C](ma))

def test_suite_from_io_tree[A : ccompare : equal,
                             B : ccompare : equal : mapping_impl,
                             C : ccompare : equal : mapping_impl](ma:
                            fsm[A, B, C],
                           q: A, x2: prefix_tree[(B, C)]):
      prefix_tree[((B, C), Boolean)]
  =
  (ma, q, x2) match {
  case (ma, q, MPT(Rbt_mapping(m))) =>
    (ccompare_proda[B, C] match {
       case None =>
         { sys.error("test_suite_from_io_tree RBT_set: ccompare = None");
           (((_: Unit) =>
              test_suite_from_io_tree[A, B,
                                       C](ma, q,
   MPT[(B, C)](Rbt_mapping[(B, C), prefix_tree[(B, C)]](m))))).apply(())
           }
       case Some(_) =>
         MPT[((B, C),
               Boolean)](tabulate[((B, C), Boolean),
                                   prefix_tree[((B, C),
         Boolean)]](mapa[((B, C), prefix_tree[(B, C)]),
                          ((B, C),
                            Boolean)](((a: ((B, C), prefix_tree[(B, C)])) =>
{
  val (aa, b) = a: (((B, C), prefix_tree[(B, C)]));
  ({
     val (x, y) = aa: ((B, C));
     ((_: prefix_tree[(B, C)]) =>
       ((x, y),
         ! (is_none[A]((h_obs[A, B, C](ma)).apply(q).apply(x).apply(y)))))
   })(b)
}),
                                       entriesa[(B, C),
         prefix_tree[(B, C)]](m)),
                     ((a: ((B, C), Boolean)) =>
                       {
                         val (aa, b) = a: (((B, C), Boolean));
                         ({
                            val (x, y) = aa: ((B, C));
                            ((_: Boolean) =>
                              ((h_obs[A, B, C](ma)).apply(q).apply(x).apply(y)
                                 match {
                                 case None => emptyc[((B, C), Boolean)]
                                 case Some(qa) =>
                                   test_suite_from_io_tree[A, B,
                    C](ma, qa,
                        {
                          val (Some(t)) =
                            (lookupb[(B, C),
                                      prefix_tree[(B, C)]](m)).apply((x, y)):
                              Option[prefix_tree[(B, C)]];
                          t
                        })
                               }))
                          })(b)
                       })))
     })
}

def get_initial_test_suite_h[A : card_univ : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                              B : ceq : ccompare : equal : mapping_impl : linorder,
                              C : ceq : ccompare : equal : mapping_impl : linorder : set_impl](v:
                 A => List[(B, C)],
                ma: fsm[A, B, C], m: nat):
      prefix_tree[(B, C)]
  =
  {
    val rstates = reachable_states_as_list[A, B, C](ma): (List[A])
    val n = card[A](reachable_states[A, B, C](ma)): nat
    val iM = inputs_as_list[A, B, C](ma): (List[B])
    val hMap =
      ((a: (A, B)) =>
        map_of[(A, B),
                List[(A, (B, (C, A)))]](mapa[(A, B),
      ((A, B),
        List[(A, (B, (C, A)))])](((aa: (A, B)) =>
                                   {
                                     val (q, x) = aa: ((A, B));
                                     ((q, x),
                                       mapa[(C, A),
     (A, (B, (C, A)))](((ab: (C, A)) => {
  val (y, qa) = ab: ((C, A));
  (q, (x, (y, qa)))
}),
                        sorted_list_of_set[(C, A)](h[A, B, C](ma, (q, x)))))
                                   }),
                                  product[A,
   B](states_as_list[A, B, C](ma), iM)),
 a)):
        (((A, B)) => Option[List[(A, (B, (C, A)))]]);
    ((q: A) => (x: B) => (hMap((q, x)) match {
                            case None => Nil
                            case Some(ts) => ts
                          }))
    val t =
      from_list[(B, C)](maps[A, List[(B, C)]](((q: A) =>
        mapa[List[(B, C)],
              List[(B, C)]](((a: List[(B, C)]) => v(q) ++ a),
                             h_extensions[A, B, C](ma, q, minus_nat(m, n)))),
       rstates)):
        (prefix_tree[(B, C)]);
    t
  }

def pair_framework_h_components[A : card_univ : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                 B : ceq : ccompare : equal : mapping_impl : linorder,
                                 C : ceq : ccompare : equal : mapping_impl : linorder : set_impl](ma:
                    fsm[A, B, C],
                   m: nat,
                   get_separating_traces:
                     (fsm[A, B, C]) =>
                       (((List[(B, C)], A), (List[(B, C)], A))) =>
                         (prefix_tree[(B, C)]) => prefix_tree[(B, C)]):
      prefix_tree[(B, C)]
  =
  {
    val v = get_state_cover_assignment[A, B, C](ma): (A => List[(B, C)]);
    pair_framework[A, B,
                    C](ma, m,
                        ((a: fsm[A, B, C]) => (b: nat) =>
                          get_initial_test_suite_h[A, B, C](v, a, b)),
                        ((a: fsm[A, B, C]) => (b: nat) =>
                          get_pairs_h[A, B, C](v, a, b)),
                        get_separating_traces)
  }

def simple_cg_lookup_with_conv[A : ceq : ccompare : equal : linorder](g:
                                List[fset[List[A]]],
                               ys: List[A]):
      List[List[A]]
  =
  {
    val lookup_for_prefix =
      ((i: nat) =>
        {
          val pref = take[A](i, ys): (List[A])
          val suff = drop[A](i, ys): (List[A])
          val a =
            foldl[fset[List[A]],
                   fset[List[A]]](((a: fset[List[A]]) => (b: fset[List[A]]) =>
                                    sup_fset[List[A]](a, b)),
                                   bot_fset[List[A]],
                                   filtera[fset[List[A]]](((x: fset[List[A]]) =>
                    member[List[A]](pref, fset[List[A]](x))),
                   g)):
              (fset[List[A]]);
          fimage[List[A], List[A]](((prefa: List[A]) => prefa ++ suff), a)
        }):
        (nat => fset[List[A]]);
    sorted_list_of_fset[List[A]](finsert[List[A]](ys,
           foldl[fset[List[A]],
                  nat](((cs: fset[List[A]]) => (i: nat) =>
                         sup_fset[List[A]](lookup_for_prefix(i), cs)),
                        bot_fset[List[A]],
                        upt(zero_nat, Suc(size_list[A].apply(ys))))))
  }

def contains_distinguishing_trace[A : ccompare : equal,
                                   B : cenum : ceq : ccompare : equal : set_impl,
                                   C : cenum : ceq : ccompare : equal : set_impl](m:
    fsm[A, B, C],
   x1: prefix_tree[(B, C)], q1: A, q2: A):
      Boolean
  =
  (m, x1, q1, q2) match {
  case (m, MPT(t1), q1, q2) =>
    Bex[(B, C)](keys[(B, C), prefix_tree[(B, C)]](t1),
                 ((a: (B, C)) =>
                   {
                     val (x, y) = a: ((B, C));
                     ((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y) match {
                        case None =>
                          ! (is_none[A]((h_obs[A, B,
        C](m)).apply(q2).apply(x).apply(y)))
                        case Some(q1a) =>
                          ((h_obs[A, B, C](m)).apply(q2).apply(x).apply(y) match
                             {
                             case None => true
                             case Some(aa) =>
                               contains_distinguishing_trace[A, B,
                      C](m, the[prefix_tree[(B,
      C)]]((lookupa[(B, C), prefix_tree[(B, C)]](t1)).apply((x, y))),
                          q1a, aa)
                           })
                      })
                   }))
}

def w_method_via_h_framework_2[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
       fsm[A, B, C],
      m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val handlePair =
      ((w: prefix_tree[(B, C)]) => (a: (A, A)) =>
        {
          val (q, qa) = a: ((A, A));
          (if (contains_distinguishing_trace[A, B, C](ma, w, q, qa)) w
            else insertb[(B, C)](w, (distHelper(q))(qa)))
        }):
        ((prefix_tree[(B, C)]) => ((A, A)) => prefix_tree[(B, C)])
    val distSet =
      foldl[prefix_tree[(B, C)], (A, A)](handlePair, emptyc[(B, C)], pairs):
        (prefix_tree[(B, C)])
    val distFun =
      ((_: nat) => (_: A) => distSet): (nat => A => prefix_tree[(B, C)]);
    h_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def spy_framework_static_with_empty_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
   B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m1:
                  fsm[A, B, C],
                 dist_fun: nat => A => prefix_tree[(B, C)], m: nat):
      prefix_tree[(B, C)]
  =
  spy_framework[A, B, C,
                 Unit](m1, ((a: fsm[A, B, C]) =>
                             get_state_cover_assignment[A, B, C](a)),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => Unit)
                            =>
                          (d: Unit => (List[(B, C)]) => Unit) =>
                          (e: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          handle_state_cover_static[A, B, C,
             Unit](dist_fun, a, b, c, d, e)),
                        ((_: fsm[A, B, C]) => (_: A => List[(B, C)]) =>
                          (ts: List[(A, (B, (C, A)))]) => ts),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: prefix_tree[(B, C)]) => (d: Unit) =>
                          (e: Unit => (List[(B, C)]) => Unit) =>
                          (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          (g: nat) => (h: (A, (B, (C, A)))) =>
                          establish_convergence_static[A, B, C,
                Unit](dist_fun, a, b, c, d, e, f, g, h)),
                        ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
                          (c: prefix_tree[(B, C)]) => (d: Unit) =>
                          (e: Unit => (List[(B, C)]) => Unit) =>
                          (f: Unit => (List[(B, C)]) => List[List[(B, C)]]) =>
                          (g: A) => (h: B) => (i: C) =>
                          handle_io_pair[A, B, C,
  Unit](false, true, a, b, c, d, e, f, g, h, i)),
                        ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
                          empty_cg_initial[A, B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) =>
                          empty_cg_insert[B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) =>
                          empty_cg_lookup[B, C](a, b)),
                        ((a: Unit) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
                          empty_cg_merge[B, C](a, b, c)),
                        m)

def w_method_via_spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
       fsm[A, B, C],
      m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val distSet =
      from_list[(B, C)](mapa[(A, A),
                              List[(B, C)]](((a: (A, A)) =>
      {
        val (aa, b) = a: ((A, A));
        (distHelper(aa))(b)
      }),
     pairs)):
        (prefix_tree[(B, C)])
    val distFun =
      ((_: nat) => (_: A) => distSet): (nat => A => prefix_tree[(B, C)]);
    spy_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def wp_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                               B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                               C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
      fsm[A, B, C],
     m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val distSet =
      from_list[(B, C)](mapa[(A, A),
                              List[(B, C)]](((a: (A, A)) =>
      {
        val (aa, b) = a: ((A, A));
        (distHelper(aa))(b)
      }),
     pairs)):
        (prefix_tree[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val l = Suc(minus_nat(m, card[A](reachable_states[A, B, C](ma)))): nat
    val distFun =
      ((k: nat) => (q: A) =>
        (if (equal_nata(k, l))
          (if (member[A](q, states[A, B, C](ma)))
            the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))
          else distSet)):
        (nat => A => prefix_tree[(B, C)]);
    h_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def intersection_is_distinguishing[A : ccompare : equal,
                                    B : cenum : ceq : ccompare : equal : set_impl,
                                    C : cenum : ceq : ccompare : equal : set_impl](m:
     fsm[A, B, C],
    x1: prefix_tree[(B, C)], q1: A, x3: prefix_tree[(B, C)], q2: A):
      Boolean
  =
  (m, x1, q1, x3, q2) match {
  case (m, MPT(t1), q1, MPT(t2), q2) =>
    Bex[(B, C)](inf_seta[(B, C)](keys[(B, C), prefix_tree[(B, C)]](t1),
                                  keys[(B, C), prefix_tree[(B, C)]](t2)),
                 ((a: (B, C)) =>
                   {
                     val (x, y) = a: ((B, C));
                     ((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y) match {
                        case None =>
                          ! (is_none[A]((h_obs[A, B,
        C](m)).apply(q2).apply(x).apply(y)))
                        case Some(q1a) =>
                          ((h_obs[A, B, C](m)).apply(q2).apply(x).apply(y) match
                             {
                             case None => true
                             case Some(aa) =>
                               intersection_is_distinguishing[A, B,
                       C](m, the[prefix_tree[(B,
       C)]]((lookupa[(B, C), prefix_tree[(B, C)]](t1)).apply((x, y))),
                           q1a,
                           the[prefix_tree[(B,
     C)]]((lookupa[(B, C), prefix_tree[(B, C)]](t2)).apply((x, y))),
                           aa)
                           })
                      })
                   }))
}

def add_distinguishing_sequence_if_required[A : ccompare : equal,
     B : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
     C : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl](dist_fun:
A => A => List[(B, C)],
                                       m: fsm[A, B, C],
                                       x2:
 ((List[(B, C)], A), (List[(B, C)], A)),
                                       t: prefix_tree[(B, C)]):
      prefix_tree[(B, C)]
  =
  (dist_fun, m, x2, t) match {
  case (dist_fun, m, ((alpha, q1), (beta, q2)), t) =>
    (if (intersection_is_distinguishing[A, B,
 C](m, aftera[(B, C)](t, alpha), q1, aftera[(B, C)](t, beta), q2))
      emptyc[(B, C)] else insertb[(B, C)](emptyc[(B, C)], (dist_fun(q1))(q2)))
}

def h_method_via_pair_framework[A : card_univ : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                 B : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                 C : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl](ma:
                            fsm[A, B, C],
                           m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val a =
      ((a: fsm[A, B, C]) => (b: ((List[(B, C)], A), (List[(B, C)], A))) =>
        (c: prefix_tree[(B, C)]) =>
        add_distinguishing_sequence_if_required[A, B, C](distHelper, a, b, c)):
        ((fsm[A, B, C]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) =>
            (prefix_tree[(B, C)]) => prefix_tree[(B, C)]);
    pair_framework_h_components[A, B, C](ma, m, a)
  }

def w_method_via_pair_framework[A : card_univ : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                 B : ceq : ccompare : equal : mapping_impl : linorder,
                                 C : ceq : ccompare : equal : mapping_impl : linorder : set_impl](ma:
                    fsm[A, B, C],
                   m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val distSet =
      from_list[(B, C)](mapa[(A, A),
                              List[(B, C)]](((a: (A, A)) =>
      {
        val (aa, b) = a: ((A, A));
        (distHelper(aa))(b)
      }),
     pairs)):
        (prefix_tree[(B, C)])
    val a =
      ((_: fsm[A, B, C]) => (_: ((List[(B, C)], A), (List[(B, C)], A))) =>
        (_: prefix_tree[(B, C)]) => distSet):
        ((fsm[A, B, C]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) =>
            (prefix_tree[(B, C)]) => prefix_tree[(B, C)]);
    pair_framework_h_components[A, B, C](ma, m, a)
  }

def hsi_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
       fsm[A, B, C],
      m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val distFun =
      ((_: nat) => (q: A) =>
        (if (member[A](q, states[A, B, C](ma)))
          the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))):
        (nat => A => prefix_tree[(B, C)]);
    h_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def complete_inputs_to_tree_initial[A : ccompare : equal : linorder,
                                     B : ccompare : equal : mapping_impl : linorder,
                                     C : ceq : ccompare : equal : mapping_impl : linorder](m:
             fsm[A, B, C],
            xs: List[B]):
      prefix_tree[(B, C)]
  =
  complete_inputs_to_tree[A, B,
                           C](m, initial[A, B, C](m),
                               outputs_as_list[A, B, C](m), xs)

def get_initial_test_suite_h_2[A : card_univ : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](c:
       Boolean,
      v: A => List[(B, C)], ma: fsm[A, B, C], m: nat):
      prefix_tree[(B, C)]
  =
  (if (c) get_initial_test_suite_h[A, B, C](v, ma, m)
    else {
           val ts =
             get_initial_test_suite_h[A, B, C](v, ma, m): (prefix_tree[(B, C)])
           val xss =
             mapa[List[(B, C)],
                   List[B]](((a: List[(B, C)]) =>
                              mapa[(B, C),
                                    B](((aa: (B, C)) => fst[B, C](aa)), a)),
                             sorted_list_of_maximal_sequences_in_tree[(B,
                                C)](ts)):
               (List[List[B]]);
           outputs_as_list[A, B, C](ma);
           foldl[prefix_tree[(B, C)],
                  List[B]](((t: prefix_tree[(B, C)]) => (xs: List[B]) =>
                             combinea[(B,
C)](t, complete_inputs_to_tree_initial[A, B, C](ma, xs))),
                            ts, xss)
         })

def pair_framework_h_components_2[A : card_univ : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                   B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
          fsm[A, B, C],
         m: nat,
         get_separating_traces:
           (fsm[A, B, C]) =>
             (((List[(B, C)], A), (List[(B, C)], A))) =>
               (prefix_tree[(B, C)]) => prefix_tree[(B, C)],
         c: Boolean):
      prefix_tree[(B, C)]
  =
  {
    val v = get_state_cover_assignment[A, B, C](ma): (A => List[(B, C)]);
    pair_framework[A, B,
                    C](ma, m,
                        ((a: fsm[A, B, C]) => (b: nat) =>
                          get_initial_test_suite_h_2[A, B, C](c, v, a, b)),
                        ((a: fsm[A, B, C]) => (b: nat) =>
                          get_pairs_h[A, B, C](v, a, b)),
                        get_separating_traces)
  }

def h_framework_static_with_simple_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
  B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
  C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m1:
                 fsm[A, B, C],
                dist_fun: nat => A => prefix_tree[(B, C)], m: nat):
      prefix_tree[(B, C)]
  =
  h_framework[A, B, C,
               List[fset[List[(B, C)]]]](m1,
  ((a: fsm[A, B, C]) => get_state_cover_assignment[A, B, C](a)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
    (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => List[fset[List[(B, C)]]]) =>
    (d: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (e: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    handle_state_cover_static[A, B, C,
                               List[fset[List[(B,
        C)]]]](dist_fun, a, b, c, d, e)),
  ((_: fsm[A, B, C]) => (_: A => List[(B, C)]) =>
    (ts: List[(A, (B, (C, A)))]) => ts),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
    (d: List[fset[List[(B, C)]]]) =>
    (e: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    (g: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (h: nat) => (i: (A, (B, (C, A)))) => (j: List[(A, (B, (C, A)))]) =>
    handleut_static[A, B, C,
                     List[fset[List[(B, C)]]]](dist_fun, a, b, c, d, e, f, g, h,
        i, j)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
    (d: List[fset[List[(B, C)]]]) =>
    (e: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    (g: A) => (h: B) => (i: C) =>
    handle_io_pair[A, B, C,
                    List[fset[List[(B, C)]]]](false, false, a, b, c, d, e, f, g,
       h, i)),
  ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
    simple_cg_initial[A, B, C](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
    simple_cg_insert[(B, C)](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
    simple_cg_lookup_with_conv[(B, C)](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
    simple_cg_merge[(B, C)](a, b, c)),
  m)

def spy_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
       fsm[A, B, C],
      m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val distFun =
      ((_: nat) => (q: A) =>
        (if (member[A](q, states[A, B, C](ma)))
          the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))):
        (nat => A => prefix_tree[(B, C)]);
    h_framework_static_with_simple_graph[A, B, C](ma, distFun, m)
  }

def wp_method_via_spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
        fsm[A, B, C],
       m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val pairs =
      filtera[(A, A)](((a: (A, A)) => {
val (x, y) = a: ((A, A));
! (eq[A](x, y))
                                      }),
                       list_ordered_pairs[A](states_as_list[A, B, C](ma))):
        (List[(A, A)])
    val distSet =
      from_list[(B, C)](mapa[(A, A),
                              List[(B, C)]](((a: (A, A)) =>
      {
        val (aa, b) = a: ((A, A));
        (distHelper(aa))(b)
      }),
     pairs)):
        (prefix_tree[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val l = Suc(minus_nat(m, card[A](reachable_states[A, B, C](ma)))): nat
    val distFun =
      ((k: nat) => (q: A) =>
        (if (equal_nata(k, l))
          (if (member[A](q, states[A, B, C](ma)))
            the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))
          else distSet)):
        (nat => A => prefix_tree[(B, C)]);
    spy_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def add_distinguishing_sequence_and_complete_if_required[A : ccompare : equal : linorder,
                  B : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                  C : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl](distFun:
             A => A => List[(B, C)],
            completeInputTraces: Boolean, m: fsm[A, B, C],
            x3: ((List[(B, C)], A), (List[(B, C)], A)), t: prefix_tree[(B, C)]):
      prefix_tree[(B, C)]
  =
  (distFun, completeInputTraces, m, x3, t) match {
  case (distFun, completeInputTraces, m, ((alpha, q1), (beta, q2)), t) =>
    (if (intersection_is_distinguishing[A, B,
 C](m, aftera[(B, C)](t, alpha), q1, aftera[(B, C)](t, beta), q2))
      emptyc[(B, C)]
      else {
             val w = (distFun(q1))(q2): (List[(B, C)])
             val ta = insertb[(B, C)](emptyc[(B, C)], w): (prefix_tree[(B, C)]);
             (if (completeInputTraces)
               {
                 val t1 =
                   from_list[(B, C)](language_for_input[A, B,
                 C](m, q1, mapa[(B, C), B](((a: (B, C)) => fst[B, C](a)), w))):
                     (prefix_tree[(B, C)])
                 val t2 =
                   from_list[(B, C)](language_for_input[A, B,
                 C](m, q2, mapa[(B, C), B](((a: (B, C)) => fst[B, C](a)), w))):
                     (prefix_tree[(B, C)]);
                 combinea[(B, C)](ta, combinea[(B, C)](t1, t2))
               }
               else ta)
           })
}

def h_method_via_pair_framework_2[A : card_univ : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                   B : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                   C : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl](ma:
                              fsm[A, B, C],
                             m: nat, c: Boolean):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val a =
      ((a: fsm[A, B, C]) => (b: ((List[(B, C)], A), (List[(B, C)], A))) =>
        (d: prefix_tree[(B, C)]) =>
        add_distinguishing_sequence_and_complete_if_required[A, B,
                      C](distHelper, c, a, b, d)):
        ((fsm[A, B, C]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) =>
            (prefix_tree[(B, C)]) => prefix_tree[(B, C)]);
    pair_framework_h_components[A, B, C](ma, m, a)
  }

def find_cheapest_distinguishing_trace[A : ccompare : equal,
B : ccompare : equal : mapping_impl : linorder,
C : ccompare : equal : mapping_impl : linorder](m: fsm[A, B, C],
         distFun: A => A => List[(B, C)], ios: List[(B, C)],
         x3: prefix_tree[(B, C)], q1: A, x5: prefix_tree[(B, C)], q2: A):
      (List[(B, C)], (nat, nat))
  =
  (m, distFun, ios, x3, q1, x5, q2) match {
  case (m, distFun, ios, MPT(m1), q1, MPT(m2), q2) =>
    {
      val f =
        ((a: (List[(B, C)], (nat, nat))) =>
          {
            val (omega, (l, w)) = a: ((List[(B, C)], (nat, nat)));
            ((aa: (B, C)) =>
              {
                val (x, y) = aa: ((B, C))
                val w1L =
                  (if (is_leaf[(B, C)](MPT[(B, C)](m1))) zero_nat
                    else one_nata):
                    nat
                val w1C =
                  (if (! (is_none[prefix_tree[(B,
        C)]]((lookupa[(B, C), prefix_tree[(B, C)]](m1)).apply((x, y)))))
                    zero_nat else one_nata):
                    nat
                val w1 = min[nat](w1L, w1C): nat
                val w2L =
                  (if (is_leaf[(B, C)](MPT[(B, C)](m2))) zero_nat
                    else one_nata):
                    nat
                val w2C =
                  (if (! (is_none[prefix_tree[(B,
        C)]]((lookupa[(B, C), prefix_tree[(B, C)]](m2)).apply((x, y)))))
                    zero_nat else one_nata):
                    nat
                val w2 = min[nat](w2L, w2C): nat
                val wa = plus_nat(w1, w2): nat;
                ((h_obs[A, B, C](m)).apply(q1).apply(x).apply(y) match {
                   case None =>
                     ((h_obs[A, B, C](m)).apply(q2).apply(x).apply(y) match {
                        case None => (omega, (l, w))
                        case Some(_) =>
                          (if (equal_nata(wa, zero_nat) || less_eq_nat(wa, w))
                            (List((x, y)), (plus_nat(w1C, w2C), wa))
                            else (omega, (l, w)))
                      })
                   case Some(q1a) =>
                     ((h_obs[A, B, C](m)).apply(q2).apply(x).apply(y) match {
                        case None =>
                          (if (equal_nata(wa, zero_nat) || less_eq_nat(wa, w))
                            (List((x, y)), (plus_nat(w1C, w2C), wa))
                            else (omega, (l, w)))
                        case Some(q2a) =>
                          (if (eq[A](q1a, q2a)) (omega, (l, w))
                            else ((lookupa[(B, C),
    prefix_tree[(B, C)]](m1)).apply((x, y))
                                    match {
                                    case None =>
                                      ((lookupa[(B, C),
         prefix_tree[(B, C)]](m2)).apply((x, y))
 match {
 case None =>
   {
     val omegaa = (distFun(q1a))(q2a): (List[(B, C)])
     val la =
       plus_nat(nat_of_integer(BigInt(2)),
                 times_nata(nat_of_integer(BigInt(2)),
                             size_list[(B, C)].apply(omegaa))):
         nat;
     (if (less_nat(wa, w) || equal_nata(wa, w) && less_nat(la, l))
       ((x, y) :: omegaa, (la, wa)) else (omega, (l, w)))
   }
 case Some(t2) =>
   {
     val (omegaa, (la, waa)) =
       find_cheapest_distinguishing_trace[A, B,
   C](m, distFun, ios, emptyc[(B, C)], q1a, t2, q2a):
         ((List[(B, C)], (nat, nat)));
     (if (less_nat(plus_nat(waa, w1), w) ||
            equal_nata(plus_nat(waa, w1), w) &&
              less_nat(plus_nat(la, one_nata), l))
       ((x, y) :: omegaa, (plus_nat(la, one_nata), plus_nat(waa, w1)))
       else (omega, (l, w)))
   }
                                       })
                                    case Some(t1) =>
                                      ((lookupa[(B, C),
         prefix_tree[(B, C)]](m2)).apply((x, y))
 match {
 case None =>
   {
     val (omegaa, (la, waa)) =
       find_cheapest_distinguishing_trace[A, B,
   C](m, distFun, ios, t1, q1a, emptyc[(B, C)], q2a):
         ((List[(B, C)], (nat, nat)));
     (if (less_nat(plus_nat(waa, w2), w) ||
            equal_nata(plus_nat(waa, w2), w) &&
              less_nat(plus_nat(la, one_nata), l))
       ((x, y) :: omegaa, (plus_nat(la, one_nata), plus_nat(waa, w2)))
       else (omega, (l, w)))
   }
 case Some(t2) =>
   {
     val (omegaa, (la, waa)) =
       find_cheapest_distinguishing_trace[A, B,
   C](m, distFun, ios, t1, q1a, t2, q2a):
         ((List[(B, C)], (nat, nat)));
     (if (less_nat(waa, w) || equal_nata(waa, w) && less_nat(la, l))
       ((x, y) :: omegaa, (la, waa)) else (omega, (l, w)))
   }
                                       })
                                  }))
                      })
                 })
              })
          }):
          (((List[(B, C)], (nat, nat))) =>
            ((B, C)) => (List[(B, C)], (nat, nat)));
      foldl[(List[(B, C)], (nat, nat)),
             (B, C)](f, ((distFun(q1))(q2),
                          (zero_nat, nat_of_integer(BigInt(3)))),
                      ios)
    }
}

def add_cheapest_distinguishing_trace[A : ccompare : equal : linorder,
                                       B : ceq : ccompare : equal : mapping_impl : linorder,
                                       C : ceq : ccompare : equal : mapping_impl : linorder](distFun:
               A => A => List[(B, C)],
              completeInputTraces: Boolean, m: fsm[A, B, C],
              x3: ((List[(B, C)], A), (List[(B, C)], A)),
              t: prefix_tree[(B, C)]):
      prefix_tree[(B, C)]
  =
  (distFun, completeInputTraces, m, x3, t) match {
  case (distFun, completeInputTraces, m, ((alpha, q1), (beta, q2)), t) =>
    {
      val w =
        fst[List[(B, C)],
             (nat, nat)](find_cheapest_distinguishing_trace[A, B,
                     C](m, distFun,
                         product[B, C](inputs_as_list[A, B, C](m),
outputs_as_list[A, B, C](m)),
                         aftera[(B, C)](t, alpha), q1, aftera[(B, C)](t, beta),
                         q2)):
          (List[(B, C)])
      val ta = insertb[(B, C)](emptyc[(B, C)], w): (prefix_tree[(B, C)]);
      (if (completeInputTraces)
        {
          val t1 =
            complete_inputs_to_tree[A, B,
                                     C](m, q1, outputs_as_list[A, B, C](m),
 mapa[(B, C), B](((a: (B, C)) => fst[B, C](a)), w)):
              (prefix_tree[(B, C)])
          val t2 =
            complete_inputs_to_tree[A, B,
                                     C](m, q2, outputs_as_list[A, B, C](m),
 mapa[(B, C), B](((a: (B, C)) => fst[B, C](a)), w)):
              (prefix_tree[(B, C)]);
          combinea[(B, C)](ta, combinea[(B, C)](t1, t2))
        }
        else ta)
    }
}

def h_method_via_pair_framework_3[A : card_univ : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                   B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
          fsm[A, B, C],
         m: nat, c1: Boolean, c2: Boolean):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val distFun =
      ((a: fsm[A, B, C]) => (b: ((List[(B, C)], A), (List[(B, C)], A))) =>
        (c: prefix_tree[(B, C)]) =>
        add_cheapest_distinguishing_trace[A, B, C](distHelper, c2, a, b, c)):
        ((fsm[A, B, C]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) =>
            (prefix_tree[(B, C)]) => prefix_tree[(B, C)]);
    pair_framework_h_components_2[A, B, C](ma, m, distFun, c1)
  }

def apply_method_to_prime(m: fsm[BigInt, BigInt, BigInt],
                           additionalStates: BigInt, isAlreadyPrime: Boolean,
                           f: (fsm[BigInt, BigInt, BigInt]) =>
                                nat => prefix_tree[(BigInt, BigInt)]):
      prefix_tree[(BigInt, BigInt)]
  =
  {
    val ma =
      (if (isAlreadyPrime) m else to_prime[BigInt, BigInt, BigInt](m)):
        (fsm[BigInt, BigInt, BigInt])
    val a =
      plus_nat(card[BigInt](reachable_states[BigInt, BigInt, BigInt](ma)),
                nat_of_integer(additionalStates)):
        nat;
    (f(ma))(a)
  }

def fsm_from_list_integer(q: BigInt,
                           ts: List[(BigInt, (BigInt, (BigInt, BigInt)))]):
      fsm[BigInt, BigInt, BigInt]
  =
  fsm_from_list[BigInt, BigInt, BigInt](q, ts)

def hsi_method_via_spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
         fsm[A, B, C],
        m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val distFun =
      ((_: nat) => (q: A) =>
        (if (member[A](q, states[A, B, C](ma)))
          the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))):
        (nat => A => prefix_tree[(B, C)]);
    spy_framework_static_with_empty_graph[A, B, C](ma, distFun, m)
  }

def h_framework_dynamic_with_simple_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
   B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](convergence_decisision:
                  (fsm[A, B, C]) =>
                    (A => List[(B, C)]) =>
                      ((A, (B, (C, A)))) =>
                        (List[(A, (B, (C, A)))]) => nat => Boolean,
                 m1: fsm[A, B, C], m: nat, completeInputTraces: Boolean,
                 useInputHeuristic: Boolean):
      prefix_tree[(B, C)]
  =
  h_framework[A, B, C,
               List[fset[List[(B, C)]]]](m1,
  ((a: fsm[A, B, C]) => get_state_cover_assignment[A, B, C](a)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
    (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => List[fset[List[(B, C)]]]) =>
    (d: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (e: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    handle_state_cover_dynamic[A, B, C,
                                List[fset[List[(B,
         C)]]]](completeInputTraces, useInputHeuristic,
                 ((aa: A) => (ba: A) =>
                   get_distinguishing_sequence_from_ofsm_tables[A, B,
                         C](m1, aa, ba)),
                 a, b, c, d, e)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: List[(A, (B, (C, A)))]) =>
    sort_unverified_transitions_by_state_cover_length[A, B, C](a, b, c)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
    (d: List[fset[List[(B, C)]]]) =>
    (e: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    (g: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (h: nat) => (i: (A, (B, (C, A)))) => (j: List[(A, (B, (C, A)))]) =>
    handleut_dynamic[A, B, C,
                      List[fset[List[(B, C)]]]](completeInputTraces,
         useInputHeuristic,
         ((aa: A) => (ba: A) =>
           get_distinguishing_sequence_from_ofsm_tables[A, B, C](m1, aa, ba)),
         convergence_decisision, a, b, c, d, e, f, g, h, i, j)),
  ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
    (d: List[fset[List[(B, C)]]]) =>
    (e: (List[fset[List[(B, C)]]]) =>
          (List[(B, C)]) => List[fset[List[(B, C)]]])
      =>
    (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
    (g: A) => (h: B) => (i: C) =>
    handle_io_pair[A, B, C,
                    List[fset[List[(B, C)]]]](completeInputTraces,
       useInputHeuristic, a, b, c, d, e, f, g, h, i)),
  ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
    simple_cg_initial[A, B, C](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
    simple_cg_insert[(B, C)](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
    simple_cg_lookup_with_conv[(B, C)](a, b)),
  ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
    simple_cg_merge[(B, C)](a, b, c)),
  m)

def spyh_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                 C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl]:
      (fsm[A, B, C]) => nat => Boolean => Boolean => prefix_tree[(B, C)]
  =
  ((a: fsm[A, B, C]) => (b: nat) => (c: Boolean) => (d: Boolean) =>
    h_framework_dynamic_with_simple_graph[A, B,
   C](((_: fsm[A, B, C]) => (_: A => List[(B, C)]) => (_: (A, (B, (C, A)))) =>
        (_: List[(A, (B, (C, A)))]) => (_: nat) => true),
       a, b, c, d))

def spy_framework_static_with_simple_graph[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
    B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
    C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m1:
                   fsm[A, B, C],
                  dist_fun: nat => A => prefix_tree[(B, C)], m: nat):
      prefix_tree[(B, C)]
  =
  spy_framework[A, B, C,
                 List[fset[List[(B, C)]]]](m1,
    ((a: fsm[A, B, C]) => get_state_cover_assignment[A, B, C](a)),
    ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
      (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => List[fset[List[(B, C)]]])
        =>
      (d: (List[fset[List[(B, C)]]]) =>
            (List[(B, C)]) => List[fset[List[(B, C)]]])
        =>
      (e: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
      handle_state_cover_static[A, B, C,
                                 List[fset[List[(B,
          C)]]]](dist_fun, a, b, c, d, e)),
    ((_: fsm[A, B, C]) => (_: A => List[(B, C)]) =>
      (ts: List[(A, (B, (C, A)))]) => ts),
    ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
      (d: List[fset[List[(B, C)]]]) =>
      (e: (List[fset[List[(B, C)]]]) =>
            (List[(B, C)]) => List[fset[List[(B, C)]]])
        =>
      (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
      (g: nat) => (h: (A, (B, (C, A)))) =>
      establish_convergence_static[A, B, C,
                                    List[fset[List[(B,
             C)]]]](dist_fun, a, b, c, d, e, f, g, h)),
    ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) => (c: prefix_tree[(B, C)]) =>
      (d: List[fset[List[(B, C)]]]) =>
      (e: (List[fset[List[(B, C)]]]) =>
            (List[(B, C)]) => List[fset[List[(B, C)]]])
        =>
      (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]]) =>
      (g: A) => (h: B) => (i: C) =>
      handle_io_pair[A, B, C,
                      List[fset[List[(B, C)]]]](false, true, a, b, c, d, e, f,
         g, h, i)),
    ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
      simple_cg_initial[A, B, C](a, b)),
    ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
      simple_cg_insert[(B, C)](a, b)),
    ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
      simple_cg_lookup_with_conv[(B, C)](a, b)),
    ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) => (c: List[(B, C)]) =>
      simple_cg_merge[(B, C)](a, b, c)),
    m)

def spy_method_via_spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                  C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](ma:
         fsm[A, B, C],
        m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val hsiMap =
      ((a: A) =>
        map_of[A, prefix_tree[(B, C)]](mapa[A,
     (A, prefix_tree[(B, C)])](((q: A) =>
                                 (q, from_list[(B,
         C)](map_filter[A, List[(B, C)]](((x: A) =>
   (if (! (eq[A](q, x))) Some[List[(B, C)]]((distHelper(q))(x)) else None)),
  states_as_list[A, B, C](ma))))),
                                states_as_list[A, B, C](ma)),
a)):
        (A => Option[prefix_tree[(B, C)]])
    val distFun =
      ((_: nat) => (q: A) =>
        (if (member[A](q, states[A, B, C](ma)))
          the[prefix_tree[(B, C)]](hsiMap(q)) else get_hsi[A, B, C](ma, q))):
        (nat => A => prefix_tree[(B, C)]);
    spy_framework_static_with_simple_graph[A, B, C](ma, distFun, m)
  }

def hsi_method_via_pair_framework[A : card_univ : cenum : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                                   B : ceq : ccompare : equal : mapping_impl : linorder,
                                   C : ceq : ccompare : equal : mapping_impl : linorder : set_impl](ma:
                      fsm[A, B, C],
                     m: nat):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](ma, minus_nat(size[A, B, C](ma), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, ma, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](ma),
                                   states_as_list[A, B, C](ma))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](ma)) &&
               (member[A](q2, states[A, B, C](ma)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](ma, q1, q2))):
        (A => A => List[(B, C)])
    val a =
      ((_: fsm[A, B, C]) => (a: ((List[(B, C)], A), (List[(B, C)], A))) =>
        {
          val (aa, b) = a: (((List[(B, C)], A), (List[(B, C)], A)));
          ({
             val (_, q1) = aa: ((List[(B, C)], A));
             ((ab: (List[(B, C)], A)) =>
               {
                 val (_, q2) = ab: ((List[(B, C)], A));
                 ((_: prefix_tree[(B, C)]) =>
                   insertb[(B, C)](emptyc[(B, C)], (distHelper(q1))(q2)))
               })
           })(b)
        }):
        ((fsm[A, B, C]) =>
          (((List[(B, C)], A), (List[(B, C)], A))) =>
            (prefix_tree[(B, C)]) => prefix_tree[(B, C)]);
    pair_framework_h_components[A, B, C](ma, m, a)
  }

def test_suite_to_input_sequences[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   B : card_univ : cenum : ceq : cproper_interval : equal : linorder : set_impl](t:
                                   prefix_tree[(A, B)]):
      List[List[A]]
  =
  sorted_list_of_maximal_sequences_in_tree[A](from_list[A](mapa[List[(A, B)],
                         List[A]](((a: List[(A, B)]) =>
                                    mapa[(A, B),
  A](((aa: (A, B)) => fst[A, B](aa)), a)),
                                   sorted_list_of_maximal_sequences_in_tree[(A,
                                      B)](t))))

def do_establish_convergence[A : ceq : ccompare : equal : mapping_impl : linorder : set_impl,
                              B : ceq : ccompare : equal : mapping_impl : linorder,
                              C : ceq : ccompare : linorder : set_impl](m:
                                  fsm[A, B, C],
                                 v: A => List[(B, C)], t: (A, (B, (C, A))),
                                 x: List[(A, (B, (C, A)))], l: nat):
      Boolean
  =
  ! (is_none[(A, (B, (C, A)))](find[(A, (B,
  (C, A)))](((ta: (A, (B, (C, A)))) =>
              distance_at_most[A, B,
                                C](m, snd[C,
   A](snd[B, (C, A)](snd[A, (B, (C, A))](t))),
                                    fst[A, (B, (C, A))](ta), l)),
             x)))

def establish_convergence_dynamic[A : card_univ : cenum : ceq : ccompare : equal : linorder : set_impl,
                                   B : ceq : ccompare : equal : mapping_impl : linorder,
                                   C : ceq : ccompare : equal : mapping_impl : linorder,
                                   D](completeInputTraces: Boolean,
                                       useInputHeuristic: Boolean,
                                       dist_fun: A => A => List[(B, C)],
                                       m1: fsm[A, B, C], v: A => List[(B, C)],
                                       ta: prefix_tree[(B, C)], g: D,
                                       cg_insert: D => (List[(B, C)]) => D,
                                       cg_lookup:
 D => (List[(B, C)]) => List[List[(B, C)]],
                                       m: nat, t: (A, (B, (C, A)))):
      (prefix_tree[(B, C)], D)
  =
  distinguish_from_set[A, B, C,
                        D](m1, v, ta, g, cg_lookup, cg_insert, dist_fun,
                            v(fst[A, (B, (C, A))](t)) ++
                              List((fst[B, (C, A)](snd[A, (B, (C, A))](t)),
                                     fst[C,
  A](snd[B, (C, A)](snd[A, (B, (C, A))](t))))),
                            v(snd[C, A](snd[B,
     (C, A)](snd[A, (B, (C, A))](t)))),
                            mapa[A, List[(B,
   C)]](v, reachable_states_as_list[A, B, C](m1)),
                            times_nata(nat_of_integer(BigInt(2)),
size[A, B, C](m1)),
                            minus_nat(m, card[A](reachable_states[A, B,
                           C](m1))),
                            completeInputTraces,
                            (if (useInputHeuristic)
                              ((a: prefix_tree[(B, C)]) => (b: List[(B, C)]) =>
                                (c: (List[(B, C)], int)) => (d: List[(B, C)]) =>
                                append_heuristic_input[A, B, C](m1, a, b, c, d))
                              else ((a: prefix_tree[(B, C)]) =>
                                     (b: List[(B, C)]) =>
                                     (c: (List[(B, C)], int)) =>
                                     (d: List[(B, C)]) =>
                                     append_heuristic_io[B, C](a, b, c, d))),
                            false)

def spyh_method_via_spy_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                   C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl](m1:
          fsm[A, B, C],
         m: nat, completeInputTraces: Boolean, useInputHeuristic: Boolean):
      prefix_tree[(B, C)]
  =
  {
    val tables =
      compute_ofsm_tables[A, B, C](m1, minus_nat(size[A, B, C](m1), one_nata)):
        (mapping[nat, mapping[A, set[A]]])
    val distMap =
      ((a: (A, A)) =>
        map_of[(A, A),
                List[(B, C)]](map_filter[(A, A),
  ((A, A),
    List[(B, C)])](((x: (A, A)) =>
                     (if (! (eq[A](fst[A, A](x), snd[A, A](x))))
                       Some[((A, A),
                              List[(B, C)])]({
       val (q1, q2) = x: ((A, A));
       ((q1, q2),
         get_distinguishing_sequence_from_ofsm_tables_with_provided_tables[A, B,
                                    C](tables, m1, q1, q2))
     })
                       else None)),
                    product[A, A](states_as_list[A, B, C](m1),
                                   states_as_list[A, B, C](m1))),
                               a)):
        (((A, A)) => Option[List[(B, C)]])
    val distHelper =
      ((q1: A) => (q2: A) =>
        (if (member[A](q1, states[A, B, C](m1)) &&
               (member[A](q2, states[A, B, C](m1)) && ! (eq[A](q1, q2))))
          the[List[(B, C)]](distMap((q1, q2)))
          else get_distinguishing_sequence_from_ofsm_tables[A, B,
                     C](m1, q1, q2))):
        (A => A => List[(B, C)]);
    spy_framework[A, B, C,
                   List[fset[List[(B, C)]]]](m1,
      ((a: fsm[A, B, C]) => get_state_cover_assignment[A, B, C](a)),
      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
        (c: (fsm[A, B, C]) => (prefix_tree[(B, C)]) => List[fset[List[(B, C)]]])
          =>
        (d: (List[fset[List[(B, C)]]]) =>
              (List[(B, C)]) => List[fset[List[(B, C)]]])
          =>
        (e: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]])
          =>
        handle_state_cover_dynamic[A, B, C,
                                    List[fset[List[(B,
             C)]]]](completeInputTraces, useInputHeuristic, distHelper, a, b, c,
                     d, e)),
      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
        (c: List[(A, (B, (C, A)))]) =>
        sort_unverified_transitions_by_state_cover_length[A, B, C](a, b, c)),
      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
        (c: prefix_tree[(B, C)]) => (d: List[fset[List[(B, C)]]]) =>
        (e: (List[fset[List[(B, C)]]]) =>
              (List[(B, C)]) => List[fset[List[(B, C)]]])
          =>
        (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]])
          =>
        (g: nat) => (h: (A, (B, (C, A)))) =>
        establish_convergence_dynamic[A, B, C,
                                       List[fset[List[(B,
                C)]]]](completeInputTraces, useInputHeuristic, distHelper, a, b,
                        c, d, e, f, g, h)),
      ((a: fsm[A, B, C]) => (b: A => List[(B, C)]) =>
        (c: prefix_tree[(B, C)]) => (d: List[fset[List[(B, C)]]]) =>
        (e: (List[fset[List[(B, C)]]]) =>
              (List[(B, C)]) => List[fset[List[(B, C)]]])
          =>
        (f: (List[fset[List[(B, C)]]]) => (List[(B, C)]) => List[List[(B, C)]])
          =>
        (g: A) => (h: B) => (i: C) =>
        handle_io_pair[A, B, C,
                        List[fset[List[(B,
 C)]]]](completeInputTraces, useInputHeuristic, a, b, c, d, e, f, g, h, i)),
      ((a: fsm[A, B, C]) => (b: prefix_tree[(B, C)]) =>
        simple_cg_initial[A, B, C](a, b)),
      ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
        simple_cg_insert[(B, C)](a, b)),
      ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
        simple_cg_lookup_with_conv[(B, C)](a, b)),
      ((a: List[fset[List[(B, C)]]]) => (b: List[(B, C)]) =>
        (c: List[(B, C)]) => simple_cg_merge[(B, C)](a, b, c)),
      m)
  }

def apply_to_prime_and_return_io_lists(m: fsm[BigInt, BigInt, BigInt],
additionalStates: BigInt, isAlreadyPrime: Boolean,
f: (fsm[BigInt, BigInt, BigInt]) => nat => prefix_tree[(BigInt, BigInt)]):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  {
    val ma =
      (if (isAlreadyPrime) m else to_prime[BigInt, BigInt, BigInt](m)):
        (fsm[BigInt, BigInt, BigInt]);
    sorted_list_of_maximal_sequences_in_tree[((BigInt, BigInt),
       Boolean)](test_suite_from_io_tree[BigInt, BigInt,
  BigInt](ma, initial[BigInt, BigInt, BigInt](ma),
           apply_method_to_prime(m, additionalStates, isAlreadyPrime, f)))
  }

def h_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                 additionalStates: BigInt,
                                 isAlreadyPrime: Boolean, c: Boolean,
                                 b: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
h_method_via_h_framework[BigInt, BigInt,
                          BigInt].apply(ma).apply(maa).apply(c).apply(b)))

def w_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                 additionalStates: BigInt,
                                 isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => w_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def wp_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                  additionalStates: BigInt,
                                  isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => wp_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def hsi_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                   additionalStates: BigInt,
                                   isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => hsi_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def spy_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                   additionalStates: BigInt,
                                   isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => spy_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def w_method_via_h_framework_2_ts(m: fsm[BigInt, BigInt, BigInt],
                                   additionalStates: BigInt,
                                   isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => w_method_via_h_framework_2[BigInt, BigInt, BigInt](a, b)))

def w_method_via_spy_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                   additionalStates: BigInt,
                                   isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => w_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def apply_to_prime_and_return_input_lists(m: fsm[BigInt, BigInt, BigInt],
   additionalStates: BigInt, isAlreadyPrime: Boolean,
   f: (fsm[BigInt, BigInt, BigInt]) => nat => prefix_tree[(BigInt, BigInt)]):
      List[List[BigInt]]
  =
  test_suite_to_input_sequences[BigInt,
                                 BigInt](apply_method_to_prime(m,
                        additionalStates, isAlreadyPrime, f))

def h_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean, c: Boolean,
                                    b: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   h_method_via_h_framework[BigInt, BigInt,
                             BigInt].apply(ma).apply(maa).apply(c).apply(b)))

def h_method_via_pair_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => h_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def spyh_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean, c: Boolean,
                                    b: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
spyh_method_via_h_framework[BigInt, BigInt,
                             BigInt].apply(ma).apply(maa).apply(c).apply(b)))

def w_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   w_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def w_method_via_pair_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => w_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def wp_method_via_spy_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                    additionalStates: BigInt,
                                    isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => wp_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def hsi_method_via_spy_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                     additionalStates: BigInt,
                                     isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => hsi_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def spy_method_via_spy_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                     additionalStates: BigInt,
                                     isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => spy_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def wp_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                     additionalStates: BigInt,
                                     isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   wp_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def partial_s_method_via_h_framework[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                      B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                                      C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl]:
      (fsm[A, B, C]) => nat => Boolean => Boolean => prefix_tree[(B, C)]
  =
  ((a: fsm[A, B, C]) => (b: nat) => (c: Boolean) => (d: Boolean) =>
    h_framework_dynamic_with_simple_graph[A, B,
   C](((aa: fsm[A, B, C]) => (ba: A => List[(B, C)]) =>
        (ca: (A, (B, (C, A)))) => (da: List[(A, (B, (C, A)))]) => (e: nat) =>
        do_establish_convergence[A, B, C](aa, ba, ca, da, e)),
       a, b, c, d))

def h_method_via_pair_framework_2_ts(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean, c: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
h_method_via_pair_framework_2[BigInt, BigInt, BigInt](ma, maa, c)))

def h_method_via_pair_framework_3_ts(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean, c1: Boolean,
                                      c2: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
h_method_via_pair_framework_3[BigInt, BigInt, BigInt](ma, maa, c1, c2)))

def hsi_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   hsi_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def hsi_method_via_pair_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((a: fsm[BigInt, BigInt, BigInt]) =>
(b: nat) => hsi_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def spy_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   spy_method_via_h_framework[BigInt, BigInt, BigInt](a, b)))

def spyh_method_via_spy_framework_ts(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean, c: Boolean,
                                      b: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
spyh_method_via_spy_framework[BigInt, BigInt, BigInt](ma, maa, c, b)))

def w_method_via_h_framework_2_input(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   w_method_via_h_framework_2[BigInt, BigInt, BigInt](a, b)))

def w_method_via_spy_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                      additionalStates: BigInt,
                                      isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   w_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def h_method_via_pair_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                       additionalStates: BigInt,
                                       isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   h_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def spyh_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                       additionalStates: BigInt,
                                       isAlreadyPrime: Boolean, c: Boolean,
                                       b: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   spyh_method_via_h_framework[BigInt, BigInt,
                                BigInt].apply(ma).apply(maa).apply(c).apply(b)))

def w_method_via_pair_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                       additionalStates: BigInt,
                                       isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   w_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def wp_method_via_spy_framework_input(m: fsm[BigInt, BigInt, BigInt],
                                       additionalStates: BigInt,
                                       isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   wp_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def hsi_method_via_spy_framework_input(m: fsm[BigInt, BigInt, BigInt],
additionalStates: BigInt, isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   hsi_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def spy_method_via_spy_framework_input(m: fsm[BigInt, BigInt, BigInt],
additionalStates: BigInt, isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   spy_method_via_spy_framework[BigInt, BigInt, BigInt](a, b)))

def calculate_test_suite_naive_as_io_sequences_with_assumption_check[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                              C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](ma:
                                  fsm[A, B, C],
                                 m: nat):
      sum[String, set[List[(B, C)]]]
  =
  (if (! (is_empty[B](inputs[A, B, C](ma))))
    (if (observable[A, B, C](ma))
      (if (completely_specified[A, B, C](ma))
        Inr[set[List[(B, C)]],
             String](test_suite_to_io_maximal[A, B, C,
       sum[(A, A), A]](ma, calculate_test_suite_naive[A, B, C](ma, m)))
        else Inl[String,
                  set[List[(B, C)]]]("specification is not completely specified"))
      else Inl[String, set[List[(B, C)]]]("specification is not observable"))
    else Inl[String, set[List[(B, C)]]]("specification has no inputs"))

def generate_reduction_test_suite_naive(ma: fsm[BigInt, BigInt, BigInt],
 m: BigInt):
      sum[String, List[List[(BigInt, BigInt)]]]
  =
  (calculate_test_suite_naive_as_io_sequences_with_assumption_check[BigInt,
                             BigInt, BigInt](ma, nat_of_integer(m))
     match {
     case Inl(a) => Inl[String, List[List[(BigInt, BigInt)]]](a)
     case Inr(ts) =>
       Inr[List[List[(BigInt, BigInt)]],
            String](sorted_list_of_set[List[(BigInt, BigInt)]](ts))
   })

def h_method_via_pair_framework_2_input(m: fsm[BigInt, BigInt, BigInt],
 additionalStates: BigInt, isAlreadyPrime: Boolean, c: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   h_method_via_pair_framework_2[BigInt, BigInt, BigInt](ma, maa, c)))

def h_method_via_pair_framework_3_input(m: fsm[BigInt, BigInt, BigInt],
 additionalStates: BigInt, isAlreadyPrime: Boolean, c1: Boolean, c2: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   h_method_via_pair_framework_3[BigInt, BigInt, BigInt](ma, maa, c1, c2)))

def hsi_method_via_pair_framework_input(m: fsm[BigInt, BigInt, BigInt],
 additionalStates: BigInt, isAlreadyPrime: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((a: fsm[BigInt, BigInt, BigInt]) => (b: nat) =>
   hsi_method_via_pair_framework[BigInt, BigInt, BigInt](a, b)))

def partial_s_method_via_h_framework_ts(m: fsm[BigInt, BigInt, BigInt],
 additionalStates: BigInt, isAlreadyPrime: Boolean, c: Boolean, b: Boolean):
      List[List[((BigInt, BigInt), Boolean)]]
  =
  apply_to_prime_and_return_io_lists(m, additionalStates, isAlreadyPrime,
                                      ((ma: fsm[BigInt, BigInt, BigInt]) =>
(maa: nat) =>
partial_s_method_via_h_framework[BigInt, BigInt,
                                  BigInt].apply(ma).apply(maa).apply(c).apply(b)))

def spyh_method_via_spy_framework_input(m: fsm[BigInt, BigInt, BigInt],
 additionalStates: BigInt, isAlreadyPrime: Boolean, c: Boolean, b: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   spyh_method_via_spy_framework[BigInt, BigInt, BigInt](ma, maa, c, b)))

def calculate_test_suite_greedy_as_io_sequences_with_assumption_check[A : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                               B : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : linorder : set_impl,
                               C : card_univ : cenum : ceq : cproper_interval : equal : mapping_impl : set_impl](ma:
                                   fsm[A, B, C],
                                  m: nat):
      sum[String, set[List[(B, C)]]]
  =
  (if (! (is_empty[B](inputs[A, B, C](ma))))
    (if (observable[A, B, C](ma))
      (if (completely_specified[A, B, C](ma))
        Inr[set[List[(B, C)]],
             String](test_suite_to_io_maximal[A, B, C,
       sum[(A, A), A]](ma, calculate_test_suite_greedy[A, B, C](ma, m)))
        else Inl[String,
                  set[List[(B, C)]]]("specification is not completely specified"))
      else Inl[String, set[List[(B, C)]]]("specification is not observable"))
    else Inl[String, set[List[(B, C)]]]("specification has no inputs"))

def generate_reduction_test_suite_greedy(ma: fsm[BigInt, BigInt, BigInt],
  m: BigInt):
      sum[String, List[List[(BigInt, BigInt)]]]
  =
  (calculate_test_suite_greedy_as_io_sequences_with_assumption_check[BigInt,
                              BigInt, BigInt](ma, nat_of_integer(m))
     match {
     case Inl(a) => Inl[String, List[List[(BigInt, BigInt)]]](a)
     case Inr(ts) =>
       Inr[List[List[(BigInt, BigInt)]],
            String](sorted_list_of_set[List[(BigInt, BigInt)]](ts))
   })

def partial_s_method_via_h_framework_input(m: fsm[BigInt, BigInt, BigInt],
    additionalStates: BigInt, isAlreadyPrime: Boolean, c: Boolean, b: Boolean):
      List[List[BigInt]]
  =
  apply_to_prime_and_return_input_lists(m, additionalStates, isAlreadyPrime,
 ((ma: fsm[BigInt, BigInt, BigInt]) => (maa: nat) =>
   partial_s_method_via_h_framework[BigInt, BigInt,
                                     BigInt].apply(ma).apply(maa).apply(c).apply(b)))

} /* object GeneratedCode */
